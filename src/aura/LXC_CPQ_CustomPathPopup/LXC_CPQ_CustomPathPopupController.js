({
    
    showMessage :  function(component, event, helper) {  

        if(component.get("v.oldProcessStep") == 'None' && component.get("v.newProcessStep") == 'Strategy Call'){
            component.set('v.isOpen', true); 
            component.set("v.popupMessage",'Strategy Call Scheduled.'); 
            component.set("v.popupMessage2",'Do you need a Map Call?'); 
            component.set('v.showDate', true); 
            component.set("v.oldProcessStep","Strategy Call");
            component.set("v.newProcessStep","");
            var today = new Date();
            var monthDigit = today.getMonth() + 1;
            if (monthDigit <= 9) {
                monthDigit = '0' + monthDigit;
            }
            var dayDigit = today.getDate();
            if(dayDigit <= 9){
                dayDigit = '0' + dayDigit;
            }
            component.set('v.todayDate', today.getFullYear() + "-" + monthDigit + "-" + dayDigit);
        }else if(component.get("v.oldProcessStep") == 'Strategy Call' && component.get("v.newProcessStep") == 'Map Call'){
            component.set('v.isOpen', true);
            component.set("v.popupMessage",'Do you need a Map Call?'); 
            component.set("v.popupMessage2",'');
            component.set('v.showDate', false); 
            component.set("v.oldProcessStep","Map Call");
            component.set("v.newProcessStep","");
        }else if(component.get("v.oldProcessStep") == 'Map Call' && component.get("v.newProcessStep") == 'QC Self Check Draft'){
            component.set('v.isOpen', true);
            component.set("v.popupMessage",'Do you need QC Self Check?'); 
            component.set("v.popupMessage2",'');
            component.set('v.showDate', false); 
            component.set("v.oldProcessStep","QC Self Check Draft");
        }else if((component.get("v.oldProcessStep") == 'QC Self Check Draft' && component.get("v.newProcessStep") == 'Line Manager QC' && !component.get("v.iscompleteQCSelfCheckDraft")) || (component.get("v.oldProcessStep") == 'Strategy Call' && component.get("v.newProcessStep") == 'QC Self Check Draft')){
            component.set('v.isOpen', true);
            component.set('v.isMessage', false);
            component.set("v.oldProcessStep","QC Self Check Draft");
            component.set("v.newProcessStep","");
            component.set("v.qcProcessStep", "QC Self Check Draft");
            var childComp = component.find('QCComponent');
            childComp.loadGuidelines(); 
        }else if(component.get("v.oldProcessStep") == 'QC Self Check Draft' && component.get("v.newProcessStep") == 'Line Manager QC' && component.get("v.iscompleteQCSelfCheckDraft")){
            component.set("v.popupMessage",'Do you need Line Manager QC');
            component.set("v.popupMessage2",'');
            component.set('v.showDate', false); 
            component.set("v.oldProcessStep","Line Manager QC");
            component.set("v.newProcessStep","");
            component.set("v.iscompleteQCSelfCheckDraft", false);
        }else if(component.get("v.oldProcessStep") == 'Line Manager QC' && component.get("v.newProcessStep") == 'Key Stakeholder Review and Challenge Call'){
            component.set("v.popupMessage",'Proceed with Key Stakeholder Review and Challenge Call');
            component.set("v.popupMessage2",'');
            component.set('v.showDate', false); 
            component.set("v.oldProcessStep","Key Stakeholder Review and Challenge Call");
            component.set("v.newProcessStep","");
        }else if(component.get("v.newProcessStep") == 'Challenge Call Complete' && component.get("v.agreementStatus") == 'Internal Review'){
            component.set('v.isOpen', true);
            component.set("v.popupMessage",'Proceed with TSL Review'); 
            component.set("v.popupMessage2",'');
            component.set('v.showDate', false); 
            component.set("v.oldProcessStep","Challenge Call Complete");
            component.set("v.newProcessStep","");
        }else if((component.get("v.oldProcessStep") == 'Bid Grid (Maintenance) Completion' && component.get("v.newProcessStep") == 'QC Self Check Final') 
            || (component.get("v.oldProcessStep") == 'QC Self Check Final' && component.get("v.newProcessStep") == 'Final QC' && !component.get("v.iscompleteQCSelfCheckDraft"))){
            component.set('v.isOpen', true);
            component.set('v.isMessage', false);
            component.set("v.oldProcessStep","QC Self Check Final");
            component.set("v.newProcessStep","");
            component.set("v.qcProcessStep", "QC Self Check Final");
            var childComp = component.find('QCComponent');
            childComp.loadGuidelines(); 
        }else if(component.get("v.oldProcessStep") == 'QC Self Check Final' && component.get("v.newProcessStep") == 'Final QC' && component.get("v.iscompleteQCSelfCheckDraft")){
            component.set("v.popupMessage",'Do you need Final QC');
            component.set("v.popupMessage2",'');
            component.set('v.showDate', false); 
            component.set("v.oldProcessStep","Final QC");
            component.set("v.newProcessStep","");
            component.set("v.iscompleteQCSelfCheckDraft", false);
        }else if(component.get("v.oldProcessStep") == 'Final QC' && component.get("v.newProcessStep") == '' && component.get("v.isFinalQcComplete")){
            component.set('v.isOpen', true);
            component.set("v.popupMessage",'Budget is ready for customer delivery'); 
            component.set('v.showDate', false); 
            component.set("v.newProcessStep","");
        }else if(component.get("v.oldProcessStep") == 'Final QC' && component.get("v.newProcessStep") == '' && !component.get("v.isFinalQcComplete")){
            component.set('v.isOpen', true);
            component.set('v.isMessage', false);
            component.set("v.newProcessStep","");
            component.set("v.qcProcessStep", "Final QC");
            var childComp = component.find('QCComponent');
            childComp.loadGuidelines(); 
        }else if(component.get("v.oldProcessStep") == 'Line Manager QC' && component.get("v.newProcessStep") == '' && !component.get("v.iscompleteQCSelfCheckDraft")){
            component.set('v.isOpen', true);
            component.set('v.isMessage', false);
            component.set("v.newProcessStep","");
            component.set("v.qcProcessStep", "Line Manager QC");
            var childComp = component.find('QCComponent');
            childComp.loadGuidelines(); 
        }
    },
    NoModel: function(component, event, helper) {
        
        if(component.get("v.oldProcessStep") == 'Strategy Call'){
            if(component.find('inputfield') && component.find('inputfield').get('v.value')){
                component.set("v.isOpen", false);
                component.set("v.oldProcessStep", 'Map Call');
                component.set("v.newProcessStep", 'QC Self Check Draft');
                component.showMessage(component, event, helper);
            }
        }else if(component.get("v.oldProcessStep") == 'Map Call'){
            component.set("v.isOpen", false);
            component.set("v.oldProcessStep", 'Map Call');
            component.set("v.newProcessStep", 'QC Self Check Draft');
            component.showMessage(component, event, helper);
        }else if(component.get("v.oldProcessStep") == 'Final QC' && !component.get("v.isFinalQcComplete")){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'QC Self Check Final', '');
        }else if(component.get("v.oldProcessStep") == 'Final QC' && component.get("v.isFinalQcComplete")){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'None', 'Proposal Delivery');
        }else if(component.get("v.oldProcessStep") == 'Line Manager QC'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'Key Stakeholder Review and Challenge Call', 'Internal Review');
        }else if(component.get("v.oldProcessStep") == 'Key Stakeholder Review and Challenge Call'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'QC Self Check Draft', '');
        }else if(component.get("v.oldProcessStep") == 'Challenge Call Complete'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'Challenge Call Complete', '');
        }else if(component.get("v.oldProcessStep") == 'QC Self Check Draft'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'Strategy Call', '');
        }
    },
    YesModel: function(component, event, helper) {
        
        if(component.get("v.oldProcessStep") == 'Strategy Call'){
            if(component.find('inputfield') && component.find('inputfield').get('v.value')){
                component.set("v.isOpen", false);
                helper.updateStrategyDate(component,event);
                component.set("v.newProcessStep","Map Call");
                helper.updateProcessStep(component,event, 'Map Call', '');
            }          
        }else if(component.get("v.oldProcessStep") == 'Map Call'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'Map Call', '');
        }else if(component.get("v.oldProcessStep") == 'Final QC' && !component.get("v.isFinalQcComplete")){
            component.set("v.isOpen", false);
            helper.sendFinalQCRegionMail(component);
            helper.updateProcessStep(component,event, 'Final QC', '');
        }else if(component.get("v.oldProcessStep") == 'Final QC' && component.get("v.isFinalQcComplete")){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'Customer Deliverable Sent', 'Proposal Delivery');
        }else if(component.get("v.oldProcessStep") == 'Line Manager QC'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'Line Manager QC', '');
        }else if(component.get("v.oldProcessStep") == 'Challenge Call Complete'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'TSL Review', '');
        }else if(component.get("v.oldProcessStep") == 'QC Self Check Draft'){
            helper.updateProcessStep(component,event, 'QC Self Check Draft', '');
            component.set("v.newProcessStep","Line Manager QC");
            component.set('v.isMessage', false);
            component.showMessage(component, event, helper);
        }else if(component.get("v.newProcessStep") == 'QC Self Check Draft'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'QC Self Check Draft', '');
        }
        else if(component.get("v.newProcessStep") == 'QC Self Check Final'){
            component.set("v.isOpen", false);
            helper.updateProcessStep(component,event, 'QC Self Check Final', '');
        }
    },
    updateProcessStepForQcCheck: function(component, event, helper) {
        
        helper.updateProcessStep(component,event, component.get("v.oldProcessStep"), '');
    }
})