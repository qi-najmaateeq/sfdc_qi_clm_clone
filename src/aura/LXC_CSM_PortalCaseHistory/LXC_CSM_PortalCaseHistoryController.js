({
    doInit: function(component, event, helper) {
        helper.getCSM_CaseHistory(component);
    },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        console.log(eventParams.changeType);
        if(eventParams.changeType === "LOADED") {
        } else if(eventParams.changeType === "CHANGED") {
            // record is changed
            $A.get('e.force:refreshView').fire();

        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        }
        component.set("v.isLoading", false);
    },
    handleSave: function(component, event, helper) {
        if(component.find("commentBody").get("v.value") != "")
            helper.insertCaseComment(component);
    }
})