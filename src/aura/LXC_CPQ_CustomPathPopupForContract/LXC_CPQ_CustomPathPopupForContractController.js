({
    
    showMessage :  function(component, event, helper) {  

        if(component.get("v.oldProcessStep") == 'None' && component.get("v.newProcessStep") == 'Proposal to Contract Handover Call'){
            component.set('v.isOpen', true); 
            component.set("v.popupMessage",'Key Stakeholder review required');
            component.set("v.oldProcessStep","Proposal to Contract Handover Call");
            component.set("v.newProcessStep","");
        }
        else if(component.get("v.oldProcessStep") == 'Proposal to Contract Handover Call' 
            && component.get("v.newProcessStep") == 'Key Stakeholder Review'){
            component.set('v.isOpen', true); 
            component.set("v.isEmailPopupOpen", false);
            component.set("v.popupMessage",'Proceed with PL Review');
            component.set("v.oldProcessStep","Key Stakeholder Review");
            component.set("v.newProcessStep","");
        }else if((component.get("v.oldProcessStep") == 'Bid Grid (Maintenance) Completion' && component.get("v.newProcessStep") == 'QC Self Check Final') 
            || (component.get("v.oldProcessStep") == 'QC Self Check Final' && component.get("v.newProcessStep") == 'Final QC' && 
            !component.get("v.iscompleteQCSelfCheckDraft"))){
            component.set('v.isOpen', true);
            component.set('v.isMessage', false);
            component.set("v.oldProcessStep","QC Self Check Final");
            component.set("v.newProcessStep","");
            component.set("v.qcProcessStep", "QC Self Check Final");
            var childComp = component.find('QCComponent');
                childComp.loadGuidelines(); 
        }else if(component.get("v.oldProcessStep") == 'QC Self Check Final' && component.get("v.newProcessStep") == 'Final QC' && component.get("v.iscompleteQCSelfCheckDraft")){
            component.set("v.popupMessage",'Do you need Final QC');
            component.set("v.popupMessage2",'');
            component.set('v.showDate', false); 
            component.set("v.oldProcessStep","Final QC");
            component.set("v.newProcessStep","");
            component.set("v.iscompleteQCSelfCheckDraft", false);
        }else if(component.get("v.oldProcessStep") == 'Final QC' && component.get("v.newProcessStep") == '' && !component.get("v.isFinalQcComplete")){
            component.set('v.isOpen', true);
            component.set('v.isMessage', false);
            component.set("v.newProcessStep","");
            component.set("v.qcProcessStep", "Final QC");
            var childComp = component.find('QCComponent');
            childComp.loadGuidelines(); 
        }else if(component.get("v.oldProcessStep") == 'Final QC' && component.get("v.newProcessStep") == '' && component.get("v.isFinalQcComplete")){
            component.set('v.isOpen', false);
        }
    },
    NoModel: function(component, event, helper) {
        
        if(component.get("v.oldProcessStep") == 'Proposal to Contract Handover Call'){
            component.set("v.isOpen", false);
            component.set("v.isEmailPopupOpen", false);
            component.set("v.oldProcessStep", 'Proposal to Contract Handover Call');
            component.set("v.newProcessStep", 'Key Stakeholder Review');
            helper.updateProcessStep(component,event, 'Key Stakeholder Review', 'Internal Review');
            component.showMessage(component, event, helper);
        }
        else if(component.get("v.oldProcessStep") == 'Key Stakeholder Review'){
            component.set("v.isOpen", false);
            component.set("v.isEmailPopupOpen", false);
            component.showMessage(component, event, helper);
            helper.updateProcessStep(component,event, 'None', 'Pending PL Review');
        }else if(component.get("v.oldProcessStep") == 'Final QC' && !component.get("v.isFinalQcComplete")){
            component.set("v.isOpen", false);
            component.set("v.isEmailPopupOpen", false);
            helper.updateProcessStep(component,event, 'QC Self Check Final', '');
        }else if(component.get("v.oldProcessStep") == 'Final QC' && component.get("v.isFinalQcComplete")){
            component.set("v.isOpen", false);
            component.set("v.isEmailPopupOpen", false);
            helper.updateProcessStep(component,event, 'None', 'Proposal Delivery');
        }
    },
    YesModel: function(component, event, helper) {

        if(component.get("v.oldProcessStep") == 'Proposal to Contract Handover Call'){
            component.set("v.isOpen", false);
            component.set("v.isEmailPopupOpen", true);
        }
        else if(component.get("v.oldProcessStep") == 'Key Stakeholder Review') {
            component.set("v.isOpen", false);
            component.set("v.isEmailPopupOpen", false);        
            var parentPopUp = component.get("v.parent");
            if(!component.get("v.projectManagerEmail")) {
                parentPopUp.set("v.showErrorMessage",true);
                parentPopUp.set("v.errorMessage",'Please Provide Project Manager Name');
            } else {
                parentPopUp.set("v.showErrorMessage",false);
            }
            helper.updateProcessStep(component,event, 'PL Review', 'Pending PL Review');
        }else if(component.get("v.oldProcessStep") == 'Final QC' && !component.get("v.isFinalQcComplete")){
            component.set("v.isOpen", false);
            component.set("v.isEmailPopupOpen", false);
            helper.sendFinalQCRegionMail(component);
            helper.updateProcessStep(component,event, 'Final QC', '');
        }
    },
    updateProcessStepForQcCheck: function(component, event, helper) {
        
        helper.updateProcessStep(component,event, component.get("v.oldProcessStep"), '');
    }
})