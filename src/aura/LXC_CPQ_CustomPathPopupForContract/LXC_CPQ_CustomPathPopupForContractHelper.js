({
    updateProcessStep : function(component, event, prcoessStep, agreementStatus) {
        
        component.set("v.showSpinner", true);
        var action  = component.get("c.updateAgreementPrcoessStep");
        action.setParams({ 
            agreementId : component.get("v.recordId"), 
            processStep : prcoessStep,
            agreementStatus : agreementStatus
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
            }else{ 
                var message = '';
                var errors = response.getError();
                if (errors) {
                    for(var i=0; i < errors.length; i++) {
                        if(errors[i].pageErrors) {
                            for(var pageError in errors[i].pageErrors) {
                                message = errors[i].pageErrors[0].message;                                
                            }
                        }                        
                    }
                } else {
                    message += (message.length > 0 ? '\n' : '') + 'Unknown error';
                }
                var parentPopUp = component.get("v.parent");
                parentPopUp.set("v.errorMessage",message);
                parentPopUp.set("v.showErrorMessage",true);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    sendFinalQCRegionMail : function(component) {
        
        component.set("v.showSpinner", true);
        var action  = component.get("c.sendFinalQCMail");
        action.setParams({ 
            agreementId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
        });
        $A.enqueueAction(action);
    }
})