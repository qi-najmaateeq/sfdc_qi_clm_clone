({
	doInit : function(component, event, helper) {
            var action = component.get('c.fetchContract');
            action.setParams({
            agreementId : component.get('v.recordId')
        });        
        action.setCallback(this, function(a){
        var state = a.getState();
            if(state == 'SUCCESS') {
                component.set('v.contract', a.getReturnValue());
            }
        });        
        $A.enqueueAction(action);        
	},        
    handleClick: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.contract").Id
        });
        navEvt.fire();
    }
})