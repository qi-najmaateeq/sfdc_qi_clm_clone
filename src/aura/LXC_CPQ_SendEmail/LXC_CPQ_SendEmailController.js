({
    doInit: function(component, event, helper) {
        component.set("v.showSpinner", true); 
        var action = component.get("c.getEmailTemplate");
        action.setParams({
            'recordId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = [];
                storeResponse = response.getReturnValue();
                component.set("v.subject", storeResponse[0]);
                component.set("v.body", storeResponse[1]);
                component.set("v.showSpinner", false); 
            }
        });
        $A.enqueueAction(action);
    },
    sendMail: function(component, event, helper) {
        var agreementID= component.get("v.recordId");
        var getEmail = component.get("v.email");
        var getCcEmail = component.get("v.Ccemail");
        var getBccEmail = component.get("v.Bccemail");
        var getSubject = component.get("v.subject");
        var getbody = component.get("v.body");
        var getAgreementStatus = component.get("v.agreementStatus");
        var getProcessStep = component.get("v.processStep");
        component.set("v.showSpinner", true); 
        var getAttachmentIds = component.get("v.AttachmentIds");   
        if ($A.util.isEmpty(getEmail) || !getEmail.includes("@")) {
            component.set("v.showSpinner", false);
            var inputCmp = component.find("emailAddress");
            inputCmp.set("v.errors", [{message:"Enter valid email address"}]);
        } else {
            helper.sendHelper(component,agreementID, getEmail, getCcEmail, getBccEmail, getSubject, getbody, getAttachmentIds);
            if(getAgreementStatus!="" && getProcessStep!="") {
                helper.updateProcessStep(component,event, getProcessStep, getAgreementStatus);
            }
        }
    }, 
    closeMessage: function(component, event, helper) {
        component.set("v.mailStatus", false);
        component.set("v.email", null);
        component.set("v.subject", null);
        component.set("v.body", null);
        component.set("v.hidePopUp", false);
        component.set("v.showSpinner", false);
    },
    ClosePopUp: function(component, event, helper) {
        var getAgreementStatusClose = component.get("v.agreementStatusClose");
        var getProcessStepClose = component.get("v.processStepClose");
        if(getAgreementStatusClose!="" && getProcessStepClose!=""){
            helper.updateProcessStep(component,event, getProcessStepClose, getAgreementStatusClose);
        }
        else{
            component.set("v.hidePopUp", false);
        }
    },
    handleUploadFinished: function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        var attachmentList = component.get("v.attachmentList");
        attachmentList.push({value : documentId, label : fileName});
        component.set("v.attachmentList", attachmentList);        
        var attachmentId = component.get("v.AttachmentIds");        
        var attachmentName = component.get("v.AttachmentIds");
        attachmentId.push(documentId);
        attachmentName.push(fileName);
        component.set("v.AttachmentIds", attachmentId);
        component.set("v.AttachmentNames", attachmentName)
        component.set("v.showFiles", true);
    },
    deleteFile: function(component, event, helper) {
        var attachmentList = component.get("v.attachmentList");
        var index = event.target.dataset.index;
        attachmentList.splice(index, 1);
        component.set("v.attachmentList", attachmentList);
        var action = component.get("c.deleteAttachments");
        action.setParams({
            'attachmentIds': event.target.id
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            }
        });
        $A.enqueueAction(action);
    }
})