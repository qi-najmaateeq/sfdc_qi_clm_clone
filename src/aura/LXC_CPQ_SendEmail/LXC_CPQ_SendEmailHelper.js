({
    sendHelper: function(component,agreementID, getEmail, getCcEmail, getBccEmail, getSubject, getbody, getAttachmentIds) {
        
        var action = component.get("c.sendMailMethod");
        action.setParams({
            'agreementID': agreementID,
            'mEmail': getEmail,
            'ccEmail': getCcEmail,
            'bccEmail' : getBccEmail,
            'mSubject': getSubject,
            'mbody': getbody,
            'attachmentIds' : getAttachmentIds
        });
        action.setCallback(this, function(response) {
        var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.mailStatus", true);
            }else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.showSpinner", false); 
            }
        });
        $A.enqueueAction(action);
    },
    updateProcessStep : function(component, event, prcoessStep, agreementStatus) {
        
        component.set("v.showSpinner", true);
        var action  = component.get("c.updateAgreementPrcoessStep");
        action.setParams({ 
            agreementId : component.get("v.recordId"), 
            processStep : prcoessStep,
            agreementStatus : agreementStatus
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.hidePopUp", false);
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
})