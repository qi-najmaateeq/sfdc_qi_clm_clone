({
	setProductRecordToCurrentLayer: function(component, event, helper, currentLayer, productRecords , productMap) {
       if(currentLayer == '2') {
           component.set("v.LayerTwo", productRecords);
           component.set("v.LayerThree", []);
           component.set("v.LayerFour", []);
           component.set("v.LayerFive", []);
           component.set("v.LayerSix", []);
           component.set("v.LayerSeven", []);
           component.set("v.LayerEight", []);
       }
       else if(currentLayer == '3') {
           component.set("v.LayerThree", productRecords);
           component.set("v.LayerFour", []);
           component.set("v.LayerFive", []);
           component.set("v.LayerSix", []);
           component.set("v.LayerSeven", []);
           component.set("v.LayerEight", []);
       }
       else if(currentLayer == '4') {
           component.set("v.LayerFour", productRecords);
           component.set("v.LayerFive", []);
           component.set("v.LayerSix", []);
           component.set("v.LayerSeven", []);
           component.set("v.LayerEight", []);
       }
       else if(currentLayer == '5') {
           component.set("v.LayerFive", productRecords);
           component.set("v.LayerSix", []);
           component.set("v.LayerSeven", []);
           component.set("v.LayerEight", []);
       }
       else if(currentLayer == '6') {
           component.set("v.LayerSix", productRecords);
           component.set("v.LayerSeven", []);
           component.set("v.LayerEight", []);
       }
       else if(currentLayer == '7') {
           component.set("v.LayerSeven", productRecords);
           component.set("v.LayerEight", []);
       }
       else if(currentLayer == '8') {
           component.set("v.LayerEight", productRecords);
       }
       $A.get("e.c:LXE_CRM_SpinnerEvent").setParams({"action" : "stop"}).fire();
    }
})