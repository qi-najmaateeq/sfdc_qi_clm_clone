({
    disableOtherTabs : function(component, event, helper) {
        var searchComp = component.find("searchProducts");
        var favouriteComp = component.find("favourite"); 
        var viewOliComp = component.find("viewOli");
        $A.util.addClass(searchComp, "disabledTab");
        $A.util.addClass(favouriteComp, "disabledTab");
        viewOliComp.set("v.class", "disabledTab");
    },
    
    hideResolveTab : function(component, event, helper) {
        var resolveProductsComp = component.find("resolveProducts");
        var searchComp = component.find("searchProducts");
        var favouriteComp = component.find("favourite"); 
        var viewOliComp = component.find("viewOli");
        if(searchComp != undefined) {
            $A.util.removeClass(searchComp, "disabledTab");
        }
        if(favouriteComp != undefined) {
            $A.util.removeClass(favouriteComp, "disabledTab");
        }
        if(viewOliComp != undefined) {
            viewOliComp.set("v.class", ""); 
        }
        if(resolveProductsComp != undefined) {
            resolveProductsComp.set("v.class", "slds-hide");
        }
        component.set("v.activeTabId", "viewOli");
    },
    
    changeLineItem : function(component, event, helper, oliId) {
        var fieldList = ["Name","UnitPrice","Delivery_Country__c","Billing_System__c","Revenue_Type__c","Sale_Type__c","Product_Start_Date__c","Product_End_Date__c","Description","Id",
                         "Therapy_Area__c", "Hierarchy_Level__c", "Product_SalesLead__c", "SalesEngineer__c", "Id", "OpportunityId", "CurrencyISOCode", "Product2.Name", "PricebookEntryId", 
                         "Product2.Offering_Segment__c", "Product2.Territory__c", "Product2.Offering_Group__c", "Product2.Offering_Type__c", "Product2.COE_Name__c", 
                         "Product2.Product_Group__c", "Product2.Unit_Name__c", "Product2.Hierarchy_Level__c", "Product2.CanUseRevenueSchedule"];
        var action = component.get("c.getOpportunityLineItemsDetail");
        var oliId = component.get("v.oliId");
        var oliIdList = [oliId];
        action.setParams({ 
            oliIdList : oliIdList, 
            oliFieldList : fieldList
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resolveProductsComp = component.find("resolveProducts");
                if(resolveProductsComp != undefined) {
                    resolveProductsComp.set("v.class", "");
                    var resolveLabel = resolveProductsComp.get("v.label");
                    resolveLabel[0].set("v.value", "Change Products");
                }
               
                var searchComp = component.find("searchProducts");
                var favouriteComp = component.find("favourite"); 
                var viewOliComp = component.find("viewOli");
                if(favouriteComp != undefined) {
                    $A.util.addClass(favouriteComp, "disabledTab");
                }
                if(viewOliComp != undefined) {
                    $A.util.addClass(viewOliComp, "disabledTab");
                }
                if(searchComp != undefined) {
                    searchComp.set("v.class", "disabledTab");
                }
                component.set("v.activeTabId", "resolveProducts");
                var oliList = response.getReturnValue();
                if(oliList.length > 0) {
                    var resolveProductEvent = $A.get("e.c:LXE_CRM_ResolveProductEvent");
                    resolveProductEvent.setParams({
                        "resolveLineItem" :  oliList[0],
                        "action" : "search",
                        "actionType" : "change",
                        "screen" : "ProductDetail"
                    });
                    resolveProductEvent.fire();
                }
            }else {
                var errors = response.getError();
                if (errors[0] && errors[0].message) {
                    var err = JSON.parse(errors[0].message).errorList;
                    console.log(err);
                }
            }
        });
        $A.enqueueAction(action);
    }
})