({
    saveProposalQASelfCheckListRecords : function(component, isSaveAndComplete) {
        component.set("v.Spinner", true);
        var action = component.get("c.saveProposalQASelfCheckList");
        action.setParams({ 
            "proposalQASelfCheckListWrapperListString" : JSON.stringify(component.get("v.proposalQASelfCheckListWrapperList")),
            "agreementId" : component.get("v.recordId"),
            "processStep" : component.get("v.processStep"),
            "isSaveAndComplete" : isSaveAndComplete,
            "overAllComments" : component.get("v.overAllComments")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    }, 
})