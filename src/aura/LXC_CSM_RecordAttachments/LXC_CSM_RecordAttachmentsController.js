({
    init: function (component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var recordId;
        if (component.get('v.recordId')) {
            recordId = component.get('v.recordId');
        } else {
            recordId = myPageRef && myPageRef.state ? myPageRef.state.c__recordId : undefined;
        }
        if (recordId) {
            component.set("v.recordId", recordId);
            component.find("record").reloadRecord();
            helper.getAttachments(component);
            helper.getPublishStatus(component);
            helper.getPermissionSets(component);
            helper.getCurrentUser(component);
        }


    },

    openSingleFile: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var ContentDocumentId = selectedItem.dataset.value;
        $A.get('e.lightning:openFiles').fire({
            recordIds: [ContentDocumentId]
        });
    },

    sortBy: function (component, event, helper) {
        var files = component.get("v.files");
        var val = event.currentTarget.dataset.val;
        if (event.currentTarget.dataset.sort == 'desc') {
            files.sort(function (a, b) { return (a[val].toLowerCase() < b[val].toLowerCase()) ? 1 : ((b[val].toLowerCase() < a[val].toLowerCase()) ? -1 : 0); });
            event.currentTarget.dataset.sort = "asc"
        } else {
            files.sort(function (a, b) { return (a[val].toLowerCase() > b[val].toLowerCase()) ? 1 : ((b[val].toLowerCase() > a[val].toLowerCase()) ? -1 : 0); });
            event.currentTarget.dataset.sort = "desc"
        }
        component.set("v.files", files);
    },

    deleteFile: function (component, event, helper) {
        var r = confirm("Are you sure you want to delete this file ?");
        var selectedItem = event.currentTarget;
        var childRecordId = selectedItem.dataset.value;
        if (r == true) {
            helper.deleteContentDocumentById(component, childRecordId);
        }
    },

    openSubtab: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.value;
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openSubtab({
            url: '/lightning/r/' + recordId + '/view',
            focus: true
        }).then(function (subtabId) {
        })
            .catch(function (error) {
                console.log(error);
            });
    },

    openRecord: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.value;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    },

    updateVisibility: function (component, event, helper) {
        var selectedItem = event.currentTarget;
        var documentId = selectedItem.dataset.value;
        var visibility = selectedItem.dataset.visibility;
        helper.updateContentDocumentLinkVisibility(component, documentId, visibility);
    }
})

