({
    getAttachments: function (component) {
        component.set("v.isLoading", true);
        var startLetter = component.get("v.recordId").substring(0, 1);
        if (startLetter == 'k') {
            component.set("v.knowledge", true);
        }
        else {
            component.set("v.knowledge", false);
        }
        var action = component.get("c.getAttachments");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var files = response.getReturnValue();
                component.set("v.files", files);
            }
            else if (state === "ERROR") {
                console.log("LXC_CSM_PortalFile] ERROR " + JSON.stringify(response.getError()));
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    getPermissionSets: function (component) {
        var action = component.get("c.getPermissionSets");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var files = response.getReturnValue();
                component.set("v.permission", files);
            }
            else if (state === "ERROR") {
                console.log("ERROR " + JSON.stringify(response.getError()));
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    getPublishStatus: function (component) {
        var action = component.get("c.getPublishStatus");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.publishStatus", response.getReturnValue());
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },


    getCurrentUser: function (component) {
        var action = component.get("c.currentUser");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var files = response.getReturnValue();
                component.set("v.user", files);
            }
            else if (state === "ERROR") {
                console.log("ERROR " + JSON.stringify(response.getError()));
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    deleteContentDocumentById: function (component, contentDocumentId) {
        var action = component.get("c.deleteContentDocumentById");
        action.setParams({
            "contentDocumentId": contentDocumentId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "SUCCESS",
                    "message": "File was deleted."
                });
                resultsToast.fire();
                this.getAttachments(component);
            } else if (state === "ERROR") {
                if (response.getError()[0].pageErrors[0].statusCode === "INSUFFICIENT_ACCESS_OR_READONLY") {
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "ERROR",
                        "title": " Action Denied",
                        "message": "You are not allowed to delete this file"
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                } else {
                    console.log("ERROR " + JSON.stringify(response.getError()));
                }
            }
        });
        $A.enqueueAction(action);
    },

    updateContentDocumentLinkVisibility: function (component, contentDocumentId, visibility) {
        var action = component.get("c.updateContentDocumentLinkVisibility");
        action.setParams({
            "contentDocumentId": contentDocumentId,
            "linkedEntityId": component.get("v.recordId"),
            "visibility": visibility
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "Success",
                    "message": "The visibility of the attachment has been updated."
                });
                resultsToast.fire();
                $A.get('e.force:refreshView').fire();
            } else if (state === "ERROR") {
                console.log("ERROR: " + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
})

