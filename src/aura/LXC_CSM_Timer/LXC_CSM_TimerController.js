({
	doInit : function(component, event, helper) {
        component.set("v.cssStyle", "<style>.cuf-scroller-outside {background: rgb(255, 255, 255) !important;}</style>");
        var recordId = component.get("v.recordId");
        if(recordId != undefined){
            helper.callServer(component, "c.getTimeSheet",function(response){
                if(response != undefined){
                    component.set('v.timeSheet',response);
                    
                        helper.onClickHelp(component, event, helper);    
                    
                } 
            },{"caseId" : recordId },true);
        }
        
    },
    doScriptLoad : function(component, event, helper) {

	},
    onClick : function(component, event, helper) {
        helper.onClickHelp(component, event, helper);
	},
    save : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        //if(recordId != undefined && component.get('v.timeSheet') == undefined){
            helper.callServer(component, "c.saveTimeSheet",function(response){
                component.set("v.timeinHours","");
                component.set("v.comment","");
            },{"caseId" : recordId, "timeInHours" : component.get("v.timeinHours") , "comment" : component.get("v.comment")},true);
        //}
	}
    
    
})