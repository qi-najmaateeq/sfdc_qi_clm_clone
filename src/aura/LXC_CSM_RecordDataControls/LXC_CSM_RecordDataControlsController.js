({
    doInit: function(component, event, helper) {
    },

    recordUpdate: function(component, event, helper) {
        if(component.get("v.simpleRecord").AccountId != undefined){
            component.set("v.accountRecordId", component.get("v.simpleRecord").AccountId)
            component.find("accountRecord").reloadRecord();
        }else{
            console.log("No AccountId on this record");
        }

    },

    accountRecordUpdate: function(component, event, helper) {
        if(component.get("v.simpleAccountRecord").MDM_Validation_Status__c != "Validated"){
            component.set("v.warning",true);
        }
    }
})