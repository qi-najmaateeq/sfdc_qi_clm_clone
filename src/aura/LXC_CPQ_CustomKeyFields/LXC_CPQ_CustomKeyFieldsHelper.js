({
    checkIfUserInQCGroup : function(component) {
        var action  = component.get("c.checkIfCurrentUserInQCGroup");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.showSpinner",false);
                component.set("v.isShowQcButton",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    checkIfUserIsLineManager : function(component) {
        var action  = component.get("c.checkIfCurrentUserIsLineManager");
        action.setParams({ 
            agreementId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.showSpinner",false);
                component.set("v.isShowQcButton",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})