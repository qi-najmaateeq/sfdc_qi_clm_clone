({
    doInit :  function(component, event, helper) {
        
        var childComp = component.find('popupComponent');
        childComp.set("v.showSpinner", false);
        component.set('v.oldProcessStep', component.get("v.simpleRecord").Process_Step__c);  
        component.set("v.showSpinner", false);
        component.set("v.isShowQcButton",false);
        if(component.get("v.simpleRecord").Process_Step__c == 'Line Manager QC' && component.get("v.simpleRecord").OwnerId){
            component.set("v.showSpinner", true);
            helper.checkIfUserIsLineManager(component);
        }else if(component.get("v.simpleRecord").Process_Step__c == 'Final QC'){
            component.set("v.showSpinner", true);
            helper.checkIfUserInQCGroup(component);
        }
    },
    onsubmit : function(component, event, helper) {
        
        component.set("v.errorMessage","");
        component.set('v.newProcessStep', event.getParam("fields").Process_Step__c);
        component.set('v.showModal', true); 
        var childComp = component.find('popupComponent');
        childComp.set("v.showSpinner", true);
        childComp.showMessage();    
    },
    onerror : function(component, event, helper) {
        
        var childComp = component.find('popupComponent');
        childComp.set("v.showSpinner", false);
    },
    showQCSelfCheckTable: function (component, event, helper) {

        component.set('v.oldProcessStep', component.get("v.simpleRecord").Process_Step__c);  
        component.set('v.newProcessStep', '');
        var childComp = component.find('popupComponent');
        childComp.showMessage();    
    },
})