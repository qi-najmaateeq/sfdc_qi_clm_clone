({
    getStageFieldValues : function(component, event, helper) {
        var action = component.get("c.getSobjectFieldDetail");
        var fieldList = ["StageName"];
        action.setParams({ 
            objectName : "Opportunity",
            fieldList : fieldList
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var objectFieldsWrapperList = response.getReturnValue();
                if(objectFieldsWrapperList.length > 0) {
                    var pickListFieldsMap = {};
                    for(var count in objectFieldsWrapperList) {
                        var fieldWrapper = objectFieldsWrapperList[count];
                        if(fieldWrapper.fieldDataType =="PICKLIST") {
                            if(fieldWrapper.fieldApiName == "stagename") {
                                var fieldValues = fieldWrapper.fieldValues;
                                var stageValues = [];
                                for(var stageValue in fieldValues) {
                                    if(fieldValues[stageValue].includes("--None--") || fieldValues[stageValue].includes("5") || fieldValues[stageValue].includes("6") || fieldValues[stageValue].includes("7") || fieldValues[stageValue].includes("In-Hand")) {
                                        
                                    } else {
                                        stageValues.push(fieldValues[stageValue]);
                                    }
                                }
                                pickListFieldsMap[fieldWrapper.fieldApiName] = stageValues;
                            } else {
                                pickListFieldsMap[fieldWrapper.fieldApiName] = fieldWrapper.fieldValues;
                            }
                        }
                    }
                    component.set("v.pickListFieldsMap", pickListFieldsMap);
                    helper.changeStageValue(component, event, helper);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var errorMsgList = JSON.parse(errors[0].message).errorList;
                        console.log(errorMsgList);
                        component.set("v.errors", errorMsgList);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    changeStageValue : function(component, event, helper) {
        var clonedOpp = component.get("v.clonedOpp");
        component.set("v.OpportunityName", clonedOpp["Name"]);
        var stageName = clonedOpp["StageName"];
        var pickListFieldsMap = component.get("v.pickListFieldsMap");
        var isStageFound = false;
        for(var stageValue in pickListFieldsMap["stagename"]) {
            if(stageName === pickListFieldsMap["stagename"][stageValue]) {
                isStageFound = true;
                break;
            }
        }
        if(!isStageFound) {
            clonedOpp["StageName"] =  pickListFieldsMap["stagename"][0];
            component.set("v.clonedOpp", clonedOpp);
        }
    },
    
    doCloneOpportunity : function(component, event, helper) {
        var isAllCorrect = helper.validateFields(component, event, helper);
        if(!isAllCorrect) {
            return;
        }
        var clonedOpp = component.get("v.clonedOpp");
        var OpportunityId = component.get("v.OpportunityId");
        var selectedContactRoleOptions =  component.get("v.selectedContactRoleOptions");
        var RevenueDateShift =  component.get("v.RevenueDateShift");
        var PriceChangePercent =  component.get("v.PriceChangePercent");
        var selectedCloneOption =  component.get("v.selectedCloneOption");
        var isCloneProducts = false;
        if(selectedCloneOption == "Clone with Products") {
            isCloneProducts = true;
        }
        var isCloneContactRoles = false;
        if(selectedContactRoleOptions == "Clone with Contact Roles") {
            isCloneContactRoles = true;
        }
        var isRenewalOptions =  component.get("v.isRenewalOptions");
        var action = component.get("c.cloneOpportunityProducts");
        var mapTofieldValue = {};
        mapTofieldValue["CloseDate"] = clonedOpp["CloseDate"];
        mapTofieldValue["Name"] = clonedOpp["Name"];
        mapTofieldValue["StageName"] = clonedOpp["StageName"];
        var objectTypeToIsCloneMap = {};
        objectTypeToIsCloneMap["Product2"] = isCloneProducts;
        objectTypeToIsCloneMap["OpportunityContactRole"] = isCloneContactRoles;
        objectTypeToIsCloneMap["RenewalOptions"] = isRenewalOptions;
        console.log(objectTypeToIsCloneMap);
        action.setParams({
            "opportunityId" : OpportunityId,
            "mapTofieldValue" : mapTofieldValue,
            "pricePercentAdjustment" : PriceChangePercent,
            "monthMoved" : RevenueDateShift,
            "objectTypeToIsCloneMap" : objectTypeToIsCloneMap
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if(state === "SUCCESS") {
            	$A.get("e.force:closeQuickAction").fire();
                var createdOppId = actionResult.getReturnValue();
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": createdOppId,
                    "isredirect" : false
                });
                navEvt.fire();
            }
            else if(state === "ERROR") {
                var errors = actionResult.getError();
                console.log(errors);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var errorMsgList = JSON.parse(errors[0].message).errorList;
                        component.set("v.errors", errorMsgList);
                    }
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    
    validateFields : function(component, event, helper) {
        var isAllCorrect = true;
        var oppNameComp = component.find("oppName");
        var oppCloseDateComp = component.find("oppCloseDate");
        var pricechangepercentComp = component.find("pricechangepercent");
        var isRenewalOptions = component.get("v.isRenewalOptions");
        var errorList = [];
        component.set("v.errors", null);
        var selectedCloneOption =  component.get("v.selectedCloneOption");
        var clonedOpp = component.get("v.clonedOpp");
        if(selectedCloneOption == "Clone without Products" && clonedOpp["StageName"] == "5. Finalizing Deal") {
            $A.util.addClass(oppNameComp, "slds-has-error");
            $A.util.removeClass(oppNameComp, "hide-error-message");
            isAllCorrect = false;
            errorList.push("You cannot create an Opportunity at the Stage Finalizing Deal, Received ATP/LOI, Closed Won or Closed Lost. Please review and revise.");
        }
        if(oppNameComp.get("v.value") == undefined || oppNameComp.get("v.value") == null || oppNameComp.get("v.value") == '' ) {
            $A.util.addClass(oppNameComp, "slds-has-error");
            $A.util.removeClass(oppNameComp, "hide-error-message");
            isAllCorrect = false;
            errorList.push("Please Enter Opportunity Name");
        } else {
            $A.util.removeClass(oppNameComp, "slds-has-error");
            $A.util.addClass(oppNameComp, "hide-error-message");
        }
        if(oppCloseDateComp.get("v.value") == undefined || oppCloseDateComp.get("v.value") == null || oppCloseDateComp.get("v.value") == "") {
            $A.util.addClass(oppCloseDateComp, "sldsRedError");
            $A.util.removeClass(oppCloseDateComp, "hide-error-message");
            isAllCorrect = false;
            errorList.push("Please Enter Opportunity Close Date");
        } else {
            $A.util.removeClass(oppCloseDateComp, "sldsRedError");
            $A.util.addClass(oppCloseDateComp, "hide-error-message");
        }
        if(isRenewalOptions) {
            if(pricechangepercentComp.get("v.value") == undefined || pricechangepercentComp.get("v.value") == null 
               || pricechangepercentComp.get("v.value") < -999 || pricechangepercentComp.get("v.value") > 999) {
                $A.util.addClass(pricechangepercentComp, "slds-has-error");
                $A.util.removeClass(pricechangepercentComp, "hide-error-message");
                isAllCorrect = false;
                errorList.push("Please Enter price change percent between -999 to 999");
            } else {
                $A.util.removeClass(pricechangepercentComp, "slds-has-error");
                $A.util.addClass(pricechangepercentComp, "hide-error-message");
            }
        }
        if(errorList.length > 0) {
            component.set("v.errors", errorList);
        }
        return isAllCorrect;
    },
})