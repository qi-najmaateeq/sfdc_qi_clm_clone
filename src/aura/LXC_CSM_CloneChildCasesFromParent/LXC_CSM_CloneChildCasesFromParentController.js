({
    doInit : function(component, event, helper) {
        component.set('v.caseId',component.get('v.pageReference').state.caseId);
        var workspaceAPI = component.find("workspace");
        if(component.get('v.pageReference').state.caseId != undefined ){
            var ws = component.get('v.pageReference').state.ws;
            component.set("v.caseSearchDisable",true);
            component.set("v.buttonSDisable",false);
            component.set("v.data",null);
            if(ws == undefined){
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    workspaceAPI.closeTab({tabId: response.tabId});
                })
                .catch(function(error) {
                    console.log(error);
                });
                
                workspaceAPI.openTab({
                    url: '/lightning/r/Case/'+component.get('v.pageReference').state.caseId+'/view',
                    focus: false
                }).then(function(response) {
                    workspaceAPI.openSubtab({
                        parentTabId: response,
                        url: '/lightning/r/'+component.get('v.pageReference').state.caseId+'/related/Cases/view',
                        focus: false
                    });
                    workspaceAPI.openSubtab({
                        parentTabId: response,
                        url: '/lightning/n/New_Bulk_RandD_Child_Cases?caseId='+component.get('v.pageReference').state.caseId,
                        focus: true
                    });
                })
                .catch(function(error) {
                    console.log(error);
                });
            }else if(component.get('v.pageReference').state.caseId != undefined){
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.isSubtab({
                        tabId: focusedTabId
                    }).then(function(response) {
                        if (response) {
                            workspaceAPI.setTabLabel({
                                tabId: focusedTabId,
                                label: "RandD Bulk Child Case Create"
                            });
                        }
                    });
                    workspaceAPI.setTabIcon({
                        tabId: focusedTabId,
                        icon: "standard:case",
                        iconAlt: "settings"
                    });
                })
                .catch(function(error) {
                    console.log(error);
                });
                helper.callServer(component, "c.searchParentCase",function(response){
                    component.set("v.parentCase", response);
                    component.set("v.sltContactId", response.ContactId);
                    component.set("v.sltContactName", response.Contact.FirstName + ' ' + response.Contact.LastName);
                    component.set("v.sltSSCId", response.Site_Related_to_the_Study__c);
                    component.set("v.sltSSCName", response.Site_Related_to_the_Study__r.Name);
                    component.set("v.sltLos", response.LOS__c);
                    component.set("v.sltST1", response.SubType1__c);
                    component.set("v.sltST2", response.SubType2__c);
                    component.set("v.sltST3", response.SubType3__c);
                    component.set("v.sltTemplate", response.Template__c);
                    helper.getControllingField(component);
                    if(response.RecordTypeName__c == 'ActivityPlan'){
                        this.fetchPicklistValues(component, 'LOS__c', 'Template__c');
                        var  columnLst = component.get('v.columns');
                        columnLst.splice(4 , 0, { label: "Template", fieldName: "template", type: "text"});
                        component.set('v.columns', columnLst);
                    }
                    
                },{ caseId : component.get('v.pageReference').state.caseId},true);
            }    
        }else{
            component.set("v.caseSearchDisable",false);
            workspaceAPI.getFocusedTabInfo().then(function(response) {
                var focusedTabId = response.tabId;
                workspaceAPI.setTabLabel({
                    tabId: focusedTabId,
                    label: "RandD Bulk Child Case Create"
                });
            })
            .catch(function(error) {
                console.log(error);
            });
        }
        var columnLst = [];
        columnLst.push({ label: "Row No", fieldName: "Id", type: "number"});
        columnLst.push({ label: "Contact Name", fieldName: "contactName", type: "text"});
        columnLst.push({ label: "Site Name", fieldName: "siteName", type: "text"});
        columnLst.push({ label: "LOS", fieldName: "los", type: "text"});
        columnLst.push({ label: "Sub-Type1", fieldName: "subType1", type: "text"});
        columnLst.push({ label: "Sub-Type2", fieldName: "subType2", type: "text"});
        columnLst.push({ label: "Sub-Type3", fieldName: "subType3", type: "text"});
        columnLst.push({ type: "action", typeAttributes: { rowActions: [{ label: "Delete", name: "delete" }]}});
        component.set('v.columns', columnLst);
        
        
    },
    
	searchForm : function(component, event, helper) {
        helper.callServer(component, "c.searchParentCase",function(response){
            component.set("v.parentCase", response);
            component.set("v.sltContactId", response.ContactId);
            component.set("v.sltContactName", response.Contact.FirstName + ' ' + response.Contact.LastName);
            component.set("v.sltSSCId", response.Site_Related_to_the_Study__c);
            component.set("v.sltSSCName", response.Site_Related_to_the_Study__r.Name);
            component.set("v.sltLos", response.LOS__c);
            component.set("v.sltST1", response.SubType1__c);
            component.set("v.sltST2", response.SubType2__c);
            component.set("v.sltST3", response.SubType3__c);
            component.set("v.sltTemplate", response.Template__c);
            },{ caseId : component.get("v.caseId")},true);
    },
    resetForm : function(component, event, helper){
        
    },
    addtoDataTable : function(component, event, helper){
        var count = component.get("v.sltChildNo");
        var array = [];
        
        if(component.get("v.caseId") == undefined){
            helper.showToastmsg(component,"Warning","warning",'Please Select the Parent Case');
            return ;
        }else if(component.get("v.sltContactId") == undefined){
            helper.showToastmsg(component,"Warning","warning",'Please Select the Contact Name or StudySiteContact Name');
            return ;
        }
        if(component.get("v.data") != null){
          array = component.get("v.data");
        }
        if(array.length == 0){
        var i=1;    
        }else if(array.length > 0){
          var i = parseInt(array.length)+1;
            count = parseInt(count) + parseInt(array.length);
        }
        component.set("v.dataTblCount",count);
        for(; i<= count; i++){
            var myCon = new Object();
            myCon.contactName = component.get("v.sltContactName");
            myCon.contactId = component.get("v.sltContactId");
            myCon.caseNumber = '';
            myCon.siteName = component.get("v.sltSSCName");
            myCon.sscId = component.get("v.sltSSCId");
            myCon.los = component.get("v.sltLos") != 'Please Specify' ?component.get("v.sltLos") : '';
            myCon.template = component.get("v.sltTemplate") != 'Please Specify' ?component.get("v.sltTemplate") : '';
            myCon.subType1 = component.get("v.sltST1") != 'Please Specify' ?component.get("v.sltST1") : '';
            myCon.subType2 = component.get("v.sltST2") != 'Please Specify' ?component.get("v.sltST2") : '';
            myCon.subType3 = component.get("v.sltST3") != 'Please Specify' ?component.get("v.sltST3") : '';
            myCon.Id = i;
            array.push(myCon);
            
        }
        component.set("v.arrayStr", array);
        component.set("v.data", array);
        
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'show_details':
                break;
            case 'delete':
                var id = row.Id;
                var list = cmp.get("v.data");
                var index = list.map(x => {
                    return x.Id;
                }).indexOf(id);
                
                list.splice(index, 1);
                for (var i = 0; i < list.length; i++){
                   list[i].Id = i+1; 
                }
                cmp.set("v.data",list);
                break;
        }
    },
    saveChildCases : function(component, event, helper) {
        $A.createComponent("c:LXC_CSM_OverlayLibraryModal", {},function(content, status) {
                               if (status === "SUCCESS") {
                                   var modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "You are going to create '"+ component.get("v.data").length + "' Child Case, please confirm to proceed.",
                                       body: modalBody, 
                                       showCloseButton: false,
                                       closeCallback: function(ovl) {
                                           console.log('Overlay is closing');
                                       }
                                   }).then(function(overlay){
                                       console.log("Overlay is made");
                                   });
                               }
                           });
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        //component.set("v.spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
       // component.set("v.spinner", false);
    },
    
    onLOSChange: function(component, event, helper) {
        var record = component.get("v.parentCase");
        if (record != undefined){
            helper.getSubtype(component,1);
            if(record.RecordTypeName__c == 'ActivityPlan'){
                helper.getTemplate(component);
            }    
        }
    },

    onSubType1Change: function(component, event, helper) {
        helper.getSubtype(component,2);
    },
    onSubType2Change: function(component, event, helper) {
        helper.getSubtype(component,3);
    },

    handleApplicationEvent : function(component, event, helper) {
        var message = event.getParam("message");
        if(message == 'Ok')
        {
            component.set("v.spinner", true);
            helper.callServer(component, "c.insertChildCases",function(response){
                var columnLst = [];
                columnLst.push({ label: "Row No", fieldName: "Id", type: "number"});
                columnLst.push({ label: "Case Number", fieldName: "caseNumber", type: "text"});
                columnLst.push({ label: "Contact Name", fieldName: "contactName", type: "text"});
                columnLst.push({ label: "Site Name", fieldName: "siteName", type: "text"});
                columnLst.push({ label: "LOS", fieldName: "los", type: "text"});
                if(component.get("v.parentCase").RecordTypeName__c == 'ActivityPlan'){
                    columnLst.push({ label: "Template", fieldName: "template", type: "text"});
                }
                columnLst.push({ label: "Sub-Type1", fieldName: "subType1", type: "text"});
                columnLst.push({ label: "Sub-Type2", fieldName: "subType2", type: "text"});
                columnLst.push({ label: "Sub-Type3", fieldName: "subType3", type: "text"});
                component.set('v.columns', columnLst);
                component.set("v.data",response);
                component.set("v.buttonSDisable",true);
                helper.showToastmsg(component,"success","success", response.length + ' Child cases saved Successfully.');
                component.set("v.spinner", false);
            },{ caseId : component.get("v.caseId"), childData : JSON.stringify(component.get("v.data"))},true);
        }
        else if(message == 'Cancel')
        {
            // if the user clicked the Cancel button do your further Action here for canceling
        }
    }
    
})
