({
	doInit : function(component, event, helper) {
		var action = component.get('c.fetchBidHistory'); 
		action.setParams({
			agreementId : component.get('v.recordId')
        });
        action.setCallback(this, function(a){
            var state = a.getState(); 
            if(state == 'SUCCESS') {
                console.log(a.getReturnValue());
                component.set('v.bidHistory', a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    handleClick: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.bidHistory").Id
        });
        navEvt.fire();
    }
})