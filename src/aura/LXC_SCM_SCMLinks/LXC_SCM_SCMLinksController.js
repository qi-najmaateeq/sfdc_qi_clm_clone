({
	navigateToATCChart : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            "url": "/apex/ATC_Chart"
        });
        navEvt.fire();
    },
    
    navigateToAgreementLocator : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            "url": "/apex/AgreementLocator"
        });
        navEvt.fire();
    },
    
    navigateToContractChart : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            "url": "/apex/ContractChart"
        });
        navEvt.fire();
    }
})