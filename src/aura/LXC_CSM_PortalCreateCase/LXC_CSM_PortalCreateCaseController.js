({
    doInit: function (component, event, helper) {
        component.find("caseRecordCreator").getNewRecord(
            "Case",
            null,
            false,
            $A.getCallback(function () {
                var rec = component.get("v.newCase");
                var error = component.get("v.newCaseError");
                if (error || (rec === null)) return;
                helper.getUserAccount(component);
                helper.getUserContact(component);
                /** helper.getPriority(component);*/
                helper.getUrgency(component);
                helper.getImpact(component);
            })
        );
    },
    prioritychange: function (component, event, helper) {
        helper.getPriority(component);
    },

    portalCaseTypeChange: function (component, event, helper) {
        if (event.getParam('value') == 'Technology Solutions') {
            component.set("v.simpleNewCase.RecordTypeId", "0126A000000hC35QAE");
            helper.getProducts(component);
            component.set("v.simpleNewCase.Urgency__c", "Low");
            component.set("v.simpleNewCase.Techno_Impact__c", "Low");
        } else if (event.getParam('value') == 'Information Offering') {
            component.set("v.simpleNewCase.RecordTypeId", "0126A000000hC33QAE");
            var account = component.get("v.account");
            if (account.AccountCountry__c != 'US') helper.getProducts(component);
            else helper.getUserAssetsForDATA(component);
            component.set("v.simpleNewCase.Priority", "Low");
            component.set("v.simpleNewCase.Urgency__c", "");
            component.set("v.simpleNewCase.Techno_Impact__c", "");
            helper.getPickListOptions(component, "Metric__c", "v.metrics");
        }
    },

    onProductChangeUS: function (component, event, helper) {
        component.set("v.productSelectError", false);
        var assetIds = component.get("v.assets");
        var recordId = component.get("v.simpleNewCase.RecordTypeId");
        var assetId = component.get("v.simpleNewCase.AssetId");
        
        for (var i = 0; i < assetIds.length; i++) {
            if (assetIds[i].value == assetId) {
                component.set("v.simpleNewCase.ProductName__c", assetIds[i].label);
                break;
            }
        }
    },
    
    onProductChange: function (component, event, helper) {
        component.set("v.productSelectError", false);
        var assetIds = component.get("v.assetIds");
        var recordId = component.get("v.simpleNewCase.RecordTypeId");
        var assetId = component.find("product" + recordId).get("v.value");
        for (var i = 0; i < assetIds.length; i++) {
            if (assetIds[i].label == assetId) {
                component.set("v.simpleNewCase.AssetId", assetIds[i].value);
                component.set("v.simpleNewCase.ProductName__c", assetIds[i].label);
                break;
            }
        }
        $A.util.removeClass(component.find("productSelectFormElement" + recordId), "is-required slds-has-error lightningInput");
        if (component.find("product" + recordId).get("v.label") == "Please Specify") {
            component.set("v.productSelectError", true);
            $A.util.addClass(component.find("productSelectFormElement" + recordId), "is-required slds-has-error lightningInput");
        }

        var showTechnoSubtypes = false;
        var allSubTypeForThisProduct = component.get("v.allSubTypeForThisProduct");
        for (var i = 0; i < allSubTypeForThisProduct.length; i++) {
            if (allSubTypeForThisProduct[i] === component.find("product" + recordId).get("v.value")) {
                showTechnoSubtypes = true;
            }
        }
        component.set("v.showTechnoSubtypes", showTechnoSubtypes);
        if (showTechnoSubtypes) {
            helper.getSubtype(component, 1);
        } else if (component.get("v.AccountCaseSubtype") && component.get("v.ContactUserType") == "HO User" && recordId != "0126A000000hC33QAE") {
            helper.getTechnoSubtype(component);
        }
        helper.getCategorizationAndAsset(component);
    },

    onTechnoSubTypeChange: function (component, event, helper) {
        helper.getCategorizationAndAsset(component);
    },

    onSubType1Change: function (component, event, helper) {
        helper.getSubtype(component, 2);
        $A.util.removeClass(component.find("subType1SelectFormElement"), "is-required slds-has-error lightningInput");
        component.set("v.simpleNewCase.SubType1__c", component.find("subType1").get("v.value"));
    },
    onSubType2Change: function (component, event, helper) {
        helper.getSubtype(component, 3);
        $A.util.removeClass(component.find("subType2SelectFormElement"), "is-required slds-has-error lightningInput");
        component.set("v.simpleNewCase.SubType2__c", component.find("subType2").get("v.value"));
    },
    onSubType3Change: function (component, event, helper) {
        $A.util.removeClass(component.find("subType3SelectFormElement"), "is-required slds-has-error lightningInput");
        component.set("v.simpleNewCase.SubType3__c", component.find("subType3").get("v.value"));
    },
    onSimplePickListChange: function (component, event, helper) {
        component.set("v.simpleNewCase." + event.getSource().getLocalId(), event.getSource().get("v.value"));
    },

    onCaseCategoryChange: function (component, event, helper) {
        // var selectedVal = component.get("v.simpleNewCase.CaseSubType2__c");
        var selectedVal = component.get("v.simpleNewCase.SubType2__c");
        if (selectedVal != "") {
            var caseCategories = component.get("v.caseCategories");
            var index = caseCategories.map(function (e) { return e.api; }).indexOf(selectedVal);
            var pbTypesObj = caseCategories[index].pbTypesObj;
            component.set("v.pbTypesObj", pbTypesObj);
            component.set("v.disabledProblem_Type", false);
        } else {
            // component.set("v.simpleNewCase.CaseSubType3__c",""); 
            component.set("v.simpleNewCase.SubType3__c", "");
            component.set("v.disabledProblem_Type", true);
        }
    },

    closeAttachmentForm: function (component, event, helper) {
        var message = "Your case is created";
        helper.closeAttachmentForm(component);
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": "success",
            "title": "Saved",
            "message": message
        });
        resultsToast.fire();
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/case/" + component.get("v.simpleNewCase.Id")
        });
        urlEvent.fire();
    },

    openAttachmentForm: function (component, event, helper) {
        helper.openAttachmentForm(component);
    },

    handleUploadFinished: function (cmp, event) {
        var uploadedFiles = event.getParam("files");
        var message = "";
        if (uploadedFiles.length == 1) message = uploadedFiles.length + " file was added to case"
        else if (uploadedFiles.length > 1) message = uploadedFiles.length + " files were added to case"
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "type": "success",
            "title": "Saved",
            "message": message
        });
        resultsToast.fire();
    },

    handleSaveCase: function (component, event, helper) {
        if (helper.validateCaseForm(component)) {
            component.set("v.isLoading", true);
            var recordTypeId = component.get("v.simpleNewCase.RecordTypeId");
            // PEP set the request origin
            component.set("v.simpleNewCase.Origin", component.get("v.communityOrigin"));
            if (recordTypeId === "0126A000000hC33QAE") {
                component.set("v.simpleNewCase.SubType1__c", "DATA MGT/PRODUCTION");
            }
            component.find("caseRecordCreator").saveRecord(function (saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    component.set("v.newCaseId", component.get("v.simpleNewCase.Id"));
                    helper.openAttachmentForm(component);
                } else if (saveResult.state === "INCOMPLETE") {
                    component.set("v.newCaseError", "You are offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    component.set("v.newCaseError", "Problem saving case, error: " + JSON.stringify(saveResult.error));
                } else {
                    component.set("v.newCaseError", "Unknown problem, state: " + saveResult.state + ", error: " + JSON.stringify(saveResult.error));
                }
                component.set("v.isLoading", false);
            });
        }
    }
})