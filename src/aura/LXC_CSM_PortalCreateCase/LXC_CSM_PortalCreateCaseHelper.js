({
    getCategorizationAndAsset: function (component) {
        component.set("v.isLoading", true);
        var recordId = component.get("v.simpleNewCase.RecordTypeId");
        var productName = component.find("product" + recordId).get("v.value");
        var cshSubtype;
        if (recordId != "0126A000000hC33QAE")
            cshSubtype = (component.get("v.AccountCaseSubtype") && component.get("v.ContactUserType") == "HO User" && component.find("caseSubType").get("v.value") != "undefined") ? component.find("caseSubType").get("v.value") : "Please Specify";
        var action = component.get("c.getCategorization");
        action.setParams({
            "productName": productName,
            "cshSubtype": cshSubtype
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.simpleNewCase.Case_CategorizationId__c", response.getReturnValue()[0].Id);
                component.set("v.simpleNewCase.ProductName__c", response.getReturnValue()[0].ProductName__c);
                if (component.get("v.AccountCaseSubtype") && component.get("v.ContactUserType") == "HO User" && recordId != "0126A000000hC33QAE") {
                    component.set("v.simpleNewCase.CSHSubType__c", response.getReturnValue()[0].CSHSubType__c);
                }
                component.set("v.simpleNewCase.SubType1__c", response.getReturnValue()[0].SubType1__c);
                component.set("v.simpleNewCase.SubType2__c", response.getReturnValue()[0].SubType2__c);
                component.set("v.isLoading", false);

            } else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.isLoading", false);
            }
        });
        $A.enqueueAction(action);
    },

    getUserAccount: function (component) {
        var action = component.get("c.getUserAccount");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var account = response.getReturnValue()[0];
                component.set("v.account", account);
                if (account) {
                    if (account.AccountCountry__c == 'US') {
                        component.set("v.simpleNewCase.PhoneVerification__c", true);
                    } else {
                        component.set("v.simpleNewCase.PhoneVerification__c", false);
                        component.set("v.disabledProblem_Type", true);
                    }
                    component.set("v.loggedInUserAccountId", account.Id);
                    component.set("v.AccountCaseSubtype", account.CSH_SubType__c);
                    component.set("v.simpleNewCase.AccountId", account.Id);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    getUserContact: function (component) {
        var action = component.get("c.getUserContact");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var contact = response.getReturnValue()[0];
                if (contact) {
                    component.set("v.contact", contact);
                    var portal_case_type = contact.Portal_Case_Type__c.split(';');
                    component.set("v.portalCaseTypes", portal_case_type);
                    if (portal_case_type.length == 1) {
                        component.set("v.portalCaseTypeSelectedValue", portal_case_type[0])
                    }
                    component.set("v.simpleNewCase.ContactId", contact.Id);
                    component.set("v.ContactUserType", contact.Contact_User_Type__c);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    getUserAssetsForDATA: function (component) {
        var action = component.get("c.getUserAssetsForDATA2");
        action.setParams({
            "names": component.get("v.assetsName")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var assets = response.getReturnValue();
                var opts = [];
                if (assets != undefined && assets.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "Please Specify",
                        value: ""
                    });
                }
                for (var i = 0; i < assets.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: assets[i].Name,
                        value: assets[i].Id
                    });
                }
                component.set("v.assets", opts);


            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },

    getPickListOptions: function (component, field, optionsNameAttribut) {
        var action = component.get("c.getPickListOptions");
        action.setParams({
            "fld": field
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var opts = [];
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "Please Specify",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set(optionsNameAttribut, opts);

            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },

    getProducts: function (component) {
        if (component.get("v.AccountCaseSubtype") && component.get("v.ContactUserType") == "HO User") {
            component.set("v.isDependentTechnoSubTypeDisable", true);
        }
        component.set("v.isLoading", true);
        var action = component.get("c.getProductCategorizationForNewCase");
        action.setParams({
            "accountId": component.get("v.account").Id,
            "recordType": component.get("v.simpleNewCase.RecordTypeId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var productsList = response.getReturnValue();
                component.set("v.assetIds", productsList);
                var productNamesArray = [];
                productNamesArray.push({
                    class: "optionClass",
                    label: "Please Specify",
                    value: "Please Specify"
                });
                for (var i = 0; i < productsList.length; i++) {
                    productNamesArray.push({ class: "optionClass", label: productsList[i].label, value: productsList[i].label });
                }
                var recordId = component.get("v.simpleNewCase.RecordTypeId");
                component.find("product" + recordId).set("v.options", productNamesArray);
                component.set("v.isLoading", false);
            } else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.isLoading", false);
            }

        });
        $A.enqueueAction(action);
    },

    getTechnoSubtype: function (component) {
        component.set("v.isLoading", true);
        var recordId = component.get("v.simpleNewCase.RecordTypeId");
        var productName = component.find("product" + recordId).get("v.value");
        if (productName != "Please Specify") {
            //PEP-ACN add new param to filter by PRM subtype in case of partner portal
            var action = component.get("c.getSubtypeCategorization");
            action.setParams({
                "productName": productName,
                "origin": component.get("v.communityOrigin")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var subtypeList = response.getReturnValue();
                    var subtypeArray = [];
                    if (subtypeList.length > 1) {
                        for (var i = 0; i < Object.keys(subtypeList).length; i++) {
                            var subtype = subtypeList[i].CSHSubType__c;
                            subtypeArray.push({ class: "optionClass", label: subtype, value: subtype });
                        }
                        var idx = subtypeArray.map(function (element) { return element.value; }).indexOf("Please Specify");
                        if (idx != -1) {
                            subtypeArray.splice(idx, 1);
                            subtypeArray.splice(0, 0, { class: "optionClass", label: "Please Specify", value: "Please Specify" });
                        }
                        component.find("caseSubType").set("v.options", subtypeArray);
                        component.set("v.isDependentTechnoSubTypeDisable", false);
                        component.set("v.isLoading", false);
                    } else if (subtypeList.length == 1 && subtypeList[0].CSHSubType__c == "Please Specify") {
                        subtypeArray.splice(idx, 1);
                        subtypeArray.splice(0, 0, { class: "optionClass", label: "Please Specify", value: "Please Specify" });
                        component.find("caseSubType").set("v.options", subtypeArray);
                        component.set("v.isDependentTechnoSubTypeDisable", true);
                        component.set("v.isLoading", false);
                    }
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    component.set("v.isLoading", false);
                }

            });
        } else {
            if (component.get("v.AccountCaseSubtype")) {
                component.set("v.isDependentTechnoSubTypeDisable", true);
                component.find("caseSubType").set("v.options", { class: "optionClass", label: "Please Specify", value: "Please Specify" });
            }

            component.set("v.isLoading", false);
        }
        $A.enqueueAction(action);
    },
    validateCaseForm: function (component) {
        var recordId = component.get("v.simpleNewCase.RecordTypeId");
        component.set("v.productSelectError", false);
        component.set("v.mailFormatError", false);
        if (component.find("productSelectFormElement" + recordId)) $A.util.removeClass(component.find("productSelectFormElement" + recordId), "is-required slds-has-error lightningInput");
        if (component.find("MailCcFormElement" + recordId)) $A.util.removeClass(component.find("MailCcFormElement" + recordId), "slds-has-error lightningInput");
        var validCase = true;
        // Show error messages if required fields are blank
        var validCase = component.find('caseField').reduce(function (validFields, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        if (component.find("product" + recordId)) {
            if (component.find("product" + recordId).get("v.value") == "Please Specify") {
                validCase = false;
                $A.util.addClass(component.find("productSelectFormElement" + recordId), "is-required slds-has-error lightningInput");
                component.set("v.productSelectError", true);
            }
        }

        if (component.get("v.showTechnoSubtypes")) {
            if (component.find("subType1").get("v.value") == "Please Specify") {
                validCase = false;
                $A.util.addClass(component.find("subType1SelectFormElement"), "is-required slds-has-error lightningInput");
            }
            if (component.find("subType2").get("v.value") == "Please Specify") {
                validCase = false;
                $A.util.addClass(component.find("subType2SelectFormElement"), "is-required slds-has-error lightningInput");
            }
            if (component.find("subType3").get("v.value") == "Please Specify") {
                validCase = false;
                $A.util.addClass(component.find("subType3SelectFormElement"), "is-required slds-has-error lightningInput");
            }
        }
        if (component.find("mailCcList" + recordId)) {
            var mailFlag = true;
            var emailField = component.find("mailCcList" + recordId);
            var emailFieldValue = emailField.get("v.value");
            var mailformat = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
            if (!$A.util.isEmpty(emailFieldValue)) {
                emailFieldValue = emailFieldValue.trim();
                if (emailFieldValue.indexOf(";") > -1) {
                    var multiemails = emailFieldValue.split(";");
                    for (var index = 0; index < multiemails.length; index++) {
                        var emailAddress = multiemails[index].trim();
                        if (emailAddress.trim().match(mailformat)) {
                            mailFlag = true;
                        }
                        else {
                            mailFlag = false;
                            break;
                        }
                    }
                }
                else {
                    if (emailFieldValue.match(mailformat)) {
                        mailFlag = true;
                    }
                    else {
                        mailFlag = false;
                    }
                }
                if (mailFlag) {
                    $A.util.removeClass(component.find("MailCcFormElement" + recordId), 'slds-has-error');
                    component.set("v.mailFormatError", false);
                    validCase = true;
                }
                else {
                    $A.util.addClass(component.find("MailCcFormElement" + recordId), 'slds-has-error lightningInput');
                    component.set("v.mailFormatError", true);
                    validCase = false;
                }
            }
        }
        return (validCase);
    },

    openAttachmentForm: function (component) {
        $A.util.removeClass(component.find("modaldialog_caseCreateAttachment"), "slds-fade-in-hide");
        $A.util.addClass(component.find("modaldialog_caseCreateAttachment"), "slds-fade-in-open");
        $A.util.removeClass(component.find("backdrop_caseCreateAttachment"), "slds-backdrop--hide");
        $A.util.addClass(component.find("backdrop_caseCreateAttachment"), "slds-backdrop--open");

    },
    closeAttachmentForm: function (component) {
        $A.util.addClass(component.find("modaldialog_caseCreateAttachment"), "slds-fade-in-hide");
        $A.util.removeClass(component.find("modaldialog_caseCreateAttachment"), "slds-fade-in-open");
        $A.util.addClass(component.find("backdrop_caseCreateAttachment"), "slds-backdrop--hide");
        $A.util.removeClass(component.find("backdrop_caseCreateAttachment"), "slds-backdrop--open");
    },
    getPriority: function (component) {
        var action = component.get("c.getpriorityvalue");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var priorityList = response.getReturnValue();
                var priorityNamesArray = [];
                for (var i = 0; i < Object.keys(priorityList).length; i++) {
                    var productName = priorityList[i];
                    priorityNamesArray.push(productName);
                }
                component.set("v.prioritytype", priorityNamesArray);
                component.set("v.simpleNewCase.Priority", "Low");
                //component.find("priority").set("v.prioritytype", productNamesArray);               
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.isLoading2", false);
            }
        });
        $A.enqueueAction(action);
    },

    getUrgency: function (component) {
        var action = component.get("c.getUrgencyList");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var urgencyList = response.getReturnValue();
                var urgencyNamesArray = [];
                for (var i = 0; i < Object.keys(urgencyList).length; i++) {
                    var urgencyName = urgencyList[i];
                    urgencyNamesArray.push(urgencyName);
                }
                component.set("v.urgencyList", urgencyNamesArray);
                component.set("v.simpleNewCase.Urgency__c", "Low");
                //component.find("priority").set("v.prioritytype", productNamesArray);               
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.isLoading2", false);
            }
        });
        $A.enqueueAction(action);
    },
    getImpact: function (component) {
        var action = component.get("c.getImpactList");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var impactList = response.getReturnValue();
                var impactNamesArray = [];
                for (var i = 0; i < Object.keys(impactList).length; i++) {
                    var impactName = impactList[i];
                    impactNamesArray.push(impactName);
                }
                component.set("v.impactList", impactNamesArray);
                component.set("v.simpleNewCase.Techno_Impact__c", "Low");
                //component.find("priority").set("v.prioritytype", productNamesArray);               
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.isLoading2", false);
            }
        });
        $A.enqueueAction(action);
    },

    getSubtype: function (component, idx) {
        var record = component.get("v.simpleNewCase");
        var currentControllingField;
        if (idx > 1) {
            currentControllingField = "subType" + (idx - 1);
            $A.util.removeClass(component.find("subType" + idx + "SelectFormElement"), "is-required slds-has-error lightningInput");
        }
        else currentControllingField = "product0126A000000hC35QAE";
        if (component.find(currentControllingField).get("v.value") != "Please Specify") {
            var q = "";
            var c = "";
            if (idx > 1) {
                for (var j = 1; j < idx; j++) {
                    c += " and SubType" + j + "__c='" + component.find("subType" + j).get("v.value") + "'";
                }
            }
            var controllingField = component.find("product0126A000000hC35QAE").get("v.value");
            q = "select SubType" + idx + "__c  from CSM_QI_Case_Categorization__c where RecordTypeId__c='" + record.RecordTypeId + "' and Active__c=true and ProductName__c ='" + controllingField + "'" + c + " group by SubType" + idx + "__c";
            var action = component.get("c.getCategorizationWithAggregate");
            action.setParams({ "q": q });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var subtypeList = response.getReturnValue();
                    var subtypeArray = [];
                    var subtype;
                    for (var i = 0; i < Object.keys(subtypeList).length; i++) {
                        subtype = subtypeList[i]["SubType" + idx + "__c"];
                        if (subtype != undefined) subtypeArray.push({ class: "optionClass", label: subtype, value: subtype });
                        else subtypeArray.push({ class: "optionClass", label: "none", value: "Please Specify" });
                    }
                    for (var i = 0; i < subtypeArray.length; i++) {
                        if (subtypeArray[i].value === "Please Specify") {
                            subtypeArray.unshift(subtypeArray[i]);
                            subtypeArray.splice(i + 1, 1);
                        }
                    }
                    component.find("subType" + idx).set("v.options", subtypeArray);
                    component.find("subType" + idx).set("v.value", "Please Specify");
                    component.set("v.isDependentSubType" + idx + "Disable", false);
                    component.set("v.simpleNewCase.SubType" + idx + "__c", component.find("subType" + idx).get("v.value"));

                    if (idx < 3) this.getSubtype(component, idx + 1);
                    else component.set("v.isLoading", false);
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.isDependentSubType" + idx + "Disable", true);
            component.find("subType" + idx).set("v.options", { class: "optionClass", label: "Please Specify", value: "Please Specify" });
            if (idx < 3) this.getSubtype(component, idx + 1);
            else component.set("v.isLoading", false);
        }
    },
})