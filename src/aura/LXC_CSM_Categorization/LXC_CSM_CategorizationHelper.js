({
    getRecord: function (component) {
        component.set("v.isLoading", true);
        var recordId = component.get("v.recordId");
        var objName = component.get("v.sObjectName ");
        var action = component.get("c.getRecord");
        action.setParams({
            "recordId": recordId,
            "objName": objName,
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var object = response.getReturnValue()[0];
                if (object != undefined) {
                    component.set("v.showEditButton", true);
                    component.set("v.object", object);
                    component.set("v.simpleRecord", object);
                    if (objName === "Knowledge__kav") {
                        this.mustHideEdit(component);
                        this.getControllingField(component);
                    } else if (object.RecordTypeId == component.get("v.TechnoRecordTypeId") && (objName === "Case")) {
                        if (object.AccountId == undefined) {
                            var resultsToast = $A.get("e.force:showToast");
                            resultsToast.setParams({
                                "type": "warning",
                                "title": "Categorization",
                                "message": "Please select the Account Name"
                            });
                            var errors = [];
                            errors.push("Please select the Account Name");
                            component.set("v.errors", errors);
                        }
                        this.getControllingField(component);
                        this.getCaseArticles(component);
                        this.mustForceToFill(component);
                        this.mustHideEdit(component);
                        this.getActiveTimeSheetRecord(component);
                    }
                    else if ((object.RecordTypeId == component.get("v.RDAssistReqRecordTypeId")) || (object.RecordTypeId == component.get("v.RDActivityPlanRecordTypeId"))) {
                        this.getControllingField(component);
                        if (object.RecordTypeId == component.get("v.RDActivityPlanRecordTypeId")) {
                            this.fetchPicklistValues(component, 'LOS__c', 'Template__c');
                        }
                    } else if (object.RecordTypeId == component.get("v.DataRecordTypeId")) {
                        this.getControllingField(component);
                        this.mustForceToFill(component);
                        this.mustHideEdit(component);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },

    getControllingField: function (component) {
        var objName = component.get("v.sObjectName");
        var record = component.get("v.simpleRecord");
        var action;
        var field;
        if (objName === "Case") {
            if (component.get("v.TechnoRecordTypeId") === record.RecordTypeId) {
                field = "ProductName__c";
                action = component.get("c.getProductCategorizationForCase");
                action.setParams({
                    "recordId": record.Id
                });
            } else if ((record.RecordTypeId == component.get("v.RDAssistReqRecordTypeId")) || (record.RecordTypeId == component.get("v.RDActivityPlanRecordTypeId"))) {
                field = "LOS__c";
                action = component.get("c.getCategorizationWithAggregate");
                action.setParams({
                    "q": "select " + field + "  from CSM_QI_Case_Categorization__c where RecordTypeId__c='" + record.RecordTypeId + "' and Active__c=true group by " + field
                });
            } else if (component.get("v.DataRecordTypeId") === record.RecordTypeId) {
                field = "ProductName__c";
                action = component.get("c.getProductCategorizationForCase");
                action.setParams({
                    "recordId": record.Id
                });
            }
        } else if (objName === "Knowledge__kav") {
            field = "ProductName__c";
            action = component.get("c.getProductCategorization");
        }
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var controllingFieldList = response.getReturnValue();
                var controllingFieldArray = [];
                controllingFieldArray.push({
                    class: "optionClass",
                    label: "Please Specify",
                    value: "Please Specify"
                })
                for (var i = 0; i < Object.keys(controllingFieldList).length; i++) {
                    var value;
                    if (controllingFieldList[i].hasOwnProperty("Name")) {
                        value = controllingFieldList[i].Name;
                    } else { value = controllingFieldList[i][field]; }
                    if (value != undefined) controllingFieldArray.push({ class: "optionClass", label: value, value: value });
                }
                component.find("controllingField").set("v.options", controllingFieldArray);
                if (record[field] == undefined) component.find("controllingField").set("v.value", "Please Specify");
                else component.find("controllingField").set("v.value", record[field]);
                this.getSubtype(component, 1);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getSubtype: function (component, idx) {
        var objName = component.get("v.sObjectName");
        var record = component.get("v.simpleRecord");
        var currentControllingField;
        if (idx > 1) currentControllingField = "subType" + (idx - 1);
        else currentControllingField = "controllingField";
        if (component.find(currentControllingField).get("v.value") != "Please Specify" || component.get("v.DataRecordTypeId") === record.RecordTypeId) {
            var q = "";
            var c = "";
            if (idx > 1) {
                for (var j = 1; j < idx; j++) {
                    c += " and SubType" + j + "__c='" + component.find("subType" + j).get("v.value") + "'";
                }
            }
            var controllingField = component.find("controllingField").get("v.value");
            if (objName === "Knowledge__kav") q = "select SubType" + idx + "__c  from CSM_QI_Case_Categorization__c where Active__c=true and ProductName__c ='" + controllingField + "'" + c + " group by SubType" + idx + "__c";
            else if (record.RecordTypeId == component.get("v.TechnoRecordTypeId")) q = "select SubType" + idx + "__c  from CSM_QI_Case_Categorization__c where RecordTypeId__c='" + record.RecordTypeId + "' and Active__c=true and ProductName__c ='" + controllingField + "'" + c + " group by SubType" + idx + "__c";
            else if ((record.RecordTypeId == component.get("v.RDAssistReqRecordTypeId")) || (record.RecordTypeId == component.get("v.RDActivityPlanRecordTypeId"))) q = "select SubType" + idx + "__c  from CSM_QI_Case_Categorization__c where RecordTypeId__c='" + record.RecordTypeId + "' and Active__c=true and LOS__c ='" + controllingField + "'" + c + " group by SubType" + idx + "__c";
            else if (record.RecordTypeId == component.get("v.DataRecordTypeId")) q = "select SubType" + idx + "__c  from CSM_QI_Case_Categorization__c where RecordTypeId__c='" + record.RecordTypeId + "' and Active__c=true " + c + " group by SubType" + idx + "__c";
            var action = component.get("c.getCategorizationWithAggregate");
            action.setParams({
                "q": q
            });
            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === "SUCCESS") {
                    var subtypeList = response.getReturnValue();
                    var subtypeArray = [];
                    var subtype;
                    for (var i = 0; i < Object.keys(subtypeList).length; i++) {
                        subtype = subtypeList[i]["SubType" + idx + "__c"];
                        if (subtype != undefined) subtypeArray.push({ class: "optionClass", label: subtype, value: subtype });
                        else subtypeArray.push({ class: "optionClass", label: "none", value: "Please Specify" });
                    }

                    for (var i = 0; i < subtypeArray.length; i++) {
                        if (subtypeArray[i].value === "Please Specify") {
                            subtypeArray.unshift(subtypeArray[i]);
                            subtypeArray.splice(i + 1, 1);
                        }
                    }

                    component.find("subType" + idx).set("v.options", subtypeArray);
                    component.find("subType" + idx).set("v.value", "Please Specify");
                    for (var i = 0; i < subtypeArray.length; i++) {
                        if (subtypeArray[i].value === record["SubType" + idx + "__c"]) component.find("subType" + idx).set("v.value", record["SubType" + idx + "__c"]);

                    }
                    component.set("v.isDependentSubType" + idx + "Disable", false);

                    if (idx < 3) this.getSubtype(component, idx + 1);
                    else component.set("v.isLoading", false);
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.isDependentSubType" + idx + "Disable", true);
            component.find("subType" + idx).set("v.options", { class: "optionClass", label: "Please Specify", value: "Please Specify" });
            if (idx < 3) this.getSubtype(component, idx + 1);
            else component.set("v.isLoading", false);
        }
    },

    getTemplate: function (component) {
        var controllerValueKey = component.find("controllingField").get("v.value");
        var Map = component.get("v.dependentFieldMap");
        if (controllerValueKey != 'Please Specify') {
            var ListOfDependentFields = Map[controllerValueKey];
            this.fetchDepValues(component, ListOfDependentFields);
        } else {
            var defaultVal = [{
                class: "optionClass",
                label: 'Please Specify',
                value: 'Please Specify'
            }];
            component.find('template').set("v.options", defaultVal);
            component.set("v.isDependentDisable", true);
        }
        var record = component.get("v.simpleRecord");
        if (record.Template__c) component.find("template").set("v.value", record.Template__c);
    },

    getCaseArticles: function (component) {
        var objName = component.get("v.sObjectName");
        var record = component.get("v.simpleRecord");
        if ((objName === "Case") && (record.RecordTypeId == component.get("v.TechnoRecordTypeId"))) {
            var recordId = component.get("v.recordId");
            var action = component.get("c.getCaseArticles");
            action.setParams({
                "caseId": recordId
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var numberOfArticle = Object.keys(response.getReturnValue()).length;
                    var c = record;
                    if (numberOfArticle != c.Number_of_Article__c) {
                        c.Number_of_Article__c = numberOfArticle;
                        this.updateCase(component);
                    }
                    var caseArticle = response.getReturnValue()[0];
                    component.set("v.caseArticle", caseArticle);
                    if (caseArticle != undefined) {
                        var recordId = caseArticle.KnowledgeArticleVersionId;
                        var objName = "Knowledge__kav";
                        var action2 = component.get("c.getRecord");
                        action2.setParams({
                            "recordId": recordId,
                            "objName": objName
                        });
                        action2.setCallback(this, function (response) {
                            var state = response.getState();
                            if (state === "SUCCESS") {
                                var k = response.getReturnValue()[0];
                                var c = component.get("v.simpleRecord");
                                if (k.ProductName__c != undefined) {
                                    component.find("controllingField").set("v.value", k.ProductName__c);
                                    component.find("subType1").set("v.value", k.SubType1__c);
                                    component.find("subType2").set("v.value", k.SubType2__c);

                                    var kb_subtype3;
                                    if (k.SubType3__c == undefined) {
                                        if (k.ProductName__c == "OCE Marketing" || k.ProductName__c == "OCE Sales") kb_subtype3 = "Please Specify";
                                        else kb_subtype3 = "--none--";
                                    }
                                    else kb_subtype3 = k.SubType3__c;
                                    component.find("subType3").set("v.value", kb_subtype3);

                                    if ((k.ProductName__c != c.ProductName__c) ||
                                        (k.SubType1__c != c.SubType1__c) ||
                                        (k.SubType2__c != c.SubType2__c) ||
                                        (kb_subtype3 != c.SubType3__c)) {
                                        component.set("v.showEditButton", false);
                                        component.find("controllingField").set("v.options", { class: "optionClass", label: k.ProductName__c, value: k.ProductName__c });
                                        component.find("controllingField").set("v.value", k.ProductName__c);
                                        component.find("subType3").set("v.options", { class: "optionClass", label: kb_subtype3, value: kb_subtype3 });
                                        component.find("subType3").set("v.value", kb_subtype3);
                                        component.find("subType2").set("v.options", { class: "optionClass", label: k.SubType2__c, value: k.SubType2__c });
                                        component.find("subType2").set("v.value", k.SubType2__c);
                                        component.find("subType1").set("v.options", { class: "optionClass", label: k.SubType1__c, value: k.SubType1__c });
                                        component.find("subType1").set("v.value", k.SubType1__c);
                                        this.updateObjectCategorization(component);
                                    } else if ((component.find("controllingField").get("v.value") == k.ProductName__c)) {
                                        component.set("v.showEditButton", false);
                                        component.set("v.isLoading", false);
                                    } else {
                                        component.set("v.showEditButton", true);
                                        component.set("v.isLoading", false);
                                    }
                                } else {
                                    component.set("v.showEditButton", true);
                                    component.set("v.isLoading", false);
                                }
                            }
                            //this.updateCaseCountArticle(component);
                        });
                        $A.enqueueAction(action2);

                    } else {
                        this.getControllingField(component);
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },

    mustHideEdit: function (component) {
        var objName = component.get("v.sObjectName");
        if (objName === "Knowledge__kav") {
            var k = component.get("v.object");
            if (k.PublishStatus != 'Draft') component.set("v.showEditButton", false);
        } else if (objName === "Case") {
            var c = component.get("v.object");
            if (c.Status === 'Closed' || c.Status === 'Canceled' || c.Status === 'Abandoned') component.set("v.showEditButton", false);
        }
    },

    mustForceToFill: function (component) {
        var objName = component.get("v.sObjectName");
        var record = component.get("v.simpleRecord");
        if (objName === "Case") {
            var c = component.get("v.object");
            var createdDate = new Date(c.CreatedDate);
            var closedDate = new Date(c.ClosedDate);
            var today = new Date();
            var accountCountry = c.AccountCountry__c;
            if (record.RecordTypeId == component.get("v.DataRecordTypeId")
                && ((c.SubType1__c == undefined) || (c.SubType2__c == undefined)|| (c.SubType3__c == undefined)
                   || (c.SubType1__c == "Please Specify") || (c.SubType2__c == "Please Specify")|| (c.SubType3__c == "Please Specify") || (c.SubType3__c == "--none--"))
                ) {
                this.openForm(component);
                component.set("v.isMandatory", true);
            } else if (((closedDate.getTime() === createdDate.getTime()))
                && ((c.ProductName__c == undefined)
                    || (c.SubType1__c == "Please Specify")
                    || (c.SubType2__c == "Please Specify")
                    || (c.SubType3__c == "Please Specify"))
                && (accountCountry != "US")) {
                this.openForm(component);
                component.set("v.isMandatory", true);
            } else if ((((today.getTime() - createdDate.getTime()) < 10000)
                && (c.ProductName__c == undefined)
                && (accountCountry != "US"))) {
                this.openForm(component);
            }
        }
    },

    updateCase: function (component) {
        var c = component.get("v.object");
        var action = component.get("c.updateCase");
        action.setParams({
            "c": c
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            }
        });
        $A.enqueueAction(action);
    },

    updateObjectCategorization: function (component) {
        component.set("v.isLoading2", true);
        var record = component.get("v.simpleRecord");
        var objName = component.get("v.sObjectName");
        var controllingField = component.find("controllingField").get("v.value");
        var subtype1 = component.find("subType1").get("v.value");
        var subtype2 = component.find("subType2").get("v.value");
        var subtype3 = component.find("subType3").get("v.value");
        var template = "";
        if (record.RecordTypeId == component.get("v.RDActivityPlanRecordTypeId")) {
            template = component.find("template").get("v.value");
            if (template == "Please Specify") template = "";
        }
        var action = component.get("c.updateObjectCategorization");
        action.setParams({
            "objectToUpdate": record,
            "controllingField": controllingField,
            "subtype1": subtype1,
            "subtype2": subtype2,
            "subtype3": subtype3,
            "template": template,
            recordTypeId: record.RecordTypeId
        });
        action.setCallback(this, function (response) {
            component.set("v.isLoading2", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("[LXC_CSM_Categorization] Save SUCCESS...");
                if(component.get("v.isMandatory") == true){
                    component.set("v.isMandatory",false);
                }
                this.closeForm(component);
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "success",
                    "title": "Saved",
                    "message": "the categorization was saved."
                });
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            } else if (state === "ERROR") {
                var errors = response.getError();
                //alert(JSON.stringify(errors));
                if (errors.length > 0) {
                    if (errors[0] != null && errors[0].message != null) {
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "type": "error",
                            "title": "Error",
                            "message": errors[0].message
                        });
                        resultsToast.fire();
                        $A.get("e.force:refreshView").fire();
                    } else if (errors[0] != null && errors[0].pageErrors[0] != null && errors[0].pageErrors[0].message != null) {
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "type": "error",
                            "title": "Error",
                            "message": errors[0].pageErrors[0].message
                        });
                        resultsToast.fire();
                        $A.get("e.force:refreshView").fire();
                    }
                } else {
                    console.log("Unknown error");
                }

            }
        });
        $A.enqueueAction(action);
    },


    fetchPicklistValues: function (component, controllerField, dependentField) {
        var action = component.get("c.getDependentOptionsImpl");
        action.setParams({
            'objApiName': "Case",
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField
        });
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set("v.dependentFieldMap", StoreResponse);
                var listOfkeys = [];
                var ControllerField = [];

                for (var singlekey in StoreResponse) {
                    listOfkeys.push(singlekey);
                }

                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push({
                        class: "optionClass",
                        label: "Please Specify",
                        value: "Please Specify"
                    });
                }

                this.getTemplate(component);
                for (var i = 0; i < listOfkeys.length; i++) {
                    ControllerField.push({
                        class: "optionClass",
                        label: listOfkeys[i],
                        value: listOfkeys[i]
                    });
                }
            }
        });
        $A.enqueueAction(action);
    },


    fetchDepValues: function (component, ListOfDependentFields) {
        var dependentFields = [];

        if (ListOfDependentFields != undefined && ListOfDependentFields.length > 0) {
            dependentFields.push({
                class: "optionClass",
                label: "Please Specify",
                value: "Please Specify"
            });

        }
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            dependentFields.push({
                class: "optionClass",
                label: ListOfDependentFields[i],
                value: ListOfDependentFields[i]
            });
        }
        component.find('template').set("v.options", dependentFields);
        component.set("v.isDependentDisable", false);
    },

    openForm: function (component) {
        $A.util.removeClass(component.find("modaldialog"), "slds-fade-in-hide");
        $A.util.addClass(component.find("modaldialog"), "slds-fade-in-open");
        $A.util.removeClass(component.find("backdrop"), "slds-backdrop--hide");
        $A.util.addClass(component.find("backdrop"), "slds-backdrop--open");

    },

    closeForm: function (component) {
        $A.util.addClass(component.find("modaldialog"), "slds-fade-in-hide");
        $A.util.removeClass(component.find("modaldialog"), "slds-fade-in-open");
        $A.util.addClass(component.find("backdrop"), "slds-backdrop--hide");
        $A.util.removeClass(component.find("backdrop"), "slds-backdrop--open");
    },
    getActiveTimeSheetRecord : function(component) {
        var record = component.get("v.simpleRecord");
        var action = component.get("c.getTimeSheetRecord");
        action.setParams({
            "caseId" : record.Id,
        });
        action.setCallback(this, function(response) {
            component.set("v.isLoading2", false);
            var state = response.getState();
            var TimeSheet = response.getReturnValue();
            if (state === "SUCCESS") {
                if(TimeSheet != undefined && record.Id == TimeSheet.Case__c){
                    
                    var actionAPI = component.find("quickActionAPI");
                    var fields = {"sObj":"Case"};
                    var args = { actionName : "Case.Time_Sheet_LC", 
                                entityName : "Case",
                                targetFields : fields };
                    actionAPI.setActionFieldValues(args).then(function() {
                        actionAPI.invokeAction(args);
                    }).catch(function(e) {
                    });
                }
            }else if(state === "ERROR") {
                var errors = response.getError();
                if (errors.length > 0) {
                    if (errors[0] != null && errors[0].message != null) {
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "type":"error",
                            "title": "Error",
                            "message": errors[0].message
                        });
                        resultsToast.fire();
                        $A.get("e.force:refreshView").fire();
                    }else if(errors[0] != null && errors[0].pageErrors[0] != null && errors[0].pageErrors[0].message != null){
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "type":"error",
                            "title": "Error",
                            "message": errors[0].pageErrors[0].message
                        });
                        resultsToast.fire();
                        $A.get("e.force:refreshView").fire();
                    }
                } else {
                    console.log("Unknown error");
                }

            }
        });
        $A.enqueueAction(action);
    }
})
