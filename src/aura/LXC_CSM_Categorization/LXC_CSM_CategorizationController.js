({
    doInit: function (component, event, helper) {
        helper.getRecord(component);
    },

    onControllingFieldChange: function (component, event, helper) {
        var record = component.get("v.simpleRecord");
        if (record != undefined) {
            helper.getSubtype(component, 1);
            if (record.RecordTypeId == component.get("v.RDActivityPlanRecordTypeId")) helper.getTemplate(component);
        }

    },

    onProductChange: function (component, event, helper) {
        helper.getSubtype1_v2(component);
    },

    onSubType1Change: function (component, event, helper) {
        helper.getSubtype(component, 2);
    },

    onSubType2Change: function (component, event, helper) {
        helper.getSubtype(component, 3);
    },

    closeForm: function (component, event, helper) {
        helper.closeForm(component);
    },

    openForm: function (component, event, helper) {
        helper.openForm(component);
    },

    onSubmit: function (component, event, helper) {
        var record = component.get("v.simpleRecord");
        if ((component.get("v.isMandatory") == true)
            && ((record.RecordTypeId != component.get("v.DataRecordTypeId") && component.find("controllingField").get("v.value") == "Please Specify")
                || (component.find("subType1").get("v.value") == "Please Specify")
                || (component.find("subType2").get("v.value") == "Please Specify")
                || (record.RecordTypeId == component.get("v.DataRecordTypeId") && component.find("subType3").get("v.value") == "Please Specify")
            )) {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "type": "error",
                "title": "Categorization",
                "message": "Please fill all fields in the categorization form."
            });
            resultsToast.fire();
        } else if ((record.RecordTypeId == component.get("v.RDActivityPlanRecordTypeId")) && (record.ContactId == undefined)) {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "type": "error",
                "title": "Categorization",
                "message": "Please select a Contact Name."
            });
            resultsToast.fire();
        } else helper.updateObjectCategorization(component);
    },

    hideErrors: function (component, event, helper) {
        component.set("v.errors", null);
    }
})