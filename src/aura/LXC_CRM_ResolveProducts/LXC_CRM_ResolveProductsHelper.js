({
    createSearchResultWrapper : function(component, event, helper, resolveProducts) {
        component.set("v.showResult", false);
        var resultObject = [];
        var headerApiList = component.get("v.headerApiList");
        for(var i = 0; i < resolveProducts.length; i++) {
            var rowItems = [];
            for(var j = 0; j < headerApiList.length; j++) {
                var dataValue = resolveProducts[i]["productRecord"][headerApiList[j]];
                if(dataValue == undefined) {
                    dataValue = "";
                }
                rowItems.push({
                    data : dataValue
                });
            }
            resultObject.push({
                dataColumns : rowItems,
                quantity : 1,
                id : resolveProducts[i]["productRecord"]["Id"]
            });
        }
        component.set("v.resultObjectWrapper", resultObject); 
        component.set("v.showResult", true);
    },
    
    addProducts : function(component, event, helper) {
        var selectedProductIdMap = new Map();
        var selectedValues = [];
        var checkboxCompList = component.find("checkbox-input");
        var quantityCompList = component.find("quantity");
        if (checkboxCompList != undefined && checkboxCompList != null && checkboxCompList.length == undefined) {
            var checkboxCompArr = [];
            checkboxCompArr.push(checkboxCompList);
            checkboxCompList = checkboxCompArr;
        }
        if(quantityCompList != undefined && quantityCompList != null && quantityCompList.length == undefined) {
            var quantityCompArr = [];
            quantityCompArr.push(quantityCompList);
            quantityCompList = quantityCompArr;
        }
        for(var i = 0; i < checkboxCompList.length; i++) {
            var checkBoxComp = checkboxCompList[i].getElements()[0];
            if(checkboxCompList[i].isRendered() && checkBoxComp.checked) {
                var quantityVal = quantityCompList[i].get("v.value");
                if(quantityVal == undefined) {
                    quantityVal = 1;
                } else if(quantityVal <= 0 || quantityVal > 15) {
                    var toastMessage = [];
                    toastMessage.push("Please input quantity between 1 - 15");
                    helper.setToast(component, event, helper, toastMessage, "error", "Error!");
                    var appEvent = $A.get("e.c:LXE_CRM_SpinnerEvent");
                    appEvent.setParams({"action" : "stop"});
                    appEvent.fire();
                    return;
                }
                var selectedId = checkBoxComp.id.split("-")[0];
                selectedProductIdMap.set(selectedId, quantityVal);
            }
        }
        
        if(selectedProductIdMap.size == 0) {
            var toastMessage = [];
            toastMessage.push("Please Select atleast one Product");
            helper.setToast(component, event, helper, toastMessage, "error", "Error!");
            var appEvent = $A.get("e.c:LXE_CRM_SpinnerEvent");
            appEvent.setParams({"action" : "stop"});
            appEvent.fire();
            return;
        }
        var resultWrapper = component.get("v.resolveProducts");
        for(var i = 0; i < resultWrapper.length; i++) {
            if(selectedProductIdMap.has(resultWrapper[i]["productRecord"]["Id"])) {
                for(var j = 0; j < selectedProductIdMap.get(resultWrapper[i]["productRecord"]["Id"]); j++) {
                    selectedValues.push(resultWrapper[i]);
                }
            }
        }
        var addOrEditProduct = $A.get("e.c:LXE_CRM_AddOrEditProductEvent");
        addOrEditProduct.setParams({
            "fieldsToDisable" : "",
            "fieldsToShow" : "UnitPrice,Revenue_Type__c,Sale_Type__c,Delivery_Country__c,Therapy_Area__c,Product_Start_Date__c,Product_End_Date__c,Product_SalesLead__c",
            "title" : "Add Product Page",
            "selectedList" : selectedValues,
            "isFadeIn" : true,
            "operationType" : "create"
        });
        addOrEditProduct.fire();
        var resolveProductEvent = $A.get("e.c:LXE_CRM_ResolveProductEvent");
        var resolveLineItem = component.get("v.resolveLineItem");
        resolveProductEvent.setParams({
            "resolveLineItem" : resolveLineItem,
            "action" : "resolve",
            "actionType" : component.get("v.actionType"),
            "screen" : component.get("v.resolveScreen")
        });
        resolveProductEvent.fire();
    },
    
    setToast : function(component, event, helper, message, type, title) {
        var errorMsg = message;
        var msg = "";
        for(var x = 0; x < errorMsg.length; x++) {
            msg = msg + errorMsg[x] + "\n";
        }
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            type : type,
            message : msg,
            mode : "sticky"
        });
        toastEvent.fire();
    },
    
    getFavoriteProduct : function(component, event, helper) {
        var action = component.get("c.getFavoriteProducts");
        action.setParams({
            "oppId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var pbeWrapperList = response.getReturnValue();
                component.set("v.resolveProducts", pbeWrapperList);
                component.set("v.actionType", "change");
                component.set("v.showFavoriteProduct", true);
                helper.createSearchResultWrapper(component, event, helper, pbeWrapperList);
            } else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var err = JSON.parse(errors[0].message).errorList;
                        helper.setToast(component, event, helper, err, "error", "Error!");
                    }
                }
            }
        })
        $A.enqueueAction(action);
    },
})