({
	doInit : function(component, event, helper) {
        component.set("v.cssStyle", "<style>.cuf-scroller-outside {background: rgb(255, 255, 255) !important;}</style>");
        helper.callServer(component, "c.getCaseRecord",function(response){
            var caseRecord = response[0];
            component.set('v.caseRecord',caseRecord);
        },{"recordId": component.get("v.recordId")},true);
        
        helper.callServer(component, "c.getCaseTechnoTaskList",function(response){
            var taskList = [];
            for (var i=0; i<response.length; i++) {
                if(response[i].label == "Resolution plan provided" || response[i].label =="Case update provided"){
                    taskList.push({name: response[i].label,value: response[i].value});    
                }else if(component.get("v.caseRecord").Case_Type__c == "Incident" && component.get("v.caseRecord") !== undefined && component.get("v.caseRecord").SubType1__c !== undefined && component.get("v.caseRecord").SubType1__c.includes("Incident") && (response[i].label == "RCA Delivered" || response[i].label == "RCA Requested")){
                    taskList.push({name: response[i].label,value: response[i].value});
                }
                
            }
            
            component.set('v.casetaskList',taskList);
        },{"recordId": component.get("v.recordId")},true);
	},
    saveTask : function(component, event, helper) {
        event.preventDefault();
        if(component.get("v.sltCaseTask") == ""){
            helper.showToastmsg(component,"Error","error", "Please select Task");
        }else{
            helper.callServer(component, "c.saveTasktoCreate",function(response){
                $A.get('e.force:refreshView').fire();
            },{"updateObject" : component.get("v.caseRecord"),"recordId": component.get("v.recordId"), "selectTask" : component.get("v.sltCaseTask")},true);    
        }
        // }else if(component.get('v.caseRecord').Status !== undefined && component.get('v.caseRecord').Status === 'Resolved with Customer' ){
        //    helper.callServer(component, "c.saveTasktoCreate",function(response){
        //        $A.get('e.force:refreshView').fire();
        //    },{"updateObject" : component.get("v.caseRecord"),"recordId": component.get("v.recordId"), "selectTask" : component.get("v.sltCaseTask")},true);    
        //}else{
        //    helper.showToastmsg(component,"Error","error", "Status should be Resolved with Customer");
        //}
        
    }
    
})