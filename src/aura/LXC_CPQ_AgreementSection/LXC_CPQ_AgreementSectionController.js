({
    doInit : function(component, event, helper) {
        var action = component.get("c.getAction");
        var approverData1 = component.get("c.getApproverData");
        var recordId = component.get("v.recordId");

        action.setParams({
            'recordId': recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.actionType", returnValue);
            }
        });

        approverData1.setParams({
            'recordId': recordId
        });
        approverData1.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.TSSUList", returnValue[0]);
                component.set("v.AccountsAndSalesList", returnValue[1]);
                component.set("v.PLList", returnValue[2]);
            }
        });
        $A.enqueueAction(action);
        $A.enqueueAction(approverData1);
    },
    openComposeEmail : function(component, event, helper) {
        component.set("v.isOpen", true);
    },
    openInvite : function(component, event, helper) {
        component.set("v.isInvite", true);
    },
    ClosePopUp: function(component, event, helper) {
        component.set("v.isInvite", false);
    }
})