({
    createRecord : function(component, event, helper) {
        var oppID =  component.get("v.selectedOppId");
        var recordTypeId = component.get("v.pageReference").state.recordTypeId;
        var OpportunityId = component.get("v.selectedOppId");
        if(OpportunityId=="")
        {
            var actionValidateRecordType = component.get("c.validateRecordType");
            actionValidateRecordType.setParams({
                "recordTypeId" : recordTypeId,
            });
            actionValidateRecordType.setCallback(this, function(actionResult) {
                var state = actionResult.getState();
                if (state === "SUCCESS") {
                    if(actionResult.getReturnValue())
                    {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Opportunity cannot be blank for this record type."
                        });
                        toastEvent.fire();  
                    }
                    else
                    {
                        var createRecordEvent = $A.get("e.force:createRecord");
                        createRecordEvent.setParams({
                            "entityApiName": "Apttus__APTS_Agreement__c",
                            "recordTypeId": recordTypeId,
                            "panelOnDestroyCallback": function(event) {
                            }
                        });
                        createRecordEvent.fire();  
                    }
                    
                }
            });
             $A.enqueueAction(actionValidateRecordType);
        }
        else
        {
            var validateAction = component.get("c.validateOpportunity");
            var recordTypeName = component.get("c.getRecorTypeName");
            var action = component.get("c.getDefaultFieldValues");
            recordTypeName.setParams({
                "recordTypeId" : recordTypeId,
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState();
                if (state === "SUCCESS") {
                    component.set("v.defaultValues",actionResult.getReturnValue());
                    console.log(component.get("v.defaultValues")); 
                    var defaultVal = component.get("v.defaultValues") ;
                    var createRecordEvent = $A.get("e.force:createRecord");
                    createRecordEvent.setParams({
                        "entityApiName": "Apttus__APTS_Agreement__c",
                        "recordTypeId": recordTypeId,
                        "defaultFieldValues": JSON.parse(defaultVal),
                        "panelOnDestroyCallback": function(event) {
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({
                                "recordId": oppID,
                            });
                            navEvt.fire();
                        }
                    });
                    createRecordEvent.fire();
                }
            });
            recordTypeName.setCallback(this, function(actionResult) {
                var state = actionResult.getState();
                if (state === "SUCCESS") {
                     recordTypeId = actionResult.getReturnValue();
                    validateAction.setParams({
                		"opportunityId" : OpportunityId,
                		"recordTypeId" : recordTypeId,
            			});
                    /*if(name == 'RFI Request' || name == 'Clinical Bid')
                    {
                         var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "Please select Bid short-form record type (Clinical / RFI)"
                        });
                        toastEvent.fire();
                    }
                    else
                    { */
                      $A.enqueueAction(validateAction);  
                   // }
                }
            });
            validateAction.setCallback(this, function(actionResult) {
                var state = actionResult.getState();
                if (state === "SUCCESS") {
                    var hasError =actionResult.getReturnValue();
                    if(hasError == false)
                    {
                        action.setParams({
                		"opportunityId" : OpportunityId,
                		"recordTypeId": recordTypeId,
            			});
                        $A.enqueueAction(action);
                    }
                    else
                    {
                    if(hasError == true) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "This bid is not allowed to be created on the current stage of Opportunity"
                        });
                        toastEvent.fire();
                    } 
                }
                }
            });            
            $A.enqueueAction(recordTypeName);  
        }
    },
    closeRecord : function(component, event, helper) {
        var url = window.location.href; 
        var value = url.substr(0,url.lastIndexOf('/') + 1);
        window.history.back();
        return false;
    }
    
})