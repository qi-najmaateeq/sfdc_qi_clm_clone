({
    doInit: function(component, event, helper) {
        component.set("v.showSpinner", true); 
        var action = component.get("c.getEmailTemplate");
        action.setParams({
            'recordId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = [];
                 storeResponse = response.getReturnValue();
                component.set("v.subject", storeResponse[0]);
                component.set("v.body", storeResponse[1]);
                component.set("v.showSpinner", false); 
            }
        });
        $A.enqueueAction(action);
    },
	sendInvite : function(component, event, helper) {
		var Attendees = component.get("v.Attendees");
		var subject = component.get("v.subject");
		var body = component.get("v.body");
		var fromDate = component.get("v.fromDate");
		var toDate = component.get("v.toDate");
        component.set("v.showSpinner", true); 
        
        var action = component.get("c.sendInviteEmail");
        action.setParams({
            'attendeesEmail': Attendees,
            'subject': subject,
            'bodyData' : body,
            'fromDate' : fromDate,
            'toDate': toDate
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {       
                component.set("v.inviteStatus", true);
            }else if (state === "ERROR") {
                var errors = response.getError();
            }
 
        });
        $A.enqueueAction(action);
	},
    
    ClosePopUp: function(component, event, helper) {
        component.set("v.hidePopUp", false);
    }
})