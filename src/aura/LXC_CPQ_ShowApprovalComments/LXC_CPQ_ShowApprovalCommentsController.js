({
    doInit : function(component, event, helper) {
        if(component.get("v.agreementRecord").RecordType.Name == 'FDTN – Initial Bid' || 
            component.get("v.agreementRecord").RecordType.Name == 'FDTN – Rebid' ){

            component.set("v.title","Approval Comments");
        }else if(component.get("v.agreementRecord").RecordType.Name == 'FDTN – Change Order'  ||
            component.get("v.agreementRecord").RecordType.Name == 'FDTN – CNF'  ||
            component.get("v.agreementRecord").RecordType.Name == 'FDTN – Contract'  ){
                
            component.set("v.title","Project Manager Comments");
        }
    }
})