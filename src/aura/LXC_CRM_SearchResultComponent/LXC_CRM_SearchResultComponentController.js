({
    addProduct : function(component, event, helper) {
        var appEvent = $A.get("e.c:LXE_CRM_SpinnerEvent").setParams({"action" : "start"}).fire();
        var selectedProductIdMap = new Map();
        var selectedValues = []; 
        var checkboxCompList = component.find("checkbox-input");
        var quantityCompList = component.find("quantity");
        if (checkboxCompList != undefined && checkboxCompList.length == undefined) {
            var checkboxCompArr = [];
            checkboxCompArr.push(checkboxCompList);
            checkboxCompList = checkboxCompArr;
        }
        if(quantityCompList != undefined && quantityCompList.length == undefined) {
            var quantityCompArr = [];
            quantityCompArr.push(quantityCompList);
            quantityCompList = quantityCompArr;
        }
        for(var i = 0; i < checkboxCompList.length; i++) {
            var checkBoxComp = checkboxCompList[i].getElements()[0];
            if(checkboxCompList[i].isRendered() && checkBoxComp.checked) {
                var quantityVal = quantityCompList[i].get("v.value");
                if(quantityVal == undefined) {
                    quantityVal = 1;
                } else if(quantityVal <= 0 || quantityVal > 15) {
                    var toastMessage = [];
                    toastMessage.push("Please input quantity between 1 - 15");
                    helper.setToast(component, event, helper, toastMessage, "error", "Error!");
                    var appEvent = $A.get("e.c:LXE_CRM_SpinnerEvent");
                    appEvent.setParams({"action" : "stop"});
                    appEvent.fire();
                    return;
                }
                var selectedId = checkBoxComp.id.split("-")[0];
                selectedProductIdMap.set(selectedId, quantityVal);
            }
        }
        
        if(selectedProductIdMap.size == 0) {
            var toastMessage = [];
            toastMessage.push("Please Select atleast one Product");
            helper.setToast(component, event, helper, toastMessage, "error", "Error!");
            var appEvent = $A.get("e.c:LXE_CRM_SpinnerEvent");
            appEvent.setParams({"action" : "stop"});
            appEvent.fire();
            return;
        }
        var resultWrapper = component.get("v.SearchResultWrapper");
        for(var i = 0; i < resultWrapper.length; i++) {
            if(selectedProductIdMap.has(resultWrapper[i]["productRecord"]["Id"])) {
                for(var j = 0; j < selectedProductIdMap.get(resultWrapper[i]["productRecord"]["Id"]); j++) {
                    selectedValues.push(resultWrapper[i]);
                }
            }
        }
        component.set("v.selectedValues", selectedValues);
        var addOrEditProduct = $A.get("e.c:LXE_CRM_AddOrEditProductEvent");
        addOrEditProduct.setParams({
            "fieldsToDisable" : "",
            "fieldsToShow" : "UnitPrice,Revenue_Type__c,Sale_Type__c,Delivery_Country__c,Therapy_Area__c,Product_Start_Date__c,Product_End_Date__c,Product_SalesLead__c",
            "title" : "Add Product Page",
            "selectedList" : component.get("v.selectedValues"),
            "isFadeIn" : true,
            "operationType" : "create",
            "territory" : component.get("v.territory")
        });
        addOrEditProduct.fire();
    },
    
    callEvent : function(component, event, helper) {
        component.set("v.timeStamp", new Date().getTime());
        component.isReloadEvent = false;
        helper.getFavoriteProducts(component, event, helper);
    },
    
    callReloadEvent : function(component, event, helper) {
        var eventFor = event.getParams().eventFor;
        if(eventFor == "deSelectProducts") {
            var checkboxCompList = component.find("checkbox-input");
            var quantityCompList = component.find("quantity");
            if (checkboxCompList != undefined && checkboxCompList.length == undefined) {
                var checkboxCompArr = [];
                checkboxCompArr.push(checkboxCompList);
                checkboxCompList = checkboxCompArr;
            }
            if (quantityCompList != undefined && quantityCompList.length == undefined) {
                var quantityCompArr = [];
                quantityCompArr.push(quantityCompList);
                quantityCompList = quantityCompArr;
            }
            if(checkboxCompList != undefined && checkboxCompList != null) {
                for(var i = 0; i < checkboxCompList.length; i++) {
                    var checkBoxComp = checkboxCompList[i].getElements()[0];
                    if(checkboxCompList[i].isRendered())
                        checkBoxComp.checked = false;
                } 
            }
            if(quantityCompList != undefined && quantityCompList != null) {
                for(var i = 0; i < quantityCompList.length; i++) {
                    if(quantityCompList[i].isRendered())
                        quantityCompList[i].set("v.value", 1);
                } 
            }
        } else if(eventFor == "searchResultFavoriteProduct") {
            component.isReloadEvent = true;
            helper.getFavoriteProducts(component, event, helper);
        }
    },
    
    addFavoriteProduct : function(component, event, helper) {
        component.action = "create";
        helper.modifyFavoriteProduct(component, event, helper);
    },
    
    deleteFavoriteProduct : function(component, event, helper) {
        component.action = "Delete";
        helper.modifyFavoriteProduct(component, event, helper);
    },
    
    setQuantityValue : function(component, event, helper) {
        var quantity = event.getSource().get("v.value");
        var productId = event.getSource().get("v.label");
        var quantityMap = component.get("v.quantityWrapper");
        quantityMap.set(productId, quantity);
    }
});