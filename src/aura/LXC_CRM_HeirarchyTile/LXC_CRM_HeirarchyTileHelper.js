({
    setNextLayerProducts: function(component, event, helper) {
        var action = component.get("c.getProductDetails");
        action.setParams({
            "currentChain" : component.get("v.currentChain"),
        });
        if(component.get("v.currentLayer") != "1") {
            var selecteNode = event.currentTarget;
            var selectedClass = selecteNode.className;
            var productIndex = event.target.getAttribute("data-index");
            var productRecords = component.get("v.productRecords");
            var searchFieldValueList = [];
            var fieldsAPIList = component.get("v.fieldsAPIList");
            for(var count in fieldsAPIList) {
                var fieldValue = productRecords[productIndex][fieldsAPIList[count]];
                searchFieldValueList.push(fieldsAPIList[count]);
                searchFieldValueList[fieldsAPIList[count]] = fieldValue;
            }
            var appEvent = $A.get("e.c:LXE_CRM_PopulateFieldSearchProductEvent");
            appEvent.setParams({"searchFieldValueList" : searchFieldValueList});
            appEvent.setParams({"fieldsAPIList" : fieldsAPIList});
            appEvent.fire();
            var elements = component.find("link");
            if(elements.length != undefined || elements.length > 0) {
                for (var index = 0; index < elements.length; index++) {    
                    elements[index].getElements()[0].classList.remove("active");
                }   
            }
            $A.util.addClass(selecteNode, "active");
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(component.get("v.currentLayer") == "1") {
                    component.set("v.productRecords", response.getReturnValue());
                }
                var appEvent = $A.get("e.c:LXE_CRM_ProductSelectedEvent");
                var prodList = response.getReturnValue();
                if(prodList == undefined || prodList.length <= 0){
                    var spans = component.find("expandArrow");
                    var productList = component.find("link");
                    if(spans != null && spans.length == undefined) {
                        var selectedspan1 = spans;
                        $A.util.addClass(selectedspan1, 'slds-hide');
                    }
                    if(spans != null && spans.length != undefined) {
                        var selectedIndex = 1;
                        for (var index = 0; index < productList.length; index++) {
                            var sourceProduct = productList[index];
                            if(productList[index].getElements()[0].id == event.target.getAttribute("data-chain")) { 
                                selectedIndex = index;
                            }
                        }
                        var selectedspan = spans[selectedIndex];
                        $A.util.addClass(selectedspan, 'slds-hide');
                    }
                }
                appEvent.setParams({"productRecords" : response.getReturnValue()});
                appEvent.setParams({"currentChain" : component.get("v.currentChain")});
                appEvent.setParams({"currentLayer" : component.get("v.currentLayer")});
                appEvent.fire();
            } else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var errorMsg = JSON.parse(errors[0].message).errorList;
                        helper.setToast(component, event, helper, errorMsg, "error", "Error!");
                        $A.get("e.c:LXE_CRM_SpinnerEvent").setParams({"action" : "stop"}).fire();
                    }
                }
            }
        });
        $A.enqueueAction(action);
    }
})