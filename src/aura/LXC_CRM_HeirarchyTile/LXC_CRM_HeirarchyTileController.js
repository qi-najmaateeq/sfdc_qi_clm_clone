({
    doInit: function(component, event, helper) {
        if(component.get("v.isParent")) {
            $A.get("e.c:LXE_CRM_SpinnerEvent").setParams({"action" : "start"}).fire();
            var currentChain = '';
            component.set("v.currentLayer", "1");
            component.set("v.currentChain", currentChain);
            helper.setNextLayerProducts(component, event, helper);
            $A.get("e.c:LXE_CRM_SpinnerEvent").setParams({"action" : "stop"}).fire();
        }
    },
    SetCurrentValue: function(component, event, helper) { 
        $A.get("e.c:LXE_CRM_SpinnerEvent").setParams({"action" : "start"}).fire();
        var currentChain = event.target.getAttribute("data-chain") + "->";
        component.set("v.currentLayer", (currentChain.match(/->/g)).length + 1);
        component.set("v.currentChain", currentChain);
        helper.setNextLayerProducts(component, event, helper);
    }
})