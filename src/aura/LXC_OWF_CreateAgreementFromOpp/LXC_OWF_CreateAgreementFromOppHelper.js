({
	getRecordTypes : function(component, event, helper) {
		var action = component.get("c.getEligibleRecordTypes");
        var OpportunityId = component.get("v.recordId");
        var oppFields = component.get("v.oppFields");
        action.setParams({
            "opportunityId" : OpportunityId,
            "oppFields" : oppFields
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                component.set("v.bidTypes",actionResult.getReturnValue());             
            }
       });
       $A.enqueueAction(action);
   },
    getOppDetails : function(component, event, helper) {
		var action = component.get("c.getAccountId");
        var OpportunityId = component.get("v.recordId");
        var oppFields = component.get("v.oppFields");
        action.setParams({
            "opportunityId" : OpportunityId,
            "oppFields" : oppFields
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                component.set("v.accountId",actionResult.getReturnValue());             
            }
       });
       $A.enqueueAction(action);
   },
 usingMap : function(component, event, helper) {       
  var action = component.get("c.getEligibleRecordTypes");
        action.setCallback(this,function(response){
            var values = [];
            var result = response.getReturnValue();
            for(var key in result){
                values.push({
                    class:'optionClass',
                    label:key.bidHeader+key.bidDescription,
                    value:key.typeId});         
            }
            component.set("v.optionsList",values);
        });
        $A.enqueueAction(action);
 }
})