({
	getValidationMessages : function(component, event, helper) {
        var opportunityId = component.get("v.opportunityId");
        var action = component.get("c.getPricingErrors");
        action.setParams({
            "opportunityId" : opportunityId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                debugger;
                var result = response.getReturnValue();
                console.log('result',result);
                if(result != null) { 
                    var errorMessages = JSON.parse(result['errorMessages']);
                    component.set('v.isLoading', false);
                    if(errorMessages.length > 0) {
                        component.set("v.showError",true);
                        component.set("v.arrayOfErrorMessage", errorMessages);                
                    } else { 
                        window.open('https://'+result['legacyOrgLink']+'/apex/VFP_PA_Pricing_Calculator_Opp_Screen?OppId='+result['legacyOrgOppId'],'_top');
                    }
                }
           }else if (state === "ERROR") {
               console.log('Error :: ',response.getError());
           }
       });
       $A.enqueueAction(action);
	}
})