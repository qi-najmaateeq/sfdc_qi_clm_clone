({
	doInit : function(component, event, helper) { 
        component.find("pricingTool").set("v.value","CPQ_QIP");
        var opportunityNumber= component.get("v.simpleRecord").Apttus__Related_Opportunity__r.Legacy_Quintiles_Opportunity_Number__c;
        if(opportunityNumber!='')
        {            
            component.set("v.opportunityNumber", opportunityNumber);
        }
        component.set("v.showSpinner",true);
		var opportunityId=component.get("v.simpleRecord").Apttus__Related_Opportunity__c;
        var action = component.get("c.getLegacyOppNo");
                        action.setParams({ 
                            "oppId" : opportunityId
                        });
                        action.setCallback(this, function(response) {                            
        					var responseList = [];
                            var response2;
                            var state2 = response.getState();
                            if (state2 === "SUCCESS") {
                                responseList = response.getReturnValue();
                               // alert('responseList[0]'+responseList[0]);
                                //alert('responseList[1]'+responseList[1]);
                                component.set("v.name",responseList[0]);
                                component.set("v.owner",responseList[1]);
                                //component.find("ownerIdDisplay").set("v.value", responseList[1]);
                                component.set("v.showSpinner", false);                              
                            }
                        }); 
        
        $A.enqueueAction(action);
        var a = component.get('c.pricingToolChange');
        $A.enqueueAction(a);
	},
    
    pricingToolChange : function(component, event, helper){
    	var pricingTool=component.find("pricingTool").get("v.value");
        if(pricingTool != '') {
            var action1 = component.get("c.getPricingTool");
                        action1.setParams({ 
                            "pricingTool" : pricingTool
                        });
                        action1.setCallback(this, function(response) {
                            var storeResponse = [];
                            var state2 = response.getState();
                            if (state2 === "SUCCESS") {
                                storeResponse = response.getReturnValue();
                              	component.set("v.recordTypeId",storeResponse[0]);
                              	component.set("v.processStep", storeResponse[1]);
                            }
                        });         
        $A.enqueueAction(action1);
        } 
	},
    
    handleSuccess: function(component, event, helper) { 
        $A.get('e.force:refreshView').fire();
    }
})