({
    doInit: function(component, event, helper) {      
        helper.getOpportunityRecord(component,event, helper);
        document.addEventListener("scroll", function() {
            helper.scrollEventCall();
        });
    },
    
    gotoURL : function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "isredirect" : true
        });
        navEvt.fire();
    },
})