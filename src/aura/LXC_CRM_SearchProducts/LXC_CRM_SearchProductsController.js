({  
    doInit : function(component, event, helper) {
        helper.getProductSearchInitData(component, event, helper);
    },
    
    filterProducts : function(component, event, helper) {
        helper.searchfilterProducts(component, event, helper);
    },
    
    clearFilter : function(component, event, helper) {
        helper.clearDefaultFilter(component, event, helper);
    },
    
    saveFilter : function(component, event, helper) {
        helper.saveDefaultFilter(component, event, helper);
    },
    
    hideErrors : function(component, event, helper) {
        component.set("v.errors", null);
    },
    
    searchResolvedProduct : function(component, event, helper) {
        var params = event.getParams();
        if(params.action != "search") {
            return;
        }
        helper.searchResolvedProduct(component, event, helper, params);
    },
    
    hideResolveProduct : function(component, event, helper) {
        helper.hideResolveProduct(component, event, helper);
    },
    
    setSearchedField : function(component, event, helper) {
        var params = event.getParams();
        var defaultProductFilterObj = component.get("v.defaultProductFilterObj");
        var fieldsAPIList = params.fieldsAPIList;
        for(var count in fieldsAPIList) {
            defaultProductFilterObj["Default_"+fieldsAPIList[count]] = params.searchFieldValueList[fieldsAPIList[count]] ;
        }
        component.set("v.defaultProductFilterObj", defaultProductFilterObj);
    },
})