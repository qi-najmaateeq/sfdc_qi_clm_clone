({
    doInit :  function(component, event, helper) {
        var childComp = component.find('popupComponent');
        childComp.set("v.showSpinner", false);
        component.set('v.oldProcessStep', component.get("v.simpleRecord").Process_Step__c);  
        component.set("v.showSpinner", false);
        component.set("v.isShowQcButton",false);
        if(component.get("v.simpleRecord").Process_Step__c == 'Final QC'){
            component.set("v.showSpinner", true);
            helper.checkIfUserInQCGroup(component);
        }
    },
    onsubmit : function(component, event, helper) {
        
        component.set("v.showErrorMessage", false);
        component.set('v.newProcessStep', event.getParam("fields").Process_Step__c);
        component.set('v.showModal', true); 
        var childComp = component.find('popupComponent');
        childComp.set("v.showSpinner", true);
        if(component.get("v.oldProcessStep") == 'None' && component.get("v.newProcessStep") == 'PL Review'){
            if(!component.get("v.simpleRecord").Project_Manager_Email__c) {
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage",'Please Provide Project Manager Name');
            }else{
                component.set("v.showErrorMessage", false);
            }
        } 
        childComp.showMessage();    
    },
    showQCSelfCheckTable: function (component, event, helper) {

        component.set('v.oldProcessStep', component.get("v.simpleRecord").Process_Step__c);  
        component.set('v.newProcessStep', '');
        var childComp = component.find('popupComponent');
        childComp.showMessage();    
    },
    onerror : function(component, event, helper) {
        
        var childComp = component.find('popupComponent');
        childComp.set("v.showSpinner", false);
    },
})