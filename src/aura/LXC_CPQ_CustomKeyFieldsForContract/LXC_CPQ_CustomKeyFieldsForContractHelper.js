({
    checkIfUserInQCGroup : function(component) {
        var action  = component.get("c.checkIfCurrentUserInQCGroup");
            action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isShowQcButton",response.getReturnValue());
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
    }
})