({
    getFieldValidated : function(component, event, helper, isFromEdit) {
        var action = component.get("c.getOpportunityStageRecord");
        var recordId = component.get("v.recordId");
        action.setParams({
            recordId : recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set("v.opportunityStageRecord", returnValue);
                    helper.getRequiredFields(component, event, helper, false);
                }else if(isFromEdit){
                    helper.getRequiredFields(component, event, helper, false);
                }else{
                    helper.getLIGDetails(component, event, helper);
                    helper.getRequiredFields(component, event, helper, false);
                }
            }else if(state === "ERROR") {
                var errors = response.getError();
                var err = JSON.parse(errors[0].message).errorList;
                helper.setToast(component, event, helper, err, "error", "Error!");
            }
        })
        $A.enqueueAction(action);
    },
    handleChange : function(component, event, helper) {
        component.set("v.showSpinner", true);
        var ligFields = component.get("v.ligFields");
        var localId = event.getSource().getLocalId();
        var inputField = '';
        var inputFieldError = '';
        var str = '';
        var selectedValue = event.getSource().get("v.value");
        inputField = component.find(localId);
        str = localId+'_Error';
        inputFieldError = component.find(str);
        if(selectedValue != undefined && selectedValue != '' && selectedValue != null) {
            $A.util.removeClass(inputField,"slds-has-error");
            $A.util.addClass(inputFieldError, "slds-hide");
        }else{
            $A.util.removeClass(inputFieldError,"slds-hide");
            $A.util.addClass(inputField, "slds-has-error");
        }
        component.set("v.showSpinner", false);
    },
    getRequiredFields : function(component, event, helper, isFromSubmit) {
        var action = component.get("c.getListOfRequiredFields");
        var recordId = component.get("v.recordId");
        var errorList = component.get("v.errors");
        var expectedStage;
        var expectedLineOfBuisness;
        if(component.get("v.opportunityStageRecord[Expected_Opp_Stage__c]") != null){
            expectedStage = component.get("v.opportunityStageRecord[Expected_Opp_Stage__c]");
        } else {
            expectedStage = component.get("v.child.StageName");
        }
        if(component.get("v.opportunityStageRecord[Expected_Line_of_Buisness__c]") != null){
            expectedLineOfBuisness = component.get("v.opportunityStageRecord[Expected_Line_of_Buisness__c]");
        } else {
            expectedLineOfBuisness = component.get("v.child.Line_of_Business__c");
        }
        action.setParams({
            recordId : recordId,
            expectedStage : expectedStage,
            expectedLineOfBuisness : expectedLineOfBuisness
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                var ligFieldList = component.get("v.ligFields");
                var rdProductCount = component.get("v.child.RD_Product_Count__c");
                if(returnValue != null && returnValue.length > 0) {
                    for(var i = 0; i < returnValue.length; i++){
                        ligFieldList.push(returnValue[i]);
                    }
                    ligFieldList.push("Phase__c");
        			ligFieldList.push("Indication__c");
                    if (rdProductCount > 0) {
                    	ligFieldList.push("Global_project_unit__c");
                    }    
                }
                if(ligFieldList.length > 0){
                    component.set("v.ligFields", ligFieldList);
                    var elementEdit= document.getElementById('editableDiv');
                    elementEdit.style.display = 'block';
                    var elementView = document.getElementById('viewableDiv');
                    elementView.style.display = 'none';
                    component.set("v.saved", false);
                    var ligFields = component.get("v.ligFields");
                    var inputField = '';
                    var inputFieldError = '';
                    var str = '';
                    var isValidated = true;
                    for(var i = 0; i < ligFields.length; i++) {
                        inputField = component.find(ligFields[i]);
                        str = ligFields[i]+'_Error';
                        inputFieldError = component.find(str);
                        if(inputField != null && inputField.get('v.value') == undefined || inputField.get('v.value') == '' || inputField.get('v.value') == null) {
                            $A.util.removeClass(inputFieldError, "slds-hide");
                            inputField.set('v.class', "slds-has-error");
                            isValidated = false;
                            component.set("v.showSpinner", false);
                            var fieldLabel = ligFields[i].replace(/_/g, ' ').substring(0, ligFields[i].length-3);
                            errorList.push(fieldLabel);
                        }
                    }
                    if(isFromSubmit && isValidated) {
                        var fspField = component.find("FSP__c");
                        var fspFieldError = component.find("FSP_BD_Lead__c_Error");
                        var fspBDLeadField = component.find("FSP_BD_Lead__c");
                        var fspBDLeadFieldUI = component.find("FSP_BD_Lead__c_UI");
                        var fspFuncField = component.find("FSP_Function__c");
                        var fspFuncFieldError = component.find("FSP_Function__c_Error");
                        var fspDurField = component.find("FSP_Duration__c");
                        var fspDurFieldError = component.find("FSP_Duration__c_Error");
                        if(fspField.get('v.value') == "Yes"){
                            if(fspBDLeadField.get('v.value') == undefined || fspBDLeadField.get('v.value') == '' || fspBDLeadField.get('v.value') == null){
                                $A.util.removeClass(fspFieldError, "slds-hide");
                                fspBDLeadFieldUI.set('v.class', "slds-has-error");
                                isValidated = false;
                                component.set("v.showSpinner", false);
                                errorList.push("FSP BD Lead");
                            } else {
                                $A.util.removeClass(fspBDLeadFieldUI, "slds-has-error");
                                $A.util.addClass(fspFieldError, "slds-hide");
                            }
                            if(fspFuncField.get('v.value') == undefined || fspFuncField.get('v.value') == '' || fspFuncField.get('v.value') == null){
                                $A.util.removeClass(fspFuncFieldError, "slds-hide");
                                fspFuncField.set('v.class', "slds-has-error");
                                isValidated = false;
                                component.set("v.showSpinner", false);
                                errorList.push("FSP Function");
                                var ligFieldList = component.get("v.ligFields");
                                ligFieldList.push("FSP_Function__c");
                                component.set("v.ligFields", ligFieldList);
                            } else {
                                $A.util.removeClass(fspFuncField, "slds-has-error");
                                $A.util.addClass(fspFuncFieldError, "slds-hide");
                            }
                            if(fspDurField.get('v.value') == undefined || fspDurField.get('v.value') == '' || fspDurField.get('v.value') == null){
                                $A.util.removeClass(fspDurFieldError, "slds-hide");
                                fspDurField.set('v.class', "slds-has-error");
                                isValidated = false;
                                component.set("v.showSpinner", false);
                                errorList.push("FSP Duration");
                                var ligFieldList = component.get("v.ligFields");
                                ligFieldList.push("FSP_Duration__c");
                                component.set("v.ligFields", ligFieldList);
                            } else {
                                $A.util.removeClass(fspDurField, "slds-has-error");
                                $A.util.addClass(fspDurFieldError, "slds-hide");
                            }
                        }
                        if(component.find("Phase__c").get("v.value") == undefined || component.find("Phase__c").get("v.value") == '' || component.find("Phase__c").get("v.value") == null){
                            $A.util.removeClass(component.find("Phase__c_Error"), "slds-hide");
                            component.find("Phase__c").set('v.class', "slds-has-error");
                            isValidated = false;
                            component.set("v.showSpinner", false);
                            errorList.push("Phase");
                        }
                        if(component.find("Indication__c").get("v.value") == undefined || component.find("Indication__c").get("v.value") == '' || component.find("Indication__c").get("v.value") == null){
                            $A.util.removeClass(component.find("Indication__c_Error"), "slds-hide");
                            component.find("Indication__c").set('v.class', "slds-has-error");
                            isValidated = false;
                            component.set("v.showSpinner", false);
                            errorList.push("Indication");
                        }
                        if((component.find("Global_project_unit__c").get("v.value") == undefined || component.find("Global_project_unit__c").get("v.value") == '' || component.find("Global_project_unit__c").get("v.value") == null) && rdProductCount > 0){
                            $A.util.removeClass(component.find("Global_project_unit__c_Error"), "slds-hide");
                            component.find("Global_project_unit__c").set('v.class', "slds-has-error");
                            isValidated = false;
                            component.set("v.showSpinner", false);
                            errorList.push("Global Project Unit");
                        }
                        if(isValidated) {
                            event.preventDefault();
                            component.find("editForm").submit();
                        }
                    }
                } else {
                    if(isFromSubmit) {
                        var fspField = component.find("FSP__c");
                        var fspFieldError = component.find("FSP_BD_Lead__c_Error");
                        var fspBDLeadField = component.find("FSP_BD_Lead__c");
                        var fspBDLeadFieldUI = component.find("FSP_BD_Lead__c_UI");
                        var fspFuncField = component.find("FSP_Function__c");
                        var fspFuncFieldError = component.find("FSP_Function__c_Error");
                        var fspDurField = component.find("FSP_Duration__c");
                        var fspDurFieldError = component.find("FSP_Duration__c_Error");
                        if(fspField.get('v.value') == "Yes") {
                            if(fspBDLeadField.get('v.value') == undefined || fspBDLeadField.get('v.value') == '' || fspBDLeadField.get('v.value') == null){
                                $A.util.removeClass(fspFieldError, "slds-hide");
                                fspBDLeadFieldUI.set('v.class', "slds-has-error");
                                isValidated = false;
                                component.set("v.showSpinner", false);
                                errorList.push("FSP BD Lead");
                            } else {
                                $A.util.removeClass(fspBDLeadFieldUI, "slds-has-error");
                                $A.util.addClass(fspFieldError, "slds-hide");
                            }
                            if(fspFuncField.get('v.value') == undefined || fspFuncField.get('v.value') == '' || fspFuncField.get('v.value') == null){
                                $A.util.removeClass(fspFuncFieldError, "slds-hide");
                                fspFuncField.set('v.class', "slds-has-error");
                                isValidated = false;
                                component.set("v.showSpinner", false);
                                errorList.push("FSP Function");
                            } else {
                                $A.util.removeClass(fspFuncField, "slds-has-error");
                                $A.util.addClass(fspFuncFieldError, "slds-hide");
                            }
                            if(fspDurField.get('v.value') == undefined || fspDurField.get('v.value') == '' || fspDurField.get('v.value') == null){
                                $A.util.removeClass(fspDurFieldError, "slds-hide");
                                fspDurField.set('v.class', "slds-has-error");
                                isValidated = false;
                                component.set("v.showSpinner", false);
                                errorList.push("FSP Duration");
                            } else {
                                $A.util.removeClass(fspDurField, "slds-has-error");
                                $A.util.addClass(fspDurFieldError, "slds-hide");
                            }
                        }
                        if(isValidated) {
                            component.find("editForm").submit();
                        }
                    }
                    if(!isFromSubmit) {
                        helper.getLIGDetails(component, event, helper);
                    }
                }
            }else if(state === "ERROR") {
                var errors = response.getError();
                var err = JSON.parse(errors[0].message).errorList;
                helper.setToast(component, event, helper, err, "error", "Error!");
                component.set("v.showSpinner", false);
            }
            if(errorList.length > 0 ){
                component.set("v.isError", true);
                var con = component.get("v.errors");
                var uniqueObjs = {};
                //create a object with unique leads
                con.forEach(function(conItem){
                    uniqueObjs[conItem] = conItem;
                });
                //reinitialise con array
                con = [];
                var keys = Object.keys(uniqueObjs);
                //fill up con again
                keys.forEach(function(key){
                    con.push(uniqueObjs[key]);
                });
                component.set("v.errors", con);
            }
        })
        $A.enqueueAction(action);
    },
    
    getLIGDetails : function(component, event, helper) {
		var action = component.get("c.getLIGRecordDetail");
        var ligFieldList = ["Id", "Opportunity__c","Indication__c","Indication__r.Name","Therapy_Area__c","Verbatim_Indication_Term__c", "Phase__c", "Population_Age_Group__c",
                        "Drug_Product_Name__c","Potential_Services__c", "FPI_Date__c", "RFP_Received_Date__c", "Proposal_Due_Date__c","Proposal_Sent_Date__c","Inside_Sales_Transfer_Date__c",
                        "Expected_Project_Start_Date__c","Expected_Project_End_Date__c","Total_Project_Awarded_Price__c","Total_Signed_Contract_Price__c","Inside_Sales__c", "Inside_Sales__r.Name",
                        "Financial_Check__c","Executive_Sponsor__c","Business_Area__c","Proposal_Lead__c","Binary_Decision__c","Global_project_unit__c","FSP__c","Is_this_a_renewal__c",
                        "FSP_BD_Lead__c","FSP_BD_Lead__r.Name","FSP_Duration__c","FSP_Function__c","Anticipated_RFP_Date__c","Protocol_Number__c","Q2_Budget_Tool__c","Q2_CTMS_Picklist__c","Test_Type__c",
                        "Will_Contract_have_a_fixed_price__c","Extension_Continuation_Part_of_Progam__c","Does_study_include_LNS__c","GPM_to_be_located_in_specific_region__c",
                        "Q2_Contract_Currency__c","QLIMS_Project_Code__c","Efficacy_Parameters__c","Research_Setting__c","Real_World_Study_Population__c","Real_World_Study_Design__c",
                        "Real_World_Secondary_Data_Source__c","Real_World_Primary_Data_Source__c","Outcome_of_Efficacy_Study__c","Type_Trial__c","Sub_Indication__c","Outcome__c","Exposure__c",
                        "Product_Special_Considerations__c","Customer_Strategic_Goal_s__c","Research_Study_Classification__c","Real_World_Categories__c","Phase_1_Study_Type__c","Other_Phase_1_Study_Type__c",
                        "Patient_Type__c","Predictive_Biomarker__c","Exploratory_Predictive_Biomarker_Testing__c","Predictive_Biomarker_Patient_Selection__c","COE_Assigned__c","Potential_Assignment_Role__c","RWE_Study__c","MD_D__c",
                        "Is_this_opportunity_a_potential_BCOI__c","AstraZeneca_Operational_Model__c","Sub_Organization_PL_Use_Only__c","Cardiac_Safety_ECG_Equipment__c","Cote_Orphan_Contract_turn_around_time__c",
                        "Cote_Orphan_Contract_Type__c","Kits_on_Site_Date__c","GSK_Competitive_Bid__c","Project_Type__c","Q2_supporting_BD__c","Proposal_Lead__c","Startup_Months__c","Follow_up_Months__c","Enrollment_Rate__c","Minutes_per_Page_US__c","BD_Comments__c","Active_Months__c","Closeout_Months__c",
                        "Direct_Cost_per_Patient__c","Minutes_per_Page_EU__c","Other_Patient_Type__c","BD_Lead_Sub_Region__c","Evidence_Strategy_Lead__c","Qualification_Tier__c","Real_World_Data_Source__c","Real_World_Study_Direction__c","Evidence_Strategy_Lead__r.Full_User_Name__c","Is_this_IQVIA_Biotech__c","Care_Setting__c","Total_Project_Awarded_Price_USD__c","Project_Manager__c"] ;
        action.setParams({
            oppId : component.get("v.recordId"),
            ligFieldList : ligFieldList
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.LIG", response.getReturnValue());
                component.set("v.showSpinner", false);
            } else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var errorMsg = JSON.parse(errors[0].message).errorList;
                        console.log(errorMsg);
                    }
                }
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
	},
    getManagerUserDetail : function(component, event, helper) {
        var action = component.get("c.getManagerUserDetail");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var perList = response.getReturnValue();
                component.set("v.isSalesManagerUser", perList[0]);
                component.set("v.hasInsideSalePermission", perList[1]);
            } else if(state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var errorMsg = JSON.parse(errors[0].message).errorList;
                        console.log(errorMsg);
                    }
                }
            }
        });
        $A.enqueueAction(action);
	},
})