({
    recordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED" || (eventParams.changeType === "CHANGED" && !component.get("v.preventSubmit"))) {
            var bdLead = component.get("v.child.Line_Item_Group__r.FSP_BD_Lead__c");
            component.set("v.BDLeadValue", bdLead);
            component.set("v.showSpinner", true);
            helper.getFieldValidated(component, event, helper, false);
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
        } else{
            component.set("v.preventSubmit", false);
        }
    },
    navigateToOpp : function(component, event, helper) {
        component.set("v.saved", true);
        var ligFields = [];
        component.set("v.ligFields", ligFields);
        component.set("v.saved", true);
        var bdLead = component.get("v.child.Line_Item_Group__r.FSP_BD_Lead__c");
        component.set("v.BDLeadValue", bdLead);
        var elementEdit= document.getElementById('editableDiv');
        elementEdit.style.display = 'none';
        //component.find("RecordEdit").reloadRecord(true);
        var elementView = document.getElementById('viewableDiv');
        elementView.style.display = 'block';
        component.set("v.showSpinner", false);
    },
    showEditPannel : function(component, event, helper) {
        var ligFields = component.get("v.ligFields");
        var rdProductCount = component.get("v.child.RD_Product_Count__c");
        ligFields.push("Phase__c");
        ligFields.push("Indication__c");
        if (rdProductCount > 0) {
        	ligFields.push("Global_project_unit__c");
        }
        component.set("v.ligFields", ligFields);
        var elementEdit = document.getElementById('editableDiv');
        elementEdit.style.display = 'block';
        var elementView = document.getElementById('viewableDiv');
        elementView.style.display = 'none';
        helper.getFieldValidated(component, event, helper, true);
    },
    showViewPannel : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "isredirect" : false
        });
        navEvt.fire();
    },
    doInit : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.getManagerUserDetail(component, event, helper);
    },
    handleCloneCancel : function(component, event, helper) {
        helper.getFieldValidated(component, event, helper, false);
    },
    errorInformation : function(component, event, helper) {
        var eventName = event.getName();
        var eventDetails = event.getParams("fieldErrors");
        component.set("v.showSpinner", false);
        var err = JSON.stringify(eventDetails);
        var lst = err.split("\"fieldLabel\":\"");
        var errorList = component.get("v.errors");
        for(var i = 1; i < lst.length; i++){
            errorList.push(lst[i].substr(0,lst[i].indexOf('\",')));
        }
        if(errorList.length > 0 ){
            component.set("v.isError", true);
            var con = errorList;
            var uniqueObjs = {};
            //create a object with unique leads
            con.forEach(function(conItem){
                uniqueObjs[conItem] = conItem;
            });
            //reinitialise con array
            con = [];
            var keys = Object.keys(uniqueObjs);
            //fill up con again
            keys.forEach(function(key){
                con.push(uniqueObjs[key]);
            });
            component.set("v.errors", con);
        }
    },
    handleChange : function(component, event, helper) {
        component.set("v.isError", false);
        var localId = event.getSource().getLocalId();
        var ligFields = component.get("v.ligFields");
        var callOnChangeHanler = false;
        for(var i = 0; i < ligFields.length; i++) {
            if(ligFields[i] == localId) {
                callOnChangeHanler = true;
            }
        }
        if(callOnChangeHanler) {
            helper.handleChange(component, event, helper);
        }
    },
    onSubmit : function(component, event, helper) {
        var SelectedBdLead = component.find("FSP_BD_Lead__c_UI");
        component.set("v.errors", []);
        component.set("v.isError", false);
        var ActualbdLead = component.find("FSP_BD_Lead__c");
        ActualbdLead.set("v.value", SelectedBdLead.get("v.value"));
        component.set("v.showSpinner", true);
        helper.getRequiredFields(component, event, helper, true);
    }
})