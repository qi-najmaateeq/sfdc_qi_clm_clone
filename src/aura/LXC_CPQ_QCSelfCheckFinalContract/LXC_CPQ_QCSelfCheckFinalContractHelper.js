({
    saveProposalQASelfCheckListRecords : function(component, isSaveAndComplete) {
        component.set("v.Spinner", true);
        var action = component.get("c.saveProposalQASelfCheckList");
        action.setParams({ 
            "proposalQASelfCheckListWrapperListString" : JSON.stringify(component.get("v.proposalQASelfCheckListWrapperList")),
            "agreementId" : component.get("v.recordId"),
            "processStep" : component.get("v.processStep"),
            "isSaveAndComplete" : isSaveAndComplete,
            "overAllComments" : component.get("v.overAllComments")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    }, 
    fetchPicklistValueForFields : function(component, fieldName) {
        component.set("v.Spinner", true);
        var action = component.get("c.fetchPicklistValue");
        action.setParams({ 
            "fieldName" : fieldName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Spinner", false);
                if(fieldName == 'Response__c')
                    component.set("v.responseList",response.getReturnValue());
                else if(fieldName == 'Reviewer_response__c')
                    component.set("v.reviewerResponseList",response.getReturnValue());
                else if(fieldName == 'Reviewer_confirm_major_findings_correcte__c')
                    component.set("v.reviewerMajorFindingList",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})