({
    loadGuidelines : function(component, event, helper) {

        helper.fetchPicklistValueForFields(component, "Response__c");
        helper.fetchPicklistValueForFields(component, "Reviewer_response__c");
        helper.fetchPicklistValueForFields(component, "Reviewer_confirm_major_findings_correcte__c");
        if(component.get("v.processStep") == 'Bid Grid (Maintenance) Completion'){
            component.set("v.processStep","QC Self Check Final"); 
        }
        component.set("v.Spinner", true);
        var action = component.get("c.fetchQCCheckListRecords");
        action.setParams({ 
            "agreementId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Spinner", false);
                component.set("v.proposalQASelfCheckListWrapperList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);	
        if(component.get("v.processStep") == 'Final QC'){
            var action1 = component.get("c.fetchOverAllComments");
            action1.setParams({ 
                "agreementId" : component.get("v.recordId")
            });
            action1.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.overAllComments", response.getReturnValue());
                }
            });
            $A.enqueueAction(action1);
        }
    },
    saveData : function(component, event, helper) {

        component.set("v.isErrorMessage",false);
        var proposalQASelfCheckListWrapperList = component.get("v.proposalQASelfCheckListWrapperList")
        for(var cmp in proposalQASelfCheckListWrapperList){
            if(proposalQASelfCheckListWrapperList[cmp].isNew && (!proposalQASelfCheckListWrapperList[cmp].proposalQASelfCheckList.Question__c 
                || !proposalQASelfCheckListWrapperList[cmp].proposalQASelfCheckList.Guidelines__c )){
                component.set("v.isErrorMessage",true);
                return;
            }
        }
        var parentPopUp = component.get("v.parent");
        parentPopUp.set("v.isMessage", true);
        parentPopUp.set('v.isOpen', false);
        parentPopUp.set("v.oldProcessStep",component.get("v.processStep"));
        parentPopUp.updateProcessStepForQcCheck();
        helper.saveProposalQASelfCheckListRecords(component, false);
    },
    saveAndCompleteData : function(component, event, helper) {
        
        component.set("v.isErrorMessage",false);
        var proposalQASelfCheckListWrapperList = component.get("v.proposalQASelfCheckListWrapperList")
        for(var cmp in proposalQASelfCheckListWrapperList){
            if(proposalQASelfCheckListWrapperList[cmp].isNew && (!proposalQASelfCheckListWrapperList[cmp].proposalQASelfCheckList.Question__c ||
                !proposalQASelfCheckListWrapperList[cmp].proposalQASelfCheckList.Guidelines__c )){
                component.set("v.isErrorMessage",true);
                return;
            }
        }
        var parentPopUp = component.get("v.parent");
        parentPopUp.set("v.oldProcessStep",component.get("v.processStep"));
        helper.saveProposalQASelfCheckListRecords(component, true);
        if(component.get("v.processStep") == 'Final QC'){
            parentPopUp.set("v.isFinalQcComplete", true);
            parentPopUp.set("v.newProcessStep","");
            $A.get('e.force:refreshView').fire();
            parentPopUp.set('v.isOpen', false);
            return;
        }else{
            parentPopUp.set("v.newProcessStep","Final QC");
            parentPopUp.set("v.iscompleteQCSelfCheckDraft", true);
        }
        parentPopUp.set("v.isMessage", true);
        parentPopUp.showMessage(component, event, helper);
    },
    addMoreItem : function(component, event, helper) {
        
        component.set("v.Spinner", true);
        var action = component.get("c.addMoreItemInList");
        action.setParams({ 
            "proposalQASelfCheckListWrapperListString" : JSON.stringify(component.get("v.proposalQASelfCheckListWrapperList")),
            "agreementId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
        var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Spinner", false);
                component.set("v.proposalQASelfCheckListWrapperList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);		
    },
    deleteItem : function(component, event, helper) {
        
        component.set("v.Spinner", true);
        var index = event.getSource().get("v.name")
        var proposalQASelfCheckListWrapperList = component.get("v.proposalQASelfCheckListWrapperList");
        proposalQASelfCheckListWrapperList.splice(index, 1);
        component.set("v.proposalQASelfCheckListWrapperList", proposalQASelfCheckListWrapperList);
        component.set("v.Spinner", false);
    },
})