<apex:page standardController="Apttus__APTS_Agreement__c" showHeader="false" sidebar="false" title="Clone Agreement" extensions="CNT_CPQ_ContractDetailsOnAgreement">
    <apex:includeLightning />
    <apex:slds />
    <head>
        <style>
           .contract_table .td_heading{
                text-align: right !important;
                width:13%;
                padding-left : 0px !important;
            }
            .contract_table{
                width: auto !important;
                white-space: normal;
                text-overflow: ellipsis;
                overflow: hidden;
            }
            .contract_table td{
                border-bottom: solid 1px lightgrey;
                font-size:12px;
                padding-bottom: 15px !important;
            }
        </style>
    </head>
    <table class="slds-m-left_small slds-table slds-table_cell-buffer contract_table">   
        <apex:outputpanel layout="none" rendered="{!Apttus__APTS_Agreement__c.RecordType.Name == 'FDTN – CNF'}">
            <tr class="slds-p-top_medium">
                <td class="td_heading">Contract Owner</td>
                <td><apex:outputField value="{!contract.OwnerId}"/></td>
                <td class="td_heading">Contract Record Type</td>
                <td><apex:outputField value="{!contract.RecordType.Name}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Contract Number</td>
                <td><apex:outputField value="{!contract.ContractNumber}"/></td>
                <td class="td_heading">Specific Contract Type</td>
                <td><apex:outputField value="{!contract.Specific_Contract_Type__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Parent Contract</td>
                <td><apex:outputField value="{!contract.Parent_Contract__c}"/></td>
                <td class="td_heading">Client/Third Party Agreement</td>
                <td><apex:outputField value="{!contract.Client_Third_Party_Agreement__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Ultimate Parent Contract Number</td>
                <td><apex:outputField value="{!contract.Ultimate_Parent_Contract_Number__c}"/></td>
                <td class="td_heading">Sequential Reference</td>
                <td><apex:outputField value="{!contract.Sequential_Reference__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Account Name</td>
                <td><apex:outputField value="{!contract.AccountId}"/></td>
                <td class="td_heading">Contract Name</td>
                <td><apex:outputField value="{!contract.Name}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Other Opportunities</td>
                <td><apex:outputField value="{!contract.Other_Opportunities__c}"/></td>
                <td class="td_heading">Division, Business Unit</td>
                <td><apex:outputField value="{!contract.Division_Business_Unit__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Project Number</td>
                <td><apex:outputField value="{!contract.Project_Number__c}"/></td>
                <td class="td_heading">Is this Contract a Ballpark?</td>
                <td><apex:outputField value="{!contract.Is_this_Contract_a_Ballpark__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Protocol Number</td>
                <td><apex:outputField value="{!contract.Protocol_Number__c}"/></td>
                <td class="td_heading">Lead Business Group</td>
                <td><apex:outputField value="{!contract.Lead_Business_Group__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Negotiating Office</td>
                <td><apex:outputField value="{!contract.Negotiating_Office__c}"/></td>                                                    
            </tr>
        	<tr>
                <td class="td_heading">Date Tracking Started</td>
                <td><apex:outputField value="{!contract.Date_Tracking_started__c}"/></td>                                                       
        	</tr>
            <tr>
                <td class="td_heading">Contract Ranking</td>
                <td><apex:outputField value="{!contract.Contract_Ranking__c}"/></td>                                                     
            </tr>
            <tr>
                <td class="td_heading">Change Order Number</td>
                <td><apex:outputField value="{!contract.Change_Order_Number__c}"/></td>                                                     
            </tr>
            <tr>
                <td class="td_heading">Is the CNF Accepted?</td>
                <td><apex:outputField value="{!contract.Is_the_CNF_Accepted__c}"/></td>                                                     
            </tr>
            <tr>
                <td class="td_heading">Confidence in Approval of Budget Draft</td>
                <td><apex:outputField value="{!contract.Confidence_in_Approval_of_Budget_Draft__c}"/></td>                                                      
            </tr>
        </apex:outputpanel>
        <apex:outputpanel layout="none" rendered="{!Apttus__APTS_Agreement__c.RecordType.Name != 'FDTN – CNF'}">
            <tr class="slds-p-top_medium">
                <td class="td_heading">Contract Number</td>
                <td><apex:outputField value="{!contract.ContractNumber}"/></td>
                <td class="td_heading">Contract Record Type</td>
                <td><apex:outputField value="{!contract.RecordType.Name}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Parent Contract</td>
                <td><apex:outputField value="{!contract.Parent_Contract__c}"/></td>
                <td class="td_heading">Specific Contract Type</td>
                <td><apex:outputField value="{!contract.Specific_Contract_Type__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Parent Contract Number</td>
                <td><apex:outputField value="{!contract.Parent_Contract_Number__c}"/></td>
                <td class="td_heading">Client/Third Party Agreement</td>
                <td><apex:outputField value="{!contract.Client_Third_Party_Agreement__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Ultimate Parent Contract Number</td>
                <td><apex:outputField value="{!contract.Ultimate_Parent_Contract_Number__c}"/></td>
                <td class="td_heading">Sequential Reference</td>
                <td><apex:outputField value="{!contract.Sequential_Reference__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Account Name</td>
                <td><apex:outputField value="{!contract.AccountId}"/></td>
                <td class="td_heading">Contract Name</td>
                <td><apex:outputField value="{!contract.Name}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Legal Entity (Customer)</td>
                <td><apex:outputField value="{!contract.Legal_Entity_Customer__c}"/></td>
                <td class="td_heading">Lead Business Group</td>
                <td><apex:outputField value="{!contract.Lead_Business_Group__c}"/></td>                
            </tr>
            <tr>
                <td class="td_heading">Legal Entity (IQVIA)</td>
                <td><apex:outputField value="{!contract.Legal_Entity_Quintiles__c}"/></td>
                <td class="td_heading">Division, Business Unit</td>
                <td><apex:outputField value="{!contract.Division_Business_Unit__c}"/></td>                
            </tr>
            <tr>
                <td class="td_heading">Opportunity Number</td>
                <td><apex:outputField value="{!contract.Opportunity_Number__c}"/></td>
                <td class="td_heading">Is this Contract a Ballpark?</td>
                <td><apex:outputField value="{!contract.Is_this_Contract_a_Ballpark__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Opportunity Name</td>
                <td><apex:outputField value="{!contract.Opportunity_Name__c}"/></td>                                                       
            </tr>
            <tr>
                <td class="td_heading">Opportunity Link</td>
                <td><apex:outputField value="{!contract.Opportunity_Link__c}"/></td>
            </tr>
            <tr>
                <td class="td_heading">Other Opportunities</td>
                <td><apex:outputField value="{!contract.Other_Opportunities__c}"/></td>                                                       
        	</tr>
        	<tr>
                <td class="td_heading">Project Number</td>
                <td><apex:outputField value="{!contract.Project_Number__c}"/></td>                                                      
        	</tr>
            <tr>
                <td class="td_heading">Cost Point Project Code</td>
                <td><apex:outputField value="{!contract.Cost_Point_Project_Code__c}"/></td>                                                       
            </tr>
            <tr>
                <td class="td_heading">Protocol Number</td>
                <td><apex:outputField value="{!contract.Protocol_Number__c}"/></td>                                                       
            </tr>
            <tr>
                <td class="td_heading">Drug/Product Name</td>
                <td><apex:outputField value="{!contract.Drug_Product_Name__c}"/></td>                                                       
            </tr>
            <tr>
                <td class="td_heading">Therapy Area</td>
                <td><apex:outputField value="{!contract.Therapy_Area__c}"/></td>                                                       
            </tr>
            <tr>
                <td class="td_heading">Global Project Unit</td>
                <td><apex:outputField value="{!contract.Global_Project_Unit__c}"/></td>                                                       
            </tr>
            <tr>
                <td class="td_heading">Delivery Unit</td>
                <td><apex:outputField value="{!contract.Delivery_Unit__c}"/></td>                                                       
            </tr>
            <tr>
                <td class="td_heading">Data Transfer Agreement Included?</td>
                <td><apex:outputField value="{!contract.Data_Transfer_Agreement_included__c}"/></td>                                                       
            </tr>
            <tr>
                <td class="td_heading">Negotiating Office</td>
                <td><apex:outputField value="{!contract.Negotiating_Office__c}"/></td>                                                    
            </tr>
        	<tr>
                <td class="td_heading">Date Tracking Started</td>
                <td><apex:outputField value="{!contract.Date_Tracking_started__c}"/></td>                                                       
        	</tr>
            
        </apex:outputpanel>                
    </table>
</apex:page>