/**
 * Author : Shubham Jain
 * Created Date : 26-03-2019
 * This is OpportunityTeamMember Domain handler class.
 */
public class DAOH_OpportunityTeamMember {
    
    /**
     * This method is used to verify Analytics Products
     * @param List<OpportunityTeamMember> oldList
     * @returns void 
     */ 
    public static void verifyPrivacyAnalyticsProducts(List<OpportunityTeamMember> oldList) {
        Map<Id, List<OpportunityTeamMember>> oppIdToOTMMap = new Map<Id, List<OpportunityTeamMember>>();
        Set<String> paProductCodeSet = new Set<String>();
        Set<Id> paGroupMembersSet = new Set<Id>();
        Set<Id> oppIdSet = new Set<Id>();
        List<GroupMember> groupMemberList = [SELECT UserOrGroupId FROM GroupMember WHERE Group.Name =: CON_CRM.GROUP_PLATFORM_ANALYTICS_SALES_GROUP];
        for (GroupMember gm : groupMemberList) {
            paGroupMembersSet.add(gm.UserOrGroupId);
        }
        for(OpportunityTeamMember otm: oldList) {
            if(oppIdToOTMMap.get(otm.OpportunityId) == null) {
                oppIdToOTMMap.put(otm.OpportunityId, new List<OpportunityTeamMember>());
            } 
            oppIdToOTMMap.get(otm.OpportunityId).add(otm);      
        }
        List<PrivacyAnalyticsProductCode__c> paProductCodeList = PrivacyAnalyticsProductCode__c.getall().values();
        for (PrivacyAnalyticsProductCode__c papc : paProductCodeList) {
            paProductCodeSet.add(papc.Product_Code__c);
        }
        for (OpportunityTeamMember otm : oldList) {
            oppIdSet.add(otm.OpportunityId);
        }
        Set<String> fieldSet = new Set<String>{'Id', 'OpportunityId'};
        List<OpportunityLineItem> oliList = new SLT_OpportunityLineItems().getOLIByOppIdAndProductCode(oppIdSet, fieldSet, paProductCodeSet);
        for (OpportunityLineItem oli: oliList) {
            for(OpportunityTeamMember otm :oppIdToOTMMap.get(oli.OpportunityId)) {
                if(paGroupMembersSet.contains(otm.UserId)) {
                	otm.addError('Cannot remove PA team member from Opportunity with PA products');
                }
            }
        }
    }
    
}