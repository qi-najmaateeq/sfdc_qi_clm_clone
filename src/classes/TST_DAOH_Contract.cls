@isTest
private class TST_DAOH_Contract {
    
    /**
    * This method is used to setup data for all methods.
    */
    @testSetup
    static void dataSetup() { 
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Stage_Gate_Status_Values__c statusvalue = new Stage_Gate_Status_Values__c();
        statusvalue.Name = 'AnalystPP';
        statusvalue.Status__c = 'Analyst Preparing Customer Budget Deliverable';
        upsert statusvalue;
        IQVIA_Legal_Entity__c iqviaLegalEntity = UTL_TestData.createIQVIALegalEntity();
        insert iqviaLegalEntity;
        Contract cntrt = new Contract();
        cntrt.AccountId = acc.Id;
        cntrt.Status = 'Draft';
        cntrt.CurrencyIsoCode = 'INR';
        cntrt.Status = 'Analyst Preparing Customer Budget Deliverable';
        cntrt.StartDate = System.today();
        cntrt.ContractTerm = 12;
        cntrt.Parent_Contract_Number__c = 1234;
        cntrt.Ultimate_Parent_Contract_Number__c = 5678;
        cntrt.Legal_Entity_IQVIA__c = iqviaLegalEntity.Id;
        cntrt.Legal_Entity_Customer__c = 'Test';
        insert cntrt;
        Opportunity testOpp = UTL_TestData.createOpportunity(acc.id);
        insert testOpp;
        Contract parentContract = UTL_TestData.createContract(testOpp, 'CNF_GBO');
        parentContract.Parent_Contract_Number__c = 1234;
        parentContract.Ultimate_Parent_Contract_Number__c = 5678;
        parentContract.Legal_Entity_IQVIA__c = iqviaLegalEntity.Id;
        parentContract.Legal_Entity_Customer__c = 'Test';
        insert parentContract;
    }
    
    /**
    * This method used to get User Metric By Id
    */    
    @IsTest
    static void testCreateContractLifeCycleRecord() {
        Test.startTest();
        Contract ctnrt = [Select Id from Contract LIMIT 1];
        update ctnrt;
        delete ctnrt;
        Test.stopTest();
    }
    
    /**
    * This method used to set ContractFields   (there are 3 methods test SetContractFields)
    */ 
    @IsTest
    static void testSetContractFields1() {
        Test.startTest();
        List<Id> recordTypesIds = new List<Id>{CON_CRM.CONTRACT_RECORD_TYPE_CNF_GBO,CON_CRM.CONTRACT_RECORD_TYPE_CHANGE_ORDER_GBO,CON_CRM.CONTRACT_RECORD_TYPE_GENERAL_CLIENT_AGREEMENT_GBO,CON_CRM.CONTRACT_RECORD_TYPE_PRELIMINARY_AGREEMENT_GBO};
        List<Contract> ctnrtList = [Select Id,Status,Update_Contract_Status__c,Unsigned_Status__c from Contract LIMIT 1];
        List<RecordType> recordTypList = [SELECT Id FROM RecordType where SobjectType = 'Contract' AND Id IN : recordTypesIds];
        if(recordTypList.size() > 0){
            ctnrtList[0].RecordTypeId = recordTypList[0].Id;
            ctnrtList[0].Unsigned_Status__c = CON_CRM.CONTRACT_STATUS_EXECUTED;
            update ctnrtList;
        }
        Test.stopTest();
        System.assertEquals(CON_CRM.CONTRACT_STATUS_EXECUTED,ctnrtList[0].Unsigned_Status__c);
    }
    
    /**
    * This method used to test set contract field method. 
    */ 
    @IsTest
    static void testSetContractFields2() {
        Test.startTest();
        List<Id> recordTypesIds = new List<Id>{CON_CRM.CONTRACT_RECORD_TYPE_CNF_GBO,CON_CRM.CONTRACT_RECORD_TYPE_CHANGE_ORDER_GBO,CON_CRM.CONTRACT_RECORD_TYPE_GENERAL_CLIENT_AGREEMENT_GBO,CON_CRM.CONTRACT_RECORD_TYPE_PRELIMINARY_AGREEMENT_GBO};
        List<Contract> ctnrtList = [Select Id,Status,Update_Contract_Status__c,Unsigned_Status__c from Contract LIMIT 1];
        List<RecordType> recordTypList = [SELECT Id FROM RecordType where SobjectType = 'Contract' AND Id IN : recordTypesIds];
        if(recordTypList.size() > 0){
            ctnrtList[0].RecordTypeId = recordTypList[0].Id;
            ctnrtList[0].Update_Contract_Status__c = CON_CRM.CONTRACT_STATUS_NEGOTIATION_TERMINATED;
            update ctnrtList;
            Test.stopTest();
        }
        System.assertEquals(CON_CRM.CONTRACT_STATUS_NEGOTIATION_TERMINATED,ctnrtList[0].Update_Contract_Status__c);
    }
    
    /**
    * This method used to test set contract field method. 
    */ 
    @IsTest
    static void testSetContractFields3() {
        Test.startTest();
        List<Id> recordTypesIds = new List<Id>{CON_CRM.CONTRACT_RECORD_TYPE_CNF_GBO,CON_CRM.CONTRACT_RECORD_TYPE_CHANGE_ORDER_GBO,CON_CRM.CONTRACT_RECORD_TYPE_GENERAL_CLIENT_AGREEMENT_GBO,CON_CRM.CONTRACT_RECORD_TYPE_PRELIMINARY_AGREEMENT_GBO};
        List<Contract> ctnrtList = [Select Id,Status,Update_Contract_Status__c,Unsigned_Status__c from Contract LIMIT 1];
        List<RecordType> recordTypList = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType = 'Contract' AND Id IN : recordTypesIds];
        if(recordTypList.size() > 0){
            ctnrtList[0].RecordTypeId = recordTypList[0].Id;
            ctnrtList[0].Update_Contract_Status__c = CON_CRM.CONTRACT_STATUS_NEGOTIATION_TERMINATED;
            update ctnrtList;
        } 
        Test.stopTest();
        System.assertEquals(CON_CRM.CONTRACT_STATUS_NEGOTIATION_TERMINATED,ctnrtList[0].Update_Contract_Status__c);
    }
    
    /**
    * This method used to test set contract field method. 
    */ 
    @IsTest
    static void testSetContractFields4() {
        Test.startTest();
        List<Id> recordTypesIds = new List<Id>{CON_CRM.CONTRACT_RECORD_TYPE_CNF_GBO,CON_CRM.CONTRACT_RECORD_TYPE_CHANGE_ORDER_GBO,CON_CRM.CONTRACT_RECORD_TYPE_GENERAL_CLIENT_AGREEMENT_GBO,CON_CRM.CONTRACT_RECORD_TYPE_PRELIMINARY_AGREEMENT_GBO};
        List<Contract> ctnrtList = [Select Id,Status,Update_Contract_Status__c,Unsigned_Status__c from Contract LIMIT 1];
        List<RecordType> recordTypList = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType = 'Contract' AND Id IN : recordTypesIds];
        if(recordTypList.size() > 0){
            ctnrtList[0].RecordTypeId = recordTypList[0].Id; 
            ctnrtList[0].Is_this_Contract_a_Ballpark__c = CON_CRM.CONTRACT_IS_A_BALLPARK_NO;
            ctnrtList[0].Confidence_in_Approval_of_Budget_Draft__c = CON_CRM.CONTRACT_CONFIDENCE_IN_APPROVAL_OF_BUDGET_DRAFT;
            ctnrtList[0].Actual_Contract_Value__c = CON_CRM.CONTRACT_ACTUAL_CONTRACT_VALUE;
            ctnrtList[0].Status = CON_CRM.CONTRACT_STATUS_CONTRACT_AT_CUSTOMER_FOR_REVIEW;
            ctnrtList[0].Project_Expenses__c = CON_CRM.CONTRACT_ACTUAL_CONTRACT_VALUE;
            update ctnrtList;
        } 
        Test.stopTest();
        System.assertEquals(CON_CRM.CONTRACT_ACTUAL_CONTRACT_VALUE, [Select Id,Status,Update_Contract_Status__c,Unsigned_Status__c,Initial_Draft_at_High_Confidence_Value__c from Contract where id= : ctnrtList[0].id].get(0).Initial_Draft_at_High_Confidence_Value__c);
    }
    
    /**
    * This method used to test set contract field method. 
    */ 
   @IsTest
    static void testSetParentContractFieldsOnCNF() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        IQVIA_Legal_Entity__c iqviaLegalEntity = [SELECT Id FROM IQVIA_Legal_Entity__c LIMIT 1];
        Contract testCNFGBOContract = new Contract();
        testCNFGBOContract.RecordTypeId = CON_CRM.CONTRACT_RECORD_TYPE_CNF_GBO;
        testCNFGBOContract.Project_Number__c = 'Test Project Number';
        testCNFGBOContract.Change_Order_Number__c = '12';
        testCNFGBOContract.Status = 'Analyst Preparing Customer Budget Deliverable';
        testCNFGBOContract.AccountId = acc.Id;
        testCNFGBOContract.Parent_Contract_Number__c = 1234;
        testCNFGBOContract.Ultimate_Parent_Contract_Number__c = 5678;
        testCNFGBOContract.Legal_Entity_IQVIA__c = iqviaLegalEntity.Id;
        testCNFGBOContract.Legal_Entity_Customer__c = 'Test';
        insert testCNFGBOContract;
        
        Contract changeOrderContract = new Contract();
        changeOrderContract.RecordTypeId = CON_CRM.CONTRACT_RECORD_TYPE_CHANGE_ORDER_GBO;
        changeOrderContract.Project_Number__c = 'Test Project Number';
        changeOrderContract.Change_Order_Number__c = '12';
        changeOrderContract.Status = 'Analyst Preparing Customer Budget Deliverable';
        changeOrderContract.AccountId = acc.Id;
        changeOrderContract.Parent_Contract_Number__c = 1234;
        changeOrderContract.Ultimate_Parent_Contract_Number__c = 5678;
        changeOrderContract.Legal_Entity_IQVIA__c = iqviaLegalEntity.Id;
        changeOrderContract.Legal_Entity_Customer__c = 'Test';
        Test.startTest();
            insert changeOrderContract;
        Test.stopTest();
    }
    
    /**
    * This method used to test parent contract.
    */ 
   /*
   @IsTest
    static void testParentContract() {
        Account acc = [select id from Account where Name = 'TestAccount'];
        Opportunity testOpp = [select id from Opportunity where accountId = :acc.id];
        Id devRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('CNF - GBO').getRecordTypeId();
        Contract parentContract = [select id from Contract where recordTypeId =: devRecordTypeId ][0];
        Contract cntrt = [select id from Contract where AccountId = :acc.Id and Parent_Contract__c = null][0];
        cntrt.Parent_Contract__c = parentContract.id;
        Test.startTest();
        update cntrt;
        Test.stopTest();     
    }
	*/
}