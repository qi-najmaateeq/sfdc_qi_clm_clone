@isTest
private class TST_SLT_ApprovalMatrix {

    static Approval_Matrix__c setApprovalMatrixData(){
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_PL, CON_CPQ.GENERAL);
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, CON_CPQ.OPPORTUNITY_RFP,
            CON_CPQ.JAPAN, CON_CPQ.ONCOLOGY, 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;
        return approvalMatrix;
    }
    
    @isTest
    static void testSelectApprovalMatrixById(){
        Approval_Matrix__c testApprovalMatrix  = setApprovalMatrixData();
        
        Test.startTest();
        	List<Approval_Matrix__c> approvalMatrixList = new SLT_ApprovalMatrix().selectById(new Set<Id>{testApprovalMatrix.Id});
        Test.stopTest();
    }
    
    @isTest
    static void testSelectApprovalMatrixCondition(){
        setApprovalMatrixData();
        
        Test.startTest();
        	List<Approval_Matrix__c> testApprovalMatrixList = new SLT_ApprovalMatrix().selectApprovalMatrixCondition(CON_CPQ.OPPORTUNITY_RFP, CON_CPQ.JAPAN);
        Test.stopTest();
    }
    
    @isTest
    static void testSelectApprovalMatrixByOpportunityType(){
        setApprovalMatrixData();
        
        Test.startTest();
        	List<Approval_Matrix__c> testApprovalMatrixList = new SLT_ApprovalMatrix().selectApprovalMatrixByOpportunityType(CON_CPQ.OPPORTUNITY_RFP);
        Test.stopTest();
    }
}