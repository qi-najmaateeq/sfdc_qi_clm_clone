public class CNT_CPQ_BidHistoryRelatedList {

    public static List<Bid_History__x> mockedBidHistorys = new List<Bid_History__x>();
    
    @AuraEnabled
    public static Bid_History__x fetchBidHistory(String agreementId){

        Set<Id> recordtypeIds = new Set<Id>{
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId(),
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId()};

        if(recordtypeIds.size() > 0){

            Set<String> fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.AGREEMENT_BId_HISTORY_NUMBER};
            List<Apttus__APTS_Agreement__c> agreements = new SLT_Agreement().getAgreementDetailsByIdAndRecorType(agreementId,
                                                            recordtypeIds, fieldSet);
            if(agreements.size() > 0 && !String.isBlank(agreements[0].Bid_History_Number_CPQ__c)){

                fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.NAME, CON_CPQ.DISPLAYURL,
                                            CON_CPQ.BID_HISTORY_BID_NUMBER, CON_CPQ.BID_HISTORY_QIP_LOADED};
                List<Bid_History__x> bidHistorys = new SLT_Bid_History_External().getBidHistoryUsingBidHistoryNumber(
                                                    agreements[0].Bid_History_Number_CPQ__c, fieldSet);
                if(bidHistorys.size() > 0 && !Test.isRunningTest() )
                    return bidHistorys[0];
                else if(mockedBidHistorys.size() > 0 && Test.isRunningTest())
                    return mockedBidHistorys[0];
            }
        }
        return new Bid_History__x();
    }
}