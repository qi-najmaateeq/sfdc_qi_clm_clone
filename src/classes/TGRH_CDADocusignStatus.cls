public class TGRH_CDADocusignStatus {
    static List < CDA_Request__c > cdaRequestList = new List < CDA_Request__c > ();
    static Map < String, CDA_Request__c > cdaRequestMapWithCDAId = new Map < String, CDA_Request__c > ();
    static Map < id, CDA_Request__c > cdaRequestUpdateMap = new Map < id, CDA_Request__c > ();
    
	public static void bulkBefore(Set<String> cdaIds) {
		String query = 'SELECT id, Name, CDA_Id__c, RecordTypeId FROM CDA_Request__c WHERE CDA_Id__c IN: cdaIds AND RecordType.Name != \''+UTL_CDAUtility.historicalDataRecordType+'\'';
        system.debug('####inside DAOH_DocusignStatus.bulkBefore query: '+query);
        cdaRequestList = Database.query(query);
        for (CDA_Request__c cdaRequest: cdaRequestList) {
            cdaRequestMapWithCDAId.put((String)cdaRequest.get('CDA_Id__c'), cdaRequest);
        }
    }
    
    public static void handleDocusignStatusBeforeInsert(fflib_SObjectUnitOfWork uow, List<dsfs__DocuSign_Status__c> newList, Set<String> cdaIds, Map<String, String> statusWithCDAId, Boolean toCommit) {
        for (dsfs__DocuSign_Status__c docusignStatus: newList) {
            if (statusWithCDAId != null && statusWithCDAId.size() > 0 && statusWithCDAId.containsKey(docusignStatus.id) && cdaRequestMapWithCDAId != null && cdaRequestMapWithCDAId.size() > 0 && cdaRequestMapWithCDAId.containsKey(statusWithCDAId.get(docusignStatus.Id)) && cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).RecordTypeId != Schema.SObjectType.CDA_Request__c.getRecordTypeInfosByName().get(UTL_CDAUtility.historicalDataRecordType).getRecordTypeId()) {   //Updated by Vikram Singh under CR-11691
                docusignStatus.CDA_Request__c = cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id;
                cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status__c = UTL_CDAUtility.STATUS_SENTFORSIGN;
                cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status_Start_Date__c = System.now(); //Added by Vikram Singh under Issue-11710
                if (cdaRequestUpdateMap != null && cdaRequestUpdateMap.containsKey(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id)) {
                    cdaRequestUpdateMap.get(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id).Status__c = UTL_CDAUtility.STATUS_SENTFORSIGN;
                    cdaRequestUpdateMap.get(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id).Status_Start_Date__c = System.now(); //Added by Vikram Singh under Issue-11710
                    toCommit = true;
                } else {
                    cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status__c = UTL_CDAUtility.STATUS_SENTFORSIGN;
                    cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status_Start_Date__c = System.now(); //Added by Vikram Singh under Issue-11710
                    cdaRequestUpdateMap.put(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id, cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)));
                    toCommit = true;
                }
            }
            if(toCommit) {
                uow.registerDirty(cdaRequestUpdateMap.values());
            }
        }
        system.debug('####inside DAOH_DocusignStatus.BeforeInsert uow: '+uow);
        if(toCommit) {
            uow.commitWork();
        }
        SRV_CDA_DocusignStatus.setVoidToDocusignEnvelope(cdaIds);
    }
    
    public static void handleDocusignStatusBeforeUpdate(List<dsfs__DocuSign_Status__c> newList, Boolean toCommit, fflib_SObjectUnitOfWork uow, Map<String, String> statusWithCDAId, Map<Id, dsfs__DocuSign_Status__c> oldMap) {
    	for (dsfs__DocuSign_Status__c docusignStatus: newList) {
            // dsfs__DocuSign_Status__c oldDocusignStatusObj = (dsfs__DocuSign_Status__c)trigger.oldMap.get(docusignStatus.Id);
            dsfs__DocuSign_Status__c oldDocusignStatusObj = oldMap.get(docusignStatus.Id);
            if(docusignStatus.dsfs__Envelope_Status__c == UTL_CDAUtility.DS_DECLINED && oldDocusignStatusObj.dsfs__Envelope_Status__c != UTL_CDAUtility.DS_DECLINED && statusWithCDAId != null && statusWithCDAId.size() > 0 && statusWithCDAId.containsKey(docusignStatus.id) && cdaRequestMapWithCDAId != null && cdaRequestMapWithCDAId.size() > 0 && cdaRequestMapWithCDAId.containsKey(statusWithCDAId.get(docusignStatus.Id)) && cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).RecordTypeId != Schema.SObjectType.CDA_Request__c.getRecordTypeInfosByName().get(UTL_CDAUtility.historicalDataRecordType).getRecordTypeId()) {   //Updated by Vikram Singh under CR-11691
                cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status__c = UTL_CDAUtility.STATUS_SIGNATURE_REQUEST_DENIED;
                cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Negotiator_Assigned_List__c = UTL_CDAUtility.NEGO_NOT_ASSIGN;
                cdaRequestUpdateMap.put(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id, cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)));
                toCommit = true;
            }
            if(toCommit) {
                uow.registerDirty(cdaRequestUpdateMap.values());
            }
        }
        system.debug('####inside DAOH_DocusignStatus.BeforeUpdate uow: '+uow);
        if(toCommit) {
            uow.commitWork();
        }
    }

    public static void cancelRequestsHavingExpiredEnvelope(List<dsfs__DocuSign_Status__c> newList, Boolean toCommit, fflib_SObjectUnitOfWork uow, Map<String, String> statusWithCDAId, Map<Id, dsfs__DocuSign_Status__c> oldMap) {
        system.debug('####TGRH_CDADocusignStatus.cancelRequestsHavingExpiredEnvelope newList: ' +newList);
        system.debug('####TGRH_CDADocusignStatus.cancelRequestsHavingExpiredEnvelope oldList: ' +oldMap.values());
        for (dsfs__DocuSign_Status__c docusignStatus: newList) {
            dsfs__DocuSign_Status__c oldDocusignStatusObj = oldMap.get(docusignStatus.Id);
            if(docusignStatus.dsfs__Envelope_Status__c == UTL_CDAUtility.DS_VOIDED && oldDocusignStatusObj.dsfs__Envelope_Status__c != UTL_CDAUtility.DS_VOIDED && (docusignStatus.dsfs__Voided_Reason__c == UTL_CDAUtility.DS_ENVELOPE_VOID_REASON_EXPIRED || docusignStatus.dsfs__Voided_Reason_Extended__c == UTL_CDAUtility.DS_ENVELOPE_VOID_REASON_EXPIRED) && statusWithCDAId != null && statusWithCDAId.size() > 0 && statusWithCDAId.containsKey(docusignStatus.id) && cdaRequestMapWithCDAId != null && cdaRequestMapWithCDAId.size() > 0 && cdaRequestMapWithCDAId.containsKey(statusWithCDAId.get(docusignStatus.Id)) && cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).RecordTypeId != Schema.SObjectType.CDA_Request__c.getRecordTypeInfosByName().get(UTL_CDAUtility.historicalDataRecordType).getRecordTypeId()) {   //Updated by Vikram Singh under CR-11691
                cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status__c = UTL_CDAUtility.STATUS_CANCELEDBYREQUESTOR;
                cdaRequestUpdateMap.put(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id, cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)));
                toCommit = true;
            }
            if(toCommit) {
                uow.registerDirty(cdaRequestUpdateMap.values());
            }
        }
        system.debug('####inside DAOH_DocusignStatus.cancelRequestsHavingExpiredEnvelope uow: '+uow);
        if(toCommit) {
            uow.commitWork();
        }
    }
}
