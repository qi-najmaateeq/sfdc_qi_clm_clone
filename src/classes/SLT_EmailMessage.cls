public class SLT_EmailMessage  extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_EmailMessage() {
        super(false, true, true);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return EmailMessage.sObjectType;
    }
    
    /**
     * This method used to get EmailMessage by Id
     * @return  Map<Id, User>
     */
    
    public List<EmailMessage> selectById(Set<ID> idSet, Set<String> fieldSet) {
        return (List<EmailMessage>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL());
    }
    
    
}