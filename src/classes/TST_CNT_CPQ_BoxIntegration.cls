@isTest
private class TST_CNT_CPQ_BoxIntegration {

    static Apttus__APTS_Agreement__c setAgreementData(Id OpportuntiyId){

        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.Mark_as_Primary__c = true;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.Pricing_Tool_Locked__c = true;
        testAgreement.Budget_Checked_Out_By__c = UserInfo.getUserId();
        testAgreement.Agreement_Status__c = CON_CPQ.DRAFT;
        testAgreement.Process_Step__c = CON_CPQ.STRATEGY_CALL;
        testAgreement.Bid_Number__c = 1;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        testOpportunity.Legacy_Quintiles_Opportunity_Number__c = 'EVS12';
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }

    public static Apttus__APTS_Agreement__c getAgreementData(Id agreementId){
        return [SELECT Bid_History_Number__c FROM Apttus__APTS_Agreement__c WHERE Id =: agreementId];
    }

    @isTest
    static void testGetboxNameshouldReturnBoxName(){
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = setAgreementData(testOpportunity.Id);
        insert agreement;
        ApexPages.CurrentPage().getParameters().put(CON_CPQ.ID, agreement.Id);

        Test.startTest();
        String boxName = new CNT_CPQ_BoxIntegration().getBoxName();
        Test.stopTest();

        Apttus__APTS_Agreement__c updateAgreement = getAgreementData(agreement.Id);
        system.assertEquals('TestAccount EVS12 Bid 1: '+updateAgreement.Bid_History_Number__c, boxName, 'Should Return Box Name');
    }
}