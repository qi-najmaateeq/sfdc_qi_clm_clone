/*  ===============================================================================
        Created:        Shweta Alwani
        Date:           18/03/2019
        Description:    This is used to test CNT_CPQ_ContractDetailsOnAgreement apex class.
        ===============================================================================
    */
@isTest
public class TST_CNT_CPQ_ContractDetailsOnAgreement {
    static Account accountDataSetup() {
        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }
    
    static Apttus__APTS_Agreement__c agreementDataSetup(String contractNumber) {
        
        Account accountData = accountDataSetup();
        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountData.Id);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;
        
        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CONTRACT).getRecordTypeId();
        Apttus__APTS_Agreement__c agreement = UTL_TestData.createAgreement();
        agreement.Contract_Number__c = contractNumber;
        agreement.RecordTypeId = recordTypeId;
        agreement.Apttus__Related_Opportunity__c = testOpportunity.Id;
        insert agreement;
        return agreement;
    }
    
    static Contract contractDataSetup() {
        Account accountData = accountDataSetup();
        Contract contract = new Contract(Name = 'Work Order');
        contract.AccountId=accountData.Id;
        contract.Parent_Contract_Number__c = 123;
        contract.Ultimate_Parent_Contract_Number__c = 345;
        insert contract;
        return contract;
    }
    
    static Contract getContract(Id contractId){
        Set<Id> contractIdSet = new Set<Id> {contractId};
        Set<String> fieldsToQuery = new Set<String> {CON_CPQ.ID, CON_CPQ.CONTRACT_CONTRACTNUMBER};
        List<Contract> contractList = new SLT_Contract().fetchContract(contractIdSet, fieldsToQuery);        
        return contractList[0];
    }
    
    @isTest
    static void testFetchContract() {
        Contract contractData = contractDataSetup();
        Contract contractQuery=getContract(contractData.Id);
        Apttus__APTS_Agreement__c agreement = agreementDataSetup(contractQuery.contractNumber);
        Apexpages.Currentpage().getparameters().put(CON_CPQ.Id, agreement.Id); 
        
        Test.startTest();      
            ApexPages.StandardController sc = new ApexPages.StandardController(agreement);
            CNT_CPQ_ContractDetailsOnAgreement controllerObj = new CNT_CPQ_ContractDetailsOnAgreement(sc);
            Contract contract = controllerObj.getContract();
        Test.stopTest();
        
        System.assertNotEquals(null, contract, 'should return contract');
        
    }
}