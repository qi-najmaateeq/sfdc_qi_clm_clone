/**
* This class is used to test SLT_MIBNF 
*/ 
@isTest
public class TST_SLT_MIBNF {
    
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        insert opp;
        Revenue_Analyst__c revenueAnalyst = UTL_TestData.createRevenueAnalyst();
        insert revenueAnalyst;
        MIBNF2__c mibnf2 = UTL_TestData.createMIBNF(opp, revenueAnalyst);
        insert mibnf2; 
    }
    
    @isTest
    public static void testSelectByFilter() {
        Set<ID> oppIdSet = new Set<Id>();
        Set<String> fieldSet = new Set<String>{'Name', 'Id'};
        String filterCondition = 'Opportunity__c in :oppIdSet';    
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        oppIdSet.add(opp.Id);
        Test.startTest();
        SLT_MIBNF mibnfSelector = new SLT_MIBNF();
        List<MIBNF2__c> mibnf2List = mibnfSelector.selectByFilter(oppIdSet, fieldSet, filterCondition);
        System.assertEquals(1, mibnf2List.size());
        Test.stopTest();
    }
}