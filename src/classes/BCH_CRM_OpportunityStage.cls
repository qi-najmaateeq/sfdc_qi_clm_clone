/*
 * Version       : 1.0
 * Description   : This Batch Class for Deleting OpportunityStage Custom Setting
 */
public class BCH_CRM_OpportunityStage  implements Database.Batchable<Sobject> {
    
    /**
     * start method 
     * @params  Database.BatchableContext context
     * @return  Database.QueryLocator
     */
    public Database.QueryLocator start(Database.BatchableContext context) {
        Date daybefYstrDay = Date.today() - 1;
        return Database.getQueryLocator([SELECT Id, Name FROM Opportunity_Stage__c WHERE CreatedDate < :daybefYstrDay]);
    }
    
    /**
     * execute method 
     * @params  Database.BatchableContext context
     * @params  List<Opportunity_Stage__c> records
     * @return  void
     */
    public void execute(Database.BatchableContext context, List<Opportunity_Stage__c> records) {
        delete records;
    }
    
    /**
     * finish method 
     * @params  Database.BatchableContext context
     * @return  void
     */
    public void finish(Database.BatchableContext context) {
       
    }
}