/**
 * Test class to check SAP_Address_MDM_Extension class
 */ 
@isTest
private class TST_SAP_Address_MDM_Extension {
    
    /**
     * Method which creates data for test class
     */ 
    @testSetup
    static void dataSetup() { 
        Account account = new Account(Name='Test Account',Status__c=MDM_Defines.AddressStatus_Map.get('SAP_VALIDATED'));
        insert account;
        
        List<User> userList = UTL_TestData.createUser(CON_CRM.SYSTEM_ADMIN_PROFILE, 2);
        userList.addall(UTL_TestData.createUser('Sales User', 1));
        insert userList;
        
        Contact cnt = UTL_TestData.createContact(account.Id);
        cnt.RecordTypeId = CON_CRM.IQVIA_USER_CONTACT_RECORD_TYPE_ID;
        cnt.Salesforce_User__c = userList[0].Id;
        insert cnt;
        
        Opportunity TestOpp = new Opportunity(Name='test',StageName='1. Identifying Opportunity',CloseDate=System.today());
        TestOpp.LeadSource = 'Account Planning';
        TestOpp.Budget_Available__c = 'Yes';
        TestOpp.StageName='5. Finalizing Deal';
        TestOpp.AccountId = account.Id;
        TestOpp.Contract_Term__c='Single-Period';
        TestOpp.Contract_End_Date__c = system.today();
        TestOpp.Contract_Start_Date__c = system.today();
        TestOpp.Contract_Type__c='Individual';
        TestOpp.LeadSource = 'Account Planning';
        TestOpp.CurrencyIsoCode = 'USD';
        insert TestOpp;
        
        OpportunityContactRole contactRole = UTL_TestData.createOpportunityContactRole(cnt.Id, TestOpp.Id);
        insert contactRole;
        
        //  Add a line item to the opportunity
        List<OpportunityLineItem> OLI_Array = new List<OpportunityLineItem>();
        Product2 product2 = UTL_TestData.createProduct();
        product2.Hierarchy_Level__c = CON_CRM.MATERIAL_LEVEL_HIERARCHY_OLI;
        product2.IsActive = true;
        product2.Material_Type__c = 'ZPUB';
        insert product2;
        PricebookEntry pbEntry = UTL_TestData.createPricebookEntry(product2.Id);
        pbEntry.CurrencyIsoCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        PricebookEntry PE1 = [SELECT Id, CurrencyIsoCode FROM PricebookEntry WHERE CurrencyIsoCode = 'USD' and IsActive = true AND Product2.IsActive = true AND Product2.Material_Type__c = 'ZPUB' limit 1][0];
        OpportunityLineItem OLI1 = UTL_TestData.createOpportunityLineItem(TestOpp.Id, PE1.Id);
        OLI1.Quantity = 1.00;
        OLI_Array.add(OLI1);
        PricebookEntry PE2 = [SELECT Id, CurrencyIsoCode FROM PricebookEntry WHERE CurrencyIsoCode = 'USD' and IsActive = true AND Product2.IsActive = true AND Product2.Material_Type__c = 'ZPUB' LIMIT 1][0];
        OpportunityLineItem OLI2 = UTL_TestData.createOpportunityLineItem(TestOpp.Id, PE2.Id);
        OLI2.Quantity = 1.00;
        OLI_Array.add(OLI2);
        insert OLI_Array;
        
        BNF2__c bnf = new BNF2__c(Opportunity__c=TestOpp.Id);
        bnf.BNF_Status__c = 'New';
        bnf.IMS_Sales_Org__c = 'IMS Spain';
        bnf.Client__c = account.Id;
        insert bnf;
        
        MIBNF2__c MIBNF=new MIBNF2__c();
        MIBNF.Client__c=TestOpp.AccountId;
        MIBNF.Opportunity__c=TestOpp.Id;
        MIBNF.Sales_Org_Code__c='CH08';
        MIBNF.Billing_Currency__c='USD';
        MIBNF.IMS_Sales_Org__c='Acceletra';
        MIBNF.Fair_Value_Type__c='Stand Alone';
        MIBNF.Invoice_Default_Day__c='15';
        MIBNF.Contract_Start_Date__c=system.today();
        MIBNF.Contract_End_Date__c=system.today();
        MIBNF.Contract_Type__c='Individual';
        MIBNF.Contract_Term__c='Single-Period';
        MIBNF.Payment_Terms__c='0000-Default Payment Terms of Customer Master Data';
        insert MIBNF;
        
        MIBNF_Component__c MIBNF_Comp=new MIBNF_Component__c();
        MIBNF_Comp.MIBNF__c=MIBNF.Id;
        MIBNF_Comp.BNF_Status__c='New';
        MIBNF_Comp.Is_this_a_retainer_downpayment__c='No';
        MIBNF_Comp.Print_Shop__c='No'; 
        insert MIBNF_Comp;   
    }
    
    /**
     * test method to check constructor conditions
     */ 
    static testMethod void testSAPAddressMDMExtensionsConstructor() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        Apexpages.currentPage().getParameters().put('popup','0');
        Apexpages.currentPage().getParameters().put('ApprovalRequest','true');
        Apexpages.currentPage().getParameters().put('retURL','/' + bnfList[0].Id);  
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        ApexPages.Message msg1 = ApexPages.getMessages()[0];
        System.assertEquals('There are currently no addresses available for this account.  Please use the form below to submit a request for the creation of a new address in SAP.', msg1.getDetail()); 
        Test.stopTest();
    }
    
    /**
     * test method to check constructor conditions
     */ 
    static testMethod void testSAPAddressMDMExtensionsConstructor2() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id, Client__c FROM BNF2__c];
        List<MIBNF_Component__c> mibnfComp = [SELECT Id FROM MIBNF_Component__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        Apexpages.currentPage().getParameters().put('compId',mibnfComp[0].Id);
        Apexpages.currentPage().getParameters().put('BnfAccountId',accountList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        Test.stopTest();
    }
    
    /**
     * test method to check GetTitles method
     */ 
    static testMethod void testGetTitles() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension(); 
        controller.getTitles();
        Test.stopTest();
    }
    
    /**
     * test method to check ReturnToBnf method
     */ 
    static testMethod void testReturnToBnf() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.ReturnToBnf();
        Apexpages.currentPage().getParameters().put('retURL','/' + bnfList[0].Id);
        controller.ReturnToBnf();
        Apexpages.currentPage().getParameters().put('BnfAccountId',accountList[0].Id);
        SAP_Address_MDM_Extension controller1 = new SAP_Address_MDM_Extension();
        controller1.ReturnToBnf();
        Test.stopTest();
    }
    
    /**
     * test method to check SendNewRequestNotificationEmail method Validation
     */ 
    static testMethod void testSendNewRequestNotificationEmailValidation() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.SAP_Contact = controller.getSAP_Contact();
        PageReference pg = controller.SendNewRequestNotificationEmail();
        ApexPages.Message msg = ApexPages.getMessages()[0];
        System.assertEquals('Last Name Required.', msg.getDetail());
        Test.stopTest();
    }
    
    /**
     * test method to check SendNewRequestNotificationEmail method
     */ 
    static testMethod void testSendNewRequestNotificationEmail() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.SAP_Contact = controller.getSAP_Contact();
        controller.SAP_Contact.Last_Name__c = 'Test Sap';
        controller.Address.Name='Test User';
        controller.Address.City__c='Temp';
        controller.Address.Country__c='Turkey';
        controller.Address.PostalCode__c='123456';
        controller.Address.Payment_Terms__c='15 days';
        controller.Address.Taxation_Status__c='Fully taxable';
        controller.Address.VAT_Registration_Number__c = '123';
        controller.Address.Address_Line_1__c = 'Abc';
        controller.Address.Tax_Number_1__c = '123456';
        PageReference pg = controller.SaveAddress();
        pg = controller.SendNewRequestNotificationEmail();
        ApexPages.Message msg = ApexPages.getMessages()[0];
        System.assertEquals('New contact request has been sent to MDM Helpdesk. You will be notified once the contact has been created in SAP.', msg.getDetail()); 
        Test.stopTest();
    }
    
    /**
     * test method to check AddSAPContact method Validation
     */ 
    static testMethod void testAddSAPContactValidation() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.SAP_Contact = controller.getSAP_Contact();
        controller.SAP_Contact.Last_Name__c=null;
        PageReference pg = controller.AddSAPContact();
        ApexPages.Message msg = ApexPages.getMessages()[0];
        System.assertEquals('Last Name Required.', msg.getDetail()); 
        Test.stopTest();
    }
    
    /**
     * test method to check AddSAPContact method
     */ 
    static testMethod void testAddSAPContact() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.SAP_Contact = controller.getSAP_Contact();
        controller.SAP_Contact.Last_Name__c = 'Test Sap';
        PageReference pg = controller.AddSAPContact();
        ApexPages.Message msg = ApexPages.getMessages()[0];
        System.assertEquals('New Contact request has been added. You can add more contact or you can click finish to submit your request to MDM Help desk.', msg.getDetail());     
        System.assertEquals(controller.RequestSubmitted,true);
        Test.stopTest();
    }
    
    /**
     * test method to add SaveAddress method Validation
     */ 
    static testMethod void testSaveAddressValidation() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);       
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.Address.City__c='Temp';
        controller.Address.Country__c='Austria';
        controller.Address.PostalCode__c='123456';
        controller.Address.Payment_Terms__c='15 days';
        controller.Address.Taxation_Status__c='Fully taxable';
        controller.Address.Name='Test User Test User Test User Test User Test User Test User Test User Test User Test User';
        PageReference pg = controller.SaveAddress();
        ApexPages.Message msg = ApexPages.getMessages()[0];
        System.assertEquals('Name must be less than 40 characters', msg.getDetail());
        
        controller.Address.Name='Test User';
        controller.SaveAddress();
        msg = ApexPages.getMessages()[1];
        System.assertEquals('VAT Registration Number is mandatory for addresses in ' + controller.address.Country__c, msg.getDetail());
        
        controller.Address.VAT_Registration_Number__c = '123';
        controller.Address.Country__c='Turkey';
        controller.SaveAddress();
        msg = ApexPages.getMessages()[2];
        System.assertEquals('Address Line 1 is mandatory for addresses in ' + controller.address.Country__c, msg.getDetail());
        
        controller.Address.Address_Line_1__c = 'Abc';
        controller.SaveAddress();
        msg = ApexPages.getMessages()[3];
        System.assertEquals('Tax Number 1 is mandatory for addresses in ' + controller.address.Country__c, msg.getDetail());
        Test.stopTest();
    }
    
    /**
     * test method to check SaveAddress method
     */ 
    static testMethod void testSaveAddress() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.Address.Name='Test User';
        controller.address.City__c='Temp';
        controller.address.Country__c='Turkey';
        controller.address.PostalCode__c='123456';
        controller.Address.Payment_Terms__c='15 days';
        controller.Address.Taxation_Status__c='Fully taxable';
        controller.Address.VAT_Registration_Number__c = '123';
        controller.Address.Address_Line_1__c = 'Abc';
        controller.Address.Tax_Number_1__c = '123456';
        PageReference pg = controller.SaveAddress();
        System.assertEquals(controller.RequestSubmitted,true);
        Test.stopTest();
    }
    
    /**
     * test method to check SendNewAddressRequestNotificationEmail method
     */ 
    static testMethod void testSendNewAddressRequestNotificationEmail() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.Address.Name='Test User';
        controller.address.City__c='Temp';
        controller.address.Country__c='Turkey';
        controller.address.PostalCode__c='123456';
        controller.Address.Payment_Terms__c='15 days';
        controller.Address.Taxation_Status__c='Fully taxable';
        controller.Address.VAT_Registration_Number__c = '123';
        controller.Address.Address_Line_1__c = 'Abc';
        controller.Address.Tax_Number_1__c = '123456';
        PageReference pg = controller.SendNewAddressRequestNotificationEmail();
        ApexPages.Message msg = ApexPages.getMessages()[0];
        System.assertEquals('New address request has been sent to MDM Helpdesk. You will be notified once the address has been created in SAP.', msg.getDetail());
        Test.stopTest();
    }
    
    /**
     * test method to check DoNothing method
     */ 
    static testMethod void testDoNothing() {
        Test.startTest();
        List<Account> accountList = [SELECT Id FROM Account];
        List<BNF2__c> bnfList = [SELECT Id FROM BNF2__c];
        Apexpages.currentPage().getParameters().put('AccountId',accountList[0].Id);        
        Apexpages.currentPage().getParameters().put('BnfId',bnfList[0].Id);
        SAP_Address_MDM_Extension controller = new SAP_Address_MDM_Extension();
        controller.DoNothing();
        System.assertEquals('YYYXXXXX',controller.Debug);
        Test.stopTest();
    }
}