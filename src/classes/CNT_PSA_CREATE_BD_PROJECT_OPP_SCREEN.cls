/***********************************************
   Name      :     CNT_PSA_CREATE_BD_PROJECT_OPP_SCREEN 
   Date      :     8-April-2019
   Usage     :     Validate Opportunity for BD Project before redirecting to LI org 
   Author    :     Suman Sharma
***********************************************/

public class CNT_PSA_CREATE_BD_PROJECT_OPP_SCREEN {
    @AuraEnabled 
    public static List<String> validateOppForProjectCreation(Id opportunityId) {
        Boolean passedValidation = true;
        Id selectedComponentId;
        List<String> errorMessages = new List<String>();
        Opportunity currentOpportunity = new Opportunity();
        List<Opportunity> lstOpportunity = new List<Opportunity>([ SELECT LI_Opportunity_Id__c,Name,Opportunity_Number__c,StageName,(Select id from Projects__r)
                                                                  FROM Opportunity 
                                                                  WHERE id =:opportunityId ]);
        currentOpportunity = lstOpportunity[0];
        // ensure oppty stage is 2,3 or 4
        if(currentOpportunity.Projects__r.size() > 0) {
            passedValidation = false;
            errorMessages.add('BD Project Cannot be created for this Opportunity.Billable or BD project is already created.');
            return errorMessages;
        }
        if(currentOpportunity.StageName.substring(0,1) != '2' && 
           currentOpportunity.StageName.substring(0,1) != '3' &&
           currentOpportunity.StageName.substring(0,1) != '4' ) {                
               passedValidation = false;
               errorMessages.add('Opportunity stage is not valid for creating BD projects.BD Project can only be created between stage 2 , 3 and 4.');
               return errorMessages;
           }  
        
        selectedComponentId = getComponentListData(opportunityId);
        // check for selected component        
        if(selectedComponentId == null) {           
            passedValidation = false;
            errorMessages.add('Component is required to create BD  Project.');
            return errorMessages;
        } 
        if(!passedValidation){
            errorMessages.add('Validation Failed');
            return errorMessages;
        }
        if(currentOpportunity.LI_Opportunity_Id__c!=null && currentOpportunity.LI_Opportunity_Id__c!='') {
            errorMessages.add(currentOpportunity.LI_Opportunity_Id__c);
            return errorMessages;
        }
        return null;        
        
    }
    @AuraEnabled
    public static Id getComponentListData(Id opportunityId) {        
        
        List <OpportunityLineItem> opportunityLineItemList = [SELECT o.Id,o.PricebookEntry.Product2.Name,o.PricebookEntry.Product2.Id,o.TotalPrice FROM OpportunityLineItem o WHERE o.OpportunityId = : opportunityId AND o.PSA_Budget__c  = null  AND o.PricebookEntry.Product2.Material_Type__c = 'ZREP' ORDER BY o.PricebookEntry.Product2.Name ASC ];
        
        //if(opportunityLineItemList.size() > 0 && selectedComponentId == null)
        if(opportunityLineItemList.size() > 0){
            return opportunityLineItemList[0].Id;
            
        }
        return null; 
    }
    
    @AuraEnabled
    public static Legacy_Org_Link__c getLegacyOrgLink(){
        return Legacy_Org_Link__c.getInstance();
    }
}