global class BCH_CRM_CreateBoxAssociations_Agree implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global Database.QueryLocator start( Database.BatchableContext BC ) {
        String sQuery = 'SELECT Id, Quintiles_Agreement_Box_ID__c FROM Apttus__APTS_Agreement__c WHERE Quintiles_Agreement_Box_ID__c != null';
        return Database.getQueryLocator(sQuery);
    }
    
    global void execute( Database.BatchableContext BC, List<Apttus__APTS_Agreement__c> scope ) {
        box.Toolkit boxToolkit = new box.Toolkit();
        for(Apttus__APTS_Agreement__c agr : scope) {
            if(agr.Quintiles_Agreement_Box_ID__c != null) {
                boxToolkit.createFolderAssociation(agr.Id, agr.Quintiles_Agreement_Box_ID__c);
            }
        }
        boxToolkit.commitChanges();
    }
    
    global void finish( Database.BatchableContext BC ) {}
}