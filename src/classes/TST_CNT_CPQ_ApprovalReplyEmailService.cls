@isTest
public class TST_CNT_CPQ_ApprovalReplyEmailService {

    static Apttus__APTS_Agreement__c setAgreementData(Boolean markAsPrimary, Id OpportuntiyId, String recordType){
        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = RecordTypeId;
        testAgreement.Mark_as_Primary__c = markAsPrimary;
        testAgreement.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
        testAgreement.Project_Manager_Name__c = 'test manager';
        testAgreement.Project_Manager_Email__c = 'testmanager@test.com';
        insert testAgreement;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){
        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData() {
        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }
    
    static Document setDocumentData(){
        Document documentRecord = UTL_TestData.createDocument('QC Check List', 'QC_Check_List', 'application/pdf');
        documentRecord.IsPublic = true;
        documentRecord.FolderId = UserInfo.getUserId();
        insert documentRecord;
        return documentRecord;
    }
    
    static Messaging.InboundEmail setUpEmailData(Messaging.InboundEnvelope envelope, string status, Apttus__APTS_Agreement__c testAgreement ) {
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        email.subject = 'Test Contact Applicant';
        if(status == 'Approve') {
            email.htmlBody = '<div>Approve</div><div class="agreementId" style="display:none">'+testAgreement.Id+'</div>';
        }
        else
            email.htmlBody = '<div>Reject</div><div class="agreementId" style="display:none">'+testAgreement.Id+'</div>';
        email.fromname = 'FirstName LastName';
        envelope.fromAddress = 'sfdcsrini@email.com';
        return email;
    }    
    
    @isTest
    static void testEmailServiceShouldUpdateStatusFromApprovalReviwMVP(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        testAgreement.Agreement_Status__c = CON_CPQ.PENDING_APPROVAL;
        testAgreement.Process_Step__c = CON_CPQ.APPROVAL_REVIEW;        
        update testAgreement;       
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();        
        // setup the data for the email        
        Messaging.InboundEmail email = setUpEmailData(envelope, 'Approve', testAgreement);
        
        Test.startTest();
        	new CNT_CPQ_ApprovalReplyEmailService().handleInboundEmail(email, envelope);
        Test.stopTest();
        
        Apttus__APTS_Agreement__c updatedAgreement = [SELECT Process_Step__c FROM Apttus__APTS_Agreement__c WHERE Id= :
                                                        testAgreement.Id];
        system.assertEquals(CON_CPQ.BUDGET_APPROVED, updatedAgreement.Process_Step__c, 'Should update status');
        
    }
    
    @isTest
    static void testEmailServiceShouldUpdateStatusFromPLReviwApprovalMVP(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CONTRACT);
        setDocumentData();
        testAgreement.Agreement_Status__c = CON_CPQ.PENDING_PL_REVIEW;
        testAgreement.Process_Step__c = CON_CPQ.PL_REVIEW;
        testAgreement.OwnerId = UserInfo.getUserId();
        
		Attachment attach=UTL_TestData.createAttachment();   	
        attach.Name=CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_NOBI_APP_NAME;
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=testAgreement.id;
        attach.contentType = CON_CPQ.ATTACHMENT_CONTENT_TYPE;
        insert attach;
        
        update testAgreement;             
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();        
        // setup the data for the email        
        Messaging.InboundEmail email = setUpEmailData(envelope, 'Approve', testAgreement);
        
        Test.startTest();
        	new CNT_CPQ_ApprovalReplyEmailService().handleInboundEmail(email, envelope);
        Test.stopTest();
        
        Apttus__APTS_Agreement__c updatedAgreement = [SELECT Process_Step__c FROM Apttus__APTS_Agreement__c WHERE Id= :
                                                        testAgreement.Id];
        system.assertEquals(CON_CPQ.PL_APPROVAL_RECEIVED, updatedAgreement.Process_Step__c, 'Should update status');
        
    }
    
    @isTest
    static void testEmailServiceShouldUpdateStatusFromPLReviwRejectionMVP(){
        setDocumentData();
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CONTRACT);
        testAgreement.Agreement_Status__c = CON_CPQ.PENDING_PL_REVIEW;
        testAgreement.Process_Step__c = CON_CPQ.PL_REVIEW;
        testAgreement.OwnerId = UserInfo.getUserId();
        
		Attachment attach=UTL_TestData.createAttachment();   	
        attach.Name=CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_NOBI_APP_NAME;
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=testAgreement.id;
        attach.contentType = CON_CPQ.ATTACHMENT_CONTENT_TYPE;
        insert attach;   
        
        update testAgreement;
            
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();        
        // setup the data for the email        
        Messaging.InboundEmail email = setUpEmailData(envelope, 'Reject', testAgreement);
        
        Test.startTest();
        	new CNT_CPQ_ApprovalReplyEmailService().handleInboundEmail(email, envelope);
        Test.stopTest();
        
        Apttus__APTS_Agreement__c updatedAgreement = [SELECT Process_Step__c FROM Apttus__APTS_Agreement__c WHERE Id= :
                                                        testAgreement.Id];
        system.assertEquals(CON_CPQ.NONE, updatedAgreement.Process_Step__c, 'Should update status');
        
    }
}