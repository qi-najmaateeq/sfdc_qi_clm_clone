/**
 * This test class is used to test all methods in opportunity trigger helper.
 * version : 1.0
 */
@isTest
private class TST_DAOH_OpportunityLineItem {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Current_Release_Version__c crv = new Current_Release_Version__c();
        crv.Current_Release__c = '3000.01';
        upsert crv;       
        BNF_Settings__c bs = new BNF_Settings__c();
        bs.BNF_Release__c = '2019.01';
        upsert bs; 
        upsert new RWEStudyProduct__c(Name='Q_100339', Product_Code__c='Q_100339');
        upsert new  Mulesoft_Integration_Control__c(Enable_OLIS_JSON__c = true, Is_Mulesoft_User__c = false, Enable_OLI_Sync_Validation__c = true, Enable_Opportunity_Sync_Validation__c = true);
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Contact cnt = UTL_TestData.createContact(acc.Id);
        insert cnt;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        insert opp;
        update opp;
        Product2 product = UTL_TestData.createProduct();
        product.Hierarchy_Level__c = CON_CRM.MATERIAL_LEVEL_HIERARCHY_OLI;
        product.ProductCode = 'Test01';
        insert product;
        PricebookEntry pbEntry = UTL_TestData.createPricebookEntry(product.Id);
        insert pbEntry;
        PrivacyAnalyticsProductCode__c papc = new PrivacyAnalyticsProductCode__c();
        papc.Name = 'Test01';
        papc.Product_Code__c = 'Test01';
        insert papc;
        List<User> userList = UTL_TestData.createUser('System Administrator', 1);
		insert userList;
        OpportunityTeamMember otm = UTL_TestData.createOpportunityTeamMember(opp.Id, userList[0].Id);
        insert otm;
        Group gp = new Group();
        gp.Name = 'Platform Analytics Sales Group';
        insert gp;
        System.runAs(userList[0]) {
            GroupMember gm = UTL_TestData.createGroupMember(gp.Id, userList[0].Id);
            insert gm;
        }
        OpportunityLineItem oppLineItem = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
        oppLineItem.Description = 'Testing';
        oppLineItem.Update_Type__c = CON_CRM.UPDATE_TYPE_MULESOFT_SLAVE;
        oppLineItem.LI_OpportunityLineItem_Id__c = opp.id;
        System.runAs(userList[0]) {  
            insert oppLineItem;
        }
        OpportunityLineItemSchedule olis = UTL_TestData.createOpportunityLineItemSchedule(oppLineItem.Id);
        insert olis;
    }
    
    /**
     * This test method used to create OutboundMessageDeletionQueue
     */ 
    static testMethod void testCreateOutboundMessageDeletionQueue() {   
        List<OpportunityLineItem> oliList = [SELECT Id FROM OpportunityLineItem];
        Test.startTest();
            delete oliList;
            List<Outbound_Message_Deletion_queue__c> outboundList = [SELECT Id FROM Outbound_Message_Deletion_queue__c LIMIT 1];
        Test.stopTest();
        System.assertEquals(1, outboundList.size());
    }
    
    /**
     * This test method used to test the method SetOLIS
     */ 
    static testMethod void testSetOLIS() {
        OpportunityLineItem oli = [SELECT Id, OpportunityLineItemSchedule_JSON__c, (SELECT Id, Revenue, scheduleDate FROM OpportunityLineItemSchedules) FROM OpportunityLineItem LIMIT 1];
        List<OpportunityLineItemSchedule> olisList = new List<OpportunityLineItemSchedule>();
        for(OpportunityLineItemSchedule olis : oli.OpportunityLineItemSchedules) {
            OpportunityLineItemSchedule olisJSON = new OpportunityLineItemSchedule();
            olisJSON.Revenue = olis.Revenue;
            olisJSON.ScheduleDate = olis.ScheduleDate;
            olisList.add(olisJSON);
        }
        oli.OpportunityLineItemSchedule_JSON__c = JSON.serialize(olisList);
        Test.startTest();
            upsert oli;
        Test.stopTest();
    }
    
    /**
     * This test method used to test the method SetOLIS
     */ 
    static testMethod void testSetOLISMulesoft() {
        upsert new  Mulesoft_Integration_Control__c(Enable_OLIS_JSON__c = true, Is_Mulesoft_User__c = true, Enable_OLI_Sync_Validation__c = true);
        OpportunityLineItem oli = [SELECT Id, OpportunityLineItemSchedule_JSON__c, (SELECT Id, Revenue, scheduleDate FROM OpportunityLineItemSchedules) FROM OpportunityLineItem LIMIT 1];
        List<OpportunityLineItemSchedule> olisList = new List<OpportunityLineItemSchedule>();
        for(OpportunityLineItemSchedule olis : oli.OpportunityLineItemSchedules) {
            OpportunityLineItemSchedule olisJSON = new OpportunityLineItemSchedule();
            olisJSON.Revenue = olis.Revenue;
            olisJSON.ScheduleDate = olis.ScheduleDate;
            olisList.add(olisJSON);
        }
        oli.OpportunityLineItemSchedule_JSON__c = JSON.serialize(olisList);
        Test.startTest();
            upsert oli;
        Test.stopTest();
    }
    
    /**
     * This test method used to test the method SetOLIS 
     */ 
    static testMethod void testSetOLISChangeCountry() {
        OpportunityLineItem oli = [SELECT Id, Delivery_Country__c FROM OpportunityLineItem LIMIT 1];
        oli.Delivery_Country__c = 'India';
        Test.startTest();
            upsert oli;
            oli = [SELECT Id, OpportunityLineItemSchedule_JSON__c FROM  OpportunityLineItem LIMIT 1];
        Test.stopTest();
        system.assertEquals(false, String.ISBLANK(oli.OpportunityLineItemSchedule_JSON__c));
    }
    
    /**
     * This test method used to test the method validateMulesoftFieldUpdates 
     */ 
    static testMethod void testValidateMulesoftFieldUpdates() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        opp.LI_Opportunity_Id__c = opp.Id;
        update opp;
        Mulesoft_Opportunity_Sync__c mos = [SELECT Id, LI_Opportunity_Id__c, Opportunity__c FROM Mulesoft_Opportunity_Sync__c LIMIT 1];
        mos.LI_Opportunity_Id__c = mos.Opportunity__c;
        update mos; 
        PricebookEntry pbEntry = [SELECT Id FROM PricebookEntry LIMIT 1];
        OpportunityLineItem oli = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
        oli.Description = 'Testing';
        Test.startTest();
            insert oli;
            Mulesoft_OpportunityLineItem_Sync__c olisync = [SELECT Id , Mulesoft_Sync_Status__c FROM Mulesoft_OpportunityLineItem_Sync__c WHERE OpportunityLineItemId__c = :oli.id];
            try {
                CON_CRM.MULESOFT_OLI_VALIDATION_TRIGGER_HAS_RUN = false;
                oli.Delivery_Country__c = 'China';
                update oli;
            } catch(Exception e) {
                System.assertEquals(System.StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION, e.getDmlType(0));
            }
            try {
                olisync.Mulesoft_Sync_Status__c = CON_CRM.MULESOFT_SYNC_STATUS_FAILED;
                update olisync;
                CON_CRM.MULESOFT_OLI_VALIDATION_TRIGGER_HAS_RUN = false;
                oli.Delivery_Country__c = 'India';
                update oli;
            } catch(Exception e) {
                System.assertEquals(System.StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION, e.getDmlType(0));
            }
            try {
                olisync.LI_OpportunityLineItem_Id__c = oli.id;
                update olisync;
                CON_CRM.MULESOFT_OLI_VALIDATION_TRIGGER_HAS_RUN = false;
                oli.Delivery_Country__c = 'India';
                update oli;
            } catch(Exception e) {
                System.assertEquals(System.StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION, e.getDmlType(0));
            }
            Product2 product = [SELECT Id from Product2 LIMIT 1];
            product.Material_Type__c = CON_CRM.PRODUCT_MATERIAL_TYPE_ZQUI;
            update product;
            try {
                CON_CRM.MULESOFT_OLI_VALIDATION_TRIGGER_HAS_RUN = false;
                oli.Delivery_Country__c = 'India';
                update oli;
            } catch(Exception e) {
                System.assertEquals(System.StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION, e.getDmlType(0));
            }
            try {
                olisync.LQ_OpportunityLineItem_Id__c = oli.id;
                update olisync;
                CON_CRM.MULESOFT_OLI_VALIDATION_TRIGGER_HAS_RUN = false;
                oli.Delivery_Country__c = 'India';
                update oli;
            } catch(Exception e) {
                System.assertEquals(System.StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION, e.getDmlType(0));
            }
        Test.stopTest();
    }
    
    /**
     * This test method used to test the method validationOnOliForProxyProject 
     */ 
    static testMethod void testvalidationOnOliForProxyProject() {
        Opportunity opp = [Select id from Opportunity LIMIT 1];
        Proxy_Project__c proxyProject = UTL_TestData.createProxyProject(opp.id);
        proxyProject.LI_OpportunityLineItem_Id__c = opp.id;
        insert proxyProject;
        OpportunityLineItem oli = [Select id from OpportunityLineItem LIMIT 1];
        Test.startTest();
            try {
                delete oli;
            } catch(DMLException e) {
            }
        Test.stopTest();
        
    }
    
    /**
     * This test method used to test the method setProxyProjectFields. 
     */ 
    static testMethod void testsetProxyProjectFields() {
        Opportunity opp = [Select id from Opportunity LIMIT 1];
        Proxy_Project__c proxyProject = UTL_TestData.createProxyProject(opp.id);
        proxyProject.LI_OpportunityLineItem_Id__c = opp.id;
        proxyProject.LI_Record_id__c = '000000000000000125';
        insert proxyProject;
        OpportunityLineItem oli = [Select id from OpportunityLineItem LIMIT 1];
        Test.startTest();
            try {
                oli.PSA_Project__c = '000000000000000125';
                update oli;
                oli.PSA_Project__c = '';
                update oli;
            } catch(DMLException e) {
            }
        Test.stopTest();
        
    }
    
    /**
    * This test method used to test the method setMC_CESAndSegmantFlag. 
    */ 
    @IsTest
    private static void testSetMC_CESAndSegmantFlag() {
        Product2 product = [SELECT Id, Offering_Type__c, Offering_Segment__c FROM Product2 LIMIT 1];
        product.Offering_Type__c = 'Information Offerings';
        product.Offering_Segment__c = 'Real World & Analytics Solutions';
        update product;
        List<OpportunityLineItem> oliList = [SELECT Id, PricebookEntryId, MC_CES__c, Segment_Flag__c FROM OpportunityLineItem];
        Test.startTest();
        DAOH_OpportunityLineItem.setMC_CESAndSegmantFlag(oliList);
        Test.stopTest();
        
        //System.assertEquals('Innovation', oliList[0].MC_CES__c, 'MC CES not updated');
        //System.assertEquals('Real World Insights', oliList[0].Segment_Flag__c, 'Segment Flag not updated');   
    }
    
    /**
     *  This method is used to verify Privacy Analytics TeamMember.
     */ 
    @isTest
    private static void testVerifyPrivacyAnalyticsTeamMember() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        PricebookEntry pbEntry = [SELECT Id FROM PricebookEntry LIMIT 1];
        List<User> userList = UTL_TestData.createUser('Mulesoft System Administrator', 1);

        insert userList;
        OpportunityLineItem oppLineItem = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
        Group gp = [SELECT Id FROM Group WHERE Name = 'Platform Analytics Sales Group' LIMIT 1];
        GroupMember gm = UTL_TestData.createGroupMember(gp.Id, userList[0].Id);
        insert gm;
        Test.startTest();
        System.runAs(userList[0]) {
            insert oppLineItem;
        }
        Test.stopTest();
    }
}
