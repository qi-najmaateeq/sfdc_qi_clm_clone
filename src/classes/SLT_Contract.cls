public class SLT_Contract extends fflib_SObjectSelector {
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_Contract() {
        super(false, true, false);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
            Contract.Id,
            Contract.Name,
            Contract.ContractNumber,
            Contract.Parent_Contract__c,
            Contract.Parent_Contract_Number__c,
            Contract.Ultimate_Parent_Contract_Number__c,
            Contract.AccountId,
            Contract.Legal_Entity_Customer__c,
            Contract.Legal_Entity_Quintiles__c,
            Contract.Opportunity_Number__c,
            Contract.Opportunity_Name__c,
            Contract.Opportunity_Link__c,
            Contract.Other_Opportunities__c,
            Contract.Project_Number__c,
            Contract.Therapy_Area__c,
            Contract.Drug_Product_Name__c,
            Contract.Global_Project_Unit__c,
            Contract.Delivery_Unit__c,
            Contract.Data_Transfer_Agreement_included__c,
            Contract.Negotiating_Office__c,
            Contract.Date_Tracking_started__c,
            Contract.Protocol_Number__c,
            Contract.Cost_Point_Project_Code__c,
            Contract.Account.Name,
            Contract.Parent_Contract__r.ContractNumber
        };
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Contract.sObjectType;
    }
    
    /**
     * This method used to get Contact by Id
     * @return  Map<Id, Contact>
     */
    public Map<Id, Contract> selectByContractId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Contract>((List<Contract>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    public List<Contract> selectByContractIdList(Set<ID> idSet, Set<String> fieldSet) {
        return ((List<Contract>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    public Map<Id, Contract> selectCNFContractsByProjAndChangeOrderNumber(Set<String> projNumSet, Set<String> chanOrdSet, Set<String> fieldSet) {
        string cnfGBORecordType = CON_CRM.CONTRACT_RECORD_TYPE_CNF_GBO;
        return new Map<Id, Contract>((List<Contract>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Project_Number__c in :projNumSet AND Change_Order_Number__c in :chanOrdSet AND Change_Order_Parent__c = null AND RecordTypeId = :cnfGBORecordType').toSOQL()));
    }
    public Map<Id, Contract> fetchParentContract(Set<Id> idSet, Set<String> fieldSet){
        return new Map<Id, Contract>((List<Contract>) Database.query(newQueryFactory(false).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    public List<Contract> fetchContract(Set<Id> idSet, Set<String> fieldSet){
        return ((List<Contract>) Database.query(newQueryFactory(false).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    public List<Contract> getContractUsingContractNumber(String contractNumber, Set<String> fieldSet){
        return (List<Contract>) Database.query(
            newQueryFactory(true).selectFields(fieldSet).setCondition('ContractNumber =: contractNumber').toSOQL());
    }
}