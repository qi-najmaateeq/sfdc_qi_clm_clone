/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Profile
 */
public class SLT_Profile extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_Profile() {
        super(false, false, false);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Profile.Id,
            Profile.Name
        };
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Profile.sObjectType;
    }
    
    /**
     * This method used to get Account by Id
     * @return  List<Account>
     */
    public List<Profile> selectById(Set<Id> idSet) {
        return (List<Profile>) selectSObjectsById(idSet);
    }
}