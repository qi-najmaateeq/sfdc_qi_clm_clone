/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Line Item Group
 */
public class SLT_LineItemGroup extends fflib_SObjectSelector {
    
    /**
     * constructor to initialize CRUD and FLS
     */
    public SLT_LineItemGroup() {
        super(false, true, true);
    }
    
    /**
     * constructor to initialize CRUD and FLS
     */
    public SLT_LineItemGroup(Boolean enforceCRUD, Boolean enforceFLS) {
        super(false, enforceCRUD, enforceFLS);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Line_Item_Group__c.sObjectType;
    }
    
    /**
     * This method used to get LineItemGroup by OpportunityId
     * @return List<Line_Item_Group__c>
     */
    public List<Line_Item_Group__c> selectByOpportunityId(Set<ID> oppIdSet, Set<String> fieldSet) {
        return (List<Line_Item_Group__c>)Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Opportunity__c IN :oppIdSet').toSOQL());
    }
    
    /**
     * This method used to get LineItemGroup by Id
     * @return  Map<Id, Line_Item_Group__c>
     */
    public Map<Id, Line_Item_Group__c> selectById(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Line_Item_Group__c>((List<Line_Item_Group__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
}