/**
 * This test class is used to test all methods in Project trigger.
 * version : 1.0
 */
@isTest
private class TST_TGR_Proj {

    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
       Account acc = UTL_OWF_TestData.createAccount();
      insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreement(acc.Id, opp.Id);
        insert agreement; 
    }
    
    /**
     * This test method used for insert Project record
     */
    static testmethod void testProjectInsert() {
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        pse__Proj__c project = UTL_OWF_TestData.createBidProject(grp1.id);
        project.pse__Opportunity__c = oppty.Id;
        project.Agreement__c = agreement.id;
        Test.startTest();
          insert project ;
        Test.stopTest();
    }
}