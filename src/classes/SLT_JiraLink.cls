/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for JiraLink
 */
public class SLT_JiraLink extends fflib_SObjectSelector {
    /**
     * This method used to get field list of sobject
     */

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
            CSM_QI_JiraLink__c.Id
        };
    }
    
     /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return CSM_QI_JiraLink__c.sObjectType;
    }
    
    /**
     * This method used to get CSM_QI_JiraLink__c by Id
     * @return  List<CSM_QI_JiraLink__c>
     */
    public List<CSM_QI_JiraLink__c> selectById(Set<ID> idSet) {
        return (List<CSM_QI_JiraLink__c>) selectSObjectsById(idSet);
    }
    
        /**
     * This method used to get CSM_QI_JiraLink__c by CaseId
     * @return  List<CSM_QI_JiraLink__c>
     */
    public List<CSM_QI_JiraLink__c> selectByCaseId(Set<ID> CaseId) {
         return [SELECT Id FROM CSM_QI_JiraLink__c WHERE case_recordId__c=:CaseId];
    }

    
}