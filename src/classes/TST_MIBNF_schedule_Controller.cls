@isTest
public class TST_MIBNF_schedule_Controller {
	@testSetup static void setupTestData(){
        Current_Release_Version__c crv = new Current_Release_Version__c();
        crv.Current_Release__c = '3000.01';
        upsert crv;
        
        Global_Variables.PCFlag = false;
        Global_Variables.RunQuickOppLimitTrigger = false;
        Global_Variables.RenewalCloneFlag = TRUE;
        Global_Variables.syncAgreementToOppty=false;
        Global_Variables.PCTrigger_Hault_Execution=false;
        
        Account TestAccount = BNF_Test_Data.createAccount();
        Opportunity opp = BNF_Test_Data.createOpp();
        BNF_Settings__c bnfsetting = BNF_Test_Data.createBNFSetting();
        List<OpportunityLineItem> OLI_Array = BNF_Test_Data.createOppLineItem();
        List<Address__c> TestAddress_Array = BNF_Test_Data.createAddress_Array();
        User u = BNF_Test_Data.createUser();
        Revenue_Analyst__c TestLocalRA = BNF_Test_Data.createRA();
        MIBNF2__c TestMIBNF = BNF_Test_Data.createMIBNF();
        MIBNF_Component__c TestMIBNF_Comp = BNF_Test_Data.createMIBNF_Comp();
        MI_BNF_LineItem__c TestMI_BNFLineItem = BNF_Test_Data.createMI_BNF_LineItem();
    }
    
    public static testMethod void testMyController() {
		MIBNF_Component__c TestMIBNF_Comp = [Select id,name,MIBNF__c, Addendum__c,Is_this_a_retainer_downpayment__c, Print_Shop__c,BNF_Status__c, Bill_To__c,X2nd_Copy__c,Carbon_Copy__c,Ship_To__c,Cover_Sheet__c from MIBNF_Component__c][0];
        MIBNF2__c TestMIBNF = [Select id,name,Client__c,Opportunity__c,Sales_Org_Code__c,Billing_Currency__c,IMS_Sales_Org__c,Fair_Value_Type__c,Invoice_Default_Day__c,Contract_Start_Date__c,Contract_End_Date__c,Contract_Type__c,Contract_Term__c,Payment_Terms__c,Revenue_Analyst__c from MIBNF2__c][0];
        OpportunityLineItem oli = [select id, OpportunityId, Opportunity.StageName, Opportunity.currencyIsoCode, Opportunity.Contract_Start_Date__c, Opportunity.Contract_End_Date__c, Project_Start_Date__c,Project_End_Date__c,
        							TotalPrice,PricebookEntry.Product2.Name, PricebookEntry.Product2.material_type__c,PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Item_Category_Group__c, Product_Start_Date__c, Product_End_Date__c,Revised_Price__c,Revised_Revenue_Schedule__c from OpportunityLineItem][0];
        
        Test.starttest();
      
        PageReference pageRef = new PageReference('Page.MIBNF_Billing_Schedule');
        Test.setCurrentPage(pageRef);
        ApexPages.standardController controller = new ApexPages.standardController(TestMIBNF_Comp);
        MIBNF_schedule_Controller extension = new MIBNF_schedule_Controller(controller);
        
        BNF_Settings__c bnfSetting =BNF_Settings__c.getOrgDefaults();
        bnfSetting.BNF_Opportunity_Threshold__c = 9456453;
        upsert bnfSetting;
        MIBNF_schedule_Controller extension1 = new MIBNF_schedule_Controller(controller);
        
        bnfSetting.BNF_Opportunity_Threshold__c = null;
        upsert bnfSetting;
        MIBNF_schedule_Controller extension2 = new MIBNF_schedule_Controller(controller);
        
        Test.stoptest();

    }
}