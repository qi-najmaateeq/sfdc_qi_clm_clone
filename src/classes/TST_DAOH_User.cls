/**
* This test class is used to test all methods in User Domain class.
* version : 1.0
*/
@isTest
private class TST_DAOH_User {

    @testSetup
    private static void dataSetup(){
        Account accContact = TST_CSM_TestDataFactory.createAccount();
        insert accContact;
        
        String contactFirstName = 'Saurabh';
        Contact con1 = TST_CSM_TestDataFactory.createContact(accContact.Id, contactFirstName);
        con1.Contact_User_Type__c = 'HO User';
        insert con1;
        
        contactFirstName = 'Richa';
        Contact con2 = TST_CSM_TestDataFactory.createContact(accContact.Id, contactFirstName);
        insert con2;
    }

    static testMethod void testCreateAssignPermissionSetToUser() {
        
        UserRole userRole_1 = [SELECT Id FROM UserRole WHERE PortalType = 'None' LIMIT 1];
        Profile profile_1 = [SELECT Id FROM Profile WHERE Name = 'Service User' LIMIT 1];
        User user_1;
        UserRole portalRole = [Select Id From UserRole Where DeveloperName = 'CSM' Limit 1];
        
        String profilId2 = [select id from Profile where Name='System Administrator'].Id;
        User admin = New User(Alias = 'su',UserRoleId= portalRole.Id, ProfileId = profilId2, Email = 'john2@iqvia.com',IsActive =true ,Username ='john2@iqvia.com', LastName= 'testLastName', CommunityNickname ='testSuNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert admin;
        System.runAs(admin) {
            
            user_1 = new User( 
                Email = 'yourusername@iqvia.com',
                ProfileId = profile_1.Id, 
                UserName = 'yourusername@gmail.com', 
                Alias = 'Test',
                TimeZoneSidKey = 'America/New_York',
                EmailEncodingKey = 'ISO-8859-1',
                LocaleSidKey = 'en_US', 
                LanguageLocaleKey = 'en_US',
                Article_Manager__c = true,
                PortalRole = 'Manager',
                FirstName = 'Firstname',
                LastName = 'Lastname'
            );
            insert user_1;
            user_1.Article_Manager__c = false;
            update user_1;
        }
    }

    @isTest
    static void testUpdateContactOnPortalUser(){
        UserRole portalRole = [Select Id,DeveloperName,PortalType  From UserRole Where DeveloperName = 'IQVIA_Global' and PortalType='None'];
        String profilId2 = [select id from Profile where Name='System Administrator'].Id;
        User accOwner = New User(Alias = 'su',UserRoleId= portalRole.Id, ProfileId = profilId2, Email = 'john2@iqvia.com',IsActive =true ,Username ='john2@iqvia.com', LastName= 'testLastName', CommunityNickname ='testSuNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert accOwner;
        
        system.runAs(accOwner){
            Account accContact = TST_CSM_TestDataFactory.createAccount();
            insert accContact;
            String contactFirstName = 'Saurabh';
            Contact con1 = TST_CSM_TestDataFactory.createContact(accContact.Id, contactFirstName);
            con1.Contact_User_Type__c = 'HO User';
            insert con1;
            
            contactFirstName = 'Richa';
            Contact con2 = TST_CSM_TestDataFactory.createContact(accContact.Id, contactFirstName);
            insert con2;
            List<String> listOfName = new List<String>{'Customer Community Plus Login User', 'CSM Customer Community Plus Login User'};
                List<Profile> listOfProfile = [SELECT Id FROM Profile WHERE Name IN:listOfName];
            List<User> userlist = new List<User>();
            
            User usr1 = new User();
            usr1.FirstName = 'Harry';
            usr1.LastName = 'Singh';
            usr1.Alias = 'hsing';
            usr1.Email = 'noreply@demouser.com';
            usr1.Username = 'HarrySingh@gmail.com';
            usr1.CommunityNickname = 'HarrySingh';
            usr1.ProfileId = listOfProfile[0].Id;
            usr1.ContactId = con1.Id;
            usr1.TimeZoneSidKey = UserInfo.getTimeZone().getID();
            usr1.LocaleSidKey = 'en_US';
            usr1.LanguageLocaleKey = 'en_US';
            usr1.EmailEncodingKey = 'UTF-8';
            userlist.add(usr1);
            
            User usr2 = new User();
            usr2.FirstName = 'Harjit';
            usr2.LastName = 'Kaur';
            usr2.Alias = 'hkaur';
            usr2.Email = 'noreply@demouser.com';
            usr2.Username = 'Harjitkaur@gmail.com';
            usr2.CommunityNickname = 'Harjitkaur';
            usr2.ProfileId = listOfProfile[1].Id;
            usr2.ContactId = con2.Id;
            usr2.TimeZoneSidKey = UserInfo.getTimeZone().getID();
            usr2.LocaleSidKey = 'en_US';
            usr2.LanguageLocaleKey = 'en_US';
            usr2.EmailEncodingKey = 'UTF-8';
            userlist.add(usr2);
            
            Test.startTest();
            insert userlist;
            Test.stopTest();
            
            System.assertEquals(2, userlist.size(), 'Successful Inserted');
        }
    }
    
    @isTest 
    static void testQueue() {
        List<User> aUsers = [select Id from User];
        
        User u = new User (
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@accenture.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        insert u;
        
        User u1 = new User (
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last1',
            Email = 'puser001@accenture.com',
            Username = 'puser001@accenture.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title1',
            Alias = 'alias1',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        insert u1;
        
        User u2 = new User (
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last1',
            Email = 'puser002@accenture.com',
            Username = 'puser002@accenture.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title2',
            Alias = 'alias2',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        insert u2;
        
        Group g1 = new Group(Name='group name', type='Queue');
        insert g1;
        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
        insert q1;
        
        GroupMember grpUser = new GroupMember (
            UserOrGroupId = u.Id,
            GroupId = g1.Id);
        
        insert grpUser;
        
        GroupMember grpUser1 = new GroupMember (
            UserOrGroupId = u1.Id,
            GroupId = g1.Id);
        
        insert grpUser1;
        
        GroupMember grpUser2 = new GroupMember (
            UserOrGroupId = u2.Id,
            GroupId = g1.Id);
        
        insert grpUser2;
        
        DAOH_User.manageQueuesRelations();
        
        DAOH_User.manageUserQueuesRelations(new List<Group>{g1});
        
    }
    
    @isTest 
    static void testCreateContactForUser() {
        User_Contact_Sync__c testUserContactSync = UTL_TestData.createUserContactSync();
        insert testUserContactSync;
        
        List<User> userList = UTL_TestData.createUser('System Administrator', 1);
        Test.startTest();
        insert userList;
        Test.stopTest();
        
        userList[0].Phone = '123456';
        update userList;
    }
}