/*
 * Version       : 1.0
 * Description   : Test Class for SLT_ProcessInstanceWorkitem
 */
@isTest
private class TST_SLT_ProcessInstanceWorkitem {
    
    /**
     * This method used to test sobject field list
     */    
    @IsTest
    static void testGetSObjectFieldList() {
        Test.startTest();
            SLT_ProcessInstanceWorkitem sltProcessInstanceWorkitem = new SLT_ProcessInstanceWorkitem();
            sltProcessInstanceWorkitem.getSObjectFieldList();
        Test.stopTest();
    }
    
    /**
     * This method used to test SObjectType
     */    
    @IsTest
    static void testGetSObjectType() {
        Test.startTest();
            SLT_ProcessInstanceWorkitem sltProcessInstanceWorkitem = new SLT_ProcessInstanceWorkitem();
            sltProcessInstanceWorkitem.getSObjectType();
        Test.stopTest();
    }
    
    /**
     * This method used to get ProcessInstanceWorkitem by id
     */    
    @IsTest
    static void testSelectById() {
        List<ProcessInstanceWorkitem> processInstanceWorkitems = new  List<ProcessInstanceWorkitem>();  
        Test.startTest();
            processInstanceWorkitems = new SLT_ProcessInstanceWorkitem().selectById(new Set<Id> {});
        Test.stopTest();
        Integer expected = 0;
        Integer actual = processInstanceWorkitems.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get all Pending ProcessInstanceWorkitem by id
     */    
    @IsTest
    static void testGetAllPendingWorkItems() {
        List<ProcessInstanceWorkitem> processInstanceWorkitems = new  List<ProcessInstanceWorkitem>();  
        Test.startTest();
            processInstanceWorkitems = new SLT_ProcessInstanceWorkitem().getAllPendingWorkItems(new Set<Id> {});
        Test.stopTest();
        Integer expected = 0;
        Integer actual = processInstanceWorkitems.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get RecordType by Name
     */    
    @IsTest
    static void testGetRecordTypeIdByName() {
        RecordType recordType = [select id, name, developername from recordType LIMIT 1];
        Test.startTest();
            recordType = new SLT_ProcessInstanceWorkitem().getRecordTypeIdByName(recordType.developername);
        Test.stopTest();
    }
	
    /**
     * This method used to get RecordType by sObjectType
     */    
    @IsTest
    static void testGetRecordTypesBySObjectType() {
        RecordType recordType = [select Id, Name from recordType LIMIT 1];
        Test.startTest();
            List<recordType> recordTypeList = new SLT_ProcessInstanceWorkitem().getRecordTypesBySObjectType(recordType.Name);
        Test.stopTest();
        Integer expected = 0;
        Integer actual = recordTypeList.size();
        System.assertEquals(expected, actual);
    }
	
    @IsTest
    static void testGetProcessInstanceWorkItem() {
        UTL_TestData.createOneKeyConfig();
        Test.setMock(HttpCalloutMock.class, new TST_Mock_WSC_CSM_OneKeyCaseVerification());
        case newCase1 = UTL_TestData.createCase();
        insert newCase1;
        SLT_ProcessInstanceWorkitem processIntance = new SLT_ProcessInstanceWorkitem();
        
        Test.startTest();
            List<ProcessInstanceWorkitem> result = processIntance.getProcessInstanceWorkitemByTargetId(new Set<Id>{newCase1.Id});
        Test.stopTest();
        
        System.assertEquals(true, result.isEmpty(), 'Case is in approval process');
    }
}