/**
 * This test class is used to test all methods in CNT_CRM_ChangeCurrency Controller.
 * version : 1.0
 */
@isTest
public class TST_CNT_CRM_RND_TAB {
	/**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        String profileName = 'Sales User';
        List<User> userList = UTL_TestData.createUser(profileName, 1);
        insert userList;
        System.runAs(userList[0]) {
            Account acc = UTL_TestData.createAccount();
            acc.Website = 'https://www.test.com';
            insert acc;
            Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
            opp.Line_of_Business__c = 'Lab';
            insert opp;
            Product2 product = UTL_TestData.createProduct();
            insert product;
            PricebookEntry pbEntry = UTL_TestData.createPricebookEntry(product.Id);
            insert pbEntry;
            OpportunityLineItem oppLineItem = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
            insert oppLineItem;
            OpportunityLineItemSchedule olis =UTL_TestData.createOpportunityLineItemSchedule(oppLineItem.Id); 
            insert olis;
            List<PermissionSetAssignment> paList = new List<PermissionSetAssignment>();
            paList.add(UTL_TestData.createPermissionSetAssignmentRecord('Inside_Sales', UserInfo.getUserId()));
            paList.add(UTL_TestData.createPermissionSetAssignmentRecord('PD_Binary_Decision', UserInfo.getUserId()));
            insert paList;
        }
        
        
    }
    
    /**
     * test method for getListOfRequiredFields.
     */
    static testmethod void testGetListOfRequiredFields() {
        List<User> userList = [SELECT Id FROM User WHERE LastName = 'lastName123'];
        Test.startTest();
        System.runAs(userList[0]) {
            Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Name = 'TestOpportunity'];
            String expectedStage = '4. Delivering Proposal';
            String expectedLineOfBuisness = 'Lab';
            List<String> mapOfRequiredFieldAPI = new List<String>();
            Opportunity_Stage__c oppStage = CNT_CRM_RND_TAB.getOpportunityStageRecord(opp.id);
            mapOfRequiredFieldAPI = CNT_CRM_RND_TAB.getListOfRequiredFields(opp.Id, expectedStage, expectedLineOfBuisness);
            CNT_CRM_RND_TAB.getManagerUserDetail();
            List<String> ligFieldList = new List<String>{'Id'};
            CNT_CRM_RND_TAB.getLIGRecordDetail(opp.Id, ligFieldList);
        }
        
        Test.stopTest();
    }
    
    static testmethod void testGetManagerUserDetail() {
        Test.startTest();
        CNT_CRM_RND_TAB.getManagerUserDetail();
        Test.stopTest();
    }
}