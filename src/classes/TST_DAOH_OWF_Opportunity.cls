/**
* This test class is used to test methods in opportunity trigger.
* version : 1.0
*/
@isTest(seeAllData=false)
public class TST_DAOH_OWF_Opportunity {
    
    /**
    * This method is used to setup data for all methods.
    */
    @testSetup
    static void dataSetup() {
        upsert new  Mulesoft_Integration_Control__c(Enable_OLIS_JSON__c = true, Is_Mulesoft_User__c = true, Enable_OLI_Sync_Validation__c = true, Enable_Opportunity_Sync_Validation__c = true);
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        OWF_Config__c owfConfig = UTL_OWF_TestData.createOWFConfig(grp.Id);
        insert owfConfig;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        List<User> userList = UTL_OWF_TestData.createUser(CON_CRM.SYSTEM_ADMIN_PROFILE, 2);
        userList.addall(UTL_OWF_TestData.createUser('Sales User', 1));
        userList[0].PIC_Eligible__c = true;
        userList[1].PIC_Eligible__c = true;
        insert userList;
    }
    
    /**
    * This test method used to test method setNoOfPotentialRegionsBasedOnPotentialRegions
    */     
    static testMethod void testSetNoOfPotentialRegionsBasedOnPotentialRegions(){
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'TestAccount'];
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        Test.startTest();
            opp.Potential_Regions__c = 'Asia Pacific';
            insert opp;
            Opportunity modifiedOpp = [Select Id, Number_of_Potential_Regions__c From Opportunity Where Id = :opp.Id];
            system.assertEquals(1, modifiedOpp.Number_of_Potential_Regions__c, 'Validating no of potential regions after inserting opportunity.');
            opp.Potential_Regions__c = 'Asia Pacific;Japan';
            update opp;
            modifiedOpp = [Select Id, Number_of_Potential_Regions__c From Opportunity Where Id = :opp.Id];
            system.assertEquals(2, modifiedOpp.Number_of_Potential_Regions__c, 'Validating no of potential regions after inserting opportunity.');
            opp.Potential_Regions__c = '';
            update opp;
            modifiedOpp = [Select Id, Number_of_Potential_Regions__c From Opportunity Where Id = :opp.Id];
            system.assertEquals(0, modifiedOpp.Number_of_Potential_Regions__c, 'Validating no of potential regions after inserting opportunity.');
        Test.stopTest();
    }
    
    /**
     * This test method used to delete Agreement Bases On Opportunity
     */ 
    static testMethod void testDeleteAgreementBasesOnOpportunity() {   
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'TestAccount'];
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id); //UTL_TestData.createOpportunity(acc.Id);
        
        Test.startTest();
            insert opp;
        
            Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreement(acc.Id, opp.Id);
            agreement.Bid_Number__c = 0;
            insert agreement;
        
            List<Opportunity> oppList = [SELECT Id FROM Opportunity];
            delete oppList;
        Test.stopTest();
        
        List<Opportunity> opptyList = [Select Id From Opportunity];
        Integer expected = 0;
        System.assertEquals(expected, opptyList.size());
    }
    
    /**
     * This test method used to delete Agreement Bases On Opportunity
     */ 
    static testMethod void testCreateClinicalBidResRequestsOnOppUpdate() {   
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'TestAccount'];
        Indication_List__c indication = UTL_OWF_TestData.createIndication('Test Indication', 'Acute Care');    
        
        Test.startTest();
            insert indication;
            Line_Item_Group__c lineItemGroup = UTL_OWF_TestData.createLineItemGroup(indication.Id);
            lineItemGroup.Presentation_Date__c = system.today().addDays(5);
            lineItemGroup.Phase__c = 'Phase 1';
            insert lineItemGroup;
            Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
            opp.Line_Item_Group__c = lineItemGroup.Id;
            opp.Potential_Regions__c = 'Asia Pacific';
		    opp.Bid_Defense_Date__c = system.today().addDays(5);
            insert opp;
            
            Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
            agreement.Bid_Due_Date__c = system.today().addDays(5);
            insert agreement;
            
            opp.QI_Invited_to_Present__c = 'Not Sure';
            update opp;
        Test.stopTest();
    }
}