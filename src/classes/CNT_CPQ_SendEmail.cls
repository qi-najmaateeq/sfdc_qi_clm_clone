public class CNT_CPQ_SendEmail {

    @AuraEnabled
	public static void sendMailMethod(String agreementID, String mEmail ,String ccEmail, String bccEmail, String mSubject ,String mbody,
	    List<String> attachmentIds){
        Set<String> fieldSet = new Set<String>{CON_CPQ.AGREEMENT_PROCESS_STEP, CON_CPQ.AGREEMENT_AGREEMENT_STATUS};
        List<Apttus__APTS_Agreement__c> agreement = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId}, fieldSet);
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
        List<String> emails = mEmail.split(',');
        List<ContentVersion> attachmentList = getAttachments(attachmentIds);
        Map<string, string> mimeTypeMap = new Map<string, string>();
        mimeTypeMap.put(CON_CPQ.POWER_POINT_X, CON_CPQ.APPLICATION_OPENXMLFORMAT);
        mimeTypeMap.put(CON_CPQ.POWER_POINT, CON_CPQ.APPLICATION_POWERPOINT);
        mimeTypeMap.put(CON_CPQ.EXCEL_M, CON_CPQ.APPLICATION_EXCEL);

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();       
        mail= CPQ_Utility.getSingleMessage(CON_CPQ.SEND_EMAIL_FOR_REQUEST_TO_KEYSTAKEHOLDER, agreementId, UserInfo.getUserId(),emails);
         if(ccEmail != '' && ccEmail != null) {
		    List <String> ccEmails = ccEmail.split(',');
            mail.setCcAddresses(ccEmails);
		}

        if(bccEmail != '' && bccEmail != null) {
		    List <String> bccEmails = bccEmail.split(',');
            mail.setBccAddresses(bccEmails);
		}
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        if(cpqSetting != null && cpqSetting.Approval_Email_Service__c != null &&
            agreement[0].Agreement_Status__c == CON_CPQ.PENDING_APPROVAL && agreement[0].Process_Step__c == CON_CPQ.APPROVAL_REVIEW) {
            mail.setReplyTo(cpqSetting.Approval_Email_Service__c);
        }
        String mailBody= '';
        mailBody+= '<span style="display:none;visibility:hidden" id="remove_this_line"></span>' + mbody;
        mailBody+= '<div class="agreementId" style="display:none">'+agreementId+'</div>';
        mail.setSubject(mSubject);
        mail.setHtmlBody(mailBody);

        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        if(attachmentList.size()>0) {
            for(ContentVersion attachment : attachmentList) {
                Messaging.Emailfileattachment fileAttachment = new Messaging.Emailfileattachment();
                fileAttachment.setBody(attachment.VersionData);
                fileAttachment.setFileName(attachment.Title + '.' + attachment.FileExtension);
                fileAttachment.setcontenttype(mimeTypeMap.get(attachment.FileType));
                fileAttachments.add(fileAttachment);
            }
        }

        mail.setFileAttachments(fileAttachments);

        mails.add(mail);

        Messaging.sendEmail(mails);
        deleteAttachments(attachmentIds);
    }

    private static List<ContentVersion> getAttachments(List<String> attachmentIds) {
        Set<String> fieldSet = new Set<String>{CON_CPQ.ID, CON_CPQ.VERSION_DATA, CON_CPQ.FILE_TYPE, CON_CPQ.FILE_EXTENSION};
        return new SLT_ContentVersion().getContentVersionByDocumentIds(new Set<Id>((List<Id>)attachmentIds), fieldSet);
    }

    @AuraEnabled
    public static void deleteAttachments(List<String> attachmentIds) {
        List<ContentDocument> documentList = new SLT_ContentDocument().selectById(new Set<Id>((List<Id>)attachmentIds));
        delete documentList;
    }

    @AuraEnabled
    public static List<String> getEmailTemplate(String recordId) {
        List<EmailTemplate> emailTemplate = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(
                    CON_CPQ.SEND_EMAIL_FOR_REQUEST_TO_KEYSTAKEHOLDER, new Set<String>{CON_CPQ.Id});
        List<String> emailTemplateValues = new List<String>();
        if(emailTemplate.size() > 0){
            Messaging.SingleEmailMessage mailTemp = Messaging.renderStoredEmailTemplate(
                emailTemplate[0].Id, UserInfo.getUserId(), recordId);
            emailTemplateValues.add(mailTemp.getSubject());
            emailTemplateValues.add(mailTemp.getHtmlbody());
        }
        return emailTemplateValues;
    }

    @AuraEnabled
    public static void updateAgreementPrcoessStep(Id agreementId, String processStep, String agreementStatus){

        Apttus__APTS_Agreement__c agreementRecord = new Apttus__APTS_Agreement__c(Id = agreementId);
        agreementRecord.Process_Step__c = processStep;

        if(!String.isBlank(agreementStatus)) {
            agreementRecord.Agreement_Status__c = agreementStatus;
        }
        update agreementRecord;
    }

}
