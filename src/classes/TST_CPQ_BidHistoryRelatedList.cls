@isTest
public class TST_CPQ_BidHistoryRelatedList {

    static Apttus__APTS_Agreement__c agreementDataSetup() {

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;

        Opportunity testOpportunity= UTL_TestData.createOpportunity(testAccount.Id);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;
        
        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId();
        Apttus__APTS_Agreement__c agreement = UTL_TestData.createAgreement();
        agreement.RecordTypeId = RecordTypeId;
        agreement.Bid_History_Number_CPQ__c = '123';
        agreement.Apttus__Related_Opportunity__c = testOpportunity.Id;
        insert agreement;
        return agreement;
    }

    static Bid_History__x bidHistoryDataSetup() {

        Bid_History__x bidHistory = UTL_TestData.createBidHistoryExternal();
        return bidHistory;
    }

    @isTest
    static void testFetchBidHistory() {

        /*Apttus__APTS_Agreement__c agreement = agreementDataSetup();
        CNT_CPQ_BidHistoryRelatedList.mockedBidHistorys.add(bidHistoryDataSetup());

        Test.startTest();
            Bid_History__x bidHistory = CNT_CPQ_BidHistoryRelatedList.fetchBidHistory(agreement.Id);
        Test.stopTest();

        System.assertNotEquals(null, bidHistory, 'should return app Id');*/

    }
}