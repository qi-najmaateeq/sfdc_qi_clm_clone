global class SCH_Qualtrics_CreateSurveyRecipient implements Schedulable {
    global void execute(SchedulableContext sc) {
      
        BCH_Qualtrics_CreateSurveyRecipient surveyRecipient = new BCH_Qualtrics_CreateSurveyRecipient();
        Database.executebatch(surveyRecipient); 
    }    
}