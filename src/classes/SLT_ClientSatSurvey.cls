/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Client Sat Survey
 */
public class SLT_ClientSatSurvey {
    
    /**
     * This method is used to get Client Sat Survey by Opportunity and created Date
     * @params Set<Id> oppIdset
     * @params Date createdDate
     * @return  List<Client_Sat_Survey__c>
     */
    public List<Client_Sat_Survey__c> getClientSurveyByOpportunityAndCreatedDate(Set<Id> oppIdSet, Date createdDate) {
        return [SELECT Opportunity__c, Id 
                FROM Client_Sat_Survey__c 
                WHERE Opportunity__c IN : oppIdSet
                AND CreatedDate >: createdDate
               ];
    }
    
    /**
     * This method is used to get Client Sat Survey by Id
     * @params Set<Id> csIdSet
     * @return  List<Client_Sat_Survey__c>
     */
    public List<Client_Sat_Survey__c> getClientSurveyById(Set<Id> csIdSet) {
        return [SELECT Id, Opportunity__r.Principle_inCharge__r.pse__Salesforce_User__r.Business_Unit__c,
                Opportunity__r.Principle_inCharge__r.pse__Salesforce_User__r.PeopleSoft_Product_Offering_Type__c,
                Opportunity__r.Main_Delivery_Country__c,
                Opportunity__r.Principle_inCharge__r.pse__Salesforce_User__r.User_Country__c
                FROM Client_Sat_Survey__c 
                WHERE Id IN : csIdSet
               ];
    }
}