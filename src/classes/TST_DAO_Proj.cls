@isTest
private class TST_DAO_Proj {
    /**
    * This method is used to setup data for all methods.
    */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        Indication_List__c indication = UTL_OWF_TestData.createIndication('Test Indication', 'Acute Care');
        insert indication; 
        Line_Item_Group__c lineItemGroup = UTL_OWF_TestData.createLineItemGroup(indication.Id);
        lineItemGroup.BD_Lead_Sub_Region__c = 'United States of America';
        insert lineItemGroup;
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        opp.Potential_Regions__c = 'Global';
        opp.Line_of_Business__c = 'Core Clinical';
        opp.Line_Item_Group__c = lineItemGroup.Id;
        insert opp;
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        insert agreement; 
        
    }
    
    
    /**
    * This test method used to test Project Delete
    */    
    static testMethod void testProjectDelete(){
        pse__Proj__c bidProject = [Select Id from pse__Proj__c limit 1];
        
        Test.startTest();
            delete bidProject;
        Test.stopTest();
        
        List<pse__Proj__c> projectList = [Select Id from pse__Proj__c];
        System.assertEquals(0, projectList.size());
    }
}