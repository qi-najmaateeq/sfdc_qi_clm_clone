/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Case
 */
public class SLT_Case extends fflib_SObjectSelector {
    /**
     * This method used to get field list of sobject
     */

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
            Case.Id,
            Case.IsClosed,
            Case.SlaStartDate,
            Case.SlaExitDate,
            Case.RecordTypeId,
            Case.CaseNumber,
            Case.AccountId,
            Case.ContactId,
            Case.Case_CategorizationId__c,
            Case.ProductName__c,
            Case.LOS__c,
            Case.SubType1__c,
            Case.SubType2__c,
            Case.SubType3__c,
            Case.Template__c,
            Case.Subject,
            Case.Description,
            Case.CreatedDate,
            Case.ClosedDate,
            Case.Number_of_Article__c,
            Case.Status,
            Case.ProjectCode__c,
            Case.StudyProtocolName__c,
            Case.LabProjectCode__c,  
            Case.SiteNumber__c,
            Case.LabSiteNumber__c,
            Case.Priority,
            Case.LastModifiedById,
            Case.InitialQueue__c,
            Case.Case_SiteName__c,
            Case.InitialQueue__c,
            Case.ParentId,
            Case.OwnerId,
            Case.RecordTypeName__c,
            Case.NewCaseComment__c,
            Case.Origin,
            Case.RecordTypeID,
            Case.Mail_CC_List__c,
            Case.AlignmentName__c,
            Case.CaseRequestBy__c,
            Case.CaseSubType1__c,
            Case.CaseSubType2__c,
            Case.CaseSubType3__c,
            Case.ClientObjectiveBusinessNeed__c,
            Case.CrossDatabase__c,
            Case.DataDeliveryEndDate__c, 
            Case.DataMeasurement__c,
            Case.DigitalInfuzionRegister__c,
            Case.NoContactKnown__c,
            Case.NovartisCaseType__c,
            Case.OnBehalfOf__c,
            Case.Reason,
            Case.ReasonForMissingArticle__c,
            Case.SuppliedCompany,
            Case.SuppliedEmail,
            Case.SuppliedName,
            Case.SuppliedPhone,
            Case.Tag__c,
            Case.Type,
            Case.UrgentForCustomer__c,
            Case.Language__c,
            Case.ProductMarketorOther__c,
            Case.Pharma_Product_Name__c,
            Case.NDC_Number__c,
            Case.Number_of_Corrective_Action_Lookups__c,
            Case.Did_this_Require_Corrective_Action__c,
            Case.RunDateOfReport__c,
            Case.TimeIntervalsquestioningcomparing__c,
            Case.ProductMarketOrOtherName__c,
            Case.Product_Market_or_Other_Name__c,
            Case.CMFProd__c,
            Case.USC5__c,
            Case.AreyouquestioningcomparingProjected__c,
            Case.Auditsquestioningcomparing__c,
            Case.Metricsquestioningcomparing__c,
            Case.InternalOrExternalDI__c,
            Case.Internal_or_External_DI__c,
            Case.FillinthenameoftheClientCompanyt__c,
            Case.PRSNumber__c,
            Case.Channelsquestioningcomparing__c,
            Case.MarketNameOnWeb__c,
            Case.AgeGrouping__c,
            Case.MonoComboTherapy__c,
            Case.SpecialtyGrouping__c,
            Case.DataWeekMonthofReportLifelink__c,
            Case.TimingofReportChanges__c,
            Case.Study__c,
            Case.CSM_QI_Data_Originator__c,
            Case.RandD_Location__c,
            Case.Owner__c,
            Case.AssetId,
            Case.CaseTaskAction__c,
            Case.TaskMilestone__c,    
            Case.Case_Type__c,
            Case.From_EmailToCase__c,
            Case.CaseOriginatorName__c,
            Case.CaseOriginatorEmail__c,
            Case.AFU_TECH__c,
            Case.AFU_Email_Flag__c,
            Case.Case_Thread_ID__c,
            Case.LastCaseComment__c,
            Case.AccountCountry__c,
            Case.IsEscalated
    
        };
    }
    
     /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Case.sObjectType;
    }
    
    /**
     * This method used to get Case by Id
     * @return  List<Case>
     */
    public List<Case> selectById(Set<ID> idSet) {
        return (List<Case>) selectSObjectsById(idSet);
    }

    /**
     * This method used to get Case by Id
     * @return  Map<Id, Case>
     */
      public Map<Id, Case> getCaseById(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Case>((List<Case>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    /**
     * This method used to selectByCaseId
     * @return  Map<Id, Case>
     */
    public Map<Id, Case> selectByCaseId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Case>((List<Case>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    /**
     * This method used to selectByCaseId
     * @return  Map<Id, Case>
     */
    public List<Case> selectByCaseIdList(Set<ID> idSet, Set<String> fieldSet) {
        return (List<Case>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL());
    }

    /**
     * This method used to selectByStudyIdList
     * @return  List<Case>
     */
    public List<Case> selectByStudyIdList(Set<ID> idSet, Set<String> fieldSet) {
        Set<String> statusList=new Set<String>{CON_CSM.S_CLOSED,CON_CSM.S_RESOLVED};
        return (List<Case>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Status not in :statusList and Study__c in :idSet').toSOQL());
    }

    /**
     * This method used to selectByStudySiteContactIdList
     * @return  List<Case>
     */
    public List<Case> selectByStudyStudySiteContactIdList(Set<ID> idSet, Set<String> fieldSet) {
        return (List<Case>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Site_Related_to_the_Study__c in :idSet').toSOQL());
    }

    /**
     * This method used to select List of case based on CaseNumber
     * @return  Case
     */
    public Case selectByCaseNumberList(String cNumber, Set<String> fieldSet) {
        return (Case)Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('CaseNumber = :cNumber').toSOQL());
    }
    
    /**
     * This method used to selectByCaseQueueList
     * @return  Map<Id, Case>
     */
    public List<Case> selectByCaseQueueList(String queueName, Set<String> fieldSet) {
        Set<String> caseStatus=new Set<String>{'Closed','Canceled'};
        return (List<Case>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('InitialQueue__c = :queueName and Status Not In :caseStatus ').toSOQL());
    }
    
    public List<Case> selectByContactId(Set<Id> idSet) {
        return (List<Case>) Database.query('Select Id,CaseNumber,LOS__C,Case_CategorizationId__r.ProductName__c,RecordTypeName__c,CreatedDate,Owner.Alias,Subject,Priority,Status From Case Where ContactId in :idSet order by Id desc LIMIT 5');
    }
    
    public Set<Id> selectByStatusAndParentId(Set<Id> caseIdSet) {
        return new Map<Id, Case>((List<Case>) Database.query('select Id,ParentId from Case where ParentId != null and  ParentId in :caseIdSet and ((Status not in (\'Waiting for\',\'Resolved\',\'Closed\') and RecordTypeName__c in (\'RandDCase\',\'ActivityPlan\')) OR (Status not in (\'Resolved with Customer\',\'Closed\') and RecordTypeName__c = \'TechnologyCase\'))')).keySet();
    } 
    
    public Map<Id, Case> selectByParentId(Set<Id> caseIdSet) {
        return new Map<Id, Case>((List<Case>) Database.query('select Id,ParentId from Case where Id in :caseIdSet'));
    }
    
    public List<Case> selectByEmailMessage(Set<Id> caseIdSet) {
        return Database.query('select id, type, origin, CreatedDate,RecordType.Name, OwnerId, CaseNumber,Description, (select id from EmailMessages) FROM Case where id IN :caseIdSet');
    }
    
    public List<Case> getCaseListForActivity(Set<Id> caseIdSet){
        return Database.query('SELECT Id, AccountId, ContactId, Study__c, LOS__c, RandD_Location__c FROM Case Where Id = :caseIdSet');
    }
    
    public List<Case> getCaseListHierarchy(String byField, List<String> CurrentParent){
        String queryString = 'SELECT Id, CaseNumber,ParentId,Priority,Subject FROM Case';
        String finalQueryString = '';
        if(byField == 'ParentId'){
            finalQueryString = queryString + ' WHERE ParentId IN : CurrentParent ORDER BY ParentId Limit 1000';
        }
        else{
            finalQueryString = queryString + ' WHERE Id IN : CurrentParent ORDER BY ParentId Limit 1000';
        }
        return Database.query(finalQueryString);
    }
    
    public Case getParentCaseHierarchy(String recId){
        return Database.query('SELECT Id, ParentId FROM Case Where Id = :recId');
    }

    public list<Case> getCaseByCaseNumber(String caseNumber){
        return Database.query('Select Id,Status from case where CaseNumber =: caseNumber limit 1');
    }
	
    public List<case> getCaseByIdAndStatus(List<Id> listOfId, String status) {
        return Database.query('select Id, OneKeyID__c, OneKey_FirstName__c, OneKey_LastName__c, OneKey_Validation_Error__c,Type,Status from Case where Id in : listOfId AND status !=: status');
    }
    
    public List<Case> getCaseDetails(List<String> caseIdSet){
        return Database.query('SELECT Id, RecordTypeId,CaseNumber, RecordTypeName__c, AccountId, ContactId, Contact.FirstName, Contact.LastName,Site_Related_to_the_Study__c,Site_Related_to_the_Study__r.Name, LOS__c, SubType1__c, SubType2__c, SubType3__c, Template__c FROM Case Where Id in :caseIdSet');
    }
}