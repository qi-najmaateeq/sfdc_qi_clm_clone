/*
 * Version       : 1.0
 * Description   : Test class for CNT_PEP_CreateContract
 */
@isTest
public class TST_CNT_PEP_CreateContract {

	@testSetup
    public static void setup(){        
        User partnerUser = TST_PEP_TestDataFactory.createPartnerUser('partneruser@iqvia.partner.com');
        User adminUser = TST_PEP_TestDataFactory.createAdminUser('admin','adminPartner@iqvia.com');

        List<User> lstUsers = new List<User>{partnerUser, adminUser};
        insert lstUsers;
        
       System.runAs(adminUser){
            Product2 prod = TST_PEP_TestDataFactory.createProduct('Test');
            insert prod;
        	Agency_Program__c ap = TST_PEP_TestDataFactory.createAgencyProgram(prod);
            insert ap;
	        
	   }   
     }
    
     @isTest
     public static void testFoundActiveAgencyProg(){         
        User partnerUser = [SELECT id FROM User WHERE email='partneruser@iqvia.partner.com'];
        List<Agency_Program__c> agencyProgramList = new List<Agency_Program__c>();

        System.runAs(partnerUser){
	        Test.startTest();	         	
	         	 agencyProgramList = CNT_PEP_CreateContract.getActiveAgencyProg();
	        Test.stopTest();
	     }       
        System.assertEquals(1,agencyProgramList.size());       
    }
    
     @isTest
     public static void testFoundNoActiveAgencyProg(){
		List<Agency_Program__c> lstAp = new List<Agency_Program__c>();         
        User partnerUser = [SELECT id FROM User WHERE email='partneruser@iqvia.partner.com'];
        User adminUser = [SELECT id FROM User WHERE email='adminPartner@iqvia.com'];
        List<Agency_Program__c> agencyProgramList = [SELECT Id, Name, Status__c from Agency_Program__c where status__c = 'Active'];
        System.runAs(adminuser){
            for(Agency_Program__c ap : agencyProgramList){
                ap.Status__c = 'Inactive';
            }
            
            update(agencyProgramList);
	     } 
         
        System.runAs(partnerUser){
	        Test.startTest();	         	
	         	 lstAp = CNT_PEP_CreateContract.getActiveAgencyProg();
	        Test.stopTest();
	     }       
        System.assertEquals(0,lstAp.size());       
    }

    @isTest
    public static void testcreateContractController(){
        User partnerUser = [SELECT id FROM User WHERE email='partneruser@iqvia.partner.com'];
        Agency_Program__c ap = [SELECT id FROM Agency_Program__c WHERE Name='Test'];
        String agencyId = ap.Id;
        
         System.runAs(partnerUser){
            Test.startTest();               
                String ctrId = CNT_PEP_CreateContract.createContractController(agencyId);
            Test.stopTest();

            system.assertNotEquals(null, ctrId);

         }
    }
}