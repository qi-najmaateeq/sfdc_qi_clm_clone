/**
 * @Author  : Babita Dadarwal
 * @Name    : SCH_CDA_RecipientReviewReminders
 * @Purpose : This class is used to send reminders for the CDA requets that are in 'Sent for Recipient Review' Status.
 * Created Under CR-11169
 */
global class SCH_CDA_RecipientReviewReminders implements Schedulable {
	global void execute(SchedulableContext sc) {
		BCH_CDA_RecipientReviewReminders b = new BCH_CDA_RecipientReviewReminders();
		database.executebatch(b, 1);//Updated by Babita Dadarwal under Issue-11280 
	}
}