/*
* Name              : TST_CNT_PSA_CREATE_PROJECT_OPP_SCREEN
* Created By        : Mahima Gupta
* Created Date      : 15 April, 2019
* Revision          : 
* Description       : Test Class for CNT_PSA_CREATE_PROJECT_OPP_SCREEN apex class
*/
@isTest 
public class TST_CNT_PSA_CREATE_PROJECT_OPP_SCREEN { 
  
    @testSetup
    static void setupTestData(){
        Opportunity NewOpportunity = new Opportunity (Name='Test Opp');
        NewOpportunity.StageName = '2 - Verify Opportunity';
        NewOpportunity.CloseDate = System.today();
        NewOpportunity.Budget_Available__c = 'Yes';
        NewOpportunity.CurrencyIsoCode = 'USD';
        insert NewOpportunity;
    }
    
    testmethod static void testValidateOppForProjectCreation() {
        
        Opportunity testOpportunity = [select id from opportunity limit 1];
        
        Test.startTest();
         List<String> validateOppForProjectCreation = CNT_PSA_CREATE_PROJECT_OPP_SCREEN.validateOppForProjectCreation(testOpportunity.id);
        Test.stopTest();
         System.debug('validateOppForProjectCreation' + validateOppForProjectCreation);
         System.assertEquals('Component is required to create projects.', validateOppForProjectCreation[1]);
    }
    
    testmethod static void testGetComponentListData() {
        Opportunity testOpportunity = [select id from opportunity limit 1];
        
        Test.startTest();
         Id componentListData = CNT_PSA_CREATE_PROJECT_OPP_SCREEN.getComponentListData(testOpportunity.id);
        Test.stopTest();
          System.assertEquals(null, componentListData);
    }
    
    testmethod static void testGetLegacyOrgLink(){
        Test.startTest();
         Legacy_Org_Link__c legacyOrgLink = CNT_PSA_CREATE_PROJECT_OPP_SCREEN.getLegacyOrgLink();
        Test.stopTest();
        System.assertNotEquals(null, legacyOrgLink.SetupOwnerId);
    }
}