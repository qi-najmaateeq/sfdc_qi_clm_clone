/**
* This test class is used to test BCH_OWF_UpdateResourceRequestStatus batch class.
* version : 1.0
*/
@isTest(seeAllData=false)
private class TST_BCH_OWF_UpdateResourceRequestStatus {
    
    /**
* This method is used to setup data for all methods.
*/
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        cont.sub_group__c = 'TSL-Japan';
        cont.available_for_triage_flag__c = true;
        
        insert cont;
        
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        agreement.Bid_Number__c = 0;
        agreement.Bid_Due_Date__c = system.today();
        insert agreement;
        
        pse__Proj__c bidProject = [Select id from pse__Proj__c where Agreement__c =: agreement.Id];
        
        List<pse__Resource_Request__c> listToInsertResRequests = new List<pse__Resource_Request__c>();
        pse__Resource_Request__c resourceRequest1 = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest1.pse__Group__c = grp.Id;
        resourceRequest1.SubGroup__c = 'TSL-Japan';
        pse__Resource_Request__c resourceRequest2 = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest2.pse__Group__c = grp.Id;
        resourceRequest2.SubGroup__c = 'TSL-Japan';
        pse__Resource_Request__c resourceRequest3 = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest3.pse__Group__c = grp.Id;
        resourceRequest3.SubGroup__c = 'TSL-Japan';
        pse__Resource_Request__c resourceRequest4 = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest4.pse__Group__c = grp.Id;
        resourceRequest4.SubGroup__c = 'TSL-Japan';
        pse__Resource_Request__c resourceRequest5 = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest5.pse__Group__c = grp.Id;
        resourceRequest5.SubGroup__c = 'TSL-Japan';
        listToInsertResRequests.add(resourceRequest1);
        listToInsertResRequests.add(resourceRequest2);
        listToInsertResRequests.add(resourceRequest3);
        listToInsertResRequests.add(resourceRequest4);
        listToInsertResRequests.add(resourceRequest5);
        insert listToInsertResRequests;
        
        List<pse__Schedule__c> listToInsertSchedules = new List<pse__Schedule__c>();
        listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
        listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
        listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
        listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
        listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
        insert listToInsertSchedules;
        
        List<pse__Assignment__c> listToInsertAssignments = new List<pse__Assignment__c>();
        listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, bidProject.Id, listToInsertSchedules.get(0).Id, cont.Id, listToInsertResRequests.get(0).Id));
        listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, bidProject.Id, listToInsertSchedules.get(1).Id, cont.Id, listToInsertResRequests.get(1).Id));
        listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, bidProject.Id, listToInsertSchedules.get(2).Id, cont.Id, listToInsertResRequests.get(2).Id));
        listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, bidProject.Id, listToInsertSchedules.get(3).Id, cont.Id, listToInsertResRequests.get(3).Id));
        listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, bidProject.Id, listToInsertSchedules.get(4).Id, cont.Id, listToInsertResRequests.get(4).Id));
        insert listToInsertAssignments;
    }
    
    /**
* This test method used to test BCH_OWF_UpdateResourceRequestStatus batch class.
*/ 
    static testMethod void testUpdateCompletedStatusOnResourceRequests(){
        Test.StartTest() ;
        BCH_OWF_UpdateResourceRequestStatus batch = new BCH_OWF_UpdateResourceRequestStatus();
        database.executeBatch(batch, 10);        
        Test.StopTest() ;
        List<pse__Resource_Request__c> modifiedResourceRequests = [Select Id, pse__Status__c From pse__Resource_Request__c];
        //system.assertEquals(CON_OWF.OWF_STATUS_COMPLETED, modifiedResourceRequests.get(0).pse__Status__c);
    }
}