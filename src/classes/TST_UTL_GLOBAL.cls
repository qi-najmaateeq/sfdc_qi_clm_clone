/**
 * This test class is used to test all methods in UTL_GLOBAL class.
 * version : 1.0
 */
@isTest
private class TST_UTL_GLOBAL {
    /**
     * This test method used for testing getCurrentReleaseVersion method with null value
     */ 
    static testMethod void testgetCurrentReleaseVersionWithNullValue() {
        Test.startTest();
            UTL_GLOBAL.getCurrentReleaseVersion();
        Test.stopTest();
    }
    
    /**
     * This test method used for testing getCurrentReleaseVersion method
     */ 
    static testMethod void testgetCurrentReleaseVersion() {
        Current_Release_Version__c testReleaseVersionSetting = new Current_Release_Version__c();
        testReleaseVersionSetting.Current_Release__c = '2018.10';
        insert testReleaseVersionSetting;
        Test.startTest();
            UTL_GLOBAL.getCurrentReleaseVersion();
        Test.stopTest();
    }
}