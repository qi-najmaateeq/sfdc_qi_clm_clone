@isTest
private class TST_SLT_ContentVersion {

	  @testSetup
	  static void dataSetup() {
				ContentVersion testContentVersion = new Contentversion();
				testContentVersion.Title = 'CZDSTOU';
				testContentVersion.PathOnClient = 'test';
				testContentVersion.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
				List<ContentVersion> contentVersionList = new List<ContentVersion>();
				contentVersionList.add(testContentVersion);
				insert contentVersionList;
	  }

	  @isTest static void testGetContentVersionByDocumentIds() {

        SLT_ContentVersion sltContentVersion = new SLT_ContentVersion();
				Set<String> fieldSet = new Set<String>{CON_CPQ.ID, CON_CPQ.VERSION_DATA, CON_CPQ.FILE_TYPE, CON_CPQ.FILE_EXTENSION};
				List<ContentDocument> contentDocumentList = [SELECT Id FROM ContentDocument];

				Test.startTest();
				    List<ContentVersion> contentVersions = sltContentVersion.getContentVersionByDocumentIds(new Set<Id>{contentDocumentList[0].Id}, fieldSet) ;
				Test.stopTest();

				System.assertEquals(1, contentVersions.size(), 'Should return ContentVersion');
	  }

}