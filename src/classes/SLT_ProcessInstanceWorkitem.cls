/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for ProcessInstanceWorkitem
 */
public class SLT_ProcessInstanceWorkitem extends fflib_SObjectSelector {
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            ProcessInstanceWorkitem.Id,
            ProcessInstanceWorkitem.ProcessInstanceId            
        };
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */    
    public Schema.SObjectType getSObjectType() {
        return ProcessInstanceWorkitem.sObjectType;
    }
    
    /**
     * This method used to get ProcessInstanceWorkitem by Id
     * @return  List<ProcessInstanceWorkitem>
     */
    public List<ProcessInstanceWorkitem> selectById(Set<ID> idSet) {
        return (List<ProcessInstanceWorkitem>) selectSObjectsById(idSet);
    }        
    
    /**
     * This method used to get pending work items list
     * @Param    idSet    Set<ID>
     * @Retuen    List<ProcessInstanceWorkitem>
     * @return  List<ProcessInstanceWorkitem>
     */    
    public List<ProcessInstanceWorkitem> getAllPendingWorkItems(Set<ID> idSet) {
        Set<String> fieldSet = new Set<String>{CON_CRM.WORKITEM_PROCESSINSTANCE_TARGETOBJECTID_FIELD_API};        
        return (List<ProcessInstanceWorkitem>) Database.query(newQueryFactory().selectFields(fieldSet).setCondition('ProcessInstance.Status like \'Pending\' and ProcessInstance.TargetObjectId IN :idSet').toSOQL());
    }    
    
    /**
     * Return RecordType Id
     * @Param    recordTypeName    
     */    
    public RecordType getRecordTypeIdByName(String recordTypeName) {   
        List<RecordType> recordType = [select id, name, developername from recordType where developername =: recordTypeName];
        return recordType[0];
    }

    /**
     * Return List<RecordType>
     * @Param    sObjectType
     */    
    public List<RecordType> getRecordTypesBySObjectType(String sObjectTypeName) {   
        List<RecordType> listRecordTypes = [select id, name, developername from recordType where SobjectType =: sObjectTypeName ];
        return listRecordTypes;
    }
	
    /**
     * Return List<ProcessInstanceWorkitem>
     * @Param set Of object Id for which approval process is working
     */    
    public List<ProcessInstanceWorkitem> getProcessInstanceWorkitemByTargetId(Set<Id> setOfTargetId) {   
        List<ProcessInstanceWorkitem> listOfworkItemLst = [SELECT Id 
                                                           FROM ProcessInstanceWorkitem  
                                                           WHERE processInstance.TargetObjectId=:setOfTargetId]; 
        return listOfworkItemLst;
    }
}