/*
 * Version       : 1.0
 * Description   : Utility Class for Creating Records
 */
public class UTL_TestData {
    
    /**
     * This method used to insert Account
     * @return  Account
     */
    public static Account createAccount() {
        return new Account(Name = 'TestAccount', BillingStreet = 'testStreet', BillingCity = 'testCity', BillingCountry = 'testCountry', BillingPostalCode = '123465',AccountCountry__c = 'AF');
    }
    
    /**
     * This method used to insert Contact
     * @params  Id accountId
     * @return  Contact
     */
    public static Contact createContact(Id accountId) {
        return new Contact(LastName = 'TestContact', accountId = accountId);
    }
    
    /**
     * This method used to insert Contacts
     * @params  Id accountId
     * @params  Integer noOfContact
     * @return  Contact
     */
    public static List<Contact> createContacts(Id accountId, Integer noOfContact) {
        List<Contact> contactList = new List<Contact>();
        for(Integer i = 0; i < noOfContact; i++) {
            contactList.add(new Contact(LastName = 'TestContact' + i, accountId = accountId));
        }
        return contactList;
    }
    
    /**
     * This method used to insert Lead
     * @return  Lead
     */
    public static Lead createLead() {
        return new Lead(LastName = 'TestLead', Status = 'New', Company = 'Metacube');
    }
    
    /**
     * This method used to insert Contract
     * @return  Contract
     */
    public static Contract createContract(Opportunity opp, String recordTypeDevName) {
        Contract newContract = new Contract(AccountId = opp.AccountId, ContractTerm = 12, StartDate = opp.CloseDate, OwnerExpirationNotice = '30');
        newContract.recordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByDeveloperName().get(recordTypeDevName).getRecordTypeId();
        return newContract;
    }
    
    /**
     * This method used to insert Contract In/Out Log Record
     * @return  Contract_In_Out_Log__c
     */
    public static Contract_In_Out_Log__c createContractInOutLog(Contract ctr) {
        Contract_In_Out_Log__c newInOutLog = new Contract_In_Out_Log__c();
        newInOutLog.Contract__c = ctr.Id;
        return newInOutLog;
    }

    /**
     * This method used to insert Opportunity put Stage at your end
     * @params  Id accountId
     * @return  Opportunity
     */
    public static Opportunity createOpportunity(Id accountId) {
        Opportunity opp = new Opportunity();
        opp.Name = 'TestOpportunity';
        opp.accountId = accountId;
        opp.stageName = CON_CRM.IDENTIFYING_OPP_STAGE;
        opp.CloseDate = System.today().addYears(1);
        opp.Probability = 10;
        opp.Budget_Available__c = 'Yes';
        opp.Contract_Start_Date__c = system.today();
        opp.Contract_End_Date__c = system.today().addYears(1);
        opp.LeadSource = 'Account Planning';
        opp.CurrencyIsoCode = 'USD';
        opp.Line_of_Business__c = 'Commercial Solutions';
        //opp.Principle_inCharge__c = testContact.Id;
        opp.Primary_Win_Reason__c ='Project Performance';
        opp.Win_Type__c = 'Non-competitive bid';
        return opp;
    }

    /**
     * This method used to insert OpportunityContactRole
     * @params  Id accountId
     * @params  Id opportunityId
     * @return  OpportunityContactRole
     */
    public static OpportunityContactRole createOpportunityContactRole(Id contactId, Id opportunityId) {
        return new OpportunityContactRole(contactId = contactId, role = 'Business User', opportunityId = opportunityId);
    }
    
    /**
     * This method used to insert Product update Material_Type__c either 'ZREP' or 'ZPUB'
     * @return  Product2
     */
    public static Product2 createProduct() {
        return new Product2(Name = 'TestProduct', ProductCode = '1234', isActive = true, CanUseRevenueSchedule = true, InterfacedWithMDM__c = true, Enabled_Sales_Orgs__c = 'CH04', CurrencyIsoCode = 'USD', Delivery_Media__c = 'DVD [DV]:CD [CD]', Delivery_Frequency__c = 'Monthly:Quaterly', Business_Type__c = 'I&A');
    }
    
    /**
     * This method used to insert Favorite Product
     * @return  Product2
     */
    public static Favorite_Product__c createFavoriteProduct(Product2 product) {
        return new Favorite_Product__c(Product__c = product.Id, User__c = UserInfo.getUserId());
    }

    /**
     * This method used to insert PricebookEntry
     * @return  PricebookEntry
     */
    public static PricebookEntry createPricebookEntry(Id productId) {
        return new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(),CurrencyIsoCode = 'USD', Product2Id = productId, UnitPrice = 100.00, isActive = true);
    }
    
    /**
     * This method used to insert OpportunityLineItem
     * @params  Id opportunityId
     * @params  Id pricebookEntryId
     * @return  OpportunityLineItem
     */
    public static OpportunityLineItem createOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = opportunityId, PricebookEntryId = pricebookEntryId, Quantity = 1, TotalPrice = 100, Delivery_Country__c = 'USA', Sale_Type__c = 'New', Revenue_Type__c = 'Ad Hoc');
        oli.Delivery_Country__c = 'India';
        oli.Product_Start_Date__c = Date.today();
        oli.Product_End_Date__c = Date.today().addYears(1);
        oli.Billing_Frequency__c = 'Once';
        oli.Proj_Rpt_Frequency__c='Once [O]';
        oli.Therapy_Area__c= 'Hepatitis C [21]';
        oli.List_Price__c = 100;
        oli.Wbsrelementcode__c = 'test Code1';
        oli.Billing_Date__c = Date.today();
        oli.Delivery_Date__c = Date.today().addYears(2);
        return oli;
    }
    
    /**
     * This method used to insert Billing_Schedule__c
     * @return  Billing_Schedule__c
     */
    public static Billing_Schedule__c createBillingSchedule(Id oliId) {
        return new Billing_Schedule__c(name = 'textSchedule1', oliId__c = oliId);
    }
    
    /**
     * This method used to insert Billing_Schedule_Item__c
     * @return  Billing_Schedule_Item__c
     */
    public static Billing_Schedule_Item__c createBillingScheduleItem(Id bilingScheduleId) {
        return new Billing_Schedule_Item__c(name = 'textScheduleItem1', Billing_Amount__c = 100, Billing_Date__c = system.today(), Billing_Schedule__c = bilingScheduleId);
    }
    
    
    /**
     * This method used to insert DefaultProductSearch
     * @return  Default_Product_Search__c
     */
    public static Default_Product_Search__c createDefaultProductSearch(Id userId) {
        return new Default_Product_Search__c(User__c = userId);
    }
    
    
    
    /**
     * This method used to insert OpportunitySplit
     * @return  OpportunitySplit
     */
    public static OpportunitySplit createOpportunitySplit(Id opportunityId, Id userId, Id splitTypeId) {
        return new OpportunitySplit(OpportunityId = opportunityId, SplitOwnerId = userId, splitTypeId = splitTypeId, SplitPercentage = 100, SplitNote = 'Test');
    }
    
    /**
     * This method used to insert OpportunityTeamMember
     * @return  OpportunityTeamMember
     */
    public static OpportunityTeamMember createOpportunityTeamMember(Id opportunityId, Id userId) {
        return new OpportunityTeamMember(OpportunityId = opportunityId, UserId = userId, TeamMemberRole = 'Account Director', OpportunityAccessLevel = 'Edit');
    }
    
    /**
     * This method used to insert User List
     * @return  List<User>
     */
    public static List<User> createUser(String profileName, Integer noOfUser) {
        List<User> userList = new List<User>();
        for(Integer index = 0; index < noOfUser; index++) {
            userList.add(new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = :profileName].Id,
                LastName = 'lastName123',
                Email = 'testuser3133@iqvia.com',
                Username = 'testuser3133@imshealth.com' + System.currentTimeMillis(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alia3133',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                EmployeeNumber ='500' + index,
                Q1_Target__c = 1.0
            ));
        }
        return userList;
    }
    
    /**
     * This method used to insert Competitor
     * @return  Competitor
     */
    public static Competitor__c createCompetitor() {
        return new Competitor__c(Name = 'TestCompetitor');
    }
    
    /**
     * This method used to insert OpportunityLineItem
     * @params  Id OpportunityLineItemId
     * @return  OpportunityLineItemSchedule
     */
    public static OpportunityLineItemSchedule createOpportunityLineItemSchedule(Id OpportunityLineItemId) {
        return new OpportunityLineItemSchedule(OpportunityLineItemId = OpportunityLineItemId, Revenue = 100, Description = 'Test', Type = 'Revenue', ScheduleDate = system.today());
    }
    
    /**
     * This method used to insert BNF2__c record
     * @params  Id opportunityId
     * @params  OpportunityLineItem oli
     * @params  List<Id> addressList
     * @params  Id revenueAnalystId
     * @return  BNF2__c
     */
    public static BNF2__c createBNFRecord(Opportunity opp, OpportunityLineItem oli, List<Address__c> addressList, Id revenueAnalystId) {
        BNF2__c TestBnf = new BNF2__c();
        TestBnf.Opportunity__c = opp.Id;
        TestBNF.Opportunity_Number__c = opp.Opportunity_Number__c;
        TestBnf.BNF_Status__c = 'New';
        TestBnf.IMS_Sales_Org__c = 'IMS Spain';
        TestBnf.RecordTypeId = MDM_Defines.SAP_SD_Integrated_Record_Type_Id;
        TestBnf.Bill_To__c = addressList[0].Id;
        TestBnf.X2nd_Copy__c = addressList[1].Id;
        TestBnf.Carbon_Copy__c = addressList[2].Id;
        TestBnf.Ship_To__c = addressList[3].Id;
        TestBnf.Cover_Sheet__c = addressList[4].Id;
        TestBnf.Revenue_Analyst__c = revenueAnalystId;
        TestBnf.Sales_Org_Code__c = 'CH04';
        TestBnf.OLI_Json_Data_1__c = JSON.serialize(oli);
        return TestBnf;
    }
    public static MIBNF2__c createMIBNF(Opportunity TestOpp,Revenue_Analyst__c TestLocalRA){
        MIBNF2__c TestMIBNF=new MIBNF2__c();
        TestMIBNF.Client__c=TestOpp.AccountId;
        TestMIBNF.Opportunity__c=TestOpp.Id;
        TestMIBNF.Sales_Org_Code__c='CH08';
        TestMIBNF.Billing_Currency__c='USD';
        TestMIBNF.IMS_Sales_Org__c='Acceletra';
        TestMIBNF.Fair_Value_Type__c='Stand Alone';
        TestMIBNF.Invoice_Default_Day__c='15';
        TestMIBNF.Contract_Start_Date__c=system.today();
        TestMIBNF.Contract_End_Date__c=system.today();
        TestMIBNF.Contract_Type__c='Individual';
        TestMIBNF.Contract_Term__c='Single-Period';
        TestMIBNF.IMS_Sales_Org__c = 'IMS Spain';
        TestMIBNF.Payment_Terms__c='0000-Default Payment Terms of Customer Master Data';
        TestMIBNF.Revenue_Analyst__c = TestLocalRA.id;
        return TestMIBNF;
    }
    public static MIBNF_Component__c createMIBNF_Comp(MIBNF2__c TestMIBNF,List<Address__c> TestAddress_Array){
        MIBNF_Component__c TestMIBNF_Comp=new MIBNF_Component__c();
        TestMIBNF_Comp.MIBNF__c=TestMIBNF.Id;
        TestMIBNF_Comp.BNF_Status__c='New';
        TestMIBNF_Comp.Is_this_a_retainer_downpayment__c='No';
        TestMIBNF_Comp.Print_Shop__c='No';
        TestMIBNF_Comp.Bill_To__c=TestAddress_Array[0].id;
        TestMIBNF_Comp.X2nd_Copy__c=TestAddress_Array[1].id;
        TestMIBNF_Comp.Carbon_Copy__c=TestAddress_Array[2].id;
        TestMIBNF_Comp.Ship_To__c=TestAddress_Array[3].id;
        TestMIBNF_Comp.Cover_Sheet__c=TestAddress_Array[4].id;
        return TestMIBNF_Comp;
    }
    
    /**
     * This method used to insert Address Record
     * @params  Account TestAccount
     * @return List<Address__c>
     */
    public static List<Address__c> createAddresses(Account TestAccount){
        List<Address__c> TestAddress_Array = new List<Address__c>();
        for (Integer i = 0; i < 5; i++) {
            Address__c TempAddress = new Address__c();
            TempAddress.Name = String.valueOf(i);
            TempAddress.Enabled_Sales_Orgs__c='IHA, IMS Health Rotkreuz [CH03]';
            TempAddress.Account__c=TestAccount.Id;
            TempAddress.Street__c = 'Street ' + i.format();
            TempAddress.City__c = 'City '+ i.format();
            TempAddress.Country__c = 'Country ' + i.format();
            TempAddress.SAP_Reference__c = String.valueOf(495000+i);
            TempAddress.International_Name__c = 'Test Address Name' + i;
            TestAddress_Array.add(TempAddress);
        } 
        return TestAddress_Array;
    }
    
    /**
     * This method used to insert SAP_Contact__c Records
     * @params  List<Address__c> addressList
     * @return List<SAP_Contact__c>
     */
    public static List<SAP_Contact__c> createSAPContacts(List<Address__c> addressList){
        List<SAP_Contact__c> TestSapContact_Array = new List<SAP_Contact__c>();
        for (Integer i = 0; i < 5; i++) {
            SAP_Contact__c newSAPContact = New SAP_Contact__c();
            newSAPContact.Name= 'TestSAPContact' + String.valueOf(i);
            newSAPContact.Address__c = addressList[i].Id;
            newSAPContact.SAP_Contact_Number__c = String.valueOf(999999 + i);
            TestSapContact_Array.add(newSAPContact);
        }
        return TestSapContact_Array;
    }
    
    /**
     * This method used to insert Revenue Analyst Record
     * @return Revenue_Analyst__c
     */
    public static Revenue_Analyst__c createRevenueAnalyst() {
        return new Revenue_Analyst__c(User__c = UserInfo.getUserId(), Is_SAP_Revenue_Analyst__c = true, Name='SAPRAUser, Test');
    }
    
    
    /**
     * This method used to insert Mulesoft Integration Control
     * @return  Mulesoft_Integration_Control__c
     */
    public static Mulesoft_Integration_Control__c createMulesoftIntegrationControl(Id userOrProfileOrOrgId) {
        return new Mulesoft_Integration_Control__c(SetupOwnerId = userOrProfileOrOrgId, Enable_OLIS_JSON__c = false, Enable_OLI_Sync_Validation__c = false, Enable_Opportunity_Sync_Validation__c = false, 
                                                   Ignore_Validation_Rules__c = false, Is_Mulesoft_User__c = false, Suppress_Outbound_Messages__c = false);
    }
    
    /**
     * This method used to insert Legacy Org Link
     * @return  Legacy_Org_Link__c
     */
    public static Legacy_Org_Link__c createLegacyOrgLink() {
        return new Legacy_Org_Link__c(SetupOwnerId = UserInfo.getOrganizationId(), Legacy_IMS_URL__c = URL.getSalesforceBaseUrl().toExternalForm(), Legacy_Quintiles_URL__c = URL.getSalesforceBaseUrl().toExternalForm());
    }
    
    /**
     * This method used to insert proxy SCM agreement record.
     * @return  Proxy_SCM_Agreement__c
     */
    public static Proxy_SCM_Agreement__c createProxySCMAgreement(Id OpportunityId) {
        return new Proxy_SCM_Agreement__c(Opportunity__c = OpportunityId, Record_Type_Name__c = 'SOW', Apttus_Status_c__c = 'Activated', LI_Record_Id__c = 'xxxxxxxx');
    }
    
    /**
     * This method used to insert proxy Project record.
     * @return  Proxy_SCM_Agreement__c
     */
    public static Proxy_Project__c createProxyProject(Id OpportunityId) {
        return new Proxy_Project__c(Opportunity__c = OpportunityId, Record_Type_Name__c = 'Project', Project_Status__c = 'Draft', LI_Record_Id__c = 'xxxxxxxx');
    }
    
    /**
     * This method used to insert user contact sync custom setting record.
     * @return User_Contact_Sync__c
     */
    public static User_Contact_Sync__c createUserContactSync() {
        User_Contact_Sync__c testUserContactSync = new User_Contact_Sync__c();
        testUserContactSync.Record_Active__c = 'IsActive,Inactive__c,User To Contact';
        testUserContactSync.Record_Email__c = 'Email,Email,User To Contact';
        testUserContactSync.Record_First_Name__c = 'FirstName,FirstName,User To Contact';
        testUserContactSync.Record_Language__c = 'LanguageLocaleKey,PreferredLanguage__c,User To Contact';
        testUserContactSync.Record_Last_Name__c = 'LastName,LastName,User To Contact';
        testUserContactSync.Record_Phone__c = 'Phone,Phone,Both';
        return testUserContactSync;
    }
    
    /**
     * This method used to insert Merge_Queue__c
     * @return  Merge_Queue__c
     */
    public static Merge_Queue__c createMergeQueue() {
        return new Merge_Queue__c(Name = 'TestMergeQueue', Sobjecttype__c = 'Account', Merge_status__c = 'Pending');
    }
    
    /**
    * This method used to insert Agreement
    * @return  Apttus__APTS_Agreement__c
    */
    public static Apttus__APTS_Agreement__c createAgreement() {
        return new Apttus__APTS_Agreement__c(Name='Test Agreement');
    }
    
    /**
     * This method used to insert Application
     * @return  Apttus_XApps__Application__c
     */
    public static Apttus_XApps__Application__c createApplication() {
        return new Apttus_XApps__Application__c(Name='Test Application');
    }
    
    /**
     * This method used to insert Application
     * @return  Bid_History__x
     */
    public static Bid_History__x createBidHistoryExternal() {
        return new Bid_History__x(Bid_Number_c__c = 0, QIP_Loaded_c__c = 'No',
            DisplayUrl = 'https://quintiles.my.salesforce.com/a0nw000000EclWKAAZ');

    }

    /**
     * This method used to insert Application
     * @return  Contract__x
     */
    public static Contract__x createContractExternal() {
        return new Contract__x(Name__c = 'Work Order', DisplayUrl = 'https://quintiles.my.salesforce.com/a0nw000000EclWKAAZ');
    }
    
    /**
     * This method used to insert CPQ Budget Unlock Setting
     * @return  CPQ_Budget_Unlock_Setting__c
     */
    public static CPQ_Budget_Unlock_Setting__c createBudgetUnlockSetting(){

        CPQ_Budget_Unlock_Setting__c unlockSetting = new CPQ_Budget_Unlock_Setting__c();
        unlockSetting.Name = 'unlock setting';
        unlockSetting.First_Reminder_Hours_To_Unlock_Budget__c = 1;
        unlockSetting.Second_Reminder_Hours_To_Unlock_Budget__c = 2;
        unlockSetting.Third_Reminder_Hours_To_Unlock_Budget__c = 3;
        unlockSetting.Minute_To_Unlock_Agreement__c = 15;
        return unlockSetting;
    }
    
    /**
    * This method used to insert Group
    * @return  Group
    */
    public static Group createGroup(String groupName, String groupType){
    
        Group testGroup = new Group();
        testGroup.name = groupName;
        testGroup.Type = groupType; 
        return testGroup;
    }
    
    /**
    * This method used to insert GroupMember
    * @return  GroupMember
    */
    public static GroupMember createGroupMember(Id groupId, Id userOrGroupId){

        GroupMember testGroupMember = new GroupMember();
        testGroupMember.UserOrGroupId = userOrGroupId;
        testGroupMember.GroupId = groupId;
        return testGroupMember;
    }
    
    /**
    * This method used to insert EmailTemplate
    * @return  EmailTemplate
    */
    public static EmailTemplate createEmailTemplate(String name, String developerName, String templateType){
        EmailTemplate testEmailTemplate = new EmailTemplate();
        testEmailTemplate.isActive = true;
        testEmailTemplate.Name = 'name';
        testEmailTemplate.DeveloperName = developerName;
        testEmailTemplate.TemplateType = templateType;
        return testEmailTemplate;
    }
    
    /**
    * This method used to insert Attachment
    * @return Attachment
    */
    public static Attachment createAttachment() {
        return new Attachment(Name='Test Attachment');
    }/*
    *This method is used to insert QC_Check_List_Item__c
    *return QC_Check_List_Item__c
    */
    public static QC_Check_List_Item__c createQCCheckListItem(){
        return new QC_Check_List_Item__c(Type__c = 'Budget', Question__c = 'Test', Guidelines__c = 'Test');
    }

    /*
    *This method is used to insert Proposal_QA_Self_Check_List__c
    *return Proposal_QA_Self_Check_List__c
    */
    public static Proposal_QA_Self_Check_List__c createProposalQASelfCheckList(){
        return new Proposal_QA_Self_Check_List__c(Type__c = 'Budget', Question__c = 'Test', Guidelines__c = 'Test');
    }

    /*
    *This method is used to insert Document
    *return Document
    */
    public static Document createDocument(String name, String developerName, String contentType){
        return new Document(DeveloperName = developerName, Name = name, ContentType = contentType, Body = Blob.valueOf('Some Text'));
    }

    public static Task createTask(String subject, Id whatId, String status, String priority){
        return new task(Subject = subject, WhatId = whatId, Status = status, Priority = priority);
    }

    /**
    * This method used to insert CPQ_Settings
    * @return CPQ_Settings__c
    */
    public static CPQ_Settings__c createCPQSettings() {
        return new CPQ_Settings__c(Name='Test CPQ Setting');
    }
	
    /**
    * This method used to insert Client Sat Survey Record
    * @return Client_Sat_Survey__c
    */
    public static Client_Sat_Survey__c createClientSatSurveyRecord(String Name, String sendSurvey, String surveyRecipient1_Id, String opportunityId) {
        Client_Sat_Survey__c clientSatSurvey = new Client_Sat_Survey__c();
        clientSatSurvey.Send_Survey__c = sendSurvey;
        clientSatSurvey.Survey_Recipient_1__c = surveyRecipient1_Id;
        clientSatSurvey.Opportunity__c = opportunityId;
        return clientSatSurvey;
    }

    /**
    * This method used to insert Declined Survey Approver Group
    * @return DeclinedSurveyApproverGroup__c
    */    
    public static DeclinedSurveyApproverGroup__c createDeclinedSurveyApproverGroup(String offeringType, String businessUnit, 
                                                                            String mainDeliveryCountry, String userCountry, 
                                                                            String declinedSurveyApproverGroup, 
                                                                            Decimal priority){
        DeclinedSurveyApproverGroup__c declinedApproverGroup = new DeclinedSurveyApproverGroup__c();
        declinedApproverGroup.BusinessUnit__c = businessUnit;
        declinedApproverGroup.DeclinedSurveyApproverGroup__c = declinedSurveyApproverGroup;
        declinedApproverGroup.PeopleSoft_User_Country__c = userCountry;
        declinedApproverGroup.MainDeliveryCountry__c = mainDeliveryCountry;
        declinedApproverGroup.PeopleSoftProductOfferingType__c = offeringType;
        declinedApproverGroup.Priority__c = priority;    
        return declinedApproverGroup;
    }
	
    public static case createCase() {
        SLT_RecordType sltRecordType = new SLT_RecordType();
        Queue_User_Relationship__c queues=new Queue_User_Relationship__c();
        queues.Name ='Q1';
        queues.QueueName__c ='Q1';
        queues.Type__c ='Queue';
        queues.User__c = UserInfo.getUserId(); 
        insert queues;
        Queue_User_Relationship__c queueUser=new Queue_User_Relationship__c();
        queueUser.Name ='Q1';
        queueUser.QueueName__c ='Q1';
        queueUser.Type__c ='User';
        queueUser.User__c = UserInfo.getUserId();
        insert queueUser;
        
        case newCase = new Case(
            Origin = 'Chat',
            Status = 'Request for Approval',
            AssignCaseToCurrentUser__c = false,
            RandD_Location__c = 'Dalian',
            CurrentQueue__c=queues.Id,
            InitialQueue__c = 'Q1',
            OneKeyID__c = 'WFRD00890450',
			OneKey_LastName__c = 'BISMUTH',
            OneKey_FirstName__c = 'ALAIN',
            OwnerId = UserInfo.getUserId(),
            Type = 'Total Erasure',
            RecordTypeId = sltRecordType.getRecordType(CON_CSM_OneKey.S_HCP_ONE_KEY_REQUEST)[0].Id
        );
        return newCase;
    }
	
	public static PermissionSetAssignment createPermissionSetAssignmentRecord(String permissionSetAPIName, Id userId) {
        Id pId = null;
        List<PermissionSet> pList = [SELECT Id FROM PermissionSet WHERE Name = :permissionSetAPIName];
        if(pList.size() > 0) {
            pId = pList[0].Id;
        }
        return new PermissionSetAssignment(PermissionSetId = pId, AssigneeId = userid); 
    }
    
    //Added by Surbhi Singh : 02 March 2019
    /**
     * This method used to insert Sales_Team_Hierarchy__c
     * @return  Sales_Team_Hierarchy__c
     */
    public static Sales_Team_Hierarchy__c createSalesTeamHierarchy(){
        Sales_Team_Hierarchy__c testSalesTeam = new Sales_Team_Hierarchy__c();
        testSalesTeam.Team_Name__c = 'testSalesTeam';
        return testSalesTeam;     
    }
    
    //Added by Surbhi Singh : 02 March 2019
    /**
     * This method used to insert Sales_Team_Hierarchy_ST__c
     * @return  Sales_Team_Hierarchy_ST__c
     */
    public static Sales_Team_Hierarchy_ST__c createSalesTeamHierarchyST(String userEmployeeNumber){
        Sales_Team_Hierarchy_ST__c testSalesTeamHierarchyST = new Sales_Team_Hierarchy_ST__c();
        testSalesTeamHierarchyST.ST_Code__c = userEmployeeNumber;
        testSalesTeamHierarchyST.ST_Name__c = 'testSalesTeamHierarchyST';
        testSalesTeamHierarchyST.VP__C =  [Select Id,VP_Code__c,VP_Name__c from Sales_Team_Hierarchy_VP__c where VP_Name__c = 'testSalesTeamHierarchyVP'][0].Id;
        testSalesTeamHierarchyST.Hierarchy_Id__c= '12305';
        testSalesTeamHierarchyST.Type__c = 'Sales Team';
        return testSalesTeamHierarchyST;     
    }
    
    //Added by Surbhi Singh : 02 March 2019
    /**
     * This method used to insert SalesTeamHierarchy-VP
     * @return  SalesTeamHierarchy-VP
     */
    public static Sales_Team_Hierarchy_VP__c createSalesTeamHierarchyVP(String userEmployeeNumber){
        Sales_Team_Hierarchy_VP__c testSalesTeamHierarchyVP = new Sales_Team_Hierarchy_VP__c();
        testSalesTeamHierarchyVP.VP_Name__c = 'testSalesTeamHierarchyVP';
        testSalesTeamHierarchyVP.VP_Code__c = userEmployeeNumber;
        testSalesTeamHierarchyVP.Hierarchy_Id__c= '1255';
        testSalesTeamHierarchyVP.Type__c = 'Sales Team';
        return testSalesTeamHierarchyVP;
    }
    
    //Added by Surbhi Singh : 02 March 2019
    /**
     * This method used to insert Sales_Team_Hierarchy_AE__c
     * @return  Sales_Team_Hierarchy_AE__c
     */
    public static Sales_Team_Hierarchy_AE__c createSalesTeamHierarchyAE(String userEmployeeNumber){
        Sales_Team_Hierarchy_AE__c testSalesTeamHierarchyAE = new Sales_Team_Hierarchy_AE__c();
        testSalesTeamHierarchyAE.AE_Code__c = userEmployeeNumber;
        Sales_Team_Hierarchy_ST__c testSalesTeamHierarchy = [Select id from Sales_Team_Hierarchy_ST__c where ST_Name__c = 'testSalesTeamHierarchyST'];
        testSalesTeamHierarchyAE.Sales_Team__c = testSalesTeamHierarchy.id;
        testSalesTeamHierarchyAE.Hierarchy_Id__c= '12455';
        testSalesTeamHierarchyAE.Type__c ='Sales Team';
        return testSalesTeamHierarchyAE;     
    }
    
    /**
     * This method used to insert IQVIA_Legal_Entity__c
     * @return  IQVIA_Legal_Entity__c
     */
     
    public static IQVIA_Legal_Entity__c createIQVIALegalEntity() {
        return new IQVIA_Legal_Entity__c(Name = 'Test Entity');
    }

    public static void createOneKeyConfig() {
        OneKey_Config__c oneKey = new OneKey_Config__c(OneKey_Request_1__c = '{"fields": [{"name": "individual.individualEid","values":[',
                                                       OneKey_Request_2__c = '],"method": "EXACT"}],"codBase": [{0}"]}');
        insert oneKey;
    }

    /**
     * This method used to insert Challenge_Matrix__c
     * @return  Challenge_Matrix__c
     */
    
    public static Challenge_Matrix__c createChallengeMatrix(String fees, String opportunityType, String reviewType, String action) {
      return new Challenge_Matrix__c(Fees__c = fees, Opportunity_Type__c = opportunityType,
          Review_Type__c = reviewType, Action__c = action);
    }
    
    /**
     * This method used to insert Approver_Group__c
     * @return  Approver_Group__c
     */

    public static Approver_Group__c createApproverGroup(String name, String type) {
        return new Approver_Group__c(Name=name, Type__c=type);
    }
    
    /**
     * This method used to insert Approval_Matrix__c
     * @return  Approval_Matrix__c
     */

    public static Approval_Matrix__c createApprovalMatrix(Approver_Group__c approverGroup, String opportunityType,
        String region, String therapyArea, String X0_5M_USD, String X10_20M_USD, String X20_50M_USD, String X5_10M_USD, String X50M_USD, String sales) {
          Approval_Matrix__c approvalMatrix = new Approval_Matrix__c(Approver_Group__c = approverGroup.Id,
              Opportunity_Type__c = opportunityType,
              Region__c = region,
              Therapy_Area__c = therapyArea,
              X0_5M_USD__c = X0_5M_USD,
              X5_10M_USD__c = X5_10M_USD,
              X10_20M_USD__c = X10_20M_USD,
              X20_50M_USD__c = X20_50M_USD,
              X50M_USD__c = X50M_USD,
              Sales__c = sales);
              return approvalMatrix;
    }
    
    public static Apttus_CMConfig__AgreementSummaryGroup__c createAgreementSummaryGroup(Apttus__APTS_Agreement__c testAgreement, Decimal extendedPrice) {
      return new Apttus_CMConfig__AgreementSummaryGroup__c(Apttus_CMConfig__AgreementId__c = testAgreement.Id,
          Apttus_CMConfig__ExtendedPrice__c = extendedPrice);
    }
}