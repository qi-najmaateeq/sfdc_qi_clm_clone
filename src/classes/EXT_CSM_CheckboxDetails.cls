public class EXT_CSM_CheckboxDetails {
    public EXT_CSM_CheckboxDetails(String value,String label){
        this.value = value;
        this.label = label;
    }
    public EXT_CSM_CheckboxDetails(String value,String label,Boolean checked){
        this.value = value;
        this.label = label;
        this.checked = checked;
    }
    @AuraEnabled
    public String label{set; get;}
    @AuraEnabled
    public String value{set;get;}
    @AuraEnabled
    public Boolean checked{set;get;}
}