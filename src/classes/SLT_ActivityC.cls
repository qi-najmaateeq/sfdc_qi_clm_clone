public class SLT_ActivityC extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_ActivityC() {
        super(false, true, true);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Activity__c.sObjectType;
    }
    
    /**
     * This method used to get Activity__c by Id
     * @return  Map<Id, Activity__c>
     */
    public Map<Id, Activity__c> selectByActivityId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Activity__c>((List<Activity__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    public List<Activity__c> selectByActivityCaseId(Set<ID> idSet) {
        return Database.query('Select Id,Description__c,CreatedDate From Activity__c  where Case__c = :idSet  order by CreatedDate desc LIMIT 1');
    }
}