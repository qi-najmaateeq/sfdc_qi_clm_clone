/*
 * Version       : 1.0
 * Description   : Test Class for SLT_Asset
 */
@isTest
private class TST_SLT_Asset {
	/**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
    	Product2 product = UTL_TestData.createProduct();
        insert product;
        Account account = UTL_TestData.createAccount();
        insert account;
        Asset asset = new Asset(Name='TestAsset', AccountId = account.Id, Product2Id = product.id);
        insert asset;
    }
    
    /**
     * This method used to get Asset by AccountId and Product2 ID
     */    
    @IsTest
    static void testSelectByAccountIdAndProductId() {
    	List<Asset> assets = new  List<Asset>();
    	Product2 product = [SELECT id FROM Product2 WHERE name = 'TestProduct'];
    	Account account = [SELECT id FROM Account WHERE name = 'TestAccount']; 	
        Test.startTest();
        assets = new SLT_Asset().selectByAccountIdAndProductId(new Set<Id> {account.Id},new Set<Id> {product.Id});
        new SLT_Asset().selectById(new Set<Id> {assets[0].Id});
        new SLT_Asset().selectByAssetId(new Set<Id> {assets[0].Id},new Set<String> {'Id','Name'});
        
        Test.stopTest();
        Integer expected = 1;
        Integer actual = assets.size();
        System.assertEquals(expected, actual);
    }
    
}