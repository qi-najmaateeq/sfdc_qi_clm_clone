/*
* Version       : 1.0
* Description   : Test class for CNT_PEP_SignSOW
*/
@isTest
public class TST_CNT_PEP_SignSOW {
    
    /**
* This method is used to setup data for all methods.
*/
    @testSetup
    static void dataSetup() {
        Account ac = new Account(name ='Acme') ;
        ac.AccountCountry__c = 'BM' ;
        insert ac; 
        
        List<Product2> prodList = new List<Product2>();
        List<Agency_Program__c> agencyprogramList = new List<Agency_Program__c>();
        List<Contract> contractList = new List<Contract>();
        
        Product2 prod = TST_PEP_TestDataFactory.createProduct('Oce Sales');
        Product2 prodmarketing = TST_PEP_TestDataFactory.createProduct('Oce Marketing');
        prodList.add(prod);
        prodList.add(prodmarketing);
        Insert prodList;
        
        Agency_Program__c agencyprog = TST_PEP_TestDataFactory.createAgencyProgram(prod);
        Agency_Program__c agencyprogMark = TST_PEP_TestDataFactory.createAgencyProgram(prodmarketing);
		agencyprogramList.add(agencyprog);
		agencyprogramList.add(agencyprogMark);
        insert agencyprogramList;

        Contract ctr = TST_PEP_TestDataFactory.createContract('Ctr000125', ac.Id, prod.Id);
        Contract ctrMark = TST_PEP_TestDataFactory.createContract('Ctr000126', ac.Id, prodmarketing.Id);
        ctr.Parent_Contract_Number__c = 123;
        ctr.Ultimate_Parent_Contract_Number__c = 345;
        ctrMark.Parent_Contract_Number__c = 123;
        ctrMark.Ultimate_Parent_Contract_Number__c = 345;
        contractList.add(ctr);
        contractList.add(ctrMark);
        insert contractList; 
        
    }
    
    @isTest
    public static void testgetContractId(){
        Agency_Program__c agencyprog = [SELECT Id, Name, Product__c FROM Agency_Program__c WHERE Name = 'OCE Sales'];
        Agency_Program__c agencyprogMark = [SELECT Id, Name, Product__c FROM Agency_Program__c WHERE Name = 'OCE Marketing'];
		Contract ctr = [SELECT Id, Product__c, Name FROM Contract WHERE Name = 'Ctr000125'];
		Contract ctr2 = [SELECT Id, Product__c, Name FROM Contract WHERE Name = 'Ctr000126'];
        
        Contract_Template__c ct = TST_PEP_TestDataFactory.createContractTemplate(agencyprog);
        Contract_Template__c ct1 = TST_PEP_TestDataFactory.createContractTemplate(agencyprog);
        Contract_Template__c ct2 = TST_PEP_TestDataFactory.createContractTemplate(agencyprogMark);
        
        Test.startTest();
        	String returnURL1 = CNT_PEP_SignSOW.getContractId(ctr.Id,'True');
        	String returnURL2 = CNT_PEP_SignSOW.getContractId(ctr2.Id,'False');
        Test.stopTest();
        
        system.assertNotEquals(null, returnURL1);
        system.assertNotEquals(null, returnURL2);
    }
}