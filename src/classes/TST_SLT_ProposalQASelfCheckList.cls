@isTest
private class TST_SLT_ProposalQASelfCheckList {

    static Apttus__APTS_Agreement__c getAgreementData(Id OpportuntiyId, String recordTypeName){

        Id recordTypeId =
        SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.Opportunity_Type__c = CON_CPQ.OPPORTUNITY_RFP;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }

    static Proposal_QA_Self_Check_List__c setProposalQASelfCheckListData(String agreementId, String typeOfProcessStep, String typeOfBid){

        Proposal_QA_Self_Check_List__c proposalQASelfCheckList = UTL_TestData.createProposalQASelfCheckList();
        proposalQASelfCheckList.Agreement__c = agreementId;
        proposalQASelfCheckList.Type_Of_Process_Step__c = typeOfProcessStep;
        proposalQASelfCheckList.Type_Of_Bids__c = typeOfBid;
        insert proposalQASelfCheckList;
        return proposalQASelfCheckList;
    }

    @isTest
    static void testSelectProposalCheckListByIdShouldReturnProposalCheckList() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        insert agreement;
        Proposal_QA_Self_Check_List__c testProposalQASelfCheckList  = setProposalQASelfCheckListData(agreement.Id, 'Draft', CON_CPQ.OPPORTUNITY_RFP);

        Test.startTest();
            List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = new SLT_ProposalQASelfCheckList().selectById(new Set<Id>{testProposalQASelfCheckList.Id});
        Test.stopTest();

    system.assertEquals(1, proposalQASelfCheckList.size(), 'Should Return proposal check list by Id');
    }

    @isTest
    static void testGetProposalCheckListForProcessStepAndBidTypeOfAgreement() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        insert agreement;
        Proposal_QA_Self_Check_List__c testProposalQASelfCheckList  = setProposalQASelfCheckListData(agreement.Id, 'Draft', CON_CPQ.OPPORTUNITY_RFP);
        Set<String> propsalFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT, CON_CPQ.TYPE, CON_CPQ.QUESTION, CON_CPQ.GUIDELINES, CON_CPQ.PD_SELD_REVIEW, CON_CPQ.PD_COMMENT, CON_CPQ.LINE_MANAGER_QC_COMMENT, CON_CPQ.PROPOSAL_QC_COMMNET, CON_CPQ.TYPE_OF_PROPOSAL, CON_CPQ.TYPE_OF_BIDS};

        Test.startTest();
            List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = new SLT_ProposalQASelfCheckList().getProposalCheckListForProcessStepAndBidTypeOfAgreement(
                new Set<Id>{agreement.Id}, propsalFieldSet, CON_CPQ.DRAFT, CON_CPQ.OPPORTUNITY_RFP);
        Test.stopTest();

        system.assertEquals(1, proposalQASelfCheckList.size(), 'Should Return proposal check list ');
    }


    @isTest
    static void testGetProposalCheckListByAgreement() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        insert agreement;
        Proposal_QA_Self_Check_List__c testProposalQASelfCheckList  = setProposalQASelfCheckListData(agreement.Id, 'Draft', CON_CPQ.OPPORTUNITY_RFP);
        Set<String> propsalFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT, CON_CPQ.TYPE, CON_CPQ.QUESTION, CON_CPQ.GUIDELINES, 
            CON_CPQ.PD_SELD_REVIEW, CON_CPQ.PD_COMMENT, CON_CPQ.LINE_MANAGER_QC_COMMENT, CON_CPQ.PROPOSAL_QC_COMMNET, CON_CPQ.TYPE_OF_PROPOSAL, 
            CON_CPQ.TYPE_OF_BIDS};

        Test.startTest();
            List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = new SLT_ProposalQASelfCheckList().getProposalCheckListByAgreement(
                new Set<Id>{agreement.Id}, propsalFieldSet);
        Test.stopTest();

        system.assertEquals(1, proposalQASelfCheckList.size(), 'Should Return proposal check list ');
    }
}