/*
 * Version       : 1.0
 * Description   : Test Class for CNT_CSM_PortalCaseHistory
 */
@isTest
private class TST_CNT_CSM_PortalCaseHistory {
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        Account acc = TST_CSM_Util.createAccount();
        insert acc;
        Contact cnt = TST_CSM_Util.createContact(acc.Id,'CaseTestContact');
        insert cnt;
        Account account = UTL_TestData.createAccount();
        insert account;
        Contact contact = UTL_TestData.createContact(account.Id);
        insert contact;
        Id recordType =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        Queue_User_Relationship__c queues=new Queue_User_Relationship__c();
        queues.Name ='Q1';
        queues.QueueName__c ='Q1';
        queues.Type__c ='Queue';
        queues.User__c = UserInfo.getUserId(); 
        insert queues;
        Queue_User_Relationship__c queueUser=new Queue_User_Relationship__c();
        queueUser.Name ='Q1';
        queueUser.QueueName__c ='Q1';
        queueUser.Type__c ='User';
        queueUser.User__c = UserInfo.getUserId();
        insert queueUser;
        Case c = New Case(Subject = 'TestCase',RecordTypeId=recordType, ContactId = contact.Id, AccountId = account.Id, Status = 'New', Priority = 'Medium', Origin = 'Email',CurrentQueue__c=queues.Id,Current_Queue__c = 'Q1');
        insert c;
        CaseComment cc = new CaseComment(ParentId = c.Id, IsPublished=true,CommentBody='TestComment');
        insert cc;
        CaseHistory ch = new CaseHistory();
        ch.CaseId = c.Id;
        ch.Field = 'Status';
        insert ch;
        EmailMessage em = new EmailMessage();
		em.status = '3';
		em.relatedToId = c.Id;
		em.fromAddress = 'sender@example.com';
		em.fromName = 'Jean Test'; // from name
		em.subject = 'This is the Subject!'; // email subject
		em.htmlBody = '<html><body><b><p>Hello abc@mailinator.com</b></body></html>';
		String[] toIds = new String[]{UserInfo.getUserId()}; 
		em.toIds = toIds;
		insert em; 
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = c.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
    }
    
    /**
     * This method used to get a List of CaseComment for a caseId
     */    
    @IsTest
    static void testGetCaseComments() {
        List<Case> c = new  List<Case>();
        List<CaseComment> ccList = new List<CaseComment>();
        c = [SELECT id FROM Case WHERE Subject = 'TestCase'];
        Test.startTest();
        ccList = CNT_CSM_PortalCaseHistory.getCaseComments(c[0].Id);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = ccList.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get a List of CaseHistory for a caseId
     */    
    @IsTest
    static void testGetCaseHistory() {
        List<Case> c = new  List<Case>();
        List<CaseHistory> ch = new List<CaseHistory>();
        c = [SELECT id, Status FROM Case WHERE Subject = 'TestCase'];
        Test.startTest();
        ch = CNT_CSM_PortalCaseHistory.getCaseHistory(c[0].Id);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = ch.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get a List of EmailMessage for a caseId
     */    
    @IsTest
    static void testGetEmailMessages() {
        List<Case> c = new  List<Case>();
        List<EmailMessage> em = new List<EmailMessage>();
        c = [SELECT id FROM Case WHERE Subject = 'TestCase'];
        Test.startTest();
        em = CNT_CSM_PortalCaseHistory.getEmailMessages(c[0].Id);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = em.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get a List of CNT_CSM_PortalCaseHistory.CSM_CaseHistory for a caseId
     */    
    @IsTest
    static void testGetCSM_CaseHistory() {
        List<Case> c = new  List<Case>();
        List<CNT_CSM_PortalCaseHistory.CSM_CaseHistory> csm_ch = new List<CNT_CSM_PortalCaseHistory.CSM_CaseHistory>();
        c = [SELECT id FROM Case WHERE Subject = 'TestCase'];
        Test.startTest();
        csm_ch = CNT_CSM_PortalCaseHistory.getCSM_CaseHistory(c[0].Id);
        Test.stopTest();
        Integer expected = 4;
        Integer actual = csm_ch.size();
        System.assertEquals(expected, actual);
    }
    
    @IsTest
    static void testInsertCaseComment() {
        List<Case> c = new  List<Case>();
        c = [SELECT id FROM Case WHERE Subject = 'TestCase'];
        CaseComment cc = new CaseComment(ParentId = c[0].Id, IsPublished=true,CommentBody='TestComment2');
        Test.startTest();
        CNT_CSM_PortalCaseHistory.insertCaseComment(cc);
        Test.stopTest();
    }

}