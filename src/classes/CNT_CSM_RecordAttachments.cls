/*
 * Version       : 1.0
 * Description   : Apex Controller for RecordAttachments component.
 */
 
public without sharing class CNT_CSM_RecordAttachments {
    public class CSM_Attachment {
        @AuraEnabled
        public String id {get;set;}
        @AuraEnabled
        public String title {get;set;}
        @AuraEnabled
        public DateTime lastModifiedDate {get;set;}
        @AuraEnabled
        public String createdById {get;set;}
        @AuraEnabled
        public String createdByName {get;set;}
        @AuraEnabled
        public String fileExtension {get;set;}
        @AuraEnabled
        public String contentSize {get;set;}
        @AuraEnabled
        public String parentId {get;set;}
        @AuraEnabled
        public String parentType {get;set;}
        @AuraEnabled
        public String parentName {get;set;}
        @AuraEnabled
        public String  visibility {get;set;}
    }
    @AuraEnabled
    public static boolean getPermissionSets(){
        List<PermissionSetAssignment> permissionAssign = [SELECT AssigneeId,PermissionSet.Name FROM PermissionSetAssignment where PermissionSet.Name = :CON_CSM.S_KB_ARTICLE_MANAGER_PERMISSION_SET and AssigneeId = :UserInfo.getUserId()];
        if(permissionAssign.size()>0){
            return true;
            
        }
        else{
            return false;
        }
    }
    
    @AuraEnabled
    public static boolean getPublishStatus(String recordId){
        List<Knowledge__kav> knowledgelist = [select id,PublishStatus from Knowledge__kav where id =:recordId AND PublishStatus =:'Draft'  ];
        if(knowledgelist.size()>0){
            return true;    
        }
        else{
            return false;
        }
    }
    
    @AuraEnabled
    public static List<CSM_Attachment> getAttachments(String recordId ){
        CSM_Attachment a;
        List<CSM_Attachment> csm_al = new List<CSM_Attachment>();
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT ContentDocumentId,ContentDocument.Title, ContentDocument.LastModifiedDate, ContentDocument.CreatedBy.Id, ContentDocument.CreatedBy.Name, ContentDocument.FileExtension, ContentDocument.ContentSize, LinkedEntityId, Visibility , LinkedEntity.Name , LinkedEntity.Type FROM ContentDocumentLink where LinkedEntityId =:recordId];     
        for(Integer i=0; i< cdl.size();i++){
            a =new CSM_Attachment();
            a.id = cdl[i].ContentDocumentId;
            a.title = cdl[i].ContentDocument.Title;
            a.lastModifiedDate = cdl[i].ContentDocument.LastModifiedDate;
            a.createdById = cdl[i].ContentDocument.CreatedBy.Id;
            a.createdByName = cdl[i].ContentDocument.CreatedBy.Name;
            a.fileExtension = cdl[i].ContentDocument.FileExtension;
            a.contentSize =  String.valueOf(cdl[i].ContentDocument.ContentSize);
            a.parentId = cdl[i].LinkedEntityId;
            a.parentType = cdl[i].LinkedEntity.Type.toLowerCase();
            a.parentName = cdl[i].LinkedEntity.Name;
            a.visibility = cdl[i].Visibility;
            csm_al.add(a);
        }
        
        List<Attachment> al = new List<Attachment>();
        al= [SELECT Id, Name ,LastModifiedDate, CreatedBy.Id, CreatedBy.Name, ContentType, IsPrivate, ParentId, Parent.name FROM Attachment WHERE ParentId IN (
         SELECT Id FROM EmailMessage WHERE ParentId = :recordId)];
         
        for(Integer i=0; i< al.size();i++){
            a =new CSM_Attachment();
            a.id = al[i].Id;
            a.title = al[i].Name;
            a.lastModifiedDate = al[i].LastModifiedDate;
            a.createdById = al[i].CreatedBy.Id;
            a.createdByName = al[i].CreatedBy.Name;
            a.fileExtension = al[i].ContentType;
            a.contentSize = '-';
            a.parentId = al[i].ParentId;
            a.parentType = 'email';
            a.parentName = al[i].Parent.name;
            if(al[i].IsPrivate==false)a.visibility = 'AllUsers';
            else a.visibility = 'Internal';
            csm_al.add(a);
        }
        return csm_al;
    }
  
    @AuraEnabled
    public static void deleteContentDocumentById(String contentDocumentId){
        ContentDocument cd = New ContentDocument (Id=contentDocumentId);
        delete cd;
    }
    /**
     * This method used to update visibility in ContentDocumentLink
     * @params String contentDocumentId
     * @params String visibility,
     */ 
    @AuraEnabled
    public static void updateContentDocumentLinkVisibility(String contentDocumentId, String linkedEntityId, String visibility){
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT Id FROM ContentDocumentLink where ContentDocumentId =:contentDocumentId and LinkedEntityId =:linkedEntityId];
        cdl[0].Visibility = visibility;
        update cdl[0];
    }
    
    @AuraEnabled 
    public static ID currentUser(){
       ID userID = UserInfo.getUserId(); 
         return userID;
    }
    
    
}

