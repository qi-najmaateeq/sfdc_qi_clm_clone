public class CNT_CPQ_UpdateRecordType {

    @AuraEnabled
    public static List<String> getLegacyOppNo(String oppId)
    {
        List<String> responseList = new List<String>();
        String legacyQuintilesOpportunityNumber;
        Map<Id, Opportunity> opportunityMap = new SLT_Opportunity().getOpportunityById(new Set<Id> {oppId}, new Set<String> {CON_CPQ.ID, CON_CPQ.LEGACY_QUINTILES_OPPORTUNITY_NUMBER});
        if(opportunityMap.containsKey(oppId)){
            legacyQuintilesOpportunityNumber = opportunityMap.get(oppId).Legacy_Quintiles_Opportunity_Number__c;
        }
        responseList.add(legacyQuintilesOpportunityNumber + '_Budget_Agreement_Initial_Bid_' + Datetime.now().format('dd-MMM-yyy'));
        responseList.add(UserInfo.getUserId());
        return responseList;
    }

    @AuraEnabled
    public static List<String> getPricingTool(String pricingTool)
    {
        Id recordTypeId;
        String processStep;
        List<String> outputResponse = new List<String>();
        if(pricingTool == CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP || pricingTool == CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_NOBI ||
            pricingTool == CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_POP || pricingTool == CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_UPT){ 
			
            recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId();
            processStep = CON_CPQ.NONE;
            outputResponse.add(recordTypeId);
            outputResponse.add(processStep);
        }
        return outputResponse;       
    }    
}