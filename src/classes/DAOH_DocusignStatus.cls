/**
* This is dsfs__DocuSign_Status__c trigger handler class.
* version : 1.0
*/
public class DAOH_DocusignStatus {

    //private Variable
    static Map < id, CDA_Request__c > cdaRequestUpdateMap = new Map < id, CDA_Request__c > ();
    static Map < String, String > statusWithCDAId = new Map < String, String > ();
    static Set < String > cdaIds = new Set < String > ();
    static List < CDA_Request__c > cdaRequestList = new List < CDA_Request__c > ();
    static Map < String, CDA_Request__c > cdaRequestMapWithCDAId = new Map < String, CDA_Request__c > ();

    /**
    * This method is called prior to execution of a BEFORE trigger. Use this to cache
    * any data required into maps prior execution of the trigger.
    * @params List<dsfs__DocuSign_Status__c> newList
    * @return void
    */
    public static void bulkBefore(List<dsfs__DocuSign_Status__c> newList) {
        String str;
        //List<String> cdaIdsList = new List<String>();
        // for (dsfs__DocuSign_Status__c docusignStatus: (List < dsfs__DocuSign_Status__c > ) trigger.new) {
        for (dsfs__DocuSign_Status__c docusignStatus: newList) {
            if (docusignStatus.dsfs__Subject__c != null && docusignStatus.dsfs__Subject__c.contains('CDA-') && !UTL_CDAUtility.isSkipCDATriggers) {
                str = 'CDA-' + docusignStatus.dsfs__Subject__c.split('-')[1];
                cdaIds.add(str);
                statusWithCDAId.put(docusignStatus.id, str);
            }
        }
        //for(String id: cdaIds){
        //    cdaIdsList.add(id);
        //}
        //String query = 'SELECT id, Name, CDA_Id__c, RecordTypeId FROM CDA_Request__c WHERE CDA_Id__c IN (\''+String.join(cdaIdsList, '\',\'')+'\') AND RecordType.Name != \''+UTL_CDAUtility.historicalDataRecordType+'\'';
        if(!UTL_CDAUtility.isSkipCDATriggers && cdaIds != null && cdaIds.size() > 0) {
            /*String query = 'SELECT id, Name, CDA_Id__c, RecordTypeId FROM CDA_Request__c WHERE CDA_Id__c IN: cdaIds AND RecordType.Name != \''+UTL_CDAUtility.historicalDataRecordType+'\'';
            system.debug('####inside DAOH_DocusignStatus.bulkBefore query: '+query);
            if (cdaIds != null && cdaIds.size() > 0) {
                cdaRequestList = Database.query(query);
                for (CDA_Request__c cdaRequest: cdaRequestList) {
                    cdaRequestMapWithCDAId.put((String)cdaRequest.get('CDA_Id__c'), cdaRequest);
                }
            }*/
            TGRH_CDADocusignStatus.bulkBefore(cdaIds);
        }
    }

    /**
    * This method is called  for list of new record to be inserted during a BEFORE trigger
    * @params List<dsfs__DocuSign_Status__c> newList
    * @return void.
    */
    public static void handleDocusignStatusBeforeInsert(List<dsfs__DocuSign_Status__c> newList) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                CDA_request__c.SobjectType
                    }
        );
        Boolean toCommit = false;
        system.debug('####inside DAOH_DocusignStatus.BeforeInsert cdaIds: '+cdaIds);
        if(!UTL_CDAUtility.isSkipCDATriggers && cdaIds != null && cdaIds.size() > 0) {
            /*if (cdaIds != null && cdaIds.size() > 0) {
                for (dsfs__DocuSign_Status__c docusignStatus: newList) {
                    if (statusWithCDAId != null && statusWithCDAId.size() > 0 && statusWithCDAId.containsKey(docusignStatus.id) && cdaRequestMapWithCDAId != null && cdaRequestMapWithCDAId.size() > 0 && cdaRequestMapWithCDAId.containsKey(statusWithCDAId.get(docusignStatus.Id)) && cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).RecordTypeId != Schema.SObjectType.CDA_Request__c.getRecordTypeInfosByName().get(UTL_CDAUtility.historicalDataRecordType).getRecordTypeId()) {   //Updated by Vikram Singh under CR-11691
                        docusignStatus.CDA_Request__c = cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id;
                        cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status__c = UTL_CDAUtility.STATUS_SENTFORSIGN;
                        cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status_Start_Date__c = System.now(); //Added by Vikram Singh under Issue-11710
                        if (cdaRequestUpdateMap != null && cdaRequestUpdateMap.containsKey(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id)) {
                            cdaRequestUpdateMap.get(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id).Status__c = UTL_CDAUtility.STATUS_SENTFORSIGN;
                            cdaRequestUpdateMap.get(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id).Status_Start_Date__c = System.now(); //Added by Vikram Singh under Issue-11710
                            toCommit = true;
                        } else {
                            cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status__c = UTL_CDAUtility.STATUS_SENTFORSIGN;
                            cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status_Start_Date__c = System.now(); //Added by Vikram Singh under Issue-11710
                            cdaRequestUpdateMap.put(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id, cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)));
                            toCommit = true;
                        }
                    }
                    if(toCommit) {
                        uow.registerDirty(cdaRequestUpdateMap.values());
                    }
                }
                system.debug('####inside DAOH_DocusignStatus.BeforeInsert uow: '+uow);
                if(toCommit) {
                    uow.commitWork();
                }
                SRV_CDA_DocusignStatus.setVoidToDocusignEnvelope(cdaIds);*/
            TGRH_CDADocusignStatus.handleDocusignStatusBeforeInsert(uow, newList, cdaIds, statusWithCDAId, toCommit);
        }
    }

    /**
    * This method is called  for list of new record to be updated during a BEFORE trigger
    * @params List<dsfs__DocuSign_Status__c> newList, Map<Id, dsfs__DocuSign_Status__c> oldMap
    * @return void
    */
    public static void handleDocusignStatusBeforeUpdate(List<dsfs__DocuSign_Status__c> newList, Map<Id, dsfs__DocuSign_Status__c> oldMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                CDA_request__c.SobjectType
                    }
        );
        Boolean toCommit = false;
        system.debug('####inside DAOH_DocusignStatus.BeforeUpdate cdaIds: '+cdaIds);
        if(!UTL_CDAUtility.isSkipCDATriggers && cdaIds != null && cdaIds.size() > 0) {
            /*if (cdaIds != null && cdaIds.size() > 0) {
                for (dsfs__DocuSign_Status__c docusignStatus: newList) {
                    // dsfs__DocuSign_Status__c oldDocusignStatusObj = (dsfs__DocuSign_Status__c)trigger.oldMap.get(docusignStatus.Id);
                    dsfs__DocuSign_Status__c oldDocusignStatusObj = oldMap.get(docusignStatus.Id);
                    if(docusignStatus.dsfs__Envelope_Status__c == UTL_CDAUtility.DS_DECLINED && oldDocusignStatusObj.dsfs__Envelope_Status__c != UTL_CDAUtility.DS_DECLINED && statusWithCDAId != null && statusWithCDAId.size() > 0 && statusWithCDAId.containsKey(docusignStatus.id) && cdaRequestMapWithCDAId != null && cdaRequestMapWithCDAId.size() > 0 && cdaRequestMapWithCDAId.containsKey(statusWithCDAId.get(docusignStatus.Id)) && cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).RecordTypeId != Schema.SObjectType.CDA_Request__c.getRecordTypeInfosByName().get(UTL_CDAUtility.historicalDataRecordType).getRecordTypeId()) {   //Updated by Vikram Singh under CR-11691
                        cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Status__c = UTL_CDAUtility.STATUS_SIGNATURE_REQUEST_DENIED;
                        cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Negotiator_Assigned_List__c = UTL_CDAUtility.NEGO_NOT_ASSIGN;
                        cdaRequestUpdateMap.put(cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)).Id, cdaRequestMapWithCDAId.get(statusWithCDAId.get(docusignStatus.Id)));
                        toCommit = true;
                    }
                    if(toCommit) {
                        uow.registerDirty(cdaRequestUpdateMap.values());
                    }
                }
                system.debug('####inside DAOH_DocusignStatus.BeforeUpdate uow: '+uow);
                if(toCommit) {
                    uow.commitWork();
                }
            }*/
            TGRH_CDADocusignStatus.handleDocusignStatusBeforeUpdate(newList, toCommit, uow, statusWithCDAId, oldMap);
        }
    }

    /**
    * This method is called for list of expired envelope record during a AFTER trigger
    * @params Map<Id, dsfs__DocuSign_Status__c> oldMap
    * @return void
    */
    public static void cancelRequestsHavingExpiredEnvelope(List<dsfs__DocuSign_Status__c> newList, Map<Id, dsfs__DocuSign_Status__c> oldMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                CDA_request__c.SobjectType
                    }
        );
        Boolean toCommit = false;
        system.debug('####inside DAOH_DocusignStatus.AfterUpdate cdaIds: '+cdaIds);
        if(!UTL_CDAUtility.isSkipCDATriggers && cdaIds != null && cdaIds.size() > 0) {
            TGRH_CDADocusignStatus.cancelRequestsHavingExpiredEnvelope(newList, toCommit, uow, statusWithCDAId, oldMap);
        }
    }

    public static void updateDocuSign_SignedStatusRecords(List<dsfs__DocuSign_Status__c> statusList )
    {
        map<String,String> envelopMap = new map<String,String>();
        for(dsfs__DocuSign_Status__c ds: statusList)
        {
            //envelopId.add(ds.dsfs__DocuSign_Envelope_ID__c);
            envelopMap.put(ds.dsfs__DocuSign_Envelope_ID__c, ds.Id);
        }
        
         if(!envelopMap.isEmpty()){
             
         List<Contract> listToUpdate = new List<Contract>();
            // for(Contract c : [Select Id, DocuSign_Envelope_Id__c, DocusignStatusId__c
               //  From Contract Where DocuSign_Envelope_Id__c IN : envelopMap.keySet()]){
          for(Contract c : getListOfContract(envelopMap.keySet())){
             c.DocusignStatusId__c = envelopMap.get(c.DocuSign_Envelope_Id__c);
             listToUpdate.add(c);
             }
             //update listToUpdate;
             updateListOfContract(listToUpdate);
         } 
    }
     @testVisible private static void updateListOfContract( List<Contract> listToUpdate)
     {
        if(!useMock) {
            update listToUpdate;
        }
    }
    
    @testVisible private static List<Contract> getListOfContract(Set<String> envelopKeySet) {
          List<Contract> cnt = new List<Contract>();
        if(!useMock) {
          
            cnt=[Select Id, DocuSign_Envelope_Id__c, DocusignStatusId__c
                 From Contract Where DocuSign_Envelope_Id__c IN : envelopKeySet];
        }
        else{
            cnt=testContractList;
        }
        return cnt;
    }
    @testvisible static List<Contract> testContractList;
    @testVisible static Boolean useMock = false;
}
