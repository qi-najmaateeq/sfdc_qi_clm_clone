public class DAOH_CaseComment {
    
    public static void updateStatusforLogAWorkNoteonCase(List<CaseComment> newList){
        List<Case> updateCase = new List<Case>();
        Map<Id,String> caseIds = new Map<Id,String>();
        boolean IsPublished = false;
        boolean InitialResonseProdOpps = false;
        List<Id> technoCaseId = new List<Id>();
        List<Id> technoCaseIdProd = new List<Id>();
        List<Case> caseList = null;
        Id userId = UserInfo.getProfileId();
        for(CaseComment c : newList) {
            IsPublished = c.IsPublished;
            if(c.ParentId != null && String.isNotBlank(c.CommentBody) && !c.CommentBody.startsWith(CON_CSM.S_Case_Description)){
                caseIds.put(c.ParentId,c.CommentBody);
            }
        }
        if(!caseIds.isEmpty()){
            Map<ID,Schema.RecordTypeInfo> rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
            caseList = new SLT_Case().selectById(caseIds.keySet()); 
            boolean checkUpdate = false;
            for(Case cs : caseList){
                checkUpdate = false;
                if(IsPublished && (rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_DATA_CASE_R_T) ||  rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_TECHNOLOGY_R_T)) && (CON_PEP.S_PARTNER_PORTAL.equalsIgnoreCase(cs.Origin))){
                    cs.LastCaseComment__c = caseIds.get(cs.Id);
                    checkUpdate = true;
                } 
                if(rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_TECHNOLOGY_R_T) && (CON_CSM.S_EMAIL.equalsIgnoreCase(cs.Origin) || CON_CSM.S_CUSTOMER_PORTAL.equalsIgnoreCase(cs.Origin)) && string.valueOf(cs.OwnerId).startsWith(CON_CSM.S_QUEUE_ID) && CON_CSM.S_NEW.equalsIgnoreCase(cs.Status)){
                    if(cs.LastCaseComment__c != null && !cs.LastCaseComment__c.startsWith(CON_CSM.S_Case_Description)){
                        cs.Status = CON_CSM.S_IN_PROGRESS;
                        checkUpdate = true;
                    }
                }
                if(rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_TECHNOLOGY_R_T) && !CON_CSM.S_CLOSED.equalsIgnoreCase(cs.Status) && UserInfo.getUserType().contains('CustomerSuccess')){
                    if(cs.LastCaseComment__c != null && !cs.LastCaseComment__c.startsWith(CON_CSM.S_Case_Description)){
                        cs.Status = CON_CSM.S_IN_PROGRESS;
                        cs.SubStatus__c = CON_CSM.S_RESPONSE_RECEIVED;
                        checkUpdate = true;
                    }
                }
                if(IsPublished && (rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_DATA_CASE_R_T) ||  rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_TECHNOLOGY_R_T)) && CON_CSM.S_CUSTOMER_PORTAL.equalsIgnoreCase(cs.Origin)){
                    cs.LastCaseComment__c = caseIds.get(cs.Id);
                    if(cs.LastCaseComment__c != null && !cs.LastCaseComment__c.startsWith(CON_CSM.S_Case_Description)){
                        if(UserInfo.getUserType().contains(CON_CSM.S_CUSTOMER_SUCCCESS) ){
                            cs.Owner__c = UserInfo.getUserType();    
                        }else{
                            cs.Owner__c = UserInfo.getUserId();    
                        }    
                    }
                    checkUpdate = true;
                }
                if(IsPublished && rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_TECHNOLOGY_R_T) && (CON_CSM.S_EMAIL.equalsIgnoreCase(cs.Origin) || CON_CSM.S_CUSTOMER_PORTAL.equalsIgnoreCase(cs.Origin))){
                    DateTime completionDate = System.now();
                    if (((cs.SlaStartDate <= completionDate) && (cs.SlaExitDate == null))){
                        if(cs.TaskMilestone__c != null && !cs.TaskMilestone__c.contains(CON_CSM.S_FIRST_RESPONSE_T)){
                            cs.TaskMilestone__c += ';' + CON_CSM.S_FIRST_RESPONSE_T;
                            checkUpdate = true;
                            technoCaseId.add(cs.Id);
                        }else if(cs.TaskMilestone__c == null){
                            cs.TaskMilestone__c = CON_CSM.S_FIRST_RESPONSE_T; 
                            checkUpdate = true;
                            technoCaseId.add(cs.Id);
                        }  
                    }
                }else if(IsPublished && rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_TECHNOLOGY_R_T)){
                    technoCaseIdProd.add(cs.Id);
                    InitialResonseProdOpps = true;
                    checkUpdate = true;
                }
                if(checkUpdate){
                    updateCase.add(cs);
                }
                
            }
            
            if(!updateCase.isEmpty()){
                try{
                    update updateCase;
                    sendCaseCommentCCEmails(updateCase);
                    if(UserInfo.getUserType().contains(CON_CSM.S_CUSTOMER_SUCCCESS) ){
                        sendLatestCommenttoDataCaseOriginator(updateCase);
                    }
                }catch (DmlException e) {
                    System.debug('Failed due to : '+e);
                }
                
            }
            
            if(!technoCaseId.isEmpty()){
                DAOH_Case.completeMilestone(technoCaseId, new List<String>{CON_CSM.S_FIRST_RESPONSE_T,CON_CSM.S_PRODOPS_INITIAL_RESPONSE}, System.now()); 
            }else if(!technoCaseIdProd.isEmpty() && InitialResonseProdOpps){
                DAOH_Case.completeMilestone(technoCaseIdProd, new List<String>{CON_CSM.S_PRODOPS_INITIAL_RESPONSE}, System.now());
            }
        }
    }

    /* Added by Saurabh */
    
    public static boolean isCSMccEmailsHaveSent = false;
    public static void sendCaseCommentCCEmails(List<Case> newList){
        if(!isCSMccEmailsHaveSent) {
            List<String> toAddresses;
            List<Messaging.SingleEmailMessage> ccSendEmailList = new List<Messaging.SingleEmailMessage>();
            Map<ID,Schema.RecordTypeInfo> rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
            for(Case cs : newList){
                if(rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_TECHNOLOGY_R_T)){
                    toAddresses = new List<String>();
                    if(cs.Mail_CC_List__c != null && cs.Mail_CC_List__c != ''){
                        for(string emailId : cs.Mail_CC_List__c.split(';')) {
                            toAddresses.add(emailId.trim());
                        }                        
                        
                        Messaging.SingleEmailMessage mailMessage = new Messaging.SingleEmailMessage();
                        if(DAOH_Case.getCSMSettingData().get('PEP~CaseComment') != null) {
                            mailMessage.setTemplateId(DAOH_Case.getCSMSettingData().get('PEP~CaseComment').Component_Id__c);
                        }
                        else if(DAOH_Case.getCSMSettingData().get(cs.AccountCountry__c + '~CaseComment') != null) {
                            mailMessage.setTemplateId(DAOH_Case.getCSMSettingData().get(cs.AccountCountry__c + '~CaseComment').Component_Id__c);
                        }
                        else if(DAOH_Case.getCSMSettingData().get('Default~CaseComment') != null) {
                            mailMessage.setTemplateId(DAOH_Case.getCSMSettingData().get('Default~CaseComment').Component_Id__c);
                        }
                        else{
                            continue;
                        }
                        String senderId = '';
                        if(CON_PEP.S_PARTNER_PORTAL.equalsIgnoreCase(cs.Origin) && DAOH_Case.getCSMSettingData().get(CON_PEP.S_PARTNER_PORTAL) != null) {
                            senderId = DAOH_Case.getCSMSettingData().get(CON_PEP.S_PARTNER_PORTAL).Component_Id__c;
                        }
                        else if(CON_CSM.S_CUSTOMER_PORTAL.equalsIgnoreCase(cs.Origin) && DAOH_Case.getCSMSettingData().get(CON_CSM.S_CUSTOMER_PORTAL) != null) {
                            senderId = DAOH_Case.getCSMSettingData().get(CON_CSM.S_CUSTOMER_PORTAL).Component_Id__c;
                        }
                        else if(CON_CSM.S_AGENT_INITIATED.equalsIgnoreCase(cs.Origin) && DAOH_Case.getCSMSettingData().get(CON_CSM.S_AGENT_INITIATED) != null) {
                            senderId = DAOH_Case.getCSMSettingData().get(CON_CSM.S_AGENT_INITIATED).Component_Id__c;
                        }
                        if(senderId != null && senderId.length() > 0) {
                            mailMessage.setOrgWideEmailAddressId(senderId);
                        }
                        mailMessage.setSaveAsActivity(false);
                        mailMessage.setTargetObjectId(cs.ContactId);
                        mailMessage.setTreatTargetObjectAsRecipient(false);
                        mailMessage.setToAddresses(toAddresses);
                        mailMessage.setWhatId(cs.Id);
                        ccSendEmailList.add(mailMessage);
                    }
                }
            }            
            if(ccSendEmailList != null && ccSendEmailList.size() > 0) {
                try{
                    Messaging.sendEmail(ccSendEmailList);
                }
                catch(Exception ex){
                    System.debug('Mail Exception: '+ex.getMessage());
                }
            }
            isCSMccEmailsHaveSent = true;
        }
    }
    
    /* Added by Saurabh */
    
    public static void sendLatestCommenttoDataCaseOriginator(List<Case> newList) {
        Map<ID,Schema.RecordTypeInfo> rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
        Map<Id,Id> userList = new Map<Id,Id>();
        Map<Id,Id> queueList = new Map<Id,Id>();
        List<String> toAddresses = new List<String>();
        for (Case c : newList) {
            if(c.CSM_QI_Data_Originator__c != null && (c.CSM_QI_Data_Originator__c.startsWith(CON_CSM.S_QUEUE_ID) || c.CSM_QI_Data_Originator__c.startsWith('005')) && rt_map.get(c.recordTypeID).getName().contains(CON_CSM.S_DATA_CASE_R_T) && UserInfo.getUserType().contains('CustomerSuccess')) {
                queueList.put(c.CSM_QI_Data_Originator__c,c.Id); 
            }
        }
        if(!queueList.isEmpty()){
            List<Queue_User_Relationship__c> queueUser = new SLT_QueueUserRelationshipC().selectByGroupIdSet(queueList.keySet());
            if(!queueUser.isEmpty()){
                Pattern patt = Pattern.compile(CON_CSM.S_EMAIL_REGEXP);
                Matcher match = null;
                String emailId;
                for (Queue_User_Relationship__c c : queueUser) {
                    emailId = (CON_CSM.S_QUEUE.equalsIgnoreCase(c.Type__c) && c.Queue_Email__c != null) ? c.Queue_Email__c : c.User_Email__c;
                    if(emailId != null){
                        match = patt.matcher(emailId.trim());
                        if(match.matches())
                            toAddresses.add(emailId);    
                    }
                       
                }
            }
        }
        if(!toAddresses.isEmpty()){
            List<Messaging.SingleEmailMessage> dataSendEmailList = new List<Messaging.SingleEmailMessage>();
            for(Case cs : newList){
                Messaging.SingleEmailMessage mailMessage = new Messaging.SingleEmailMessage();
                if(DAOH_Case.getCSMSettingData().get(cs.AccountCountry__c + CON_CSM.SPC_TILDE + CON_CSM.S_ORG_CASECOMMENT) != null) {
                    mailMessage.setTemplateId(DAOH_Case.getCSMSettingData().get(cs.AccountCountry__c + CON_CSM.SPC_TILDE + CON_CSM.S_ORG_CASECOMMENT).Component_Id__c);
                }
                else if(DAOH_Case.getCSMSettingData().get(CON_CSM.S_DEFAULT + CON_CSM.SPC_TILDE + CON_CSM.S_ORG_CASECOMMENT) != null) {
                    mailMessage.setTemplateId(DAOH_Case.getCSMSettingData().get(CON_CSM.S_DEFAULT + CON_CSM.SPC_TILDE + CON_CSM.S_ORG_CASECOMMENT).Component_Id__c);
                }
                else{
                    continue;
                }
                String senderId = '';
                if(CON_CSM.S_CUSTOMER_PORTAL.equalsIgnoreCase(cs.Origin) && DAOH_Case.getCSMSettingData().get(CON_CSM.S_CUSTOMER_PORTAL) != null) {
                    senderId = DAOH_Case.getCSMSettingData().get(CON_CSM.S_CUSTOMER_PORTAL).Component_Id__c;
                }
                else if(CON_CSM.S_AGENT_INITIATED.equalsIgnoreCase(cs.Origin) && DAOH_Case.getCSMSettingData().get(CON_CSM.S_AGENT_INITIATED) != null) {
                    senderId = DAOH_Case.getCSMSettingData().get(CON_CSM.S_AGENT_INITIATED).Component_Id__c;
                }
                if(senderId != null && senderId.length() > 0) {
                    mailMessage.setOrgWideEmailAddressId(senderId);
                }
                mailMessage.setSaveAsActivity(false);
                mailMessage.setTargetObjectId(cs.ContactId);
                mailMessage.setTreatTargetObjectAsRecipient(false);
                mailMessage.setToAddresses(toAddresses);
                mailMessage.setWhatId(cs.Id);
                dataSendEmailList.add(mailMessage);                            
                
            }
            if(dataSendEmailList != null && dataSendEmailList.size() > 0) {
                try{
                    Messaging.sendEmail(dataSendEmailList);
                }
                catch(Exception ex){
                    System.debug('Mail Exception: '+ex.getMessage());
                }
            }
        }
    }
    
    public static void updateCSATInternalObject(List<CaseComment> newList) {
        List<Case> caseList = null;
        for(CaseComment comment : newList) {
           caseList = new SLT_Case().selectById(new Set<ID> {comment.ParentId});
           CSM_QI_CSATInternal__c cr;
           List<CSM_QI_CSATInternal__c> crl = new List<CSM_QI_CSATInternal__c>();
           crl = [SELECT Id from CSM_QI_CSATInternal__c where CommentId__c =: comment.Id];
           if (crl.size() > 0) cr = crl[0];
           else cr = new CSM_QI_CSATInternal__c();
           cr.CommentId__c = comment.Id;
           cr.CommentBody__c = comment.CommentBody;
           cr.CommentParentId__c = comment.ParentId;
           cr.CommentIsPublished__c = comment.IsPublished;
           cr.CommentCreatedById__c = comment.CreatedById;
           cr.CommentCreatedDate__c = comment.CreatedDate;
           cr.CommentLasModifiedById__c = comment.LastModifiedById;
           cr.CommentLastModified__c = comment.LastModifiedDate;
           cr.CommentIsDeleted__c = comment.IsDeleted;
           upsert cr;
        }
    }
    
    public static void deleteCSATInternalObject(List<CaseComment> newList) {
        for(CaseComment comment : newList) {
           List<CSM_QI_CSATInternal__c> crl = new List<CSM_QI_CSATInternal__c>();
           crl = [SELECT Id from CSM_QI_CSATInternal__c where CommentId__c =: comment.Id];
           if (crl.size() > 0) delete crl[0];
        }
    }
}
