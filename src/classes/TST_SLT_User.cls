/*
 * Version       : 1.0
 * Description   : Test Class for SLT_User
 */
@isTest
private class TST_SLT_User {
    /**
     * This method used to get Asset by AccountId and Product2 ID
     */    
    @IsTest
    static void testSelectById() {
        List<User> users = new  List<User>();
        Account account_1 = new Account( Name = 'Community'  );
        insert account_1;
        Contact contact_1 = new Contact(AccountId = account_1.Id, LastName = 'xgeek');
        insert contact_1;
        Test.startTest();
        users = new SLT_User().selectById(new Set<Id> {UserInfo.getUserId()});
        List<User> userList = new SLT_User().selectByContactId(new Set<Id> {contact_1.Id},new Set<String>{CON_CSM.S_ID,CON_CSM.S_NAME,CON_CSM.S_CONTACTID_C});
        Test.stopTest();
        Integer expected = 1;
        Integer actual = users.size();
        System.assertEquals(expected, actual);
    }

    @IsTest
    static void testSelectActiveUserByUserId() {
        List<User> userList = UTL_TestData.createUser('Standard User', 1);
        userList[0].IsActive = true;
        insert userList;
        List<User> users = new  List<User>();
        Test.startTest();
        users = new SLT_User().selectActiveUserByUserId(new Set<Id> {userList[0].Id}, new Set<String>{'Id'});
        Test.stopTest();
        Integer expected = 1;
        Integer actual = users.size();
        System.assertEquals(expected, actual);
    }
    
    @IsTest
    static void testSelectUserByUserId() {
        List<User> userList = UTL_TestData.createUser('Standard User', 1);
        userList[0].IsActive = true;
        insert userList;
        User user;
        Test.startTest();
        user = new SLT_User().selectUserByUserId(new Set<Id> {userList[0].Id});
        Test.stopTest();
        System.assertEquals(true, user != null);
    }
    
    @IsTest
    static void testSelectUserByName() {
        List<User> userList = UTL_TestData.createUser('Standard User', 1);
        userList[0].IsActive = true;
        insert userList;
        List<User> users = new List<User>();
        Test.startTest();
        users = new SLT_User().selectUserByName('lastName123');
        Test.stopTest();
        Integer expected = 1;
        Integer actual = users.size();
        System.assertEquals(expected, actual);
    }

    @IsTest
    static void testSelectUserByAccountId() {
        User partnerUser = TST_PEP_TestDataFactory.createPartnerUser('partneruser@iqvia.partner.com');
        insert partnerUser;

        User testPartner = [SELECT id, AccountId FROM User WHERE email='partneruser@iqvia.partner.com'];

        Test.startTest();
            List<User> userListAst = new SLT_User().selectUserByAccountId(new Set<Id>{testPartner.AccountId});
            System.assertEquals(1, userListAst.size());
        Test.stopTest();

    }

    @IsTest
    static void testSelectManagerUserByAccountId() {
        User partnerUser = TST_PEP_TestDataFactory.createPartnerUser('partneruser@iqvia.partner.com');
        partnerUser.PortalRole = 'Manager';
        insert partnerUser;

        User testPartner = [SELECT id, AccountId FROM User WHERE email='partneruser@iqvia.partner.com'];

        Test.startTest();
            List<User> userListAst = new SLT_User().selectManagerUserByAccountId(new Set<Id>{testPartner.AccountId});
            System.assertEquals(1, userListAst.size());
        Test.stopTest();

    }
	
    @IsTest
    static void testSelectMapOfActiveUserByUserId() {
        List<User> userList = UTL_TestData.createUser('Standard User', 1);
        userList[0].IsActive = true;
        insert userList;

        Test.startTest();
        	Map<Id,User> mapOfUser = new SLT_User().selectMapOfActiveUserByUserId(new Set<Id>{userList[0].Id}, new Set<String>{'Id','Name'});
            System.assertEquals(1, mapOfUser.size());
        Test.stopTest();

    }
	
    @IsTest
    static void testSelectContactDetailByUser() {
        List<User> userList = UTL_TestData.createUser('Standard User', 1);
        userList[0].IsActive = true;
        insert userList;

        Test.startTest();
            List<User> ListOfUser = new SLT_User().selectContactDetailByUser(new Set<Id>{userList[0].Id}, new Set<String>{'Id','Name'});
            System.assertEquals(1, ListOfUser.size());
        Test.stopTest();

    }
}