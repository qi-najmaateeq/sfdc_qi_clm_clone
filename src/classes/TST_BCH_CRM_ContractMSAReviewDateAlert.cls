@isTest
public class TST_BCH_CRM_ContractMSAReviewDateAlert {
    
    /**
     * method to create data for Batch
     */ 
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        List<User> userList = UTL_TestData.createUser('System Administrator',1);
        insert userList;
        Contract contract = new Contract();
        contract.OwnerId = userList[0].Id;
        contract.AccountId = acc.Id;
        contract.Specific_Contract_Type__c = 'Master Independent Contractor Agreement';
        contract.Date_on_which_MSA_should_be_reviewed__c = System.today();
        insert contract;    
    }
    
    /**
     * test method to check BCH_CRM_ContractMSAReviewDateAlert Batch
     */ 
    public static testMethod void testBCHCRMContractMSAReviewDateAlertBatch() {
        Test.StartTest();
        Database.executeBatch(new BCH_CRM_ContractMSAReviewDateAlertBatch()); 
        Test.stopTest();
    }
}