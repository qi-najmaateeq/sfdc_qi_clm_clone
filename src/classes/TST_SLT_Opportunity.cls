/*
* Version       : 1.0
* Description   : This test class is used for Select Opportunity
*/
@isTest
private class TST_SLT_Opportunity {
    
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Contact cnt = UTL_TestData.createContact(acc.Id);
        insert cnt;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        opp.Opportunity_Number__c = '124';
        opp.Legacy_Quintiles_Opportunity_Number__c = '12';
        insert opp;
        Product2 product = UTL_TestData.createProduct();
        insert product;
        PricebookEntry pbEntry = UTL_TestData.createPricebookEntry(product.Id);
        insert pbEntry;
        OpportunityLineItem oppLineItem = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
        insert oppLineItem;
        List<User> userList = UTL_TestData.createUser(CON_CRM.SYSTEM_ADMIN_PROFILE, 1);
        insert userList;
        OpportunityTeamMember teamMember = UTL_TestData.createOpportunityTeamMember(opp.Id, userList[0].Id);
        insert teamMember;
        Id splitTypeId = SRV_CRM_OpportunitySplit.getOpportunitySplitTypeIdByName(CON_CRM.SPLIT_TYPE_NAME);
        pse__Proj__c project = new pse__Proj__c(pse__Is_Billable__c = false, pse__Opportunity__c = opp.Id);
        project.Name = 'Annual Leave/Vacation';
        insert project;
        System.runAs(userList[0]) {
            OpportunitySplit oppSplit = UTL_TestData.createOpportunitySplit(opp.Id, userList[0].Id, splitTypeId);
            insert oppSplit;
        }
        Product2 product2 = UTL_TestData.createProduct();
        insert product2;
        PricebookEntry pbe = UTL_TestData.createPricebookEntry(product2.Id);
        insert pbe;
        OpportunityLineItem oli = UTL_TestData.createOpportunityLineItem(opp.Id, pbe.Id);
        insert oli;
        List<Address__c> addressList = UTL_TestData.createAddresses(acc);
        insert addressList;
        Revenue_Analyst__c revenueAnalyst = UTL_TestData.createRevenueAnalyst();
        insert revenueAnalyst;
        BNF2__c bnf2 = UTL_TestData.createBNFRecord(opp, oli, addressList, revenueAnalyst.Id);
        insert bnf2;
        MIBNF2__c mibnf2 = UTL_TestData.createMIBNF(opp, revenueAnalyst);
        insert mibnf2;
    }
    
    @isTest
    static void testGetOpportunityById() {
        Opportunity opp = [SELECT id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<String> oppFieldSet = new Set<String> {'Id'};
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().getOpportunityById(new Set<Id> { opp.Id }, oppFieldSet);
        Test.stopTest();
        System.assertEquals(opp.Id, idToOpportunityMap.get(opp.Id).Id); 
    }
    
    @isTest
    static void testSelectByIdWithOpportunitySplit() {
        Opportunity opp = [SELECT id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Set<String> oppSplitFieldSet = new Set<String>{'Id'};
        Set<String> oppTeamFieldSet = new Set<String>{'Id'};    
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectByIdWithOpportuntiyTeamSplits(new Set<Id>{opp.Id}, oppFieldSet, oppSplitFieldSet, oppTeamFieldSet, CON_CRM.SPLIT_TYPE_NAME);
        List<Opportunity> oppList = idToOpportunityMap.values();
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppList.size();
        System.assertEquals(expected, actual);
    }
    
    static testMethod void testSelectByIdWithOpportuntiyLineItem() {
        Set<Id> oppIdSet = new Set<Id>();
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Set<String> oliFieldSet = new Set<String>{'Id'};
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
     	oppIdSet.add(opp.Id);
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().SelectByIdWithOpportuntiyLineItem(oppIdSet, oppfieldSet, oliFieldSet);
        List<Opportunity> oppList = idToOpportunityMap.values();
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppList.size();
        System.assertEquals(expected, actual);
    }
    
    static testMethod void testSelectAgreementByOpportunity() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<Id> oppIdSet = new Set<Id>();
        oppIdSet.add(opp.Id);
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Set<Id> agreementRecordTypesSet = new Set<Id>();
        Set<String> agreementFieldsSet = new Set<String>{'Id'}; 
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectAgreementByOpportunity(oppIdSet, oppfieldSet, agreementRecordTypesSet, agreementFieldsSet);
        Test.stopTest();
        System.assertEquals(1, idToOpportunityMap.size());  
    }
    
    static testMethod void testSelectByIdWithOpportuntiyAgreement(){
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<Id> oppIdSet = new Set<Id>();
        oppIdSet.add(opp.Id);
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Set<String> agreementFieldSet = new Set<String>{'Id'};
        Integer recordLimit = 1;
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectByIdWithOpportuntiyAgreement(oppIdSet, oppfieldSet, agreementFieldSet);
        List<Opportunity> oppList = idToOpportunityMap.values();
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppList.size();
        System.assertEquals(expected, actual);
    }
    
    static testMethod void testSelectByIdWithOpportuntiyAgreementWithCondition(){
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<Id> oppIdSet = new Set<Id>();
        oppIdSet.add(opp.Id);
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Set<String> agreementFieldSet = new Set<String>{'Id'};
        Integer recordLimit = 1;
        String agrCondition = 'name = \'Test Agr\'';
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectByIdWithAgrCondtion(oppIdSet, oppfieldSet, agreementFieldSet,agrCondition );
        List<Opportunity> oppList = idToOpportunityMap.values();
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppList.size();
        System.assertEquals(expected, actual);
    }
    
    @isTest
    static void testSelectAgreementByOpportunities() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<Id> oppIdSet = new Set<Id>();
        oppIdSet.add(opp.Id);
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Set<String> agreementFieldSet = new Set<String>{'Id'};
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectAgreementByOpportunities(oppIdSet, oppfieldSet, agreementFieldSet);
        List<Opportunity> oppList = idToOpportunityMap.values();
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppList.size();
        System.assertEquals(expected, actual);
    }
    
    @isTest
    static void testGetOpportunityByFieldCondition(){
        Set<String> fieldSet = new Set<String>{'Id', 'Name'};
        List<String> fieldValueList = new List<String>{'124'};
        Test.startTest();
        List<Opportunity> oppList = new SLT_Opportunity().getOpportunityByFieldCondition(fieldSet, fieldValueList);
        Test.stopTest();
        Integer expected = 0;
        Integer actual = oppList.size();
        System.assertEquals(expected, actual);
    }
    
    @isTest
    static void testSelectByIdWithBNF() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<Id> oppIdSet = new Set<Id>();
        oppIdSet.add(opp.Id);
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Set<String> bnfFieldSet = new Set<String>{'Id'};
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectByIdWithBNF(oppIdSet, oppfieldSet, bnfFieldSet);
        Test.stopTest();
        System.assertEquals(opp.Id, idToOpportunityMap.get(opp.Id).Id);      
    }
    
    @isTest
    static void testSelectByIdWithMIBNF() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<Id> oppIdSet = new Set<Id>();
        oppIdSet.add(opp.Id);
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Set<String> mibnfFieldSet = new Set<String>{'Id'};
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectByIdWithMIBNF(oppIdSet, oppfieldSet, mibnfFieldSet);
        Test.stopTest();
        System.assertEquals(opp.Id, idToOpportunityMap.get(opp.Id).Id); 
    }
    
    @isTest
    static void testSelectByOpportunityId() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<Id> oppIdSet = new Set<Id>();
        oppIdSet.add(opp.Id);
        Set<String> oppfieldSet = new Set<String>{'Id'};
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectByOpportunityId(oppIdSet, oppfieldSet);
        Test.stopTest();
        System.assertEquals(opp.Id, idToOpportunityMap.get(opp.Id).Id); 
    }
    
    @isTest
    static void testSelectByIdWithLineItemGroups() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<String> oppfieldSet = new Set<String>{'Id'};
        List<String> oppFieldsGroupSet = new List<String>{'12'};
        Set<String> lineItemFieldsSet = new Set<String>{'Id'};
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().selectByIdWithLineItemGroups(oppfieldSet, oppFieldsGroupSet, lineItemFieldsSet);
        Test.stopTest();
        System.assertEquals(opp.Id, idToOpportunityMap.get(opp.Id).Id); 
    } 
    
    @isTest
    static void testGetMapOfOpportunitiesWithProjectsById(){
        Opportunity opp = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        Test.startTest();
        Map<Id, Opportunity> idToOpportunityMap = new SLT_Opportunity().getMapOfOpportunitiesWithProjectsById(new Set<Id>{opp.id});
        Test.stopTest();
        System.assertEquals(false, idToOpportunityMap.isEmpty(),'Map is not fetched');
        System.assertEquals(1, idToOpportunityMap.size(),'Map is not fetched');
    }
}