@isTest
private class TST_CNT_CPQ_QCSelfCheckFinalContract {

    static Apttus__APTS_Agreement__c getAgreementData(Id OpportuntiyId, String recordTypeName){

        Id recordTypeId =
        SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_FINAL;
        testAgreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        testOpportunity.Opportunity_Type__c = COn_CPQ.OPPORTUNITY_RFP;
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }

    static QC_Check_List_Item__c setQCCheckListItemData(String recordTypeName){
    
        QC_Check_List_Item__c qcCheckListItem = UTL_TestData.createQCCheckListItem();
        qcCheckListItem.Record_Type__c = recordTypeName;
        insert qcCheckListItem;
        return qcCheckListItem;
    }

    static Proposal_QA_Self_Check_List__c setProposalQASelfCheckListData(String agreementId){
    
        Proposal_QA_Self_Check_List__c proposalQASelfCheckList = UTL_TestData.createProposalQASelfCheckList();
        proposalQASelfCheckList.Agreement__c = agreementId;
        insert proposalQASelfCheckList;
        return proposalQASelfCheckList;
    }

    static List<Proposal_QA_Self_Check_List__c> getProposalQASelfCheckListData(String agreementId){
        return [SELECT Id FROM Proposal_QA_Self_Check_List__c WHERE Agreement__c =: agreementId];
    }

    @isTest
    static void testFetchCheckListRecordsShouldReturnQCListForChangeOrder(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER);
        insert agreement;
        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData(CON_CPQ.CHNAGE_ORDER);

        Test.startTest();
            List<CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper> qcCheckListForDraft =
                CNT_CPQ_QCSelfCheckFinalContract.fetchQCCheckListRecords(agreement.Id);
        Test.stopTest();

        system.assertEquals(1, qcCheckListForDraft.size(),'Should Return QC List For Change Order');
    }

    @isTest
    static void testFetchCheckListRecordsShouldReturnQCListForCNF(){
        
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CNF);
        insert agreement;
        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData(CON_CPQ.CNF);

        Test.startTest();
            List<CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper> qcCheckListForDraft =
                CNT_CPQ_QCSelfCheckFinalContract.fetchQCCheckListRecords(agreement.Id);
        Test.stopTest();

        system.assertEquals(1, qcCheckListForDraft.size(),'Should Return QC List For CNF');
    }

    @isTest
    static void testFetchCheckListRecordsShouldReturnQCListForContract(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CONTRACT);
        insert agreement;
        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData(CON_CPQ.CONTRACT);

        Test.startTest();
            List<CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper> qcCheckListForDraft =
                CNT_CPQ_QCSelfCheckFinalContract.fetchQCCheckListRecords(agreement.Id);
        Test.stopTest();

        system.assertEquals(1, qcCheckListForDraft.size(),'Should Return QC List For Contract');
    }

    @isTest
    static void testFetchCheckListRecordsShouldReturnQCListForExistingProposalList(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CONTRACT);
        insert agreement;
        setProposalQASelfCheckListData(agreement.Id);

        Test.startTest();
            List<CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper> qcCheckListForDraft =
                CNT_CPQ_QCSelfCheckFinalContract.fetchQCCheckListRecords(agreement.Id);
        Test.stopTest();

        system.assertEquals(1, qcCheckListForDraft.size(),'Should Return QC List For Existing Proposal List');
    }

    @isTest
    static void testsaveProposalQASelfCheckListShouldReturnPropsalList(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CONTRACT);
        agreement.Process_Step__c = CON_CPQ.FINAL_QC;
        insert agreement;

        List<CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList =
            new List<CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper>();

        QC_Check_List_Item__c qcRecord = setQCCheckListItemData(CON_CPQ.CONTRACT);

        CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper  wrapperObj = new CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper(
            new Proposal_QA_Self_Check_List__c(Agreement__c = agreement.Id, Question__c = qcRecord.Question__c, Guidelines__c = qcRecord.Guidelines__c), 
                false, proposalQASelfCheckListWrapperList.size());
        proposalQASelfCheckListWrapperList.add(wrapperObj);
        String proposalQASelfCheckListWrapperListString = JSON.serialize(proposalQASelfCheckListWrapperList);

        Test.startTest();
            CNT_CPQ_QCSelfCheckFinalContract.saveProposalQASelfCheckList(proposalQASelfCheckListWrapperListString, agreement.Id,
                CON_CPQ.FINAL_QC, true, 'testing');
        Test.stopTest();

        List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = getProposalQASelfCheckListData(agreement.Id);
        system.assertEquals(1, proposalQASelfCheckList.size(),'Should Return Proposal List');
    }

    @isTest
    static void testAddMoreItemInListShouldReturnPropsalList(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CONTRACT);
        insert agreement;
        QC_Check_List_Item__c qcRecord = setQCCheckListItemData(CON_CPQ.CONTRACT);
        List<CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList =
            new List<CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper>();
        CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper  wrapperObj = new CNT_CPQ_QCSelfCheckFinalContract.ProposalQASelfCheckListWrapper(
            new Proposal_QA_Self_Check_List__c(Agreement__c = agreement.Id, Question__c = qcRecord.Question__c, Guidelines__c = qcRecord.Guidelines__c), 
                false, proposalQASelfCheckListWrapperList.size());
        proposalQASelfCheckListWrapperList.add(wrapperObj);
        String proposalQASelfCheckListWrapperListString = JSON.serialize(proposalQASelfCheckListWrapperList);

        Test.startTest();
            proposalQASelfCheckListWrapperList =
            CNT_CPQ_QCSelfCheckFinalContract.addMoreItemInList(proposalQASelfCheckListWrapperListString, agreement.Id);
        Test.stopTest();

        system.assertEquals(2, proposalQASelfCheckListWrapperList.size(),'Should Return Proposal List');
    }

    @isTest
    static void testFetchPicklistValueShouldReturnPicklist(){

        Test.startTest();
            List<String> pickList =
                CNT_CPQ_QCSelfCheckFinalContract.fetchPicklistValue('Response__c');
        Test.stopTest();

        system.assertNotEquals(null, pickList, 'Should Return PickList');
    }

    @isTest
    public static void TestFetchOverAllCommentsShouldReturnFinalQCComment(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CONTRACT);
        agreement.Agreement_Purpose_List__c = 'testing final qc';
        insert agreement;

        Test.startTest();
            String Comments = CNT_CPQ_QCSelfCheckFinalContract.fetchOverAllComments(agreement.Id);
        Test.stopTest();

        system.assertEquals('testing final qc', Comments,'Should Return Final QC Comment');
    }
}