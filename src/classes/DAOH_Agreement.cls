public with sharing class DAOH_Agreement {
    /**
    * This method is used for updating agreement of opportunity when any other agreement is makr primary
    * @params  RelatedOpportunityIds, CurrentAreementIds
    * @return  void
    */
    public static void updateAgreementToMakeNonPrimary(Set<Id> opportunityIds, Set<Id> currentAgreementIds){

        Set<String> fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.AGREEMENT_MARK_AS_PRIMARY};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getRelatedOpportunityPrimaryAgreement(opportunityIds,
            currentAgreementIds, fieldSet);
        if(agreementList.size() > 0)   {
            for(Apttus__APTS_Agreement__c agreement : agreementList)
                agreement.Mark_as_Primary__c = false;

            update agreementList;
        }
    }
    
    /**
    * This method is used to bid History Number Increment
    * @params  List<Apttus__APTS_Agreement__c> newList, Map<Id, Apttus__APTS_Agreement__c> oldMap
    * @return  void
    */
    public static void bidHistoryNumberIncrement(List<Apttus__APTS_Agreement__c> newList, Map<Id, Apttus__APTS_Agreement__c> oldMap){
        List<Id> parentOpportunityIds = new List<Id>();
        Set<Id> agreementRecordType = new Set<Id>{CON_CRM.AGREEMENT_RECORD_TYPE_PRELIM_AGREEMENT,
            CON_CRM.AGREEMENT_RECORD_TYPE_EARLY_ENGAGEMENT_BID,
            CON_CRM.AGREEMENT_RECORD_TYPE_POST_AWARD_BID,
            CON_CRM.AGREEMENT_RECORD_TYPE_NON_RFP_BID,
            CON_CRM.AGREEMENT_RECORD_TYPE_RFI_REQUEST,
            CON_CRM.AGREEMENT_RECORD_TYPE_RFI_SHORT_FORM
            };
        List<Apttus__APTS_Agreement__c> filteredBidHistories = new List<Apttus__APTS_Agreement__c>();
        for(Apttus__APTS_Agreement__c currentAgreement : newList){
            if(currentAgreement.RecordTypeId == CON_CRM.AGREEMENT_RECORD_TYPE_PRELIM_AGREEMENT &&  ( currentAgreement.Bid_Number__c != 0 || currentAgreement.Bid_Number__c == null )){
                currentAgreement.Bid_Number__c = 0;
            }
            if(currentAgreement.Bid_Number__c == null && (agreementRecordType.contains(currentAgreement.RecordTypeId))){
                parentOpportunityIds.add(currentAgreement.Apttus__Related_Opportunity__c);
                filteredBidHistories.add(currentAgreement);
            }
        }
        if(parentOpportunityIds.size() > 0){
            Set<Id> idSet = new Set<Id>();
            idSet.addAll(parentOpportunityIds);
            try{
                agreementRecordType.remove(CON_CRM.AGREEMENT_RECORD_TYPE_NON_RFP_BID);
                Map<Id, Opportunity> parentOpportunityMap = new SLT_Opportunity().selectAgreementByOpportunity(idSet, new Set<String>{'Id'},agreementRecordType,new Set<String>{'Id'});
                for(Apttus__APTS_Agreement__c filteredAgreement : filteredBidHistories){
                    filteredAgreement.Bid_Number__c = ((List<Apttus__APTS_Agreement__c>)parentOpportunityMap.get(filteredAgreement.Apttus__Related_Opportunity__c).Apttus__R00N50000001Xl0FEAS__r).size() + 1;
                }
            }catch(Exception e){
                
            }
        }
    }


    public static void updateEmailBodyAndSubject(List<Apttus__APTS_Agreement__c> agreementList, Boolean isUpdate){
        List<EmailTemplate> emailTemplateForProjectManager = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(
            CON_CPQ.CPQ_APPROVAL_REQUEST_TO_PROJECT_MANAGER, new Set<String>{CON_CPQ.Id});
        List<Apttus__APTS_Agreement__c> updateAgreementList = new List<Apttus__APTS_Agreement__c>();
        if(emailTemplateForProjectManager.size() > 0 && agreementList.size() > 0){

            for(Apttus__APTS_Agreement__c agreement : agreementList){

                Apttus__APTS_Agreement__c newAgreement = new Apttus__APTS_Agreement__c(Id = agreement.Id);
                Messaging.SingleEmailMessage mailTemp = Messaging.renderStoredEmailTemplate(emailTemplateForProjectManager[0].Id, null, agreement.Id);
                if(!isUpdate)
                    newAgreement.Email_Body__c = mailTemp.getHtmlbody();
                newAgreement.Email_subject__c = mailTemp.getSubject();
                updateAgreementList.add(newAgreement);
            }

            if(updateAgreementList.size() > 0)
            	update updateAgreementList;
        }
    }
    
    /**
    * created By Shikha Badhera
    * This method set the agreement status and process step field, based on state of other field. 
    * @params agreementList, oldAgreementMap
    * @return  none
    */

    public static void setAgreementStatusAndProcessStepForMVP(List<Apttus__APTS_Agreement__c> newList, Map<Id, Apttus__APTS_Agreement__c> oldMap){

        Set<Id> InitiaBidrecordTypeIds = CPQ_Utility.getInitialRecordTypes();       
        Set<Id> ContractrecordTypeIds = CPQ_Utility.getContractRecordTypes();        

        for(Apttus__APTS_Agreement__c agreement : newList){
            if((InitiaBidrecordTypeIds.contains(agreement.RecordTypeId) ||
                ContractrecordTypeIds.contains(agreement.RecordTypeId)) &&
                agreement.Process_Step__c  != oldMap.get(agreement.Id).Process_Step__c){
                if(InitiaBidrecordTypeIds.contains(agreement.RecordTypeId) &&
                    (agreement.Process_Step__c == CON_CPQ.CUSTOMER_DELIVERABLE_SENT || 
                    agreement.Process_Step__c == CON_CPQ.RECEIVED_ATP_LOI) &&
                    agreement.Agreement_Status__c == CON_CPQ.PROPOSAL_DELIVERY){

                    agreement.Pricing_Tool_Locked__c = true;
                }
                if((agreement.Process_Step__c == CON_CPQ.STRATEGY_CALL ||
                    agreement.Process_Step__c == CON_CPQ.MAP_CALL ||
                    agreement.Process_Step__c == CON_CPQ.LINE_MANAGER_QC ||
                    agreement.Process_Step__c == CON_CPQ.QC_SELF_CHECK_DRAFT) &&
                    agreement.Agreement_Status__c != CON_CPQ.DRAFT){

                    agreement.Agreement_Status__c = CON_CPQ.DRAFT;
                }else If(InitiaBidrecordTypeIds.contains(agreement.RecordTypeId) &&
                    agreement.Process_Step__c == CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL &&
                    agreement.Agreement_Status__c != CON_CPQ.INTERNAL_REVIEW){

                    agreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
                }else If(InitiaBidrecordTypeIds.contains(agreement.RecordTypeId) &&
                    agreement.Process_Step__c == CON_CPQ.TSL_APPROVED &&
                    agreement.Agreement_Status__c != CON_CPQ.PENDING_APPROVAL){

                    agreement.Agreement_Status__c = CON_CPQ.PENDING_APPROVAL;
                }else If(InitiaBidrecordTypeIds.contains(agreement.RecordTypeId) &&
                    agreement.Process_Step__c == CON_CPQ.BUDGET_APPROVED &&
                    agreement.Agreement_Status__c != CON_CPQ.BUDGET_APPROVED){

                    agreement.Agreement_Status__c = CON_CPQ.BUDGET_APPROVED;
                }else If(InitiaBidrecordTypeIds.contains(agreement.RecordTypeId) &&
                    agreement.Process_Step__c == CON_CPQ.RECEIVED_ATP_LOI &&
                    agreement.Agreement_Status__c != CON_CPQ.STUDY_AWARDED){

                    agreement.Mark_as_Primary__c = true;
                    agreement.Agreement_Status__c = CON_CPQ.STUDY_AWARDED;
                    agreement.Process_Step__c = CON_CPQ.BUDGET_ACCEPTED;
                }else If(InitiaBidrecordTypeIds.contains(agreement.RecordTypeId) &&
                    agreement.Process_Step__c == CON_CPQ.FINAL_PREPARATION &&
                    agreement.Agreement_Status__c != CON_CPQ.FINAL_PREPARATION){

                    agreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
                    agreement.Process_Step__c = CON_CPQ.NONE;
                }else If(ContractrecordTypeIds.contains(agreement.RecordTypeId) &&
                    agreement.Process_Step__c == CON_CPQ.BID_GRID_MAINTENANCE_COMPLETION &&
                    agreement.Agreement_Status__c != CON_CPQ.FINAL_PREPARATION){

                    agreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
                }
                else If(ContractrecordTypeIds.contains(agreement.RecordTypeId) &&
                    (agreement.Process_Step__c == CON_CPQ.CNF_DELIVERED ||
                    agreement.Process_Step__c == CON_CPQ.COL_DELIVERED) &&
                    agreement.Agreement_Status__c != CON_CPQ.CNF_ACCEPTED){

                    agreement.Agreement_Status__c = CON_CPQ.CNF_ACCEPTED;
                    agreement.Process_Step__c = CON_CPQ.NONE;
                }
                else If(ContractrecordTypeIds.contains(agreement.RecordTypeId) &&
                    (agreement.Process_Step__c == CON_CPQ.BUDGET_DELIVERED ||
                    agreement.Process_Step__c == CON_CPQ.BUDGET_CONTRACT_DELIVERED	||
                    agreement.Process_Step__c == CON_CPQ.BALLPARK_DELIVERED) &&
                    agreement.Agreement_Status__c == CON_CPQ.CUSTOMER_DELIVERABLE){

                    agreement.Pricing_Tool_Locked__c = true;
                }else If(ContractrecordTypeIds.contains(agreement.RecordTypeId) &&
                    agreement.Process_Step__c == CON_CPQ.PAYMENT_SCHEDULE_DELIVERED &&
                    agreement.Agreement_Status__c == CON_CPQ.CUSTOMER_DELIVERABLE){

                    agreement.Pricing_Tool_Locked__c = false;
                }else if(ContractrecordTypeIds.contains(agreement.RecordTypeId) && agreement.Process_Step__c == CON_CPQ.BUDGET_CHANGES_POST_PL_REVIEW &&
                    agreement.Agreement_Status__c != CON_CPQ.PL_REVIEW) {
                    agreement.Agreement_Status__c = CON_CPQ.Draft;
                    agreement.Process_Step__c = CON_CPQ.NONE;
                }else if(ContractrecordTypeIds.contains(agreement.RecordTypeId) && agreement.Agreement_Status__c == CON_CPQ.CNF_DELIVERED &&
                    (agreement.Process_Step__c == CON_CPQ.CNF_DELIVERED || agreement.Process_Step__c == CON_CPQ.COL_DELIVERED)) {
                    agreement.Agreement_Status__c = CON_CPQ.CNF_ACCEPTED;
                    agreement.Process_Step__c = CON_CPQ.NONE;
                }else if(ContractrecordTypeIds.contains(agreement.RecordTypeId) && agreement.Agreement_Status__c == CON_CPQ.INTERNAL_REVIEW &&
                    agreement.Process_Step__c == CON_CPQ.PENDING_PL_REVIEW) {
                    agreement.Agreement_Status__c = CON_CPQ.PENDING_PL_REVIEW;
                    agreement.Process_Step__c = CON_CPQ.NONE;
                }else if(ContractrecordTypeIds.contains(agreement.RecordTypeId) && agreement.Agreement_Status__c == CON_CPQ.PENDING_PL_REVIEW &&
                    agreement.Process_Step__c == CON_CPQ.PL_REVIEW) {
                    if(String.isBlank(agreement.Project_Manager_Email__c)) {
                        agreement.Process_Step__c = CON_CPQ.NONE;
                    } else {
                        CNT_CPQ_CustomPathPopup.sendMail(agreement.Id);
                    }
                }else If(InitiaBidrecordTypeIds.contains(agreement.RecordTypeId)  &&  agreement.Agreement_Status__c == CON_CPQ.PENDING_APPROVAL
                    && agreement.Process_Step__c == CON_CPQ.CHALLENGE_CALL_COMPLETE){
                    agreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
                } else If(InitiaBidrecordTypeIds.contains(agreement.RecordTypeId)  &&  agreement.Agreement_Status__c == CON_CPQ.PENDING_APPROVAL
                    && agreement.Process_Step__c == CON_CPQ.TSL_REVIEW){
                    agreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
                }
            }
            if(ContractrecordTypeIds.contains(agreement.RecordTypeId) &&
                agreement.Agreement_Status__c == CON_CPQ.CUSTOMER_DELIVERABLE &&
                agreement.Apttus__Company_Signed_Date__c != null &&
                agreement.Apttus__Other_Party_Signed_Date__c != null) {                
                agreement.Agreement_Status__c = CON_CPQ.CONTRACT_EXECUTED;
                agreement.Process_Step__c = CON_CPQ.BUDGET_CONTRACT_ACCEPTED;
            }
        }
    }
    
    
    public static void createTaskOnAgreement(List<Apttus__APTS_Agreement__c> newAgreementList,
        Map<Id, Apttus__APTS_Agreement__c> oldRecordsMap){
        
        Set<Id> recordTypeIds = CPQ_Utility.getInitialRecordTypes();  
        
        Set<Id> proposalDeveloperIds = new Set<Id>();
        Map<Id, Id> lineManagerMap = new Map<Id, Id>();
        for(Apttus__APTS_Agreement__c agreement : newAgreementList){
            if(agreement.Agreement_Status__c == CON_CPQ.DRAFT
                && oldRecordsMap.get(agreement.Id).Process_Step__c == CON_CPQ.QC_SELF_CHECK_DRAFT
                && agreement.Process_Step__c == CON_CPQ.LINE_MANAGER_QC
                && recordTypeIds.contains(agreement.RecordTypeId)
                && agreement.OwnerId != null
                && recordTypeIds.contains(agreement.RecordTypeId)){
                    proposalDeveloperIds.add(agreement.OwnerId);
                }
        }
        if(proposalDeveloperIds.size() > 0){
            List<User> userList = new List<User>();
            userList = new SLT_User().selectByManagerId(proposalDeveloperIds, new Set<String> {CON_CPQ.ID, CON_CPQ.MANAGERID});
            for(User usr : userList){
                lineManagerMap.put(usr.Id, usr.ManagerId);
            }
            List<Task> taskListToInsert = new List<Task>();
            Database.DMLOptions dmlo = new Database.DMLOptions();
            dmlo.EmailHeader.triggerUserEmail = true;
            for(Apttus__APTS_Agreement__c agreement : newAgreementList){
                if(recordTypeIds.contains(agreement.RecordTypeId)
                    && agreement.Agreement_Status__c == CON_CPQ.DRAFT
                    && oldRecordsMap.get(agreement.Id).Process_Step__c == CON_CPQ.QC_SELF_CHECK_DRAFT
                    && agreement.Process_Step__c == CON_CPQ.LINE_MANAGER_QC
                    && lineManagerMap.containsKey(agreement.OwnerId)){

                    Task taskObj = new Task();
                    taskObj.Subject = CON_CPQ.LINE_MANAGER_QC;
                    taskObj.OwnerId = lineManagerMap.get(agreement.OwnerId);
                    taskObj.Status = CON_CPQ.OPEN;
                    taskObj.Priority = CON_CPQ.NORMAL;
                    taskObj.WhatId = agreement.Id;
                    taskListToInsert.add(taskObj);

                }
            }

            if(taskListToInsert.size() > 0){
                insert taskListToInsert;
            }
        } 
    }
    
    /**
    * created By Jyoti Agrawal
    * This method is creating and updating budget agreement history record
    * @params agreementList, oldAgreementMap
    * @return  void
    */
    public static void createOrUpdateBudgetAgreementHistoryRecord(List<Apttus__APTS_Agreement__c> newAgreementList,
        Map<Id, Apttus__APTS_Agreement__c> existingRecords) 
        {
        List<Budget_Agreement_History__c> budgetAgreementHistoryToUpdate = new List<Budget_Agreement_History__c>();
        List<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();
        Map<Id, Apttus__APTS_Agreement__c> agreementMap = new Map<Id, Apttus__APTS_Agreement__c>();
        agreementList = new SLT_Agreement().selectBudgetAgreementHistory(newAgreementList);
        for(Apttus__APTS_Agreement__c agreement :agreementList) {
            agreementMap.put(agreement.Id, agreement);
        }

        Set<Id> initialRecordTypeIds = CPQ_Utility.getInitialRecordTypes();
        Set<Id> contractRecordTypeIds = CPQ_Utility.getContractRecordTypes();

        for(Apttus__APTS_Agreement__c agreement : newAgreementList) {
            if(initialRecordTypeIds.contains(agreement.RecordTypeId) || contractRecordTypeIds.contains(agreement.RecordTypeId)) {
                if(agreement.Process_Step__c != existingRecords.get(agreement.Id).Process_Step__c) {
                    Budget_Agreement_History__c newBudgetAgreementHistory = new Budget_Agreement_History__c();
                    newBudgetAgreementHistory.Old_Agreement_Status__c = existingRecords.get(agreement.Id).Agreement_Status__c;
                    newBudgetAgreementHistory.New_Agreement_Status__c = agreement.Agreement_Status__c;
                    newBudgetAgreementHistory.Old_Process_Step__c = existingRecords.get(agreement.Id).Process_Step__c;
                    newBudgetAgreementHistory.New_Process_Step__c = agreement.Process_Step__c;
                    newBudgetAgreementHistory.Agreement__c = agreement.Id;
                    newBudgetAgreementHistory.Process_Step_Start_Date__c = DateTime.now();
                    Map<String, Budget_Agreement_History__c> budgetAgreementHistoryMap = new Map<String, Budget_Agreement_History__c>();
                    for(Budget_Agreement_History__c budgetAgreementHistory : agreementMap.get(agreement.Id).Budget_Agreement_History__r) {
                        budgetAgreementHistoryMap.put(budgetAgreementHistory.New_Process_Step__c, budgetAgreementHistory);
                    }
                    if(initialRecordTypeIds.contains(agreement.RecordTypeId)) {
                        if(agreement.Agreement_Status__c == CON_CPQ.PENDING_APPROVAL && agreement.Process_Step__c == CON_CPQ.TSL_APPROVED &&
                            existingRecords.get(agreement.Id).Agreement_Status__c == CON_CPQ.INTERNAL_REVIEW && existingRecords.get(agreement.Id).Process_Step__c == CON_CPQ.TSL_REVIEW) {
                            newBudgetAgreementHistory.Approver_Name__c = agreement.User__r.Name;
                        } else if(agreement.Agreement_Status__c == CON_CPQ.INTERNAL_REVIEW && agreement.Process_Step__c == CON_CPQ.CHALLENGE_CALL_COMPLETE &&
                            existingRecords.get(agreement.Id).Process_Step__c == CON_CPQ.TSL_REVIEW && existingRecords.get(agreement.Id).Agreement_Status__c == CON_CPQ.INTERNAL_REVIEW) {
                            newBudgetAgreementHistory.Approver_Name__c = agreement.User__r.Name;
                        } else if(agreement.Agreement_Status__c == CON_CPQ.INTERNAL_REVIEW && agreement.Process_Step__c == CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL &&
                            existingRecords.get(agreement.Id).Agreement_Status__c == CON_CPQ.DRAFT && existingRecords.get(agreement.Id).Process_Step__c == CON_CPQ.LINE_MANAGER_QC) {
                            System.debug('inside11...'+agreement.createdBy.Name);
                                System.debug('inside11...'+agreement.LastModifiedBy);
                            newBudgetAgreementHistory.Approver_Name__c = UserInfo.getFirstName() + UserInfo.getLastName();
                        } else if(agreement.Agreement_Status__c == CON_CPQ.BUDGET_APPROVED && agreement.Process_Step__c == CON_CPQ.BUDGET_APPROVED &&
                            existingRecords.get(agreement.Id).Agreement_Status__c == CON_CPQ.PENDING_APPROVAL && existingRecords.get(agreement.Id).Process_Step__c == CON_CPQ.APPROVAL_REVIEW) {
                            newBudgetAgreementHistory.Approver_Name__c = agreement.Data_Asset_Physical_Location__c;
                        } else if(agreement.Agreement_Status__c == CON_CPQ.PROPOSAL_DELIVERY && agreement.Process_Step__c == CON_CPQ.CUSTOMER_DELIVERABLE_SENT &&
                            existingRecords.get(agreement.Id).Agreement_Status__c == CON_CPQ.FINAL_PREPARATION && existingRecords.get(agreement.Id).Process_Step__c == CON_CPQ.FINAL_QC) {
                            newBudgetAgreementHistory.Approver_Name__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                        }
                    } else if(contractRecordTypeIds.contains(agreement.RecordTypeId)) {
                        if(agreement.Agreement_Status__c == CON_CPQ.PL_BUDGET_APPROVED && agreement.Process_Step__c == CON_CPQ.PL_APPROVAL_RECEIVED){
                            newBudgetAgreementHistory.Approver_Name__c = agreement.Data_Asset_Physical_Location__c;
                        }
                    }
                    budgetAgreementHistoryToUpdate.add(newBudgetAgreementHistory);
                    if(budgetAgreementHistoryMap.containsKey(newBudgetAgreementHistory.Old_Process_Step__c)){
                        Budget_Agreement_History__c existingBudgetAgreementHistory = 
                            budgetAgreementHistoryMap.get(newBudgetAgreementHistory.Old_Process_Step__c);
                        existingBudgetAgreementHistory.Process_Step_End_Date__c = DateTime.now();
                        budgetAgreementHistoryToUpdate.add(existingBudgetAgreementHistory);
                    }
                }
            }
        }
        if(!budgetAgreementHistoryToUpdate.isempty())
            upsert budgetAgreementHistoryToUpdate;
    }
	
    /*  ===============================================================================
        Created:        Shikha Badhera
        Date:           06/12/2018
        Last Modified By:   Shikha Badhera
        Last Modified Date:	24/03/2019
        Description:    It triggers the approval process
        @param newAgreementList,   oldRecordsMap
        @return none
        ===============================================================================
    */
    public static void fireAprrovalRequestWhenAgreementStatusChanges(List<Apttus__APTS_Agreement__c> newAgreementList,
        Map<Id, Apttus__APTS_Agreement__c> oldRecordsMap){
        Set<Id> recordTypeIds = CPQ_Utility.getInitialRecordTypes();

        for(Apttus__APTS_Agreement__c agreement : newAgreementList){
            if(agreement.Agreement_Status__c == CON_CPQ.INTERNAL_REVIEW
                && oldRecordsMap.get(agreement.Id).Process_Step__c == CON_CPQ.CHALLENGE_CALL_COMPLETE
                && agreement.Process_Step__c == CON_CPQ.TSL_REVIEW
                && agreement.User__c != null
                && recordTypeIds.contains(agreement.RecordTypeId)){
                Approval.ProcessSubmitRequest request= new Approval.ProcessSubmitRequest();
                request.setObjectId(agreement.Id);
                request.setNextApproverIds(new Id[] {agreement.User__c});
                Approval.ProcessResult result = Approval.process(request);
            }
        }
    }
}
