/*
 * Version       : 1.0
 * Description   : Apex Controller for RND tab.
 */
public class CNT_CRM_RND_TAB {
    
    /**
     * This method used to get custom setting record.
     * @params  String recordId
     * @return  Opportunity_Stage__c oppStage
     */
    @AuraEnabled
    public static Opportunity_Stage__c getOpportunityStageRecord(String recordId) {
        Opportunity_Stage__c OpportunityStageRecord = new Opportunity_Stage__c();
        try {
            OpportunityStageRecord = Opportunity_Stage__c.getValues(recordId);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return OpportunityStageRecord;
    }
    
    /**
     * This method used to get required fields for LIG.
     * @params  String recordId
     * @params  String expectedStage
     * @return  List<String> 
     */
    @AuraEnabled
    public static List<String> getListOfRequiredFields(Id recordId, String expectedStage, String expectedLineOfBuisness) {
        List<String> mapOfRequiredFieldAPI = new List<String>();
        try {
        	mapOfRequiredFieldAPI = SRV_CRM_LineItemGroup.getListOfRequiredFields(recordId, expectedStage, expectedLineOfBuisness);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return mapOfRequiredFieldAPI;
    }
    
    /**
     * This method used to get LIG Record Detail
     * @params  String jsonWrapper
     * @params  String jsonWrapper
     * @return  Line_Item_Group__c
     */
    @AuraEnabled
    public static Line_Item_Group__c getLIGRecordDetail(Id oppId, List<String> ligFieldList) {
        Line_Item_Group__c ligRecord = new Line_Item_Group__c();
        try {
            ligRecord = SRV_CRM_LineItemGroup.getLIGRecordDetail(new Set<Id>{oppId}, new Set<String>(ligFieldList));
        } catch (exception ex) {
            system.debug(ex);
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return ligRecord;
    }
    
    /**
     * This method used to check if manager permission set is assigned or not.
     * @return  Boolean isManagerUser
     */
    @AuraEnabled
    public static List<Boolean> getManagerUserDetail() {
        List<Boolean> perList = new List<Boolean>();
        Boolean isManagerUser = false;
        Boolean hasInsideSalesAssigned = false;
        try {
            String userProfile = UTL_CRM.getCurrentUserProfileName();
            if(!userProfile.equalsIgnoreCase(CON_CRM.SYSTEM_ADMIN_PROFILE)) {       
                List<PermissionSetAssignment> lstcurrentUserPerSet = [SELECT Id, PermissionSet.Name, AssigneeId FROM PermissionSetAssignment
                                                                      WHERE AssigneeId = :Userinfo.getUserId() AND  Permissionset.Name = 'PD_Binary_Decision' ];
                List<PermissionSetAssignment> insideSalesPerList = [SELECT Id, PermissionSet.Name, AssigneeId FROM PermissionSetAssignment
                                                                      WHERE AssigneeId = :Userinfo.getUserId() AND  Permissionset.Name = 'Inside_Sales' ];
                if(lstcurrentUserPerSet.size() > 0) {
                    isManagerUser = true;
                }
                if(insideSalesPerList.size() > 0) {
                    hasInsideSalesAssigned = true;
                }
            } else{
                isManagerUser = true;
                hasInsideSalesAssigned = true;
            }
            perList.add(isManagerUser);
            perList.add(hasInsideSalesAssigned);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return perList;
    }

}