@isTest
public class TST_SLT_EmailTemplate {
    
    @isTest
    static void testSelectByIdShouldReturnEmailTemplate(){

        EmailTemplate ownerTemplate = [SELECT Id FROM EmailTemplate LIMIT 1];

        Test.startTest();
            List<EmailTemplate> emailTemplateList = new SLT_EmailTemplate().selectById(new Set<Id>{ownerTemplate.Id});
        Test.stopTest();

        System.assertEquals(1, emailTemplateList.size(), 'Should Return Email Template');
    }

    @isTest
    static void testSelectEmailTemplateByDeveloperNameShouldReturnEmailTemplate(){

        Test.startTest();
            List<EmailTemplate> emailTemplateList = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(
                'CPQ_Another_User_Request_To_Unlock_Budget', new Set<String>{CON_CPQ.Id});
        Test.stopTest();

        System.assertEquals(1, emailTemplateList.size(), 'Should Return Email Template by Developer Name');
    }
}