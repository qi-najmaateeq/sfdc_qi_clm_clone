/**
 * This class is used to test SLT_Profile class
 */ 
@isTest
public class TST_SLT_Profile {
    
    static testMethod void testSelectById() {
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'Sales User' LIMIT 1];
        Set<Id> profileIdSet = new Set<Id>();
        profileIdSet.add(profile.Id);
        Test.startTest();
        List<Profile> profileList = new SLT_Profile().selectById(profileIdSet);
        System.assertEquals('Sales User',profileList[0].Name);
        Test.stopTest();
    }    
}