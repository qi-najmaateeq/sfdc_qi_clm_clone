/**
 * This class is used to test BCH_CRM_OpportunityStage Batch class
 */ 
@isTest
public class TST_BCH_CRM_OpportunityStage {

    @testSetup
    static void dataSetup() {
        List<Opportunity_Stage__c> oppStageList = new List<Opportunity_Stage__c>();
        Opportunity_Stage__c oppStage = new Opportunity_Stage__c();
        oppStage.Name = 'Opportunity Stage 1';
        oppStage.CreatedDate = Date.today() - 3;
        oppStageList.add(oppStage);
        oppStage = new Opportunity_Stage__c();
        oppStage.Name = 'Opportunity Stage 2';
        oppStage.CreatedDate = Date.today() - 1;
        oppStageList.add(oppStage);
        insert oppStageList;
    }
    
    static testMethod void testBCHCRMOpportunityStageBatch() {
        List<Opportunity_Stage__c> oppStageList;
        oppStageList = [SELECT Id FROM Opportunity_Stage__c];
        System.assertEquals(2, oppStageList.size()); 
        Test.startTest();
        BCH_CRM_OpportunityStage batchOppStage = new BCH_CRM_OpportunityStage();
        Database.executeBatch(batchOppStage);
        Test.stopTest();
        oppStageList = [SELECT Id FROM Opportunity_Stage__c];
        System.assertEquals(1, oppStageList.size()); 
    }
}