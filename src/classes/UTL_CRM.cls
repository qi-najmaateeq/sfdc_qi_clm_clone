/*
 * Version       : 1.0
 * Description   : Apex Utility class for CRM. 
 */ 
public class UTL_CRM {
    
    Static String currentUserProfile;
    private static Blob cryptoKey = EncodingUtil.base64Decode('MGpu+ZQa1mHJg0qq2YyFGwnUM2GCoR8ZS5PA8YuLPnU=');
    private static String ALGORITHM_NAME = 'AES256';
    
    /**
     * This function will return profile name of current loged in user.
     */
    public static String getCurrentUserProfileName(){
        if(String.isBlank(currentUserProfile)){
            Set<Id> idSet = new Set<Id>{UserInfo.getUserId()};
            Set<String> fieldSet = new Set<String>{'Profile.Name'};
            Map<Id, User> idUserMap = new SLT_User().selectByUserId(idSet, fieldSet);
            currentUserProfile = idUserMap.get(UserInfo.getUserId()).Profile.Name;
        }
        return currentUserProfile;
    }
    
    /**
     * method used to encrypt the password
     * @param : String : password to be encrypt
     * @return : String : encrypted String
     */
    public static string encryptPassword(string password) {
        String encryptedPassword = '';
        if(password != null && password != '') {
            Blob data = Blob.valueOf(password);
            // Encrypt the data 
            Blob encryptedData = Crypto.encryptWithManagedIV(ALGORITHM_NAME, cryptoKey, data);
            encryptedPassword = EncodingUtil.base64Encode(encryptedData);
        }
        return encryptedPassword;
    }
    
    /**
     * method used to decrypt the password
     * @param : String : password to be decrypt
     * @return : String : decrypted String
     */
    public static string decryptPassword(String encryptedPassword) {
        String decryptedPassword = '';
        if(encryptedPassword != null && encryptedPassword != '') {
            Blob data = EncodingUtil.base64Decode(encryptedPassword);
            // Decrypt the data 
            Blob decryptedData = Crypto.decryptWithManagedIV(ALGORITHM_NAME, cryptoKey, data);
            decryptedPassword = decryptedData.toString();
        }
        return decryptedPassword;
    }  
}