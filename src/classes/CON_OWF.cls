/**
 * This class used for constants used in OWF project.
 * version : 1.0
 */
public class CON_OWF {
    public static final String SEMI_COLON = ';';
    public static final Integer DEFAULT_BATCH_SIZE = 200;
    
    // This is all for query record type
    public static final String SOBJECT_TYPE_APTS_AGREEMENT = 'Apttus__APTS_Agreement__c';
    
    public static final String OPPORTUNITY_AGREEMENTS = 'Apttus__R00N50000001Xl0FEAS__r';
    public static final String AGREEMENT_PROJECTS = 'Projects__r';
    public static final String AGREEMENT_RESOURCE_REQUESTS = 'Resource_Requests__r';
    public static final String AGREEMENT_ASSIGNMENTS = 'Assignments__r';
    public static final String ASSIGNMENT_STATUS = 'pse__Status__c';
    public static final String RESOURCE_REQUEST_ASSIGNMENTS = 'pse__Assignments__r';
    public static final String RR_RESOURCE_SKILL_REQUESTS = 'pse__Resource_Skill_Requests__r';
    public static final String RESOURCE_SKILL_CERTIFICATION_RATINGS = 'pse__Skill_Certification_Ratings__r';
    
    public static final String OWF_STATUS_COMPLETE = 'Complete';
    public static final String OWF_STATUS_ACCEPTED = 'Accepted';
    public static final String OWF_STATUS_COMPLETED = 'Completed';
    public static final String OWF_STATUS_ASSIGNED = 'Assigned';
    public static final String OWF_STATUS_PENDING = 'Pending';
    public static final String OWF_STATUS_CANCELLED = 'Cancelled';
    public static final String OWF_STATUS_REJECTED = 'Rejected';
	public static final String OWF_STATUS_READYTOSTAFF= 'Ready to Staff';
    
    public static final String OWF_RESOURCE_REQUEST_RECORD_TYPE_NAME = 'OWF_Resource_Request';
    public static final String OWF_ASSIGNMENT_RECORD_TYPE_NAME = 'OWF_Assignment';
    
    public static final String LINE_ITEM_GROUP_PHASE_4 = 'Phase 4';
    public static Id OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Clinical_Bid').getRecordTypeId();
    public static Id OWF_NON_RFP_BID_AGREEMENT_RECORD_TYPE_ID = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Non_RFP_Bid').getRecordTypeId();
    public static Id OWF_ASSIGNMENT_RECORD_TYPE_ID = Schema.SObjectType.pse__Assignment__c.getRecordTypeInfosByName().get('OWF Assignment').getRecordTypeId();

    public static final Set<Id> OWF_AGREEMENT_RECORD_TYPE_IDS_SET
     = new Set<Id>{Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Clinical_Bid').getRecordTypeId(),
     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Early_Engagement_Bid').getRecordTypeId(),
     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Non_RFP_Bid').getRecordTypeId(),
     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Post_Award_Bid').getRecordTypeId(),
     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Request').getRecordTypeId()};
    
     public static final Set<Id> OWF_AGREEMENT_RECORD_TYPE_BID_HISTORY
     = new Set<Id>{Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Clinical_Short_Form').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('CSS_Short_Form').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Clinical_Bid').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('CSS_Bid').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Early_Engagement_Bid').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Non_RFP_Bid').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Post_Award_Bid').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('PPA_Agreement').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Prelim_Agreement').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Short_Form').getRecordTypeId(),
         Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Request').getRecordTypeId()};
         
    public static final Id OWF_BID_PROJECT_RECORD_TYPE_ID = Schema.SObjectType.pse__Proj__c.getRecordTypeInfosByName().get('Bid').getRecordTypeId();

    public static final Set<Id> RESOURCES_NEEEDED_FIELD_BASED_RR_AGR_RECORDTYPE_SET
     = new Set<Id>{
     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Early_Engagement_Bid').getRecordTypeId(),
     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Post_Award_Bid').getRecordTypeId(),
     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Non_RFP_Bid').getRecordTypeId()};
     
    public static Id OWF_RFI_AGREEMENT_RECORD_TYPE_ID = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Request').getRecordTypeId();
    public static Id OWF_RR_RECORD_TYPE_ID = Schema.SObjectType.pse__Resource_Request__c.getRecordTypeInfosByDeveloperName().get('OWF_Resource_Request').getRecordTypeId();
    
    //This is all for batch class names
    public static final String BCH_OWF_UPDATEASSIGNMENTSSTATUS = 'BCH_OWF_UpdateAssignmentsStatus';
    public static final String BCH_OWF_UPDATERESOURCEREQUESTSTATUS = 'BCH_OWF_UpdateResourceRequestStatus';
    public static final String BCH_OWF_ACCEPTUNASSIGNEDASSIGNMENTS = 'BCH_OWF_AcceptUnassignedAssignments';
    public static final String QUEUEAGREEMENTDELETION = 'QueueAgreementDeletion';
    public static final String QUEUEPROJECTDELETION = 'QueueProjectDeletion';
    public static final String QUEUERESOURCEREQUESTDELETION = 'QueueResourceRequestDeletion';
    public static final String QUEUEOPPORTUNITYDELETION = 'QueueOpportunityDeletion';
    public static final String QUEUEASSIGNMENTDELETION = 'QueueAssignmentDeletion';
    
    //
    public static final String RES_REQ_TYPE_BID_DEFENCE = 'Bid Defense';
    public static final String RES_REQ_TYPE_GBO_LEAD_PD = 'GBO-Lead PD';
    public static final String RES_REQ_TYPE_CP_A_GLOBAL_FEASIBILITY = 'CP&A-Global Feasibility';
    public static final String RES_REQ_TYPE_TSL = 'TSL';
    public static final String RES_REQ_TYPE_JAPAN = 'TSL-Japan';
    public static final String RES_REQ_TYPE_CP_A_TOPS = 'CP&A-TOPS';
    public static final String RES_REQ_TYPE_CA_S = 'CP&A-CA&S';
    public static final String RES_REQ_TYPE_SSI = 'CP&A-SSI Lead';
    public static final String RES_REQ_TYPE_MEDICAL = 'Medical-MSL';
    public static final String RES_REQ_TYPE_SNP_PRODUCT_SPECIALIST = 'SPN Product Specialist';
    public static final String RES_REQ_TYPE_DSB = 'DSB';
    
    public static final String SKILL_TYPE_LINE_OF_BUSINESS = 'Line of Business';
    public static final String SKILL_TYPE_INDICATION = 'Indication';
    public static final String SKILL_TYPE_THERAPY_AREA = 'Therapy Area';
    public static final String SKILL_TYPE_POTENTIAL_REGION = 'User Regional Geographic Working Scope';
    
    public static final String PSE_SALESFORCE_USER = 'pse__Salesforce_User__c';
    public static final String RFI_RR = 'RFI';
    
    public static final Map<Id,String> recordIdToBidCategoryMap = new Map<Id,String>
                     {Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Clinical_Bid').getRecordTypeId() => 'Clinical',
                     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Early_Engagement_Bid').getRecordTypeId() => 'Early Engagement',
                     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Post_Award_Bid').getRecordTypeId() => 'Post Award',
                     Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Request').getRecordTypeId() => 'RFI'};
    
    public static final String CREATED_DATE = 'CreatedDate';
    public static final String PRESENTATION_DATE = 'Presentation_Date';
    public static final String BID_DUE_DATE = 'Bid_Due_Date';
    
    public static final Map<String, Double> skillRatingValueMap = new Map<String, Double>{
                                                                                    'None' => 0,
                                                                                    '1 - Limited Exposure' => 20,
                                                                                    '2 - Some Familiarity' => 40,
                                                                                    '3 - Comfortable' => 60,
                                                                                    '4 - Strong' => 80,
                                                                                    '5 - Expert' => 100};
    
    public static final String RFI_SUBGROUP = 'GBO-RFI';            
}
