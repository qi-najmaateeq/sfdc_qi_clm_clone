public class CNT_CPQ_XaeAppController {

    public String getAppId() {

        Set<String> fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL};
        Id agreementId = ApexPages.currentPage().getParameters().get(CON_CPQ.ID);
        Apttus__APTS_Agreement__c agreement = new SLT_Agreement().getAgreementDetails(agreementId, fieldSet);

        String appName = '';
        if(agreement.Select_Pricing_Tool__c == CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP){
            appName = CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_QIP_APP_NAME;
        }
        else if(agreement.Select_Pricing_Tool__c == CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_NOBI){
            appName = CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_NOBI_APP_NAME;
        }
        
        else if(agreement.Select_Pricing_Tool__c == CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_UPT){
            appName = CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_UPT_APP_NAME;
        }

        Set<String> fieldSetApp = new Set<String> {CON_CPQ.ID, CON_CPQ.APPLICATION_APTTUS_XAPPS_UNIQUEID};
        List<Apttus_XApps__Application__c> appList = 
            new SLT_Application().getApplicationByName(new Set<String>{appName}, fieldSetApp);
        if(appList.size() > 0)
            return appList[0].Apttus_XApps__UniqueId__c;
        
        return null;
    }
}