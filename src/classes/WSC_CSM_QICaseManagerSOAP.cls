global with sharing class WSC_CSM_QICaseManagerSOAP {
    
    global class CaseInfo {
        webservice String CaseNumber;
        webservice String Subject;
        webservice String Description;
        webservice String Status;
        webservice String resolution;
        webservice String Priority;
        webservice String lastUpdatedBy;
        
    }
    
    webservice static String createCase(String contactEmail,String subject,String description, String productName,String subtype1,String subtype2,String priority,String currentQueue,String caseSource) {
        
        Contact cont = null;
        Case thisCase = null;
        List<Case> caseList = null;
        try{
            cont = new SLT_Contact().selectByEmail(contactEmail);
            
        }catch(System.QueryException e){
            throw new System.QueryException('The contact '+ contactEmail +' is not found in our Customer Service Management system. Please contact you usual helpdesk center.');  
        }
        if(cont != null){
            Map<String,Schema.RecordTypeInfo> rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
            
            List<CSM_QI_Case_Categorization__c> categorizations= null;
            List<Asset> assets=null;
            List<Queue_User_Relationship__c> caseQueue = new SLT_QueueUserRelationshipC().selectByQueueName(currentQueue,new Set<String>{'Id','Name'},UserInfo.getUserId());
            if(caseQueue.isEmpty()){
               throw new System.ListException('Current Queue : '+ currentQueue +' is not found in our Customer Service Management system. Please contact you usual helpdesk center.');  
            }
            categorizations = new SLT_CaseCategorization().getCaseCategorizationByFilter('Product__r.Name=\''+productName+'\' and SubType1__c=\''+subtype1+'\' and SubType2__c=\''+subtype2+'\'');
            if (categorizations.isEmpty() == false) {
                assets = new SLT_Asset().selectByAccountIdAndProductId(new Set<Id> {cont.AccountId},new Set<Id>{categorizations[0].Product__c});
                thisCase = new Case(AccountId = cont.AccountId,ContactId = cont.Id,Subject = subject,Description = description,Status = 'New',Origin = 'External Service', Device__c='Laptop' ,recordTypeID = rt_map.get('TechnologyCase').getRecordTypeId(), CaseSource__c = caseSource);
                thisCase.AssetId=assets[0].Id;
                thisCase.Case_CategorizationId__c = categorizations[0].Id;
                thisCase.ProductName__c = categorizations[0].ProductName__c;
                thisCase.SubType1__c = categorizations[0].SubType1__c;
                thisCase.SubType2__c = categorizations[0].SubType2__c;
                thisCase.CurrentQueue__c = caseQueue[0].Id;
                insert thisCase;
                caseList = new SLT_Case().selectByCaseIdList(new Set<Id>{thisCase.Id} ,new Set<String>{'Id','CaseNumber'}); 
               
                return caseList[0].CaseNumber;
                
            }else{
             throw new System.QueryException('productName : '+ productName +' subtype1 : '+ subtype1 +' subtype2 : '+ subtype2 +' is not found in our Customer Service Management system. Please contact you usual helpdesk center.');  
            }
            
        }else{
            return null;  
        }
        
    }
    
    webservice static CaseInfo getCaseById(String caseId) {
        Case result = null;
        CaseInfo caseIn = null;
        try{
            result =  new SLT_Case().selectByCaseNumberList(caseId, new Set<String>{'CaseNumber','Subject','Status','Description','Priority','LastModifiedById'});
        }catch(System.QueryException e){
            throw new System.QueryException('Case Number : '+ caseId +' is not found in our Customer Service Management system. Please contact you usual helpdesk center.');  
        }
        
        if(result != null){
            caseIn = new CaseInfo();
            caseIn.CaseNumber = result.CaseNumber;
            caseIn.Subject = result.Subject;
            caseIn.Status = result.Status;
            caseIn.Priority = result.Priority;
            Map<Id,User> userData = new SLT_User().selectByUserId(new Set<Id>{result.LastModifiedById}, new Set<String>{'Id','Name'});
            if(userData.isEmpty() == false){
                caseIn.lastUpdatedBy = userData.get(result.LastModifiedById).Name;                            
            }
        }
        return caseIn;
        
    }
    
    webservice static CaseInfo closeCase(String caseId,String resolution) {
        Case result = null;
        CaseInfo caseIn = null;
        try{
            result =  new SLT_Case().selectByCaseNumberList(caseId, new Set<String>{'CaseNumber','Subject','Status','Description','Priority','LastModifiedById'});
        }catch(System.QueryException e){
            throw new System.QueryException('Case Number : '+ caseId +' is not found in our Customer Service Management system. Please contact you usual helpdesk center.');  
        }
        
        if(result != null ){
            result.Status = 'Closed';
            result.Resolution__c = resolution;
            try{
            update result;    
            }catch(System.QueryException exe){
                throw new QueryException('Resolution, Subtype 1 and Subtype 2 are mandatory to close the case');
            }
            
            
            caseIn = new CaseInfo();
            caseIn.CaseNumber = result.CaseNumber;
            caseIn.Subject = result.Subject;
            caseIn.Status = result.Status;
            caseIn.resolution  = result.Resolution__c;
            caseIn.Priority = result.Priority;
            Map<Id,User> userData = new SLT_User().selectByUserId(new Set<Id>{result.LastModifiedById}, new Set<String>{'Id','Name'});
            if(userData.isEmpty() == false){
                caseIn.lastUpdatedBy = userData.get(result.LastModifiedById).Name;                            
            }
            
        }
        
        return caseIn;
    }
    /** Get the Case Details based on Queue Name
     * 
     * */
    webservice static List<CaseInfo> getCaseListByQueueName(String queueName) {
        CaseInfo caseIn = null;
        List<CaseInfo> caseList=null;
        List<Case> cases=null;
        try{
            cases =  new SLT_Case().selectByCaseQueueList(queueName, new Set<String>{'CaseNumber','Subject','Status','Description','Priority','LastModifiedById'});
        }catch(System.QueryException e){
            throw new System.QueryException('Queue : '+ queueName +' is not found in our Customer Service Management system. Please contact you usual helpdesk center.');  
        }
        
        if(cases.isEmpty() == False){
            caseList = new List<CaseInfo>();
            for(Case result : cases){
                caseIn = new CaseInfo();
                caseIn.CaseNumber = result.CaseNumber;
                caseIn.Subject = result.Subject;
                caseIn.Status = result.Status;
                caseIn.Priority = result.Priority;
                caseList.add(caseIn);
            }
        }else{
            throw new System.QueryException('No cases are found under \''+ queueName +'\' Queue' );     
        }
        return caseList;
        
    }
   
}