/**
 * This test class is used to test SCH_CRM_OpportunityStage
 * version : 1.0
 */
@isTest
private class TST_SCH_CRM_OpportunityStage {
    
    /**
     * This method is testing all method in SCH_CRM_OpportunityStage and BCH_CRM_OpportunityStage Class
     */
    static testmethod void test() {
        Opportunity_Stage__c oppStage = new Opportunity_Stage__c();
        oppStage.Name = 'testScheduledApexFromTestMethod';
        insert oppStage;
        Test.setCreatedDate(oppStage.Id, Date.today() - 2);
        Opportunity_Stage__c oppStage1 = new Opportunity_Stage__c();
        oppStage1.Name = 'testScheduledApexFromTestMethod1';
        insert oppStage1;
        Test.setCreatedDate(oppStage1.Id, Date.today() - 3);
        Opportunity_Stage__c oppStage2 = new Opportunity_Stage__c();
        oppStage2.Name = 'testScheduledApexFromTestMethod2';
        insert oppStage2;
        Test.setCreatedDate(oppStage2.Id, Date.today() - 1);
        Opportunity_Stage__c oppStage3 = new Opportunity_Stage__c();
        oppStage3.Name = 'testScheduledApexFromTestMethod3';
        insert oppStage3;
        List<Opportunity_Stage__c> stageList = [SELECT Id, Name, CreatedDate FROM Opportunity_Stage__c];
        stageList = [SELECT Id, Name, CreatedDate FROM Opportunity_Stage__c WHERE CreatedDate < YESTERDAY];
        Test.startTest();
            String cronExp = '0 0 13 * * ?';
            String jobId = System.schedule('testBasicScheduledApex', cronExp, new SCH_CRM_OpportunityStage());
            BCH_CRM_OpportunityStage batchOppStage = new BCH_CRM_OpportunityStage();
            Database.executeBatch(batchOppStage);
        Test.stopTest();
        stageList = [SELECT Id, Name FROM Opportunity_Stage__c];
        System.assertEquals(stageList.size(), 2);
    }
}