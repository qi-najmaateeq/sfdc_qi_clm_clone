@isTest
public class TST_CNT_CPQ_UpdateRecordType {
    
    static Apttus__APTS_Agreement__c setAgreementData(string recordTypeName,Id OpportuntiyId, string pricingTool){

        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = RecordTypeId;
        testAgreement.Opportunity_Type__c= CON_CPQ.OPPORTUNITY_RFP;
        testAgreement.Select_Pricing_Tool__c = pricingTool;
        insert testAgreement;
        return testAgreement;
    }

      static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        testOpportunity.Legacy_Quintiles_Opportunity_Number__c='MVP123';
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }
    
    static ID getRecordTypeId(String recordTypeName){
        return [SELECT ID from RecordType where name =:recordTypeName].Id;
    }
    
    @isTest
    static void testGetLegacyOppNo(){
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        List<String> responseList = new List<String>();
        Test.startTest();
        	responseList = CNT_CPQ_UpdateRecordType.getLegacyOppNo(testOpportunity.Id);
        Test.stopTest();
        System.assert(responseList.size()>0, 'list is not null');
    }
    
    @isTest
    static void testGetPricingToolForFDTNInitialBid(){
        
        Id recordId= getRecordTypeId(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
            
        Test.startTest();
        	List<String> reponseList = CNT_CPQ_UpdateRecordType.getPricingTool(CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP);
        Test.stopTest();
        
        System.assertEquals(reponseList[0], recordId, 'FDTN Initial Bid record Created');        
    } 
}