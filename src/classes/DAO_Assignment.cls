/**
 * This is Assignment trigger handler class.
 * version : 1.0
 */
public class DAO_Assignment extends fflib_SObjectDomain {
    
    /**
     * Constructor of this class
     * @params sObjectList List<pse__Assignment__c>
     */
    public DAO_Assignment(List<pse__Assignment__c> sObjectList) {
        super(sObjectList);
    }

    /**
     * Constructor Class for construct new Instance of This Class
     */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new DAO_Assignment(sObjectList);
        }
    }
    
    /**
    * This method is used for before insert of the Assignment trigger.
    * @return void
    */
    public override void onBeforeInsert() {
        //This is the section where all the methods that needs to be run in a normal sequence are included.       
        DAOH_OWF_Assignment.updateAssignmentOwner((List<pse__Assignment__c>)Records,null);
        DAOH_OWF_Assignment.updateFieldsWhenStatusAndResourceChanged((List<pse__Assignment__c>)Records,null);
    }
    
    /**
     * This method is used for after insert of the Assignment trigger.
     * @return void
     */
    public override void onAfterInsert() {
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        DAOH_OWF_Assignment.populateRollupAssignmentFieldsOnContact((List<pse__Assignment__c>)Records,null);
        DAOH_OWF_Assignment.updateAgreementOwnerByAssignment((List<pse__Assignment__c>)Records,null);    
        DAOH_OWF_Assignment.updateRrStatus((List<pse__Assignment__c>)Records,null);  
        DAOH_OWF_Assignment.updateScheduleHours((List<pse__Assignment__c>)Records,null);  
    }
         
    /**
    * This method is used for Before update of the Assignment trigger.
    * @return void
    */
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        DAOH_OWF_Assignment.updateAssignmentOwner((List<pse__Assignment__c>)Records, (Map<Id, pse__Assignment__c>)existingRecords);
        DAOH_OWF_Assignment.updateFieldsWhenStatusAndResourceChanged((List<pse__Assignment__c>)Records, (Map<Id, pse__Assignment__c>)existingRecords);
    }
    
    /**
     * This method is used for after update of the Assignment trigger.
     * @return void
     */
    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        DAOH_OWF_Assignment.createSkillCertRatingsOnAssignCompleted((List<pse__Assignment__c>)Records, (Map<Id, pse__Assignment__c>)existingRecords);
        DAOH_OWF_Assignment.populateRollupAssignmentFieldsOnContact((List<pse__Assignment__c>)Records, (Map<Id, pse__Assignment__c>)existingRecords);
        DAOH_OWF_Assignment.updateAgreementOwnerByAssignment((List<pse__Assignment__c>)Records, (Map<Id, pse__Assignment__c>)existingRecords);
        DAOH_OWF_Assignment.updateRelatedFieldsOnRejected((List<pse__Assignment__c>)Records,(Map<Id, pse__Assignment__c>)existingRecords);
    }
    
    /**
     * This method is used for after delete of the Assignment trigger.
     * @return void
     */
     
    public override void onAfterDelete() {
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        DAOH_OWF_Assignment.populateRollupAssignmentFieldsOnContact((List<pse__Assignment__c>)Records,null);
    }  
 
}
