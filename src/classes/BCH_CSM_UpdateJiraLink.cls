global class BCH_CSM_UpdateJiraLink  implements Database.Batchable<sObject>,Database.AllowsCallouts{
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query='select Id, assignee__c, case_recordId__c, description__c, issue_type__c, jira_base_url__c, jira_key__c, priority__c, reporter__c, status__c, summary__c, fixVersions__c, PSA_Project__c, due_date__c, customer__c, country__c from CSM_QI_JiraLink__c';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<CSM_QI_JiraLink__c> jls){
        List<CSM_QI_JiraLink__c> updateJls=new List<CSM_QI_JiraLink__c>();
        List<CSM_QI_JiraLinkComment__c> jlclToUpdate = new List<CSM_QI_JiraLinkComment__c>();
        List<CaseComment> caseCommentToInsert = new List<CaseComment>();
        for(CSM_QI_JiraLink__c jl : jls){
            String url = jl.jira_base_url__c +CON_JIRA.ISSUE_ENDPOINT+jl.jira_key__c;
            try{
                HttpResponse res = SRV_CSM_JiraCallouts.makeGetCallout(url);      
                if (res.getStatusCode()==200) {
                    Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                    Map<String, Object> fields = (Map<String, Object>)m.get('fields');
                    if(jl.summary__c != fields.get('summary'))jl.summary__c = (String)fields.get('summary');
                    if(jl.description__c != fields.get('description'))jl.description__c = (String)fields.get('description');
                    
                    Map<String, Object> issuetype = (Map<String, Object>)fields.get('issuetype');
                    if(jl.issue_type__c != issuetype.get('name'))jl.issue_type__c = (String)issuetype.get('name');
                    
                    Map<String, Object> status = (Map<String, Object>)fields.get('status');
                    if(jl.status__c != status.get('name')){
                        jl.status__c = (String)status.get('name');
                        try {
	                        CaseComment caseComment = new CaseComment();
	                        caseComment.ParentId = jl.case_recordId__c;
	                        caseComment.CommentBody = 'JIRA status was updated with the value ' + jl.status__c;
                            List<User> users = [select Id from User where Name = 'Jira Service User'];
                            if(users != null && !users.isEmpty()){
	                            caseComment.CreatedById = users[0].Id;
                            }
                            caseCommentToInsert.add(caseComment);
	                    } catch(DmlException e) {
                            System.debug('The following exception has occurred to insert case comment: ' + e.getMessage());
                        }
                    }
                    
                    Map<String, Object> priority = (Map<String, Object>)fields.get('priority');
                    if(jl.priority__c != priority.get('name'))jl.priority__c = (String)priority.get('name');
                    
                    Map<String, Object> reporter = (Map<String, Object>)fields.get('reporter');
                    if(jl.reporter__c != reporter.get('displayName'))jl.reporter__c = (String)reporter.get('displayName');
                    
                    Map<String, Object> assignee = (Map<String, Object>)fields.get('assignee');
                    if(assignee != null)
                        if(jl.assignee__c != assignee.get('displayName'))jl.assignee__c = (String)assignee.get('displayName');
                  
                    List<Map<String, Object>> fixVersions = new List<Map<String, Object>>();
                    List<Object> listFixVersions = (List<Object>)fields.get('fixVersions');
                    if (listFixVersions != null){
	                    for (Object instance : listFixVersions)
	                           fixVersions.add((Map<String, Object>)instance);
	                        String fixVersion='';
	                        for (Integer i = 0; i < fixVersions.size(); i++) {
	                            fixVersion+=fixVersions.get(i).get('name')+';';
	                        }
	                        if (fixVersion.length()>0)
	                            jl.fixVersions__c = fixVersion.substring(0, fixVersion.length() - 1);
                    }
                    if (fields.get('customfield_16646') != null)
                        if(jl.PSA_Project__c != fields.get('customfield_16646'))jl.PSA_Project__c = (String)fields.get('customfield_16646');
                   
                    if (fields.get('customfield_14449') != null)
                        if(jl.due_date__c != date.valueOf((String)fields.get('customfield_14449'))){
                            jl.due_date__c = date.valueOf((String)fields.get('customfield_14449'));
                        }
                   
                    if (fields.get('customfield_14510') != null)
                        if(jl.country__c != fields.get('customfield_14510')){
                            Map<String, Object> country = (Map<String, Object>)fields.get('customfield_14510');
                            jl.country__c = (String)country.get('value');
                        }
                    if (fields.get('customfield_14511') != null)
                        if(jl.customer__c != fields.get('customfield_14511')){
                            Map<String, Object> customer = (Map<String, Object>)fields.get('customfield_14511');
                            jl.customer__c = (String)customer.get('value');
                        }
                    try{         
                        updateJls.add(jl);
                    }catch(DmlException e) {
                        System.debug('The following exception has occurred to update jiraLink: ' + e.getMessage());
                    }
                }else{
                    System.debug(jl.jira_base_url__c + ' error');
                }
                try {
                    HttpResponse res2 = SRV_CSM_JiraCallouts.makeGetCallout(url + '/comment');      
                    if (res2.getStatusCode()==200) {
                        Map<String, Object> m2 = (Map<String, Object>) JSON.deserializeUntyped(res2.getBody());
                        List<Map<String, Object>> jiraComments = new List<Map<String, Object>>();
                        for (Object jiraComment : (List<Object>)m2.get('comments'))
                            jiraComments.add((Map<String, Object>)jiraComment);    

                        CSM_QI_JiraLinkComment__c jlc;
                        for (Map<String, Object> comment : jiraComments) {
                            List<CSM_QI_JiraLinkComment__c> jlcl = new List<CSM_QI_JiraLinkComment__c>();
                            String jiraCommentId = (String)comment.get('id');
                            jlcl = [select Id, Source__c from CSM_QI_JiraLinkComment__c where JiraCommentId__c =:jiraCommentId  and JiraLink__c =: jl.Id];
                            if (jlcl.size() > 0) jlc = jlcl[0];
                            else jlc = new CSM_QI_JiraLinkComment__c();
                            jlc.Body__c = (String)comment.get('body');
                            jlc.JiraCommentId__c = (String)comment.get('id');
                            jlc.JiraCommentCreatedDate__c =  (Datetime) JSON.deserialize('"' + (String)comment.get('created') + '"', Datetime.class);
                            jlc.JiraCommentUpdatedDate__c =  (Datetime) JSON.deserialize('"' + (String)comment.get('updated') + '"', Datetime.class);
                            jlc.JiraLink__c = jl.Id;
                            Map<String, Object> author = (Map<String, Object>)comment.get('author');
                            if(jlc.Source__c != 'CSM') {
                                jlc.Author__c = (String)author.get('displayName');
                                jlc.Source__c = 'JIRA';
                            }
                            Map<String, Object> updateAuthor = (Map<String, Object>)comment.get('updateAuthor');
                            jlc.UpdateAuthor__c = (String)updateAuthor.get('displayName');
                            jlclToUpdate.add(jlc);
                        }
                        
                    }
                } catch (System.CalloutException e) {
                    System.debug('ERROR:' + e);
                }
            }catch(System.CalloutException e) {
                System.debug('ERROR:' + e);
            }
       }
       insert caseCommentToInsert;
       update updateJls;
       upsert jlclToUpdate;
    }
    
    global void finish(Database.BatchableContext BC){

    }

}