/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Task
 */
public class SLT_Task extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_Task() {
        super(false, false, false);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Task.Id,
            Task.Status,
            Task.WhatId,
            Task.OwnerId
        };
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Task.sObjectType;
    }
    
    /**
     * This method used to get Task by Id
     * @return  List<Task>
     */
    public List<Task> selectById(Set<ID> idSet) {
        return (List<Task>) selectSObjectsById(idSet);
    }
   /**
     * This method used to get Task by WhatId and OwnerId
     * @return  Map<Id, User>
     */
    public List<Task> selectNonCompletedTaskByWhatIdAndOwnerId(Set<String> fieldSet, Id whatId, Id ownerId) {
        return new List<Task>((List<Task>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('WhatId =: whatId AND OwnerId =: ownerId AND Status != \'Completed\'').toSOQL()));
    }
    
    
}