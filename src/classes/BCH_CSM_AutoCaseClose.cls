global class BCH_CSM_AutoCaseClose implements Database.Batchable<sObject>{
    String[] errors = new List<String>();
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query='select Id,CaseNumber,Status,RecordTypeName__c,LastModifiedDate,Los__c,SubType1__c,SubStatus__c,CaseSource__c,CurrentQueue__r.Name,(Select Id, ParentId,CaseNumber, Status from Cases where ((Status not in (\'Waiting for\',\'Resolved\',\'Closed\') and RecordTypeName__c in (\'RandDCase\',\'ActivityPlan\')) OR (Status not in (\'Resolved with Customer\',\'Closed\') and SubStatus__c != \'RCA Requested\' and RecordTypeName__c = \'TechnologyCase\'))) From Case Where LastModifiedDate < LAST_N_DAYS:5 and Status in (\'Resolved with Customer\',\'Resolved\') and RecordTypeName__c in (\'TechnologyCase\',\'RandDCase\',\'ActivityPlan\',\'DATACase\')';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Case> cases){
        List<Case> updateCaseist = new List<Case>();
        List<Id> uParentCases = new List<Id>();
        
        for(Case cas : cases){
            if(cas.cases.isEmpty()) {
                if(cas.RecordTypeName__c == 'ActivityPlan' && cas.Los__c == 'Account Management' && cas.SubType1__c == 'Inform' && !(cas.CaseSource__c == 'GDN/CAS' || cas.CaseSource__c == 'Study Team') ){
                    
                }else if(cas.RecordTypeName__c == 'TechnologyCase' && cas.SubStatus__c  == 'RCA Requested'){
                    
                }else if(cas.RecordTypeName__c != 'DATACase'){
                    cas.Status = 'Closed';
                    updateCaseist.add(cas);
                }else{
                    if(cas.CurrentQueue__c != null){
                        if(cas.CurrentQueue__r.Name.equalsIgnoreCase('Data Global E-Service')){
                            cas.Status = 'Closed';
                            updateCaseist.add(cas);
                        }
                    }
                }
                
            }
        }
        try {
            if(!updateCaseist.isEmpty()){
              update updateCaseist;  
            }
            
        } catch(Exception e) {
            errors.add(e.getLineNumber()+' - ' + e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
        if(!errors.isEmpty()) {
            errors.remove(null);
            string allstring = string.join(errors,',');
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Errors occurred during BCH_CSM_AutoCaseClose batch process.');
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setSaveAsActivity(false);
            mail.setPlainTextBody(allstring);
            
            try{
                Messaging.sendEmail(new Messaging.Email[] { mail });
            } catch(Exception e) {
                
            }
        }

    }
    
}