@isTest
public class TST_CNT_CPQ_SendEmail {

    @testSetup
    static void dataSetUp() {
        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        
        Opportunity testOpportunity= UTL_TestData.createOpportunity(testAccount.Id);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        testOpportunity.Legacy_Quintiles_Opportunity_Number__c='MVP123';
        insert testOpportunity;
        
        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = testOpportunity.Id;
        testAgreement.RecordTypeId = RecordTypeId;
        testAgreement.Agreement_Status__c = 'Draft';
        testAgreement.Process_Step__c = 'None';
        testAgreement.Select_Pricing_Tool__c = CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP;
        insert testAgreement;
        
        ContentVersion attach = new ContentVersion();       
        attach.Title = 'Unit Test Attachment'; 
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.VersionData = bodyBlob;
        attach.PathOnClient='abc';
        insert attach;
    }
    
    private static Id getAgreementId() {
        Apttus__APTS_Agreement__c testAgreement = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
        return testAgreement.Id;
    } 
    
    @isTest
    static void testsendMail() {
        Id agreementId = getAgreementId();
        List<ContentVersion> attachments=[select id, Title, VersionData, ContentDocumentId from ContentVersion];
        
        Test.startTest();
            CNT_CPQ_SendEmail.sendMailMethod(agreementId, 'test@test.com', 'test@test.com', 'test@test.com', 'test subject', 'test body', new List<String>{attachments[0].ContentDocumentId});
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        
        System.assertEquals(1, invocations, 'Should Send Mail');
    }
    
    @isTest
    static void testDeleteAttachments() {
        List<ContentVersion> attachments=[select id, Title, VersionData, ContentDocumentId from ContentVersion];
        
        Test.startTest();
            CNT_CPQ_SendEmail.deleteAttachments(new String[]{attachments[0].ContentDocumentId});
        Test.stopTest();
        
        List<ContentVersion> actualAttachments=[select id, Title, VersionData, ContentDocumentId from ContentVersion];
        
        System.assertEquals(0, actualAttachments.size(), 'Should delete attachment');
    }
    
    @isTest
    static void testGetEmailTemplate() {
        Id agreementId = getAgreementId();
        
        Test.startTest();
            List<String> emailTemplates = CNT_CPQ_SendEmail.getEmailTemplate(agreementId);
        Test.stopTest();
        
        System.assertEquals(true, emailTemplates.size()>0, 'Should return emailTemplates');
    }
    
    @isTest
    static void testUpdateAgreementPrcoessStep() {
        Id agreementId = getAgreementId();
        
        Test.startTest();
            CNT_CPQ_SendEmail.updateAgreementPrcoessStep(agreementId, 'None', 'Draft');
        Test.stopTest();
        
        Apttus__APTS_Agreement__c actualAgreement = [SELECT Process_Step__c FROM Apttus__APTS_Agreement__c LIMIT 1];
        System.assertEquals('None', actualAgreement.Process_Step__c, 'Should update agreement');
    }
}