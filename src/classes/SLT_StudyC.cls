public class SLT_StudyC extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_StudyC() {
        super(false, true, true);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Study__c.sObjectType;
    }
    
    /**
     * This method used to get Study__c by Id
     * @return  Map<Id, Study__c>
     */
    public Map<Id, Study__c> selectByStudyId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Study__c>((List<Study__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    /**
     * This method used to get Study__c by Id
     * @return  List<Study__c>
     */
    public List<Study__c> selectAccountByStudyId(Set<ID> idSet, Set<String> fieldSet) {
        return (List<Study__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL());
    }
    
    /**
     * This method used to get Study__c by Name
     * @return  List<Study__c>
     */
    public List<Study__c> selectAccountByStudyName(String name, Set<String> fieldSet) {
        return (List<Study__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Name =:name').toSOQL());
    }
    
    public List<Study__c> selectAllStudies(Set<String> fieldSet)
    {
        return (List<Study__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).toSOQL()); 
    }
}
