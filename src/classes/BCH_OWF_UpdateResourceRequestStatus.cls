/**
 * This is Batch class to udpate status on Resource_Requests based on related Agreement's Bid_Due_Date field
 * version : 1.0
 */
global class BCH_OWF_UpdateResourceRequestStatus implements Database.Batchable<sObject>, Database.Stateful{
    @TestVisible Map<Id, String> resRequestIdToErrorMessageMap;
    
    /**
     * Constructor
     */
    public BCH_OWF_UpdateResourceRequestStatus() {
        resRequestIdToErrorMessageMap = new Map<Id, String>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
         String query = 'Select Id, pse__Status__c ' + 
                        ' From pse__Resource_Request__c ' + 
                        ' Where pse__Project__c != NULL And Agreement__c != NULL And RecordType.DeveloperName = \''+ CON_OWF.OWF_RESOURCE_REQUEST_RECORD_TYPE_NAME + '\'' +
                        //' And Agreement__r.Bid_Due_Date__c = TODAY And pse__Status__c != \''+ CON_OWF.OWF_STATUS_COMPLETED + '\'';
                        ' And Agreement__r.Bid_Due_Date__c <= TODAY And pse__Status__c = \''+ CON_OWF.OWF_STATUS_ASSIGNED + '\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<pse__Resource_Request__c> resourceRequests) {
        List<pse__Resource_Request__c> resourceRequestsUpdateList = new List<pse__Resource_Request__c>();
        
        for(pse__Resource_Request__c resRequest : resourceRequests){
            resRequest.pse__Status__c = CON_OWF.OWF_STATUS_COMPLETED;
            resourceRequestsUpdateList.add(resRequest);
        }
        if(!resourceRequestsUpdateList.isEmpty()) {
            Database.SaveResult[] results = Database.Update(resourceRequestsUpdateList, false);
            if (results.size() > 0){
                for (Integer i=0; i< results.size(); i++){
                    if (!results[i].isSuccess()) {
                        resRequestIdToErrorMessageMap.put(resourceRequestsUpdateList[i].Id, 'Error in updating status of Resource Request : ' + results[i].getErrors()[0].getMessage());
                    }
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if (resRequestIdToErrorMessageMap.size() > 0 || Test.isRunningTest()){
            UTL_OWF.sendMailOnException(CON_OWF.BCH_OWF_UPDATERESOURCEREQUESTSTATUS, resRequestIdToErrorMessageMap, 'Completed Status Update on Resource Request Batch: Failed');
        }
        
        //Run batch after completing Resource_Request batch to update status on Assignments
        OWF_Batch_Config__c batchConfig = OWF_Batch_Config__c.getInstance(CON_OWF.BCH_OWF_UPDATEASSIGNMENTSSTATUS);
        Integer batchSize;
        if(batchConfig != null && batchConfig.Batch_Size__c != null)
            batchSize = (Integer)batchConfig.Batch_Size__c;
        else
            batchSize = CON_OWF.DEFAULT_BATCH_SIZE;
        BCH_OWF_UpdateAssignmentsStatus batch = new BCH_OWF_UpdateAssignmentsStatus();
        database.executeBatch(batch, batchSize);
    }
}