/*
 * Version       : 1.0
 * Description   : This Scheduler Class for BCH_CRM_OpportunityStage
 */
public class SCH_CRM_OpportunityStage implements Schedulable {
    /**
     * execute method 
     * @params  SchedulableContext context
     * @return  void
     */
    public void execute(SchedulableContext SC) {
        BCH_CRM_OpportunityStage batchOppStage = new BCH_CRM_OpportunityStage();
        Database.executeBatch(batchOppStage);
    }
}