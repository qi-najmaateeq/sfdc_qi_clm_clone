public class DAOH_QueueUserRelationship {

    public static void customEmailAddressValidation(List<Queue_User_Relationship__c> queueUserList){
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern patt = null;
        Matcher match = null;
        if(queueUserList != null && queueUserList.size() > 0){
            for(Queue_User_Relationship__c queueUserRecord : queueUserList){
                if(queueUserRecord.AFU_Queue_Email__c != null && queueUserRecord.AFU_Queue_Email__c != ''){
                    patt = Pattern.compile(emailRegex);
                    match = patt.matcher(queueUserRecord.AFU_Queue_Email__c.trim());
                    if(!match.matches()){
                        queueUserRecord.AFU_Queue_Email__c.addError('Invalid email address format : '+queueUserRecord.AFU_Queue_Email__c);
                    }
                }
            }
        }
    }
}