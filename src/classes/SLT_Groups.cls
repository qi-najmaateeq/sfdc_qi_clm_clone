/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Group
 */
public class SLT_Groups {
    /**
     * This method is used to get Groups with group members
     * @params Set<Id> grpNameSet
     * @return List<Group> 
     */
    public List<Group> getGroupsWithGroupMembersByGroupName(Set<String> grpNameSet) {
        return [SELECT Id, Name, (SELECT UserOrGroupId 
                                   FROM GroupMembers)
                FROM Group g 
                WHERE Name IN : grpNameSet
               ];
    }
}