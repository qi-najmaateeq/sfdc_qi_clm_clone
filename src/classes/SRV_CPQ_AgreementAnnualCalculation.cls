public class SRV_CPQ_AgreementAnnualCalculation {
	/**
	 * Get existing Annual Calculations associated with an agreement Id
	 * @param	Id agmtId
	 * @return	List<Apttus_Config2__ProductConfiguration__c>
	 */
	public static List<Agreement_Annual_Calculation__c> getExistingAnnualCalculations(Id agmtId) {
        return [Select Id From Agreement_Annual_Calculation__c Where Agreement__c = :agmtId];
	}

	/**
	 * Get finalized product configuration with Subscription Fee and Implementation Fee lines
	 * for Agreement.
	 * @param	Id agmtId
	 * @return	List<Apttus_Config2__ProductConfiguration__c>
	 */
	public static Apttus_Config2__ProductConfiguration__c getFinalizedProductConfiguration(Id agmtId) {
        List<Apttus_Config2__ProductConfiguration__c> config =
            [Select Id,
                    Apttus_CMConfig__AgreementId__c,
                    Apttus_CMConfig__AgreementId__r.O_Ammortize_Y_N__c,
                    Apttus_CMConfig__AgreementId__r.O_Annual_Price_Escalation_Factor__c,
                    Apttus_CMConfig__AgreementId__r.O_Money_Factor__c,
                    Apttus_CMConfig__AgreementId__r.O_Term_Years__c, 
                    (Select Id, Apttus_Config2__ExtendedPrice__c, Apttus_Config2__ChargeType__c
                     From Apttus_Config2__LineItems__r
                     Where Apttus_Config2__ChargeType__c in ('Subscription Fee', 'Implementation Fee')) 
             From Apttus_Config2__ProductConfiguration__c
             Where Apttus_CMConfig__AgreementId__c = :agmtId
             And Apttus_CMConfig__AgreementId__r.RecordType.DeveloperName = 'Subscription_Products'
             And Apttus_Config2__Status__c = 'Finalized'
             Limit 1];

        Apttus_Config2__ProductConfiguration__c pc = null;

        if (config != null && !config.isEmpty()) {
        	pc = config[0];
        }

        return pc;
	}

	/**
	 * This method calculates the total subscription fee from a List of Line Items
	 * 
	 * @param   List<Apttus_Config2__LineItem__c> items
	 * @return  Decimal
	 */
	public static Decimal getSubscriptionTotal(List<Apttus_Config2__LineItem__c> items) {
        // Total Subscription Fees and Implementation Fees by Config
        Decimal subscriptionTotal = 0.0;
        for (Apttus_Config2__LineItem__c item: items) {
            if (item.Apttus_Config2__ChargeType__c == 'Subscription Fee') {
                subscriptionTotal += item.Apttus_Config2__ExtendedPrice__c;
            }
        }

        return subscriptionTotal;
	}

	/**
	 * This method calculates the total implementation fee from a List of Line Items
	 * 
	 * @param   List<Apttus_Config2__LineItem__c> items
	 * @return  Decimal
	 */
	public static Decimal getImplementationTotal(List<Apttus_Config2__LineItem__c> items) {
        // Total Subscription Fees and Implementation Fees by Config
        Decimal implementationTotal = 0.0;
        for (Apttus_Config2__LineItem__c item: items) {
			if (item.Apttus_Config2__ChargeType__c == 'Implementation Fee') {
				implementationTotal += item.Apttus_Config2__ExtendedPrice__c;
			}
        }

        return implementationTotal;
	}

	/**
	 * Calculate annual escalation and amortization values for an Agreement.
	 * 
	 * @param   Id agmtId
	 * @param	Apttus_Config2__ProductConfiguration__c pc
	 * @param	Decimal term
	 * @param	Decimal subscriptionTotal
	 * @param	Decimal implementationTotal
	 * @return  List<Agreement_Annual_Calculation__c>
	 */
	public static List<Agreement_Annual_Calculation__c> calculateAnnualEscalationAndAmortization
			(Id agmtId, Apttus_Config2__ProductConfiguration__c pc, 
			 Decimal term, Decimal subscriptionTotal, Decimal implementationTotal) {
        // Calculate Subscription and Implementation Totals by Config
        List<Agreement_Annual_Calculation__c> aacList = new List<Agreement_Annual_Calculation__c>();
        List<Apttus_Config2__LineItem__c> items = pc.Apttus_Config2__LineItems__r;

        // Calculate escalation and amortization
        Decimal escalationFactor = 1.0;
        if (pc.Apttus_CMConfig__AgreementId__r.O_Annual_Price_Escalation_Factor__c != null) {
            escalationFactor = 1.0 + (pc.Apttus_CMConfig__AgreementId__r.O_Annual_Price_Escalation_Factor__c/100.0);
        }
        Decimal annualSubscriptionFee = subscriptionTotal/term;
        Decimal runningEscalation = annualSubscriptionFee;
        Decimal totalEscalation = 0.0;

        Decimal moneyFactor = 1.0;
        if (pc.Apttus_CMConfig__AgreementId__r.O_Money_Factor__c != null) {
            moneyFactor = 1.0 + (pc.Apttus_CMConfig__AgreementId__r.O_Money_Factor__c/100.0);
        }
        Decimal amortization = implementationTotal/term;
        Decimal amortizationWithMoneyFactor = amortization * moneyFactor;
        Decimal totalAmortization = 0.0;

        for (Integer year = 1; year <= Integer.valueOf(term); year++) {
            Agreement_Annual_Calculation__c aac = new Agreement_Annual_Calculation__c();
            aac.Agreement__c = pc.Apttus_CMConfig__AgreementId__c;
            aac.Year__c = year;
            aac.Subscription_Revenue__c = annualSubscriptionFee;
            aac.Escalation__c = runningEscalation;
            totalEscalation += runningEscalation;
            runningEscalation = runningEscalation * escalationFactor;
            if (pc.Apttus_CMConfig__AgreementId__r.O_Ammortize_Y_N__c == 'Yes') {
                aac.Amortization__c = amortization;
                if (year > 1) {
                    aac.Amortization_With_Money_Factor__c = amortizationWithMoneyFactor;
                } else {
                    aac.Amortization_With_Money_Factor__c = amortization;
                }
                totalAmortization += aac.Amortization_With_Money_Factor__c;
            }
            aacList.add(aac);
        }

        return aacList;
	}

	/**
	 * Calculate agreement-level totals from annual escalation and amortization data.
	 * 
	 * @param   Id agmtId
	 * @param	Apttus_Config2__ProductConfiguration__c pc
	 * @param	Decimal term
	 * @param	Decimal subscriptionTotal
	 * @param	Decimal implementationTotal
	 * @return  List<Agreement_Annual_Calculation__c>
	 */
	public static Apttus__APTS_Agreement__c calculateAgreementTotals
			(Id agmtId, List<Agreement_Annual_Calculation__c> aacList,
			 Decimal term, Decimal subscriptionTotal, Decimal implementationTotal) {
        Decimal totalEscalation = 0.0;
        Decimal totalAmortization = 0.0;

        for (Agreement_Annual_Calculation__c aac: aacList) {
        	if (aac.Escalation__c != null) {
            	totalEscalation += aac.Escalation__c;
            }
            if (aac.Amortization_With_Money_Factor__c != null) {
            	totalAmortization += aac.Amortization_With_Money_Factor__c;
            } else if (aac.Amortization__c != null) {
            	totalAmortization += aac.Amortization__c;
            }
        }

        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c(Id = agmtId);
        agmt.O_Adjusted_Total_Revenue__c = totalEscalation;
        agmt.O_Adjusted_Base_Revenue__c = totalEscalation/term;
        agmt.O_Escalation_Average__c = 0.0;
        if (subscriptionTotal > 0 && totalEscalation > 0) {
            agmt.O_Escalation_Average__c = 100.0 * (totalEscalation - subscriptionTotal)/subscriptionTotal;
        }
        agmt.O_Adjusted_Total_Implementation__c = totalAmortization;
        agmt.O_Adjusted_Base_Implementation__c = totalAmortization/term;
        agmt.O_Amortization_Average__c = 0.0;
        if (implementationTotal > 0 && totalAmortization > 0) {
            agmt.O_Amortization_Average__c = 100.0 * (totalAmortization - implementationTotal)/implementationTotal;
        }

        return agmt;
    }
}