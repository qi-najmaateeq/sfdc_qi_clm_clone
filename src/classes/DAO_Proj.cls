/**
 * This is Project (pse__Proj__c) trigger handler class.
 * version : 1.0
 */
public class DAO_Proj extends fflib_SObjectDomain {
      
    /**
     * Constructor of this class
     * @params sObjectList List<pse__Proj__c>
     */
    public DAO_Proj(List<pse__Proj__c> sObjectList) {
        super(sObjectList);
    }

    /**
     * Constructor Class for construct new Instance of This Class
     */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new DAO_Proj(sObjectList);
        }
    }
    
    /**
     * This method is used for before insert of the Project (pse__Proj__c) trigger.
     * @return void
     */
    public override void onBeforeInsert() {
        DAOH_OWF_Proj.checkExistingProjectForAgreement((List<pse__Proj__c>)records);
    }
    
    /**
     * This method is used for after insert of the Project (pse__Proj__c) trigger.
     * @return void
     */
    public override void onAfterInsert() {
        DAOH_OWF_Proj.createRRBasedOnAgrAssociatedWithProj((List<pse__Proj__c>)records);
        DAOH_OWF_Proj.createClinicalBidRRsBasedOnBidNoOfProjects((List<pse__Proj__c>)records);
    }
    /**
     * This method is used for after insert of the Project (pse__Proj__c) trigger.
     * @return void
     */
    public override void onBeforeDelete() {
        DAOH_OWF_Proj.deleteResourceRequestBasedOnProject((List<pse__Proj__c>)records);
    }
}