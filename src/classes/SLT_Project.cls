/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Project
 */
public class SLT_Project extends fflib_SObjectSelector {
    
    /**
     * constructor to initialize CRUD and FLS
     */
    public SLT_Project() {
        super(false, true, true);
    }
    
    /**
     * constructor to initialise CRUD and FLS with a parameter for FLS.
     */
    public SLT_Project(Boolean enforceFLS) {
        super(false, true, enforceFLS);
    }
    public SLT_Project(Boolean enforceFLS,Boolean enforceCRUD) {
        super(false, enforceCRUD, enforceFLS);
    }
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return pse__Proj__c.sObjectType;
    }
    
     /**
     * This method used to get Project by with Agreement
     * @params  Set<Id> agreementIdset
     * @params  Set<String> projectFieldSet
     * @return  Map<Id, pse__Proj__c>
     */
    public Map<Id, pse__Proj__c> getProjectByAgreementID(Set<ID> agreementIdset, Set<String> projectFieldSet) {
        fflib_QueryFactory projectQueryFactory = newQueryFactory(true);
        String queryString = projectQueryFactory.selectFields(projectFieldSet).setCondition('Agreement__c in :agreementIdset').toSOQL();
        return new Map<Id, pse__Proj__c>((List<pse__Proj__c>) Database.query(queryString));
    }
}