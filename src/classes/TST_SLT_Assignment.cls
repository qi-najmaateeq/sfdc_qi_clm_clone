/**
 * This test class is used to test all methods in Assignment service class
 * version : 1.0
 */
@isTest
private class TST_SLT_Assignment {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        insert agreement;
        pse__proj__c bidProject = [Select id from pse__Proj__c where Agreement__c =: agreement.Id];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest.pse__Group__c = grp.Id;
        insert resourceRequest;
    }
    
    /**
     * This test method used to cover basic methods
     */ 
    static testMethod void testAssignment() {
        List<Schema.SObjectField> assignmenetList = new SLT_Assignment().getSObjectFieldList();
        Schema.SObjectType assignment = new SLT_Assignment(true).getSObjectType();
    } 
    
    /**
     * This test method used to cover SelectAssignmentsByIdSet method
     */ 
    static testMethod void testSelectAssignmentsByIdSet() {   
        pse__Resource_Request__c resourceRequest = [Select Id From pse__Resource_Request__c Limit 1];
        Set<ID> sObjectIdset = new Set<ID>{resourceRequest.Id};
        String condition = 'pse__Resource_Request__c in :sObjectIdset';
        Set<String> assignmentFieldSet = new Set<String>{'Id', 'pse__Status__c'};
        Map<Id, pse__Assignment__c> assignmentMap = new SLT_Assignment(true).selectAssignmentsByIdSet(sObjectIdset, condition, assignmentFieldSet);
    } 
    
    /**
     * This test method used to get Assignment using Resource Request
     */ 
    static testMethod void testGetAssignmentResourceRequest() {   
        pse__Resource_Request__c resourceRequest = [Select Id From pse__Resource_Request__c Limit 1];
        Set<ID> sObjectIdset = new Set<ID>{resourceRequest.Id};
        Set<String> assignmentFieldSet = new Set<String>{'Id'};
            
        Test.startTest();
            Map<Id, pse__Assignment__c> assignmentMap = new SLT_Assignment(true).getAssignmentResourceRequest(sObjectIdset, assignmentFieldSet);
        Test.stopTest();
        
        Integer expected = 0;
        System.assertEquals(expected, assignmentMap.size());
    } 
    
    /**
     * This test method used to get Assignment by resource id or any sobject id
     */ 
    static testMethod void testGetAssignmentWithCondition() {   
        Contact cont = [Select Id From Contact Limit 1];
        Apttus__APTS_Agreement__c agr = [Select Id from Apttus__APTS_Agreement__c  limit 1];
        Set<ID> sObjectIdset = new Set<ID>{cont.Id};
        Set<ID> agrIdset = new Set<ID>{agr.Id};
        Set<String> assignmentFieldSet = new Set<String>{'Id'};
        String assignmentCondition1 = 'pse__resource__c in: sObjectIdset';
        String assignmentCondition2 = '';    
        Test.startTest();
            List<pse__Assignment__c> assignmentList = new SLT_Assignment(true).getAssignmentByResource(sObjectIdset,assignmentCondition1, assignmentFieldSet);
            assignmentList = new SLT_Assignment(true).getAssignmentByAgrAndCondition(agrIdset,assignmentCondition2 , assignmentFieldSet);
        Test.stopTest();
        
        Integer expected = 0;
        System.assertEquals(expected, assignmentList.size());
    } 
}