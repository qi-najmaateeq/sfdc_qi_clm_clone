@isTest
private class TST_SLT_QCCheckListItem {

    static QC_Check_List_Item__c setQCCheckListItemData(String typeOfProcessStep, String typeOfBid){
    
        QC_Check_List_Item__c qcCheckListItem = UTL_TestData.createQCCheckListItem();
        qcCheckListItem.Type_Of_Process_Step__c = typeOfProcessStep;
        qcCheckListItem.Type_Of_Bids__c = typeOfBid;
        qcCheckListItem.Record_Type__c = CON_CPQ.INITIAL_BID_REBID;
        insert qcCheckListItem;
        return qcCheckListItem;
    }

    @isTest
    static void testSelectQCCheckListItemById() {

        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData('Draft', CON_CPQ.OPPORTUNITY_RFP);
            Set<Id> idSet = new Set<Id>{qcCheckListItem.Id};

        Test.startTest();
            List<QC_Check_List_Item__c> qcSelfCheckDraftList = new SLT_QCCheckListItem().selectById(idSet);
        Test.stopTest();

        system.assertEquals(1, qcSelfCheckDraftList.size(), 'Should return one Qc Check List Item');
    }

    @isTest
    static void testGetQCCheckListItemByProcessStepBidTypeRecordType() {

        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData('Draft', CON_CPQ.OPPORTUNITY_RFP);
        Set<Id> idSet = new Set<Id>{qcCheckListItem.Id};
        Set<String> qcFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.RECORDTYPE, CON_CPQ.TYPE, CON_CPQ.QUESTION, CON_CPQ.GUIDELINES, 
            CON_CPQ.TYPE_OF_PROPOSAL, CON_CPQ.TYPE_OF_BIDS};

        Test.startTest();
            List<QC_Check_List_Item__c> qcSelfCheckDraftList = new SLT_QCCheckListItem().getQCCheckListItemByProcessStepBidTypeRecordType(CON_CPQ.INITIAL_BID_REBID, 
                qcFieldSet, 'Draft', CON_CPQ.OPPORTUNITY_RFP);
        Test.stopTest();

        system.assertEquals(1, qcSelfCheckDraftList.size(), 'Should return one Qc Check List Item');
    }

    @isTest
    static void testGetQCCheckListItemByRecordType() {

        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData('Draft', CON_CPQ.OPPORTUNITY_RFP);
        Set<Id> idSet = new Set<Id>{qcCheckListItem.Id};
        Set<String> qcFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.RECORDTYPE, CON_CPQ.TYPE, CON_CPQ.QUESTION, CON_CPQ.GUIDELINES, 
            CON_CPQ.TYPE_OF_PROPOSAL, CON_CPQ.TYPE_OF_BIDS};

        Test.startTest();
            List<QC_Check_List_Item__c> qcSelfCheckDraftList = new SLT_QCCheckListItem().getQCCheckListItemByRecordType(CON_CPQ.INITIAL_BID_REBID, qcFieldSet);
        Test.stopTest();

        system.assertEquals(1, qcSelfCheckDraftList.size(), 'Should return one Qc Check List Item');
    }

}