global class SCH_CRM_ContractMSAReviewBatch implements Schedulable {
    global void execute(SchedulableContext sc) {
        BCH_CRM_ContractMSAReviewDateAlertBatch b = new BCH_CRM_ContractMSAReviewDateAlertBatch();
        Database.executebatch(b);
    }
}