/**
 * This class used for constants used in CRM project.
 * version : 1.0
 */
public class CON_CRM {
    // This is all stageName Constant
    public static final String IDENTIFYING_OPP_STAGE = '1. Identifying Opportunity';
    public static final String QUALIFYING_OPP_STAGE = '2. Qualifying Opportunity';
    public static final String DEVELOPING_PROP_STAGE = '3. Developing Proposal';
    public static final String DELIVERING_PROP_STAGE = '4. Delivering Proposal';
    public static final String FINALIZING_DEAL_STAGE = '5. Finalizing Deal';
    public static final String RECEIVED_ATP_STAGE = '6. Received ATP/LOI';
    public static final String CLOSED_WON_STAGE = '7a. Closed Won';
    public static final String CLOSED_LOST_STAGE = '7b. Closed Lost';
    public static final String IN_HAND_STAGE = 'In-Hand';
    
    // This is all offering group code  Constant
    public static final String OFFERING_GROUP_CODE_GLOBAL_RND = 'GPRDSX';
    public static final String MATERIAL_LEVEL_HIERARCHY_OLI = 'Material';
    
    // This is all offering group Constant
    public static final String OFFERING_GROUP_GLOBAL_RND = 'Research & Development Solutions';
    
    // This is all Object Codes for Objects
    public static final String OPPORTUNITY_OBJECT_CODE = '006';
    public static final String CONTACT_OBJECT_CODE = '003';
    public static final String LEAD_OBJECT_CODE = '00Q';
    
    // This is all for query any object
    public static final String ROUND_BRACKET_OPEN = ' ( ';
    public static final String ROUND_BRACKET_CLOSE = ' ) ';
    public static final String ACTIVE_TRUE = 'isActive = true ';
    public static final String OR_LOGIC = ' OR ';
    public static final String AND_LOGIC = ' AND ';
    public static final String SINGLE_SPACE = ' ';
    public static final String EQUAL_LOGIC = ' = ';
    public static final String LIKE_LOGIC = ' like ';
    public static final String PERCENT_LOGIC = '%';
    public static final String BACKSLASH = '\'';
    public static final String FORWARDSLASH = '/';
    public static final String DOT_LOGIC = '.';
    public static final String PRODUCT_OBJECT = 'Product2';
    public static final String NAME_FIELD = 'name';
    public static final String USER_FIELD = 'user__c';
    public static final String AURA_EXCEPTION = 'System.AuraHandledException';
    public static final String COLON = ':';
    public static final String IN_LOGIC = ' IN ';
    public static final String USD = 'USD';
    
    public static final String OPPORTUNITYLINEITEM_OBJECT_API = 'OpportunityLineItem';
    public static final String FAVORITEPRODUCT_OBJECT_API = 'Favorite_Product__c';
    public static final String EDIT_LABEL = 'edit';
    public static final String CREATE_LABEL = 'create';
    public static final String DELETE_LABEL = 'Delete';
    public static final String SYSTEM_ADMIN_PROFILE = 'System Administrator';
    public static final String COMMERCIAL_OPERATION_PROFILE = 'Commercial Operations User';
    public static final String MULESOFT_SYS_ADMIN_PROFILE = 'Mulesoft System Administrator';
    public static final String TEST_PRODUCT_NAME = 'TestProduct';
    public static final Integer RECORD_LIMIT = 50;
    public static final String OPPORTUNITY_OBJECT = 'Opportunity';
    public static final String CONTACT_OBJECT = 'Contact';
    public static final String ACCOUNT_OBJECT = 'Account';
    public static final String SPLIT_TYPE_NAME = 'Overlay';
    public static final String OPPORTUNTIY_SPLITS = 'OpportunitySplits';
    public static final String OPPORTUNITY_TEAM_MEMBERS = 'OpportunityTeamMembers';
    public static final String OPPORTUNITY_LINE_ITEMS = 'OpportunityLineItems';
    public static final String OPPORTUNITY_BNF_RELATIONSHIP = 'Purchase_BNF_s1__r';
    public static final String OPPORTUNITYLINEITEMGROUP_RELATIONSHIP= 'Line_Item_Groups__r';
    public static final String OPPORTUNITY_MIBNF_RELATIONSHIP = 'MIBNFS__r';
    public static final String OPPORTUNITY_MIBNF_COMPONENT_RELATIONSHIP= 'BNF_s__r';
    public static final String OPPORTUNITY_TERRITORY_FIELD_API = 'Territory__c';
    public static final String PIC_ROLE = 'Principal in Charge';
    public static final String PRODUCT_MATERIAL_TYPE_ZREP = 'ZREP';
    public static final String PRODUCT_MATERIAL_TYPE_ZQUI = 'ZQUI';
    public static final String PRODUCT_OFFERING_GROUP_CODE = 'GPRDSX';
    public static final String LIG_ACTUAL_PROBABILITY = '<10';
    
    public static final String UNIT_NAME_FIELD = 'unit_name__c';
    public static final String INTERFACEDWITHMDM_TRUE = 'InterfacedWithMDM__c = true ';
    public static final String GLOBAL_REGION = 'Global';
    
    // Account Approval Status
    public static final String WORKITEM_PROCESSINSTANCE_TARGETOBJECTID_FIELD_API = 'ProcessInstance.TargetObjectId';
    public static final String MDM_VALIDATION_STATUS_APPROVE = 'Approve';
    public static final String MDM_VALIDATION_COMMENT_APPROVE = 'MDM validation completed';
    public static final String MDM_VALIDATION_STATUS_REJECT = 'Reject';
    public static final String MDM_VALIDATION_COMMENT_REJECT = 'MDM rejected';
    public static final String MDM_VALIDATION_STATUS_VALIDATED = 'Validated';
    public static final String MDM_UNVALIDATION_STATUS_VALIDATED = 'Unvalidated';
    public static final String MDM_VALIDATION_STATUS_REJECTED = 'Rejected';    
    public static final String OPPORTUNITYLINEITEMSCHEDULE = 'OpportunityLineItemSchedules';
    public static final String SCHEDULE_ORDERING = 'ScheduleDate, Description';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_TYPE = 'Revenue';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_DIVIDE_SCHEDULE = 'Divide Amount into multiple installments';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_REPEAT_SCHEDULE = 'Repeat Amount for each installment';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_DAILY = 'Daily';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_WEEKLY = 'Weekly';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_MONTHLY = 'Monthly';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_QUARTERLY = 'Quarterly';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_YEARLY = 'Yearly';
    public static final String OPPORTUNITYLINEITEMSCHEDULE_SEMESTERLY = 'Semesterly';
    public static final String SRV_CRM_OPPORTUNITYLINEITEMSCHEDULE_EXCEPTION = 'SRV_CRM_OpportunityLineItemSchedule.OpportunityLineItemScheduleServiceException';
    public static final String VALIDATED_ACCOUNT_RECORD_TYPE_NAME = 'MDM_Validated_Account';
    public static final String UNVALIDATED_ACCOUNT_RECORD_TYPE_NAME = 'Unvalidated_Account';
    public static final String MARKETING_TO_NURTURE = 'Marketing to Nurture';
    public static final String THREE_MONTHS = '3 months';
    public static final String SIX_MONTHS = '6 months';
    public static final String NINE_MONTHS = '9 months';
    public static final String TWELVE_MONTHS = '12 months';
    public static final String FIFTEEN_MONTHS = '15 months';
    public static final String EIGHTEEN_MONTHS = '18 months';
    
    //Mulesoft Integration Fields
    public static final String MULESOFT_SYNC_STATUS_COMPLETED = 'Completed';
    public static final String MULESOFT_SYNC_STATUS_PENDING = 'Pending';
    public static final String MULESOFT_SYNC_STATUS_FAILED = 'Failed';
    public static final String UPDATE_TYPE_MULESOFT_SLAVE = 'MULESOFT_SLAVE';
    public static final String UPDATE_TYPE_MULESOFT_TALKBACK = 'MULESOFT_TALKBACK';
    public static final String UPDATE_TYPE_USER = 'USER';
    public static final String EMPTY_STRING = '';   
    public static final String UPDATE_TYPE_MULESOFT_SLAVE_LI = 'MULESOFT_SLAVE_LI';
    public static final String UPDATE_TYPE_MULESOFT_SLAVE_LQ = 'MULESOFT_SLAVE_LQ';
    public static final String UPDATE_TYPE_MULESOFT_TALKBACK_LI = 'MULESOFT_TALKBACK_LI';
    public static final String UPDATE_TYPE_MULESOFT_TALKBACK_LQ = 'MULESOFT_TALKBACK_LQ';
    public static Boolean MULESOFT_OPP_VALIDATION_TRIGGER_HAS_RUN = false;
    public static Boolean MULESOFT_OLI_VALIDATION_TRIGGER_HAS_RUN = false;
    public static Boolean MULESOFT_OPP_SYNC_TRIGGER_HAS_RUN = false;
    public static Boolean MULESOFT_OLI_SYNC_TRIGGER_HAS_RUN = false;
    public static String TEMP_UPDATE_TYPE_MULESOFT_SLAVE = 'USER';
    public static String TEMP_UPDATE_TYPE_MULESOFT_SLAVE_OLI = 'USER';
    
    //Proxy BNF status
    public static final String SAP_SD_INTEGRATED = 'SAP SD Integrated';
    public static final String MIBNF = 'MIBNF';
    public static final String STATUS_NEW = 'New';
    public static final String REJECTED = 'Rejected';
    public static final String SUBMITTED = 'Submitted';
    public static final String RA_REJECTED = 'RA Rejected';
    public static final String SAP_CONTRACT_CONFIRMED = 'SAP Contract Confirmed';
    public static final String LQ_REJECTEDQ = 'LO Rejected';
    public static final String OPPORTUNITY_API = 'Opportunity__c';
    public static final String RECORD_TYPE_NAME_API = 'Record_Type_Text__c';
    public static final String AGREEMENT_RECORD_TYPE_NAME_API = 'Record_Type_Name__c';
    public static final String BNF_STATUS_API = 'BNF_Status__c';
    public static final String PA_STATUS_APPROVED = 'Approved';
    
    //Proxy SCM Agreement record type and status
    public static final String ACTIVATED = 'Activated';
    public static final String SOW_AGREEMENT_RECORD_TYPE = 'SOW';
    public static final String AGREEMENT_STATUS_API = 'Apttus_Status_c__c';
    
    public static final String UPDATE_TYPE_MULESOFT_FORCE_UPDATE_LI = 'MULESOFT_FORCE_UPDATE_LI';    
    
    //LIG Probability
    public static final String VERBAL = 'Verbal';
    public static final String CONFIDENT = 'Confident';
    public static final String POTENTIAL = 'Potential';
    public static final String ADVANCE_TO_POTENTIAL = 'Advance to Potential';
    public static final Integer PERCENTAGE_VERBAL = 100;
    public static final Integer PERCENTAGE_CONFIDENT = 90;
    public static final Integer PERCENTAGE_POTENTIAL = 50;
    public static final Integer PERCENTAGE_ADVANCE_TO_POTENTIAL = 25;
    
    public static final String ON_TRACK = 'On Track';
    public static final String BEST_CASE = 'Best Case';
    public static final String CLOSED_FORECAST_CATEGORY = 'Closed';
    public static final String OMITTED_FORECAST_CATEGORY = 'Omitted';
    public static final String PIPELINE_FORECAST_CATEGORY = 'Pipeline';
    
    public static Integer OLI_NEW_LIST_OLD_COUNT = 0;
    public static Integer OPP_NEW_LIST_OLD_COUNT = 0;
    public static Id GLOBAL_CUSTOMER_ACCOUNT_RECORD_TYPE_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Global Customer Account').getRecordTypeId();
    public static Id MQL_TASK_RECORD_TYPE_ID = Schema.SObjectType.Task.getRecordTypeInfosByName().get('MQL Task').getRecordTypeId();
    public static Id MDM_VALIDATED_ACCOUNT_RECORD_TYPE_ID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MDM Validated Account').getRecordTypeId();
    public static Boolean IGNORE_PA_VALIDATION = false;
    public static Id IQVIA_USER_CONTACT_RECORD_TYPE_ID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('IQVIA User Contact').getRecordTypeId();
    public static final String USER_CONTACT_SYNC = 'User_Contact_Sync__c';
    public static final String RECORD_ACTIVE = 'Record_Active__c';
    public static final String UNDERSCORE_C = '__c';
    public static final String BOTH = 'Both';
    public static final String USER_TO_CONTACT = 'User To Contact';
    public static final String CONTACT_TO_USER = 'Contact To User';
    public static final String COMMA = ',';
    public static final String SALESFORCE_USER = 'Salesforce_User__c';
    public static final String RECORD_TYPE_ID = 'RecordTypeId';
    public static Boolean preventContactUpdate = false;
    
    public static final String SALESFORCE_LICENSE = 'Salesforce';
    public static final String SALESFORCE_PLATFORM_LICENSE = 'Salesforce Platform';
    public static final String CHATTER_FREE_LICENSE = 'Chatter Free';
    
    public static Boolean IS_CURRENCY_CHANGE_FLOW = false;
    public static Boolean IS_CLONE_OPPORTUNITY_FLOW = false;
    public static final String FAVOURITE_PRODUCT_LIMIT_ERROR = 'You can favourite products up to ' ;
    public static final Id GLOBAL_ACCOUNT_RECORD_TYPE_ID = '0126A000000hDGm';
    
    //Contract record type and status
    public static String CONTRACT_RECORD_TYPE_NAME_CHANGE_ORDER_GBO = 'Change Order - GBO';
    public static String CONTRACT_RECORD_TYPE_NAME_WORK_ORDER_GBO = 'Work Order - GBO';
    public static String CONTRACT_RECORD_TYPE_NAME_THIRD_PARTY_AGREEMENT = 'Third Party Agreement';
    public static String CONTRACT_RECORD_TYPE_NAME_CHANGE_ORDER = 'Change Order';
    public static String CONTRACT_RECORD_TYPE_NAME_WORK_ORDER_SALES_MEDICAL = 'Work Order - Contract Sales & Medical Solutions';
    public static Id CONTRACT_RECORD_TYPE_CNF_GBO  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('CNF - GBO').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_MASTER_SERVICE_AGREEMENT  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Master Service Agreement').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_CHANGE_ORDER_GBO  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Change Order - GBO').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_GENERAL_CLIENT_AGREEMENT_GBO  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('General Client Agreement - GBO').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_PRELIMINARY_AGREEMENT_GBO  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Preliminary Agreement - GBO').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_WORK_ORDER_GBO  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Work Order - GBO').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_THIRD_PARTY_AGREEMENT  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Third Party Agreement').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_PRELIMINARY_AGREEMENT_SALES_MEDICAL  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Preliminary Agreement - Contract Sales & Medical Solutions').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_WORK_ORDER_SALES_MEDICAL  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Work Order - Contract Sales & Medical Solutions').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_CHANGE_ORDER  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Change Order').getRecordTypeId();
    public static Id CONTRACT_RECORD_TYPE_GENERIC_CONTRACT  = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Generic Contract').getRecordTypeId();
    public static Id BNF_RECORD_TYPE_SAP_INTEGRATED  = Schema.SObjectType.BNF2__c.getRecordTypeInfosByName().get('SAP SD Integrated').getRecordTypeId();
    public static String CONTRACT_STATUS_NEGOTIATION_TERMINATED = 'Negotiation Terminated';   
    public static String CONTRACT_STATUS_CONTRACT_AMENDED = 'Contract Amended';
    public static String CONTRACT_STATUS_CONTRACT_EXECUTED = 'Contract Executed';  
    public static String CONTRACT_STATUS_EXECUTED = 'Executed';    
    public static String CONTRACT_STATUS_BUDGET_AT_CUSTOMER_FOR_REVIEW = 'Budget at Customer for Review';  
    public static String CONTRACT_STATUS_BUDGET_WITH_ANALYST_FOR_CUSTOMER_EDITS = 'Budget with Analyst for Customer Edits';
    public static String CONTRACT_STATUS_CONTRACT_AT_CUSTOMER_FOR_REVIEW = 'Contract at Customer for Review';
    public static String CONTRACT_STATUS_CONTRACT_BEING_PREPARED_BY_CA = 'Contract Being Prepared by CA';
    public static String CONTRACT_STATUS_CONTRACT_AWAITING_INTERNAL_TEAM_REVIEW = 'Contract Awaiting Internal Team Review';
    public static String CONTRACT_STATUS_CONTRACT_WITH_ANALYST_FOR_OPERATIONAL_EDITS ='Contract with Analyst for Operational Edits';
    public static String CONTRACT_STATUS_CONTRACT_AWAITING_INTERNAL_APPROVAL ='Contract Awaiting Internal Approval';
    public static String CONTRACT_STATUS_CONTRACT_ANALYST_PREPARING_CUSTOMER_CONTRACT_DELIVERABLE ='Analyst Preparing Customer Contract Deliverable';
    public static String CONTRACT_STATUS_CONTRACT_WITH_ANALYST_FOR_CUSTOMER_EDITS ='Contract with Analyst for Customer Edits';
    public static String CONTRACT_STATUS_READY_TO_EXECUTE ='Ready to Execute';
    public static String CONTRACT_STATUS_ASSIGNED_NOT_STARTED ='Assigned - Not Started';
    public static String CONTRACT_STATUS_PENDING_ASSIGNMENT ='Pending Assignment';
    public static String CONTRACT_IS_A_BALLPARK_NO ='No';
    public static String CONTRACT_IS_A_BALLPARK_YES ='Yes';
    public static String CONTRACT_CONFIDENCE_IN_APPROVAL_OF_BUDGET_DRAFT = 'High';
    public static Decimal CONTRACT_ACTUAL_CONTRACT_VALUE = 1.00;
    public static String CONTRACT_LEAD_BUSINESS_GROUP_PRODUCT_DEVELOPMENT_INITIATIVES = 'Product Development Initiatives';
    public static String CONTRACT_LEAD_BUSINESS_GROUP_CONTRACT_RESEARCH_ORGANIZATION = 'Contract Research Organization';
    public static String CONTRACT_LEAD_BUSINESS_GROUP_EARLY_CLINICAL_DEVELOPMENT = 'Early Clinical Development';
    public static String CONTRACT_LEAD_BUSINESS_GROUP_GLOBAL_LABS = 'Global Labs';
    public static String CONTRACT_LEAD_BUSINESS_GROUP_CONSULTING = 'Consulting';
    public static String CONTRACT_STATUS_START_PREPARING_BUDGET = 'Start Preparing Budget';
    public static String CONTRACT_DIVISION_BUSINESS_UNIT = 'Outcome';
    public static String CONTRACT_OWNER_ROLE = 'OGC:Clinical:Contract:Negotiations';
    public static String CONTRACT_BALLPARK = 'Ballpark';
    public static String CONTRACT_GLOBAL_CONTRACT_TRIAGE_APP = 'Global Contract Triage App';
    public static String CONTRACT_GLOBAL_SALES_OPERATIONS = 'Global Sales Operations';
    public static Id AGREEMENT_RECORD_TYPE_PRELIM_AGREEMENT  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Prelim_Agreement').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_EARLY_ENGAGEMENT_BID  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Early_Engagement_Bid').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_POST_AWARD_BID  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Post_Award_Bid').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_NON_RFP_BID  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Non_RFP_Bid').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_RFI_REQUEST  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Request').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_RFI_SHORT_FORM  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Short_Form').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_FDTN_REBID  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('FDTN_Rebid').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_FDTN_CNF  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('FDTN_CNF').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_FDTN_CHANGE_ORDER  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('FDTN_Change_Order').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_FDTN_CONTRACT  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('FDTN_Contract').getRecordTypeId();
    public static Id AGREEMENT_RECORD_TYPE_FDTN_INITIAL_BID  = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('FDTN_Initial_Bid').getRecordTypeId();
    public static final String OPPORTUNITY_AGREEMENT_FIELD_API = 'Apttus__R00N50000001Xl0FEAS__r';
    public static final String AGR_ORDERING_DESC = 'CreatedDate DESC';
    public static String CONTRACT_STATUS_ACTIVATED = 'Activated';
    public static Id CONTACT_RECORD_TYPE_IQVIA_USER_CONTACT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('IQVIA User Contact').getRecordTypeId();
    static Map<String, RecordTypeInfo> agreementRecordTypeMap = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName();
    public static Set<Id> CRM_AGREEMENT_RECORD_TYPE_SET = new Set<Id> {
        agreementRecordTypeMap.get('CSS_Short_Form').getRecordTypeId(),
        agreementRecordTypeMap.get('CSS_Bid').getRecordTypeId(),
        agreementRecordTypeMap.get('Clinical_Short_Form').getRecordTypeId(),
        agreementRecordTypeMap.get('Clinical_Bid').getRecordTypeId(),
        agreementRecordTypeMap.get('Early_Engagement_Bid').getRecordTypeId(),
        agreementRecordTypeMap.get('PPA_Agreement').getRecordTypeId(),
        agreementRecordTypeMap.get('Post_Award_Bid').getRecordTypeId(),
        agreementRecordTypeMap.get('Prelim_Agreement').getRecordTypeId(),
        agreementRecordTypeMap.get('RFI_Short_Form').getRecordTypeId(),
        agreementRecordTypeMap.get('RFI_Request').getRecordTypeId()
    };
    
    public static Boolean updateAccountAddress = false;
    
    public static final String CONTRACT_OBJECT = 'Contract';
    public static final String CONTRACT_IN_OUT_LOG_OBJECT_API = 'Contract_In_Out_Log__c';
    public static final String CONTRACT_MILESTONE_OBJECT_API = 'Contract_Milestone__c';
    public static final String CONTRACT_LIFECYCLE_OBJECT_API = 'Contract_Lifecycle__c';
    public static final String CONTRACT_STAGE_DETAIL_OBJECT_API = 'Contract_Stage_Detail__c';
    public static final String GROUP_PLATFORM_ANALYTICS_SALES_GROUP = 'Platform Analytics Sales Group';
}