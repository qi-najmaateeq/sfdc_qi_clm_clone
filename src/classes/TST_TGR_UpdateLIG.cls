/**
* This test class is used to test all methods in updateLIG trigger.
* version : 1.0
*/
@isTest
private class TST_TGR_UpdateLIG {
    
    /**
    * This method is testing all methods on after insert
    */
    static testMethod void testUpdateLIGInsert() {
        Update_LIG__e ligEvent = new Update_LIG__e();
        ligEvent.Opp_Id__c = 'a10testOppId';
        ligEvent.Current_Opp_Stage__c = 'Test Current Stage';
        ligEvent.Expected_Opp_Stage__c = 'Test Expected Stage';
        
        Test.startTest();
        EventBus.publish(ligEvent);
        Test.stopTest();
    }
}