@isTest
public class TST_CNT_CPQ_XaeAppController {

    static Apttus__APTS_Agreement__c dataSetup(String appName, String pricingTool) {
    
        Apttus_XApps__Application__c app = UTL_TestData.createApplication();
        app.Name=appName;
        app.Apttus_XApps__UniqueId__c='1234';
        insert app;
        
        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;

        Opportunity testOpportunity= UTL_TestData.createOpportunity(testAccount.Id);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;
        
        Apttus__APTS_Agreement__c agreement = UTL_TestData.createAgreement();
        agreement.Select_Pricing_Tool__c = pricingTool;
        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId();
        agreement.Apttus__Related_Opportunity__c = testOpportunity.Id;
        agreement.RecordTypeId = recordTypeId;
        insert agreement;
        return agreement;
    }
    
    @isTest
    static void testGetQIPAppId() {
        	
        Apttus__APTS_Agreement__c agreement = dataSetup(CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_QIP_APP_NAME,
                                                        CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP);
        ApexPages.currentPage().getParameters().put(CON_CPQ.ID, agreement.Id);
        Test.startTest();
            String appId = new CNT_CPQ_XaeAppController().getAppId();
        Test.stopTest();
        
        System.assertNotEquals(null, appId, 'should return app Id');
        
    }
    
    @isTest
    static void testGetNOBIAppId() {
        
        Apttus__APTS_Agreement__c agreement = dataSetup(CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_NOBI_APP_NAME,CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_NOBI);
        ApexPages.currentPage().getParameters().put(CON_CPQ.ID, agreement.Id);
        Test.startTest();
            String appId = new CNT_CPQ_XaeAppController().getAppId();
        Test.stopTest();
        
        System.assertNotEquals(null, appId, 'should return app Id');
    }
    
    @isTest
    static void testGetUPTAppId() {
        
        Apttus__APTS_Agreement__c agreement = dataSetup(CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_UPT_APP_NAME,CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_UPT);
        ApexPages.currentPage().getParameters().put(CON_CPQ.ID, agreement.Id);
        Test.startTest();
            String appId = new CNT_CPQ_XaeAppController().getAppId();
        Test.stopTest();
        
        System.assertNotEquals(null, appId, 'should return app Id');
    }
}