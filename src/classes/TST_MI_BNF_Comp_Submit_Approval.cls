/**
* Test Class for MI_BNF_Comp_Submit_Approval  
 */
@isTest
private class TST_MI_BNF_Comp_Submit_Approval {
    
    static ApexPages.StandardController sc;
    static MIBNF2__c MIBNF;
    static MIBNF_Component__c MIBNF_Comp;
    static MI_BNF_LineItem__c MIBNFLineItem;
    static Opportunity opp;
    static OpportunityLineItem OLI1;
    static User TestUser1;
    static Account TestAccount;
    static List<Address__c> TestAddress_Array; 
    static BNF_Settings__c bnfsetting;
    
    @Testsetup
    static void testDataSetup() {
    	Current_Release_Version__c crv = new Current_Release_Version__c();
        crv.Current_Release__c = '3000.01';
        upsert crv;
        
    	Global_Variables.PCFlag = false;
        Global_Variables.RunQuickOppLimitTrigger = false;
        Global_Variables.RenewalCloneFlag = true;
        Global_Variables.syncAgreementToOppty=false;
        Global_Variables.PCTrigger_Hault_Execution=false;
        //OpportunityTriggerUtil.RunBeforeTrigger = false;
        //OpportunityTriggerUtil.RunAfterTrigger = false;
        Global_Variables.isupdatableOppPlan = false;
        Global_Variables.isclonningOpportunity = true;
        bnfsetting=new BNF_Settings__c(Enable_Material_Validation__c=false,Enable_Customer_Validation__c=false,BNF_Opportunity_Threshold__c = 1000, Enable_BNF_Surcharge__c=false);
        upsert bnfsetting;
        Profile p = [select id from profile where ( Name = 'SYSTEM ADMINISTRATOR') limit 1]; 
        TestUser1  = new User(alias = 'jpusr', email='japnalocal@metacube.com', 
                              emailencodingkey='UTF-8',firstname='User',lastname='Testing', languagelocalekey='en_US', 
                              localesidkey='en_US', profileid = p.Id, 
                              timezonesidkey='America/Los_Angeles', username='japnalocal@metacube.com');
        System.runAs(TestUser1) {     
            
        TestAccount = new Account();
        TestAccount.Name = 'Test Account';
        TestAccount.Status__c = MDM_Defines.AddressStatus_Map.get('SAP_VALIDATED');
        TestAccount.OwnerId = TestUser1.Id;
        insert TestAccount;
           
        List<User> userList = UTL_TestData.createUser(CON_CRM.SYSTEM_ADMIN_PROFILE, 2);
        userList.addall(UTL_TestData.createUser('Sales User', 1));
        insert userList;
        
        Contact cnt = UTL_TestData.createContact(TestAccount.Id);
        cnt.RecordTypeId = CON_CRM.IQVIA_USER_CONTACT_RECORD_TYPE_ID;
        cnt.Salesforce_User__c = userList[0].Id;
        insert cnt;
        
        insert new Profit_Centre__c(name='xz',ProfitCenterCode__c='USD' );
        Contact MainDecisionMakerContact1 = new Contact(FirstName = 'MainDecisionMaker', LastName='Contact', AccountId = TestAccount.Id, Email = 'abc@metacube.com', CurrencyIsoCode = 'USD');
        MainDecisionMakerContact1.pse__Is_Resource__c=true;
        MainDecisionMakerContact1.pse__Is_Resource_Active__c=true;
        insert MainDecisionMakerContact1;
        
        /*Principal_In_Charge__c PIC=new Principal_In_Charge__c(NAME=UserInfo.getLastName()+', '+UserInfo.getFirstName(), User__c=UserInfo.getUserId());
        PIC.PseResource__c=MainDecisionMakerContact1.id;
         
         insert PIC;*/
        
         // Create Opportunity
        Revenue_Analyst__c TestRA=new Revenue_Analyst__c(name='Test RA',User__c=TestUser1.Id);
        insert TestRA;
        
        opp =new Opportunity(Name='Quick opp',StageName='1. Identifying Opportunity',Amount=2000,CloseDate=System.today(),
        Contract_Term__c='Single-Period',Contract_Type__c='Individual');
        opp.LeadSource = 'Account Planning';
        opp.Budget_Available__c = 'Yes';
        opp.Contract_Start_Date__c = Date.today();
        opp.Contract_End_Date__c = Date.today();
          
        //opp.Unique_Business_Value__c = 'Unknown';
        //opp.Compelling_Event__c = 'No';
        //opp.Main_Decision_Maker__c=MainDecisionMakerContact1.Id;
        //opp.Budget_Holder__c=MainDecisionMakerContact1.Id;
        //opp.Principal_In_Charge__c=PIC.Id;
        //opp.Exclude_From_Pricing_Calculator__c = true;
        insert opp;
        
        OLI1 = new OpportunityLineItem();
        OLI1.OpportunityId = opp.Id;
        Product2 Product1 = new Product2(Name='test1', ProductCode='1', Enabled_Sales_Orgs__c='CH08', Business_Type__c = 'I&A', Material_Type__c = 'ZSET', isactive=true, CanUseRevenueSchedule = true, InterfacedWithMDM__c = true, Hierarchy_Level__c=CON_CRM.MATERIAL_LEVEL_HIERARCHY_OLI);
        insert product1;
        List<PricebookEntry> pbeList = [Select id from PricebookEntry where IsActive = true and CurrencyIsoCode ='USD' and Pricebook2Id = :Test.getStandardPricebookId()  Limit 1];
        PricebookEntry pbe;
        if(pbeList.size() == 0) {
            pbe = new PricebookEntry();
            pbe.UseStandardPrice = false;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.Product2Id=Product1.id;
            pbe.IsActive=true;
            pbe.UnitPrice=100.0;
            pbe.CurrencyIsoCode = 'USD';
            insert pbe;
        }
        else {
            pbe = pbeList[0];
        }
        //PricebookEntry PE1 = [select Id, CurrencyIsoCode from PricebookEntry where CurrencyIsoCode = 'USD' and IsActive = true and Product2.IsActive = true and Product2.Business_Type__c = 'I&A' limit 1][0];
        OLI1.PricebookEntryId = Pbe.Id;
        OLI1.Quantity = 1.00;
        OLI1.TotalPrice=10000;
        OLI1.List_Price__c = 100;
            OLI1.Sale_Type__c = 'New';
            OLI1.Delivery_Country__c = 'USA';
            OLI1.Revenue_Type__c = 'Ad Hoc';
        insert OLI1;
        
            OpportunityContactRole contactRole = UTL_TestData.createOpportunityContactRole(cnt.Id, opp.Id);
            insert contactRole;
            
        Opportunity opp1 =new Opportunity(id=opp.Id);
        opp1.StageName='7a. Closed Won';
        //opp1.Win_Loss_Reason__c='Win - Competitive Situation';
        //opp1.Win_Additional_Details__c = 'Additional details';
        //opp1.Win_Loss_Reason_Details__c = 'Win Loss Reason Details'; 
        opp1.Primary_Win_Reason__c ='Project Performance';
        opp1.Win_Type__c = 'Non-competitive bid';
        update opp1;
        
        TestAddress_Array = new List<Address__c>();
        for (Integer i=0; i<10; i++)
        {
            Address__c TempAddress = New Address__c(Name=String.valueOf(i),
                                                    Account__c=TestAccount.Id,
                                                    Street__c = 'Street ' + i.format(),
                                                    City__c = 'City '+ i.format(),
                                                    Country__c = 'Country ' + i.format(),
                                                    SAP_Reference__c = String.valueOf(495000+i));
            TestAddress_Array.add(TempAddress);
        } 
        insert TestAddress_Array;
        
        
        MIBNF=new MIBNF2__c();
        MIBNF.Client__c=opp.AccountId;
        MIBNF.Opportunity__c=opp.Id;
        MIBNF.Sales_Org_Code__c='CH08';
        MIBNF.Billing_Currency__c='USD';
        MIBNF.IMS_Sales_Org__c='Acceletra';
        MIBNF.Fair_Value_Type__c='Stand Alone';
        MIBNF.Invoice_Default_Day__c='15';
        MIBNF.Contract_Start_Date__c=system.today();
        MIBNF.Contract_End_Date__c=system.today();
        MIBNF.Contract_Type__c='Individual';
        MIBNF.Contract_Term__c='Single-Period';
        MIBNF.Payment_Terms__c='0000-Default Payment Terms of Customer Master Data';
        MIBNF.Revenue_Analyst__c=TestRA.id;
        
        insert MIBNF;
            
        MIBNF_Comp=new MIBNF_Component__c();
        MIBNF_Comp.MIBNF__c=MIBNF.Id;
        MIBNF_Comp.BNF_Status__c='New';
        MIBNF_Comp.Is_this_a_retainer_downpayment__c='No';
        MIBNF_Comp.Print_Shop__c='No';
        MIBNF_Comp.Bill_To__c=TestAddress_Array[0].Id;
        MIBNF_Comp.Ship_To__c=TestAddress_Array[0].id;
        //MIBNF_Comp.Comp_Revenue_Analyst__c = TestRA.id; 
        insert MIBNF_Comp;
        MI_BNF_LineItem__c MI_BNFLineItem= new MI_BNF_LineItem__c();
        MI_BNFLineItem.MIBNF_Component__c=MIBNF_Comp.id;
        MI_BNFLineItem.Opportunity_Line_Itemid__c=OLI1.Id;
        MI_BNFLineItem.Total_Price__c=OLI1.TotalPrice;
        insert MI_BNFLineItem;
      }  
   }
   
   
     static testMethod void CancelTest() {
        
       Test.startTest();
       MIBNF_Component__c MIBNF_Comp1 = [Select id from MIBNF_Component__c limit 1];
       ApexPages.CurrentPage().getParameters().put('id' ,MIBNF_Comp1.id);
       PageReference pageRef = ApexPages.currentPage();
       Test.setCurrentPageReference(pageRef);
       MI_BNF_Comp_Submit_Approval controller = new MI_BNF_Comp_Submit_Approval();
       PageReference pg=controller.cancel();
       Test.stopTest();
    }
    
     static testMethod void SubmitAvailableProductTest() {
       Test.startTest();
       MIBNF_Component__c MIBNF_Comp1 = [Select id from MIBNF_Component__c limit 1];
       ApexPages.CurrentPage().getParameters().put('id' ,MIBNF_Comp1.id);
       PageReference pageRef = ApexPages.currentPage();
       Test.setCurrentPageReference(pageRef);
       MI_BNF_Comp_Submit_Approval controller = new MI_BNF_Comp_Submit_Approval();
       PageReference pg=controller.SubmitRequest();
       Test.stopTest();
    }

   /* public static MIBNF2__c MIBNF;
    public static MIBNF_Component__c MIBNF_Comp;
    public static MI_BNF_LineItem__c MIBNFLineItem;
    public static Opportunity opp;
    public static OpportunityLineItem OLI1;
    private static List<Address__c> TestAddress_Array;
    private static User TestUser;
    private static User TestUser1;
    private static Revenue_Analyst__c TestRA;
    private static Account TestAccount;
    private static Contact MainDecisionMakerContact1;

    
    //Cancel Button Test
    static testMethod void CancelTest() {
     
       SetupMIBNF();
       SetupMIBNF_Comp(MIBNF);
       ApexPages.CurrentPage().getParameters().put('id' ,MIBNF_Comp.id);
     
       PageReference pageRef = ApexPages.currentPage();
       Test.setCurrentPageReference(pageRef);
       MI_BNF_Comp_Submit_Approval controller = new MI_BNF_Comp_Submit_Approval();
       
       PageReference pg=controller.cancel();
       System.assertNotEquals(pg,null);
       Test.stopTest();
    }
   
    static testMethod void SubmitAvailableProductTest() {
     
       SetupMIBNF();
       SetupMIBNF_Comp(MIBNF);
       ApexPages.CurrentPage().getParameters().put('id' ,MIBNF_Comp.id);
       
       PageReference pageRef = ApexPages.currentPage();
       Test.setCurrentPageReference(pageRef);
       MI_BNF_Comp_Submit_Approval controller = new MI_BNF_Comp_Submit_Approval();
       
       PageReference pg=controller.SubmitRequest();
     
       ApexPages.Message msg1 = ApexPages.getMessages()[0];
       System.assertEquals('BNF cannot be submitted as there are available products which have not been allocated to an invoice', msg1.getDetail());
       System.assertEquals(pg,null);
       Test.stopTest();
    }
    
    static testMethod void SubmitRequestTest() {
     
       SetupMIBNF();
       SetupMIBNF_Comp(MIBNF);
       MI_BNF_LineItem__c MI_BNFLineItem= new MI_BNF_LineItem__c();
       MI_BNFLineItem.MIBNF_Component__c=MIBNF_Comp.id;
       MI_BNFLineItem.Opportunity_Line_Itemid__c=OLI1.Id;
       MI_BNFLineItem.Total_Price__c=OLI1.TotalPrice;
       insert MI_BNFLineItem;
       
       ApexPages.CurrentPage().getParameters().put('id' ,MIBNF_Comp.id);
       
       PageReference pageRef = ApexPages.currentPage();
       Test.setCurrentPageReference(pageRef);
       MI_BNF_Comp_Submit_Approval controller = new MI_BNF_Comp_Submit_Approval();
       
       PageReference pg=controller.SubmitRequest();
       System.assertNotEquals(pg,null);
        Test.stopTest();
    }
    
    //Setup MIBNF
    static void SetupMIBNF()
    {
         TestUser = [select Id from User where IsActive = true and (not Profile.Name like '%administrator%') and (not Profile.Name like '%operations%') and (not Profile.Name like '%Chatter%') limit 1];
        TestUser1 = [select Id from User where IsActive = true and (Profile.Name like '%administrator%') limit 1];
        
        TestAccount = new Account();
        TestAccount.Name = 'Test Account';
        TestAccount.Status__c = MDM_Defines.AddressStatus_Map.get('SAP_VALIDATED');
        TestAccount.OwnerId = TestUser1.Id;
        insert TestAccount;
        
        MainDecisionMakerContact1 = new Contact(FirstName = 'MainDecisionMaker', LastName='Contact', AccountId = TestAccount.Id, Email = 'abc@xyz.com', CurrencyIsoCode = 'USD');
        MainDecisionMakerContact1.pse__Is_Resource__c=true;
        MainDecisionMakerContact1.pse__Is_Resource_Active__c=true;
        insert MainDecisionMakerContact1;
        
        //Principal_In_Charge__c PIC=new Principal_In_Charge__c(NAME='Salecha, Anjali',User__c=UserInfo.getUserId());
        //Principal_In_Charge__c PIC=new Principal_In_Charge__c(NAME=UserInfo.getLastName()+', '+UserInfo.getFirstName(),User__c=UserInfo.getUserId());
        //PIC.PseResource__c=MainDecisionMakerContact1.id;
         
        // insert PIC;
        
         // Create Opportunity
        TestRA=new Revenue_Analyst__c(name='Test RA',Is_SAP_Revenue_Analyst__c=false,User__c=TestUser1.Id);
        insert TestRA;
        
        opp =new Opportunity(Name='Quick opp',StageName='1. Identifying Opportunity',Amount=2000,CloseDate=System.today(),
        Contract_Term__c='Single-Period',Contract_Type__c='Individual');
        opp.LeadSource = 'Account Planning';
        opp.Budget_Available__c = 'Yes';
        //opp.Unique_Business_Value__c = 'Unknown';
        //opp.Compelling_Event__c = 'No';
        //opp.Main_Decision_Maker__c=MainDecisionMakerContact1.Id;
        //opp.Budget_Holder__c=MainDecisionMakerContact1.Id;
        //opp.Principal_In_Charge__c=PIC.Id;
        
        insert opp;
        AddLineItems(opp);
        
        Opportunity opp1 =new Opportunity(id=opp.Id);
        opp1.StageName='7a. Closed Won';
        //opp1.Win_Loss_Reason__c='Win - Competitive Situation';
        //opp1.Win_Additional_Details__c = 'Additional details';
        //opp1.Win_Loss_Reason_Details__c = 'Win Loss Reason Details'; 
       
        update opp1;
        
        TestAddress_Array = new List<Address__c>();
        for (Integer i=0; i<10; i++)
        {
            Address__c TempAddress = New Address__c(Name=String.valueOf(i),
                                                    Account__c=TestAccount.Id,
                                                    Street__c = 'Street ' + i.format(),
                                                    City__c = 'City '+ i.format(),
                                                    Country__c = 'Country ' + i.format(),
                                                    SAP_Reference__c = String.valueOf(495000+i));
            TestAddress_Array.add(TempAddress);
        } 
        insert TestAddress_Array;
        
        
        MIBNF=new MIBNF2__c();
        MIBNF.Client__c=opp.AccountId;
        MIBNF.Opportunity__c=opp.Id;
        MIBNF.Sales_Org_Code__c='CH08';
        MIBNF.Billing_Currency__c='USD';
        MIBNF.IMS_Sales_Org__c='Acceletra';
        MIBNF.Fair_Value_Type__c='Stand Alone';
        MIBNF.Invoice_Default_Day__c='15';
        MIBNF.Contract_Start_Date__c=system.today();
        MIBNF.Contract_End_Date__c=system.today();
        MIBNF.Contract_Type__c='Individual';
        MIBNF.Contract_Term__c='Single-Period';
        MIBNF.Payment_Terms__c='0000-Default Payment Terms of Customer Master Data';
        MIBNF.Revenue_Analyst__c=TestRA.id;
         Test.startTest();
        insert MIBNF;
    }
    
     //setting up component for MIBNF
    static void SetupMIBNF_Comp(MIBNF2__c RefMIBNF)
    {
        MIBNF_Comp=new MIBNF_Component__c();
        MIBNF_Comp.MIBNF__c=RefMIBNF.Id;
        MIBNF_Comp.BNF_Status__c='New';
        MIBNF_Comp.Is_this_a_retainer_downpayment__c='No';
        MIBNF_Comp.Print_Shop__c='No';
        MIBNF_Comp.Bill_To__c=TestAddress_Array[0].Id;
        MIBNF_Comp.Ship_To__c=TestAddress_Array[0].id;
        
        insert MIBNF_Comp;
    }
    
     // AddLineItem to Opportunity
    static void AddLineItems(Opportunity O)
    {
        //  Add a line item to the opportunity
       
        OLI1 = new OpportunityLineItem();
        OLI1.OpportunityId = O.Id;
        PricebookEntry PE1 = [select Id, CurrencyIsoCode,Product2Id from PricebookEntry where CurrencyIsoCode = 'USD' and IsActive = true and Product2.IsActive = true and Product2.Business_Type__c = 'I&A' limit 1][0];
        Product2 p1=[Select Enabled_Sales_Orgs__c from Product2 where Id =:PE1.Product2Id limit 1][0];
        p1.Enabled_Sales_Orgs__c='CH08';
        upsert p1;
        OLI1.PricebookEntryId = PE1.Id;
        OLI1.Quantity = 1.00;
        OLI1.TotalPrice=10000;
        OLI1.List_Price__c=1000;
        insert OLI1;
 
       
    }
   */
}