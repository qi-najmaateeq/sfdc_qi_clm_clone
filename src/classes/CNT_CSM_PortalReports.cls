/*
 * Version       : 1.0
 * Description   : Apex Controller for PortalReports component.
 */
public with sharing class CNT_CSM_PortalReports {
    /*
     * Return List of Contact for current user
     */
    @AuraEnabled
    public static List<Contact> getUserContact(){ 
        List<User> users = new List<User>();
        users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        List<Contact> contacts=new List<Contact>();
        contacts = new SLT_Contact().selectByContactIdList(new Set<Id> {users[0].ContactId}, new Set<String>{'Portal_Case_Type__c','AccountId'});
        return contacts;
    }
    /**
     * This method used to get  Dashboards by ids
     * @param dashboardIds
     * @return  List<Dashboard>
     */
    @AuraEnabled
    public static List<Dashboard> getDashboards(List<String> dashboardIds){ 
        List<Dashboard> dashboards = new List<Dashboard>();
        dashboards = new SLT_Dashboard().selectById(dashboardIds);
        return dashboards;
    }
     /**
     * This method used to get  Report by ids
     * @param reportIds
     * @return  List<Report>
     */
    @AuraEnabled
    public static List<Report> getReports(List<String> reportIds){ 
        List<Report> reports = new List<Report>();
        reports = new SLT_Report().selectById(reportIds);
        return reports;
    }
    
    /**
     * This method used to get  private Report
     * @return  List<Report>
     */
    @AuraEnabled
    public static List<Report> getPrivateReports(){ 
        List<Report> reports = new List<Report>();
        reports = new SLT_Report().selectPrivateReports(userInfo.getUserId());
        return reports;
    }
}