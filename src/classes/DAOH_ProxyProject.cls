/**
 * This is Proxy_Project__c trigger handler class.
 * version : 1.0
 */
public class DAOH_ProxyProject {
    
    /**
     * This method is used to set the QI OLI Id field  on the Proxy object records
     * @params  newList List<Proxy_Project__c>
     * @params  oldMap Map<Id, Proxy_Project__c>
     * @return  void
     */
    public static void setQIOLIId(List<Proxy_Project__c> newList, Map<Id, Proxy_Project__c> oldMap) {
        Map<Id,Proxy_Project__c> liIdWithProxyProjMap = new Map<Id,Proxy_Project__c>();
        for(Proxy_Project__c proj : newList) {
            if(proj.LI_OpportunityLineItem_Id__c != null && (Trigger.isInsert || (Trigger.isUpdate && proj.LI_OpportunityLineItem_Id__c != oldMap.get(proj.id).LI_OpportunityLineItem_Id__c))) {
                liIdWithProxyProjMap.put(proj.LI_OpportunityLineItem_Id__c, proj);
            }
        }
        
        if(liIdWithProxyProjMap.size() > 0) {
            Set<Id> idSet = new Set<Id>();
            idSet.addAll(liIdWithProxyProjMap.keySet());
            Set<String> fieldSet = new Set<String>{'Id', 'LI_OpportunityLineItem_Id__c'};
            Map<Id, OpportunityLineItem> oliMap = new SLT_OpportunityLineItems().selectByLIOLIId(idSet, fieldSet);
            for(OpportunityLineItem oli : oliMap.values()) {
                if(liIdWithProxyProjMap.containsKey(oli.LI_OpportunityLineItem_Id__c)) {
                    liIdWithProxyProjMap.get(oli.LI_OpportunityLineItem_Id__c).QI_OpportunityLineItem_Id__c = oli.id;
                }
            }
        }
    }
    /**
     * This method is used to set the set OLI Date field on the OLI object records
     * @params  newList List<Proxy_Project__c>
     * @params  oldMap Map<Id, Proxy_Project__c>
     * @return  void
     */
    public static void setOLIDates(List<Proxy_Project__c> newList, Map<Id, Proxy_Project__c> oldMap) {
        List<OpportunityLineItem> oliToUpdate = new List<OpportunityLineItem>();
        for(Proxy_Project__c proxyProject : newList){
            if(Trigger.isUpdate && !(String.isBlank(proxyProject.QI_OpportunityLineItem_Id__c)) && (proxyProject.Project_Start_Date__c != oldMap.get(proxyProject.Id).Project_Start_Date__c || proxyProject.Project_End_Date__c != oldMap.get(proxyProject.Id).Project_End_Date__c)){
                OpportunityLineItem oli = new OpportunityLineItem();
                oli.Id = proxyProject.QI_OpportunityLineItem_Id__c;
                oli.Project_Start_Date__c = proxyProject.Project_Start_Date__c;
                oli.Project_End_Date__c = proxyProject.Project_End_Date__c;
                oliToUpdate.add(oli);
            }
        }
        if(oliToUpdate.size() > 0){
            UTL_ExecutionControl.stopTriggerExecution = true;
            update oliToUpdate;
            UTL_ExecutionControl.stopTriggerExecution = false;
        }
    }	
	
    /**
     * This method is used to set the Master Project field  on the Proxy object records
     * @params  newList List<Proxy_Project__c>
     * @params  oldMap Map<Id, Proxy_Project__c>
     * @return  void
     */
    public static void updateProjectFieldsFromContact(List<Proxy_Project__c> listProxyProject) {
        Set<Id> setOfMasterProjectId = new Set<Id>();
        for(Proxy_Project__c project : listProxyProject) {
            if(String.isEmpty(project.Master_Project__c) && !String.isEmpty(project.Li_Master_Project_Id__c))
                setOfMasterProjectId.add(project.Li_Master_Project_Id__c);
        }
        List<Proxy_Project__c> listOfMasterProject = new SLT_Proxy_Project().selectByLIRecordId(setOfMasterProjectId, new Set<String>{'Id', 'LI_Record_Id__c'});
       
        Map<String, Proxy_Project__c> mapOfProjectAndId = new Map<String, Proxy_Project__c>();
        for(Proxy_Project__c project : listOfMasterProject) {
            mapOfProjectAndId.put(project.LI_Record_Id__c, project);
        }
        
        for(Proxy_Project__c project : listProxyProject) {
            if(mapOfProjectAndId.containsKey(project.Li_Master_Project_Id__c)) 
            	project.Master_Project__c = mapOfProjectAndId.get(project.Li_Master_Project_Id__c).Id;
        }
    }
}