/**
 * This is LineItemGroup trigger handler class.
 * version : 1.0
 */
public class DAOH_OWF_LineItemGroup {
    /**
     * This method is used to create Resource Requests on change of Phase__c field on LineItemGroup records
     * @params  newList List<Line_Item_Group__c>
     * @params  oldMap Map<Id, Line_Item_Group__c>
     * @return  void
     */
    public static void createClinicalBidResourceRequestsOnLineItemGroupUpdate(List<Line_Item_Group__c> newList, Map<Id, Line_Item_Group__c> oldMap) {
        Set<Id> lineItemGroupIdsSet = new set<Id>();
        for(Line_Item_Group__c lineItemGrp : newList) {
            if(lineItemGrp.Phase__c != NULL && lineItemGrp.Phase__c != oldMap.get(lineItemGrp.Id).Phase__c && lineItemGrp.Phase__c != CON_OWF.LINE_ITEM_GROUP_PHASE_4) {
                lineItemGroupIdsSet.add(lineItemGrp.Id);
            }
        }
        
        if(lineItemGroupIdsSet.size() > 0) {
            String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__r.Line_Item_Group__c IN :sObjectIdSet ' +
                                    ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
            
            List<pse__Resource_Request__c> reqRequestsInsertList = new List<pse__Resource_Request__c>();
            reqRequestsInsertList = UTL_OWF.processResRequestForInitialBids(lineItemGroupIdsSet, agrCondition, null, null);
            if(reqRequestsInsertList.size() > 0) {
                insert reqRequestsInsertList;
            }    
        }
    }
}