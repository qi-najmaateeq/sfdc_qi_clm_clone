/*
 * Version       : 1.0
 * Description   : This test class is used for Select OpportunityLineItems
 */
@isTest
public class TST_SLT_OpportunityLineItems {
    
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Contact cnt = UTL_TestData.createContact(acc.Id);
        insert cnt;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        insert opp;
        Product2 product = UTL_TestData.createProduct();
        insert product;
        PricebookEntry pbEntry = UTL_TestData.createPricebookEntry(product.Id);
        insert pbEntry;
        OpportunityLineItem oppLineItem = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
        insert oppLineItem;
        
    }
	
    /**
     * This method used to Select OpportunityLineItems by opportunity id
     */    
    @IsTest
    static void testSelectByOpportunityId() {
        Opportunity opp = [Select id from Opportunity where name = 'TestOpportunity'];
        Set<String> oliFieldSet = new Set<String> {'Id', 'OpportunityId', 'Hierarchy_Level__c', 'Offering_Group_Code__c'};
        Test.startTest();
		    Map<Id, OpportunityLineItem> oppLineItemMap = new SLT_OpportunityLineItems().selectByOpportunityId(new Set<Id> { opp.Id }, oliFieldSet);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppLineItemMap.size();
        System.assertEquals(expected, actual);
	}
    
    /**
     * This method used to Select OpportunityLineItems with schedules
     */    
    @IsTest
    static void testGetOliWithSchedules() {
        OpportunityLineItem oli = [SELECT id FROM OpportunityLineItem LIMIT 1];
        Set<String> oliFieldSet = new Set<String> {'Id', 'OpportunityId', 'Hierarchy_Level__c', 'Offering_Group_Code__c'};
        Set<String> schFieldSet = new Set<String> {'Id'};
        Test.startTest();
		    Map<Id, OpportunityLineItem> oppLineItemMap = new SLT_OpportunityLineItems().getOliWithSchedules(new Set<Id> { oli.Id }, oliFieldSet, schFieldSet);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppLineItemMap.size();
        System.assertEquals(expected, actual);
	}

    /**
     * This method used to Select OpportunityLineItems with schedules by Opportunity Id
     */    
    @IsTest
    static void testGetOlisWithSchedules() {
        Opportunity opp = [SELECT id FROM Opportunity WHERE name = 'TestOpportunity'];
        Set<String> oliFieldSet = new Set<String> {'Id', 'OpportunityId', 'Hierarchy_Level__c', 'Offering_Group_Code__c'};
        Set<String> schFieldSet = new Set<String> {'Id'};
        Test.startTest();
		    Map<Id, OpportunityLineItem> oppLineItemMap = new SLT_OpportunityLineItems().getOlisWithSchedules(new Set<Id> { opp.Id }, oliFieldSet, schFieldSet);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppLineItemMap.size();
        System.assertEquals(expected, actual);
	}

    /**
     * This method used to Select OpportunityLineItems with schedules by Opportunity Id
     */    
    @IsTest
    static void testSelectOLIByOpportunityId() {
        Opportunity opp = [SELECT id FROM Opportunity WHERE name = 'TestOpportunity'];
        
        Test.startTest();
		    List<OpportunityLineItem> oppLineItemList = new SLT_OpportunityLineItems().selectOLIByOpportunityId(new Set<Id> { opp.Id });
        Test.stopTest();
        Integer expected = 1;
        Integer actual = oppLineItemList.size();
        System.assertEquals(expected, actual);
	}

}