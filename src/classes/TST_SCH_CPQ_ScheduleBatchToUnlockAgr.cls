@isTest
public class TST_SCH_CPQ_ScheduleBatchToUnlockAgr {

    @isTest
    static void scheduleBatchToUnlockAgreementShouldSchedule(){

        CPQ_Budget_Unlock_Setting__c unlockSetting = UTL_TestData.createBudgetUnlockSetting();
        insert unlockSetting;
        
        Test.startTest();
            String CRON_EXP = '0 0 2 * * ?';
            String jobId = System.schedule('SCH_CPQ_ScheduleBatchToUnlockAgreement', 
                            CRON_EXP, new SCH_CPQ_ScheduleBatchToUnlockAgreement());	
        Test.stopTest();

        system.assertEquals(1, [SELECT Id FROM CronTrigger WHERE id = :jobId].size(), 'Batch is schedule');
    }
}