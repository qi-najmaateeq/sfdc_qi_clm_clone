@isTest
public class TST_CNT_CPQ_ContractRelatedList {
	
	static Account accountDataSetup() {
		Account testAccount = UTL_TestData.createAccount();
		insert testAccount;
		return testAccount;
	}

    static Apttus__APTS_Agreement__c agreementDataSetup(string contractNumber) {

        Account accountData = accountDataSetup();
        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountData.Id);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;
        
        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CONTRACT).getRecordTypeId();
        Apttus__APTS_Agreement__c agreement = UTL_TestData.createAgreement();
        agreement.Contract_Number__c = contractNumber;
        agreement.RecordTypeId = RecordTypeId;
        agreement.Apttus__Related_Opportunity__c = testOpportunity.Id;
        insert agreement;
        return agreement;
    }

    static Contract contractDataSetup() {
        Account accountData = accountDataSetup();
        Contract contract = new Contract(Name = 'Work Order');
        contract.AccountId=accountData.Id;
        contract.Parent_Contract_Number__c = 123;
        contract.Ultimate_Parent_Contract_Number__c = 345;
        insert contract;
        return contract;
    }
    
    static Contract getContract(Id contractId){
        return [SELECT Id, ContractNumber From Contract WHERE ID=:contractId];
    }

    @isTest
    static void testFetchContract() {
        Contract contractData = contractDataSetup();
        Contract contractQuery=getContract(contractData.Id);
        Apttus__APTS_Agreement__c agreement = agreementDataSetup(contractQuery.contractNumber);
        
        Test.startTest();      
            Contract contract = CNT_CPQ_ContractRelatedList.fetchContract(agreement.Id);
        Test.stopTest();

        System.assertNotEquals(null, contract, 'should return app Id');
    }
}