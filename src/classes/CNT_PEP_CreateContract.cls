/*
* Version       : 1.0
* Description   : Apex Controller to create contract.
*/
public class CNT_PEP_CreateContract {
    /*
    * return Contract Id
    */
    @AuraEnabled 
    public static String createContractController(String agencyProgramId){
        String contractId = '';
        
        //Check if existing Contract exists with same Agency and status 'Awaiting Signature'
        List<Contract> existingContractList = [SELECT Id, AccountId, Name, Status, ContractTerm, SOW_status__c, Payment_status__c
                                                    FROM Contract
                                                    WHERE Agency_Program__c =: agencyProgramId
                                                    AND SOW_status__c = 'Awaiting Signature'
                                                    LIMIT 1];
        
        if(existingContractList.size() > 0){
            contractId = existingContractList[0].Id;
        }else{
            //get the Name of the Agency Program
            Agency_Program__c relatedAgency = [SELECT Name, Product__c
                                               FROM Agency_Program__c 
                                               WHERE id =: agencyProgramId LIMIT 1];
            
            //get the AccountId of the actual Portal User
            List<User> usersList = [SELECT ContactId, AccountId 
                                    FROM User 
                                    WHERE id =: UserInfo.getUserId()];
            
            if(usersList.size() > 0){
                Id contractPRM_RT = Schema.SObjectType.Contract.getRecordTypeInfosByDeveloperName().get(CON_PEP.S_PEP_CTR_RECORDTYPE).getRecordTypeId();
                
                Contract cntrt = new Contract();
                cntrt.AccountId = usersList[0].AccountId;
                cntrt.Name = relatedAgency.Name;
                cntrt.Status = null;
                cntrt.ContractTerm = 12;
                cntrt.RecordTypeId = contractPRM_RT;
                cntrt.SOW_status__c = 'Awaiting Signature';
                cntrt.Product__c = relatedAgency.Product__c;
                cntrt.Agency_Program__c = agencyProgramId;
                cntrt.Payment_status__c = 'Payment Required';
                cntrt.PRM_Contract_type__c = 'Agency Program SOW';
                Database.insert(cntrt, true);
                
                contractId = cntrt.Id;  
            }
        }

        return contractId;
    }
    
    @AuraEnabled
    public static List<Agency_Program__c> getActiveAgencyProg(){
        string activeStatus = CON_PEP.S_PEP_ACTIVE_STATUS;
        return [SELECT Id, Name, Description__c, CurrencyIsoCode, Duration_in_month__c, Price__c, Product__c, Status__c FROM Agency_Program__c where status__c =: activeStatus ORDER BY Name ASC];
    }
}