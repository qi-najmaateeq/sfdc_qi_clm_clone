/**
* This is Resource_Skill_Request trigger handler class.
* version : 1.0
*/
public without sharing class DAOH_OWF_Resource_Skill_Request {
    /**
    * This method is used to create ResourceCandidateMatchScore records on insertion of Resource_Skill_Request
    * @params  newList List<pse__Resource_Skill_Request__c>
    * @return  List<ResourceCandidateMatchScore>
    */
    public static List<ResourceCandidateMatchScore> createResourceCandidateMatchScore(List<pse__Resource_Skill_Request__c> resourceSkillRequestList) {
        system.debug('***createResourceCandidateMatchScore->getCpuTime()***'+Limits.getCpuTime());
        List<ResourceCandidateMatchScore> resourceCandidateMatchScoreList = new List<ResourceCandidateMatchScore>();
        Set<Id> resourceRequestIdsSet = new Set<Id>();
        
        
        for(pse__Resource_Skill_Request__c rsr : resourceSkillRequestList) {
            if(rsr.pse__Resource_Request__c != null) {
                resourceRequestIdsSet.add(rsr.pse__Resource_Request__c);
            }
        }
        
        if(resourceRequestIdsSet.size() > 0) {
            resourceCandidateMatchScoreList = validateAndCreateCandidateMatchScores(resourceRequestIdsSet);
        }
        system.debug('***getCpuTime()***'+Limits.getCpuTime());
        return resourceCandidateMatchScoreList;
    }
    
    /**
    * This method is used to validate skill certifications and create ResourceCandidateMatchScore records
    * @params  resourceRequestIdsSet Set<Id>
    * @return  List<ResourceCandidateMatchScore>
    */
    private static List<ResourceCandidateMatchScore> validateAndCreateCandidateMatchScores(Set<Id> resourceRequestIdsSet) {
        List<ResourceCandidateMatchScore> resourceCandidateMatchScoreList = new List<ResourceCandidateMatchScore>();
        Map<Id, pse__Resource_Request__c> resourceRequestsMap = new Map<Id, pse__Resource_Request__c>();
        Map<Id, List<pse__Resource_Skill_Request__c>> skillToRSRsMap = new Map<Id, List<pse__Resource_Skill_Request__c>>();
        Map<Id, List<pse__Skill_Certification_Rating__c>> skillIdToSkillCertiRatingsMap = new Map<Id, List<pse__Skill_Certification_Rating__c>>();
        
        //Fetching Resource_Requests with all ResourceSkillRequests and preparing a map b/w resource_request and list of resource_skill_requests
        String resReqCondition = 'Id IN :sObjectIdSet And RecordType.DeveloperName = \'' + CON_OWF.OWF_RESOURCE_REQUEST_RECORD_TYPE_NAME + '\'';
        Set<String> resRequestFieldSet = new Set<String>{'Id', 'pse__Resource__c', 'Resource_Request_Type__c', 'SubGroup__c', 'Complexity_Score_Total__c', 'Agreement__r.RFP_Ranking__c'};
        Set<String> resSkillRequestFieldSet = new Set<String>{'Id', 'pse__Resource_Request__c', 'pse__Skill_Certification__c', 'pse__Skill_Certification__r.pse__Type__c','pse__Resource_Request__r.SubGroup__c'};
        String resSkillReqCondition =  'pse__Resource_Request__c IN : sObjectIdSet';
        resourceRequestsMap = new SLT_Resource_Request(false,false).selectByIdWithResReqsAndRSRs(resourceRequestIdsSet, resReqCondition, resRequestFieldSet, resSkillRequestFieldSet, resSkillReqCondition);
        system.debug('***validateAndCreateCandidateMatchScores:1->getCpuTime()***'+Limits.getCpuTime());    
        //Fetching all Skill_Certification_Ratings and preparing a map b/w resource and list of Skill_Certification_Ratings
        if(!resourceRequestsMap.IsEmpty()) {
            for(pse__Resource_Request__c resourceRequest : resourceRequestsMap.values()) {
                if(resourceRequest.pse__Resource_Skill_Requests__r.size() > 0) {
                    for(pse__Resource_Skill_Request__c rsr: resourceRequest.pse__Resource_Skill_Requests__r) {
                        if(rsr.pse__Skill_Certification__c != NULL) {
                            if(!skillToRSRsMap.containsKey(rsr.pse__Skill_Certification__c)) {
                                skillToRSRsMap.put(rsr.pse__Skill_Certification__c, new List<pse__Resource_Skill_Request__c>());
                            }
                            skillToRSRsMap.get(rsr.pse__Skill_Certification__c).add(rsr);
                        }
                    }
                }
            }
            system.debug('***validateAndCreateCandidateMatchScores:2->getCpuTime()***'+Limits.getCpuTime());
            String skillCertRatingCondition = ' pse__Skill_Certification__c IN :sObjectIdSet AND is_duplicate__c = false AND isPSEActiveResource__c = True AND pse__Resource__c != NULL '
                                                + ' AND pse__Resource__r.pse__Group__c != null '
                                                + ' AND pse__Resource__r.pse__Salesforce_User__r.IsActive = true '
                                                + ' AND (pse__Skill_Certification__r.pse__Type__c = \'' + CON_OWF.SKILL_TYPE_INDICATION + '\''
                                                + ' OR pse__Skill_Certification__r.pse__Type__c = \''  + CON_OWF.SKILL_TYPE_THERAPY_AREA + '\''
                                                + ' OR pse__Skill_Certification__r.pse__Type__c = \''  + CON_OWF.SKILL_TYPE_LINE_OF_BUSINESS + '\''
                                                + ' OR pse__Skill_Certification__r.pse__Type__c = \''  + CON_OWF.SKILL_TYPE_POTENTIAL_REGION + '\')';
            Set<String> skillCertRatingFieldSet = new Set<String>{'Id', 'pse__Resource__c',  'pse__Skill_Certification__c', 'pse__Rating__c', 'pse__Skill_Certification__r.pse__Type__c','pse__Resource__r.Sub_Group__c',
                                                  'pse__Resource__r.Available_For_Triage_Flag__c','pse__Resource__r.pse__Is_Resource__c','pse__Resource__r.pse__Is_Resource_Active__c'};
            for(pse__Skill_Certification_Rating__c scr : new SLT_Skill_Certification_Rating(false,false).getSkillCertificationRatingsBySkills(skillToRSRsMap.keySet(), skillCertRatingCondition, skillCertRatingFieldSet).values()) {
                if(!skillIdToSkillCertiRatingsMap.containsKey(scr.pse__Skill_Certification__c)) {
                    skillIdToSkillCertiRatingsMap.put(scr.pse__Skill_Certification__c, new List<pse__Skill_Certification_Rating__c>());
                }
                skillIdToSkillCertiRatingsMap.get(scr.pse__Skill_Certification__c).add(scr);
            }
            system.debug('***validateAndCreateCandidateMatchScores:3->getCpuTime()***'+Limits.getCpuTime());
            if(!skillToRSRsMap.IsEmpty() && !skillIdToSkillCertiRatingsMap.IsEmpty()) {
                Map<String, List<OWF_Assignment_Setting__mdt>> oasIdToOasMap = new Map<String, List<OWF_Assignment_Setting__mdt>>();
                Set<String> oasFieldSet = new Set<String>{'Id','Complexity_Score__c','Resource_Request_Type__c','Sub_Group__c', 'RFP_Ranking__c',
                                                            'Matching_Point_Indication__c', 'Matching_Point_Therapeutic_Area__c', 'Matching_Point_Line_of_Business__c', 'Matching_Point_Potential_Regions__c',
                                                         'Ignore_FTE_Threshold__c','Matching_Point_Threshold__c','Automation_backup_flag__c','Disable_availability_flag__c'};
                for(OWF_Assignment_Setting__mdt oasCMT : new SLT_OWF_Assignment_Setting(false, false).getOWFAssignmentSettingRecords(oasFieldSet)) {
                    String assSettingKey = oasCMT.Resource_Request_Type__c + + '~' + oasCMT.Sub_Group__c 
                                            + ((String.IsBlank(oasCMT.RFP_Ranking__c) || oasCMT.RFP_Ranking__c.trim() == '*') ? 'Null' : oasCMT.RFP_Ranking__c);
                    
                    if(!oasIdToOasMap.containsKey(assSettingKey)) {
                        oasIdToOasMap.put(assSettingKey, new List<OWF_Assignment_Setting__mdt>());
                    } 
                    oasIdToOasMap.get(assSettingKey).add(oasCMT);                                      
                }
                
                system.debug('***validateAndCreateCandidateMatchScores:4->getCpuTime()***'+Limits.getCpuTime());
                for(Id skillId : skillToRSRsMap.keySet()) {
                    for(pse__Resource_Skill_Request__c rsr : skillToRSRsMap.get(skillId)) {
                        if(skillIdToSkillCertiRatingsMap.containsKey(skillId)) {
                            pse__Resource_Request__c rr = resourceRequestsMap.get(rsr.pse__Resource_Request__c);
                            OWF_Assignment_Setting__mdt matchedAssignmentSetting;
                            String assSettingKey = '';
                            if(rr.SubGroup__c != NULL && rr.Resource_Request_Type__c != NULL) {
                                assSettingKey = rr.Resource_Request_Type__c + '~' +  rr.SubGroup__c 
                                                        +(String.IsBlank(rr.Agreement__r.RFP_Ranking__c) ? 'Null' : rr.Agreement__r.RFP_Ranking__c);
                            }
                            if(String.IsNotBlank(assSettingKey) && oasIdToOasMap.containsKey(assSettingKey)) {
                                for(OWF_Assignment_Setting__mdt assSetting : oasIdToOasMap.get(assSettingKey)) {
                                    if(rr.Complexity_Score_Total__c >=  assSetting.Complexity_Score__c) {
                                        matchedAssignmentSetting = assSetting;
                                        break;
                                    }
                                }
                            }
                            
                            for(pse__Skill_Certification_Rating__c scr : skillIdToSkillCertiRatingsMap.get(skillId)) {
                                String selectedSkillCertificationRatingVal = scr.pse__Rating__c;
                                
                                if(rsr.pse__Resource_Request__r.SubGroup__c == scr.pse__Resource__r.Sub_Group__c
                                   && scr.pse__Resource__r.Available_For_Triage_Flag__c
                                   && scr.pse__Resource__r.pse__Is_Resource__c && scr.pse__Resource__r.pse__Is_Resource_Active__c) {
                                    ResourceCandidateMatchScore resCandidateMatchScoreRec = new ResourceCandidateMatchScore(rsr.pse__Resource_Request__c, scr.pse__Resource__c, skillId);
                                    if(matchedAssignmentSetting != null) {
                                        resCandidateMatchScoreRec.assignmentSettingRecordId = matchedAssignmentSetting.Id;
                                        resCandidateMatchScoreRec.assignmentSettingRecord = matchedAssignmentSetting;
                                        
                                        if(String.IsNotBlank(selectedSkillCertificationRatingVal) && CON_OWF.skillRatingValueMap.containsKey(selectedSkillCertificationRatingVal.trim())) {
                                            if(scr.pse__Skill_Certification__r.pse__Type__c == CON_OWF.SKILL_TYPE_INDICATION)
                                                resCandidateMatchScoreRec.indicationScore = matchedAssignmentSetting.Matching_Point_Indication__c * CON_OWF.skillRatingValueMap.get(selectedSkillCertificationRatingVal.trim());
                                            else if(scr.pse__Skill_Certification__r.pse__Type__c == CON_OWF.SKILL_TYPE_THERAPY_AREA)
                                                resCandidateMatchScoreRec.therapyAreaScore = matchedAssignmentSetting.Matching_Point_Therapeutic_Area__c * CON_OWF.skillRatingValueMap.get(selectedSkillCertificationRatingVal.trim());
                                            else if(scr.pse__Skill_Certification__r.pse__Type__c == CON_OWF.SKILL_TYPE_LINE_OF_BUSINESS)
                                                resCandidateMatchScoreRec.lineOfBusinessScore = matchedAssignmentSetting.Matching_Point_Line_of_Business__c * CON_OWF.skillRatingValueMap.get(selectedSkillCertificationRatingVal.trim());
                                            else if(scr.pse__Skill_Certification__r.pse__Type__c == CON_OWF.SKILL_TYPE_POTENTIAL_REGION)
                                                resCandidateMatchScoreRec.potentialRegionScore = matchedAssignmentSetting.Matching_Point_Potential_Regions__c * CON_OWF.skillRatingValueMap.get(selectedSkillCertificationRatingVal.trim());
                                        }
                                        resCandidateMatchScoreRec.matchScore = resCandidateMatchScoreRec.therapyAreaScore + 
                                                                               resCandidateMatchScoreRec.indicationScore + 
                                                                               resCandidateMatchScoreRec.potentialRegionScore + 
                                                                               resCandidateMatchScoreRec.lineOfBusinessScore;
                                    }
                                    
                                    resourceCandidateMatchScoreList.add(resCandidateMatchScoreRec);
                                }
                            }
                        }
                    }
                }
            }
        }
        system.debug('***validateAndCreateCandidateMatchScores:5->getCpuTime()***'+Limits.getCpuTime());
        return resourceCandidateMatchScoreList;
    }
    
    
    /**
    * Wrapper class to hold resource_request_Id, resourceId, assignment_Setting and matching scores
    */
    public class ResourceCandidateMatchScore implements Comparable  {
        public Id resourceRequestId;
        public Id contactId;
        public Id skillId;
        public Id assignmentSettingRecordId;
        public decimal therapyAreaScore;
        public decimal indicationScore;
        public decimal potentialRegionScore;
        public decimal lineOfBusinessScore;
        public decimal nwtPercentage;
        public Boolean availableFTE;
        public OWF_Assignment_Setting__mdt assignmentSettingRecord;
        public decimal matchScore;
        
        public decimal getMatchScore() {
            decimal totalMatchScore = this.therapyAreaScore + this.indicationScore + this.potentialRegionScore + this.lineOfBusinessScore;
            return totalMatchScore;
        }
        
        public ResourceCandidateMatchScore(Id resourceRequestId, Id contactId, Id skillId) {
            this.resourceRequestId = resourceRequestId;
            this.contactId = contactId;
            this.skillId = skillId;
            this.therapyAreaScore = 0;
            this.indicationScore = 0;
            this.potentialRegionScore = 0;
            this.lineOfBusinessScore = 0;
            this.nwtPercentage = 0;
            this.availableFTE = false;
        }
        
        public Integer compareTo(Object compareTo) {
            ResourceCandidateMatchScore rcMatchScore = (ResourceCandidateMatchScore) compareTo;
            if (matchScore == rcMatchScore.matchScore ) return 0;
            if (matchScore > rcMatchScore.matchScore ) return -1;
            return 1;       
        }
    }
}