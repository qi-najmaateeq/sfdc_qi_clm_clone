@isTest
private class TST_CPQ_PricingCallback {
    @isTest static void testOCEPricingCallback() {
        Test.startTest();

	        Account acc1 = buildTestAccount(UserInfo.getUserId());

	        Apttus_Config2__PriceList__c pl = buildTestPriceList('Test PriceList');
	        insert pl;
	        insert acc1;
	        
	        Apttus_Config2__ConfigCustomClasses__c ac = new Apttus_Config2__ConfigCustomClasses__c();
	        ac.Apttus_Config2__PricingCallbackClass__c = 'CPQ_PricingCallback';
	        ac.Name = 'System Properties';        
	        insert ac;
	        
	        Id PRICEBOOK_ID = Test.getStandardPricebookId();
	        Opportunity opp;
	        opp = buildTestOpportunity(1, acc1.Id, userinfo.getuserid(), PRICEBOOK_ID, false, true);
	        insert opp;

	        Apttus__APTS_Agreement__c  agmt = buildTestDeal(userinfo.getuserid(),opp.id,acc1.id);
	        agmt.Apttus_CMConfig__PriceListId__c=pl.ID;
    	    agmt.RecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Subscription_Products').getRecordTypeId();
	        insert agmt;
	        
	        Product2 Prod = buildTestProduct('LC9', 'Bundle','LC9', 'US', 'Go Blue');
	        list<product2> productList=new list<product2>();
	        productList.add(prod);
	        Product2 Prod1 = buildTestProduct('LC9', 'Option','LC9 Option', 'US', 'Go Blue');
	        productList.add(prod1);
	        insert productList;
	        Apttus_Config2__PriceListItem__c pli = buildTestPriceListItem(pl.id, Prod.id);  
	        insert pli;

	        Map<string,string> childProductIds = new Map<string,string>();
	        buildTestFinalizeCart(agmt.id,prod.ID,childProductIds );

        Test.stopTest();
    }

    public static Opportunity buildTestOpportunity(Integer i, Id accId, Id OwnerId, Id priceBookId, Boolean isOpen, Boolean isClosed) {
        Opportunity opp = new Opportunity();
        opp.AccountId = accId;
        opp.OwnerId = OwnerId;
        opp.Name    = 'TOpportunity' + i;
        if(isOpen){
            opp.CloseDate   = System.today() + 5;
            opp.StageName   = 'test';
        }      
        else if(isClosed){
            opp.CloseDate   = System.today() - 5;      
            opp.StageName   = 'test';
        }
        if(priceBookId != null){
            opp.Pricebook2Id = priceBookId;
        }
        //opp.CurrencyIsoCode = 'USD';
        return opp;
    }
    
    public static account buildtestAccount(id Userid) {
        //Create Test Account
        Account acc = new Account();
        acc.Name        = 'TAccount' ;
        acc.ownerId     = Userid; 
        acc.BillingStreet = 'test street';
        acc.BillingCity = 'test city';
        acc.BillingCountry = 'US';
        acc.BillingPostalCode = '34534';
    
        return acc;
    }

    public static Apttus_Config2__PriceList__c buildTestPriceList(String priceListName) {
        Apttus_Config2__PriceList__c priceList = new Apttus_Config2__PriceList__c();
        priceList.Apttus_Config2__Active__c=true;
        priceList.Name = priceListName;
          
        return PriceList;
    }
    
    public static Product2 buildTestProduct(string productName, string type, string code, string Modality, string salesFunction) {
          Product2 Product = new Product2();
          Product.IsActive=true;
          Product.Name=productName;
          Product.ProductCode=code;

          return Product;
    }
    
    public static Apttus_Config2__PriceListItem__c buildTestPriceListItem(string PriceListid, string Productid) {
          Apttus_Config2__PriceListItem__c PriceListItem = new Apttus_Config2__PriceListItem__c();
          PriceListItem.Apttus_Config2__Active__c=true;
          PriceListItem.Apttus_Config2__PriceListId__c=PriceListid;
          PriceListItem.Apttus_Config2__ProductId__c=Productid;
          PriceListItem.Apttus_Config2__PriceType__c='One Time';
          PriceListItem.Apttus_Config2__PriceMethod__c='Per Unit';
          PriceListItem.Apttus_Config2__ListPrice__c=0;
          PriceListItem.Apttus_Config2__PriceUom__c='Each';
          PriceListItem.Apttus_Config2__RollupPriceToBundle__c=true; 
          PriceListItem.Apttus_Config2__AllowManualAdjustment__c=true;
          PriceListItem.Apttus_Config2__AllocateGroupAdjustment__c=true; 
          
          return PriceListItem;
    }
    private static id buildTestConfiguration(id agreementId) {
         Apttus_Config2__ProductConfiguration__c pc = new Apttus_Config2__ProductConfiguration__c();
         pc.Apttus_CMConfig__AgreementId__c = agreementId;
         insert pc;
         return pc.Id;
    }
    
    public static Apttus__APTS_Agreement__c buildTestDeal(id ownerid, id opportunityID, id accountId) {
         Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c();
         agmt.Apttus__Related_Opportunity__c = opportunityID;
         agmt.OwnerId = ownerid;
         agmt.O_Testimonial_Discount__c = 10;
         return agmt;
    }
    
    public static boolean buildTestFinalizeCart(id agreementId, id bundleProductId, map<string,string> childProductIds) {
        Id configid;
        
        if(agreementId!=null)
            configid= buildTestConfiguration(agreementId);

        Apttus_CPQApi.CPQ.AddBundleRequestDO request=new Apttus_CPQApi.CPQ.AddBundleRequestDO();
        request.CartId= configid;         

        Apttus_CPQApi.CPQ.SelectedBundleDO SelectedBundle = new Apttus_CPQApi.CPQ.SelectedBundleDO();
        
        Apttus_CPQApi.CPQ.SelectedProductDO SelectedProduct= new Apttus_CPQApi.CPQ.SelectedProductDO();
        SelectedProduct.ProductId=bundleProductId;
        SelectedProduct.Quantity=1;
        SelectedProduct.StartDate=system.today();
        SelectedProduct.EndDate=system.today().addMonths(2);
             
        SelectedBundle.SelectedProduct=SelectedProduct;
             
        List<Apttus_CPQApi.CPQ.SelectedOptionDO> selectedOptionDoList= new list<Apttus_CPQApi.CPQ.SelectedOptionDO>();

        for(String productid: childProductIds.keySet()){
            String componentId = childProductIds.get(productid);
            Apttus_CPQApi.CPQ.SelectedOptionDO option= new Apttus_CPQApi.CPQ.SelectedOptionDO();
            option.ComponentId = componentId ;
            option.ComponentProductId=productid;
            option.Quantity = 1;
            selectedOptionDoList.add(option);
        }

        SelectedBundle.SelectedOptions= selectedOptionDoList;
        request.SelectedBundle = SelectedBundle;
        Apttus_CPQApi.CPQ.AddBundleResponseDO AddBundleRespons= Apttus_CPQApi.CPQWebService.addBundle(request);  

        List<Apttus_Config2__LineItem__c> items = [Select Id From Apttus_Config2__LineItem__c];
        for (Apttus_Config2__LineItem__c item: items) {
        	item.Apttus_Config2__ChargeType__c = 'Subscription Fee';
        }
        update items;

        // Create Adjustments
        Apttus_Config2.PricingWebService.updatePriceForCart(configId);

        // Edit Adjustments
        Apttus_Config2.PricingWebService.updatePriceForCart(configId);

        return true;
    }
}