/*
 * Version       : 1.0
 * Description   : Apex Controller for PortalTopic component.
 */
public with sharing class CNT_CSM_PortalTopic {
	
	/*
     * Return List of Topic for current user
     */
    @AuraEnabled
    public static List<Topic> getTopicsByName(List<String> topicsList){
    	Set<String> topicsName = new Set<String>();
    	for (Integer i=0; i<topicsList.size();i++)
    			topicsName.add(topicsList[i]);
    	return new SLT_Topic().selectByName(topicsName);
    }
    
    /*
     * Return List of Topic for current user
     */
    @AuraEnabled
    public static List<Topic> getTopics(){
        List<User> users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        Set<String> fieldSet = new Set<String>();
        fieldSet.add('Name');
        fieldSet.add('Id');
        String filterCondition ='';
        filterCondition = 'Id in(SELECT Product2Id FROM asset WHERE AccountId=\''+ users[0].AccountId +'\' AND Status !=\'Obsolete\')';
        List<Product2> products =  new SLT_Product2().getProductWithFilter(fieldSet,filterCondition);
      //  List<Product2> products = new SLT_Product2().selectByAccountId(new Set<Id> {users[0].AccountId});
        Set<String> productsName = new Set<String>();
        List<ContentFolder> cf;
        List<ContentFolder> cf2;
        List<ContentFolderMember> cfm;
        List<TopicAssignment> ta ;
        Boolean atLeastOneArtcile = false;
        Boolean atLeastOneFileOrFolder = false;
        for (Integer i=0; i<products.size();i++){
            if (products[i].Community_Topics__c != null){
               productsName.add(products[i].Community_Topics__c);
            }
        }
        return new SLT_Topic().selectByName(productsName);
    }

    /*
     * Return List of Topic for current user by product Community doc category
     */
    @AuraEnabled
    public static List<Topic> getTopicsByDocCategory(String category){
        List<User> users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        Set<String> fieldSet = new Set<String>();
        fieldSet.add('Name');
        fieldSet.add('Id');
        String filterCondition ='';
        filterCondition = 'Id in(SELECT Product2Id FROM asset WHERE AccountId=\''+ users[0].AccountId +'\' AND Status !=\'Obsolete\')';
        filterCondition += ' AND Community_Doc_Category__c INCLUDES (\''+category +'\') ';        
        List<Product2> products =  new SLT_Product2().getProductWithFilter(fieldSet,filterCondition);
        Set<String> productsName = new Set<String>();
        for (Integer i=0; i<products.size();i++){
            if (products[i].Community_Topics__c != null){
               productsName.add(products[i].Community_Topics__c);
            }
        }
        return new SLT_Topic().selectByName(productsName);
    }
}