/**
* This test class is used to test all methods in DAOH_OWF_Resource_Skill_Request.
* version : 1.0
*/
@isTest(seeAllData=false)
private class TST_DAOH_OWF_Resource_Skill_Request {
    /**
    * This method used to set up testdata
    */ 
    @testSetup
    static void dataSetup() {
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'OWF_Triage_Manager'];
        List<PermissionSetAssignment> psa = [Select id from PermissionSetAssignment where AssigneeId =:UserInfo.getUserId() AND PermissionSetId =:ps.Id];
        if(psa.size() == 0)
            insert new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = ps.Id);
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            Account acc = UTL_OWF_TestData.createAccount();
            insert acc;
            pse__Grp__c grp = UTL_OWF_TestData.createGroup();
            insert grp;
            Indication_List__c indication = UTL_OWF_TestData.createIndication('Test Indication', 'Acute Care');
            insert indication;
            Line_Item_Group__c lineItemGroup = UTL_OWF_TestData.createLineItemGroup(indication.Id);
            insert lineItemGroup;
            
            Contact cont = UTL_OWF_TestData.createContact(acc.Id);
            cont.pse__Is_Resource__c = true;
            cont.pse__Is_Resource_Active__c = true;
            cont.pse__Group__c = grp.Id;
            cont.pse__Salesforce_User__c = UserInfo.getUserId();
            cont.sub_group__c = 'TSL-Japan';
            cont.Available_For_Triage_Flag__c = true;
            insert cont;
            
            Contact cont2 = UTL_OWF_TestData.createContact(acc.Id);
            cont2.pse__Is_Resource__c = true;
            cont2.pse__Is_Resource_Active__c = true;
            cont2.pse__Group__c = grp.Id;
            cont.pse__Salesforce_User__c = UserInfo.getUserId();
            cont2.sub_group__c = 'GBO-RFI';
            cont2.Available_For_Triage_Flag__c = true;
            insert cont2;
            /*
            pse__Proj__c bidProject = UTL_OWF_TestData.createBidProject(grp.Id);
            insert bidProject;
            */
            pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
            insert permissionControlGroup;
            
            Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
            opp.Potential_Regions__c = 'Japan';
            opp.Line_of_Business__c = 'Core Clinical';
            opp.Line_Item_Group__c = lineItemGroup.Id;
            insert opp;
            
            Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreement(acc.Id, opp.Id);
            //agreement.Resources_Needed__c = 'TOPS;Medical';
            agreement.Bid_Due_Date__c = Date.today().addDays(7);
            agreement.recordTypeId = CON_OWF.OWF_RFI_AGREEMENT_RECORD_TYPE_ID;
            insert agreement;
            pse__Proj__c bidProject = [Select id from pse__Proj__c where Agreement__c =: agreement.Id];
            pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
            resourceRequest.pse__Group__c = grp.Id;
            resourceRequest.pse__Resource__c = cont.Id;
            resourceRequest.SubGroup__c = 'TSL-Japan';
            resourceRequest.Complexity_Score_Total__c = 50;
            insert resourceRequest;
            
            pse__Schedule__c schedule = UTL_OWF_TestData.createSchedule();
            insert schedule;
            
            pse__Assignment__c assignement =  UTL_OWF_TestData.createAssignment(agreement.Id, bidProject.Id, schedule.Id, cont.Id, resourceRequest.Id);
            insert assignement;
        }
    }
    
    /**
    * This method used to test the CreateResourceCandidateMatchScore method
    */ 
    static testmethod void testCreateResourceCandidateMatchScoreWithIndication() {
        System.runAs(new User(Id = UserInfo.getUserId())) {
            pse__Skill__c skill = UTL_OWF_TestData.createSkills('Test Skill', CON_OWF.SKILL_TYPE_INDICATION);
            insert skill;
            
            List<Contact> contactList = [Select Id From Contact where sub_group__c = 'TSL-Japan'];
            List<pse__Resource_Request__c> resRequestList = [Select Id From pse__Resource_Request__c];
            
            pse__Skill_Certification_Rating__c skillCertRating = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
            insert skillCertRating;
            
            Test.startTest();
                pse__Resource_Skill_Request__c resourceSkillRequest = UTL_OWF_TestData.createResourceSkillRequest(skill.Id, resRequestList.get(0).Id);
                insert resourceSkillRequest;
                List<pse__Resource_Skill_Request__c> resourceSkillRequestList = [Select Id, pse__Resource_Request__c FROM pse__Resource_Skill_Request__c];
                List<DAOH_OWF_Resource_Skill_Request.ResourceCandidateMatchScore> resourceCandidateMatchScoreList = DAOH_OWF_Resource_Skill_Request.createResourceCandidateMatchScore(resourceSkillRequestList);
                Integer expectedListSize = 1;
                //system.assertEquals(expectedListSize, resourceCandidateMatchScoreList.size());
            Test.stopTest();
        }
    }
    
    /**
    * This method used to test the CreateResourceCandidateMatchScore method
    */ 
    static testmethod void testCreateResourceCandidateMatchScoreWithTherapyArea() {
        System.runAs(new User(Id = UserInfo.getUserId())) {
            pse__Skill__c skill = UTL_OWF_TestData.createSkills('Test Skill', CON_OWF.SKILL_TYPE_THERAPY_AREA);
            insert skill;
            
            List<Contact> contactList = [Select Id From Contact where sub_group__c = 'TSL-Japan'];
            List<pse__Resource_Request__c> resRequestList = [Select Id From pse__Resource_Request__c];
            
            pse__Skill_Certification_Rating__c skillCertRating = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
            insert skillCertRating;
            
            Test.startTest();
                pse__Resource_Skill_Request__c resourceSkillRequest = UTL_OWF_TestData.createResourceSkillRequest(skill.Id, resRequestList.get(0).Id);
                insert resourceSkillRequest;    
                List<pse__Resource_Skill_Request__c> resourceSkillRequestList = [Select Id, pse__Resource_Request__c FROM pse__Resource_Skill_Request__c];
                
                List<DAOH_OWF_Resource_Skill_Request.ResourceCandidateMatchScore> resourceCandidateMatchScoreList = DAOH_OWF_Resource_Skill_Request.createResourceCandidateMatchScore(resourceSkillRequestList);
                Integer expectedListSize = 1;
                //system.assertEquals(expectedListSize, resourceCandidateMatchScoreList.size());
                //system.assert(true, resourceCandidateMatchScoreList.get(0).getMatchScore() > 0);
            Test.stopTest();   
        }
    }
    
    static testmethod void testWrapperClassResourceCandidateMatchScore() 
    {
        DAOH_OWF_Resource_Skill_Request.ResourceCandidateMatchScore rrCMScore = new DAOH_OWF_Resource_Skill_Request.ResourceCandidateMatchScore(null, null, null);
        test.startTest();
        Decimal suedoMScore = rrCMScore.getMatchScore();
        Integer compareResult = rrCMScore.compareTo(rrCMScore);
        test.stopTest();
        system.assertEquals(0, compareResult);
        
    }
}