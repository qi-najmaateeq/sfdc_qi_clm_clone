public class CPQ_Utility {

    public static Set<Id> getFoundationRecordTypes(){
        return new Set<Id>{
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
				CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId(),
			SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
                CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId(),
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
                CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER).getRecordTypeId(),
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
                CON_CPQ.AGREEMENT_FDTN_CNF).getRecordTypeId(),
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
                CON_CPQ.AGREEMENT_FDTN_CONTRACT).getRecordTypeId()                
		};
    }
    
    
    public static Set<Id> getContractRecordTypes(){
        return new Set<Id>{
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
                CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER).getRecordTypeId(),
			SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
                CON_CPQ.AGREEMENT_FDTN_CNF).getRecordTypeId(),
			SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
                CON_CPQ.AGREEMENT_FDTN_CONTRACT).getRecordTypeId() 
		};
    }
    
    public static Set<Id> getInitialRecordTypes(){
        return new Set<Id>{
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
				CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId(),
			SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
                CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId()   
        };
    }
    
    public static Set<Id> getInitialBidRecordTypes(){
        return new Set<Id>{
			SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(
				CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId()   
        };
    }
    
	public static Messaging.SingleEmailMessage getSingleMessage(String emailTemplateName, Id whatId, Id whoId, List<String> addressList){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<EmailTemplate> emailTemplate = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(
        	emailTemplateName, new Set<String>{CON_CPQ.Id});
        if(emailTemplate.size() > 0){
            Messaging.SingleEmailMessage mailTemp = Messaging.renderStoredEmailTemplate(
            emailTemplate[0].Id, whoId, whatId);
            mail.setSubject(mailTemp.getSubject());
            mail.setHtmlBody(mailTemp.getHtmlbody());
            mail.setUseSignature(false); 
            mail.setBccSender(false); 
            mail.setSaveAsActivity(false);
            mail.setWhatId(whoId); 
            mail.setToAddresses(addressList);
            return mail;
        }
        return null;
    }    
}