/**
 * This test class is used to test UTL_OWF class.
 * version : 1.0 
 */
@isTest(seeAllData=false)
private class TST_UTL_OWF {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        
        OWF_Config__c owfConfig = UTL_OWF_TestData.setupOWFConfig(grp.Id);
        insert owfConfig;
        
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        cont.pse__Salesforce_User__c = UserInfo.getUserId();
        cont.sub_group__c = 'TSL-Japan';
        cont.available_for_triage_flag__c = true;
        insert cont;
        
        Indication_List__c indication = UTL_OWF_TestData.createIndication('Test Indication', 'Acute Care');
        insert indication;
        
        Line_Item_Group__c lineItemGroup = UTL_OWF_TestData.createLineItemGroup(indication.Id);
        lineItemGroup.Presentation_Date__c = system.today().addDays(5);
        lineItemGroup.Phase__c = 'Phase 1';
        lineItemGroup.BD_Lead_Sub_Region__c = 'United States of America';
        insert lineItemGroup;
        
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        opp.QI_Invited_to_Present__c = 'Not Sure';
        opp.Line_Item_Group__c = lineItemGroup.Id;
        opp.Line_of_Business__c = 'Core Clinical';
        opp.Bid_Defense_Date__c = system.today().addDays(5);
        insert opp;
        
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        agreement.Bid_Due_Date__c = system.today().addDays(5);
        insert agreement;

        /*pse__Proj__c bidProject = UTL_OWF_TestData.createBidProject(grp.Id);
        bidProject.Agreement__c = agreement.Id;
        insert bidProject;
        */
        pse__Proj__c bidProject = [Select id from pse__Proj__c where Agreement__c =: agreement.Id];
        
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest.pse__Group__c = grp.Id;
        resourceRequest.SubGroup__c = 'TSL-Japan';
        insert resourceRequest;
        
        pse__Assignment__c assignment = UTL_OWF_TestData.createAssignment(agreement.id, bidProject.id, null, cont.id, resourceRequest.id);
        insert assignment;
        
        List<OWF_Resources_Needed_to_SubGroup_Map__c> resourceToSubGroupSettingList = new List<OWF_Resources_Needed_to_SubGroup_Map__c>();
        resourceToSubGroupSettingList.add(new OWF_Resources_Needed_to_SubGroup_Map__c(
            Resources_Needed__c = 'Clinical Analytics & Simulations',
            Sub_Group__c = 'CP&A-CA&S',
            Name = 'Clinical Analytics & Simulations'
        ));
        resourceToSubGroupSettingList.add(new OWF_Resources_Needed_to_SubGroup_Map__c(
            Resources_Needed__c = 'Medical',
            Sub_Group__c = 'Medical-MSL',
            Name = 'Medical'
        ));
        resourceToSubGroupSettingList.add(new OWF_Resources_Needed_to_SubGroup_Map__c(
            Resources_Needed__c = 'TOPS',
            Sub_Group__c = 'CP&A-TOPS',
            Name = 'TOPS'
        ));
        
        insert resourceToSubGroupSettingList;
    }
    
    /**
     * This test method used to test SendMailOnException method.
     */ 
    static testMethod void testSendMailOnException(){
        Map<Id, String> assignmentIdToErrorMessageMap = new Map<Id, String>();
        assignmentIdToErrorMessageMap.put(userInfo.getUserId(), 'Test Error Message');
        String emailSubject = 'Test Email Subject';
        UTL_OWF.sendMailOnException('', assignmentIdToErrorMessageMap, emailSubject);
    }
    
    /**
     * This test method used to test SendMailOnException method With User Ids.
     */ 
    static testMethod void testSendMailOnExceptionWithUserIds(){
        OWF_Batch_Config__c owfBatchConfig = UTL_OWF_TestData.setupOWFBatchConfig('BCH_OWF_AcceptUnassignedAssignments');
        owfBatchConfig.User_Ids__c = userInfo.getUserId();
        insert owfBatchConfig;
        Map<Id, String> assignmentIdToErrorMessageMap = new Map<Id, String>();
        assignmentIdToErrorMessageMap.put(userInfo.getUserId(), 'Test Error Message');
        String emailSubject = 'Test Email Subject';
        UTL_OWF.sendMailOnException('BCH_OWF_AcceptUnassignedAssignments', assignmentIdToErrorMessageMap, emailSubject);
    }
    
    static testMethod void testProcessResRequestForInitialBids_Scenario1_2(){
        Opportunity oppty = [Select Id From Opportunity where Name = 'TestOpportunity'];
        Set<Id> sObjectIdSet = new Set<Id>{oppty.Id};
        String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ' +
            ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
        List<pse__Resource_Request__c> resourceRequestList = UTL_OWF.processResRequestForInitialBids(sObjectIdSet, agrCondition, null, null);
    }
    
    static testMethod void testProcessResRequestForInitialBids_ScenarioCPA_Tops_Global(){
        Opportunity oppty = [Select Id From Opportunity where Name = 'TestOpportunity'];
        Test.startTest();
            oppty.Line_of_Business__c = 'Core Clinical';
            oppty.Potential_Regions__c = 'Global';
            update oppty;
            
            Apttus__APTS_Agreement__c agreement = [Select Id From Apttus__APTS_Agreement__c][0];
            agreement.Requested_Services__c = 'Late Phase Site Start up (SPS)';
            update agreement;
            
            Set<Id> sObjectIdSet = new Set<Id>{oppty.Id};
            String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ' +
                ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
            List<pse__Resource_Request__c> resourceRequestList = UTL_OWF.processResRequestForInitialBids(sObjectIdSet, agrCondition, null, null);
        Test.stopTest();
    }
    
    static testMethod void testProcessResRequestForInitialBids_ScenarioSNPProduct(){
        Opportunity oppty = [Select Id From Opportunity where Name = 'TestOpportunity'];
        Test.startTest();
            oppty.Line_of_Business__c = 'Novella';
            oppty.Potential_Regions__c = 'Global';
            update oppty;
            
            Apttus__APTS_Agreement__c agreement = [Select Id From Apttus__APTS_Agreement__c][0];
            agreement.Requested_Services__c = 'Project Management;Clinical Monitoring';
            update agreement;
            
            Set<Id> sObjectIdSet = new Set<Id>{oppty.Id};
            String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ' +
                ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
            List<pse__Resource_Request__c> resourceRequestList = UTL_OWF.processResRequestForInitialBids(sObjectIdSet, agrCondition, null, null);
        Test.stopTest();
    }
    
    static testMethod void testProcessResRequestForInitialBids_ScenarioTsl(){
        Opportunity oppty = [Select Id From Opportunity where Name = 'TestOpportunity'];
        Test.startTest();
            oppty.Line_of_Business__c = 'Novella';
            oppty.Potential_Regions__c = 'Asia Pacific';
            //Intervention_Type__c = '';
            update oppty;
            
            Apttus__APTS_Agreement__c agreement = [Select Id From Apttus__APTS_Agreement__c][0];
            agreement.Requested_Services__c = 'Project Management;Clinical Monitoring';
            update agreement;
            
            Set<Id> sObjectIdSet = new Set<Id>{oppty.Id};
            String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ' +
                ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
            List<pse__Resource_Request__c> resourceRequestList = UTL_OWF.processResRequestForInitialBids(sObjectIdSet, agrCondition, null, null);
        Test.stopTest();
    }
    
    static testMethod void testProcessResRequestForInitialBids_ScenarioJapan_DSB_SNP(){
        Opportunity oppty = [Select Id From Opportunity where Name = 'TestOpportunity'];
        Test.startTest();
            oppty.Line_of_Business__c = 'Core Clinical';
            update oppty;
            
            Apttus__APTS_Agreement__c agreement = [Select Id From Apttus__APTS_Agreement__c][0];
            agreement.Requested_Services__c = 'Project Management;Clinical Monitoring';
            agreement.Targeted_Countries__c = 'JP';
            update agreement;
            
            Set<Id> sObjectIdSet = new Set<Id>{oppty.Id};
            String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ' +
                ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
            List<pse__Resource_Request__c> resourceRequestList = UTL_OWF.processResRequestForInitialBids(sObjectIdSet, agrCondition, null, null);
        Test.stopTest();
    }
    
    static testMethod void testProcessResRequestForInitialBids_ScenarioMedical(){
        Opportunity oppty = [Select Id From Opportunity where Name = 'TestOpportunity'];
        Test.startTest();
            oppty.Line_of_Business__c = 'Core Clinical';
            oppty.Potential_Regions__c = 'Asia Pacific';
            update oppty;
            
            Apttus__APTS_Agreement__c agreement = [Select Id From Apttus__APTS_Agreement__c][0];
            agreement.Requested_Services__c = 'Project Management;Clinical Monitoring';
            agreement.Targeted_Countries__c = 'JP';
            update agreement;
            
            Set<Id> sObjectIdSet = new Set<Id>{oppty.Id};
            String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ' +
                ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
            List<pse__Resource_Request__c> resourceRequestList = UTL_OWF.processResRequestForInitialBids(sObjectIdSet, agrCondition, null, null);
        Test.stopTest();
    }
    
    /**
     * This test method used to validate created Resource Request based on project inserted
     */ 
    static testMethod void testProcessResRequestForInitialBids_ScenarioReBid() {
        Account account = [Select Id From Account Where Name = 'TestAccount'];
        Opportunity oppty = [Select Id From Opportunity Where Name = 'TestOpportunity'];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'][0];    
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        
        Test.startTest();
            //oppty.Line_Item_Group__c = lineItemGroup.Id;
            oppty.Potential_Regions__c = 'Asia Pacific';
            oppty.Line_of_Business__c = 'Core Clinical';
            update oppty;
            
            Apttus__APTS_Agreement__c agreement1 = UTL_OWF_TestData.createAgreementByRecordType(account.Id, oppty.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
            agreement1.Resources_Needed__c = 'TOPS;Medical';
            agreement1.Bid_Due_Date__c = Date.today().addDays(5);
            insert agreement1;
            List<pse__Resource_Request__c> resReqsList = [Select Id, SubGroup__c, pse__Status__c, pse__Assignment__c   From pse__Resource_Request__c Where Agreement__c = :agreement1.Id];
            
            pse__Proj__c project = [Select Id,pse__Group__c,Agreement__c,pse__End_Date__c,CurrencyIsoCode From pse__Proj__c Where Agreement__c =: agreement1.Id limit 1];
            
            pse__Resource_Request__c resReq =  UTL_OWF_TestData.createResourceRequest(agreement1.Id, oppty.Id, project.Id);
            resReq.pse__Status__c = 'Assigned';
            resReq.pse__Resource__c = cont.ID;
            resReq.SubGroup__c = 'TSL-Japan';
            resReq.pse__Group__c = grp1.ID;
            insert resReq;
        
            pse__Schedule__c schedule = UTL_OWF_TestData.createSchedule();
            insert schedule;
        
            pse__Assignment__c assignment = UTL_OWF_TestData.createAssignment(agreement1.Id, project.Id, schedule.Id, cont.Id, resReq.Id);
            assignment.pse__Status__c = 'Accepted';
            insert assignment;
        
            System.assert(true, resReqsList.size() > 0);

        pse__Resource_Request__c resReq1 =  UTL_OWF.cloneReBidResourceRequest(project,resReq);
        Test.stopTest();
    }
    
    /**
     * This test method used to validate the CalculateComplexityScoreTotal method
     */ 
    static testMethod void testCalculateComplexityScoreTotal() {
        Map<String,OWF_Resource_Setting__mdt> resSettingIdToResSettingMap = new Map<String,OWF_Resource_Setting__mdt>(); 
        Set<String> orsFieldSet = new Set<String>{'Id','Sub_Group__c', 'Estimated_Fees__c','Is_there_a_Client_Bid_Grid__c','Number_of_Sites__c',
                                   'Potential_Regions__c','Project_Ex_Number_of_Unique_Tables__c','Re_Bid_Complexity__c',
                                  'Requested_Services__c','RFP_Ranking__c','Staffing_Number_of_Unique_Tables__c'};
        for(OWF_Resource_Setting__mdt rsCMT : new SLT_OWF_Resource_Setting(false, false).getOWFResourceSettingRecords(orsFieldSet)) {
            resSettingIdToResSettingMap.put(rsCMT.Sub_Group__c,rsCMT);                                          
        }
        
        List<pse__Resource_Request__c> resourceRequestList = [Select id, SubGroup__c, Agreement__c from pse__Resource_Request__c limit 1];
        resourceRequestList[0].SubGroup__c = 'TSL';
        Map<Id,Apttus__APTS_Agreement__c> mapAgreementBuId = new Map<Id,Apttus__APTS_Agreement__c>([Select Id, RFP_Ranking__c, Number_of_Requested_Services__c, 
                                 Apttus__Related_Opportunity__c, Project_Ex_Number_of_Unique_Tables__c, Staffing_Number_of_Unique_Tables__c, Is_there_a_Client_Bid_Grid__c,
                                 Estimated_Fees__c, Re_Bid_Complexity__c, Number_of_Sites__c, Bid_Due_Date__c, Apttus__Related_Opportunity__r.Number_of_Potential_Regions__c,
                                 Apttus__Related_Opportunity__r.Potential_Regions__c, Apttus__Related_Opportunity__r.Presentation_Date__c                                                                
                                 From Apttus__APTS_Agreement__c]);
        Test.startTest();
            update resourceRequestList;
        List<pse__Resource_Request__c> updateResourceRequestList = UTL_OWF.calculateComplexityScoreTotal(mapAgreementBuId,resSettingIdToResSettingMap, resourceRequestList);
        Test.stopTest();
    }
    
     /**
     * This test method used to validate the isOPPhasOWFAgreement method
     */     
    static testMethod void testIsOPPhasOWFAgreement(){
        Opportunity oppty = [Select Id From Opportunity Where Name = 'TestOpportunity'];

        Test.startTest();
            Boolean isOWFAgreement = UTL_OWF.isOPPhasOWFAgreement(new Set<Id> {oppty.Id});
        system.debug('isOWFAgreement->'  + isOWFAgreement);
        Test.stopTest();
        System.assertEquals(true, isOWFAgreement, 'The result should be true');
    }
    
    /**
     * This test method used to validate the isLoginUserhasPermissionControl method
     */
    static testMethod void testIsLoginUserhasPermissionControl(){
        Test.startTest();
            Boolean isPCExist = UTL_OWF.isLoginUserhasPermissionControl();
        Test.stopTest();
        System.assertEquals(true, isPCExist, 'The result should be true');
    }
    
    static testMethod void testCreateApexErrorLogs() {
        Opportunity oppty = [Select Id From Opportunity Where Name = 'TestOpportunity'];
        String myIdPrefix = String.valueOf(oppty.id).substring(0,3);
        Map<id, String> sObjectIdToErrorMessageMap = new Map<id, String>();
        sObjectIdToErrorMessageMap.put(oppty.id, 'Test');
        Test.startTest();
            UTL_OWF.createApexErrorLogs('Opportunity', 'Error', sObjectIdToErrorMessageMap);
        Test.stopTest();
    }
    
    static testmethod void testCreateResourceCandidateMatchScoreWithTherapyArea() {
        System.runAs(new User(Id = UserInfo.getUserId())) {
            pse__Skill__c skill = UTL_OWF_TestData.createSkills('Test Skill', CON_OWF.SKILL_TYPE_THERAPY_AREA);
            insert skill;
            
            Apttus__APTS_Agreement__c agreement = [Select Id From Apttus__APTS_Agreement__c][0];
            pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
            
            pse__Schedule__c schedule = UTL_OWF_TestData.createSchedule();
            insert schedule;
            
            List<Contact> contactList = [Select Id From Contact];
            contactList[0].Available_for_Triage_Flag__c = true;
            update contactList[0];
            
            List<pse__Resource_Request__c> resRequestList = [Select Id From pse__Resource_Request__c];
            
            pse__Assignment__c assignment = UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, schedule.Id, contactList.get(0).Id, null);
            assignment.Assignment_Type__c = 'Days Off';
            insert assignment;
            
            pse__Skill_Certification_Rating__c skillCertRating = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
            insert skillCertRating;
            
            Test.startTest();
                pse__Resource_Skill_Request__c resourceSkillRequest = UTL_OWF_TestData.createResourceSkillRequest(skill.Id, resRequestList.get(0).Id);
                insert resourceSkillRequest;    
                List<pse__Resource_Skill_Request__c> resourceSkillRequestList = [Select Id, pse__Resource_Request__c FROM pse__Resource_Skill_Request__c];
                
                List<DAOH_OWF_Resource_Skill_Request.ResourceCandidateMatchScore> resourceCandidateMatchScoreList = DAOH_OWF_Resource_Skill_Request.createResourceCandidateMatchScore(resourceSkillRequestList);
                List<DAOH_OWF_Resource_Skill_Request.ResourceCandidateMatchScore> resourceCandidateMatchScoreListReturn = UTL_OWF.calculateNWTimePercentage(resourceCandidateMatchScoreList);
            Test.stopTest();   
        }
    }
}