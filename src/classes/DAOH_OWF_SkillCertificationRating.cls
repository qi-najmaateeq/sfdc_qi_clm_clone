/**
* This is pse__Skill_Certification_Rating__c trigger handler class.
* version : 1.0
*/
public class DAOH_OWF_SkillCertificationRating {
    
    public static void checkIfRatingAlreadyExists(List<pse__Skill_Certification_Rating__c> newList) {
        //if(!Test.isRunningTest()){
            Set<Id> resourceIdSet = new Set<Id>();
            Map<Id, pse__Skill_Certification_Rating__c> scrIdToSkillCertiRatingMap = new Map<Id, pse__Skill_Certification_Rating__c>();
            for(pse__Skill_Certification_Rating__c scr : newList) {
                if(scr.pse__Resource__c != null) {
                    resourceIdSet.add(scr.pse__Resource__c);
                }
            }
            String skillCertRatingCondition = ' pse__Resource__c IN :sObjectIdSet ';
            Set<String> skillCertRatingFieldSet = new Set<String>{'Id', 'pse__Resource__c',  'pse__Skill_Certification__c', 'pse__Rating__c'};
                Map<Id, Map<Id, pse__Skill_Certification_Rating__c>> scrIdToSkillCertRatingWithContactMap = new Map<Id, Map<Id, pse__Skill_Certification_Rating__c>>();
            if(resourceIdSet.size() > 0) {
                scrIdToSkillCertiRatingMap = new SLT_Skill_Certification_Rating(false,false).getSkillCertificationRatingsByContactIds(resourceIdSet, skillCertRatingCondition, skillCertRatingFieldSet);
                if(!scrIdToSkillCertiRatingMap.IsEmpty()) {
                    for(pse__Skill_Certification_Rating__c skillCertRating : scrIdToSkillCertiRatingMap.values()) {
                        if(!scrIdToSkillCertRatingWithContactMap.containsKey(skillCertRating.pse__Resource__c)) {
                            scrIdToSkillCertRatingWithContactMap.put(skillCertRating.pse__Resource__c, new Map<Id, pse__Skill_Certification_Rating__c>());
                        }
                        scrIdToSkillCertRatingWithContactMap.get(skillCertRating.pse__Resource__c).put(skillCertRating.pse__Skill_Certification__c, skillCertRating);
                    }
                }
                for(pse__Skill_Certification_Rating__c scr : newList) {
                    if(scrIdToSkillCertRatingWithContactMap.containsKey(scr.pse__Resource__c)) {
                        if(scrIdToSkillCertRatingWithContactMap.get(scr.pse__Resource__c).containsKey(scr.pse__Skill_Certification__c)) {
                            scr.addError('Skill Certification Rating for this combination of Skill/Certification and Resource already exists.');
                        }
                    }
                    
                }
            }
        //}
    }
    
  public static void setDefaultFields(List<pse__Skill_Certification_Rating__c> newList)
  {
      Date today = System.today();
      for(pse__Skill_Certification_Rating__c currentRecord: newList)
      {
          currentRecord.pse__Evaluation_Date__c = today;
      }
  }
    
    
    
    /*
    public static void setSelfIsDuplicateFlag(List<pse__Skill_Certification_Rating__c> newList) {
        
        if(UTL_OWF.ratingUpdateViaTrigger){
            return;
        }
        
        Set<Id> skillRatingIdUpdateSet = new Set<Id>();
        Set<Id> resourceIdSet = new Set<Id>();
        Set<Id> skillIdSet = new Set<Id>();
        Map<Id, pse__Skill_Certification_Rating__c> scrIdToSkillCertiRatingMap = new Map<Id, pse__Skill_Certification_Rating__c>();
        for(pse__Skill_Certification_Rating__c scr : newList) {
            if(scr.pse__Resource__c != null) {
                resourceIdSet.add(scr.pse__Resource__c);
                skillIdSet.add(scr.pse__Skill_Certification__c);
            }
        }
        String skillCertRatingCondition = ' pse__Resource__c IN :resourceIdSet AND pse__Skill_Certification__c IN : skillIdSet AND pse__Resource__r.pse__Is_Resource__c = True';
        Set<String> skillCertRatingFieldSet = new Set<String>{'Id', 'pse__Resource__c',  'pse__Skill_Certification__c', 'pse__Rating__c', 'pse__Numerical_Rating__c'};
        if(resourceIdSet.size() > 0) {
            scrIdToSkillCertiRatingMap = new SLT_Skill_Certification_Rating(false,false).getSkillCertificationRatingsBySkillsContact(resourceIdSet, skillIdSet, skillCertRatingCondition, skillCertRatingFieldSet);
        }
        for(pse__Skill_Certification_Rating__c updateScr : newList) {
            if(!scrIdToSkillCertiRatingMap.IsEmpty()) {
                for(pse__Skill_Certification_Rating__c scr : scrIdToSkillCertiRatingMap.values()) {
                    if(updateScr.id != scr.id && updateScr.pse__Rating__c <= scr.pse__Rating__c) {
                        updateScr.Is_Duplicate__c = true;
                        break;
                    }
                    else if(updateScr.id != scr.id && updateScr.pse__Rating__c > scr.pse__Rating__c) {
                        updateScr.Is_Duplicate__c = false;
                        break;
                    }

                }
            }
        }
    }
    
    public static void setIsDuplicateFlag(List<pse__Skill_Certification_Rating__c> newList){
        if(UTL_OWF.ratingUpdateViaTrigger){
            return;
        }
        UTL_OWF.ratingUpdateViaTrigger = true;
        Set<Id> resourceIdSet = new Set<Id>();
        Set<Id>  recordIDSet = new Set<Id>();
        Map<Id, pse__Skill_Certification_Rating__c> scrIdToSkillCertiRatingMap = new Map<Id, pse__Skill_Certification_Rating__c>();
        for(pse__Skill_Certification_Rating__c scr : newList) {
            if(scr.pse__Resource__c != null) {
                resourceIdSet.add(scr.pse__Resource__c);
                recordIDSet.add(scr.id);
            }
        }
        
        String skillCertRatingCondition = ' Id NOT IN : ratingIdSet AND pse__Resource__c IN :resourceIdSet AND pse__Resource__r.pse__Is_Resource__c = True';
        Set<String> skillCertRatingFieldSet = new Set<String>{'Id', 'pse__Resource__c',  'pse__Skill_Certification__c', 'pse__Rating__c', 'pse__Numerical_Rating__c'};
        if(resourceIdSet.size() > 0) {
            scrIdToSkillCertiRatingMap = new SLT_Skill_Certification_Rating(false,false).getSkillCertificationRatingsByContactIdsOrdering(resourceIdSet, recordIDSet, skillCertRatingCondition, skillCertRatingFieldSet);
            updateIsDuplicateFlag(scrIdToSkillCertiRatingMap.values());
            //system.assert(false,scrIdToSkillCertiRatingMap);
            System.debug('scrIdToSkillCertiRatingMap.values() ' + scrIdToSkillCertiRatingMap.values());
        }
    }
    
    public static void updateIsDuplicateFlag(List<pse__Skill_Certification_Rating__c> processList){
        System.debug('processList' + processList);
        List<pse__Skill_Certification_Rating__c> toupdateList = new List<pse__Skill_Certification_Rating__c>();
        Set<String> uniqueKey = new Set<String>();
        for(pse__Skill_Certification_Rating__c rating : processList) {
            String uniqueKeyString = String.valueOf(rating.pse__Resource__c) + String.valueOf(rating.pse__Skill_Certification__c);
            if(!uniqueKey.contains(uniqueKeyString)) {
                uniqueKey.add(uniqueKeyString);
                rating.is_Duplicate__c = false;
                toupdateList.add(rating);
            }
            else 
            {
                rating.is_Duplicate__c = true;
                toupdateList.add(rating);
            }
        }
        //system.assert(false,toupdateList);
        if(toupdateList.size() > 0) {
            update toupdateList;
        }

    }
    */
    
    public static void updateDuplicateFlag(List<pse__Skill_Certification_Rating__c> processList){
        if(UTL_OWF.ratingUpdateViaTrigger){
            return;
        }
        UTL_OWF.ratingUpdateViaTrigger = true;
        Set<Id> resourceIdSet = new Set<Id>();
        Map<Id,Id> mapResourceBySkill = new Map<Id,Id>();
        Map<Id, pse__Skill_Certification_Rating__c> scrIdToSkillCertiRatingMap = new Map<Id, pse__Skill_Certification_Rating__c>();
        for(pse__Skill_Certification_Rating__c scr : processList) {
            if(scr.pse__Resource__c != null) {
                resourceIdSet.add(scr.pse__Resource__c);
                mapResourceBySkill.put(scr.pse__Skill_Certification__c,scr.pse__Resource__c);
            }
        }
        String skillCertRatingCondition = ' pse__Resource__c IN : resourceIdSet AND pse__Skill_Certification__c IN : skillIdSet ';
        Set<String> skillCertRatingFieldSet = new Set<String>{'Id', 'pse__Resource__c',  'pse__Skill_Certification__c', 'pse__Rating__c','pse__Numerical_Rating__c'};
        Map<Id, Map<Id, pse__Skill_Certification_Rating__c>> scrIdToSkillCertRatingWithContactMap = new Map<Id, Map<Id, pse__Skill_Certification_Rating__c>>();
        Set<String> uniqueKey = new Set<String>();
        if(resourceIdSet.size() > 0) {
            scrIdToSkillCertiRatingMap = new SLT_Skill_Certification_Rating(false,false).getSkillCertificationRatingsBySkillsContact(resourceIdSet, mapResourceBySkill.keySet(), skillCertRatingCondition, skillCertRatingFieldSet);
            List<pse__Skill_Certification_Rating__c> toupdateList = new List<pse__Skill_Certification_Rating__c>();
            for(pse__Skill_Certification_Rating__c rating : scrIdToSkillCertiRatingMap.values()) {
                String uniqueKeyString = String.valueOf(rating.pse__Resource__c) + String.valueOf(rating.pse__Skill_Certification__c);
                if(!uniqueKey.contains(uniqueKeyString)) {
                    uniqueKey.add(uniqueKeyString);
                    rating.is_Duplicate__c = false;
                    toupdateList.add(rating);
                }
                else {
                    rating.is_Duplicate__c = true;
                    toupdateList.add(rating);
                }
            }
            if(toupdateList.size() > 0) {
                update toupdateList;
            }
        }   
    }   
    
}
