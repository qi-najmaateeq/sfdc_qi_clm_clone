/**
* This is Opportunity trigger handler class.
* version : 1.0
*/
public without sharing class DAOH_OWF_Opportunity {
    /**
    * This method is used to set Number of Potential regions based on selected Potential Regions.
    * @params  newList List<Opportunity>
    * @params  oldMap Map<Id, Opportunity>
    * @return  void
    */
    public static void setNoOfPotentialRegionsBasedOnPotentialRegions(List<Opportunity> newList, Map<Id, Opportunity> oldMap) {
        for(Opportunity opp : newList) {
            if(oldMap == NULL || (oldMap != NULL && opp.Potential_Regions__c != oldMap.get(opp.Id).Potential_Regions__c)) {
                if(String.IsNotBlank(opp.Potential_Regions__c)) {
                    opp.Number_of_Potential_Regions__c = opp.Potential_Regions__c.split(CON_OWF.SEMI_COLON).size();
                }else {
                    opp.Number_of_Potential_Regions__c = 0;
                }
            }
        }
    }
     /**
    * This method is used to add BDLeadSubRegion validation.
    * @params  newList List<Opportunity>
    * @params  oldMap Map<Id, Opportunity>
    * @return  void
    */
    public static void BDLeadSubRegionRequired(List<Opportunity> newList) {
        Mulesoft_Integration_Control__c mulesoftSetting = Mulesoft_Integration_Control__c.getInstance();
        if(!mulesoftSetting.Ignore_Validation_Rules__c){
            for(Opportunity opp : newList) {
                if(UTL_OWF.isOPPhasOWFAgreement(new Set<ID>{opp.id}) && opp.Potential_Regions__c =='Global' && opp.BD_Lead_Sub_Region__c == null)
                {
                    opp.adderror('Global Opportunities must include a BD Lead Sub Region. BD Lead Sub Region is found on the R&D Details tab.');
                }
            }
        }
    }    
    /**
    * This method is used to delete Agreement (Apttus__APTS_Agreement__c) record based on Opportunity.
    * @params  newList List<Opportunity>
    * @return  
    */
    public static void deleteAgreementBasedOnOpportunity(List<Opportunity> newList) {
        List<Apttus__APTS_Agreement__c> agreementToBeDeletedList = new List<Apttus__APTS_Agreement__c>();
        agreementToBeDeletedList = returnAgreementBasedOnOpportunity(newList);
        if(agreementToBeDeletedList.size() > 0) {
            QBL_OWF_DeletionLogic.QueueAgreementDeletion  queuableObject = new QBL_OWF_DeletionLogic.QueueAgreementDeletion (agreementToBeDeletedList);
            System.enqueueJob(queuableObject);
        }
    }
    
    /**
    * This method is used to create resource request based on agreement udpate
    * @params    newList List<Opportunity>
    * @params    oldMap Map<Id, Opportunity>
    * @return  
    */
    public static void createClinicalBidResRequestsOnOppUpdate(List<Opportunity> newList, Map<Id, Opportunity> oldMap) {
        Set<Id> agreementOppIdsSet = new set<Id>();
        for(Opportunity opp : newList) {
            if(opp.Line_of_Business__c != oldMap.get(opp.Id).Line_of_Business__c || opp.Potential_Regions__c != oldMap.get(opp.Id).Potential_Regions__c
                || opp.Is_Local_China_CRO__c != oldMap.get(opp.Id).Is_Local_China_CRO__c || opp.Is_RWI_Study__c != oldMap.get(opp.Id).Is_RWI_Study__c
                || opp.Intervention_Type__c != oldMap.get(opp.Id).Intervention_Type__c || opp.QI_Invited_to_Present__c != oldMap.get(opp.Id).QI_Invited_to_Present__c
                || opp.Bid_Defense_Date__c != oldMap.get(opp.Id).Bid_Defense_Date__c) {
                agreementOppIdsSet.add(opp.Id);
            }
        }
        
        if(agreementOppIdsSet.size() > 0) {
            if(UTL_OWF.isLoginUserhasPermissionControl())
            {
                String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ' +
                                    ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
            
                List<pse__Resource_Request__c> reqRequestsInsertList = new List<pse__Resource_Request__c>();
                reqRequestsInsertList = UTL_OWF.processResRequestForInitialBids(agreementOppIdsSet, agrCondition, null, null);
                if(reqRequestsInsertList.size() > 0) {
                    insert reqRequestsInsertList;
               }
            }
            else
            {
                for(Opportunity opp : newList) {
                    opp.adderror('You do not have the staffing permissions.');
                }
            }    
        }
    }
    
    private static List<Apttus__APTS_Agreement__c> returnAgreementBasedOnOpportunity(List<Opportunity> newList) {
        List<Apttus__APTS_Agreement__c> agreementsToBeReturnedList = new List<Apttus__APTS_Agreement__c>();
         Set<String> agreementFieldSet = new Set<String>{'Id'};
        Map<Id, Opportunity> opportunitytIdToOpportunityMap= new Map<Id, Opportunity>();
        for(Opportunity opportunity : newList)
        {
             opportunitytIdToOpportunityMap.put(opportunity.ID, opportunity );
        }
        
        Map<Id, Apttus__APTS_Agreement__c> agrIdToAgreementMap = new SLT_APTS_Agreement(false,false).getAgreementByOppID(opportunitytIdToOpportunityMap.keySet(),agreementFieldSet );
        agreementsToBeReturnedList  = agrIdToAgreementMap.values();
        return agreementsToBeReturnedList;
    }   
    
    /**
    * This method is used to update Complexity Score Total on RR.
    * @params  newList List<Opportunity>
    * @params  oldMap Map<Id, Opportunity>
    * @return  void
    */
    public static void updateComplexityScoreTotalOnRR(List<Opportunity> newList, Map<Id, Opportunity> oldMap) {
        Set<Id> rrAgreementIdSet = new Set<Id>();
        Set<Id> oppIdSet = new Set<Id>();
        Map<Id, Apttus__APTS_Agreement__c> agrIdToAgreementMap = new Map<Id, Apttus__APTS_Agreement__c>();
        Map<String,OWF_Resource_Setting__mdt> resSettingIdToResSettingMap = new Map<String,OWF_Resource_Setting__mdt>();        
        
        Set<String> orsFieldSet = new Set<String>{'Id','Sub_Group__c', 'Estimated_Fees__c','Is_there_a_Client_Bid_Grid__c','Number_of_Sites__c',
                                                   'Potential_Regions__c','Project_Ex_Number_of_Unique_Tables__c','Re_Bid_Complexity__c',
                                                    'Requested_Services__c','RFP_Ranking__c','Staffing_Number_of_Unique_Tables__c'};
        for(Opportunity opp : newList) {
             if(opp.Number_of_Potential_Regions__c != oldMap.get(opp.Id).Number_of_Potential_Regions__c
               || (opp.Potential_Regions__c != oldMap.get(opp.Id).Potential_Regions__c 
                   && opp.Potential_Regions__c == 'Global'))  {
                   
                      oppIdSet.add(opp.Id); 
             }                                        
        } 
        
        if(oppIdSet.size() > 0) {
            for(OWF_Resource_Setting__mdt rsCMT : new SLT_OWF_Resource_Setting(false,false).getOWFResourceSettingRecords(orsFieldSet)) {
                resSettingIdToResSettingMap.put(rsCMT.Sub_Group__c,rsCMT);                                          
            }
        
            Set<String> agrFieldSet = new Set<String>{'Id', 'Apttus__Related_Opportunity__c','Apttus__Related_Opportunity__r.Presentation_Date__c','Apttus__Related_Opportunity__r.Number_of_Potential_Regions__c',
                                                        'Apttus__Related_Opportunity__r.Potential_Regions__c', 'Name','RFP_Ranking__c','Number_of_Sites__c','Re_Bid_Complexity__c',
                                                        'Staffing_Number_of_Unique_Tables__c','Project_Ex_Number_of_Unique_Tables__c',
                                                      'Estimated_Fees__c','Is_there_a_Client_Bid_Grid__c','Number_of_Requested_Services__c', 'Bid_Due_Date__c','Apttus__Related_Opportunity__r.Bid_Defense_Date__c'};
            Set<String> resReqFieldSet = new Set<String>{'Id', 'pse__Opportunity__c', 'Agreement__c', 'Resource_Request_Type__c', 'Complexity_Score_Total__c', 'SubGroup__c',
                                                        'CreatedDate','Workload_Ranking__c','Suggested_FTE__c','pse__Start_Date__c',
                                                        'pse__Start_Date__c','pse__End_Date__c', 'Is_Bid_Defense__c'}; 
                                                        
            String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ';
            String resReqCondition = 'pse__Opportunity__c != NULL And Agreement__c != NULL';                                            
            agrIdToAgreementMap = new SLT_APTS_Agreement(false,false).getAgreementAndResReqsByOppId(oppIdSet, agrFieldSet, resReqFieldSet, agrCondition, resReqCondition);
            List<pse__Resource_Request__c> rrList = new List<pse__Resource_Request__c>();
            List<pse__Resource_Request__c> rrWithCSTotalList = new List<pse__Resource_Request__c>();
            List<pse__Resource_Request__c> toBeUpdatedRRList = new List<pse__Resource_Request__c>();
            for(Apttus__APTS_Agreement__c agr : agrIdToAgreementMap.values()) {  
                for(pse__Resource_Request__c rr: agr.Resource_Requests__r){
                    rrList.add(rr);
                }
            }
            rrWithCSTotalList = UTL_OWF.calculateComplexityScoreTotal(agrIdToAgreementMap,resSettingIdToResSettingMap,rrList);
            toBeUpdatedRRList = UTL_OWF.calculateWRandSuggestedFTE(agrIdToAgreementMap,rrWithCSTotalList);
            if(toBeUpdatedRRList.size() > 0) {
                Set<Id> rrIdsSet = new Set<Id>();
                List<pse__Resource_Request__c> resRequestUpdateList = new List<pse__Resource_Request__c>();
                for(pse__Resource_Request__c rr : toBeUpdatedRRList) {
                    if(!rrIdsSet.contains(rr.Id)) {
                        resRequestUpdateList.add(rr);
                    }
                }
                update resRequestUpdateList;
            }
        }
    }
}
