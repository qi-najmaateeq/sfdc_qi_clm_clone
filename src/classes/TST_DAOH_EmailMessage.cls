@isTest
private class TST_DAOH_EmailMessage {
    @testSetup
    static void dataSetup() {
    Account acct = new Account(Name = 'TestAcc',RDCategorization__c = 'Site');
        insert acct;
        
        Contact Con = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='briandent@trailhead.com',
            AccountId = acct.Id);
        insert Con;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=acct.Id,Type = 'TECHNO',
                                          BusinessHoursId = [select id from BusinessHours where Name = 'Default'].Id,
                                          StartDate=Date.valueof(System.now().addDays(-2)), 
                                          EndDate=Date.valueof(System.now().addYears(2)));
        insert ent;
        List<Queue_User_Relationship__c> queueList = new List<Queue_User_Relationship__c>();
        Queue_User_Relationship__c queues = new Queue_User_Relationship__c();
        queues.Name = 'Q1';
        queues.QueueName__c = 'Q1';
        queues.Type__c = 'Queue';
        queues.User__c = UserInfo.getUserId();
        queueList.add(queues);
        
        Queue_User_Relationship__c queueUser = new Queue_User_Relationship__c();
        queueUser.Name = 'Q1';
        queueUser.QueueName__c = 'Q1';
        queueUser.Type__c = 'User';
        queueUser.User__c = UserInfo.getUserId();
        queueList.add(queueUser); 
        insert queueList;
        
        Id RecordTypeIdCase = Schema.SObjectType.case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        Id RecordTypeIdCaseRD = Schema.SObjectType.case.getRecordTypeInfosByName().get('R&D - Activity Plan Case').getRecordTypeId();
        List<Case> cList = new List<Case>();
        Case c = new Case(
            Subject = 'TechnoCase 01',
            Description = 'Techno Case Description',
            AccountId = acct.Id
            , ContactId = con.Id
            , Origin = CON_CSM.S_EMAIL
            , Status = 'New'
            , AssignCaseToCurrentUser__c = false
            , InitialQueue__c = 'Q1'
            , OwnerId = UserInfo.getUserId()
            , RecordTypeId = RecordTypeIdCase
        );
        c.TaskMilestone__c = 'prodops';
        c.Status = CON_CSM.S_IN_PROGRESS;
        c.SlaStartDate = System.now();
            cList.add(c);
        Case cc = new Case(
            Subject = 'TechnoCaseClosed 02',
            Description = 'Techno CaseClosed Description',
            AccountId = acct.Id
            , ContactId = con.Id
            , Origin = CON_CSM.S_EMAIL
            , Status = 'Closed'
            , AssignCaseToCurrentUser__c = false
            , InitialQueue__c = 'Q1'
            , OwnerId = UserInfo.getUserId()
            , RecordTypeId = RecordTypeIdCase
        );
        cc.SlaStartDate = System.now();
            cList.add(cc);
            Case c1 = new Case(
                Subject = 'R&D - Activity Plan Case 01',
            Description = 'R&D AP Case Description',
                AccountId = acct.Id,
                ContactId = con.Id,
                Origin = 'Email',
                Status = 'Closed',
                InitialQueue__c = 'group name',
                RandD_Location__c = 'Dalian',
                OwnerId = UserInfo.getUserId(),
                EntitlementId = ent.Id,
                RecordTypeId = RecordTypeIdCaseRD
            );
        
        cList.add(c1);
        insert cList;
    }
    
    static testmethod void testPreventEmailMessageDel(){
        Case c1 = [SELECT Id, Status,template__c FROM Case WHERE Status = 'Closed' and Subject = 'R&D - Activity Plan Case 01' LIMIT 1];
            List<EmailMessage> newEmail = new List<EmailMessage>();
        newEmail.add(new EmailMessage(FromAddress = 'test@abc.org', Incoming = false, ToAddress= 'm.reddy@in.imshealth.com', Subject = 'Test email RD Out', TextBody = '23456 ', ParentId = c1.Id)); 
        
            
            
            try{
                Test.startTest();
                insert newEmail;
                EmailMessage em = [Select FromAddress,Incoming,ToAddress,Subject,TextBody,ParentId From EmailMessage where Incoming = false Limit 1];
                    em.TextBody = 'Updated Text body';
                    update em;
            }catch(DMLexception e){
                system.assert(e.getMessage().contains('Cannot delete Email with related to Case'),'Cannot delete Email with related to Case');                       
            }
            Test.stopTest();
    }
    
    static testmethod void testPreventEmailMessageRandDDelIN(){
        Case c1 = [SELECT Id, Status,template__c FROM Case WHERE Status = 'Closed' and Subject = 'R&D - Activity Plan Case 01' LIMIT 1];
        List<EmailMessage> newEmail = new List<EmailMessage>();
        newEmail.add(new EmailMessage(FromAddress = 'test@abc.org', Incoming = true, ToAddress= 'm.reddy@in.imshealth.com', Subject = 'Test email RD In', TextBody = '23456 ', ParentId = c1.Id)); 
            
            
            try{
                
                Test.startTest();
                insert newEmail;
                EmailMessage em = [Select FromAddress,Incoming,ToAddress,Subject,TextBody,ParentId From EmailMessage where Incoming = true Limit 1];
                
            }catch(DMLexception e){
            }
            Test.stopTest();
    }
    
    @isTest
    static void testMailSenderWhenEmailReceivedOnTechClosedCase(){
        Case c1 = [SELECT Id, Status,template__c FROM Case WHERE Status = 'Closed' and Subject = 'TechnoCaseClosed 02' LIMIT 1];
            List<EmailMessage> newEmail = new List<EmailMessage>();
        
        newEmail.add(new EmailMessage(FromAddress = 'shwetadxb@gmail.com', Incoming = true, ToAddress= 'shwetadxb@gmail.com', Subject = 'Test email Tech In', TextBody = '23456 ', ParentId = c1.Id)); 
        try{
             Test.startTest();
                insert newEmail;
            
            DAOH_EmailMessage.MailSenderWhenEmailReceivedOnTechClosedCase(newEmail);
        }
        catch(Exception ex){
            ex.getMessage();
        }
        Test.stopTest();
    }
    
    
    
    
}