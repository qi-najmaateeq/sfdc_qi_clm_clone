/*
* Version       : 1.0
* Description   : Test Class for SLT_Contract
*/
@isTest
private class TST_SLT_Contract {
    
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        insert opp;
        RecordType recordType = [SELECT Id, Name, DeveloperName From RecordType WHERE Id = :CON_CRM.CONTRACT_RECORD_TYPE_CNF_GBO LIMIT 1];
        IQVIA_Legal_Entity__c iqviaLegalEntity = new IQVIA_Legal_Entity__c(Name='Test');
        insert iqviaLegalEntity;
        Contract cntrt = UTL_TestData.createContract(opp, recordType.DeveloperName);
        cntrt.Name = 'Test Contract1';
        cntrt.Project_Number__c = '12';
        cntrt.Parent_Contract_Number__c = 1234;
        cntrt.Ultimate_Parent_Contract_Number__c = 5678;
        cntrt.Legal_Entity_IQVIA__c = iqviaLegalEntity.Id;
        cntrt.Legal_Entity_Customer__c = 'test';
        insert cntrt;
        recordType = [SELECT Id, Name, DeveloperName From RecordType WHERE Id = :CON_CRM.CONTRACT_RECORD_TYPE_CNF_GBO LIMIT 1];
        Contract cntrt2 = UTL_TestData.createContract(opp, recordType.DeveloperName);
        cntrt2.Parent_Contract__c = cntrt.Id;
        cntrt2.Name = 'Test Contract2';
        cntrt2.Project_Number__c = '1234';
        cntrt2.Change_Order_Number__c = '12';
        cntrt2.Legal_Entity_IQVIA__c = iqviaLegalEntity.Id;
        cntrt2.Legal_Entity_Customer__c = 'test';
        cntrt2.Parent_Contract_Number__c = 1234;
        cntrt2.Ultimate_Parent_Contract_Number__c = 5678;
        insert cntrt2;
    }
    
    @isTest
    static void testSelectByContractId() {
        Set<Id> contractIdSet = new Set<Id>();
        Set<String> fieldSet = new Set<String>{'Name', 'Id'};
        Contract contract = [SELECT Id FROM Contract WHERE Name = 'Test Contract2' LIMIT 1];
        contractIdSet.add(contract.Id);
        Test.startTest();
        Map<Id, Contract> contractMap = new SLT_Contract().selectByContractId(contractIdSet, fieldSet);
        Test.stopTest();
        System.assertEquals('Test Contract2', contractMap.get(contract.Id).Name);
    }
    
    @isTest
    static void testSelectByContractIdList() {
        Set<Id> contractIdSet = new Set<Id>();
        Set<String> fieldSet = new Set<String>{'Name', 'Id'};
        List<Contract> contractList = [SELECT Id FROM Contract];
        contractIdSet.add(contractList[0].Id);
        contractIdSet.add(contractList[1].Id);
        Test.startTest();
        contractList = new SLT_Contract().selectByContractIdList(contractIdSet, fieldSet);
        Test.stopTest();
        System.assertEquals(2, contractList.size());
    }    
    
    @isTest
    static void testSelectCNFContractsByProjAndChangeOrderNumber() {
        Set<String> projNumSet = new Set<String>{'1234'};
        Set<String> chanOrdSet = new Set<String>{'12'};
        Set<String> fieldSet = new Set<String>{'Name', 'Id'};
        Contract cntrt = [SELECT Id, Project_Number__c FROM Contract WHERE Name = 'Test Contract2' LIMIT 1];
        Test.startTest();
        Map<Id, Contract> contractMap = new SLT_Contract().selectCNFContractsByProjAndChangeOrderNumber(projNumSet, chanOrdSet, fieldSet);
        Test.stopTest();
        //System.assertEquals(cntrt.Id, contractMap.get(cntrt.Id).Id);
    }
    
    @isTest
    static void testFetchParentContract() {
        Set<Id> contractIdSet = new Set<Id>();
        Set<String> fieldSet = new Set<String>{'Name', 'Id'};
        Contract contract = [SELECT Id, Parent_Contract__c FROM Contract WHERE Name = 'Test Contract2' LIMIT 1];        
        contractIdSet.add(contract.Parent_Contract__c);
        contract = [SELECT Id FROM Contract WHERE Name = 'Test Contract1' LIMIT 1];
        Test.startTest();
        Map<Id, Contract> contractMap = new SLT_Contract().fetchParentContract(contractIdSet, fieldSet);
        Test.stopTest();
        System.assertEquals(contract.Id, contractMap.get(contract.Id).Id);
    }
    
    @isTest
    static void testFetchContract() {
        Set<Id> contractIdSet = new Set<Id>();
        Set<String> fieldSet = new Set<String>{'Name', 'Id'};
        List<Contract> contractList = [SELECT Id FROM Contract];
        contractIdSet.add(contractList[0].Id);
        contractIdSet.add(contractList[1].Id);
        Test.startTest();
        contractList = new SLT_Contract().fetchContract(contractIdSet, fieldSet);
        Test.stopTest();
        System.assertEquals(2, contractList.size());
    }
    
    @isTest
    static void testGetContractUsingContractNumber() {
        Set<String> fieldSet = new Set<String>{'Name', 'Id'};
        Contract contract = [SELECT Id, ContractNumber FROM Contract WHERE Name = 'Test Contract2' LIMIT 1];
        Test.startTest();
        List<Contract> contractList = new SLT_Contract().getContractUsingContractNumber(contract.ContractNumber, fieldSet);
        Test.stopTest(); 
        System.assertEquals(contract.Id, contractList[0].Id);
    }
    
}