public class DAO_Case extends fflib_SObjectDomain {

    /**
     * Constructor of this class
     * @params sObjectList List<Case>
     */
    public DAO_Case(List<Case> sObjectList) {
        super(sObjectList);
    }

    /**
     * Constructor Class for construct new Instance of This Class
     */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new DAO_Case(sObjectList);
        }
    }

    /**
     * This method is used for before insert of the case trigger.
     * @return void
     */
    public override void onBeforeInsert() {
        Profile profile = null;
        /** escape all method for creating the Case from Live Chat */
        if(CON_CSM.U_AUTOPROCESS_ID != UserInfo.getUserId()){
            /** CSM-88 moved  setContactTitle method to first for Techno Case Assignment */
            DAOH_Case.setContactTitle((List<Case>)Records);
            boolean isWeb2Case = DAOH_Case.web2caseCreation((List<Case>)Records);
            if(UserInfo.getProfileId() != null){
                profile = DAOH_Case.getCSMProfile(UserInfo.getProfileId());
            }
            if(profile != null && !CON_CSM.S_P_SYS_ADMIN.equalsIgnoreCase(profile.Name)){
                DAOH_Case.ValidationForAllPillar((List<Case>)Records,null);
            }
            DAOH_Case.createChildCaseAllPillar((List<Case>)Records);
            if(profile!= null && CON_PEP.S_P_PEP_COMMUNITY.equalsIgnoreCase(profile.Name)){
                
            }else if(!isWeb2Case && !DAOH_Case.createDataPillar((List<Case>)Records, (profile!= null && (CON_CSM.S_P_CSM_COMMUNITY.equalsIgnoreCase(profile.Name))) ? profile : null)){
                DAOH_Case.AP_Case((List<Case>)Records, (profile!= null && (CON_CSM.S_P_CSM_COMMUNITY.equalsIgnoreCase(profile.Name))) ? new Set<Id>{profile.Id} : new Set<Id>());
                
            }
            DAOH_Case.EntitlemebtAccAPCase01((List<Case>)Records,null);
            DAOH_Case.createCaseRelationfields((List<Case>)Records,null);
            DAOH_Case.tickStoppedCheckboxToPauseEntitlement((List<Case>)Records);
            DAOH_Case.checkFCRWhenCaseIsCreatedAsClosed((List<Case>)Records,null); 
            DAOH_Case.checkSpecialHandlingClient((List<Case>)Records,null);
            DAOH_Case.customEmailAddressesValidation((List<Case>)Records);
            DAOH_Case.setFieldForData((List<Case>)Records);
            DAOH_Case.setDescriptionFieldBlankForEmailToCase((List<Case>)Records);
            DAOH_Case.populateLOSForRnDPillar((List<Case>)Records);
            if(UserInfo.getName() != 'System' && profile != null && profile.Name != 'Automated Process' && profile.Name != CON_CSM.S_P_SYS_ADMIN && profile.Name != CON_CSM.S_P_CSM_COMMUNITY) {
                DAOH_Case.checkCaseTypeForDataCase((List<Case>)Records);
            }
            DAOH_Case.populateLocationForRAndD((List<Case>)Records, null);
        }
        
    }
   
    /**
     * This method is used for after insert of the case trigger.
     * @return void
     */
    public override void onAfterInsert() {
        /** escape all method for creating the Case from Live Chat */
        if(CON_CSM.U_AUTOPROCESS_ID != UserInfo.getUserId()){
            DAOH_Case.cloneEmailMessagefromParentforRDCase((List<Case>)Records);
            DAOH_Case.saveAuditLogAfterInsertCase((List<Case>)Records);
            DAOH_Case.sendCaseCCEmails((List<Case>)Records); 
            DAOH_Case.sendEmailtoOwnerDataPillar((List<Case>)Records);
            DAOH_Case.SetCalendarEvent((List<Case>)Records);
            DAOH_Case.addDescriptionAsCaseComment((List<Case>)Records);
            DAOH_Case.checkIfCaseIsAvaiableInOneKey((List<Case>)Records, null);
            DAOH_Case.createActivitesbyRnDTemplate((List<Case>)Records,null);
        }
    }
     /**
     * This method is used for after update of the case trigger.
     * @params  existingRecords Map<Id,SObject>
     * @return  void
     */
    public override void  onAfterUpdate(Map<Id,SObject> existingRecords) {
        /** escape all method for creating the Case from Live Chat */
        if(CON_CSM.U_AUTOPROCESS_ID != UserInfo.getUserId()){
            DAOH_Case.completeRDCaseMilestone((List<Case>) Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.completeCaseMilestone((List<Case>) Records,(Map<Id,Case>)existingRecords);
            List<FieldDefinition> fields = new EXT_CSM_FieldDefination().getFieldDetails(CON_CSM.s_case);
            if(fields != null){
                DAOH_Case.saveAuditLogAfterUpdateCaseFields((List<Case>)Records,(Map<Id,Case>)existingRecords,fields);
            }
            DAOH_Case.saveQueueCaseHistory((List<Case>)Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.updateCaseTaskQuickAction((List<Case>)Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.sendCaseCCEmails((List<Case>)Records);
            DAOH_Case.createActivitesbyRnDTemplate((List<Case>)Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.checkIfCaseIsAvaiableInOneKey((List<Case>)Records, (Map<Id,Case>)existingRecords);
        }
    }
    
    /**
     * This method is used for before update of the case trigger.
     * @params  existingRecords Map<Id,SObject>
     * @return  void
     */
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        /** escape all method for creating the Case from Live Chat */
        if(CON_CSM.U_AUTOPROCESS_ID != UserInfo.getUserId()){
            Profile profile = DAOH_Case.getCSMProfile(UserInfo.getProfileId());
            if(profile != null && !CON_CSM.S_P_SYS_ADMIN.equalsIgnoreCase(profile.Name)){
                DAOH_Case.ValidationForAllPillar((List<Case>)Records,(Map<Id,Case>)existingRecords);
                DAOH_Case.updateCaseValidationForAllPillar((List<Case>)Records,(Map<Id,Case>)existingRecords);
            }
            DAOH_Case.validQuickAction((List<Case>)Records, profile);
            DAOH_Case.AP_Case01((List<Case>)Records,(Map<Id,Case>)existingRecords, (profile!= null && (CON_CSM.S_P_CSM_COMMUNITY.equalsIgnoreCase(profile.Name) || CON_PEP.S_P_PEP_COMMUNITY.equalsIgnoreCase(profile.Name))) ? new Set<Id>{profile.Id} : new Set<Id>());
            DAOH_Case.EntitlemebtAccAPCase01((List<Case>)Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.updateCaseWithMilestone((List<Case>)Records);
            DAOH_Case.restrictClosedCaseModification((List<Case>)Records, profile);
            DAOH_Case.createCaseRelationfields((List<Case>)Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.SetCalendarEvent((List<Case>)Records);
            DAOH_Case.setFirstEscalationTimeField((List<Case>)Records);
            DAOH_Case.tickStoppedCheckboxToPauseEntitlement((List<Case>)Records);
            DAOH_Case.sendAutomatedFollowUp((List<Case>)Records);
            DAOH_Case.setFieldForData((List<Case>)Records);
            DAOH_Case.reOpenedClosedCase((List<Case>)Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.checkFCRWhenCaseIsClosedWithInOneHour((List<Case>)Records);
            DAOH_Case.unCheckFCRWhenCaseIsReopened((List<Case>)Records);
            DAOH_Case.checkFCRWhenCaseIsCreatedAsClosed((List<Case>)Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.checkSpecialHandlingClient((List<Case>)Records,(Map<Id,Case>)existingRecords);
            DAOH_Case.customEmailAddressesValidation((List<Case>)Records);
            DAOH_Case.sendRandDAutomatedFollowUpEmail((List<Case>)Records);
            DAOH_Case.updateOnekeyIdToDeleted((List<Case>)Records);
            if(UserInfo.getName() != 'System' && profile != null && profile.Name != 'Automated Process' && profile.Name != CON_CSM.S_P_SYS_ADMIN && profile.Name != CON_CSM.S_P_CSM_COMMUNITY) {
                DAOH_Case.checkCaseTypeForDataCase((List<Case>)Records);
            }
            DAOH_Case.populateLocationForRAndD((List<Case>)Records, (Map<Id,Case>)existingRecords);
        }else{
            DAOH_Case.setRecordtypetoIQVIALiveChat((List<Case>)Records);
            DAOH_Case.EntitlemebtAccAPCase01((List<Case>)Records,(Map<Id,Case>)existingRecords);
        }
    }
}
