public class CNT_PEP_SignSOW {
    
    @AuraEnabled
    public static String getrelatedContractId(String contractId){ 
        //Query Contract Fields
        Contract relatedcontract = [SELECT ID, Payment_method__c, SOW_status__c
                                    FROM Contract 
                                    WHERE ID =: contractId limit 1];
    	String paymentmethod;
        paymentmethod = relatedcontract.Payment_method__c + ';' + relatedcontract.SOW_status__c;
        
        return paymentmethod;
    }    
    
    @AuraEnabled
    public static String getContractId(String contractId, String signSOW_online){ 
        String templateId = '';
        Boolean signSOWonline = False;

        if(signSOW_online == 'True'){
            signSOWonline = True;
        }
        
        Set<Id> agencyProgramSetIds = new Set<Id>();
        List<Contract_Template__c> contractTemplateList = new List<Contract_Template__c>();
        
        
        //Query Contract Fields
        Contract relatedcontract = [SELECT ID, Product__c, AccountId, tolabel(Account.AccountCountry__c)
                                    FROM Contract 
                                    WHERE ID =: contractId limit 1];

        //Search the « Agency Program » with the same Product as the Contract
        IF(relatedcontract.Product__c != NULL){
            List<Agency_Program__c> agencyprogramList = [SELECT Id, Product__c
                                                         FROM Agency_Program__c
                                                         WHERE Product__c =: relatedcontract.Product__c];
            if(agencyprogramList.size() > 0){
                for(Agency_Program__c ag : agencyprogramList){
                    agencyProgramSetIds.add(ag.Id);
                }
            }
        }
        
        //Search the « Contract Template » where the Country = Account.Country OR Master = true
        //If a « Contract Template » exist with a corresponding country, use the template ID or else use the template ID of the Master 
        if(agencyProgramSetIds.size() > 0){
            contractTemplateList = [SELECT ID, Master__c, Country__c, TemplateID__c
                                    FROM Contract_Template__c
                                    WHERE Agency_Program__c IN: agencyProgramSetIds
                                    AND (Country__c =: relatedcontract.Account.AccountCountry__c) LIMIT 1];
            
            if(contractTemplateList.size() == 0){
                contractTemplateList = [SELECT ID, Master__c, Country__c, TemplateID__c
                                        FROM Contract_Template__c
                                        WHERE Agency_Program__c IN: agencyProgramSetIds
                                        AND Master__c = True LIMIT 1];
            }
        }
                
        if(contractTemplateList.size() > 0){
            templateId = contractTemplateList[0].TemplateID__c;
        }else{
            templateId = '';
        }
        
        String returnURL = '';
        
        //Use the template ID to construct the URL for the DocuSign signature  '1c78eee1-0897-4f1e-b4ee-418c2cf34f72'
        if(!Test.isRunningTest()){
        	returnURL = CNT_PEP_SendToDocusign.SendNow_WithRest(signSOWonline, contractId, templateId);
        }
        
        system.debug('returnURL-----SendNow_WithRest-------'+returnURL);        
        
        return returnURL;
    }
    
   /* @future(callout=true)
    public static void makeCalloutAsync(String contractId , String templateId)
    {
        //system.debug('CNT_PEP_SendToDocusign.SendNow------------'+contractId);
        //CNT_PEP_SendToDocusign.SendNow(contractId);
    }*/
}