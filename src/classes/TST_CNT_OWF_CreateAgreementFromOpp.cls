/**
 * This test class is used to test all methods in CNT_OWF_CreateAgreementFromOpp.
 * version : 1.0
 */
@isTest
private class TST_CNT_OWF_CreateAgreementFromOpp{

    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
       Account acc = UTL_OWF_TestData.createAccount();
      insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        agreement.Bid_Due_Date__c = system.today().addDays(5);
        insert agreement;
        pse__Proj__c bidProject = [Select id from pse__Proj__c where Agreement__c =: agreement.Id];
    }
    
    /**
     * This test method used for getEligibleRecordTypes
     */
    static testmethod void testGetEligibleRecordTypes() {
    
       Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
       List<String> oppFields = new List<String>{'Id','Name','StageName'};
       CNT_OWF_CreateAgreementFromOpp controller = new CNT_OWF_CreateAgreementFromOpp(new ApexPages.StandardController(oppty));
       CNT_OWF_CreateAgreementFromOpp.getEligibleRecordTypes(oppty.id,oppFields);
       
    }
    
    /**
     * This test method used for getAccountId
     */
    static testmethod void testGetAccountId() {
    
       Account acc= [SELECT Id FROM Account WHERE name = 'TestAccount'];
       Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
       List<String> oppFields = new List<String>{'Id','Name','StageName','Account.id'};
       CNT_OWF_CreateAgreementFromOpp controller = new CNT_OWF_CreateAgreementFromOpp(new ApexPages.StandardController(oppty));
       CNT_OWF_CreateAgreementFromOpp.getAccountId(oppty.id,oppFields);
       
    }
    
    /**
     * This test method used for prepopulate fields
     */
    static testmethod void testGetDefaultFieldValues() {
    
       Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
       Id recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Clinical_Short_Form').getRecordTypeId();
       CNT_OWF_CreateAgreementFromOpp controller = new CNT_OWF_CreateAgreementFromOpp(new ApexPages.StandardController(oppty));
       CNT_OWF_CreateAgreementFromOpp.validateOpportunity(oppty.id,recordTypeId);
       CNT_OWF_CreateAgreementFromOpp.getDefaultFieldValues(oppty.id,recordTypeId);
       CNT_OWF_CreateAgreementFromOpp.validateRecordType(recordTypeId);
       
    }
    
    /**
     * This test method used for prepopulate fields
     */
    static testmethod void testGetDefaultFieldValues2() {
    
       Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
       Account acc = [Select id from Account limit 1];
       Delete [Select id from Apttus__APTS_Agreement__c];
       Id recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Short_Form').getRecordTypeId();
       Apttus__APTS_Agreement__c agreement1 = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, oppty.Id, recordTypeId);
       agreement1.Bid_Due_Date__c = System.today().addDays(2);
       insert agreement1; 
       Apttus__APTS_Agreement__c agreement2 = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, oppty.Id, recordTypeId);
       insert agreement2; 
       CNT_OWF_CreateAgreementFromOpp controller = new CNT_OWF_CreateAgreementFromOpp(new ApexPages.StandardController(oppty));
       CNT_OWF_CreateAgreementFromOpp.validateOpportunity(oppty.id,recordTypeId);
       CNT_OWF_CreateAgreementFromOpp.getDefaultFieldValues(oppty.id,recordTypeId);
       CNT_OWF_CreateAgreementFromOpp.validateRecordType(recordTypeId);
    }
	
	/**
     * This test method used for prepopulate fields
     */
    static testmethod void testGetDefaultFieldValues3() {
    
       Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
       Account acc = [Select id from Account limit 1];
       Delete [Select id from Apttus__APTS_Agreement__c];
       Id recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('CSS_Short_Form').getRecordTypeId();
       Apttus__APTS_Agreement__c agreement1 = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, oppty.Id, recordTypeId);
       agreement1.Bid_Due_Date__c = System.today().addDays(2);
       insert agreement1; 
       Apttus__APTS_Agreement__c agreement2 = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, oppty.Id, recordTypeId);
       insert agreement2; 
       CNT_OWF_CreateAgreementFromOpp controller = new CNT_OWF_CreateAgreementFromOpp(new ApexPages.StandardController(oppty));
       CNT_OWF_CreateAgreementFromOpp.validateOpportunity(oppty.id,recordTypeId);
       CNT_OWF_CreateAgreementFromOpp.getDefaultFieldValues(oppty.id,recordTypeId);
       CNT_OWF_CreateAgreementFromOpp.validateRecordType(recordTypeId);
    }
    
    /**
     * This test method used for prepopulate fields
     */
    static testmethod void testGetRecordTypeName() {
      Id recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Clinical_Bid').getRecordTypeId();
      CNT_OWF_CreateAgreementFromOpp.getRecorTypeName(String.valueOf(recordTypeId));
       
      recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('RFI_Request').getRecordTypeId();
      CNT_OWF_CreateAgreementFromOpp.getRecorTypeName(String.valueOf(recordTypeId));
        
      recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Clinical_Short_Form').getRecordTypeId();
      CNT_OWF_CreateAgreementFromOpp.getRecorTypeName(String.valueOf(recordTypeId));
    }
}