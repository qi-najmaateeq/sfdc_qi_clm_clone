@isTest
public class AgreementLifecycleTest{

    @testsetup
    static void setupTestData(){
        
        Account accountObj = new Account(Name = 'Foundation 2.0 Test Account');
        insert accountObj;
        
        Opportunity opportunityObj = new Opportunity(
                                        AccountId = accountObj.Id,
                                        Name = 'HVS Loggedinas Tina 1',
                                        StageName = '1. Identifying Opportunity',
                                        Probability = 99.0,
                                        CloseDate = Date.newInstance(2018,10,24),
                                        Opportunity_Number__c = '2457855',
                                        Line_of_Business__c = 'Novella'
                                        );
        insert opportunityObj;
        
        RecordType cnf = [SELECT Id FROM RecordType WHERE Name = 'FDTN – CNF'];
        Apttus__APTS_Agreement__c parentAgreement = new Apttus__APTS_Agreement__c(
                                        Name = 'Parent Agreement',
                                        RecordTypeId = cnf.Id,
                                        Apttus__Account__c = accountObj.Id,
                                        Apttus__Agreement_Number__c = '00000002',
                                        Apttus__Related_Opportunity__c = opportunityObj.Id,
                                        Select_Pricing_Tool__c = CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP,
                                        Scenario_Number__c = 123.0,
                                        Scenario_Description__c = 'fsd',
                                        Mark_as_Primary__c = false,
                                        Agreement_Cloned_From__c = null,
                                        Is_Cloned__c = false);
        insert parentAgreement;
        
        opportunityObj.Agreement_for_This_Opportunity_Exists__c = false;
        update opportunityObj;
                
        RecordType co = [SELECT Id FROM RecordType WHERE Name = 'FDTN – Change Order'];
        
        Apttus__APTS_Agreement__c childAgreement = new Apttus__APTS_Agreement__c(
                                        Name = 'Parent Agreement',
                                        RecordTypeId = co.Id,
                                        Apttus__Account__c = accountObj.Id,
                                        Apttus__Agreement_Number__c = '00000001',
                                        Apttus__Related_Opportunity__c = opportunityObj.Id,
                                        Apttus__Requestor__c = UserInfo.getUserId(),
                                        Select_Agreement_Type__c = 'FDTN - Change Order',
                                        Select_Pricing_Tool__c = CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP,
                                        Scenario_Number__c = 123.0,
                                        Scenario_Description__c = 'fsd',
                                        Mark_as_Primary__c = false,
                                        Agreement_Cloned_From__c = parentAgreement.Id,
                                        Is_Cloned__c = true);
        insert childAgreement;

    }
    
    @isTest
    static void testAfterClone(){
        
        Apttus__APTS_Agreement__c parentAgrement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c WHERE Agreement_Cloned_From__c = NULL];
        Apttus__APTS_Agreement__c childAgreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c WHERE Agreement_Cloned_From__c <> NULL];
        
        AgreementLifecycle obj = new AgreementLifecycle();
        
        Test.startTest();
            obj.afterClone(parentAgrement, childAgreement);
        Test.stopTest();
        
        System.assertEquals(childAgreement.Agreement_Cloned_From__c, parentAgrement.Id, 
            'Agreement clone from field of ChildAgreement should be updated to ParentAgreement');
    }
    
    @isTest
    static void testAfterComplete(){
        
        Apttus__APTS_Agreement__c parentAgrement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c WHERE Agreement_Cloned_From__c = NULL];
        
        Attachment att = new Attachment();
        att.Name = 'Test Agreement';
        att.ParentId = parentAgrement.Id;
        att.Body = Blob.valueOf('test agreement.pdf');
        insert att;
        
        Apttus__APTS_Agreement__c childAgreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c WHERE Agreement_Cloned_From__c <> NULL];
        
        AgreementLifecycle obj = new AgreementLifecycle();
        
        Test.startTest();
            obj.afterComplete(childAgreement, Apttus.CustomClass.ActionType.ACTION_CLONE);
        Test.stopTest();
        
        List<Attachment> clonedAttachment = [SELECT Id, ParentId FROM Attachment WHERE ParentId=:childAgreement.Id];
        System.assertEquals(clonedAttachment.size()>0, true, 'Attachment cloned successfully');
    }
}