@isTest
private class TST_CNT_CSM_CaseHierarchy {
@testSetup
    static void dataSetup() {
        Account account = UTL_TestData.createAccount();
        insert account;
        Contact contact = UTL_TestData.createContact(account.Id);
        insert contact;

        Id recordType =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        
        Queue_User_Relationship__c queues=new Queue_User_Relationship__c();
        queues.Name ='Q1';
        queues.QueueName__c ='Q1';
        queues.Type__c ='Queue';
        queues.User__c = UserInfo.getUserId();
        insert queues;
        Case parentCase = New Case(Subject = 'Parent TestCase',RecordTypeId=recordType, ContactId = contact.Id, AccountId = account.Id, Status = 'New', Priority = 'Medium', Origin = 'Email',CurrentQueue__c=queues.Id,InitialQueue__c = 'Q1');
        insert parentCase;
        Case c = [SELECT id FROM Case WHERE Subject='Parent TestCase'];
        Case childCase = New Case(ParentId = c.Id,Subject = 'TestCase',RecordTypeId=recordType, ContactId = contact.Id, AccountId = account.Id, Status = 'New', Priority = 'Medium', Origin = 'Email',CurrentQueue__c=queues.Id,InitialQueue__c = 'Q1');
        insert childCase;
    }
    
    @IsTest
    static void testFindHierarchyData(){
        List<CNT_CSM_CaseHierarchy.HierarchyData> objects = new List<CNT_CSM_CaseHierarchy.HierarchyData>();  
        Case c = [SELECT id FROM Case WHERE Subject='TestCase'];
        String caseId = c.Id;
        Test.startTest();
        objects = CNT_CSM_CaseHierarchy.findHierarchyData(caseId);
        Test.stopTest();
        System.assert(objects.size() > 0);
    }
    
    @IsTest 
    static void testGetUltimateParentId(){
        Case c = [SELECT id FROM Case WHERE Subject='TestCase'];
        String caseId = c.Id;
        Test.startTest();
        String recId = CNT_CSM_CaseHierarchy.GetUltimateParentId(caseId);
        Test.stopTest();
        System.assert((recId != ''));
    }
}
