public class CNT_CPQ_QCSelfCheckDraft {

    @auraEnabled
    public static List<ProposalQASelfCheckListWrapper> fetchQCCheckListRecords(Id agreementId, String processStep) {

        String typeOfProcessStep = NULL;
        if(processStep == CON_CPQ.QC_SELF_CHECK_DRAFT || processStep == CON_CPQ.LINE_MANAGER_QC){
            typeOfProcessStep = CON_CPQ.DRAFT;
        }else{
            typeOfProcessStep = CON_CPQ.TYPE_FINAL;
        }
        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_OPPORTUNITY_TYPE, CON_CPQ.RECORDTYPE_NAME};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId}, fieldSet);
        List<ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList = new List<ProposalQASelfCheckListWrapper>();
        if(agreementList.size() > 0){

            String typeOfBids = NULL;
            if(agreementList[0].RecordType.Name == CON_CPQ.AGREEMENT_FDTN_REBID){
                typeOfBids = CON_CPQ.REBID;
            }else{
                typeOfBids = agreementList[0].Opportunity_Type__c;
            }
            Set<String> propsalFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT, CON_CPQ.TYPE, CON_CPQ.QUESTION, CON_CPQ.GUIDELINES, 
                CON_CPQ.PD_SELD_REVIEW, CON_CPQ.PD_COMMENT, CON_CPQ.LINE_MANAGER_QC_COMMENT, CON_CPQ.PROPOSAL_QC_COMMNET, CON_CPQ.TYPE_OF_PROPOSAL, 
                CON_CPQ.TYPE_OF_BIDS};
            List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = 
                new SLT_ProposalQASelfCheckList().getProposalCheckListForProcessStepAndBidTypeOfAgreement(new Set<Id>{agreementId}, propsalFieldSet, 
                typeOfProcessStep, typeOfBids);
            if(proposalQASelfCheckList.size() == 0){

                Set<String> qcFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.RECORDTYPE, CON_CPQ.TYPE, CON_CPQ.QUESTION, CON_CPQ.GUIDELINES, 
                    CON_CPQ.TYPE_OF_PROPOSAL, CON_CPQ.TYPE_OF_BIDS};
                List<QC_Check_List_Item__c> qcSelfCheckDraftList = 
                    new SLT_QCCheckListItem().getQCCheckListItemByProcessStepBidTypeRecordType(CON_CPQ.INITIAL_BID_REBID, qcFieldSet, typeOfProcessStep, typeOfBids);

                for(QC_Check_List_Item__c qcRecord : qcSelfCheckDraftList){
                    ProposalQASelfCheckListWrapper  wrapperObj = new ProposalQASelfCheckListWrapper(new Proposal_QA_Self_Check_List__c(
                        Agreement__c = agreementId, Type__c = qcRecord.Type__c, Question__c = qcRecord.Question__c, 
                        Guidelines__c = qcRecord.Guidelines__c), false, proposalQASelfCheckListWrapperList.size());
                    proposalQASelfCheckListWrapperList.add(wrapperObj);
                }
            }else{
                for(Proposal_QA_Self_Check_List__c qcCheck : proposalQASelfCheckList){
                    ProposalQASelfCheckListWrapper  wrapperObj = new ProposalQASelfCheckListWrapper(qcCheck, false, proposalQASelfCheckListWrapperList.size());
                    proposalQASelfCheckListWrapperList.add(wrapperObj);
                }
            }
        }
        return ProposalQASelfCheckListWrapperList;
    }
        
    @auraEnabled
    public static void saveProposalQASelfCheckList(String proposalQASelfCheckListWrapperListString, Id agreementId, String processStep, 
        Boolean isSaveAndComplete, String overAllComments){
    
        String typeOfProcessStep = NULL;
        if(processStep == CON_CPQ.QC_SELF_CHECK_DRAFT|| processStep == CON_CPQ.LINE_MANAGER_QC){
            typeOfProcessStep = CON_CPQ.DRAFT;
        }else{
            typeOfProcessStep = CON_CPQ.TYPE_FINAL;
        }

        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_OPPORTUNITY_TYPE, CON_CPQ.AGREEMENT_OPPORTUNITY_REGION, CON_CPQ.OWNER, CON_CPQ.RECORDTYPE_NAME};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId}, fieldSet);
        if(agreementList.size() > 0){
            List<ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList = 
                (List<ProposalQASelfCheckListWrapper>)JSON.deserialize(proposalQASelfCheckListWrapperListString,List<ProposalQASelfCheckListWrapper>.class);
            List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = new List<Proposal_QA_Self_Check_List__c>();
            List<QC_Check_List_Item__c> qcSelfCheckDraftList = new List<QC_Check_List_Item__c>();
            String typeOfBids = NULL;
            if(agreementList[0].RecordType.Name == CON_CPQ.AGREEMENT_FDTN_REBID){
                typeOfBids = CON_CPQ.REBID;
            }else{
                typeOfBids = agreementList[0].Opportunity_Type__c;
            }

            if(proposalQASelfCheckListWrapperList.size() > 0){

                for(ProposalQASelfCheckListWrapper proposalCheck : proposalQASelfCheckListWrapperList){
                    proposalCheck.proposalQASelfCheckList.Type_Of_Bids__c = typeOfBids;
                    proposalCheck.proposalQASelfCheckList.Type_Of_Process_Step__c = typeOfProcessStep;
                    proposalQASelfCheckList.add(proposalCheck.proposalQASelfCheckList);
                    if(proposalCheck.isNew && agreementList.size() > 0){
                        qcSelfCheckDraftList.add(new QC_Check_List_Item__c(Type__c = proposalCheck.proposalQASelfCheckList.Type__c,
                            Question__c = proposalCheck.proposalQASelfCheckList.Question__c, Guidelines__c = proposalCheck.proposalQASelfCheckList.Guidelines__c,
                            Type_Of_Process_Step__c = typeOfProcessStep, Type_Of_Bids__c = agreementList[0].Opportunity_Type__c));
                    }
                }
                if(!String.isBlank(overAllComments)){
                    Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(Id = agreementId);
                    if(processStep == CON_CPQ.LINE_MANAGER_QC)
                        agreement.Customer_Opportunity_Background__c = overAllComments;
                    else if(processStep == CON_CPQ.FINAL_QC)
                        agreement.Agreement_Purpose_List__c = overAllComments;

                    update agreement;
                }

                if(proposalQASelfCheckList.size() > 0)
                    upsert proposalQASelfCheckList;
                if(processStep == CON_CPQ.LINE_MANAGER_QC && !String.isBlank(agreementList[0].OwnerId) && isSaveAndComplete){

                    Set<String> userFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.MANAGERID};
                    List<User> lineManger = new SLT_User().selectActiveUserByUserId(new Set<Id>{agreementList[0].OwnerId}, userFieldSet);
                    if(lineManger.size() > 0){
                        if(!String.isBlank(lineManger[0].ManagerId)){
                            Set<String> taskFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.STATUS};
                            List<Task> taskList = new SLT_Task().selectNonCompletedTaskByWhatIdAndOwnerId(taskFieldSet, agreementList[0].Id, lineManger[0].ManagerId);
                            if(taskList.size() > 0){
                                taskList[0].Status = CON_CPQ.COMPLETED;
                                Update taskList;
                            }
                        }
                    }
                }
            }
        }
    }

    @auraEnabled
    public static String fetchOverAllComments(Id agreementId, String processStep){
        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_LINE_MANAGER_OVERALL_COMMENT,
            CON_CPQ.AGREEMENT_FINAL_QC_OVERALL_COMMENT};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId}, fieldSet);
        if(agreementList.size() > 0){
            if(processStep == CON_CPQ.LINE_MANAGER_QC)
                return agreementList[0].Customer_Opportunity_Background__c;
            else if(processStep == CON_CPQ.FINAL_QC)
                return agreementList[0].Agreement_Purpose_List__c;
        }
        return null;
    }

    @auraEnabled
    public static List<ProposalQASelfCheckListWrapper> addMoreItemInList(String proposalQASelfCheckListWrapperListString, String agreementId){

        List<ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList = 
            (List<ProposalQASelfCheckListWrapper>)JSON.deserialize(proposalQASelfCheckListWrapperListString,List<ProposalQASelfCheckListWrapper>.class);
        ProposalQASelfCheckListWrapper  wrapperObj = 
            new ProposalQASelfCheckListWrapper(new Proposal_QA_Self_Check_List__c(Agreement__c = agreementId), true, proposalQASelfCheckListWrapperList.size());
        proposalQASelfCheckListWrapperList.add(wrapperObj);
        return proposalQASelfCheckListWrapperList;
    }


    public class ProposalQASelfCheckListWrapper{
        @AuraEnabled public Proposal_QA_Self_Check_List__c proposalQASelfCheckList{get;set;}
        @AuraEnabled public Boolean isNew{get;set;}
        @AuraEnabled public Integer index{get;set;}
        public ProposalQASelfCheckListWrapper(Proposal_QA_Self_Check_List__c proposalQASelfCheckList, Boolean isNew, Integer index){
            this.proposalQASelfCheckList = proposalQASelfCheckList;
            this.isNew = isNew;
            this.index = index;
        }
    }
}