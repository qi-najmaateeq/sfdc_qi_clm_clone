/*  ===============================================================================
        Created:        Shweta Alwani
        Date:           18/03/2019
        Description:    To Get the contract details of the agreement on basis of contract number.
        ===============================================================================
    */
public class CNT_CPQ_ContractDetailsOnAgreement {
    
    public CNT_CPQ_ContractDetailsOnAgreement(ApexPages.StandardController controller){
        
    }
    public Contract getContract(){

        String agreementId = Apexpages.Currentpage().getparameters().get(CON_CPQ.Id); 
        Set<Id> recordtypeIds = new Set<Id>{
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER).getRecordTypeId(),
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CNF).getRecordTypeId(),
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CONTRACT).getRecordTypeId()
        };

        if(recordtypeIds.size() > 0){
            Set<String> fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.AGREEMENT_CONTRACT_NUMBER};
            List<Apttus__APTS_Agreement__c> agreements = new SLT_Agreement().getAgreementDetailsByIdAndRecorType(agreementId,
                                                            recordtypeIds, fieldSet);
            if(agreements.size() > 0 && !String.isBlank(agreements[0].Contract_Number__c)){

                fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.NAME, CON_CPQ.CONTRACT_CONTRACTNUMBER, CON_CPQ.PARENT_CONTRACT,
                    CON_CPQ.PARENT_CONTRACT_NUMBER, CON_CPQ.ULTIMATE_PARENT_CONTRACT_NUMBER, CON_CPQ.ACCOUNT_ID, CON_CPQ.LEGAL_ENTITY_CUSTOMER,
                    CON_CPQ.LEGAL_ENTITY_QUINTILES, CON_CPQ.OPPORTUNITY_NUMBER, CON_CPQ.OPPORTUNITY_NAME, CON_CPQ.OPPORTUNITY_LINK,
                    CON_CPQ.OTHER_OPPORTUNITY, CON_CPQ.PROJECT_NUMBER, CON_CPQ.COST_POINT_PROJECT_CODE, CON_CPQ.PROTOCOL_NUMBER,
                    CON_CPQ.DRUG_PRODUCT_NAME, CON_CPQ.THERAPY_AREA, CON_CPQ.GLOBAL_PROJECT_UNIT, CON_CPQ.DELIVERY_UNIT,CON_CPQ.DATA_TRANSFER_AGREEMENT_INCLUDED,
                    CON_CPQ.NEGOTIATING_OFFICE, CON_CPQ.DATE_TRACKING_STARTED, CON_CPQ.Account_Name, CON_CPQ.RELATED_PARENT_CONTRACT_NUMBER,
                    CON_CPQ.SPECIFIC_CONTRACT_TYPE, CON_CPQ.SEQUENTIAL_REFERENCE, CON_CPQ.LEAD_BUSINESS_GROUP, CON_CPQ.DIVISION_BUSINESS_UNIT,
                    CON_CPQ.IS_THIS_CONTRACT_A_BALLPARK, CON_CPQ.RECORDTYPE_NAME, CON_CPQ.OWNER_NAME, CON_CPQ.CONTRACT_RANKING,
                    CON_CPQ.IS_THE_CNF_ACCEPTED, CON_CPQ.CHNAGE_ORDER_NUMBER, CON_CPQ.CONFIDENCE_IN_APPROVAL_OF_BUDGET_DRAFT,
                    CON_CPQ.OWNER, CON_CPQ.CLIENT_THIRD_PARTY_AGREEMENT, CON_CPQ.EXECUTION_PLAN_START_DATE, CON_CPQ.TRIAGED_TO_ANALYST,
                    CON_CPQ.ANALYST_START_DATE, CON_CPQ.START_1ST_DRAFT_BUDGET, CON_CPQ.SEND_DRAFT_BUDGET_TO_INTERNAL_TEAM, CON_CPQ.RECEIVE_DRAFT_BUDGET_FROM_INTERNAL_TEAM,
                    CON_CPQ.EXPECTED_INTERNAL_BUDGET_APPROVAL, CON_CPQ.SEND_BUDGET_TO_CUSTOMER_FOR_REVIEW, CON_CPQ.RECEIVE_CUSTOMER_BUDGET_COMMENTS, CON_CPQ.RECEIVE_BUDGET_APPROVAL_BY_CUSTOMER,
                    CON_CPQ.START_1ST_DRAFT_CONTRACT, CON_CPQ.SEND_DRAFT_CONTRACT_TO_INTERNAL_TEAM, CON_CPQ.RECEIVE_DRAFT_CONTRACT_FROM_INTERNAL, CON_CPQ.EXPECTED_INTERNAL_CONTRACT_APPROVAL,
                    CON_CPQ.UPDATE_CONTRACT_STATUS, CON_CPQ.STARTDATE, CON_CPQ.RECEIVE_CONTRACT_APPROVAL_BY_CUSTOMER, CON_CPQ.EXPIRATION_TYPE,
                    CON_CPQ.ENDDATE, CON_CPQ.OWNER_EXPIRATION_NOTICE, CON_CPQ.ORIGINAL_SIGNED_AGREEMENT_LOCATION, CON_CPQ.DATE_EXECUTED_SIGNED_CONTRACT_RECEIVED,
                    CON_CPQ.STATUS_COMMENTS, CON_CPQ.CONTRACTS_ASSISTANCE_COMMENT, CON_CPQ.COMPANY_SIGNED_BY, CON_CPQ.OTHER_COMPANY_SIGNED_BY,
                    CON_CPQ.COMPANY_SIGNED_DATE, CON_CPQ.CUSTOMER_SIGNED_BY, CON_CPQ.OTHER_CUSTOMER_SIGNED_BY, CON_CPQ.CUSTOMER_SIGNED_TITLE, CON_CPQ.CUSTOMER_SIGNED_DATE, 
                    CON_CPQ.TRIAGE_COMMENTS, CON_CPQ.COMPANY_SIGNED_BY_Name, CON_CPQ.CONTRACT_EXECUTION_DATE_ACTUAL_EXPECTED, CON_CPQ.UNSIGNED_STATUS,
                    CON_CPQ.UNSIGNED_STATUS_CHANGES_DATE, CON_CPQ.UNSIGNED_COMMENTS, CON_CPQ.PROB_TO_EXECUTE_IN_THE_QTR_MGRS_ONLY,
                    CON_CPQ.CUSTOMER_SIGNED_BY_Name, CON_CPQ.SEND_CONTRACT_TO_CUSTOMER, CON_CPQ.RECEIVE_CUSTOMER_CONTRACT_COMMENTS, 
                    CON_CPQ.PLANNED_EXECUTION_DATE};
                List<Contract> contracts = new SLT_Contract().getContractUsingContractNumber(
                                                    agreements[0].Contract_Number__c, fieldSet);
                if(contracts.size() > 0)
                {
                    return contracts[0];
                }	
                else 
                    return new Contract();
            }
        }
        return new Contract();
    }    
}