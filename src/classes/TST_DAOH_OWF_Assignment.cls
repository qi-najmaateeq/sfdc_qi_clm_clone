/**
* This test class is used to test all methods in DAOH_OWF_Assignment Class.
* version : 1.0 
*/
@isTest
private class TST_DAOH_OWF_Assignment {
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        cont.FirstName = UserInfo.getFirstName();
        cont.LastName = UserInfo.getLastName();
        cont.sub_group__c = 'TSL-Japan';
        cont.available_for_triage_flag__c = true;

        insert cont;
        
        
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        Apttus__APTS_Agreement__c agreement =  UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        agreement.Bid_Number__c = 0;
        insert agreement;
        
        pse__Proj__c bidProject = [Select id from pse__Proj__c  where Agreement__c =: agreement.Id];
        pse__Schedule__c schedule = UTL_OWF_TestData.createSchedule();
        insert schedule;
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest.pse__Group__c = grp.Id;
        //resourceRequest.SubGroup__c =  'CP&A-CA&S';
        

        insert resourceRequest;
        pse__Assignment__c assignement =  UTL_OWF_TestData.createAssignment(agreement.Id, bidProject.Id, schedule.Id, cont.Id, resourceRequest.Id);
        
        insert assignement;
        pse__Skill__c skill = UTL_OWF_TestData.createSkills('Test Skill',CON_OWF.SKILL_TYPE_INDICATION);
        insert skill;
        pse__Resource_Skill_Request__c resourceSkillRequest = UTL_OWF_TestData.createResourceSkillRequest(skill.Id,resourceRequest.Id);
        insert resourceSkillRequest;
        
    }
    
    /**
     * This test method used for createSkillCertRatingsOnAssignCompleted method
     */
    static testmethod void testCreateSkillCertRatingsOnAssignCompleted() {
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'OWF_Triage_Manager'];
        List<PermissionSetAssignment> psa = [Select id from PermissionSetAssignment where AssigneeId =:UserInfo.getUserId() AND PermissionSetId =:ps.Id];
        if(psa.size() == 0)
            insert new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = ps.Id );
        
        pse__Assignment__c assignement = [Select id, pse__Status__c from pse__Assignment__c limit 1];
        assignement.pse__Status__c = CON_OWF.OWF_STATUS_COMPLETED;
        pse__Skill__c skill = [Select id from pse__Skill__c];
        Contact cont = [Select id from Contact];
        pse__Skill_Certification_Rating__c skillCertRating = UTL_OWF_TestData.createSkillCertificationRating(skill.Id,cont.Id);
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        system.runAs(usr){
            Test.startTest();
            insert skillCertRating;
            update assignement;
            system.assertEquals(1,[Select id from pse__Skill_Certification_Rating__c].size());
            delete skillCertRating;
            assignement.pse__Status__c = CON_OWF.OWF_STATUS_ACCEPTED;
            update assignement;
            assignement.pse__Status__c = CON_OWF.OWF_STATUS_COMPLETED;
            update assignement;
            system.assertEquals(1,[Select id from pse__Skill_Certification_Rating__c].size());
            Test.stopTest();
        }
            
        
    }
    
    /**
     * This test method used for PopulateRollupAssignmentFieldsOnContact method
     */
    static testmethod void testPopulateRollupAssignmentFieldsOnContact() {
        pse__Assignment__c assignement = [Select id, pse__Resource__c, pse__Status__c from pse__Assignment__c limit 1];
        Test.startTest();
            Contact resource = [Select id, COUNT_Assignemnts_Pending__c, COUNT_Assignemnts_Accepted__c from Contact where Id =: assignement.pse__Resource__c limit 1];
            system.assertEquals(1,resource.COUNT_Assignemnts_Pending__c );
            system.assertEquals(0,resource.COUNT_Assignemnts_Accepted__c );
            assignement.pse__Status__c = CON_OWF.OWF_STATUS_ACCEPTED;
            update assignement;
            resource = [Select id, COUNT_Assignemnts_Pending__c, COUNT_Assignemnts_Accepted__c from Contact where Id =: assignement.pse__Resource__c limit 1];
            system.assertEquals(0,resource.COUNT_Assignemnts_Pending__c );
            system.assertEquals(1,resource.COUNT_Assignemnts_Accepted__c );
            delete assignement;
            resource = [Select id, COUNT_Assignemnts_Pending__c, COUNT_Assignemnts_Accepted__c from Contact where Id =: assignement.pse__Resource__c limit 1];
            system.assertEquals(0,resource.COUNT_Assignemnts_Pending__c );
            system.assertEquals(0,resource.COUNT_Assignemnts_Accepted__c );
        Test.stopTest();
    }
    
    /**
     * This test method used for updateFieldsWhenStatusAndResourceChanged method
     */
    static testmethod void testUpdateFieldsWhenStatusAndResourceChanged() {
        pse__Assignment__c assignement = [Select id, pse__Resource__c, pse__Status__c from pse__Assignment__c limit 1];
        Test.startTest();
            Contact resource = [Select id, COUNT_Assignemnts_Pending__c, COUNT_Assignemnts_Accepted__c from Contact where Id =: assignement.pse__Resource__c limit 1];
            system.assertEquals(1,resource.COUNT_Assignemnts_Pending__c );
            assignement.pse__Status__c = CON_OWF.OWF_STATUS_ACCEPTED;
            update assignement;
            
            pse__Assignment__c modifiedAssignement = [Select id, Accepted_Date__c from pse__Assignment__c Where Id = :assignement.Id];
            system.assert(modifiedAssignement.Accepted_Date__c != NULL, true);
            
            assignement.pse__Status__c = CON_OWF.OWF_STATUS_REJECTED;
            assignement.Rejected_Reason__c = 'assignment rejected';
            update assignement;
            modifiedAssignement = [Select id, Rejected_Date__c from pse__Assignment__c Where Id = :assignement.Id];
            system.assert(modifiedAssignement.Rejected_Date__c != NULL, true);
        Test.stopTest();
    }
}