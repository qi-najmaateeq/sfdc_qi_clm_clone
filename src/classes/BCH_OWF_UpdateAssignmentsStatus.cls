/**
 * This is Batch class to udpate status on Assignments based on related Agreement's Bid_Due_Date field
 * version : 1.0
 */
global class BCH_OWF_UpdateAssignmentsStatus implements Database.Batchable<sObject>, Database.Stateful {
    @TestVisible Map<Id, String> assignmentIdToErrorMessageMap;
    
    /**
     * Constructor
     */
    public BCH_OWF_UpdateAssignmentsStatus() {
        assignmentIdToErrorMessageMap = new Map<Id, String>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
         String query = 'Select Id, pse__Status__c ' + 
                        ' From pse__Assignment__c ' + 
                        ' Where pse__Resource_Request__c != NULL And pse__Resource_Request__r.pse__Project__c != NULL And Agreement__c != NULL ' +
                        ' And RecordType.DeveloperName = \''+ CON_OWF.OWF_ASSIGNMENT_RECORD_TYPE_NAME + '\'' +
                        //' And Agreement__r.Bid_Due_Date__c = TODAY And pse__Status__c != \''+ CON_OWF.OWF_STATUS_COMPLETED + '\'';
                        'And Agreement__r.Bid_Due_Date__c <= TODAY And pse__Status__c = \''+ CON_OWF.OWF_STATUS_ACCEPTED + '\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<pse__Assignment__c> assignments) {
        List<pse__Assignment__c> assignmentUpdateList = new List<pse__Assignment__c>();
        
        for(pse__Assignment__c assignment : assignments){
            assignment.pse__Status__c = CON_OWF.OWF_STATUS_COMPLETED;
            assignmentUpdateList.add(assignment);
        }
        
        if (assignmentUpdateList.size() > 0){
            Database.SaveResult[] results = Database.Update(assignmentUpdateList, false);
            if (results.size() > 0){
                for (Integer i=0; i< results.size(); i++){
                    if (!results[i].isSuccess()){
                        assignmentIdToErrorMessageMap.put(assignmentUpdateList[i].Id, 'Error in updating assignment status : ' + results[i].getErrors()[0].getMessage());
                    }
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if (assignmentIdToErrorMessageMap.size() > 0 || Test.isRunningTest()){
            UTL_OWF.sendMailOnException(CON_OWF.BCH_OWF_UPDATEASSIGNMENTSSTATUS, assignmentIdToErrorMessageMap, 'Errors occurred during BCH_OWF_UpdateAssignmentsStatus batch process.');
        }
    }
}