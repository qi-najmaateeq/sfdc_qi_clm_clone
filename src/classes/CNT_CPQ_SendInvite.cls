public class CNT_CPQ_SendInvite {

    @AuraEnabled
    public static void sendInviteEmail(String attendeesEmail, String subject, String bodyData, DateTime fromDate, DateTime toDate) {
        if(!String.isBlank(attendeesEmail)) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> emails = attendeesEmail.split(',');
            mail.setToAddresses(emails);
            mail.setSubject(subject);
            mail.setHtmlBody(bodyData);
            mail.setPlainTextBody('');
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            attach.filename = 'meeting.ics';
            attach.ContentType ='text/calendar; charset=utf-8; method=REQUEST';
            attach.body = invite(fromDate, toDate);
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});
            Messaging.SendEmailResult[] er = Messaging.sendEmail(new Messaging.Email[] {mail});
        }
    }


    private static Blob invite(DateTime fromDate, DateTime toDate) { 
        Integer offset = UserInfo.getTimezone().getOffset(fromDate);
        fromDate = fromDate.addSeconds(-offset/1000);
        String formattedFromDt = fromDate.format('yyyyMMdd\'T\'hhmmss\'Z\'');
        
        Integer offset1 = UserInfo.getTimezone().getOffset(toDate);
        toDate = toDate.addSeconds(offset1/1000);
        String formattedToDt = toDate.format('yyyyMMdd\'T\'hhmmss\'Z\'');
        String txtInvite = '';
 
        txtInvite += 'BEGIN:VCALENDAR\n';
        txtInvite += 'PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\n';
        txtInvite += 'VERSION:2.0\n';
        txtInvite += 'METHOD:REQUEST\n';
        txtInvite += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n';
        txtInvite += 'BEGIN:VEVENT\n';
        txtInvite += 'CLASS:PUBLIC\n';
        txtInvite += 'DTEND:'+formattedToDt+'\n';
        txtInvite += 'DTSTART:'+formattedFromDt+'\n';
        txtInvite += 'LOCATION:Online\n';
        txtInvite += 'PRIORITY:5\n';
        txtInvite += 'SEQUENCE:0\n';
        txtInvite += 'SUMMARY;';
        txtInvite += 'LANGUAGE=en-us:Meeting\n';
        txtInvite += 'TRANSP:OPAQUE\n';
        txtInvite += 'UID:4036587160834EA4AE7848CBD028D1D200000000000000000000000000000000\n';
        txtInvite += 'X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"><HTML><HEAD><META NAME="Generator" CONTENT="MS Exchange Server version 08.00.0681.000"><TITLE></TITLE></HEAD><BODY><!-- Converted from text/plain format --></BODY></HTML>\n';
        txtInvite += 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n';
        txtInvite += 'X-MICROSOFT-CDO-IMPORTANCE:1\n';
        txtInvite += 'END:VEVENT\n';
        txtInvite += 'END:VCALENDAR';
 
        return Blob.valueOf(txtInvite);
    }
    
    @AuraEnabled
    public static List<String> getEmailTemplate(String recordId) {
        List<EmailTemplate> emailTemplate = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(
                    CON_CPQ.SEND_MAIL_FOR_CHALLENGE_CALL_INVITE, new Set<String>{CON_CPQ.Id});
        List<String> emailTemplateValues = new List<String>();
        if(emailTemplate.size() > 0){
            Messaging.SingleEmailMessage mailTemp = Messaging.renderStoredEmailTemplate(
                emailTemplate[0].Id, UserInfo.getUserId(), recordId);
            emailTemplateValues.add(mailTemp.getSubject());
            emailTemplateValues.add(mailTemp.getHtmlbody());
        }
        return emailTemplateValues;
    }
}