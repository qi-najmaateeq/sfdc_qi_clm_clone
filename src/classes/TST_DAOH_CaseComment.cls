@isTest
private class TST_DAOH_CaseComment {
    
    static testmethod void testupdateStatusforLogAWorkNoteonCase(){
        Account acct = new Account(
            Name = 'TestAcc',
            RDCategorization__c = 'Site');
        insert acct;
        
        Contact Con = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='briandent@trailhead.com',
            AccountId = acct.Id);
        insert Con;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=acct.Id,Type = 'TECHNO',
                                          BusinessHoursId = [select id from BusinessHours where Name = 'Default'].Id,
                                          StartDate=Date.valueof(System.now().addDays(-2)), 
                                          EndDate=Date.valueof(System.now().addYears(2)));
        insert ent;
        
        
        User u = [Select id from User where Id = :UserInfo.getUserId() and ProfileId = :UserInfo.getProfileId()];
        
        system.runAs(u) {
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            GroupMember grpUser = new GroupMember (
                UserOrGroupId = u.Id,
                GroupId = g1.Id);
            
            insert grpUser;
            
            Queue_User_Relationship__c qur = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                Type__c = 'Queue',
                Group_Id__c = grpUser.groupId);
            insert qur;
            
            Queue_User_Relationship__c qurUser = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                User__c = grpUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = grpUser.groupId);
            
            insert qurUser;
            
            Id RecordTypeIdCase = Schema.SObjectType.case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
            
            Case c = new Case(
                AccountId = acct.Id,
                ContactId = con.Id,
                Origin = 'Customer Portal',
                Status = 'New',
                InitialQueue__c = 'group name',
                AssignCaseToCurrentUser__c = false,
                OwnerId = g1.Id,
                EntitlementId = ent.Id,
                RecordTypeId = RecordTypeIdCase
            );
            insert c;
            
            Case cs2 = new Case();
            cs2.ContactId = con.Id;
            cs2.AccountId = acct.Id;
            cs2.AssignCaseToCurrentUser__c = false;
            cs2.OwnerId = UserInfo.getUserId();
            cs2.RecordTypeId = RecordTypeIdCase;
            cs2.Origin = 'Partner Portal';
            cs2.Mail_CC_List__c = 'nodata@info.com';
            cs2.Subject = 'Techno case';
            cs2.Description = 'Test class to check case creation';
            cs2.Status = 'New';
            insert cs2;
            
            CaseComment[] newComment = new CaseComment[0];
            newComment.add(new CaseComment(ParentId = c.Id,CommentBody = 'Test Comment Case CommentBoby',IsPublished = true)); 
            newComment.add(new CaseComment(ParentId = cs2.Id,CommentBody = 'Test Comment Case CommentBoby',IsPublished = true)); 
            insert newComment;        
        }
    }
    
    static testMethod void testCCMailOnCaseComment(){
        Account acct = new Account(
            Name = 'TestAcc',
            RDCategorization__c = 'Site');
        insert acct;
        
        Contact Con = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='briandent@trailhead.com',
            AccountId = acct.Id);
        insert Con;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=acct.Id,Type = 'TECHNO',
                                          BusinessHoursId = [select id from BusinessHours where Name = 'Default'].Id,
                                          StartDate=Date.valueof(System.now().addDays(-2)), 
                                          EndDate=Date.valueof(System.now().addYears(2)));
        insert ent;
        
        
        User u = [Select id from User where Id = :UserInfo.getUserId() and ProfileId = :UserInfo.getProfileId()];
        
        system.runAs(u) {
            
            CSM_Case_Details_Global_Setting__c custsettObj1 = new CSM_Case_Details_Global_Setting__c();
            custsettObj1.Name = 'Email Template Default CaseComment';
            custsettObj1.Setting_Type__c = 'Email Template';
            custsettObj1.Country__c = 'Default';
            custsettObj1.Other_Details__c = 'CaseComment';
            insert custsettObj1;
            
            CSM_Case_Details_Global_Setting__c custsettObj2 = new CSM_Case_Details_Global_Setting__c();
            custsettObj2.Name = 'Org Wide No-Reply';
            custsettObj2.Setting_Type__c = 'Org Wide';
            custsettObj2.Other_Details__c = 'Customer Portal';
            custsettObj2.Origin__c = 'Customer Portal';
            insert custsettObj2;
            
            CSM_Case_Details_Global_Setting__c custsettObj3 = new CSM_Case_Details_Global_Setting__c();
            custsettObj3.Name = 'Email Template register PEP';
            custsettObj3.Setting_Type__c = 'Email Template';
            custsettObj3.Country__c = 'Default PEP';
            custsettObj3.Component_Id__c = '0D26A000000L1W5';
            custsettObj3.Other_Details__c = 'New';
            custsettObj3.Origin__c = 'Partner Portal';
            insert custsettObj3;
            
            CSM_Case_Details_Global_Setting__c custsettObj4 = new CSM_Case_Details_Global_Setting__c();
            custsettObj4.Name = 'ET_TECH_EN_Case Closed  PEP';
            custsettObj4.Setting_Type__c = 'Email Template';
            custsettObj4.Country__c = 'PEP';
            custsettObj4.Component_Id__c = '0D26A000000L1W5';
            custsettObj4.Other_Details__c = 'Closed';
            custsettObj4.Origin__c = 'Partner Portal';
            insert custsettObj4;
            
            CSM_Case_Details_Global_Setting__c custsettObj5 = new CSM_Case_Details_Global_Setting__c();
            custsettObj5.Name = 'Email Template Update PEP';
            custsettObj5.Setting_Type__c = 'Email Template';
            custsettObj5.Country__c = 'PEP';
            custsettObj5.Component_Id__c = '0D26A000000L1W5';
            custsettObj5.Other_Details__c = 'CaseComment';
            custsettObj5.Origin__c = 'Partner Portal';
            insert custsettObj5;
            
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            GroupMember grpUser = new GroupMember (
                UserOrGroupId = UserInfo.getUserId(),
                GroupId = g1.Id);
            
            insert grpUser;
            
            Queue_User_Relationship__c qur = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                Type__c = 'Queue',
                Group_Id__c = grpUser.groupId);
            insert qur;
            
            Queue_User_Relationship__c qurUser = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                User__c = grpUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = grpUser.groupId);
            
            insert qurUser;
            
            Id recordTypeIdTechno = Schema.SObjectType.case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
            
            Case cs = new Case();
            cs.ContactId = Con.Id;
            cs.AccountId = acct.Id;
            cs.RecordTypeId = recordTypeIdTechno;
            cs.Priority = 'Medium';
            cs.Origin = 'Customer Portal';
            cs.Mail_CC_List__c = 'nodata@info.com';
            cs.AssignCaseToCurrentUser__c = false;
            cs.Subject = 'Techno case';
            cs.Description = 'Test class to check case creation';
            cs.Status = 'New';
            cs.OwnerId = g1.Id;
            
            insert cs;
            //DAOH_CaseComment.sendLatestCommenttoDataCaseOriginator(new List<Case>{cs});
            List<Case> caseList = [SELECT Id, Origin, Subject FROM Case];
            List<CaseComment> commentList = new List<CaseComment>();
            for(Case csc : caseList){
                CaseComment csCommentObj = new CaseComment();
                csCommentObj.IsPublished = true;
                csCommentObj.ParentId = csc.Id;
                csCommentObj.CommentBody = 'Demo text to test case comment';
                commentList.add(csCommentObj);
            }
            
            insert commentList;
        }
    }
    
    @IsTest
    static void testDataPortalUsersAssignment() {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        String profilId2 = [select id from Profile where Name='System Administrator'].Id;
        User accOwner = New User(Alias = 'su',UserRoleId= portalRole.Id, ProfileId = profilId2, Email = 'john2@iqvia.com',IsActive =true ,Username ='john2@iqvia.com', LastName= 'testLastName', CommunityNickname ='testSuNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert accOwner;
        
        System.runAs (accOwner) {
            List<Asset> assets = new List<Asset>();
            Account account = UTL_TestData.createAccount();
            account.ownerId=accOwner.Id;
            account.AccountCountry__c = 'IN';
            insert account;
            
            Product2 product = UTL_TestData.createProduct();
            product.Name = 'DDD';
            product.SpecificToCSM__c = True;
            insert product;
            
            
            Asset asset = new Asset(Name = 'DDD', AccountId = account.Id, Product2Id = product.id);
            insert asset;
            
            
            Contact contact = new Contact( 
                Firstname='Brian', 
                Lastname='Dent', 
                Phone='(619)852-4569', 
                Department='Mission Control', 
                Title='Mission Specialist - Neptune', 
                Email='john@acme.com',
                Portal_Case_Type__c = 'Information Offering',
                AccountId = account.Id);
            insert contact;
            
            CSM_Case_Details_Global_Setting__c custSet = new CSM_Case_Details_Global_Setting__c();
            custSet.Name = 'CAS_ET_CSM_Data_OriginatorNotification';
            custSet.Setting_Type__c = 'Email Template';
            custSet.Country__c = CON_CSM.S_DEFAULT;
            custSet.Origin__c = 'Customer Portal';	
            custSet.Component_Id__c = '00X6A000000hM1F';
            custSet.Other_Details__c =  CON_CSM.S_ORG_CASECOMMENT;
            insert custSet;
            
            CSM_Case_Details_Global_Setting__c custSet1 = new CSM_Case_Details_Global_Setting__c();
            custSet1.Name = 'CAS_ET_Data_OriginNotification';
            custSet1.Setting_Type__c = 'Email Template';
            custSet1.Country__c = 'IN';
            custSet1.Origin__c = 'Customer Portal';	
            custSet1.Component_Id__c = '00X6A000000hM1F';
            custSet1.Other_Details__c =  CON_CSM.S_ORG_CASECOMMENT;
            insert custSet1;
            
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            GroupMember grpUser = new GroupMember (
                UserOrGroupId = accOwner.Id,
                GroupId = g1.Id);
            
            insert grpUser;
            
            Queue_User_Relationship__c qur = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                User__c = grpUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = grpUser.groupId);
            
            insert qur;
            
            String profilId = [select id from Profile where Name='CSM Customer Community Plus Login User'].Id;
            UserRole ur = [Select PortalType, PortalAccountId From UserRole where PortalType = 'CustomerPortal' limit 1];
            User user = New User(Alias = 'com', Email = 'john@acme.com',IsActive =true , ContactId = contact.Id, ProfileId = profilId,Username =' john@acme.com', LastName= 'testLastName', CommunityNickname ='testCommunityNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
            insert user;
            
            GroupMember portalUser = new GroupMember (
                UserOrGroupId = user.Id,
                GroupId = g1.Id);
            
            insert portalUser;
            
            Queue_User_Relationship__c qurPortal = new Queue_User_Relationship__c(
                Name = portalUser.group.Name,
                User__c = portalUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = portalUser.groupId);
            
            insert qurPortal;
            
            CSM_QI_Data_Portal_Queue_User_details__c dataPortal = new CSM_QI_Data_Portal_Queue_User_details__c();
            dataPortal.Case_Subtype_3__c = 'OUTLET VOLUME INVESTIGATION';
            dataPortal.Queue_User_Relationship__c =qurPortal.Id;
            dataPortal.IQVIA_User__c = user.Id;
            dataPortal.CaseRecordType__c = 'DATACase';
            dataPortal.Order__c = 1;
            insert dataPortal;
            
            
            Test.startTest();
            system.runAs(user){
                Id RecordTypeIdCase = Schema.SObjectType.case.getRecordTypeInfosByName().get('DATA Case').getRecordTypeId();
                List<Case> cases = new List<Case>();
                Case c = new Case(
                    AccountId = account.Id,
                    ContactId = contact.Id,
                    Origin = 'Customer Portal',
                    Status = 'New',
                    AssignCaseToCurrentUser__c = false,
                    Subtype3__c='OUTLET VOLUME INVESTIGATION',
                    OwnerId = user.Id,
                    CSM_QI_Data_Originator__c = qurPortal.Group_Id__c,
                    RecordTypeId = RecordTypeIdCase
                );
                insert c;
                
                List<Case> caseList = [select id,ProductName__c,SubType1__c,SubType2__c,SubType3__c From Case where Origin = 'Customer Portal'];
                List<CaseComment> commentList = new List<CaseComment>();
                for(Case csc : caseList){
                    CaseComment csCommentObj = new CaseComment();
                    csCommentObj.IsPublished = true;
                    csCommentObj.ParentId = csc.Id;
                    csCommentObj.CommentBody = 'Demo text to test case comment';
                    commentList.add(csCommentObj);
                }
                
                insert commentList;
            }
            Test.stopTest();
        }
    }
    
}