@isTest
public class TST_CNT_CSM_RecordAttachments {
    @testSetup
    static void dataSetup() {
        Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle', language = 'en_US',UrlName='TestUrlName11', IsVisibleInCsp= true);
        insert knowledge;
        Knowledge__kav k = [SELECT Id,KnowledgeArticleId FROM Knowledge__kav WHERE Id=:knowledge.Id];
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion; 
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = k.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
    }
    
    @IsTest
    static void testgetPermissionSets() {
        List<Knowledge__kav> kv = [Select id from Knowledge__kav];
        Test.startTest();
        boolean actual =CNT_CSM_RecordAttachments.getPermissionSets();
        Test.stopTest();
        System.assertEquals(false, actual);
    }
    
    @IsTest
    static void testGetpublishStatus() {
        boolean actual ;
        Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle12', language = 'en_US',UrlName='TestUrlName', IsVisibleInCsp= true);
        insert knowledge;
        Test.startTest();
        List<Knowledge__kav> kv = [Select ID from Knowledge__kav where ID =:knowledge.ID and publishStatus ='Draft'  ];
        if(kv.size()>0){
            actual =CNT_CSM_RecordAttachments.getPublishStatus(kv[0].ID);
        }
        Test.stopTest();
        if(actual){
            System.assertEquals(true, actual);
        }
        else
        {
            System.assertEquals(false, actual);
        }
    }
    
    @IsTest
    static void testdeleteContentDocumentById() {
        boolean actual = true;
        Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle', language = 'en_US',UrlName='TestUrlName', IsVisibleInCsp= true);
        insert knowledge;
        List<Knowledge__kav> kv = [Select ID from Knowledge__kav where ID =:knowledge.id  ];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = kv[0].id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;      
        Test.startTest();
        
        CNT_CSM_RecordAttachments.deleteContentDocumentById(cdl.ContentDocumentId);
        
        Test.stopTest();
    }
    
    @IsTest
    static void testupdateContentDocumentLinkVisibility() {
        boolean actual = true;
        Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle12', language = 'en_US',UrlName='TestUrlName', IsVisibleInCsp= true);
        insert knowledge;
        Test.startTest();
        List<Knowledge__kav> kv = [Select ID from Knowledge__kav where ID =:knowledge.ID ];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = kv[0].id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        CNT_CSM_RecordAttachments.updateContentDocumentLinkVisibility(cdl.ContentDocumentId, cdl.linkedEntityId,cdl.visibility);
        
        Test.stopTest();
    }
    
    @IsTest
    static void testgetAttachments() {
        Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle01', language = 'en_US',UrlName='TestUrlName01', IsVisibleInCsp= true);
        insert knowledge;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = knowledge.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        Test.startTest();
        CNT_CSM_RecordAttachments.getAttachments(knowledge.Id);
        Test.stopTest();
    }
    
    
} 