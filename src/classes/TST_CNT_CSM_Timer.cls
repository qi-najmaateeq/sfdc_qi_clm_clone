@isTest
public class TST_CNT_CSM_Timer {
    
    static testmethod void testupdateStatusforLogAWorkNoteonCase(){
        Account acct = new Account(
            Name = 'TestAcc',
            RDCategorization__c = 'Site');
        insert acct;
        
        Contact Con = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='briandent@trailhead.com',
            AccountId = acct.Id);
        insert Con;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=acct.Id,Type = 'TECHNO',
                                          BusinessHoursId = [select id from BusinessHours where Name = 'Default'].Id,
                                          StartDate=Date.valueof(System.now().addDays(-2)), 
                                          EndDate=Date.valueof(System.now().addYears(2)));
        insert ent;
        
        
        User u = [Select id from User where Id = :UserInfo.getUserId() and ProfileId = :UserInfo.getProfileId()];
        
        system.runAs(u) {
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            GroupMember grpUser = new GroupMember (
                UserOrGroupId = u.Id,
                GroupId = g1.Id);
            
            insert grpUser;
            
            Queue_User_Relationship__c qur = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                Type__c = 'Queue',
                Group_Id__c = grpUser.groupId);
            insert qur;
            
            Queue_User_Relationship__c qurUser = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                User__c = grpUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = grpUser.groupId);
            
            insert qurUser;
            
            Id RecordTypeIdCase = Schema.SObjectType.case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
            
            Case cs2 = new Case();
            cs2.ContactId = con.Id;
            cs2.AccountId = acct.Id;
            cs2.AssignCaseToCurrentUser__c = false;
            cs2.OwnerId = UserInfo.getUserId();
            cs2.RecordTypeId = RecordTypeIdCase;
            cs2.Origin = 'Chat';
            cs2.Mail_CC_List__c = 'nodata@info.com';
            cs2.Subject = 'Techno case';
            cs2.Description = 'Test class to check case creation';
            cs2.Status = 'New';
            insert cs2;
            CNT_CSM_Timer.insertTime(cs2.Id);
            TimeSheet__c sheet = [Select Id From TimeSheet__c LIMIT 1];
            
            CNT_CSM_Timer.getTimeSheet(cs2.Id);
            CNT_CSM_Timer.insertTime(cs2.Id);
            CNT_CSM_Timer.saveTimeSheet(cs2.Id ,2, 'Test Case');
            CNT_CSM_Timer.updateStopTime(cs2.Id,sheet.Id);
            
        }
    }

}