/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for AccountContactRelation
 */
public class SLT_AccountContactRelation extends fflib_SObjectSelector {
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
            AccountContactRelation.Id
        };
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return AccountContactRelation.sObjectType;
    }
    
    /**
     * This method used to get AccountContactRelation by Id
     * @return  List<AccountContactRelation>
     */
    public List<AccountContactRelation> getAccountContactRelations(List<Account> sfdcGoldenAccountList, List<Account> survivorAccountList) {
        String query = newQueryFactory(true).setCondition('isDirect = false and ((Contact.accountid in :sfdcGoldenAccountList and Accountid in :survivorAccountList) OR (Contact.accountid in :survivorAccountList and Accountid in :sfdcGoldenAccountList))').toSOQL();
        System.debug(query);
        return (List<AccountContactRelation>) Database.query(query);
    }
}