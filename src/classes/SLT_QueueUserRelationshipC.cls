public class SLT_QueueUserRelationshipC extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_QueueUserRelationshipC() {
        super(false, true, true);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Queue_User_Relationship__c.sObjectType;
    }
    
    /**
     * This method used to get Queue_User_Relationship__c by Id
     * @return  Map<Id, User>
     */
    
    public Map<Id, Queue_User_Relationship__c> selectByQueueId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Queue_User_Relationship__c>((List<Queue_User_Relationship__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    public List<Queue_User_Relationship__c> selectByQueueName(String currentQueue, Set<String> fieldSet,String userId) {
        return ((List<Queue_User_Relationship__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Name = :currentQueue and Type__c = \'Queue\'').toSOQL()));
    }
    
    public List<Queue_User_Relationship__c> selectByUserGrop(Set<Id> addOId) {
        return [select Id,Name,Type__c,Group_Id__c,User__c,User_Email__c,Queue_Email__c FROM Queue_User_Relationship__c where (User__c In: addOId and Type__c = 'User') or (Group_Id__c In: addOId and Type__c = 'Queue') Order by Name];
    }
    
    public List<Queue_User_Relationship__c> selectByUserQueueName(Set<Id> uptOId,Set<String> queueNameSet) {
        return [select Id, Name From Queue_User_Relationship__c where User__c In: uptOId and Name In :queueNameSet];
    }
    
    public List<Queue_User_Relationship__c> selectByQueueIdSet(Set<ID> idSet, Set<String> fieldSet) {
        return (List<Queue_User_Relationship__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL());
    }
    
    public List<Queue_User_Relationship__c> selectByUserGroupQueueIdSet(Set<ID> idSet, Set<Id> userIdSet,Set<String> fieldSet) {
        return (List<Queue_User_Relationship__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Group_Id__c in :idSet and User__c in :userIdSet').toSOQL());
    }
    
    public List<Queue_User_Relationship__c> selectByGroupIdSet(Set<ID> idSet) {
        return [select Id,Name,Type__c,Group_Id__c,User__c,User__r.Name,User_Email__c,Queue_Email__c FROM Queue_User_Relationship__c where (User__c In: idSet and Type__c = 'User') or (Group_Id__c In: idSet and Type__c = 'Queue')];
    }
    
    public List<Queue_User_Relationship__c> selectAllTechQueue(Set<String> fieldSet) {
        return (List<Queue_User_Relationship__c>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Type__c = \'Queue\' and Name like\'Tech%\'').toSOQL());
    }
    
    public List<Queue_User_Relationship__c> selectAllRnDQueues(){
        return [SELECT Id, Name, AFU_Queue_Email__c FROM Queue_User_Relationship__c WHERE Name LIKE 'RnD%'];
    }
    
    public List<Queue_User_Relationship__c> getUserQueueRecords(){
        return [SELECT Id, Name, AFU_Queue_Email__c FROM Queue_User_Relationship__c WHERE Name LIKE 'RnD%' AND AFU_Queue_Email__c != null AND Type__c ='Queue'];
    }
}