/*
* This class is used to test Approval Item
*/ 
@isTest
public class TST_ApprovalItems {    
    
    @TestSetup
    static  void dataSetup() {
        BNFOpptyListLockedGrid.CheckValidation = false;
        User user = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];  
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        opp.StageName = 'In-Hand';
        insert opp;
        Revenue_Analyst__c revenueAnalyst = UTL_TestData.createRevenueAnalyst();
        insert revenueAnalyst;
        List<Address__c> addressList = UTL_TestData.createAddresses(acc);
        insert addressList;
        Product2 product2 = UTL_TestData.createProduct();
        insert product2;
        PricebookEntry pbe = UTL_TestData.createPricebookEntry(product2.Id);
        insert pbe;
        OpportunityLineItem oli = UTL_TestData.createOpportunityLineItem(opp.Id, pbe.Id);
        insert oli;
        BNF2__c bnf2 = UTL_TestData.createBNFRecord(opp, oli, addressList, revenueAnalyst.Id);
        insert bnf2;
        MIBNF2__c mibnf2 = UTL_TestData.createMIBNF(opp, revenueAnalyst);
        insert mibnf2;
        MIBNF_Component__c mibnfComp = UTL_TestData.createMIBNF_Comp(mibnf2, addressList);
        insert mibnfComp;
        MI_BNF_LineItem__c mIbnfLineItem = new MI_BNF_LineItem__c();
        mIbnfLineItem.MIBNF_Component__c = mibnfComp.id;
        mIbnfLineItem.Opportunity_Line_Itemid__c = oli.Id;
        insert mIbnfLineItem;
        System.runAs(user){
            Group gp = UTL_TestData.createGroup('Test Group', 'Queue');
            insert gp;
            GroupMember gm = UTL_TestData.createGroupMember(gp.Id, UserInfo.getUserId());
            insert gm;
        }
    }
    
    static testMethod void testSetItemWrapperValuesWithSideBar() {    
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];  
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        System.assert(result.isSuccess());
        ProcessInstanceWorkItem piw = [SELECT id,ProcessInstance.TargetObject.Type FROM ProcessInstanceWorkItem
                                       WHERE processInstance.TargetObjectId =: mibnfComp.id LIMIT 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','true');
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems approvalItems = new ApprovalItems (controller);  
    }
    
    static testMethod void testSetItemWrapperValuesWithoutSideBar() {    
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        System.assert(result.isSuccess());
        ProcessInstanceWorkItem piw = [SELECT id,ProcessInstance.TargetObject.Type FROM ProcessInstanceWorkItem
                                       WHERE processInstance.TargetObjectId =: mibnfComp.id LIMIT 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems approvalItems = new ApprovalItems (controller);
    }
    
    static testMethod void testReassignPage() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
        ai.checkboxAllValue = true;
        ai.getSelected();
        ai.ReassignPage();
    }
    
    static testMethod void testApproveRejectPage() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
        ai.checkboxAllValue = true;
        ai.getSelected();
        ai.ApproveRejectPage();
    }
    
    static testMethod void testReassign1() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
        ai.checkboxAllValue = true;
        ai.getSelected();
        ai.Reassign1();
    }    
    
    static testMethod void testApprove1() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
        ai.checkboxAllValue = true;
        ai.getSelected();
        ai.Approve1();
    }
    
    static testMethod void testReject1() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
        ai.checkboxAllValue = true;
        ai.getSelected();
        ai.Reject1();
    }   
    
    static testMethod void testCancel1() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
        ai.Cancel1(); 
    }
    
    static testMethod void testPagination() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
        ai.itemfirstPage();
        Boolean hasNext = ai.ItemhasNext;
        Boolean hasPrevious = ai.ItemhasPrevious;
    }
    
    static testMethod void testNext() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
 		ai.itemNext();
        ai.Itemprevious();
    }
    
    static testMethod void testItemLastPage() {
        MIBNF_Component__c mibnfComp = [SELECT Id FROM MIBNF_Component__c LIMIT 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(mibnfComp.id);
        Approval.ProcessResult result = Approval.process(req1);
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type, ProcessInstance.Status FROM ProcessInstanceWorkItem
                                       where processInstance.TargetObjectId =: mibnfComp.id limit 1];
        Apexpages.currentPage().getParameters().put('isShowHeader','false'); 
        ApexPages.standardController controller = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (controller);
        ApprovalItems.ItemWrapper itw = new ApprovalItems.ItemWrapper(piw);
		ai.itemLastPage();
    } 
}