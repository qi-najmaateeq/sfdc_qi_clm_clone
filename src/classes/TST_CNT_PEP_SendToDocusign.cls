@isTest
public class TST_CNT_PEP_SendToDocusign {    
    
    
     static Account getTestAccount() {
        Account testAccount=new Account(
            Name='Test Account',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Global_Customer_Account').getRecordTypeId());
         return testAccount;
    }
    
    
    static Contract getTestContract() {
        Contract testContract = new Contract(Project_Leader_Region__c='Asia Pacific', 
                                          RecordTypeId=Schema.SObjectType.Contract.getRecordTypeInfosByDeveloperName().get('PRM_Contract').getRecordTypeId(),
                                          //AccountId=testAccount.Id,
                                          Status='Assigned - Not Started',
                                          Date_Tracking_started__c=Date.newInstance(2019, 12, 9),
                                          Date_executed_signed_contract_received__c = Date.newInstance(2019, 12, 9),
                                          PRM_Contract_type__c='Master Agency Agreement');
        
        return testContract;
    }
    
    
    static testmethod void SendNow_WithRestTest(){
       
        Account testAccount= getTestAccount();
        Contract testContract = getTestContract(); 
        
        CNT_PEP_SendToDocusign.testContract =new list<Contract>{testContract};
        Test.setMock(HttpCalloutMock.class, new TST_MockHttpResponseGenerator());
        String result;
        Test.startTest();
        	result=CNT_PEP_SendToDocusign.SendNow_WithRest(True, testContract.Id, '1c78eee1-0897-4f1e-b4ee-418c2cf34f72');
        Test.stopTest();
        
        System.assertEquals(True, CNT_PEP_SendToDocusign.envelopeCreationSuccess,'Envelope Creation callout failed!!'); 
        
    }   
    
    
    public class TST_MockHttpResponseGenerator implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test"}');
            res.setStatusCode(200);
            return res;
        }
    }    
}