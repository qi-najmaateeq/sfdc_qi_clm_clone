/*
 * Version       : 1.0
 * Description   : Test Class for SLT_Account
 */
@isTest
public class TST_SLT_Account {
    	/**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
    	Account account = UTL_TestData.createAccount();
        insert account;
    }
    
     /**
     * This method used to get Case by ID
     */    
    @IsTest
    static void testSelectById() {
    	List<Account> accounts = new  List<Account>();
    	Account a = [SELECT Id FROM Account WHERE Name = 'TestAccount'];	
        Test.startTest();
        accounts = new SLT_Account().selectById(new Set<Id> {a.Id});
        Test.stopTest();
        Integer expected = 1;
        Integer actual = accounts.size();
        System.assertEquals(expected, actual);
    }
}