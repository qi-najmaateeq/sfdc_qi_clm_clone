/**
 * This test class is used to test all methods in Resource Skill Request service class
 * version : 1.0
 */
@isTest
private class TST_SLT_Resource_Skill_Request {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        
         
    }
    
    /**
     * This test method used to cover basic methods
     */ 
    static testMethod void testServiceResourceRequest() {   
        List<Schema.SObjectField> resourceRequestList = new SLT_Resource_Skill_Request().getSObjectFieldList();
        Schema.SObjectType resourceRequest = new SLT_Resource_Skill_Request(true).getSObjectType();
    }
}