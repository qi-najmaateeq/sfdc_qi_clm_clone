public class CNT_CPQ_QCSelfCheckFinalContract {

    @auraEnabled
    public static List<ProposalQASelfCheckListWrapper> fetchQCCheckListRecords(Id agreementId) {

        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.RECORDTYPE_NAME};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId}, fieldSet);
        List<ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList = new List<ProposalQASelfCheckListWrapper>();
        if(agreementList.size() > 0){
            String recordtype = '';
            if(agreementList[0].RecordType.Name == CON_CPQ.AGREEMENT_FDTN_CONTRACT){
                recordtype = CON_CPQ.CONTRACT;
            }else if(agreementList[0].RecordType.Name == CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER){
                recordtype = CON_CPQ.CHNAGE_ORDER;
            }else if(agreementList[0].RecordType.Name == CON_CPQ.AGREEMENT_FDTN_CNF){
                recordtype = CON_CPQ.CNF;
            }
            Set<String> propsalFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT, CON_CPQ.TYPE, CON_CPQ.QUESTION, CON_CPQ.GUIDELINES,
                CON_CPQ.PD_SELD_REVIEW, CON_CPQ.PD_COMMENT, CON_CPQ.LINE_MANAGER_QC_COMMENT, CON_CPQ.PROPOSAL_QC_COMMNET, 
                CON_CPQ.RESPONSE_CONFIRM_MAJOR_FINDINGS_CORRECTE, CON_CPQ.RESPONSE, CON_CPQ.REVIEWER_RESPONSE};
            List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = new SLT_ProposalQASelfCheckList().getProposalCheckListByAgreement(new Set<Id>{agreementId}, propsalFieldSet);
            if(proposalQASelfCheckList.size() == 0){
                Set<String> qcFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.RECORDTYPE, CON_CPQ.TYPE, CON_CPQ.QUESTION, 
                    CON_CPQ.GUIDELINES, CON_CPQ.TYPE_OF_PROPOSAL, CON_CPQ.TYPE_OF_BIDS};
                List<QC_Check_List_Item__c> qcSelfCheckDraftList = new SLT_QCCheckListItem().getQCCheckListItemByRecordType(recordtype, qcFieldSet);
                for(QC_Check_List_Item__c qcRecord : qcSelfCheckDraftList){
                    ProposalQASelfCheckListWrapper  wrapperObj = new ProposalQASelfCheckListWrapper(new Proposal_QA_Self_Check_List__c(Agreement__c = agreementId, 
                        Question__c = qcRecord.Question__c, Guidelines__c = qcRecord.Guidelines__c), false, proposalQASelfCheckListWrapperList.size());
                    proposalQASelfCheckListWrapperList.add(wrapperObj);
                }
            }else{
                for(Proposal_QA_Self_Check_List__c qcCheck : proposalQASelfCheckList){
                    ProposalQASelfCheckListWrapper  wrapperObj = new ProposalQASelfCheckListWrapper(qcCheck, false, proposalQASelfCheckListWrapperList.size());
                    proposalQASelfCheckListWrapperList.add(wrapperObj);
                }
            }
        }
        return ProposalQASelfCheckListWrapperList;
    }
    
    @auraEnabled
    public static void saveProposalQASelfCheckList(String proposalQASelfCheckListWrapperListString, Id agreementId,
        String processStep, Boolean isSaveAndComplete, String overAllComments){

        List<ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList =
            (List<ProposalQASelfCheckListWrapper>)JSON.deserialize(proposalQASelfCheckListWrapperListString,List<ProposalQASelfCheckListWrapper>.class);
        List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = new List<Proposal_QA_Self_Check_List__c>();
        if(proposalQASelfCheckListWrapperList.size() > 0){

            for(ProposalQASelfCheckListWrapper proposalCheck : proposalQASelfCheckListWrapperList){
                proposalQASelfCheckList.add(proposalCheck.proposalQASelfCheckList);
            }

            if(proposalQASelfCheckList.size() > 0)
                upsert proposalQASelfCheckList;
            if(processStep == CON_CPQ.FINAL_QC){
                Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.RECORDTYPE_NAME, CON_CPQ.AGREEMENT_FINAL_QC_OVERALL_COMMENT};
                List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId}, fieldSet);
                Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(Id = agreementId);
                if(isSaveAndComplete){
                    if(agreementList.size() > 0 && agreementList[0].RecordType.Name == CON_CPQ.AGREEMENT_FDTN_CNF)
                        agreement.Agreement_Status__c = CON_CPQ.CNF_DELIVERED;
                    else
                        agreement.Agreement_Status__c = CON_CPQ.CUSTOMER_DELIVERABLE;
                    agreement.Process_Step__c = CON_CPQ.NONE;
                }
                agreement.Agreement_Purpose_List__c = overAllComments;             
                update agreement;
            }
        }
    }

    @auraEnabled
    public static List<ProposalQASelfCheckListWrapper> addMoreItemInList(String proposalQASelfCheckListWrapperListString, String agreementId){

        List<ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList = 
            (List<ProposalQASelfCheckListWrapper>)JSON.deserialize(proposalQASelfCheckListWrapperListString,List<ProposalQASelfCheckListWrapper>.class);
        ProposalQASelfCheckListWrapper  wrapperObj = 
            new ProposalQASelfCheckListWrapper(new Proposal_QA_Self_Check_List__c(Agreement__c = agreementId), true, proposalQASelfCheckListWrapperList.size());
        proposalQASelfCheckListWrapperList.add(wrapperObj);
        return proposalQASelfCheckListWrapperList;
    }

    @auraEnabled
    public static List<String> fetchPicklistValue(String fieldName){

        Schema.sObjectType objType = Proposal_QA_Self_Check_List__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        List < Schema.PicklistEntry > picklistEntryResult = fieldMap.get(fieldName).getDescribe().getPickListValues();
        List < String > picklistEntries = new list < String > ();
        for( Schema.PicklistEntry picklistValue : picklistEntryResult){
            picklistEntries.add( picklistValue.getValue());
        }
        return picklistEntries;
    }
    
    @auraEnabled
    public static String fetchOverAllComments(Id agreementId){
        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_FINAL_QC_OVERALL_COMMENT};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId}, fieldSet);
        if(agreementList.size() > 0)
            return agreementList[0].Agreement_Purpose_List__c;
        return '';
    }


    public class ProposalQASelfCheckListWrapper{
        @AuraEnabled public Proposal_QA_Self_Check_List__c proposalQASelfCheckList{get;set;}
        @AuraEnabled public Boolean isNew{get;set;}
        @AuraEnabled public Integer index{get;set;}
        public ProposalQASelfCheckListWrapper(Proposal_QA_Self_Check_List__c proposalQASelfCheckList, Boolean isNew, Integer index){
            this.proposalQASelfCheckList = proposalQASelfCheckList;
            this.isNew = isNew;
            this.Index = index;
        }
    }
}