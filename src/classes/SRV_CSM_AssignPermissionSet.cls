global class SRV_CSM_AssignPermissionSet {
    @future 
    public static void AssignPermissionSetToUsers (String[] lstUsers,Id psId) {
        PermissionSetAssignment psa =null;
        List<PermissionSetAssignment> listPSAList = new List<PermissionSetAssignment>(); 
                for (String sUser : lstUsers){
                    psa = new PermissionSetAssignment (PermissionSetId = psId, AssigneeId = (ID)sUser);
                    listPSAList.add(psa);
                }
                try {
                    if(listPSAList.isEmpty() == false ){
                        insert listPSAList; 
                    }
                } catch (DmlException e) {
                    System.debug('Failed due to : '+e);
                }
        
    }
    
    @future 
    public static void DeletePermissionSetToUsers (String[] sDeleteUsers,Id psId) {
        List<PermissionSetAssignment> deletePSAList= [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId IN : sDeleteUsers AND PermissionSetId = : psId];
        try {
            if(deletePSAList.isEmpty() == false ){
                delete deletePSAList;
            }
        } catch (DmlException e) {
            System.debug('Failed due to : '+e);
        }
    }
    
    @future
    public static void createActivityFromEmailMessage(String activityList) {
        List<Activity__c> newList = (List<Activity__c>)JSON.deserialize(activityList, List<Activity__c>.Class);
        System.debug('newList : '+newList.size());
        try{
            insert newList;
        }catch (DmlException e) {
            System.debug('Failed due to : '+e);
        }
        
    }
    
    @future
    public static void updateMilestoneVilotaion(List<Id> newList) {  
        List<Case> caseList = [select Id,Milestone_Violation__c,RecordTypeName__c,CaseOriginatorName__c,TaskMilestone__c from Case where Id in :newList];
        boolean updateCheck = false;
        if(!caseList.isEmpty()) {
            for (Case c : caseList){
                if(c.Milestone_Violation__c) {
                    c.Milestone_Violation__c = false;
                    updateCheck = true;
                }
                if(CON_CSM.S_TECHNOLOGY_R_T.equalsIgnoreCase(c.RecordTypeName__c) && c.CaseOriginatorName__c != null && CON_CSM.S_RESOLUTION_FAILD.equalsIgnoreCase(c.CaseOriginatorName__c)){
                    if(c.TaskMilestone__c != null && !c.TaskMilestone__c.contains(CON_CSM.S_RESOLUTION_FAILD)){
                        c.TaskMilestone__c += ';' + CON_CSM.S_RESOLUTION_FAILD;
                    }else if(c.TaskMilestone__c == null){
                        c.TaskMilestone__c = CON_CSM.S_RESOLUTION_FAILD; 
                    }
                    c.CaseOriginatorName__c = null;
                    updateCheck = true;
                }
            }
            try {
                if(updateCheck) {
                    update caseList;
                }
                
            } catch (DmlException e) {
                System.debug('Failed due to : '+e);
            }
            
        }
    }
}