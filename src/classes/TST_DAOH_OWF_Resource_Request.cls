/**
 * This test class is used to test all methods in Resource Request trigger.
 * version : 1.0
 */
@isTest
private class TST_DAOH_OWF_Resource_Request {
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'OWF_Triage_Manager'];
        List<PermissionSetAssignment> psaList = [Select id from PermissionSetAssignment where AssigneeId =:UserInfo.getUserId() AND PermissionSetId =:ps.Id];
        if(psaList.size() == 0)
            insert new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = ps.Id);
        
        System.runAs(new User(Id = UserInfo.getUserId())) {    
            Account acc = UTL_OWF_TestData.createAccount();
            insert acc;
            pse__Grp__c grp = UTL_OWF_TestData.createGroup();
            insert grp;
            Indication_List__c indication = UTL_OWF_TestData.createIndication('Test Indication', 'Acute Care');
            insert indication;
            Line_Item_Group__c lineItemGroup = UTL_OWF_TestData.createLineItemGroup(indication.Id);
            lineItemGroup.BD_Lead_Sub_Region__c = 'United States of America';
            insert lineItemGroup;
            Contact cont = UTL_OWF_TestData.createContact(acc.Id);
            cont.pse__Is_Resource__c = true;
            cont.pse__Is_Resource_Active__c = true;
            cont.pse__Group__c = grp.Id;
            cont.Baseline_FTE__c = 1.0;
            cont.Available_for_Triage_Flag__c = true;   
            cont.pse__Salesforce_User__c = UserInfo.getUserId();
            cont.sub_group__c = 'TSL-Japan';
            insert cont; 
            Contact cont2 = UTL_OWF_TestData.createContact(acc.Id);
            cont2.pse__Is_Resource__c = true;
            cont2.pse__Is_Resource_Active__c = true;
            cont2.pse__Group__c = grp.Id;
            cont2.Baseline_FTE__c = 1.0;
            cont2.Available_for_Triage_Flag__c = true; 
            cont2.Backup_OWF_User__c = cont.id; 
            cont2.sub_group__c = 'TSL-Japan';          
            insert cont2;
            pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
            insert permissionControlGroup;
            Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
            opp.Potential_Regions__c = 'Global';
            opp.Line_of_Business__c = 'Core Clinical';
            opp.Line_Item_Group__c = lineItemGroup.Id;
            insert opp;
            Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
            insert agreement; 
            /*
            pse__Proj__c bidProject = UTL_OWF_TestData.createBidProject(grp.Id);   
            bidProject.Agreement__c = agreement.id;        
            insert bidProject;
            */
            
        }
            
    }
    
    /**
     * This test method used to update Resource Request record
     */
    static testmethod void testResourceRequestUpdate() {
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'][0];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        Test.startTest();
            insert resourceRequest;
            resourceRequest.SubGroup__c = 'Test Sub Group';
            update resourceRequest;
        Test.stopTest();
        pse__Resource_Request__c actualRR = [Select Id, pse__Resource_Request_Name__c, Resource_Request_Type__c, SubGroup__c From pse__Resource_Request__c limit 1];
        String expected = actualRR.Resource_Request_Type__c + '/' + actualRR.SubGroup__c;
        system.assertEquals(expected, actualRR.pse__Resource_Request_Name__c);
    }
    
    /**
     * This test method used to update Resource Request record
     */
    static testmethod void testCancelRelatedAssignments() {
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'][0];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        insert resourceRequest;
        
        List<pse__Schedule__c> listToInsertSchedules = new List<pse__Schedule__c>();
        listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
        listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
        insert listToInsertSchedules;
        
        List<pse__Assignment__c> listToInsertAssignments = new List<pse__Assignment__c>();
        listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, listToInsertSchedules.get(0).Id, cont.Id, resourceRequest.Id));
        listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, listToInsertSchedules.get(1).Id, cont.Id, resourceRequest.Id));
        
        insert listToInsertAssignments;
        
        Test.startTest();
            resourceRequest.pse__Status__c = 'Cancelled';
            resourceRequest.Cancellation_Reason__c = 'Duplicate Request';
            update resourceRequest;
        Test.stopTest();
        
        String expectedStatus = 'Cancelled';
        /*List<pse__Assignment__c> actualAssignmentList = [Select Id, pse__Status__c From pse__Assignment__c Where pse__Resource_Request__c = :resourceRequest.Id limit 1]; 
        system.assertEquals(expectedStatus, actualAssignmentList[0].pse__Status__c);*/
    }
    
    /**
     * This test method used to Create Resource Skill Request records
     */
    static testMethod void testCreateResourceSkillRequest() {
        List<pse__Skill__c> skillList = new List<pse__Skill__c>();
        pse__Skill__c skill1 = UTL_OWF_TestData.createSkills('Consulting', 'Line of Business');
        pse__Skill__c skill2 = UTL_OWF_TestData.createSkills('USA/Canada', 'User Regional Geographic Working Scope');
        pse__Skill__c skill3 = UTL_OWF_TestData.createSkills('Test Indication', 'Indication');
        pse__Skill__c skill4 = UTL_OWF_TestData.createSkills('Acute Care', 'Therapy Area');
        skillList.add(skill1);
        skillList.add(skill2);
        skillList.add(skill3);
        skillList.add(skill4);
        insert skillList;
        
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'][0];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        oppty.Line_of_Business__c = 'Consulting';
        update oppty;
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        Test.startTest();
            insert resourceRequest;
            oppty.Potential_Regions__c = 'USA/Canada';
            update oppty;
            pse__Resource_Request__c resourceRequest2 = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
            resourceRequest2.pse__Group__c = grp1.Id;
            insert resourceRequest2;
        Test.stopTest();
    }
    
    /**
     * This test method used to Delete Assignment Based On Resource Request
     */
    static testMethod void testDeleteAssignmentBasedOnResourceRequest(){
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'][0];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        insert resourceRequest;
        
        pse__Schedule__c schedule = UTL_OWF_TestData.createSchedule();
        insert schedule;
        
        pse__Assignment__c assignment = UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, schedule.Id, cont.Id, resourceRequest.Id);
        insert assignment;
        
        Test.startTest();
            delete resourceRequest;
        Test.stopTest();
        
        List<pse__Assignment__c> assignmentList = [Select Id From pse__Assignment__c Where pse__Resource_Request__c = :resourceRequest.Id];
        Integer expected = 0;
        System.assertEquals(expected, assignmentList.size());
    }
    
    /**
     * This test method used to test the rollup values of the Assignments fields on the Resource based on the value of Suggested FTE.
     */
    static testMethod void testpopulateRollupAssignmentFieldsOnContact(){
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'][0];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        insert resourceRequest;
        
        pse__Schedule__c schedule = UTL_OWF_TestData.createSchedule();
        insert schedule;
        
        pse__Assignment__c assignment = UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, schedule.Id, cont.Id, resourceRequest.Id);
        insert assignment;
        
        resourceRequest.Suggested_FTE__c = 10;
        Test.startTest();
            update resourceRequest;
        Test.stopTest();
        
        List<Contact> contactList = [Select Id, Pending_FTE_Sum__c From Contact Where Id = :cont.Id];
        Integer expected = 10;
        System.assertEquals(expected, contactList[0].Pending_FTE_Sum__c);
    }
    
    /**
    * This method used to test the CreateAssignment method
    */ 
    static testmethod void testCreateAssignment() {
            Test.startTest();
            List<Contact> contactList = [Select Id From Contact];          
            
            
            List<pse__Skill__c> skillList = new List<pse__Skill__c>();
            pse__Skill__c skill1 = UTL_OWF_TestData.createSkills('Consulting', 'Line of Business');
            pse__Skill__c skill2 = UTL_OWF_TestData.createSkills('Global', 'User Regional Geographic Working Scope');
            pse__Skill__c skill3 = UTL_OWF_TestData.createSkills('Test Indication', 'Indication');
            pse__Skill__c skill4 = UTL_OWF_TestData.createSkills('Acute Care', 'Therapy Area');
            skillList.add(skill1);
            skillList.add(skill2);
            skillList.add(skill3);
            skillList.add(skill4);
            insert skillList;
            
            List<pse__Skill_Certification_Rating__c> scrList = new List<pse__Skill_Certification_Rating__c>();
            //pse__Skill_Certification_Rating__c skillCertRating = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
            //insert skillCertRating;
            System.runAs(new User(Id = UserInfo.getUserId())) {
                for(pse__Skill__c skill : skillList) {
                    pse__Skill_Certification_Rating__c scr = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
                    scr.pse__Rating__c = '5 - Expert';
                    scrList.add(scr);
                }
                insert scrList;
            }
            List<pse__Schedule__c> listToInsertSchedules = new List<pse__Schedule__c>();
            listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
            listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
            insert listToInsertSchedules; 
            Account acc = UTL_OWF_TestData.createAccount();
            insert acc;
            Contact cont = [Select Id From Contact Where Name = 'TestContact'][0];
            Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
            Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, oppty.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
            insert agreement;
            pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
            
            pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
            pse__Assignment__c assign = UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, listToInsertSchedules.get(1).Id, contactList[0].Id, null);
            assign.assignment_type__c = 'Days Off';
            insert assign;
            pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
            resourceRequest.pse__Group__c = grp1.Id;
            resourceRequest.subgroup__c = 'TSL-Japan';
            insert resourceRequest;
          Test.stopTest();
        
    }
    /**
    * This method used to test the CreateAssignment method
    */ 
    static testmethod void testCreateClinicalAssignment() {
    
          System.runAs(new User(Id = UserInfo.getUserId())) {
          pse__Skill__c skill = UTL_OWF_TestData.createSkills('Test Skill', CON_OWF.SKILL_TYPE_INDICATION);
          insert skill;
          
          List<Contact> contactList = [Select Id From Contact];
          List<pse__Resource_Request__c> resRequestList = [Select Id From pse__Resource_Request__c];
          
          pse__Skill_Certification_Rating__c skillCertRating = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
          insert skillCertRating;
          
          Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
          Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
          pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
          pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        
          pse__Resource_Request__c resourceRequest1 = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
          resourceRequest1.pse__Group__c = grp1.Id;
          resourceRequest1.subgroup__c = 'TSL-Japan';
          insert resourceRequest1;
         
         List<pse__Schedule__c> listToInsertSchedules = new List<pse__Schedule__c>();
         listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
         listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
         insert listToInsertSchedules; 
        
         List<pse__Assignment__c> listToInsertAssignments = new List<pse__Assignment__c>();
         listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, listToInsertSchedules.get(0).Id, contactList[0].Id, resourceRequest1.Id));
         listToInsertAssignments.add(UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, listToInsertSchedules.get(1).Id, contactList[0].Id, resourceRequest1.Id));
         
         
         insert listToInsertAssignments;
          
          Test.startTest();
            pse__Resource_Skill_Request__c resourceSkillRequest = UTL_OWF_TestData.createResourceSkillRequest(skill.Id, resourceRequest1.Id);
            insert resourceSkillRequest;
            pse__Resource_Request__c resourceRequest2 = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
            resourceRequest2.pse__Group__c = grp1.Id;
            resourceRequest2.subgroup__c = 'TSL-Japan';
            insert resourceRequest2;
            List<pse__Resource_Skill_Request__c> resourceSkillRequestList = [Select Id, pse__Resource_Request__c FROM pse__Resource_Skill_Request__c];
            List<DAOH_OWF_Resource_Skill_Request.ResourceCandidateMatchScore> resourceCandidateMatchScoreList = DAOH_OWF_Resource_Skill_Request.createResourceCandidateMatchScore(resourceSkillRequestList);
            Integer expectedListSize = 1;
            system.assertEquals(expectedListSize, resourceCandidateMatchScoreList.size());
          Test.stopTest();
        
        }
    }
    
    /**
    * This method used to test the CreateBDRegionAssignment method
    */ 
    static testmethod void testCreateBDRegionAssignment() {
            Test.startTest();
            List<Contact> contactList = [Select Id From Contact];          
            
            
            List<pse__Skill__c> skillList = new List<pse__Skill__c>();
            pse__Skill__c skill1 = UTL_OWF_TestData.createSkills('Consulting', 'Line of Business');
            pse__Skill__c skill2 = UTL_OWF_TestData.createSkills('Global', 'User Regional Geographic Working Scope');
            skill2.name = 'United States of America';
            pse__Skill__c skill3 = UTL_OWF_TestData.createSkills('Test Indication', 'Indication');
            pse__Skill__c skill4 = UTL_OWF_TestData.createSkills('Acute Care', 'Therapy Area');
            skillList.add(skill1);
            skillList.add(skill2);
            skillList.add(skill3);
            skillList.add(skill4);
            insert skillList;
            
            List<pse__Skill_Certification_Rating__c> scrList = new List<pse__Skill_Certification_Rating__c>();
            
            System.runAs(new User(Id = UserInfo.getUserId())) {
                for(pse__Skill__c skill : skillList) {
                    pse__Skill_Certification_Rating__c scr = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
                    scr.pse__Rating__c = '5 - Expert';
                    scrList.add(scr);
                }
                insert scrList;
            }
            List<pse__Schedule__c> listToInsertSchedules = new List<pse__Schedule__c>();
            listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
            listToInsertSchedules.add(UTL_OWF_TestData.createSchedule());
            insert listToInsertSchedules; 
            Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
            Contact cont = [Select Id From Contact Where Name = 'TestContact'][0];
            Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
            pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
            pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
            pse__Assignment__c assign = UTL_OWF_TestData.createAssignment(agreement.Id, project.Id, listToInsertSchedules.get(1).Id, contactList[0].Id, null);
            assign.assignment_type__c = 'Days Off';
            insert assign;
            pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
            resourceRequest.pse__Group__c = grp1.Id;
            resourceRequest.subgroup__c = 'TSL-Japan';
            resourceRequest.Potential_Regions__c = 'Global';
            insert resourceRequest;
            resourceRequest.Is_Bid_Defense__c = true;
            update resourceRequest;
          Test.stopTest();
        
    }
    
    /**
     * This test method used to Create Resource Skill Request records
     */
    static testMethod void testCreateAssignmentBasedOnPR() {
        List<pse__Skill__c> skillList = new List<pse__Skill__c>();
        pse__Skill__c skill1 = UTL_OWF_TestData.createSkills('Consulting', 'Line of Business');
        pse__Skill__c skill2 = UTL_OWF_TestData.createSkills('USA/Canada', 'User Regional Geographic Working Scope');
        skill2.pse__type__c = 'User Regional Geographic Working Scope';
        pse__Skill__c skill3 = UTL_OWF_TestData.createSkills('Test Indication', 'Indication');
        pse__Skill__c skill4 = UTL_OWF_TestData.createSkills('Acute Care', 'Therapy Area');
        skillList.add(skill1);
        skillList.add(skill2);
        skillList.add(skill3);
        skillList.add(skill4);
        insert skillList;
        
        List<pse__Skill_Certification_Rating__c> scrList = new List<pse__Skill_Certification_Rating__c>();
        List<Contact> contactList = [Select Id From Contact where Backup_OWF_User__c != null];      
        System.runAs(new User(Id = UserInfo.getUserId())) {
            for(pse__Skill__c skill : skillList) {
                pse__Skill_Certification_Rating__c scr = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
                scr.pse__Rating__c = '5 - Expert';
                scrList.add(scr);
            }
            insert scrList;
        }
        
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        resourceRequest.subgroup__c = 'GBO-Lead PD';
        Test.startTest();
            insert resourceRequest;
            oppty.Potential_Regions__c = 'USA/Canada';
            update oppty;
            pse__Resource_Request__c resourceRequest2 = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
            resourceRequest2.pse__Group__c = grp1.Id;
            resourceRequest2.subgroup__c = 'TSL-Japan';
            insert resourceRequest2;
        Test.stopTest();
    }
    
    /**
     * This test method used to populate Project On RR
     */
    static testMethod void testPopulateProjectOnRR() {
        List<pse__Skill__c> skillList = new List<pse__Skill__c>();
        pse__Skill__c skill1 = UTL_OWF_TestData.createSkills('Consulting', 'Line of Business');
        pse__Skill__c skill2 = UTL_OWF_TestData.createSkills('USA/Canada', 'User Regional Geographic Working Scope');
        skill2.pse__type__c = 'User Regional Geographic Working Scope';
        pse__Skill__c skill3 = UTL_OWF_TestData.createSkills('Test Indication', 'Indication');
        pse__Skill__c skill4 = UTL_OWF_TestData.createSkills('Acute Care', 'Therapy Area');
        skillList.add(skill1);
        skillList.add(skill2);
        skillList.add(skill3);
        skillList.add(skill4);
        insert skillList;
        
        List<pse__Skill_Certification_Rating__c> scrList = new List<pse__Skill_Certification_Rating__c>();
        List<Contact> contactList = [Select Id From Contact where Backup_OWF_User__c != null];      
        System.runAs(new User(Id = UserInfo.getUserId())) {
            for(pse__Skill__c skill : skillList) {
                pse__Skill_Certification_Rating__c scr = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
                scr.pse__Rating__c = '5 - Expert';
                scrList.add(scr);
            }
            insert scrList;
        }
        
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, null);
        resourceRequest.pse__Group__c = grp1.Id;
        resourceRequest.subgroup__c = 'GBO-Lead PD';
        Test.startTest();
            insert resourceRequest;
            oppty.Potential_Regions__c = 'USA/Canada';
            update oppty;
            pse__Resource_Request__c resourceRequest2 = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, null);
            resourceRequest2.pse__Group__c = grp1.Id;
            resourceRequest2.subgroup__c = 'TSL-Japan';
            insert resourceRequest2;
        Test.stopTest();
    }
    
     /**
     * This test method used to populate Project On RR
     */
    static testMethod void testPopulateRRFieldsFromAgreement() {
        List<pse__Skill__c> skillList = new List<pse__Skill__c>();
        pse__Skill__c skill1 = UTL_OWF_TestData.createSkills('Consulting', 'Line of Business');
        pse__Skill__c skill2 = UTL_OWF_TestData.createSkills('USA/Canada', 'User Regional Geographic Working Scope');
        skill2.pse__type__c = 'User Regional Geographic Working Scope';
        pse__Skill__c skill3 = UTL_OWF_TestData.createSkills('Test Indication', 'Indication');
        pse__Skill__c skill4 = UTL_OWF_TestData.createSkills('Acute Care', 'Therapy Area');
        skillList.add(skill1);
        skillList.add(skill2);
        skillList.add(skill3);
        skillList.add(skill4);
        insert skillList;
        
        List<pse__Skill_Certification_Rating__c> scrList = new List<pse__Skill_Certification_Rating__c>();
        List<Contact> contactList = [Select Id From Contact where Backup_OWF_User__c != null];      
        System.runAs(new User(Id = UserInfo.getUserId())) {
            for(pse__Skill__c skill : skillList) {
                pse__Skill_Certification_Rating__c scr = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, contactList.get(0).Id);
                scr.pse__Rating__c = '5 - Expert';
                scrList.add(scr);
            }
            insert scrList;
        }
        
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        List<pse__Resource_Request__c> rrList = [Select Id from pse__Resource_Request__c where Agreement__c =: agreement.Id ];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, null);
        resourceRequest.pse__Group__c = grp1.Id;
        resourceRequest.pse__Opportunity__c = null;
        resourceRequest.subgroup__c = 'TSL-Japan';
        Test.startTest();
            insert resourceRequest;
        Test.stopTest();
        system.assertEquals(rrList.size() + 1 , [Select Id from pse__Resource_Request__c where Agreement__c =: agreement.Id ].size());
    }
}