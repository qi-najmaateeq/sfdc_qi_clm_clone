/**
 * This test class is used to test all methods in LineItemGroup trigger.
 * version : 1.0
 */
@isTest
private class TST_DAO_LineItemGroup {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Contact cnt = UTL_TestData.createContact(acc.Id);
        insert cnt;
        Indication_List__c indication = UTL_OWF_TestData.createIndication('Test Indication', 'Acute Care');
        insert indication;
        Line_Item_Group__c lineItemGroup = UTL_OWF_TestData.createLineItemGroup(indication.Id);
        insert lineItemGroup;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        opp.Line_Item_Group__c = lineItemGroup.Id;
        insert opp;
        Product2 product = UTL_TestData.createProduct();
        product.Offering_Group_Code__c = CON_CRM.PRODUCT_OFFERING_GROUP_CODE;
        insert product;
        PricebookEntry pbEntry = UTL_TestData.createPricebookEntry(product.Id);
        insert pbEntry;
        upsert new RWEStudyProduct__c(Name='Q_100339', Product_Code__c='Q_100339');
        OpportunityLineItem oppLineItem = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
        insert oppLineItem;
    }
    
    /**
     * This test method used for insert opportunity record and update Probability on opportunity record
     */ 
    static testMethod void testOpportunityUpdateStage() {
        Opportunity opp = [SELECT id , stageName FROM Opportunity WHERE name = 'TestOpportunity' LIMIT 1];
        Test.startTest();
            opp.Line_of_Business__c = 'Core Clinical';
            update opp;
        Test.stopTest();
        List<Opportunity> OppList = [SELECT id , stageName FROM Opportunity WHERE name = 'TestOpportunity'];
        Integer expected = OppList.size();
        Integer actual = 1;
        System.assertEquals(expected, actual);
    }
    
    /**
     * This test method used for update LineItemGroup record's Phase field
     */ 
    static testMethod void testLineItemGroupUpdatePhase() {
        Line_Item_Group__c lineItemGroup = [SELECT id , Phase__c FROM Line_Item_Group__c limit 1];
        
        Test.startTest();
            lineItemGroup.Phase__c = 'Phase 1';
            update lineItemGroup;
        Test.stopTest();
    }
    static testMethod void testLineItemGroupUpdate1() {
        Line_Item_Group__c lineItemGroup = [SELECT id , Phase__c FROM Line_Item_Group__c limit 1];
        
        Test.startTest();
            lineItemGroup.Phase__c = 'Phase 1';
            lineItemGroup.RWE_Study__c = true;
            update lineItemGroup;
        Test.stopTest();
    }
}