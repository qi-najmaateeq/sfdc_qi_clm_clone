@isTest
private class TST_MI_BNF_Delete {
    
    @testSetup static void setupTestData(){
        Current_Release_Version__c crv = new Current_Release_Version__c();
        crv.Current_Release__c = '3000.01';
        upsert crv;
        
        Global_Variables.PCFlag = false;
        Global_Variables.RunQuickOppLimitTrigger = false;
        Global_Variables.RenewalCloneFlag = TRUE;
        Global_Variables.syncAgreementToOppty=false;
        Global_Variables.PCTrigger_Hault_Execution=false;
        
        Account TestAccount = BNF_Test_Data.createAccount();
        List<Address__c> TestAddress_Array = BNF_Test_Data.createAddress_Array();
        List<SAP_Contact__c> TestSapContact_Array = BNF_Test_Data.createSapContact_Array();
        Opportunity opp = BNF_Test_Data.createOpp();
        BNF_Settings__c bnfsetting = BNF_Test_Data.createBNFSetting();
        List<User_Locale__c> User_LocaleSetting = BNF_Test_Data.create_User_LocaleSetting();
        List<OpportunityLineItem> OLI_Array = BNF_Test_Data.createOppLineItem();
        User u = BNF_Test_Data.createUser();
        Revenue_Analyst__c TestLocalRA = BNF_Test_Data.createRA();
        BNF2__c TestBNF = BNF_Test_Data.createBNF();
        MIBNF2__c TestMIBNF = BNF_Test_Data.createMIBNF();
        MIBNF_Component__c TestMIBNF_Comp = BNF_Test_Data.createMIBNF_Comp();
        MI_BNF_LineItem__c TestMI_BNFLineItem = BNF_Test_Data.createMI_BNF_LineItem();
        List<Billing_Schedule__c> billingSchedule = BNF_Test_Data.createBillingSchedule();
        List<Billing_Schedule_Item__c> billingScheduleItem = BNF_Test_Data.createBillingScheduleItem();
    } 
    
    static testmethod void  testMIBNFCompPDF() {
        
        Test.startTest();
        MIBNF_Component__c mibnfComp = [Select id ,MIBNF__c ,Addendum__c,Revised_BNF_Reason__c,  Revised_BNF_Comment__c, Revised_BNF_Date__c,Orignal_BNF__c, Name ,Opportunity__C from MIBNF_Component__c limit 1];
        MIBNF_Component__c TestMIBNF_Comp=new MIBNF_Component__c();
        System.debug('Name = '+mibnfComp.Name);
        TestMIBNF_Comp.Name = mibnfComp.Name;
        TestMIBNF_Comp.MIBNF__c= mibnfComp.MIBNF__c;
        TestMIBNF_Comp.BNF_Status__c='RA_ACCEPTED';
        TestMIBNF_Comp.Is_this_a_retainer_downpayment__c='No';
        TestMIBNF_Comp.Print_Shop__c='No';
     
        TestMIBNF_Comp.Opportunity__C = mibnfComp.Opportunity__C;
        insert TestMIBNF_Comp;
        
        TestMIBNF_Comp.BNF_Status__c='SAP Contract Confirmed';
        upsert TestMIBNF_Comp;
        mibnfComp.Addendum__c=true;
        mibnfComp.Revised_BNF_Comment__c = 'Test';
        mibnfComp.Revised_BNF_Reason__c = 'Cancellation';
        mibnfComp.Revised_BNF_Date__c = System.today();
        mibnfComp.Orignal_BNF__c = TestMIBNF_Comp.Id;
        upsert mibnfComp;
        MIBNF2__c mibnfRec = [Select id from MIBNF2__c limit 1];
        try {
            delete mibnfRec; 
            System.assert(false,'code never reached here');
        }
        catch(DmlException e) {
            System.assert(true, 'custom validation error occured');
        }
    }
    
    

    
}