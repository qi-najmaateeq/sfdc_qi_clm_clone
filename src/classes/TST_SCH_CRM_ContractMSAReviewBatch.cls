@isTest
public class TST_SCH_CRM_ContractMSAReviewBatch {
    
    /**
     * method to create data for SCH_CRM_ContractMSAReviewBatch
     */ 
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        List<User> userList = UTL_TestData.createUser('System Administrator',1);
        insert userList;
        Contract contract = new Contract();
        contract.OwnerId = userList[0].Id;
        contract.AccountId = acc.Id;
        contract.Date_on_which_MSA_should_be_reviewed__c = System.today();
        contract.Parent_Contract_Number__c = 1234;
        contract.Ultimate_Parent_Contract_Number__c = 5678;
        insert contract;
    }
    
    /**
     * method to test SCH_CRM_ContractMSAReviewBatch
     */ 
    public static testMethod void testScheduler() {
        Test.StartTest();
        String cronExp = '0 0 23 * * ?'; 
        System.schedule('TST_SCH_CRM_ContractMSAReviewBatch', cronExp, new SCH_CRM_ContractMSAReviewBatch()); 
        Test.stopTest();
    }
}