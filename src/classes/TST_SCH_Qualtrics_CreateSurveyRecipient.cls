@isTest
private class TST_SCH_Qualtrics_CreateSurveyRecipient {
    
    @IsTest
    static void schedulerTest() 
    {
        Test.startTest();
            
            String CRON_EXP = '0 0 2 * * ?';
            String jobId = System.schedule('SCH_Qualtrics_CS_CreateSurveyRecipient', 
                           CRON_EXP, new SCH_Qualtrics_CreateSurveyRecipient());    
        Test.stopTest();

        system.assertEquals(1, [SELECT Id FROM CronTrigger WHERE id = :jobId].size(), 'Batch is schedule');
    }
}