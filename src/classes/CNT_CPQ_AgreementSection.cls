public class CNT_CPQ_AgreementSection {
    static Set<String> TSSUApprover = new Set<String>();
    static Set<String> SalesAndAccountApprover = new Set<String>();
    static Set<String> PLApprover = new Set<String>();

    @auraEnabled
    public static String getAction(Id recordId) {
        String laborFee = '';
        Apttus__APTS_Agreement__c agreement = getAgreement(recordId);
        laborFee = getLaborFee(agreement.Labor_Fees__c);
        List<Challenge_Matrix__c> challengeMatrix = new List<Challenge_Matrix__c>();
        if(agreement.Process_Step__c == CON_CPQ.APPROVAL_REVIEW && agreement.Agreement_Status__c == CON_CPQ.PENDING_APPROVAL)
        {
            challengeMatrix = new SLT_ChallangeMatrix().selectChallangeMatrixCondition(laborFee, agreement.Opportunity_Type__c, CON_CPQ.REVIEW_TYPE_SIGNOFF);
        } 
        else {
            challengeMatrix = new SLT_ChallangeMatrix().selectChallangeMatrixCondition(laborFee, agreement.Opportunity_Type__c, CON_CPQ.REVIEW_TYPE_CHALLENGE);
        }
        if(challengeMatrix.size()>0) {
            return challengeMatrix[0].Action__c;
        }
        return null;
    }

    @auraenabled
    public static List<List<String>> getApproverData(Id recordId) {
        List<List<String>> approverData = new List<List<String>>();
        Apttus__APTS_Agreement__c agreement = getAgreement(recordId);
        List<Approval_Matrix__c> approvalMatrixListForTSSU = getApprovalMatrixList(agreement, CON_CPQ.APPROVER_GROUP_TSSU);
        List<Approval_Matrix__c> approvalMatrixListForSalesAndAccounts = getApprovalMatrixList(agreement, CON_CPQ.APPROVER_GROUP_SALES_AND_ACCOUNT_MANAGEMENT);
        List<Approval_Matrix__c> approvalMatrixListForPL = getApprovalMatrixList(agreement, '');

        if(approvalMatrixListForTSSU.size()>0) {
            for(Approval_Matrix__c tssuApprovalMatrix : approvalMatrixListForTSSU) {
                //TSSU Data
                if(tssuApprovalMatrix.Therapy_Area__c == agreement.Apttus__Related_Opportunity__r.Therapy_Area__c
                    && tssuApprovalMatrix.Approver_Group__r.Name == CON_CPQ.APPROVER_GROUP_TSSU) {
                    if(agreement.Labor_Fees__c<=5 && tssuApprovalMatrix.X0_5M_USD__c != CON_CPQ.N_A ) {
                        TSSUApprover.add(tssuApprovalMatrix.X0_5M_USD__c);
                    } else if(agreement.Labor_Fees__c>5 && agreement.Labor_Fees__c<=10 && tssuApprovalMatrix.X5_10M_USD__c != CON_CPQ.N_A) {
                        TSSUApprover.add(tssuApprovalMatrix.X5_10M_USD__c);
                    } else if(agreement.Labor_Fees__c>10 && agreement.Labor_Fees__c<=20 && tssuApprovalMatrix.X10_20M_USD__c != CON_CPQ.N_A) {
                        TSSUApprover.add(tssuApprovalMatrix.X10_20M_USD__c);
                    } else if(agreement.Labor_Fees__c>20 && agreement.Labor_Fees__c<=50 && tssuApprovalMatrix.X20_50M_USD__c != CON_CPQ.N_A) {
                        TSSUApprover.add(tssuApprovalMatrix.X20_50M_USD__c);
                    } else if(agreement.Labor_Fees__c>50 && tssuApprovalMatrix.X50M_USD__c != CON_CPQ.N_A) {
                        TSSUApprover.add(tssuApprovalMatrix.X50M_USD__c);
                    }
                }
            }
        }

        //Sales and Account data
        if(approvalMatrixListForSalesAndAccounts.size()>0) {
            for(Approval_Matrix__c salesApprovalMatrix : approvalMatrixListForSalesAndAccounts) {
                if(salesApprovalMatrix.Approver_Group__r.Name == CON_CPQ.APPROVER_GROUP_SALES_AND_ACCOUNT_MANAGEMENT &&
                    compareCategory(salesApprovalMatrix.Sales__c, agreement.Apttus__Account__r.Category__c)){
                    if(agreement.Labor_Fees__c<=5 && salesApprovalMatrix.X0_5M_USD__c != CON_CPQ.N_A) {
                        SalesAndAccountApprover.add(salesApprovalMatrix.X0_5M_USD__c);
                    } else if(agreement.Labor_Fees__c>5 && agreement.Labor_Fees__c<=10 && salesApprovalMatrix.X5_10M_USD__c != CON_CPQ.N_A) {
                        SalesAndAccountApprover.add(salesApprovalMatrix.X5_10M_USD__c);
                    } else if(agreement.Labor_Fees__c>10 && agreement.Labor_Fees__c<=20 && salesApprovalMatrix.X10_20M_USD__c != CON_CPQ.N_A) {
                        SalesAndAccountApprover.add(salesApprovalMatrix.X10_20M_USD__c);
                    } else if(agreement.Labor_Fees__c>20 && agreement.Labor_Fees__c<=50 && salesApprovalMatrix.X20_50M_USD__c != CON_CPQ.N_A) {
                        SalesAndAccountApprover.add(salesApprovalMatrix.X20_50M_USD__c);
                    } else if(agreement.Labor_Fees__c>50 && salesApprovalMatrix.X50M_USD__c != CON_CPQ.N_A){
                        SalesAndAccountApprover.add(salesApprovalMatrix.X50M_USD__c);
                    }
                }
            }
        }

        //PL Data
        if(approvalMatrixListForPL.size()>0) {
            for(Approval_Matrix__c plApprovalMatrix : approvalMatrixListForPL) {
                if(plApprovalMatrix.Approver_Group__r.Name == CON_CPQ.APPROVER_GROUP_PL && plApprovalMatrix.Therapy_Area__c == agreement.Apttus__Related_Opportunity__r.Therapy_Area__c){
                    if(agreement.Labor_Fees__c<=5 && plApprovalMatrix.X0_5M_USD__c != 'N/A') {
                        PLApprover.add(plApprovalMatrix.X0_5M_USD__c);
                    } else if(agreement.Labor_Fees__c>5 && agreement.Labor_Fees__c<=10 && plApprovalMatrix.X5_10M_USD__c != CON_CPQ.N_A) {
                        PLApprover.add(plApprovalMatrix.X5_10M_USD__c);
                    } else if(agreement.Labor_Fees__c>10 && agreement.Labor_Fees__c<=20 && plApprovalMatrix.X10_20M_USD__c != CON_CPQ.N_A) {
                        PLApprover.add(plApprovalMatrix.X10_20M_USD__c);
                    } else if(agreement.Labor_Fees__c>20 && agreement.Labor_Fees__c<=50 && plApprovalMatrix.X20_50M_USD__c != CON_CPQ.N_A) {
                        PLApprover.add(plApprovalMatrix.X20_50M_USD__c);
                    } else if(agreement.Labor_Fees__c>50 && plApprovalMatrix.X50M_USD__c != CON_CPQ.N_A){
                        PLApprover.add(plApprovalMatrix.X50M_USD__c);
                    }
                }
            }
        }
        approverData.add(new List<String>(TSSUApprover));
        approverData.add(new List<String>(SalesAndAccountApprover));
        approverData.add(new List<String>(PLApprover));
        return approverData;
    }

    @testVisible
    private static List<Approval_Matrix__c> getApprovalMatrixList(Apttus__APTS_Agreement__c agreement, String approverGroup) {
        String region = getUserRegion(agreement);
        List<Approval_Matrix__c> approvalMatrixList = new List<Approval_Matrix__c>();
        if(approverGroup == CON_CPQ.APPROVER_GROUP_TSSU) {
            approvalMatrixList = new SLT_ApprovalMatrix().selectApprovalMatrixCondition(agreement.Opportunity_Type__c, agreement.User__r.Region__c);
        } else if(approverGroup == CON_CPQ.APPROVER_GROUP_SALES_AND_ACCOUNT_MANAGEMENT) {
            approvalMatrixList = new SLT_ApprovalMatrix().selectApprovalMatrixCondition(agreement.Opportunity_Type__c, region);
        } else {
            approvalMatrixList = new SLT_ApprovalMatrix().selectApprovalMatrixByOpportunityType(agreement.Opportunity_Type__c);
        }
        return approvalMatrixList;
    }

    //Yuli 03/04/2019 added OwnerId, because getUserRegion() needs it
    @testVisible
    private static Apttus__APTS_Agreement__c getAgreement(Id recordId) {
        Apttus__APTS_Agreement__c agreement = new SLT_Agreement().getAgreementDetails(recordId, new Set<String> {CON_CPQ.ID, 
            CON_CPQ.NAME, CON_CPQ.LABOR_FEES, CON_CPQ.AGREEMENT_OPPORTUNITY_TYPE, CON_CPQ.INDICATION, CON_CPQ.RELATED_OPPORTUNITY_THERAPY_AREA,
            CON_CPQ.AGREEMENT_PROCESS_STEP, CON_CPQ.AGREEMENT_AGREEMENT_STATUS, CON_CPQ.OWNER, CON_CPQ.AGREEMENT_USER_REGION,
            CON_CPQ.AGREEMENT_ACCOUNT_CATEGORY});
        return agreement;
    }

    @testVisible
    private static String getUserRegion(Apttus__APTS_Agreement__c agreement){
        Set<String> fieldSet = new Set<String>{CON_CPQ.REGION} ;
        Map<Id, User> userMap = new SLT_User().selectByUserId(new Set<Id>{agreement.OwnerId}, fieldSet);
        return userMap.get(agreement.OwnerId).Region__c;
    }

    @testVisible
    private static Boolean compareCategory(String sales, String accountCategory) {
        if(sales == CON_CPQ.APPROVER_SALES_EMERGING_BIOPHARMA && (accountCategory == CON_CPQ.ACCOUNT_CATEGORY_EBP_EMERGING_BIOPHARMA ||
            accountCategory == CON_CPQ.ACCOUNT_CATEGORY_EBPX_EMERGING_BIOPHARMA || accountCategory == CON_CPQ.ACCOUNT_CATEGORY_LOCAL_PHARMA)){
            return true;
        } else if(sales == CON_CPQ.APPROVER_SALES_LOCAL_PHARMA && (accountCategory == CON_CPQ.ACCOUNT_CATEGORY_GLOBAL_PHARMA ||
            accountCategory == CON_CPQ.ACCOUNT_CATEGORY_LARGE_PHARMA)){
            return true;
        } else if(sales == CON_CPQ.APPROVER_SALES_MID_SIZED && (accountCategory == CON_CPQ.ACCOUNT_CATEGORY_DNU_OTHER_MIDSIZED_PHARMA ||
            accountCategory == CON_CPQ.ACCOUNT_CATEGORY_MID_SIZE_PHARMA || accountCategory == CON_CPQ.ACCOUNT_CATEGORY_MIDSIZED_GROWTH || accountCategory == CON_CPQ.ACCOUNT_CATEGORY_OTHER_MIDSIZED_PHARMA)){
            return true;
        } else {
            return false;
        }
    }
    
    @testVisible
    private static String getLaborFee(Decimal laborFees) {
        String laborFee;
        if(laborFees<=3) {
            laborFee = CON_CPQ.LABOR_FEES_0TO3;
        } else if(laborFees>3 && laborFees<=5) {
            laborFee = CON_CPQ.LABOR_FEES_3TO5;
        } else if(laborFees>5 && laborFees<=10) {
            laborFee = CON_CPQ.LABOR_FEES_5TO10;
        } else if(laborFees>10 && laborFees<=20) {
            laborFee = CON_CPQ.LABOR_FEES_10TO20;
        } else if(laborFees>20 && laborFees<=25) {
            laborFee = CON_CPQ.LABOR_FEES_20TO25;
        } else if(laborFees>25 && laborFees<=50) {
            laborFee = CON_CPQ.LABOR_FEES_25TO50;
        } else {
            laborFee = CON_CPQ.LABOR_FEES_50;
        }
        return laborFee;
    }
}