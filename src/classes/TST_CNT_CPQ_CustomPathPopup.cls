@isTest
private class TST_CNT_CPQ_CustomPathPopup {

    static Apttus__APTS_Agreement__c getAgreementData(Id OpportuntiyId, String recordTypeName){

        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_FINAL;
        testAgreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        testOpportunity.Opportunity_Type__c = COn_CPQ.OPPORTUNITY_RFP;
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }

    static user setUserData(String userRegion){

        String profileName = 'Sales User';
        User usr = UTL_TestData.createUser(profileName, 1)[0];
        usr.Region__c = userRegion;
        insert usr;
        return usr;
    } 

    static CPQ_Settings__c setCPqSetttingData(){
        CPQ_Settings__c cpqSetting = UTL_TestData.createCPQSettings();
        cpqSetting.Approval_Email_Service__c = 'test@test.com';
        cpqSetting.Region_AMR_Email__c = 'test@test.com';
        cpqSetting.Region_EMEA_Email__c = 'test@test.com';
        insert cpqSetting;
        return cpqSetting;
    }

    static Document setDocumentData(){
        Document documentRecord = UTL_TestData.createDocument('QC Check List', 'QC_Check_List', 'application/pdf');
        documentRecord.IsPublic = true;
        documentRecord.FolderId = UserInfo.getUserId();
        insert documentRecord;
        return documentRecord;
    }

    static Apttus__APTS_Agreement__c getAgreementData(Id agreementId){
        return [SELECT Id, Agreement_Status__c, Process_Step__c, Data_Access_End_Date__c FROM Apttus__APTS_Agreement__c WHERE Id =: agreementId];
    }

    @isTest
    static void testUpdateAgreementPrcoessStepShouldUpdateStep(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER);
        insert agreement;

        Test.startTest();
            CNT_CPQ_CustomPathPopup.updateAgreementPrcoessStep(agreement.Id, CON_CPQ.FINAL_QC, CON_CPQ.FINAL_PREPARATION);
        Test.stopTest();

        Apttus__APTS_Agreement__c updatedAgreement = getAgreementData(agreement.Id);
        system.assertEquals(CON_CPQ.FINAL_QC, updatedAgreement.Process_Step__c, 'Should Update Process Step');
    }

    @isTest
    static void testUpdateAgreementStrategyDateShouldUpdateDate(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER);
        insert agreement;

        Test.startTest();
            CNT_CPQ_CustomPathPopup.updateAgreementStrategyDate(agreement.Id, ''+System.today());
        Test.stopTest();

        Apttus__APTS_Agreement__c updatedAgreement = getAgreementData(agreement.Id);
        system.assertEquals(System.today(), updatedAgreement.Data_Access_End_Date__c, 'Should Update Date');
    }

    @isTest
    static void testSendFinalQCMailShouldSendMailAccordingToOwnerRegionEMEA(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.OwnerId = setUserData(CON_CPQ.REGION_EMEA).Id;
        insert agreement;
        setCPqSetttingData();

        Test.startTest();
            CNT_CPQ_CustomPathPopup.sendFinalQCMail(agreement.Id);
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertEquals(1, invocations, 'Should Send Mail According To Region EMEA');
    }

    @isTest
    static void testSendFinalQCMailShouldSendMailAccordingToOwnerRegionAMR(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.OwnerId = setUserData(CON_CPQ.REGION_AMR).Id;
        insert agreement;
        setCPqSetttingData();

        Test.startTest();
            CNT_CPQ_CustomPathPopup.sendFinalQCMail(agreement.Id);
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertEquals(1, invocations, 'Should Send Mail According To Region AMR');
    }

    @isTest
    static void testSendMailShouldSendMail(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Project_Manager_Email__c = 'test@test.com';
        insert agreement;
        setCPqSetttingData();
        setDocumentData();

        Test.startTest();
            CNT_CPQ_CustomPathPopup.sendMail(agreement.Id);
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertEquals(1, invocations, 'Should Send Mail');
    }
}