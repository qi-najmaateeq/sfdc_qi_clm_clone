@isTest
public class TST_CNT_CPQ_RequestToUnlockAgreement {

    static Group setGroupData(){
        Group testGroup = UTL_TestData.createGroup(CON_CPQ.CPQ_ADMINS_FOR_BUDGET_UNLOCKING, 'Regular');
        insert testGroup; 
        return testGroup;
    }

    static GroupMember setGroupMemberData(Id groupId){
    
        GroupMember testGroupMember = UTL_TestData.createGroupMember(groupId, UserInfo.getUserId());
        Insert testGroupMember; 
        return testGroupMember;
    }
    
    static Apttus__APTS_Agreement__c getAgreementData(Id OpportuntiyId){

        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.Mark_as_Primary__c = true;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.Pricing_Tool_Locked__c = true;
        testAgreement.Budget_Checked_Out_By__c = UserInfo.getUserId();
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }
    
    static EmailTemplate setEmailTemplate(String developerName){
        EmailTemplate testEmailTemplate = UTL_TestData.createEmailTemplate('test template', developerName, 'custom');
        testEmailTemplate.FolderId = UserInfo.getUserId();
        testEmailTemplate.Body = 'test';
        insert testEmailTemplate;
        return testEmailTemplate;
    }
    
    static Apttus__APTS_Agreement__c getAgreement(Id agreementId){
        return [SELECT Pricing_Tool_Locked__c FROM Apttus__APTS_Agreement__c WHERE Id =: agreementId];
    }

    @isTest
    static void testIsNotifyCurrentXAEOwnerShouldReturnTrue(){

        Test.startTest();
            Boolean isNotify = CNT_CPQ_RequestToUnlockAgreement.isNotifyCurrentXAEOWner();
        Test.stopTest();

        System.assertEquals(true, isNotify, 'Owner is notified');
    }

    @isTest
    static void testIsNotifyCurrentXAEOwnerShouldReturnFalse(){

        Group adminGroup = setGroupData();
        GroupMember adminGroupMember = setGroupMemberData(adminGroup.Id);
        
        Test.startTest();
            Boolean isNotify = CNT_CPQ_RequestToUnlockAgreement.isNotifyCurrentXAEOWner();
        Test.stopTest();

        System.assertEquals(false, isNotify, 'Owner is not notified');
    }

    @isTest
    static void testNotifyXAEOwnerShouldSendEmailToBudgetOwner(){

        List<User> thisUser = new SLT_User().selectById(new Set<Id>{UserInfo.getUserId()});  
        Apttus__APTS_Agreement__c agreement;
        System.runAs(thisUser[0])  { 
            Account testAccount = setAccountData();
            Opportunity testOpportunity = setOpportunityData(testAccount.Id);
            agreement = getAgreementData(testOpportunity.Id);
            insert agreement;
        }
        Group adminGroup = setGroupData();
        GroupMember adminGroupMember = setGroupMemberData(adminGroup.Id);
        EmailTemplate ownerTemplate = setEmailTemplate('CPQ_Another_User_Request_To_Unlock_Budget');
        EmailTemplate adminTemplate = setEmailTemplate('Notify_System_Admin_About_Lock_Agreement');

        Test.startTest();
            CNT_CPQ_RequestToUnlockAgreement.notifyXAEOwner(UserInfo.getUserId(), agreement.Id);
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertEquals(1, invocations, 'Email is Sent to budget owner');
    }

    @isTest
    static void testUnlockAgreementShouldUnlockAgreement(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id);
        insert agreement;

        Test.startTest();
            CNT_CPQ_RequestToUnlockAgreement.unlockAgreement(UserInfo.getUserId(), agreement.Id);
        Test.stopTest();
        
        Apttus__APTS_Agreement__c agreementRecord = getAgreement(agreement.Id);
        System.assertEquals(false,  agreementRecord.Pricing_tool_Locked__c, 'Agreement is unlocked');
    }
    
}