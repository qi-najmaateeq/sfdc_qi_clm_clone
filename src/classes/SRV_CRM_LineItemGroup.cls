/*
 * Version       : 1.0
 * Description   : Service Class for PriceBookEntry
 */
global class SRV_CRM_LineItemGroup {
	
    /**
     * This method used to get required fields for LIG based on OpportunityStage custom setting(Expected Stage).
     * @params  String recordId
     * @params  String expectedStage
     * @return  Set<String> 
     */
    global static List<String> getListOfRequiredFields(Id recordId, String expectedStage, String expectedLineOfBuisness) {
        Map<String, String> mapOfFieldAPIWithErrorMessage = new Map<String, String>();
        try {
        	mapOfFieldAPIWithErrorMessage = DAOH_CRM_LineItemGroup.validateLineItemGroup(recordId, expectedStage, null, expectedLineOfBuisness);
        } catch(Exception ex) {
            throw new LineItemGroupServiceException(new List<String>{ex.getMessage()});
        }
        return new List<String>(mapOfFieldAPIWithErrorMessage.keySet());
    }
    
    global static Line_Item_Group__c getLIGRecordDetail(Set<Id> oppIdSet, Set<String> ligFieldSet) { 
        List<Line_Item_Group__c> lineItemGroupList = new List<Line_Item_Group__c>();
        Line_Item_Group__c ligRecord = new Line_Item_Group__c();
        try {
            lineItemGroupList = new SLT_LineItemGroup(false, false).selectByOpportunityId(oppIdSet, ligFieldSet);
            if(lineItemGroupList.size() > 0) {
                ligRecord = lineItemGroupList[0];
            }
        } catch (exception ex) {
            throw new LineItemGroupServiceException(new List<String>{ex.getMessage()});
        }
        return ligRecord;
    }
    
    // Exception Class for Line Item Group Service
    public Class LineItemGroupServiceException extends Exception {
        
        List<String> errorList;
        
        /**
         * constructor
         * @params  List<String> errorList
         */ 
        public LineItemGroupServiceException(List<String> errorList) {
            this.errorList = errorList;
        }
    }
}