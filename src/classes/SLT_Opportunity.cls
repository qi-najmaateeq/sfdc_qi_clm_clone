/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Opportunity
 */
public class SLT_Opportunity extends fflib_SObjectSelector {
    
    /**
     * constructor to initialize CRUD and FLS
     */
    public SLT_Opportunity() {
        super(false, true, false);
    }
    
   /**
    * constructor to initialize CRUD and FLS
    */
   public SLT_Opportunity(Boolean enforceCRUD, Boolean enforceFLS) {
       super(false, enforceCRUD, enforceFLS);
   }
    
    /**
     * This method used to get field list of sobject
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Opportunity.sObjectType;
    }
    
    /**
     * This method used to get Opportunity by Id
     * @return  Map<Id, Opportunity>
     */
    public Map<Id, Opportunity> getOpportunityById(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    /**
     * This method used to get Opportunity by with Opportunity Splits and OpportunityTeam Members
     * @params  Set<Id> oppIdset
     * @params  Set<String> oppfieldSet
     * @params  Set<String> oppSplitFieldSet
     * @params  Set<String> oppTeamFieldSet
     * @params  String splitType
     * @return  Map<Id, Opportunity>
     */
    public Map<Id, Opportunity> selectByIdWithOpportuntiyTeamSplits(Set<ID> oppIdSet, Set<String> oppfieldSet, Set<String> oppSplitFieldSet, Set<String> oppTeamFieldSet, String splitType) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        new SLT_OpportunitySplit().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNTIY_SPLITS, true).selectFields(oppSplitFieldSet).setCondition('SplitType.DeveloperName = :splitType');
        new SLT_OpportunityTeamMember().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITY_TEAM_MEMBERS, true).selectFields(oppTeamFieldSet);
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
    
    /**
     * This method used to get Opportunity by with Opportunity line Items.
     * @params  Set<Id> oppIdset
     * @params  Set<String> oppfieldSet
     * @params  Set<String> oliFieldSet
     * @return  Map<Id, Opportunity>
     */
    public Map<Id, Opportunity> selectByIdWithOpportuntiyLineItem(Set<ID> oppIdSet, Set<String> oppfieldSet, Set<String> oliFieldSet) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        new SLT_OpportunityLineItems().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITY_LINE_ITEMS, true).selectFields(oliFieldSet);
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
    
    public Map<Id, Opportunity> selectByIdWithOpportuntiyLineItemFilter(Set<ID> oppIdSet, Set<String> oppfieldSet, Set<String> oliFieldSet, String oliCondition, Integer oliLimit) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        new SLT_OpportunityLineItems().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITY_LINE_ITEMS, true).selectFields(oliFieldSet).setCondition(oliCondition).setOrdering(OpportunityLineItem.TotalPrice, fflib_QueryFactory.SortOrder.DESCENDING, true).setLimit(oliLimit);
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
    
    public Map<Id, Opportunity> selectAgreementByOpportunity(Set<Id> oppIdSet, Set<String> oppfieldSet, Set<Id> agreementRecordTypes,Set<String> agreementFields) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        new SLT_Agreement().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITY_AGREEMENT_FIELD_API, true).selectFields(agreementFields).setCondition('RecordTypeId NOT IN : agreementRecordTypes');
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }

    /**
     * This method used to get Opportunity by with Agreements
     * @params  Set<Id> oppIdset
     * @params  Set<String> oppfieldSet
     * @params  Set<String> agreementFieldSet
     * @return  Map<Id, Opportunity>
     */
    public Map<Id, Opportunity> selectByIdWithOpportuntiyAgreement(Set<ID> oppIdSet, Set<String> oppfieldSet, Set<String> agreementFieldSet) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        fflib_QueryFactory.SortOrder fieldSortOrder = fflib_QueryFactory.SortOrder.DESCENDING;
        new SLT_APTS_Agreement().addQueryFactorySubselect(opportunitiesQueryFactory, CON_OWF.OPPORTUNITY_AGREEMENTS, true).selectFields(agreementFieldSet).setCondition('Bid_Number__c != NULL');
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
    
    /**
     * This method used to get Opportunity with Agreements using agr conditions
     * @params  Set<Id> oppIdset
     * @params  Set<String> oppfieldSet
     * @params  Set<String> agreementFieldSet
     * @params  String agrCondition
     * @return  Map<Id, Opportunity>
     */
    public Map<Id, Opportunity> selectByIdWithAgrCondtion(Set<ID> oppIdSet, Set<String> oppfieldSet, Set<String> agreementFieldSet, String agrCondition) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        fflib_QueryFactory.SortOrder fieldSortOrder = fflib_QueryFactory.SortOrder.DESCENDING;
        new SLT_APTS_Agreement().addQueryFactorySubselect(opportunitiesQueryFactory, CON_OWF.OPPORTUNITY_AGREEMENTS, true).selectFields(agreementFieldSet).setCondition(agrCondition);
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
    
    /**
     * This method used to get Opportunities with Agreements
     * @params  Set<Id> oppIdset
     * @params  Set<String> oppfieldSet
     * @params  Set<String> agreementFieldSet
     * @return  Map<Id, Opportunity>
     */
    public Map<Id, Opportunity> selectAgreementByOpportunities(Set<Id> oppIdSet, Set<String> oppfieldSet, Set<String> agreementFields) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        new SLT_Agreement().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITY_AGREEMENT_FIELD_API, true).selectFields(agreementFields);
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        System.debug('queryString' + queryString);
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
    /**
     * This method used to get Opportunities
     * @return  List<Opportunity>
     */
    public List<Opportunity> getOpportunityByFieldCondition(Set<String> fieldSet,List<String> fieldValueList) {
        return (List<Opportunity>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Opportunity_Number__c in :fieldValueList').toSOQL());
    }

    public Map<Id, Opportunity> selectByIdWithBNF(Set<ID> oppIdSet, Set<String> oppfieldSet, Set<String> bnfFieldSet) {
        Id BNF_RECORD_TYPE_SAP_INTEGRATED  = Schema.SObjectType.BNF2__c.getRecordTypeInfosByName().get('SAP SD Integrated').getRecordTypeId();
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        new SLT_BNF().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITY_BNF_RELATIONSHIP, true).selectFields(bnfFieldSet).setCondition('RecordtypeId =: BNF_RECORD_TYPE_SAP_INTEGRATED'); 
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
    public Map<Id, Opportunity> selectByIdWithMIBNF(Set<ID> oppIdSet, Set<String> oppfieldSet, Set<String> mibnfFieldSet) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        new SLT_MIBNF_Component().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITY_MIBNF_COMPONENT_RELATIONSHIP, true).selectFields(mibnfFieldSet);
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Id in :oppIdSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
	
    /**
     * This method used to get Opportunity by Id
     * @return  Map<Id, Account>
     */
    public Map<Id, Opportunity> selectByOpportunityId(Set<ID> oppIdSet, Set<String> fieldSet) {
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :oppIdSet').toSOQL()));
    }

    public Map<Id, Opportunity> selectByIdWithLineItemGroups(Set<String> oppfieldSet, List<String> oppFieldsGroupSet,Set<String> lineItemFields) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        new SLT_LineItemGroup().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITYLINEITEMGROUP_RELATIONSHIP, true).selectFields(lineItemFields);
        String queryString = opportunitiesQueryFactory.selectFields(oppfieldSet).setCondition('Legacy_Quintiles_Opportunity_Number__c in :oppFieldsGroupSet').toSOQL();
        return new Map<Id, Opportunity>((List<Opportunity>) Database.query(queryString));
    }
	
    public Map<Id, Opportunity> getMapOfOpportunitiesWithProjectsById(Set<Id> oppIdSet) {
        return new Map<Id, Opportunity>([SELECT (SELECT  Is_Project_Billable__c 
                                                 FROM Proxy_Projects__r WHERE Record_Type_Name__c =: CON_Qualtrics.RECORD_TYPE_PROJECT) 
                                         FROM Opportunity o 
                                         WHERE Id IN :oppIdSet 
                                        ]);
    }
}
