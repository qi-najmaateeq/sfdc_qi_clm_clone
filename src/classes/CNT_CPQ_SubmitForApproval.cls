public class CNT_CPQ_SubmitForApproval {

    @auraEnabled
    public static void sendEmailToApproveUser(Id agreementId) {

        String qipName = '%' + CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_QIP + '%';
        String nobiName = '%' + CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_NOBI + '%';
        String uptName = '%' + CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_UPT + '%';
        String contentType = '%' + CON_CPQ.ATTACHMENT_CONTENT_TYPE +'%';
        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_PROJECT_MANAGER_EMAIL, CON_CPQ.AGREEMENT_EMAIL_SUBJECT, CON_CPQ.AGREEMENT_EMAIL_BODY};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId}, fieldSet); 
        List<Attachment> attachmentList = [SELECT Id, Name, Body, ContentType, ParentId FROM Attachment WHERE parentId =: agreementId AND 
                                            ContentType like: contentType AND (Name like: qipName OR Name like: nobiName OR name like: uptName) 
                                            Order By CreatedDate Desc LIMIT 1];
        if(attachmentList.size() > 0 && agreementList.size() > 0 && agreementList[0].Project_Manager_Email__c != null){

            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
            if(cpqSetting != null && cpqSetting.Approval_Email_Service__c != null){
            
                Messaging.SingleEmailMessage mailToXAEOwner = new Messaging.SingleEmailMessage();                        
                mailToXAEOwner.setSubject(agreementList[0].Email_Subject__c);
                String emailBody = agreementList[0].Email_Body__c;
                emailBody+= '<div class="agreementId" style="display:none">'+agreementId+'</div>';
                mailToXAEOwner.setHTMLBody(emailBody);
                mailToXAEOwner.setToAddresses(new String[]{agreementList[0].Project_Manager_Email__c});
                mailToXAEOwner.setWhatId(agreementId);
                mailToXAEOwner.setReplyTo(cpqSetting.Approval_Email_Service__c);
                Attachment attachment = attachmentList[0];
                Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();
                fileAttachment.setFileName(attachment.Name);
                fileAttachment.setBody(attachment.body);
                fileAttachment.setContentType(attachment.ContentType);
                mailToXAEOwner.setFileAttachments(new List<Messaging.EmailFileAttachment>{fileAttachment});
                mailList.add(mailToXAEOwner);
                if(mailList.size() > 0)
                    Messaging.sendEmail(mailList);
            }
        }
    }
} 