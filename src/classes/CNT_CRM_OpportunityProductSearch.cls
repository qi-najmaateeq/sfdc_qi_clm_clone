/*
 * Version       : 1.0
 * Description   : Apex Controller for Opportunity Product Screens
 */
public class CNT_CRM_OpportunityProductSearch {
    
    public static Decimal decimalPlace{get;set;}
    public static Decimal decimalPlaceStep{get;set;}
    public CNT_CRM_OpportunityProductSearch(ApexPages.StandardController controller) {
        
    }
    
    /**
     * This method used to search product by given filter
     * @params  String fiterObjString
     * @params  List<String> priceBookEntryFieldList
     * @params  List<String> fieldsAPIList
     * @params  Integer recordLimit
     * @return  OpportunityWrapper
     */
    @AuraEnabled
    public static List<PriceBookEntryWrapper> getPriceBookEntriesBySearchFilter(String pbeWrapperJson, List<String> pbeFieldList, List<String> productFilterFieldList, List<String> pbeFilterFieldList, Integer recordLimit) {
        PriceBookEntryWrapper pbeWrapper = (PriceBookEntryWrapper)JSON.deserialize(pbeWrapperJson, PriceBookEntryWrapper.Class);
        List<PriceBookEntryWrapper> pbeWrapperList = null;
        try {
            pbeWrapperList = SRV_CRM_PriceBookEntry.getFilteredPriceBookEntries(pbeWrapper, new Set<String>(pbeFieldList), productFilterFieldList, pbeFilterFieldList, recordLimit);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return pbeWrapperList;
    }
    
    /**
     * This method used to search product by given filter
     * @params  Integer recordLimit
     * @return  List<OpportunityWrapper.PriceBookEntryWrapper>
     */
    @AuraEnabled
    public static List<PriceBookEntryWrapper> getFavoriteProducts(Id oppId) {
        List<PriceBookEntryWrapper> pbeWrapperList = null;
        try {
            pbeWrapperList = SRV_CRM_PriceBookEntry.getUserFavoriteProducts(oppId);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return pbeWrapperList;
    }
    
    /**
     * This method used to get field detail of product object
     * @params  String objectName
     * @params  List<String> fieldAPINameList
     * @return  List<ObjectFieldsWrapper>
     */
    @AuraEnabled
    public static List<OpportunityWrapper> getProductSearchInitData(String objectName, List<String> fieldAPINameList, Id opportunityId, List<String> oppFieldList, List<String> defaultProductfieldList) {
        List<OpportunityWrapper> oppWrapper = new List<OpportunityWrapper>();
        Map<Id, Opportunity> idToOpportunityMap = null;
        Opportunity opportunityRecord = new Opportunity();
        List<ObjectFieldsWrapper> objectFieldsWrapperList = null;
        Default_Product_Search__c defaultProductSearch = new Default_Product_Search__c();
        List<Default_Product_Search__c> defaultProductSearchList = null;
        Set<Id> userIdSet = new Set<Id>{ UserInfo.getUserId() };
        try {
            objectFieldsWrapperList = SRV_CRM_Product.getProductFieldsDetail(objectName, fieldAPINameList);
            defaultProductSearchList = SRV_CRM_Default_Product_Search.getDefaultProductSearchFilter(userIdSet, new Set<String>(defaultProductfieldList));
            idToOpportunityMap = SRV_CRM_Opportunity.getOppDetail(new Set<Id>{opportunityId}, new Set<String>(oppFieldList));
            if(defaultProductSearchList.size() > 0) {
                defaultProductSearch = defaultProductSearchList[0];
            }
            
            if(defaultProductSearch.Default_Territory__c ==  null) {
            	List<User> userList = getUserDetails();
            	defaultProductSearch.Default_Territory__c = userList[0].User_Country__c;
            }            

            if(idToOpportunityMap.size() > 0) {
                opportunityRecord = idToOpportunityMap.get(opportunityId);
            }
            oppWrapper.add(new OpportunityWrapper(opportunityRecord, objectFieldsWrapperList, defaultProductSearch));
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return oppWrapper;
    }
    
    /**
     * This method used to get list of OLIs
     * @params  Id opportunityId
     * @params  List<String> oliFields
     * @return  List<OpportunityWrapper>
     */
    @AuraEnabled
    public static List<OpportunityWrapper> getListOfOLI(Id opportunityId, List<String> oliFields) {
        List<OpportunityWrapper> opportunityWrapperList = new List<OpportunityWrapper>();
        List<OpportunityLineItem> oliList = null;
        try {
            String objectName = CON_CRM.OPPORTUNITYLINEITEM_OBJECT_API;
            Set<Id> oppIdset = new Set<Id>();
            Set<String> fieldSet;
            oppIdset.add(opportunityId);
            fieldSet = new Set<String>(oliFields);
            oliList = SRV_CRM_OpportunityLineItem.getListOfOLI(oppIdset, fieldSet);
            List<OpportunityLineItemWrapper> oliWrapperList = new List<OpportunityLineItemWrapper>();
            for(OpportunityLineItem oli : oliList) {
                oliWrapperList.add(new OpportunityLineItemWrapper(oli));
            }
            opportunityWrapperList.add(new OpportunityWrapper(oliWrapperList));
        } catch(exception ex) {
            throw new AuraHandledException (JSON.serialize(ex));
        }
        return opportunityWrapperList;
    }
    
    /**
     * This method used to get field detail of Oli object
     * @params  String fieldData
     * @return  List<UTL_Sobject.ObjectFieldsWrapper>
     */
    @AuraEnabled
    public static List<ObjectFieldsWrapper> getOLIFieldDetail(String fieldData) {
        List<ObjectFieldsWrapper> objectSelectedFieldList = new List<ObjectFieldsWrapper>();
        try {
            List<String> fieldList = fieldData.split(',');
            objectSelectedFieldList = new UTL_Sobject(CON_CRM.OPPORTUNITYLINEITEM_OBJECT_API).getSobjectFieldDetails(fieldList);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return objectSelectedFieldList;
    }
    
    /**
     * This method used to get field detail of Oli object
     * @params  String objectName
     * @params  List<String> fieldList
     * @return  List<UTL_Sobject.ObjectFieldsWrapper>
     */
    @AuraEnabled
    public static List<ObjectFieldsWrapper> getSobjectFieldDetail(String objectName, List<String> fieldList) {
        List<ObjectFieldsWrapper> objectSelectedFieldList = new List<ObjectFieldsWrapper>();
        try {
            objectSelectedFieldList = new UTL_Sobject(objectName).getSobjectFieldDetails(fieldList);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return objectSelectedFieldList;
    }
    
    /**
     * This method is used to clone Opportunity records. 
     * @params  Id oppId
     * @params  Map<String,String> opportunity 
     * @params  Boolean isCloneOpportunity 
     * @params  Integer pricePercentAdjustment 
     * @params  Integer monthMoved
     * @params  Boolean isCloneProduct
     * @params  Boolean isCloneRevenueSchedule
     * @params  Boolean isCloneContactRole
     */    
    @AuraEnabled
    public static Id cloneOpportunityProducts(Id opportunityId, Map<String, String> mapTofieldValue, Decimal pricePercentAdjustment, Integer monthMoved, Map<String, Boolean> objectTypeToIsCloneMap) {
        Id createdOpportunityId = null;
        try {
            createdOpportunityId = SRV_CRM_Opportunity.cloneOpportunity(opportunityId, mapTofieldValue, true, pricePercentAdjustment, monthMoved, objectTypeToIsCloneMap);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return createdOpportunityId;
    }
    
    /**
     * This method used to perform CRUD on OLI
     * @params  String action
     * @params  String recordJSON
     */
    @AuraEnabled
    public static void crudOliRecord(String recordJSON) {
        try {
            List<OpportunityLineItemWrapper> oliWrapList = (List<OpportunityLineItemWrapper>)JSON.deserialize(recordJSON, List<OpportunityLineItemWrapper>.class);
            if(oliWrapList.size() > 0) {
                SRV_CRM_OpportunityLineItem.updateOliRecords(oliWrapList);
            }
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
    
    /**
     * This method used to perform CRUD on OLI
     * @params  String action
     * @params  String recordJSON
     */
    @AuraEnabled
    public static void crudFavoriteProductRecord(String action, String recordJSON) {
        try {
            List<Favorite_Product__c> favoriteProductList = (List<Favorite_Product__c>)JSON.deserialize(recordJSON, List<Favorite_Product__c>.class);
            String objectName = CON_CRM.FAVORITEPRODUCT_OBJECT_API;
            SRV_CRM_Favorite_Product.modifyFavoriteProductRecords(favoriteProductList, objectName, action);
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }

    /**
     * This method used to update default product filter
     * @params  String defaultFilterObjString
     */
    @AuraEnabled
    public static void updateDefaultProductSearchFilter(String defaultFilterObjString) {
        try {
            Default_Product_Search__c filterProductSearch = (Default_Product_Search__c)JSON.deserialize(defaultFilterObjString, Default_Product_Search__c.Class);
            if(filterProductSearch.Id == null) {
                filterProductSearch.User__c = UserInfo.getUserId();
            }
            SRV_CRM_Default_Product_Search.updateDefaultProductSearchFilter(new List<Default_Product_Search__c>{filterProductSearch});
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
    
    /**
     * This method used to get Opportunity Detail
     * @params  Id oppId
     * @params  String oppFields
     * @return  Opportunity
     */
    @AuraEnabled
    public static Opportunity getOpportunityDetails(Id oppId, List<String> oppFields) {
        Opportunity opp = new Opportunity();
        if(oppId != null) {
            Set<String> oppFieldSet = new Set<String>(oppFields);
            Set<Id> oppIdSet = new Set<Id>{oppId};
            Map<Id, Opportunity> idToOpportunityMap = new Map<Id, Opportunity>();
            try {
                idToOpportunityMap = SRV_CRM_Opportunity.getOppDetail(oppIdSet, oppFieldSet);   
                if(idToOpportunityMap.size() > 0) {
                    opp = idToOpportunityMap.get(oppId);
                }
            } catch (exception ex) {
                throw new AuraHandledException(JSON.serialize(ex));
            }
        }
        return opp;
    }
    
    /**
     * This method used to get Opportunity Lineitem Details
     * @params  List<Id> oppId
     * @params  List<String> oliFields
     * @return  List<OpportunityLineItem>
     */
    @AuraEnabled
    public static List<OpportunityLineItem> getOpportunityLineItemsDetail(List<Id> oliIdList, List<String> oliFieldList) {
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        try {
            oliList = SRV_CRM_OpportunityLineItem.getOpportunityLineItemsDetail(new Set<Id>(oliIdList), new Set<String>(oliFieldList));   
        } catch (exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return oliList;
    }
    
    /**
     * This method used to fetch OpportunityLineItemSchedule
     * @params  Id opportunityLineItemId
     * @params  List<String> oliFields
     * @params  List<String> schFields
     * @return  OpportunityWrapper
     */
    @AuraEnabled
    public static OpportunityWrapper getOpportunityLineItemSchedule(Id opportunityLineItemId, List<String> oliFields, List<String> schFields) {
        OpportunityWrapper oppWrapper = new OpportunityWrapper();
        Set<Id> oliIdSet = new Set<Id>();
        oliIdSet.add(opportunityLineItemId);
        Map<Id, OpportunityLineItem> idToOliMap = new Map<Id, OpportunityLineItem>();
        try {
            idToOliMap = SRV_CRM_OpportunityLineItemSchedule.getOpportunityLineItemSchedule(oliIdSet, new Set<String>(oliFields), new Set<String>(schFields));
            if(idToOliMap.size() > 0) {
                List<OpportunityLineItemWrapper> lineItemWrapperList = new List<OpportunityLineItemWrapper>();
                OpportunityLineItemWrapper lineItemWrapper = null;
                for(OpportunityLineItem oli : idToOliMap.values()) {
                    lineItemWrapper = new OpportunityLineItemWrapper(oli);
                    lineItemWrapperList.add(lineItemWrapper);
                    CurrencyType currTpe = [SELECT DecimalPlaces FROM CurrencyType where IsoCode =: oli.CurrencyIsoCode];
                    decimalPlace = currTpe.DecimalPlaces;
                    if(currTpe.DecimalPlaces != 0) {
                        Decimal num = Math.pow(10, currTpe.DecimalPlaces);
                        decimalPlaceStep = 1/num;
                    } else {
                        decimalPlaceStep = currTpe.DecimalPlaces;
                    }
                }
                oppWrapper = new OpportunityWrapper(lineItemWrapperList, decimalPlace, decimalPlaceStep);
            }
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return oppWrapper;
    }
    
    /**
     * This method used to fetch OpportunityLineItemSchedule
     * @params  String jsonWrapper
     * @return  OpportunityWrapper
     */
    @AuraEnabled
    public static OpportunityWrapper establishSchedule(String jsonWrapper) {
        OpportunityWrapper oppWrapper = new OpportunityWrapper();
        try {
            OpportunityWrapper.EsatblisOpportunityLineItemScheduleWrapper establishWrapper = (OpportunityWrapper.EsatblisOpportunityLineItemScheduleWrapper)JSON.deserialize(jsonWrapper, OpportunityWrapper.EsatblisOpportunityLineItemScheduleWrapper.class);
            Map<Id, OpportunityLineItem> idToOliMap = new Map<Id, OpportunityLineItem>();
            if(establishWrapper.oliId != null) {
                idToOliMap = SRV_CRM_OpportunityLineItemSchedule.insertOLISch(establishWrapper);
                if(idToOliMap.size() > 0) {
                    List<OpportunityLineItemWrapper> lineItemWrapperList = new List<OpportunityLineItemWrapper>();
                    OpportunityLineItemWrapper lineItemWrapper = null;
                    for(OpportunityLineItem oli : idToOliMap.values()) {
                        lineItemWrapper = new OpportunityLineItemWrapper(oli);
                        lineItemWrapperList.add(lineItemWrapper);
                    }
                    oppWrapper = new OpportunityWrapper(lineItemWrapperList);
                }
            }
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return oppWrapper;
    }
    
    /**
     * This method used to update OpportunityLineItemSchedule
     * @params  String jsonWrapper
     */
    @AuraEnabled
    public static void updateOliSchedule(String jsonWrapper) {
        try {
            List<OpportunityLineItemScheduleWrapper> schWrapperList = (List<OpportunityLineItemScheduleWrapper>)JSON.deserialize(jsonWrapper, List<OpportunityLineItemScheduleWrapper>.class);
            if(schWrapperList.size() > 0) {
                SRV_CRM_OpportunityLineItemSchedule.crudOliSchedule(schWrapperList);
            }
        } catch(SRV_CRM_OpportunityLineItemSchedule.OpportunityLineItemScheduleServiceException exc) {
            throw new AuraHandledException(JSON.serialize(exc));
        }  catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
    
    /**
     * This method used to get User Detail
     * @return  List<User>
     */
    @AuraEnabled
    public static List<User> getUserDetails() {
        List<User> usersList = new List<User>();
        usersList = SRV_CRM_User.getUserDetail(new Set<Id> {UserInfo.getUserId()});
        return usersList;
    }
    
    /**
     * This method used to getMulesoft Opportunity Sync Records By Opp Id
     * @return  List<Mulesoft_Opportunity_Sync__c>
     */
    @AuraEnabled
    public static List<Mulesoft_Opportunity_Sync__c> getMulesoftOpportunitySyncByOppIds(List<Id> oppIdList) {
        List<Mulesoft_Opportunity_Sync__c> muleSoftOppSyncList = new List<Mulesoft_Opportunity_Sync__c>();
        try {
            muleSoftOppSyncList = SRV_CRM_MulesoftOpportunitySync.getMulesoftOpportunitySyncByOppIds(new Set<Id>(oppIdList));
        } catch (exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return muleSoftOppSyncList;
    }
    
    /**
     * This method used to getMulesoft Opportunity Sync Records By Opp Id
     * @return  List<Mulesoft_Opportunity_Sync__c>
     */
    @AuraEnabled
    public static String getErrorIfLQMaterialProducts(Id oppId) {
        String errorMsg = '';
        Legacy_Org_Link__c legacyOrgLink = Legacy_Org_Link__c.getOrgDefaults();
        Map<Id, Opportunity> idToOpportunityMap = new Map<Id, Opportunity>();
        Set<String> fieldSet = new Set<String>{'Id', 'LQ_Opportunity_Id__c'};
        try {
            idToOpportunityMap = SRV_CRM_Opportunity.getOppDetail(new Set<Id>{oppId}, fieldSet);
            if(idToOpportunityMap.size() > 0) {
                Id lqOppId = idToOpportunityMap.values()[0].LQ_Opportunity_Id__c;
                errorMsg = 'https://' + legacyOrgLink.Legacy_Quintiles_URL__c + '/' + lqOppId + '/e?retURL=%2F' + lqOppId;  
            }
        } catch (exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return errorMsg;
    }
    
    /**
     * This method used to get CurrencyType Decimal Detail
     * @return  List<User>
     */
    @AuraEnabled
    public static void decimalPlaceValue() {
        String oppId = Apexpages.currentPage().getParameters().get('id');
        if(oppId != null) {
            Set<String> oppFieldSet = new Set<String>{'CurrencyIsoCode'};
            Set<Id> oppIdSet = new Set<Id>{oppId};
            String isoCode;
            Map<Id, Opportunity> idToOpportunityMap = new Map<Id, Opportunity>();
            try {
                idToOpportunityMap = SRV_CRM_Opportunity.getOppDetail(oppIdSet, oppFieldSet);   
                if(idToOpportunityMap.size() > 0) {
                    Opportunity opp = idToOpportunityMap.get(oppId);
                    isoCode = opp.CurrencyIsoCode;
                    CurrencyType currTpe = [SELECT DecimalPlaces FROM CurrencyType where IsoCode =: isoCode];
                    decimalPlace = currTpe.DecimalPlaces;
                    if(currTpe.DecimalPlaces != 0) {
                        Decimal num = Math.pow(10, currTpe.DecimalPlaces);
                        decimalPlaceStep = 1/num;
                    } else {
                        decimalPlaceStep = currTpe.DecimalPlaces;
                    }
                }
            } catch (exception ex) {
                throw new AuraHandledException(JSON.serialize(ex));
            }
        }
    }
    @AuraEnabled
    public static String fetchContactRecordTypeId(){
        return CON_CRM.CONTACT_RECORD_TYPE_IQVIA_USER_CONTACT;
    }
}