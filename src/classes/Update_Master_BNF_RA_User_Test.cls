@isTest
private class Update_Master_BNF_RA_User_Test {

    /*public static ApexPages.StandardController controller;
    private static BNFOpptyListLockedGrid extension;
    public static PageReference pageRef1 ;
    
    @testSetup static void setupTestData(){
        Global_Variables.PCFlag = false;
        Global_Variables.RunQuickOppLimitTrigger = false;
        Global_Variables.RenewalCloneFlag = TRUE;
        Global_Variables.syncAgreementToOppty=false;
        Global_Variables.PCTrigger_Hault_Execution=false;
        
        Account TestAccount = BNF_Test_Data.createAccount();
        List<Address__c> TestAddress_Array = BNF_Test_Data.createAddress_Array();
        List<SAP_Contact__c> TestSapContact_Array = BNF_Test_Data.createSapContact_Array();
        BNF_Test_Data.create_User_LocaleSetting();
        Opportunity TestOpp = new Opportunity(Name='test',CloseDate=System.today());
        TestOpp.LeadSource = 'Account Planning';
        TestOpp.StageName='7a. Closed Won';
        TestOpp.AccountId = TestAccount.Id;
        TestOpp.Contract_End_Date__c = system.today();
        TestOpp.Contract_Start_Date__c = system.today().addyears(12);
        //TestOpp.Win_Loss_Reason__c='Win - Competitive Situation';
        TestOpp.Contract_Type__c='Individual';
        TestOpp.LeadSource = 'Account Planning';
        //TestOpp.Win_Additional_Details__c = 'Additional details';
        //TestOpp.Win_Loss_Reason_Details__c = 'Win Loss Reason Details'; 
        TestOpp.CurrencyIsoCode = 'USD';
        //TestOpp.Exclude_From_Pricing_Calculator__c = true;
        //TestOpp.Compelling_Event__c = 'Unknown';
        TestOpp.Budget_Available__c = 'Unknown';
        //TestOpp.Unique_Business_Value__c = 'Unknown';
        TestOpp.Therapy_Area__c= 'Hepatitis C [21]';
        insert TestOpp;
        
        Product2 prod1 = new Product2(Name='test1', ProductCode='1', Enabled_Sales_Orgs__c='CH03', Offering_Type__c = 'Commercial Tech', Material_Type__c = 'ZREP',CanUseRevenueSchedule= true, Delivery_Media__c = 'DVD [DV]:CD [CD]',Delivery_Frequency__c = 'Monthly:Quaterly');
        insert prod1;
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.UseStandardPrice = false;
        pbe.Pricebook2Id = Test.getStandardPricebookId();
        pbe.Product2Id=prod1.id;
        pbe.IsActive=true;
        pbe.UnitPrice=100.0;
        pbe.CurrencyIsoCode = 'USD';
        insert pbe;
        
        
        Profit_Centre__c pseregion = CommanUtilityTestFunction.setUpRegion();
        insert pseregion; 
        SalesOrg__c salesorg = CommanUtilityTestFunction.setUpSalesOrg();
        insert salesorg;
        Account acc = CommanUtilityTestFunction.createAccount();
        insert acc;
        pse__Work_Calendar__c workcal = CommanUtilityTestFunction.setUpWorkCalender();
        insert workcal;
        User testUser1 = [Select id , name from User where Profile.Name like '%system administrator%' and isActive = true limit 1];
        Contact testContact1 = CommanUtilityTestFunction.createContact(acc, salesorg, testUser1,workcal,pseregion);
        //testContact1.Employee_Id__c = '22232';
        insert testContact1;
        pse__Proj__c proj = CommanUtilityTestFunction.createEngagement(testContact1, pseregion, acc, salesorg);
        insert proj;
        //pse__Budget__c budg = CommanUtilityTestFunction.createBudget(proj,acc,testContact1,salesorg);
        //budg.WBSRElementCode__c = '1,3,5';
        //budg.Therapyarea__c = 'Hepatitis C [21]';
        //budg.pse__Amount__c = 9000;
        //insert budg;
        
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = TestOpp.Id;
        oli.Product_Start_Date__c = Date.today();
        oli.Product_End_Date__c = Date.today().addYears(1) ;
        oli.PricebookEntryId = pbe.Id;
        oli.Billing_Frequency__c = 'Once';
        oli.Proj_Rpt_Frequency__c='Once [O]';
        oli.Therapy_Area__c= 'Hepatitis C [21]';
        oli.Quantity = 1.00;
        oli.UnitPrice = 9000;  
        //oli.Budget__c = budg.id;
        oli.Delivery_Date__c = System.today().addYears(1);
        oli.Revenue_Type__c = 'Ad Hoc';
        oli.Sale_Type__c = 'New';
        insert oli;
        List<OpportunityLineItemSchedule> InsertOLISList = new List<OpportunityLineItemSchedule>();
        Integer RevFactor = 1;
        
        
        Date strScheduleDate = Date.valueOf(String.valueOf(System.now().year())+'-01-15 00:00:00');    
        for (Integer i = 0; i < 3; i++) 
        {
            
            Double dRevenueAmount = RevFactor*1000.00;
            RevFactor = RevFactor + 2;
            OpportunityLineItemSchedule OliSched = new OpportunityLineItemSchedule();
            OliSched.OpportunityLineItemId = OLI.Id;
            OliSched.Type = 'Revenue';
            OliSched.Revenue = dRevenueAmount;
            OliSched.ScheduleDate = strScheduleDate;
            strScheduleDate = strScheduleDate.addMonths(1);
            InsertOLISList.add(OliSched); 
        }
        insert InsertOLISList;
        Billing_Schedule__c sche = new Billing_Schedule__c(name = 'textSchedule', OLIId__c = oli.id);
        insert sche;
        
        List<Billing_Schedule_Item__c> billingScheduleItem = new List<Billing_Schedule_Item__c>();
        Billing_Schedule_Item__c sche1 = new Billing_Schedule_Item__c(name = 'textScheduleItem', Billing_Amount__c = 1000,Billing_Date__c = system.today(),Billing_Schedule__c = sche.id);
        Billing_Schedule_Item__c sche2 = new Billing_Schedule_Item__c(name = 'textScheduleItemTemp1', Billing_Amount__c = 3000, Billing_Date__c = system.today().addMonths(1),Billing_Schedule__c = sche.id );
        Billing_Schedule_Item__c sche3 = new Billing_Schedule_Item__c(name = 'textScheduleItemTemp2', Billing_Amount__c = 5000, Billing_Date__c = system.today().addMonths(2),Billing_Schedule__c = sche.id );
        billingScheduleItem.add(sche1);
        billingScheduleItem.add(sche2); 
        billingScheduleItem.add(sche3); 
        insert billingScheduleItem;
    }
    
    static void createBNFTestData() {
        BNF_Settings__c bnfsetting=new BNF_Settings__c(Enable_Material_Validation__c=true,Enable_Customer_Validation__c=true,BNF_Opportunity_Threshold__c = 1000, Enable_BNF_Surcharge__c=true, Enable_Billing_Schedule_Validation__c = true);
        insert bnfsetting;
        Revenue_Analyst__c TestLocalRA = new Revenue_Analyst__c(name='TestRA',User__c = UserInfo.getUserId());
        insert TestLocalRA;
        Opportunity testOpp = [Select id from Opportunity limit 1];
        List<Address__c> TestAddress_Array = [Select id from Address__c limit 5];
        Master_BNF__c masterBNF = new Master_BNF__c();
        masterBNF.Revenue_Analyst__c = TestLocalRA.id;
        masterBNF.IMS_Sales_Org__c = 'IMS Japan';
        masterBNF.Sales_Org_Code__c = 'JP01';
        masterBNF.SAP_Base_Code__c = '503895';
        masterBNF.Client__c = [Select id from Account limit 1].id;
        masterBNF.BNF_Status__c = 'New';
        insert masterBNF;
        BNF2__c TestBnf = new BNF2__c(Opportunity__c=TestOpp.Id);
        TestBnf.BNF_Status__c = 'New';
        TestBnf.Contract_Start_Date__c = Date.today();
        TestBnf.Contract_End_Date__c = Date.today().addYears(1);
        TestBnf.IMS_Sales_Org__c = 'IHA, IMS Health Rotkreuz';
        TestBnf.Sales_Org_Code__c='CH03';
        TestBnf.Bill_To__c=TestAddress_Array[0].id;
        TestBnf.X2nd_Copy__c=TestAddress_Array[1].id;
        TestBnf.Carbon_Copy__c=TestAddress_Array[2].id;
        TestBnf.Ship_To__c=TestAddress_Array[3].id;
        TestBnf.Cover_Sheet__c=TestAddress_Array[4].id;
        TestBnf.RecordTypeId = MDM_Defines.SAP_SD_Integrated_Record_Type_Id;
        TestBnf.Revenue_Analyst__c = TestLocalRA.id;  
        TestBNF.Master_BNF__c  = masterBNF.id;
        insert TestBnf;
    }
     static testmethod void testMasterBNFApprovalRequest() {
        Test.startTest();
        createBNFTestData();
        Master_BNF__c MasterBNF = [select id ,Comments__c,Revenue_Analyst__c from Master_BNF__c Limit 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Test Comment');
        req1.setSubmitterId(UserInfo.getUserId());
        req1.setObjectId(MasterBNF.id); 
        Approval.ProcessResult result = Approval.process(req1);
        Profile p = [SELECT Id FROM Profile WHERE Name like '%system administrator%' Limit 1];
        List<User> userlist= new List<User>();
        for (Integer k=0;k<10;k++) { 
            userlist.add(new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standarduser3144'+k+'@testorg.com'));
        }
        insert userlist;
        Revenue_Analyst__c TestLocalRA = new Revenue_Analyst__c(name='TestRA',
            User__c = userlist[0].id ,
           User_2__c = userlist[1].id,
            User_3__c = userlist[2].id,
            User_4__c = userlist[3].id,
            User_5__c = userlist[4].id,
            User_6__c = userlist[5].id,  
            User_7__c = userlist[6].id,
            User_8__c = userlist[7].id,
            User_9__c = userlist[8].id,
            User_10__c = userlist[9].id);                                                                 
        insert TestLocalRA;
        MasterBNF.Revenue_Analyst__c = TestLocalRA.id;
        upsert MasterBNF;
        Test.stopTest();
    }
    
    static testmethod void testMasterBNFApprovalRequest1() {
        Test.startTest();
        createBNFTestData();
        Master_BNF__c MasterBNF = [select id ,Comments__c,Revenue_Analyst__c from Master_BNF__c Limit 1];
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Test Comment');
        req1.setSubmitterId(UserInfo.getUserId());
        req1.setObjectId(MasterBNF.id); 
        Approval.ProcessResult result = Approval.process(req1);
        Profile p = [SELECT Id FROM Profile WHERE Name like '%system administrator%' Limit 1];
        List<User> userlist= new List<User>();
        for (Integer k=0;k<10;k++) { 
            userlist.add(new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standarduser3144'+k+'@testorg.com'));
        }
        
        userList[0].isActive = false;
        userList[1].isActive = false;
        userList[2].isActive = false;
        userList[3].isActive = false;
        userList[4].isActive = false;
        userList[5].isActive = false;
        userList[6].isActive = false;
        userList[7].isActive = false;
        userList[8].isActive = false;
        
        insert userList;
        
        Revenue_Analyst__c TestLocalRA = new Revenue_Analyst__c(name='TestRA',
            User__c = userlist[0].id);                                                                 
        insert TestLocalRA;
        MasterBNF.Revenue_Analyst__c = TestLocalRA.id;
        upsert MasterBNF;
        Test.stopTest();
    }
    */
}