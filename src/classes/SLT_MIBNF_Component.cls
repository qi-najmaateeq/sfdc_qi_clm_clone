/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for MIBNF_Component__c Object.
 */
public class SLT_MIBNF_Component extends fflib_SObjectSelector {
    
    /**
     * constructor to initialize CRUD and FLS
     */
    public SLT_MIBNF_Component() {
        super(false, false, false);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return MIBNF_Component__c.sObjectType;
    }
    
    /**
     * This method used to get MIBNF_Component__c by OpportunityId
     * @return List<MIBNF_Component__c>
     */
    public List<MIBNF_Component__c> selectByFilter(Set<ID> oppIdSet, Set<String> fieldSet ,String filterCondition) {
        return (List<MIBNF_Component__c>)Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition(filterCondition).toSOQL());
    }
}