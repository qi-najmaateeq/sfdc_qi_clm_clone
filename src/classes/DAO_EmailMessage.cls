public class DAO_EmailMessage  extends fflib_SObjectDomain {

    /**
     * Constructor of this class
     * @params sObjectList List<Task>
     */
    public DAO_EmailMessage(List<EmailMessage> sObjectList) {
        super(sObjectList);
    }

    /**
     * Constructor Class for construct new Instance of This Class
     */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new DAO_EmailMessage(sObjectList);
        }
    }

    /**
     * This method is used for after insert of the EmailMessage trigger.
     * @return void filterOppRelatedTask
     */
    public override void onAfterInsert() {
        DAOH_EmailMessage.createCaseCollabration((List<EmailMessage>) Records);
        DAOH_EmailMessage.updateStatusforEmailMessageonCase((List<EmailMessage>) Records);
        DAOH_EmailMessage.cloneCaseStatusIsClosedEmailMessageonCase((List<EmailMessage>) Records);
        DAOH_EmailMessage.MailSenderWhenEmailReceivedOnTechClosedCase((List<EmailMessage>) Records);
    }  
    
    /**
     * This method is used for after update of the EmailMessage trigger.
     * @params  existingRecords Map<Id,SObject>
     * @return  void
     */
   public override void  onAfterUpdate(Map<Id,SObject> existingRecords) {
       DAOH_EmailMessage.afterupdatetoCreateActivityforRD((List<EmailMessage>) Records,(Map<Id,EmailMessage>)existingRecords);
   }
    
    public override void onBeforeDelete() {
       String profileName=[Select Id,Name from Profile where Id=: UserInfo.getProfileId()].Name; 
       for (EmailMessage a : (List<EmailMessage>)Trigger.old)      
       {  
           if (a.ParentId != null && (profileName =='Service User' || profileName =='CSM Customer Community Plus Login User'))
              {
                  Trigger.oldMap.get(a.Id).addError('Cannot delete Email with related to Case');
              } 
      }   
  }
}
