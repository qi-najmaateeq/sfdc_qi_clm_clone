/*
 * Version       : 1.0
 * Description   : batch to create tasks for contracts 
 */
public class BCH_CRM_ContractMSAReviewDateAlertBatch  implements Database.Batchable<Sobject> , Database.Stateful   {
	
    public BCH_CRM_ContractMSAReviewDateAlertBatch() {
    }
    /**
     * start method 
     * @params  Database.BatchableContext context
     * @return  Database.QueryLocator
     */
    public Database.QueryLocator start( Database.BatchableContext context ) {
        //list of all Contracts to create Tasks
        return Database.getQueryLocator([SELECT Id, OwnerId, Date_on_which_MSA_should_be_reviewed__c FROM Contract WHERE Date_on_which_MSA_should_be_reviewed__c = Today]);
    }
    
    /**
     * start method 
     * @params  Database.BatchableContext context
     * @return  Database.QueryLocator
     */
    public void execute(Database.BatchableContext context, List<Contract> records) {
        List<Task> tasksToInsert = new List<Task>();
        Date dateToday = Date.today();
        for(Contract currentContract : records){
            Task t = new Task();
            t.OwnerId = currentContract.OwnerId;
            t.Subject = 'Contract to be reviewed';
            t.Status = 'Not Started';
            t.Priority = 'Medium';
            t.WhatId = currentContract.Id;
            t.ActivityDate = dateToday;
            tasksToInsert.add(t);
        }
        try{
            insert tasksToInsert;
        }catch(Exception e){
            System.debug('-Exception--'+e);
        }
    }
    public void finish(Database.BatchableContext context) {
    }
}