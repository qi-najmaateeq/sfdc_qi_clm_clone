global class BCH_CPQ_BatchToUnlockAgreementRecord implements Database.Batchable<sobject>, Database.Stateful {

    global Database.Querylocator start (Database.BatchableContext BC) {

        String[] recordTypes = new String[]{CON_CPQ.AGREEMENT_FDTN_INITIAL_BID, CON_CPQ.AGREEMENT_FDTN_REBID, CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER, CON_CPQ.AGREEMENT_FDTN_CNF, CON_CPQ.AGREEMENT_FDTN_CONTRACT};

        return Database.getQueryLocator('SELECT Id, Budget_Checked_Out_by__c, Pricing_Tool_Locked__c, XAE_Lock_Timestamp__c,' + 
            ' Notification_Sent_To_Budget_Owner__c ' 
            + 'FROM Apttus__APTS_Agreement__c WHERE Pricing_Tool_Locked__c = true' 
            + ' AND RecordType.Name IN :recordTypes AND XAE_Lock_Timestamp__c != null'
            + ' AND Budget_Checked_Out_by__c != null') ;
    }
    
    DateTime firstReminderDateTime;
    DateTime secondReminderDateTime;
    DateTime thirdReminderDateTime;
    DateTime finalUnlockDateTime;
    String userTimeZone;
    Id emailAddressId;
    public BCH_CPQ_BatchToUnlockAgreementRecord() {
    
        CPQ_Budget_Unlock_Setting__c unlockSetting = CPQ_Budget_Unlock_Setting__c.getOrgDefaults();
        userTimeZone = UserInfo.getTimeZone().getID();
        DateTime firstReminderPeriod = System.now().addHours(-Integer.valueOf(unlockSetting.First_Reminder_Hours_To_Unlock_Budget__c));
        String firstReminderPeriodString = firstReminderPeriod.format(CON_CPQ.DATE_TIME_FORMAT,userTimeZone);
        firstReminderDateTime = DateTime.valueOfGmt(firstReminderPeriodString);

        DateTime secondReminderPeriod = System.now().addHours(-Integer.valueOf(unlockSetting.Second_Reminder_Hours_To_Unlock_Budget__c));
        String secondReminderPeriodString = secondReminderPeriod.format(CON_CPQ.DATE_TIME_FORMAT,userTimeZone);
        secondReminderDateTime = DateTime.valueOfGmt(secondReminderPeriodString);

        DateTime thirdReminderPeriod = System.now().addHours(-Integer.valueOf(unlockSetting.Third_Reminder_Hours_To_Unlock_Budget__c));
        String thirdReminderPeriodString = thirdReminderPeriod.format(CON_CPQ.DATE_TIME_FORMAT,userTimeZone);
        thirdReminderDateTime = DateTime.valueOfGmt(thirdReminderPeriodString);

        DateTime finalUnlockPeriod = thirdReminderPeriod.addMinutes(-Integer.valueOf(unlockSetting.Minute_To_Unlock_Agreement__c));
        String finalUnlockString = finalUnlockPeriod.format(CON_CPQ.DATE_TIME_FORMAT,userTimeZone);
        finalUnlockDateTime = DateTime.valueOfGmt(finalUnlockString);
        List<OrgWideEmailAddress> orgWideEmailAddress = new SLT_OrgWideEmailAddress().selectOrgWideEmailAddressByAdress(
            CON_CPQ.CPQ_Foundation_Tool_Address, new Set<String>{CON_CPQ.Id});
        if(orgWideEmailAddress.size() > 0)
            emailAddressId = orgWideEmailAddress[0].Id;
    }

    global void execute (Database.BatchableContext BC, List<sobject> SObjectList) {

        List<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();

        for(Apttus__APTS_Agreement__c agreement : (List<Apttus__APTS_Agreement__c>)SObjectList){
        
            Datetime xaeLockTimeStamp = agreement.XAE_Lock_Timestamp__c;
            String xaeLockTimeStampString = xaeLockTimeStamp.format(CON_CPQ.DATE_TIME_FORMAT,userTimeZone);
            DateTime xaeLockDateTime = DateTime.valueOfGmt(xaeLockTimeStampString);

            if((agreement.Notification_Sent_To_Budget_Owner__c == null && xaeLockDateTime < firstReminderDateTime)
                || (agreement.Notification_Sent_To_Budget_Owner__c == CON_CPQ.FIRST_SENT && 
                xaeLockDateTime < secondReminderDateTime)){

                if(agreement.Notification_Sent_To_Budget_Owner__c == CON_CPQ.FIRST_SENT){
                    agreement.Notification_Sent_To_Budget_Owner__c = CON_CPQ.SECOND_SENT;
                    agreementList.add(agreement);       
                }else if(agreement.Notification_Sent_To_Budget_Owner__c == null){
                    agreement.Notification_Sent_To_Budget_Owner__c = CON_CPQ.FIRST_SENT;
                    agreementList.add(agreement);
                }

                List<EmailTemplate> emailTemplate = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(
                    CON_CPQ.CPQ_NOTIFICATIOn_TO_BUDGET_OWNER_ABOUT_BUDGET_LOCKING, new Set<String>{CON_CPQ.Id});
                if(emailTemplate.size() > 0){
                    Messaging.SingleEmailMessage mailToXAEOwner = new Messaging.SingleEmailMessage();
                    Messaging.SingleEmailMessage mailTemp = Messaging.renderStoredEmailTemplate(
                        emailTemplate[0].Id, agreement.Budget_Checked_Out_By__c, agreement.Id);
                    mailToXAEOwner.setSubject(mailTemp.getSubject());
                    mailToXAEOwner.setHTMLBody(mailTemp.getHtmlbody());
                    mailToXAEOwner.setTargetObjectId(agreement.Budget_Checked_out_by__c);
                    mailToXAEOwner.setSaveAsActivity(false);
                    if(emailAddressId != null)
                        mailToXAEOwner.setOrgWideEmailAddressId(emailAddressId);
                    mailList.add(mailToXAEOwner);   
                }
            }else if(agreement.Notification_Sent_To_Budget_Owner__c == CON_CPQ.SECOND_SENT 
                && xaeLockDateTime < thirdReminderDateTime){
                agreement.Notification_Sent_To_Budget_Owner__c = CON_CPQ.THIRD_SENT;
                agreementList.add(agreement);
                List<EmailTemplate> emailTemplate = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(
                    'CPQ_Last_Notification_To_Budget_Owner_For_Unlocking', new Set<String>{CON_CPQ.Id});
                if(emailTemplate.size() > 0){
                    Messaging.SingleEmailMessage mailToXAEOwner = new Messaging.SingleEmailMessage();
                    Messaging.SingleEmailMessage mailTemp = Messaging.renderStoredEmailTemplate(
                        emailTemplate[0].Id, agreement.Budget_Checked_out_by__c, agreement.Id);
                    mailToXAEOwner.setSubject(mailTemp.getSubject());
                    mailToXAEOwner.setHTMLBody(mailTemp.getHtmlbody());
                    mailToXAEOwner.setTargetObjectId(agreement.Budget_Checked_out_by__c);
                    mailToXAEOwner.setSaveAsActivity(false);
                    if(emailAddressId != null)
                        mailToXAEOwner.setOrgWideEmailAddressId(emailAddressId);
                    mailList.add(mailToXAEOwner);
                }
            }else if(agreement.Notification_Sent_To_Budget_Owner__c == CON_CPQ.THIRD_SENT && 
                xaeLockDateTime < finalUnlockDateTime){

                agreement.Pricing_Tool_Locked__c = false;
                agreement.Budget_Checked_Out_By__c = null;
                agreement.XAE_Lock_Timestamp__c = null;
                agreement.Notification_Sent_To_Budget_Owner__c = '';
                agreementList.add(agreement);
            }  
        }

        if(agreementList.size() > 0)
            update agreementList;
        if(mailList.size() > 0)
            Messaging.sendEmail(mailList);
    }

    global void finish(Database.BatchableContext BC) {
	
    }
}