/*  ===============================================================================
        Created:        Jyoti Agrawal
        Date:           04/08/2019
        Version       : 1.0
        Description   : This Apex class is selector layer for ContentVersion
    ===============================================================================
*/

public with sharing class SLT_ContentVersion extends fflib_SObjectSelector{
    public SLT_ContentVersion() {
	    super(false, false, false);
	}

    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            ContentVersion.Id,
            ContentVersion.Title,
            ContentVersion.VersionData,
            ContentVersion.FileExtension,
            ContentVersion.FileType
        };
    }

    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return ContentVersion.sObjectType;
    }

    /**
     * This method used to get ContentVersion by Id
     * @return  List<ContentVersion>
     */
    public List<ContentVersion> getContentVersionByDocumentIds(Set<ID> documentIds, Set<String> fieldSet) {
            return (List<ContentVersion>) Database.query(
                newQueryFactory(true).selectFields(fieldSet).setCondition('ContentDocumentId in: documentIds').toSOQL());
    }
}