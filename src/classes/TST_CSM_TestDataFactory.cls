@isTest
public class TST_CSM_TestDataFactory {

    public static Account createAccount(){
        Id mdmValidAccount = Schema.SObjectType.account.getRecordTypeInfosByName().get('MDM Validated Account').getRecordTypeId();
        Account accountObj = new Account();
        accountObj.RecordTypeId = mdmValidAccount;
        accountObj.Name = 'Techno Portal';
        accountObj.RDSponsor__c = false;
        accountObj.RDCategorization__c = 'Site';
        return accountObj;
    }
    
    public static Account createAccount(String categorization){
        Id mdmValidAccount = Schema.SObjectType.account.getRecordTypeInfosByName().get('MDM Validated Account').getRecordTypeId();
        Account accountObj = new Account();
        accountObj.RecordTypeId = mdmValidAccount;
        accountObj.Name = 'Techno Portal';
        accountObj.RDSponsor__c = true;
        accountObj.RDCategorization__c = categorization;
        return accountObj;
    }
    
    public static Contact createContact(Id accountId, String firstName){
        Contact contactObj = new Contact();
        contactObj.AccountId = accountId;
        contactObj.FirstName = firstName;
        contactObj.LastName = 'Sharma';
        contactObj.Email = 'noreply@dummy.com';
        contactObj.Portal_Case_Type__c = 'Technology Solutions';
        return contactObj;
    }

    public static Case createCase(Id accountId, Id contactId, Id recordTypeId, Id assetId, String origin, Id parentId, String subject, String description){
        User u = [Select id from User where Id = :UserInfo.getUserId() and ProfileId = :UserInfo.getProfileId()];
        Case caseRecord = new Case();
        
        system.runAs(u) {
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            GroupMember grpUser = new GroupMember (
                UserOrGroupId = u.Id,
                GroupId = g1.Id);
            
            insert grpUser;
            
            Queue_User_Relationship__c qur = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                User__c = grpUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = grpUser.groupId);
            
            insert qur;
        
		caseRecord.AccountId = accountId;
        caseRecord.RecordTypeId = recordTypeId;
        caseRecord.ContactId = contactId;
        caseRecord.Origin = origin;
        caseRecord.AssetId = assetId;
        caseRecord.ParentId = parentId;
        caseRecord.Subject = subject;
        caseRecord.Description = description;
		caseRecord.OwnerId = u.Id;
		
		}
        return caseRecord;
    }
	
    public static Case createClosedCase(Id accountId, Id recordTypeId, Id contactId, boolean isContactKnown, String origin, String subject, String description, 
                                        Id ownerId, String status, String resolution, String resolutionCode, Id assetId, boolean caseToUser, Id queueUserRelation){
        Case caseRecord = new Case();
        caseRecord.AccountId = accountId;
        caseRecord.RecordTypeId = recordTypeId;
        caseRecord.ContactId = contactId;
        caseRecord.Origin = origin;
        caseRecord.Subject = subject;
        caseRecord.Description = description;
        caseRecord.ownerId = ownerId;
        caseRecord.Status = status;
        caseRecord.Resolution__c = resolution;
        caseRecord.NoContactKnown__c = TRUE;
        caseRecord.ResolutionCode__c = resolutionCode;
        caseRecord.AssetId =  assetId;
        caseRecord.AssignCaseToCurrentUser__c = caseToUser;
        caseRecord.CurrentQueue__c = queueUserRelation;
        return caseRecord;
    }

    private static Integer recordCount = 1;
    public static Major_Incident__c createMajorIncident(){
        Major_Incident__c majorRecord = new Major_Incident__c();
        majorRecord.Major_Incident_Subject__c = 'Test Subject '+recordCount;
        majorRecord.Major_Incident_Description__c = 'Test Description '+recordCount;
        majorRecord.Major_Incident_Internal_Communication__c = 'Internal Communication '+recordCount;
        majorRecord.Major_Incident_Customer_Communication__c = 'Customer Communication '+recordCount;
        recordCount++;
        return majorRecord;
    }
}