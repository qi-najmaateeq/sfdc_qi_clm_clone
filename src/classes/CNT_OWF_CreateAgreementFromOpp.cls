/*
 * Version       : 1.0
 * Description   : Apex Controller for Create Bid screen.
 */
public class CNT_OWF_CreateAgreementFromOpp {
    
    public CNT_OWF_CreateAgreementFromOpp(ApexPages.StandardController controller) {
        
    }
    /**
     * This method is used to retrieve the eligible record types of Agreement that can be created
     * based on the current stage of Opportunity
     * @params  
     * @params  List<String> fieldList
     * @return  List<String> recordTypes
     */
    @AuraEnabled
    public static List<BidTypeWrapper> getEligibleRecordTypes(Id opportunityId, List<String> oppFields) {
        Map<Id, Opportunity> idToOpportunityMap = new Map<Id, Opportunity>();        
        Opportunity opportunityRecord = new Opportunity();
        Map<Id,String> agrRecordIdToRecordNameMap = new Map<Id,String>();
        List<BidTypeWrapper> bidTypeList = new List<BidTypeWrapper>();
        //Set<String> agrRecordTypeName = new Set<String>();
        //try {
        system.debug('oppFields: '+oppFields);
            idToOpportunityMap = SRV_CRM_Opportunity.getOppDetail(new Set<Id>{opportunityId}, new Set<String>(oppFields));
            if(idToOpportunityMap.size() > 0) {
                opportunityRecord = idToOpportunityMap.get(opportunityId);
                Set<String> oppStageToBidTypeMapFieldSet = new Set<String>{'DeveloperName', 'MasterLabel', 'Opp_Stage__c', 'Agreement_Record_Type__c','Bid_Header_and_Desc__c'};
                Map<String,List<Opp_Stage_to_Bid_Type_Mapping__mdt>> oppStageToBidTypeMap = new Map<String,List<Opp_Stage_to_Bid_Type_Mapping__mdt>>();
                    
                    for(Opp_Stage_to_Bid_Type_Mapping__mdt oppStageToBidType :
                        new SLT_OppStageToBidTypeMapping(true, false).getOppStageToBidTypeMappings(oppStageToBidTypeMapFieldSet)) {
                            if(!oppStageToBidTypeMap.containsKey(oppStageToBidType.Opp_Stage__c)) {
                                oppStageToBidTypeMap.put(oppStageToBidType.Opp_Stage__c,new List<Opp_Stage_to_Bid_Type_Mapping__mdt>());
                                oppStageToBidTypeMap.get(oppStageToBidType.Opp_Stage__c).add(oppStageToBidType);
                            }
                            else {
                                oppStageToBidTypeMap.get(oppStageToBidType.Opp_Stage__c).add(oppStageToBidType);
                            }
                     }
                    
                    Map<String, Schema.RecordTypeInfo> agrRecordTypes = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName();
                    if(oppStageToBidTypeMap.containsKey(opportunityRecord.stageName.substring(0,1))) {
                        for(Opp_Stage_to_Bid_Type_Mapping__mdt agrRecordType : oppStageToBidTypeMap.get(opportunityRecord.stageName.substring(0,1))) {
                            
                            bidTypeList.add(new BidTypeWrapper(agrRecordTypes.get(agrRecordType.Agreement_Record_Type__c).getRecordTypeId(),agrRecordType.Agreement_Record_Type__c,agrRecordType.Bid_Header_and_Desc__c));
                        }
                    }
                    if(oppStageToBidTypeMap.containsKey(opportunityRecord.stageName.substring(0,2))) {
                        for(Opp_Stage_to_Bid_Type_Mapping__mdt agrRecordType : oppStageToBidTypeMap.get(opportunityRecord.stageName.substring(0,2))) {
                            
                            bidTypeList.add(new BidTypeWrapper(agrRecordTypes.get(agrRecordType.Agreement_Record_Type__c).getRecordTypeId(),agrRecordType.Agreement_Record_Type__c,agrRecordType.Bid_Header_and_Desc__c));
                        }
                    }                        
                    
                }          
        /*} catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }*/
        return bidTypeList;
    }
    /**
     * This method is used to return the validation on Opportunity
     * @params  Id opportunityId
     * @params  String recordTypeId
     * @return  Boolean
     */
    @AuraEnabled
    public static Boolean validateOpportunity(Id opportunityId, String recordTypeId) {
        Set<String> oppStageToBidTypeMapFieldSet = new Set<String>{ 'Opp_Stage__c', 'Agreement_Record_Type__c' };
        Map<String,Set<String>> oppStageToBidTypeMap = new Map<String,Set<String>>();
        for(Opp_Stage_to_Bid_Type_Mapping__mdt oppStageToBidType :
                        new SLT_OppStageToBidTypeMapping(true, false).getOppStageToBidTypeMappings(oppStageToBidTypeMapFieldSet)) {
                            if(!oppStageToBidTypeMap.containsKey(oppStageToBidType.Opp_Stage__c)) {
                                oppStageToBidTypeMap.put(oppStageToBidType.Opp_Stage__c,new Set<String>());
                                oppStageToBidTypeMap.get(oppStageToBidType.Opp_Stage__c).add(oppStageToBidType.Agreement_Record_Type__c);
                            }
                            else {
                                oppStageToBidTypeMap.get(oppStageToBidType.Opp_Stage__c).add(oppStageToBidType.Agreement_Record_Type__c);
                            }
                     }
            Opportunity opp = [Select id, StageName from Opportunity where id = :opportunityId];
            
            if(!CON_OWF.OWF_AGREEMENT_RECORD_TYPE_BID_HISTORY.contains(recordTypeId)) {
                return false;
            }
            
            if(oppStageToBidTypeMap.containsKey(opp.StageName.substring(0,1)) && oppStageToBidTypeMap.get(opp.StageName.substring(0,1)).contains(Schema.getGlobalDescribe().get('Apttus__APTS_Agreement__c').getDescribe().getRecordTypeInfosById().get(recordTypeId).getName()))
            {
            return false;
            }
            else if(oppStageToBidTypeMap.containsKey(opp.StageName.substring(0,2)) && oppStageToBidTypeMap.get(opp.StageName.substring(0,2)).contains(Schema.getGlobalDescribe().get('Apttus__APTS_Agreement__c').getDescribe().getRecordTypeInfosById().get(recordTypeId).getName()))
            {
            return false;
            }
                     
            return true;
    }
    /**
     * This method is used to return the validation on Recordtype
     * @params  String recordTypeId
     * @return  Boolean
     */
    @AuraEnabled
    public static Boolean validateRecordType(String recordTypeId) {
        if(CON_OWF.OWF_AGREEMENT_RECORD_TYPE_BID_HISTORY.contains(recordTypeId))
        {
            return true;
        }
        return false;
    }
     /**
     * This method is used to return the validation on Recordtype
     * @params  String recordTypeId
     * @return  Boolean
     */
    @AuraEnabled
    public static String getRecorTypeName(String recordTypeId) {
        String name = String.valueOf(Schema.getGlobalDescribe().get('Apttus__APTS_Agreement__c').getDescribe().getRecordTypeInfosById().get(recordTypeId).getName());
        if(name == 'Clinical Bid')
        {
            return Schema.getGlobalDescribe().get('Apttus__APTS_Agreement__c').getDescribe().getRecordTypeInfosByName().get('Clinical').getRecordTypeId();
        }
        else if(name == 'RFI Request')
        {
            return Schema.getGlobalDescribe().get('Apttus__APTS_Agreement__c').getDescribe().getRecordTypeInfosByName().get('RFI').getRecordTypeId();
        }
        else 
        {
            return recordTypeId;
        }
    }
    /**
     * This method is used to get details of current oppty
     * @params  Id opportunityId
     * @params  List<String> oppFields
     * @return  Opportunity oppty
     */
    @AuraEnabled
    public static Id getAccountId(Id opportunityId, List<String> oppFields) {
        Map<Id, Opportunity> idToOpportunityMap = new Map<Id, Opportunity>();        
        Opportunity opportunityRecord = new Opportunity();
        idToOpportunityMap = SRV_CRM_Opportunity.getOppDetail(new Set<Id>{opportunityId}, new Set<String>(oppFields));
        if(idToOpportunityMap.size() > 0) {
            opportunityRecord = idToOpportunityMap.get(opportunityId);
        }
        return opportunityRecord.Account.id;
    }
    
    /**
     * This method is used to get pre-populate the fields
     * @params  Id opportunityId
     * @params  Id recordTypeId
     * @return  String
     */
    @AuraEnabled
    public static String getDefaultFieldValues(Id opportunityId, Id recordTypeId) {
        system.debug('recordTypeId-->' + recordTypeId);
        Schema.RecordTypeInfo recordTypeInfo = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosById().get(recordTypeId);
        String rtDeveloperName = recordTypeInfo.getDeveloperName();
        List<OWF_ReBidFieldMapping__mdt> fieldMappingsForRecordType = new List<OWF_ReBidFieldMapping__mdt>();
        List<OWF_ReBidFieldMapping__mdt> rebidFieldMappingList = new SLT_OWF_ReBidFieldMapping(false,false).getReBidFieldMapping();
        Set<String> opportunityFieldSet = new Set<String>{'Id','AccountId'};
        Set<String> agreementFieldSet = new Set<String>{'Id', 'Apttus__Related_Opportunity__c', 'Apttus__Account__c', 'recordTypeId'};
        Set<String> accountFieldSet = new Set<String>{'Id'};
        Set<String> userFieldSet = new Set<String>{'Id'};
        
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartObject();
        gen.writeIdField('Apttus__Related_Opportunity__c', opportunityId);
        system.debug('rtDeveloperName -->' + rtDeveloperName );
        
        Map<Id, Schema.RecordTypeInfo> recordTypeInfoIdMap = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosById();
        Map<String, Id> recordTypeDevNameToIdMap = new Map<String, Id>();
        for(Id agrRecordTypeId : recordTypeInfoIdMap.keySet()) {
            recordTypeDevNameToIdMap.put(recordTypeInfoIdMap.get(agrRecordTypeId).getDeveloperName(), agrRecordTypeId);
        }
        String recordTypeName = rtDeveloperName;
        if(recordTypeName == 'Clinical_Short_Form') {
            recordTypeId = recordTypeDevNameToIdMap.get('Clinical_Bid');
            recordTypeName = 'Clinical_Bid';
        } else if(recordTypeName == 'RFI_Short_Form') {
            recordTypeId = recordTypeDevNameToIdMap.get('RFI_Request');
            recordTypeName = 'RFI_Request';
        } else if(recordTypeName == 'CSS_Short_Form') {
            recordTypeId = recordTypeDevNameToIdMap.get('CSS_Bid');
            recordTypeName = 'CSS_Bid';
        }
        
        if(rebidFieldMappingList.size() > 0){
            for(OWF_ReBidFieldMapping__mdt rebidMapping : rebidFieldMappingList){
                if(rebidMapping.RecordType__c == recordTypeName ){
                    fieldMappingsForRecordType.add(rebidMapping);
                    if(rebidMapping.SourceType__c == 'Field'){
                        if(rebidMapping.SourceObject__c == 'Agreement')
                            agreementFieldSet.add(rebidMapping.SourceValue__c);
                        else if(rebidMapping.SourceObject__c == 'Opportunity')
                            opportunityFieldSet.add(rebidMapping.SourceValue__c);
                        else if(rebidMapping.SourceObject__c == 'Account')
                            accountFieldSet.add(rebidMapping.SourceValue__c);
                        else if(rebidMapping.SourceObject__c == 'User')
                            userFieldSet.add(rebidMapping.SourceValue__c);
                    }
                }
            }
        }
        system.debug('agreementFieldSet-->' + agreementFieldSet);
        system.debug('opportunityFieldSet-->' + opportunityFieldSet);
        Map<Id, Opportunity> opportunityMap = new SLT_Opportunity(false,false).selectByOpportunityId(new Set<Id>{opportunityId}, opportunityFieldSet);
        Opportunity oppRecord = opportunityMap.values()[0];
        Map<Id, Account> accountMap = new SLT_Account().selectByAccountId(new Set<Id>{oppRecord.AccountId}, accountFieldSet);
        Map<Id, Apttus__APTS_Agreement__c> agreementMap = new SLT_APTS_Agreement(false,false).getAgreementByOppIDSortByCreatedDateDesc(new Set<Id>{opportunityId}, agreementFieldSet);
        Map<Id, User> userMap = new SLT_User().selectByUserId(new Set<Id>{UserInfo.getUserId()}, userFieldSet);
        User runningUser = userMap.values()[0];
        system.debug('opportunityMap-->' + opportunityMap);
        gen.writeIdField('Apttus__Account__c', oppRecord.AccountId);
        
        
        String bidType = 'Initial';
        Apttus__APTS_Agreement__c initialBidRecord = new Apttus__APTS_Agreement__c();
        system.debug('agreementMap.values()-->' + agreementMap.values());
        system.debug('recordTypeId' + recordTypeId);
        for(Apttus__APTS_Agreement__c agreement : agreementMap.values()){
            if(agreement.RecordTypeId == recordTypeId){
                initialBidRecord = agreement;
                bidType = 'Rebid';
                break;
            }
        }
        
        Map<String, String> fieldDataTypeToFieldAPINameMap = new UTL_Sobject('Apttus__APTS_Agreement__c').getFieldDataTypeByFieldAPIName();
        system.debug('fieldMappingsForRecordType-->' + fieldMappingsForRecordType);
        for(OWF_ReBidFieldMapping__mdt rebidFieldMapping : fieldMappingsForRecordType) {
            String fieldValue = null;
            if(rebidFieldMapping.BidType__c != 'None' && rebidFieldMapping.BidType__c != bidType)
                continue;
            if(rebidFieldMapping.SourceType__c == 'Value') {
                if(rebidFieldMapping.SourceValue__c == 'Today') {
                    fieldValue = Date.today().format();
                } else {
                    fieldValue = rebidFieldMapping.SourceValue__c;
                }
            } else {
                if(rebidFieldMapping.SourceObject__c == 'Opportunity') {
                    if(opportunityMap.containsKey(opportunityId)) {
                        if(rebidFieldMapping.SourceValue__c.contains('.')) {
                            List<String> fieldNames = rebidFieldMapping.SourceValue__c.split('\\.');
                            fieldValue = (String)opportunityMap.get(opportunityId).getSobject(fieldNames[0]).get(fieldNames[1]);
                        } else {
                            fieldValue = (String)opportunityMap.get(opportunityId).get(rebidFieldMapping.SourceValue__c);       
                        }
                    }
                } else if(rebidFieldMapping.SourceObject__c == 'Agreement') {
                    if(initialBidRecord != null) {  
                          fieldValue = String.valueOf(initialBidRecord.get(rebidFieldMapping.SourceValue__c) );
                    }
                    
                } else if(rebidFieldMapping.SourceObject__c == 'Account') {
                    if(accountMap.containsKey(oppRecord.AccountId)) {
                        fieldValue = (String)accountMap.get(oppRecord.AccountId).get(rebidFieldMapping.SourceValue__c);
                    }
                } else if(rebidFieldMapping.SourceObject__c == 'User') {
                    fieldValue = (String)runningUser.get(rebidFieldMapping.SourceValue__c);
                }
            }
            String field = rebidFieldMapping.TargetField__c;
            String fieldType = fieldDataTypeToFieldAPINameMap.get(field.trim().toLowerCase());
            if(fieldValue == null) {
                gen.writeNullField(rebidFieldMapping.TargetField__c);
            } else if(fieldType == 'currency' || fieldType == 'double' || fieldType == 'percent' || fieldType == 'decimal') {
                gen.writeNumberField(rebidFieldMapping.TargetField__c, Decimal.valueOf(fieldValue));
            } else if(fieldType == 'boolean') {
                gen.writeBooleanField(rebidFieldMapping.TargetField__c, Boolean.valueOf(fieldValue));
            } else if(fieldType == 'date') {
                if(rebidFieldMapping.SourceType__c == 'Value') {
		    String[] arrDate = fieldValue.split('/');
                    fieldValue = date.newinstance(Integer.valueOf(arrDate[2]), Integer.valueOf(arrDate[0]), Integer.valueOf(arrDate[1])).format();
                    gen.writeDateField(rebidFieldMapping.TargetField__c, Date.parse(fieldValue));                   
                } else {
                    gen.writeDateField(rebidFieldMapping.TargetField__c, Date.valueOf(fieldValue));
                }
            } else {
                gen.writeStringField(rebidFieldMapping.TargetField__c, fieldValue);
            }
        }
        gen.writeIdField('RecordTypeId', recordTypeId);
        gen.writeEndObject();
        return gen.getAsString();
    }
    
    public class BidTypeWrapper {
      @AuraEnabled public String name;
      @AuraEnabled public String typeId;
      @AuraEnabled public String bidHeader;
      @AuraEnabled public String bidDescription;        
      public BidTypeWrapper(String typeIdParam, String nameParam, String bidHeaderAndDesc ){            
            typeId = typeIdParam;
            name = nameParam;
            String kept = bidHeaderAndDesc.substring( 0, bidHeaderAndDesc.indexOf(','));
            String remainder = bidHeaderAndDesc.substring(bidHeaderAndDesc.indexOf(',')+1, bidHeaderAndDesc.length());    
            //List<String> tempLabelList = bidHeaderAndDesc.split(',');
            bidHeader = kept;
            bidDescription = remainder;
      }
        
    }
}
