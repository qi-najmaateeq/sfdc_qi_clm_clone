@isTest
private class TST_SLT_ChallangeMatrix {

    static Challenge_Matrix__c setChallangeMatrixData(){
        Challenge_Matrix__c challangeMatrix = UTL_TestData.createChallengeMatrix(CON_CPQ.LABOR_FEES_0TO3, CON_CPQ.OPPORTUNITY_RFP, CON_CPQ.REVIEW_TYPE_CHALLENGE,
                                      CON_CPQ.EMAIL);
        insert challangeMatrix;
        return challangeMatrix;
    }
    
    @isTest
    static void testSelectChallangeMatrixById(){
        Challenge_Matrix__c testChallangeMatrix  = setChallangeMatrixData();
        
        Test.startTest();
        	List<Challenge_Matrix__c> challangeMatrixList = new SLT_ChallangeMatrix().selectById(new Set<Id>{testChallangeMatrix.Id});
        Test.stopTest();
    }
    
    @isTest
    static void testSelectChallangeMatrixCondition(){
        setChallangeMatrixData();
        
        Test.startTest();
        	List<Challenge_Matrix__c> testChallangeList = new SLT_ChallangeMatrix().selectChallangeMatrixCondition(CON_CPQ.LABOR_FEES_0TO3,
                                                              CON_CPQ.OPPORTUNITY_RFP, CON_CPQ.REVIEW_TYPE_CHALLENGE);
        Test.stopTest();
    }
}