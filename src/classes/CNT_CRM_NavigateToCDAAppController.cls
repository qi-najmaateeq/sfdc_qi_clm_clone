/*
 * Version       : 1.0
 * Description   : Apex Controller for NavigateToCDAAppController.
 */
public class CNT_CRM_NavigateToCDAAppController {
    /**
     * This method used to get custom setting instance
     * @return  Legacy_Org_Link__c
     */
    @AuraEnabled
    public static Legacy_Org_Link__c getHiearchySettings(){
        return Legacy_Org_Link__c.getInstance();
    }
}