@isTest
    private class TST_CNT_CPQ_QCSelfCheckDraft{
    
        static Apttus__APTS_Agreement__c getAgreementData(Id OpportuntiyId, String recordTypeName){
        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.Opportunity_Type__c = COn_CPQ.OPPORTUNITY_RFP;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }

    static QC_Check_List_Item__c setQCCheckListItemData(String typeOfProcessStep, String typeOfBid){
    
        QC_Check_List_Item__c qcCheckListItem = UTL_TestData.createQCCheckListItem();
        qcCheckListItem.Type_Of_Process_Step__c = typeOfProcessStep;
        qcCheckListItem.Type_Of_Bids__c = typeOfBid;
        qcCheckListItem.Record_Type__c = CON_CPQ.INITIAL_BID_REBID;
        insert qcCheckListItem;
        return qcCheckListItem;
    }

    static Proposal_QA_Self_Check_List__c setProposalQASelfCheckListData(String agreementId, String typeOfProcessStep, String typeOfBid){
    
        Proposal_QA_Self_Check_List__c proposalQASelfCheckList = UTL_TestData.createProposalQASelfCheckList();
        proposalQASelfCheckList.Agreement__c = agreementId;
        proposalQASelfCheckList.Type_Of_Process_Step__c = typeOfProcessStep;
        proposalQASelfCheckList.Type_Of_Bids__c = typeOfBid;
        insert proposalQASelfCheckList;
        return proposalQASelfCheckList;
    }

    static List<Proposal_QA_Self_Check_List__c> getProposalQASelfCheckListData(String agreementId){
        return [SELECT Id FROM Proposal_QA_Self_Check_List__c WHERE Agreement__c =: agreementId];
    }

    @isTest
    static void testFetchCheckListRecordsShouldReturnQCDraftList(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        insert agreement;
        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData('Draft', CON_CPQ.OPPORTUNITY_RFP);

        Test.startTest();
            List<CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper> qcCheckListForDraft =
                CNT_CPQ_QCSelfCheckDraft.fetchQCCheckListRecords(agreement.Id, CON_CPQ.QC_SELF_CHECK_DRAFT);
        Test.stopTest();

        system.assertEquals(1, qcCheckListForDraft.size(),'Should Return QC Draft List');
    }

    @isTest
    static void testFetchCheckListRecordsShouldReturnQCFinalList(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_FINAL;
        agreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
        insert agreement;
        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData('Final', CON_CPQ.OPPORTUNITY_RFP);

        Test.startTest();
            List<CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper> qcCheckListForDraft =
                CNT_CPQ_QCSelfCheckDraft.fetchQCCheckListRecords(agreement.Id, CON_CPQ.QC_SELF_CHECK_FINAL);
        Test.stopTest();

        system.assertEquals(1, qcCheckListForDraft.size(),'Should Return QC Final List');
    }

    @isTest
    static void testFetchCheckListRecordsShouldReturnQCRebidList(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_REBID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        insert agreement;
        QC_Check_List_Item__c qcCheckListItem = setQCCheckListItemData('Draft', CON_CPQ.REBID);

        Test.startTest();
            List<CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper> qcCheckListForDraft =
                CNT_CPQ_QCSelfCheckDraft.fetchQCCheckListRecords(agreement.Id, CON_CPQ.QC_SELF_CHECK_DRAFT);
        Test.stopTest();

        system.assertEquals(1, qcCheckListForDraft.size(),'Should Return QC Rebid List');
    }

    @isTest
    static void testFetchCheckListRecordsShouldReturnQCListForExistingProposal(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_REBID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        insert agreement;
        setProposalQASelfCheckListData(agreement.Id, 'Draft', CON_CPQ.REBID);

        Test.startTest();
            List<CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper> qcCheckListForDraft =
                CNT_CPQ_QCSelfCheckDraft.fetchQCCheckListRecords(agreement.Id, CON_CPQ.QC_SELF_CHECK_DRAFT);
        Test.stopTest();

        system.assertEquals(1, qcCheckListForDraft.size(),'Should Return QC List For Existing Propsal');
    }

    @isTest
    static void testsaveProposalQASelfCheckListShouldReturnPropsalList(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_REBID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        insert agreement;
        QC_Check_List_Item__c qcRecord = setQCCheckListItemData('Draft', CON_CPQ.REBID);
        List<CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList =
            new List<CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper>();
        CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper  wrapperObj = new CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper(
            new Proposal_QA_Self_Check_List__c(Agreement__c = agreement.Id, Type__c = qcRecord.Type__c, Question__c = qcRecord.Question__c, 
                Guidelines__c = qcRecord.Guidelines__c), true, proposalQASelfCheckListWrapperList.size());
        proposalQASelfCheckListWrapperList.add(wrapperObj);
        String proposalQASelfCheckListWrapperListString = JSON.serialize(proposalQASelfCheckListWrapperList);

        Test.startTest();
            CNT_CPQ_QCSelfCheckDraft.saveProposalQASelfCheckList(proposalQASelfCheckListWrapperListString, agreement.Id,
                CON_CPQ.QC_SELF_CHECK_DRAFT, false, 'testing');
        Test.stopTest();

        List<Proposal_QA_Self_Check_List__c> proposalQASelfCheckList = getProposalQASelfCheckListData(agreement.Id);
        system.assertEquals(1, proposalQASelfCheckList.size(),'Should Return Proposal List');
    }



    @isTest
    static void testAddMoreItemInListShouldReturnPropsalList(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_REBID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        insert agreement;
        QC_Check_List_Item__c qcRecord = setQCCheckListItemData('Draft', CON_CPQ.REBID);
        List<CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper> proposalQASelfCheckListWrapperList =
            new List<CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper>();
        CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper  wrapperObj = new CNT_CPQ_QCSelfCheckDraft.ProposalQASelfCheckListWrapper(
            new Proposal_QA_Self_Check_List__c(Agreement__c = agreement.Id, Type__c = qcRecord.Type__c, Question__c = qcRecord.Question__c, 
                Guidelines__c = qcRecord.Guidelines__c), false, proposalQASelfCheckListWrapperList.size());
        proposalQASelfCheckListWrapperList.add(wrapperObj);
        String proposalQASelfCheckListWrapperListString = JSON.serialize(proposalQASelfCheckListWrapperList);

        Test.startTest();
            proposalQASelfCheckListWrapperList =
                CNT_CPQ_QCSelfCheckDraft.addMoreItemInList(proposalQASelfCheckListWrapperListString, agreement.Id);
        Test.stopTest();

        system.assertEquals(2, proposalQASelfCheckListWrapperList.size(),'Should Return Proposal List');
    }

    @isTest
    public static void TestFetchOverAllCommentsShouldReturnLineManagerComment(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_REBID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        agreement.Customer_Opportunity_Background__c = 'testing line manager';
        insert agreement;

        Test.startTest();
            String Comments =
                CNT_CPQ_QCSelfCheckDraft.fetchOverAllComments(agreement.Id, CON_CPQ.LINE_MANAGER_QC);
        Test.stopTest();

    system.assertEquals('testing line manager', Comments,'Should Return Line Manager Comment');
    }

    @isTest
    public static void TestFetchOverAllCommentsShouldReturnFinalQCComment(){
    
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_REBID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        agreement.Agreement_Purpose_List__c = 'testing final qc';
        insert agreement;

        Test.startTest();
            String Comments =
                CNT_CPQ_QCSelfCheckDraft.fetchOverAllComments(agreement.Id, CON_CPQ.FINAL_QC);
        Test.stopTest();

        system.assertEquals('testing final qc', Comments,'Should Return Final QC Comment');
    }
}