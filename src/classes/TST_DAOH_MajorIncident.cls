@isTest
public class TST_DAOH_MajorIncident {

    @testSetup
    private static void dataSetup(){
        List<Major_Incident__c> majorList = new List<Major_Incident__c>();
        Major_Incident__c majorRecordOne = TST_CSM_TestDataFactory.createMajorIncident();
        majorList.add(majorRecordOne);
        Major_Incident__c majorRecordTwo = TST_CSM_TestDataFactory.createMajorIncident();
        majorList.add(majorRecordTwo);
        insert majorList;
        
        Account accountRecord = TST_CSM_TestDataFactory.createAccount();
        insert accountRecord;
        Contact contactRecord = TST_CSM_TestDataFactory.createContact(accountRecord.Id, 'Udit');
        insert contactRecord;
        
        Id technoCaseRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        Case caseRecordOne = TST_CSM_TestDataFactory.createCase(accountRecord.Id, contactRecord.Id, technoCaseRecordTypeId, null, 'Customer Portal', null, 'Techno Case 1', 'Techno Description 1');
        caseList.add(caseRecordOne);
        Case caseRecordTwo = TST_CSM_TestDataFactory.createCase(accountRecord.Id, contactRecord.Id, technoCaseRecordTypeId, null, 'Agent Initiated', null, 'Techno Case 2', 'Techno Description 2');
        caseList.add(caseRecordTwo);
        insert caseList;
    }
    
    @isTest
    public static void testValidateInternalEmailAddresses(){
        Major_Incident__c majorRecord = [SELECT Id, Name FROM Major_Incident__c LIMIT 1];
        majorRecord.Internal_Email_Addresses__c = 'testMail; ;testUser@info.com;';
        Test.startTest();
        try{
            update majorRecord;
        }
        catch(Exception ex){
            Boolean expectedExceptionThrown = ex.getMessage().contains('Invalid Email Address Format In Internal Email Addresses') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
        Test.stopTest();
    }
    
    @isTest
    public static void testRestrictNewMajorIncident(){
        List<Major_Incident__c> majorList = [SELECT Id, Name FROM Major_Incident__c];
        Account accountRecord = [SELECT Id, Name FROM Account LIMIT 1];
        List<Case> caseList = [SELECT Id, CaseNumber FROM Case];
        for(Major_Incident__c majorRecord : majorList){
            MI_AccountRelationship__c miaRecord = new MI_AccountRelationship__c();
            miaRecord.Major_Incident__c = majorRecord.Id;
            miaRecord.Accounts_Impacted__c = accountRecord.Id;
            insert miaRecord;
            List<MI_CaseRelationship__c> micList = new List<MI_CaseRelationship__c>();
            for(Case caseObject : caseList){
                MI_CaseRelationship__c micRecord = new MI_CaseRelationship__c();
                micRecord.Major_Incident__c = majorRecord.Id;
                micRecord.Cases_Relationship__c = caseObject.Id;
                micList.add(micRecord);
            }
            insert micList;
        }
        update majorList;
        
        majorList[0].Status__c = 'Resolved';
        update majorList[0];
        
        for(Major_Incident__c majorRecord : majorList){
            majorRecord.Status__c = 'In Progress';
        }
        Test.startTest();
        try{
            update majorList;
        }
        catch(Exception ex){
            Boolean expectedExceptionThrown = ex.getMessage().contains('New Major Incident can\'t be progressed while other is ongoing') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
    }
}