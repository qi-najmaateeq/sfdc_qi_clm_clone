/**
 * This test class is used to test all methods in Project service class.
 * version : 1.0
 */
@isTest
private class TST_SLT_Project {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        Indication_List__c indication = UTL_OWF_TestData.createIndication('Test Indication', 'Acute Care');
        insert indication; 
        Line_Item_Group__c lineItemGroup = UTL_OWF_TestData.createLineItemGroup(indication.Id);
        lineItemGroup.BD_Lead_Sub_Region__c = 'United States of America';
        insert lineItemGroup;
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        opp.Potential_Regions__c = 'Global';
        opp.Line_of_Business__c = 'Core Clinical';
        opp.Line_Item_Group__c = lineItemGroup.Id;
        insert opp;
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        insert agreement; 
    }
    
    /**
     * This test method used to cover basic methods
     */ 
    static testMethod void testServiceProject() {   
        List<Schema.SObjectField> projectList = new SLT_Project().getSObjectFieldList();
        Schema.SObjectType project = new SLT_Project(true).getSObjectType();
    }    
    
    /**
     * This test method used to cover getProjectByAgreementID
     */ 
    static testMethod void testGetProjectByAgreementID() {   
        List<Apttus__APTS_Agreement__c> agreementList = [Select Id From Apttus__APTS_Agreement__c Limit 1];
        Set<ID> agreementIDSet = new Set<ID>{agreementList[0].Id};
        Set<String> projectFieldSet = new Set<String>{'Id'};
            
        Test.startTest();
            Map<Id, pse__Proj__c> projectMap = new SLT_Project(true).getProjectByAgreementID(agreementIDSet, projectFieldSet);
        Test.stopTest();
        
        
        
    }    
}