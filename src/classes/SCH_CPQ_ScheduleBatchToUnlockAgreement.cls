global class SCH_CPQ_ScheduleBatchToUnlockAgreement implements Schedulable{

    global void execute(SchedulableContext sc) {

        database.executebatch(new BCH_CPQ_BatchToUnlockAgreementRecord(), 5);
    }
}