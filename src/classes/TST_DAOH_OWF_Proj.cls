@isTest
private class TST_DAOH_OWF_Proj {
    /**
    * This method is used to setup data for all methods.
    */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        
        OWF_Config__c owfConfig = UTL_OWF_TestData.setupOWFConfig(grp.Id);
        insert owfConfig;
        
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        
        pse__Permission_Control__c grpPermissionControl = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert grpPermissionControl;
        
        List<OWF_Resources_Needed_to_SubGroup_Map__c> resourceToSubGroupSettingList = new List<OWF_Resources_Needed_to_SubGroup_Map__c>();
        resourceToSubGroupSettingList.add(new OWF_Resources_Needed_to_SubGroup_Map__c(
            Resources_Needed__c = 'Clinical Analytics & Simulations',
            Sub_Group__c = 'CP&A-CA&S',
            Name = 'Clinical Analytics & Simulations'
        ));
        resourceToSubGroupSettingList.add(new OWF_Resources_Needed_to_SubGroup_Map__c(
            Resources_Needed__c = 'Medical',
            Sub_Group__c = 'Medical-MSL',
            Name = 'Medical'
        ));
        resourceToSubGroupSettingList.add(new OWF_Resources_Needed_to_SubGroup_Map__c(
            Resources_Needed__c = 'TOPS',
            Sub_Group__c = 'CP&A-TOPS',
            Name = 'TOPS'
        ));
        
        insert resourceToSubGroupSettingList;
        
    }
    
    /**
    * This test method used for Creating RR Based On Agreement Associated With Project
    */
    static testMethod void testCreateRRBasedOnAgrAssociatedWithProj(){
        Account account = [Select Id From Account Where Name = 'TestAccount'];
        Opportunity oppty = [Select Id From Opportunity Where Name = 'TestOpportunity'];
        
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreement(account.Id, oppty.Id);
        agreement.Resources_Needed__c = 'TOPS;Medical';
        agreement.Bid_Due_Date__c = Date.today().addDays(7);
        agreement.recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Early_Engagement_Bid').getRecordTypeId();
        
        Test.startTest();
            insert agreement;
        Test.stopTest();
        system.assert([Select id from pse__Resource_Request__c].size() > 0);
    }
    
    /**
    * This test method used for Deleting Resource Request Based On Project
    */
    static testMethod void testDeleteResourceRequestBasedOnProject(){
        Account account = [Select Id From Account Where Name = 'TestAccount'];
        Opportunity oppty = [Select Id From Opportunity Where Name = 'TestOpportunity'];
        
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreement(account.Id, oppty.Id);
        agreement.Resources_Needed__c = 'TOPS;Medical';
        agreement.Bid_Due_Date__c = Date.today().addDays(7);
        agreement.recordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Early_Engagement_Bid').getRecordTypeId();
        insert agreement;
        
        Map<Id,pse__Proj__c> bidProject = new Map<Id,pse__Proj__c>([Select Id From pse__Proj__c Where Agreement__c = :agreement.Id]);
        
        Test.startTest();
            delete bidProject.values();
        Test.stopTest();
        
        List<pse__Resource_Request__c> rrList = [Select Id From pse__Resource_Request__c Where pse__Project__c = :bidProject.KeySet()];
        Integer expected = 0;
        System.assertEquals(expected, rrList.size());
    }
    
    /**
    * This test method used for Creating RR Based On Agreement Associated With Project
    */
    static testMethod void testCreateRRBasedOnRFIAgrAssociatedWithProj(){
        Account account = [Select Id From Account Where Name = 'TestAccount'];
        Opportunity oppty = [Select Id From Opportunity Where Name = 'TestOpportunity'];
        
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreement(account.Id, oppty.Id);
        agreement.Resources_Needed__c = 'TOPS;Medical';
        agreement.Bid_Due_Date__c = Date.today().addDays(7);
        agreement.recordTypeId = CON_OWF.OWF_RFI_AGREEMENT_RECORD_TYPE_ID;
        
        Test.startTest();
            insert agreement;
        Test.stopTest();
        system.assert([Select id from pse__Resource_Request__c].size() > 0);
    }
    
    /**
     * This test method used to validate created Resource Request based on project inserted
     */ 
    static testMethod void testCreateClinicalBidRRsBasedOnBidNoOfProjects_Scenario1() {
        Account account = [Select Id From Account Where Name = 'TestAccount'];
        Opportunity oppty = [Select Id From Opportunity Where Name = 'TestOpportunity'];
        
        Test.startTest();
            //oppty.Line_Item_Group__c = lineItemGroup.Id;
            oppty.Potential_Regions__c = 'Asia Pacific';
            oppty.Line_of_Business__c = 'Core Clinical';
            update oppty;
            
            Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(account.Id, oppty.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
            agreement.Resources_Needed__c = 'TOPS;Medical';
            agreement.Bid_Due_Date__c = Date.today().addDays(7);
            insert agreement;
            
            List<pse__Resource_Request__c> resReqsList = [Select Id, SubGroup__c From pse__Resource_Request__c Where Agreement__c = :agreement.Id];
                System.assertEquals(1, resReqsList.size());
                System.assertEquals(CON_OWF.RES_REQ_TYPE_GBO_LEAD_PD, resReqsList.get(0).SubGroup__c);
                
            Apttus__APTS_Agreement__c agreement1 = UTL_OWF_TestData.createAgreementByRecordType(account.Id, oppty.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
            agreement1.Resources_Needed__c = 'TOPS;Medical';
            agreement1.Bid_Due_Date__c = Date.today().addDays(5);
            insert agreement1;
            resReqsList = [Select Id, SubGroup__c From pse__Resource_Request__c Where Agreement__c = :agreement1.Id];
                System.assertEquals(0, resReqsList.size());
        Test.stopTest();
    }
}