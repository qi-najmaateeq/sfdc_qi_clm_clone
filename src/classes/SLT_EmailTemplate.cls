public class SLT_EmailTemplate extends fflib_SObjectSelector{
    /**
    * constructor to initialise CRUD and FLS
    */
    public SLT_EmailTemplate() {
        super(false, false, false);
    }

    /**
    * This method used to get field list of sobject
    * @return  List<Schema.SObjectField>
    */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
        EmailTemplate.Id,
        EmailTemplate.Name
        };
    }

    /**
    * This method used to set up type of sobject
    * @return  Schema.SObjectType
    */
    public Schema.SObjectType getSObjectType() {
        return EmailTemplate.sObjectType;
    }

    /**
    * This method used to get EmailTemplate by Id
    * @return  List<EmailTemplate>
    */
    public List<EmailTemplate> selectById(Set<ID> idSet) {
        return (List<EmailTemplate>) selectSObjectsById(idSet);
    }

    /**
    * This method used to get Email Template by developer name
    * @return  List<EmailTemplate>
    */
    public List<EmailTemplate> selectEmailDTemplateByDeveloperName(String developerName, Set<String> fieldSet) {
        return (List<EmailTemplate>)Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('developerName =: developerName ').toSOQL());
    }

}