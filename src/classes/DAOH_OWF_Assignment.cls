/**
* This is Assignment trigger handler class.
* version : 1.0
*/
public without sharing class DAOH_OWF_Assignment {
    /**
    * This method is used to set Number of Requested Services based on selected Requested Services.
    * @params  newList List<Apttus__APTS_Agreement__c>
    * @params  oldMap Map<Id, Apttus__APTS_Agreement__c>
    * @return  void
    */
    public static void createSkillCertRatingsOnAssignCompleted(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap) {
        Set<Id> assignmentResRequestIdsSet = new Set<Id>();
        Set<Id> assignmentResourceIdsSet = new Set<Id>();
        for(pse__Assignment__c assignment : newList) {
            if(assignment.pse__Status__c != oldMap.get(assignment.Id).pse__Status__c && assignment.pse__Status__c == CON_OWF.OWF_STATUS_COMPLETED) {
                if(assignment.pse__Resource_Request__c != NULL) {
                    assignmentResRequestIdsSet.add(assignment.pse__Resource_Request__c);
                }
                
                if(assignment.pse__Resource__c != NULL) {
                    assignmentResourceIdsSet.add(assignment.pse__Resource__c);
                }
            }
        }
        
        List<pse__Skill_Certification_Rating__c> skillCertRatingInsertList = validateAndCreateSkillCertRatingsToRelatedResource(newList, oldMap, assignmentResRequestIdsSet, assignmentResourceIdsSet);
        if(skillCertRatingInsertList.size() > 0) {
            insert skillCertRatingInsertList;
        }
    }
    
    /**
    * This method is used to set Number of Requested Services based on selected Requested Services.
    * @params  newList List<Apttus__APTS_Agreement__c>
    * @params  oldMap Map<Id, Apttus__APTS_Agreement__c>
    * @params  Set<Id> assignmentResRequestIdsSet
    * @params  Set<Id> assignmentResourceIdsSet
    * @return  List<pse__Skill_Certification_Rating__c> 
    */
    private static List<pse__Skill_Certification_Rating__c> validateAndCreateSkillCertRatingsToRelatedResource(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap, Set<Id> assignmentResRequestIdsSet, Set<Id> assignmentResourceIdsSet) {
        List<pse__Skill_Certification_Rating__c> skillCertRatingInsertList = new List<pse__Skill_Certification_Rating__c>();
        Map<Id, pse__Resource_Request__c> rrIdToRresourceRequestWithSkillMap = new Map<Id, pse__Resource_Request__c>();
        Map<Id, pse__Skill_Certification_Rating__c> scrIdToSkillCertiRatingMap = new Map<Id, pse__Skill_Certification_Rating__c>();
        Map<Id, Map<Id, pse__Skill_Certification_Rating__c>> scrIdToSkillCertRatingWithContactMap = new Map<Id, Map<Id, pse__Skill_Certification_Rating__c>>();
        
        if(assignmentResRequestIdsSet.size() > 0) {
            String resReqCondition = 'Id IN :sObjectIdSet And RecordType.DeveloperName = \'' + CON_OWF.OWF_RESOURCE_REQUEST_RECORD_TYPE_NAME + '\'';
            Set<String> resRequestFieldSet = new Set<String>{'Id', 'pse__Assignment__c', 'Therapy_Area__c', 'Indication__c'};
                Set<String> resSkillRequestFieldSet = new Set<String>{'Id', 'pse__Is_Primary__c', 'pse__Resource_Request__c', 'pse__Skill_Certification__c', 'pse__Skill_or_Certification__c', 'pse__Skill_Record_Type__c'};
                    String resSkillReqCondition = '(pse__Skill_Certification__r.pse__Type__c = \'' + CON_OWF.SKILL_TYPE_INDICATION 
                    + '\' OR pse__Skill_Certification__r.pse__Type__c = \''  + CON_OWF.SKILL_TYPE_THERAPY_AREA 
                    + '\' OR pse__Skill_Certification__r.pse__Type__c = \''  + CON_OWF.SKILL_TYPE_LINE_OF_BUSINESS 
                    + '\' OR pse__Skill_Certification__r.pse__Type__c = \''  + CON_OWF.SKILL_TYPE_POTENTIAL_REGION + '\')';
            rrIdToRresourceRequestWithSkillMap = new SLT_Resource_Request(false,false).selectByIdWithResReqsAndRSRs(assignmentResRequestIdsSet, resReqCondition, resRequestFieldSet, resSkillRequestFieldSet, resSkillReqCondition);
        }
        
        if(assignmentResourceIdsSet.size() > 0) {
            String skillCertRatingCondition = ' pse__Resource__c IN :sObjectIdSet ';
            Set<String> skillCertRatingFieldSet = new Set<String>{'Id', 'pse__Resource__c',  'pse__Skill_Certification__c', 'pse__Rating__c'};
                scrIdToSkillCertiRatingMap = new SLT_Skill_Certification_Rating(false,false).getSkillCertificationRatingsByContactIds(assignmentResourceIdsSet, skillCertRatingCondition, skillCertRatingFieldSet);
            
            if(!scrIdToSkillCertiRatingMap.IsEmpty()) {
                for(pse__Skill_Certification_Rating__c skillCertRating : scrIdToSkillCertiRatingMap.values()) {
                    if(!scrIdToSkillCertRatingWithContactMap.containsKey(skillCertRating.pse__Resource__c)) {
                        scrIdToSkillCertRatingWithContactMap.put(skillCertRating.pse__Resource__c, new Map<Id, pse__Skill_Certification_Rating__c>());
                    }
                    scrIdToSkillCertRatingWithContactMap.get(skillCertRating.pse__Resource__c).put(skillCertRating.pse__Skill_Certification__c, skillCertRating);
                }
            }
        }
        
        if(!rrIdToRresourceRequestWithSkillMap.IsEmpty()) {
            for(pse__Assignment__c assignment : newList) {
                if(assignment.pse__Status__c != oldMap.get(assignment.Id).pse__Status__c && assignment.pse__Status__c == CON_OWF.OWF_STATUS_COMPLETED) {
                    if(rrIdToRresourceRequestWithSkillMap.containsKey(assignment.pse__Resource_Request__c) && rrIdToRresourceRequestWithSkillMap.get(assignment.pse__Resource_Request__c).pse__Resource_Skill_Requests__r.size() > 0) {
                        for(pse__Resource_Skill_Request__c resSkillReq : rrIdToRresourceRequestWithSkillMap.get(assignment.pse__Resource_Request__c).pse__Resource_Skill_Requests__r) {
                            if(scrIdToSkillCertRatingWithContactMap.containsKey(assignment.pse__Resource__c)) {
                                if(!scrIdToSkillCertRatingWithContactMap.get(assignment.pse__Resource__c).containsKey(resSkillReq.pse__Skill_Certification__c)) {
                                    pse__Skill_Certification_Rating__c resSkillCertRatingRec = createResourceSkillCertificationRating(assignment.pse__Resource__c, resSkillReq.pse__Skill_Certification__c);
                                    skillCertRatingInsertList.add(resSkillCertRatingRec);
                                    scrIdToSkillCertRatingWithContactMap.get(assignment.pse__Resource__c).put(resSkillReq.pse__Skill_Certification__c, resSkillCertRatingRec);
                                }
                            }else {
                                pse__Skill_Certification_Rating__c resSkillCertRatingRec = createResourceSkillCertificationRating(assignment.pse__Resource__c, resSkillReq.pse__Skill_Certification__c);
                                skillCertRatingInsertList.add(resSkillCertRatingRec);
                                if(!scrIdToSkillCertRatingWithContactMap.containsKey(assignment.pse__Resource__c)) {
                                    scrIdToSkillCertRatingWithContactMap.put(assignment.pse__Resource__c, new Map<Id, pse__Skill_Certification_Rating__c>());
                                }
                                scrIdToSkillCertRatingWithContactMap.get(assignment.pse__Resource__c).put(resSkillReq.pse__Skill_Certification__c, resSkillCertRatingRec);
                            }
                        }
                    }
                }
            }
        }
        
        return skillCertRatingInsertList;
    }
    
    /**
    * This method is used to create Skill/Certification Rating record
    * @params  Id resourceId
    * @params  Id skillCertificaitonId
    * @return  pse__Skill_Certification_Rating__c
    */
    private static pse__Skill_Certification_Rating__c createResourceSkillCertificationRating(Id resourceId, Id skillCertificaitonId) {
        pse__Skill_Certification_Rating__c skillCertificationRatingRec = new pse__Skill_Certification_Rating__c();
        skillCertificationRatingRec.pse__Resource__c = resourceId;
        skillCertificationRatingRec.pse__Skill_Certification__c = skillCertificaitonId;
        skillCertificationRatingRec.pse__Rating__c = '1 - Limited Exposure';
        return skillCertificationRatingRec;
    }
    
    
    /**
    * This method is used to rollup the Assignments fields on the Resource.
    * @params  newAssignmentList List<pse__Assignment__c>
    * @params  oldAssignmentMap Map<Id,pse__Assignment__c>
    * @return  void
    */
    public static void populateRollupAssignmentFieldsOnContact(List<pse__Assignment__c> newAssignmentList, Map<Id,pse__Assignment__c> oldAssignmentMap){
        Map<Id,List<pse__Assignment__c>> resourceIdToRelatedAssignmentListMap = new Map<Id,List<pse__Assignment__c>>();
        Set<Id> resourceIdSet = new Set<Id>();
        
        if(newAssignmentList != null){
            for(pse__Assignment__c assignment : newAssignmentList){
                if(oldAssignmentMap != null && newAssignmentList != null){
                    if(assignment.pse__Status__c != oldAssignmentMap.get(assignment.Id).pse__Status__c ||
                       assignment.Suggested_FTE__c != oldAssignmentMap.get(assignment.Id).Suggested_FTE__c ||
                       assignment.pse__Resource__c != oldAssignmentMap.get(assignment.Id).pse__Resource__c){
                           
                           if(assignment.pse__Resource__c != oldAssignmentMap.get(assignment.Id).pse__Resource__c && oldAssignmentMap.get(assignment.Id).pse__Resource__c != null){
                               resourceIdSet.add(assignment.pse__Resource__c);
                           }
                           if(assignment.pse__Resource__c != null){
                               resourceIdSet.add(assignment.pse__Resource__c);
                           }
                       } 
                }else if(assignment.pse__Resource__c != null && oldAssignmentMap == null){
                    resourceIdSet.add(assignment.pse__Resource__c);
                }
            }  
        }
        if(!resourceIdSet.isEmpty()){
            String assignmentCondition = ' pse__Resource__c IN :sObjectIdSet ';
            Set<String> assignmentFieldSet = new Set<String>{'Id', 'pse__Resource__c', 'pse__Status__c', 'Suggested_FTE__c'};
                List<pse__Assignment__c> assignmentList = new SLT_Assignment(false,false).getAssignmentByResource(resourceIdSet, assignmentCondition, assignmentFieldSet);
            
            
            updateRollupAssignmentFieldsOnContact(assignmentList,resourceIdSet);
            
        }
        
        
    }
    
    /**
    * This method is used to update the Assignments fields on the Resource.
    * @params  assignmentList List<pse__Assignment__c>
    * @return  void
    */
    public static void updateRollupAssignmentFieldsOnContact(List<pse__Assignment__c> assignmentList,Set<Id> resourceIdSet){
        Map<Id,List<pse__Assignment__c>> resourceIdToRelatedAssignmentListMap = new Map<Id,List<pse__Assignment__c>>();
        try{
            if(!assignmentList.isEmpty()){
                for(pse__Assignment__c assignment : assignmentList){
                    if(!resourceIdToRelatedAssignmentListMap.containsKey(assignment.pse__Resource__c)){
                        resourceIdToRelatedAssignmentListMap.put(assignment.pse__Resource__c, new List<pse__Assignment__c>());
                    }
                    resourceIdToRelatedAssignmentListMap.get(assignment.pse__Resource__c).add(assignment);
                }   
            }else{
                for(Id i : resourceIdSet){
                    resourceIdToRelatedAssignmentListMap.put(i,new List<pse__Assignment__c>());
                }
            }
            
            List<Contact> updatedResourceList = new List<Contact>();
            for(Id resourceId : resourceIdToRelatedAssignmentListMap.keySet()){
                Decimal pendingFTECount = 0;
                Decimal currentFTECount = 0;
                Decimal pendingRecordsCount = 0;
                Decimal acceptedRecordsCount = 0;  
                if(resourceIdToRelatedAssignmentListMap.get(resourceId).size() > 0){
                    for(pse__Assignment__c assignment : resourceIdToRelatedAssignmentListMap.get(resourceId)){
                        if(assignment.pse__Status__c == 'Pending'){
                            pendingRecordsCount++;  
                            pendingFTECount += assignment.Suggested_FTE__c;
                        }else if(assignment.pse__Status__c == 'Accepted'){
                            acceptedRecordsCount++;
                            currentFTECount += assignment.Suggested_FTE__c;
                        }
                    }  
                }
                Contact contactRecord = new Contact(Id = resourceId);
                contactRecord.Pending_FTE_Sum__c = pendingFTECount;
                contactRecord.Current_FTE_Sum__c = currentFTECount ;
                contactRecord.COUNT_Assignemnts_Pending__c = pendingRecordsCount;
                contactRecord.COUNT_Assignemnts_Accepted__c = acceptedRecordsCount;
                
                updatedResourceList.add(contactRecord);
            }
            
            if(!updatedResourceList.isEmpty()){
                update updatedResourceList;
            }    
        }catch(Exception e){
            system.debug('Error: ' + e.getMessage() + ' at Line number: ' + e.getLineNumber());
        }
    }
    
     /**
    * This method is used to reassign/Update the owner of the Assignment.
    * @params  newList List<Apttus__APTS_Agreement__c>
    * @params  oldMap Map<Id, Apttus__APTS_Agreement__c>
    * @return  void
    */
    public static void updateAssignmentOwner(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap) {
        
        Map<id,Id> resourceIdToUserIdMap = new Map<id,Id>();
        set<id> resourceIdSet = new set<id>();
        List<Contact> contactList= new List<Contact>();
        pse__Assignment__c assignmnetOld = new pse__Assignment__c();
        
        for (pse__Assignment__c assignmnetNew : newList) {
            resourceIdSet.add(assignmnetNew.pse__Resource__c);
        }
       
        if(!resourceIdSet.isEmpty()){
            String resourceCondition = 'id in : resourceIdSet ';
            Set<String> resourceFieldSet = new Set<String>{'Id', 'Salesforce_User__c', 'pse__Salesforce_User__c'};
                contactList = new SLT_Contact().selectByContactIdList(resourceIdSet, resourceFieldSet);       
        }
        
        for (Contact resource : contactList) {
            if(resource.pse__Salesforce_User__c != null)
                resourceIdToUserIdMap.put(resource.id,resource.pse__Salesforce_User__c);
        }   
         
        for (pse__Assignment__c assignmnetNew : newList) { 
            
            if(oldMap != null && oldMap.containskey(assignmnetNew.Id))
                assignmnetOld = oldMap.get(assignmnetNew.Id);
            
            if(resourceIdToUserIdMap != null && resourceIdToUserIdMap.containsKey(assignmnetNew.pse__Resource__c) && resourceIdToUserIdMap.get(assignmnetNew.pse__Resource__c) != null && ((Trigger.isInsert && assignmnetNew.pse__Resource__c != null) || ( Trigger.isUpdate && (assignmnetNew.pse__Resource__c != assignmnetOld.pse__Resource__c || assignmnetNew.OwnerId != resourceIdToUserIdMap.get(assignmnetNew.pse__Resource__c))))){
                assignmnetNew.OwnerId = resourceIdToUserIdMap.get(assignmnetNew.pse__Resource__c);
            }
        }
    } 
    
         /**
    * This method is used to to set the owner of the Agreement, based on the Assignment.
    * @params  newList List<Apttus__APTS_Agreement__c>
    * @params  oldMap Map<Id, Apttus__APTS_Agreement__c>
    * @return  void
    */
    public static void updateAgreementOwnerByAssignment(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap) {
        
        Map<id,Id> resourceIdToUserIdMap = new Map<id,Id>();
        set<id> resourceIdSet = new set<id>();
        set<id> agreementIdSet = new set<id>();
        set<id> resourceRequestIdSet = new set<id>();
        List<Contact> contactList= new List<Contact>();
        Map<id,pse__Resource_Request__c> rrIdToResourceRequestMap = new Map<id,pse__Resource_Request__c>();
        List<Apttus__APTS_Agreement__c> updatedAgreementList= new List<Apttus__APTS_Agreement__c>();
        Map<Id, Apttus__APTS_Agreement__c> agreementIdToAgreementMap= new Map<Id, Apttus__APTS_Agreement__c>();
        pse__Assignment__c assignmnetOld = new pse__Assignment__c();
     
        for (pse__Assignment__c assignmnetNew : newList) {
            if(assignmnetNew.pse__Resource__c != null)
                resourceIdSet.add(assignmnetNew.pse__Resource__c);
            if(assignmnetNew.Agreement__c != null)
                agreementIdSet.add(assignmnetNew.Agreement__c);
            if(assignmnetNew.pse__Resource_Request__c != null)
                resourceRequestIdSet.add(assignmnetNew.pse__Resource_Request__c);
        } 
        
        
        if(!resourceRequestIdSet.isEmpty()){
            String resourceRequestCondition = 'id in : resourceRequestIdSet ';
            Set<String> resourceRequestFieldSet = new Set<String>{'Id', 'SubGroup__c'};
                rrIdToResourceRequestMap = new SLT_Resource_Request(false,false).selectResReqsById(resourceRequestIdSet, resourceRequestFieldSet);       
        }
        
        if(!resourceIdSet.isEmpty()){
            String resourceCondition = 'id in : resourceIdSet ';
            Set<String> resourceFieldSet = new Set<String>{'Id', 'Salesforce_User__c', 'pse__Salesforce_User__c'};
                contactList = new SLT_Contact().selectByContactIdList(resourceIdSet, resourceFieldSet);       
        }
        
        if(!agreementIdSet.isEmpty()){
            String agreementCondition = 'id in : agreementIdSet ';
            Set<String> agreementFieldSet = new Set<String>{'Id', 'OwnerId'};
                agreementIdToAgreementMap = new SLT_APTS_Agreement(false,false).getAgreementsById(agreementIdSet,agreementFieldSet);       
        }
        
        for (Contact resource : contactList) {
            if(resource.pse__Salesforce_User__c != null)
                resourceIdToUserIdMap.put(resource.id,resource.pse__Salesforce_User__c);
        }   
        
        for (pse__Assignment__c assignmnetNew : newList) { 
            
            if(oldMap != null && oldMap.containskey(assignmnetNew.Id))
                assignmnetOld = oldMap.get(assignmnetNew.Id); 
            
            if(resourceIdToUserIdMap != null && resourceIdToUserIdMap.containsKey(assignmnetNew.pse__Resource__c) &&
               resourceIdToUserIdMap.get(assignmnetNew.pse__Resource__c) != null && assignmnetNew.pse__Status__c == 'Accepted' && 
               rrIdToResourceRequestMap.containsKey(assignmnetNew.pse__Resource_Request__c) && 
               rrIdToResourceRequestMap.get(assignmnetNew.pse__Resource_Request__c).SubGroup__c != null && 
               rrIdToResourceRequestMap.get(assignmnetNew.pse__Resource_Request__c).SubGroup__c == CON_OWF.RES_REQ_TYPE_GBO_LEAD_PD && 
               (
                   (Trigger.isInsert && assignmnetNew.pse__Resource__c != null ) || 
                   ( Trigger.isUpdate  && 
                    (assignmnetNew.pse__Resource__c != assignmnetOld.pse__Resource__c || 
                     (agreementIdToAgreementMap.containsKey(assignmnetNew.Agreement__c) && agreementIdToAgreementMap.get(assignmnetNew.Agreement__c) != null 
                      && agreementIdToAgreementMap.get(assignmnetNew.Agreement__c).OwnerId != resourceIdToUserIdMap.get(assignmnetNew.pse__Resource__c)
                     )
                    )
                   )
               )
              ){
                  
                  Apttus__APTS_Agreement__c agreement = agreementIdToAgreementMap.get(assignmnetNew.Agreement__c);
                  agreement.OwnerId = resourceIdToUserIdMap.get(assignmnetNew.pse__Resource__c);
                  updatedAgreementList.add(agreement);  
                  
              }
        }

        try{     
            if(!updatedAgreementList.isEmpty()){
                update updatedAgreementList;
            }    
        }catch(Exception e){
            system.debug('Error: ' + e.getMessage() + ' at Line number: ' + e.getLineNumber());
        }
    }
    
    /**
    * This method is used to update the accepted date based on status
    * @params  newList List<Apttus__APTS_Agreement__c>
    * @params  oldMap Map<Id, Apttus__APTS_Agreement__c>
    * @return  void
    */
    /*public static void updateAcceptedDateWhenStatusIsAccepted(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap) {
        Id owfAssignmentRecordTypeId = Schema.SObjectType.pse__Assignment__c.getRecordTypeInfosByName().get('OWF Assignment').getRecordTypeId();
        for(pse__Assignment__c assignment : newList){
            if(assignment.pse__Status__c == 'Accepted' && assignment.recordTypeId == owfAssignmentRecordTypeId){
                if(oldMap == null || ( oldMap != null && assignment.pse__Status__c != oldMap.get(assignment.Id).pse__Status__c)){
                    assignment.Accepted_Date__c = System.today();
                }
            }
        }
    }*/
    
    /**
    * This method is used to update the accepted date, rejected date, match date fields based on status change and status based on Resource change.
    * @params  newList List<Apttus__APTS_Agreement__c>
    * @params  oldMap Map<Id, Apttus__APTS_Agreement__c>
    * @return  void
    */
    public static void updateFieldsWhenStatusAndResourceChanged(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap) {
        Id owfAssignmentRecordTypeId = Schema.SObjectType.pse__Assignment__c.getRecordTypeInfosByName().get('OWF Assignment').getRecordTypeId();
        Set<Id> assignmentIdSet = new Set<Id>();
        for(pse__Assignment__c assignment : newList){
            if(assignment.recordTypeId == owfAssignmentRecordTypeId) {
                if(oldMap == null || (oldMap != null && assignment.pse__Status__c != oldMap.get(assignment.Id).pse__Status__c)){
                    if(assignment.pse__Status__c == CON_OWF.OWF_STATUS_ACCEPTED){
                        assignment.Accepted_Date__c = System.today();
                    }
                    else if(assignment.pse__Status__c == CON_OWF.OWF_STATUS_REJECTED){
                        assignment.Rejected_Date__c = System.today();
                    }
                }
                if(assignment.pse__Resource__c != null && (oldMap == null || (oldMap != null && assignment.pse__Resource__c != oldMap.get(assignment.Id).pse__Resource__c))) {
                    assignment.Match_Date__c = System.today();
                    if(assignment.Assignment_Type__c != 'Days Off') {
                        assignment.pse__Status__c = CON_OWF.OWF_STATUS_PENDING;
                    }
                }
                if(assignment.pse__Resource_Request__c != null && assignment.Agreement__c == null ) {
                    assignmentIdSet.add(assignment.id);
                }
            }
        }
		if(assignmentIdSet.size() > 0) {
			Set<String> assignmentFieldSet = new Set<String>{'Id','pse__Resource_Request__r.Agreement__c','pse__Resource_Request__c'};
			String assignCondition = 'Id in: sObjectIdset';
			Map<Id, pse__Assignment__c> assignmentMap = new SLT_Assignment(false,false).selectAssignmentsByIdSet(assignmentIdSet,assignCondition,assignmentFieldSet);
			for(pse__Assignment__c assignment : newList){
				if(assignment.recordTypeId == owfAssignmentRecordTypeId && assignmentMap.containsKey(assignment.id)) {
					assignment.Agreement__c = assignmentMap.get(assignment.id).pse__Resource_Request__r.Agreement__c;
				}
			}           
		
		}
        for(pse__Assignment__c assignment : newList){
            if(assignment.recordTypeId == owfAssignmentRecordTypeId) {
                if(oldMap == null || (oldMap != null && assignment.pse__Status__c != oldMap.get(assignment.Id).pse__Status__c)){
                    if(assignment.pse__Status__c == CON_OWF.OWF_STATUS_REJECTED || assignment.pse__Status__c == CON_OWF.OWF_STATUS_CANCELLED)
                    {
                        assignment.Former_Resource_Request__c = assignment.pse__Resource_Request__c;
                        assignment.pse__Resource_Request__c = null;
                    }
                }
            }
        }
    }
    
     /**
    * This method is used to update status of Resource Request on creation of Assignment.
    * @params  newList List<Apttus__APTS_Agreement__c>
    * @params  oldMap Map<Id, Apttus__APTS_Agreement__c>
    * @return  void
    */
    public static void updateRrStatus(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap) {
        Map<Id,pse__Assignment__c> rrIdToAssignmentMap = new Map<Id,pse__Assignment__c>();
        Set<String> rrFieldSet = new Set<String>{'Id', 'pse__Status__c','pse__Assignment__c','pse__resource__c','pse__Staffer_Resource__c'};
        List<pse__Resource_Request__c> rrList = new List<pse__Resource_Request__c>();
        for(pse__Assignment__c assignment : newList){
            if(assignment.RecordTypeId == CON_OWF.OWF_ASSIGNMENT_RECORD_TYPE_ID) {
                rrIdToAssignmentMap.put(assignment.pse__Resource_Request__c,assignment);
            }            
        }
        if(!rrIdToAssignmentMap.isEmpty()) {
            for(pse__Resource_Request__c rrToBeUpdated: new SLT_Resource_Request(false,false).selectResReqsById(rrIdToAssignmentMap.keySet(),rrFieldSet).values()) {
                rrToBeUpdated.pse__Status__c = CON_OWF.OWF_STATUS_ASSIGNED;
                rrToBeUpdated.pse__Resource__c = rrIdToAssignmentMap.get(rrToBeUpdated.Id).pse__resource__c;
                rrToBeUpdated.pse__Staffer_Resource__c = rrIdToAssignmentMap.get(rrToBeUpdated.Id).pse__resource__c;
                rrToBeUpdated.pse__Assignment__c = rrIdToAssignmentMap.get(rrToBeUpdated.Id).Id;
                rrList.add(rrToBeUpdated);                                                                    
            }    
        }
        if(rrList.size() > 0) {
            update rrList;
        }        
    }
    
    /**
    * This method is used to update hours of schedule on creation of Assignment.
    * @params  newList List<pse__Assignment__c>
    * @params  oldMap Map<Id, pse__Assignment__c>
    * @return  void
    */
    public static void updateScheduleHours(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap) {
        List<pse__Schedule__c> scheduleList = new List<pse__Schedule__c>();
        Set<String> scheduleFieldSet = new Set<String>{'Id','pse__Monday_Hours__c','pse__Tuesday_Hours__c','pse__Wednesday_Hours__c',
            'pse__Thursday_Hours__c','pse__Friday_Hours__c','pse__Saturday_Hours__c','pse__Sunday_Hours__c'};
        Map<Id, pse__Assignment__c> scheduleIdToAssignmentMap = new Map<Id,pse__Assignment__c>();
        for(pse__Assignment__c assignment : newList)
        {
            scheduleIdToAssignmentMap.put(assignment.pse__Schedule__c,assignment);
        }
        if(!scheduleIdToAssignmentMap.isEmpty())
        {
            for(pse__Schedule__c scheduleToUpdate : new SLT_Schedule(false).selectScheduleById(scheduleIdToAssignmentMap.keySet(),scheduleFieldSet).values())
            {
                if(scheduleToUpdate.pse__Monday_Hours__c != 0 || scheduleToUpdate.pse__Tuesday_Hours__c != 0 || scheduleToUpdate.pse__Wednesday_Hours__c != 0
                   || scheduleToUpdate.pse__Thursday_Hours__c != 0 || scheduleToUpdate.pse__Friday_Hours__c != 0 || scheduleToUpdate.pse__Saturday_Hours__c != 0
                   || scheduleToUpdate.pse__Sunday_Hours__c != 0)
                {
                    scheduleToUpdate.pse__Monday_Hours__c = 0;
                    scheduleToUpdate.pse__Tuesday_Hours__c = 0;
                    scheduleToUpdate.pse__Wednesday_Hours__c = 0;
                    scheduleToUpdate.pse__Thursday_Hours__c = 0;
                    scheduleToUpdate.pse__Friday_Hours__c = 0;
                    scheduleToUpdate.pse__Saturday_Hours__c = 0;
                    scheduleToUpdate.pse__Sunday_Hours__c = 0;
                    scheduleList.add(scheduleToUpdate);
                }
            }
        }
        if(scheduleList.size() > 0)
        {
            update scheduleList;
        }
    }
    
    
    
     /**
    * This method is used to update Resource on Resource Request when Assignment is Rejected.
    * @params  newList List<Apttus__APTS_Agreement__c>
    * @params  oldMap Map<Id, Apttus__APTS_Agreement__c>
    * @return  void
    */
    public static void updateRelatedFieldsOnRejected(List<pse__Assignment__c> newList, Map<Id, pse__Assignment__c> oldMap) {
        Map<Id,pse__Assignment__c> assignmentIdToAssignmentMap = new Map<Id,pse__Assignment__c>();
        Set<String> rrFieldSet = new Set<String>{'Id', 'pse__Status__c','pse__Assignment__c','pse__Resource__c','pse__Staffer_Resource__c','pse__Start_Date__c','pse__End_Date__c'};
        List<pse__Resource_Request__c> rrList = new List<pse__Resource_Request__c>();
        List<pse__Schedule__c> scheduleList = new List<pse__Schedule__c>();

        
        for(pse__Assignment__c assignment : newList){
            if(assignment.RecordTypeId == CON_OWF.OWF_ASSIGNMENT_RECORD_TYPE_ID) {
                assignmentIdToAssignmentMap.put(assignment.Id,assignment);
            }            
        }
        if(!assignmentIdToAssignmentMap.isEmpty()) {
            for(pse__Resource_Request__c rrToBeUpdated: new SLT_Resource_Request(false,false).getResourceRequestByAssignmentID(assignmentIdToAssignmentMap.keySet(),rrFieldSet).values()) {
                
                pse__Assignment__c assignment = assignmentIdToAssignmentMap.get(rrToBeUpdated.pse__Assignment__c);
                if(assignment.pse__Status__c != oldMap.get(assignment.Id).pse__Status__c)
                {
                    if(assignment.pse__Status__c == CON_OWF.OWF_STATUS_REJECTED || assignment.pse__Status__c == CON_OWF.OWF_STATUS_CANCELLED)
                    {
                                              
                        rrToBeUpdated.pse__Status__c = CON_OWF.OWF_STATUS_READYTOSTAFF;
                        rrToBeUpdated.pse__Resource__c = null;
                        rrToBeUpdated.pse__Assignment__c = null;
                        rrToBeUpdated.pse__Staffer_Resource__c = null;
                        rrList.add(rrToBeUpdated);  
                    }
                    else
                    {
                        if(rrToBeUpdated.pse__Resource__c == null || rrToBeUpdated.pse__Staffer_Resource__c == null)
                        {
                            rrToBeUpdated.pse__Resource__c = assignment.pse__Resource__c; 
                            rrToBeUpdated.pse__Staffer_Resource__c = assignment.pse__Resource__c;
                            rrList.add(rrToBeUpdated);
                        }
                    }
                }
                if(assignment.pse__End_Date__c != rrToBeUpdated.pse__End_Date__c)
                {
                    pse__Schedule__c scheduleToUpdate = new pse__schedule__c(Id = assignment.pse__Schedule__c,
                                                                            pse__Start_Date__c = rrToBeUpdated.pse__Start_Date__c,
                                                                            pse__End_Date__c = rrToBeUpdated.pse__End_Date__c 
                                                                           );
                    scheduleList.add(scheduleToUpdate);
                }
            }  
            
          
            
            
            
        }
        if(rrList.size() > 0) {
            update rrList;
        }
        if(scheduleList.size() > 0)
        {
            update scheduleList;
        }
        
       
    }
}