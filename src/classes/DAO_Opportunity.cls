/**
 * This is Opportunity trigger handler class.
 * version : 1.0
 */
public class DAO_Opportunity extends fflib_SObjectDomain {

    /**
     * Constructor of this class
     * @params sObjectList List<Opportunity>
     */
    public DAO_Opportunity(List<Opportunity> sObjectList) {
        super(sObjectList);
    }

    /**
     * Constructor Class for construct new Instance of This Class
     */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new DAO_Opportunity(sObjectList);
        }
    }

    /**
     * This method is used for before insert of the opportunity trigger.
     * @return void
     */
    public override void onBeforeInsert() {
        //This is the section where all the methods that needs to be run at first are included.
        //This should be at the top since the trigger execution flags are reset in this method
        DAOH_Opportunity.resetTriggerExecutionFlag((List<Opportunity>)Records);
        
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        DAOH_Opportunity.validateOpportunity((List<Opportunity>)records);
        DAOH_Opportunity.setTimeStampBasedOnStage((List<Opportunity>)Records, null);
        DAOH_Opportunity.setStandardPriceBookId((List<Opportunity>)records);
        DAOH_Opportunity.setClonedOpportunitiesNumerBlank((List<Opportunity>)records);
        DAOH_Opportunity.setOppFields((List<Opportunity>)records, null);
        DAOH_Opportunity.setBidTeamSplit((List<Opportunity>)Records, null);
        
        //This is the section for OWF where all the methods that needs to be run in a normal sequence are included.
        //This Section is enabled to run based on the release version
        if(!UTL_ExecutionControl.stopTriggerExecution_OWF && Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_OWF_OPP_Flow__c) {
          
              DAOH_OWF_Opportunity.setNoOfPotentialRegionsBasedOnPotentialRegions((List<Opportunity>)Records, null);  
          
        }
        
        //This is the section where all the methods that needs to be run at last are included.
        //This should be at the last since it will integrate the opp with the latest changes
        DAOH_Opportunity.setMuleSoftSyncMechanism((List<Opportunity>)records, null);      
        DAOH_Opportunity.updateConversionRate((List<Opportunity>)records);		
    }
    
    /**
     * This method is used for before update of the opportunity trigger.
     * @params  existingRecords Map<Id,SObject>
     * @return  void
     */
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        //This is the section where all the methods that needs to be run at first are included.
        //This should be at the top since the trigger execution flags are reset in this method
        DAOH_Opportunity.addLIGError((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords);
        DAOH_Opportunity.resetTriggerExecutionFlag((List<Opportunity>)Records);
        
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        DAOH_Opportunity.setTimeStampBasedOnStage((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords);
        DAOH_Opportunity.validateOpportunitiesBasedOnStage(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
        DAOH_Opportunity.resetLegacyAccountId(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
        if(!CON_CRM.IS_CLONE_OPPORTUNITY_FLOW && !CON_CRM.IS_CURRENCY_CHANGE_FLOW) {
            DAOH_Opportunity.validationForProxyBNFApproval(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
            DAOH_Opportunity.validationOnOppForProxyPA(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
            DAOH_Opportunity.populateProbabiltyOnOpportunity(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
            DAOH_Opportunity.validationForProxySCMAgreement(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
            DAOH_Opportunity.processProjectAwardedAndSignedPrice((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords, true);
        }
        DAOH_Opportunity.setOppFields((List<Opportunity>)records, (Map<Id, Opportunity>)existingRecords);
        DAOH_Opportunity.excludePACheck(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
        DAOH_Opportunity.setBidTeamSplit((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords);
        DAOH_Opportunity.hasLQMaterialProducts(new Map<Id, Opportunity>((List<Opportunity>)Records));
        
        //This is the section for OWF where all the methods that needs to be run in a normal sequence are included.
        //This Section is enabled to run based on release version
        if(!UTL_ExecutionControl.stopTriggerExecution_OWF && Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_OWF_OPP_Flow__c) {
            
            DAOH_OWF_Opportunity.setNoOfPotentialRegionsBasedOnPotentialRegions((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords);
            DAOH_OWF_Opportunity.BDLeadSubRegionRequired((List<Opportunity>)Records);
        }
        
        //This is the section where all the methods that needs to be run at last are included.
        //This should be at the last since it will integrate the opp with the latest changes
        DAOH_Opportunity.setMuleSoftSyncMechanism((List<Opportunity>)records, (Map<Id, Opportunity>)existingRecords);  
        DAOH_Opportunity.updateConversionRate((List<Opportunity>)records);			
        
    }

    /**
     * Override method After Update Call
     */
    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {
        //This is the section where all the methods that needs to be run at first are included.
        
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        //DAOH_Opportunity.createTeamMemberForPICUser(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
        DAOH_Opportunity.createLineItemGroup(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
        DAOH_Opportunity.submitUnvalidatedAccounts((List<Opportunity>)Records);
        
        //This is the section for OWF where all the methods that needs to be run in a normal sequence are included.
        //This section is enabled to run based on release version
        if((!UTL_ExecutionControl.stopTriggerExecution_OWF && Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_RR_Trigger__c) && UTL_OWF.isOPPhasOWFAgreement(trigger.newMap.keySet())) {
            
            DAOH_OWF_Opportunity.createClinicalBidResRequestsOnOppUpdate((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords);
            DAOH_OWF_Opportunity.updateComplexityScoreTotalOnRR((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords);
            
        }
        
        //This is the section where all the methods that needs to be run at last are included.
        //This should be at the last since it will set the sync fields with the latest changes
        DAOH_Opportunity.setMuleSoftSyncFields((List<Opportunity>)records, (Map<Id, Opportunity>)existingRecords);     
        if(!CON_CRM.IS_CLONE_OPPORTUNITY_FLOW && !CON_CRM.IS_CURRENCY_CHANGE_FLOW) {    
            DAOH_Opportunity.processProjectAwardedAndSignedPrice((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords, false);
        }

        DAOH_Opportunity.updateLeadStatusOnClose((List<Opportunity>)Records, (Map<Id, Opportunity>)existingRecords);  
    }

    /**
     * Override method After Insert Call
     */
    public override void onAfterInsert() {
        //This is the section where all the methods that needs to be run at first are included.
        DAOH_Opportunity.setOpportunityNumber((List<Opportunity>)Records);
        
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        //DAOH_Opportunity.createTeamMemberForPICUser(new Map<Id, Opportunity>((List<Opportunity>)Records), null); 
        DAOH_Opportunity.createLineItemGroup(new Map<Id, Opportunity>((List<Opportunity>)Records), null); 
        DAOH_Opportunity.submitUnvalidatedAccounts((List<Opportunity>)Records);
        //This is the section where all the methods that needs to be run at last are included.
        //This should be at the last since it will set the sync fields with the latest changes
        DAOH_Opportunity.setMuleSoftSyncFields((List<Opportunity>)records, null);        
    } 
    
    /**
     * Override method Before Delete Call
     */
    public override void onBeforeDelete() {
        //This is the section where all the methods that needs to be run at first are included.
        
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        DAOH_Opportunity.createOutboundMessageDeletionQueue((List<Opportunity>)Records);
        
        //This is the section for OWF where all the methods that needs to be run in a normal sequence are included.
        //This section is enabled to run based on release version
        if(!UTL_ExecutionControl.stopTriggerExecution_OWF && Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_OWF_OPP_Flow__c){
            
                DAOH_OWF_Opportunity.deleteAgreementBasedOnOpportunity((List<Opportunity>)Records);
        }
        
        //This is the section where all the methods that needs to be run at last are included.
    } 
}