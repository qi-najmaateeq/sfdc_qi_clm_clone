/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Account
 */
public class SLT_Account extends fflib_SObjectSelector {
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Account.Id,
            Account.Name,
            Account.ParentId,
            Account.AccountCountry__c,
            Account.CSH_SubType__c,
            Account.Industry,
            Account.Shared_Folder_Write_Permission__c,
            Account.Shared_Folder_Type__c
        };
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Account.sObjectType;
    }
    
    /**
     * This method used to get Account by Id
     * @return  List<Account>
     */
    public List<Account> selectById(Set<ID> idSet) {
        return (List<Account>) selectSObjectsById(idSet);
    }
    
    /**
     * This method used to get Account by Id
     * @return  Map<Id, Account>
     */
    public Map<Id, Account> selectByAccountId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Account>((List<Account>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    /**
     * This method used to get Accounts
     * @return  Map<Id, Account>
     */
    public Map<Id, Account> getAccounts(Set<String> fieldSet, String whereCondition) {
        return new Map<Id, Account>((List<Account>) Database.query(newQueryFactory(false).selectFields(fieldSet).setCondition(whereCondition).toSOQL()));
    }
    
    public List<Account> selectAllCustomerPortalAccounts(String accCountry,Set<String> fieldSet) {
        return Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('AccountCountry__c =:accCountry and IsCustomerPortal = true').toSOQL());
    }
        
}