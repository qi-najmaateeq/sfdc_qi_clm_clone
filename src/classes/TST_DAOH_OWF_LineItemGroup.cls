/**
 * This test class is used to test all methods in LineItemGroup trigger.
 * version : 1.0
 */
@isTest(seeAllData=false)
private class TST_DAOH_OWF_LineItemGroup {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Indication_List__c indication = UTL_OWF_TestData.createIndication('Test Indication', 'Acute Care');
        insert indication;
        Line_Item_Group__c lineItemGroup = UTL_OWF_TestData.createLineItemGroup(indication.Id);
        insert lineItemGroup;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        /*
        pse__Proj__c bidProject = UTL_OWF_TestData.createBidProject(grp.Id);
        insert bidProject;
        */
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        opp.Potential_Regions__c = 'Japan';
        opp.Line_of_Business__c = 'Core Clinical';
        opp.Line_Item_Group__c = lineItemGroup.Id;
        insert opp;
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        insert agreement;
    }
    
    /**
     * This test method used for update Phase field on LineItemGroup records
     */ 
    static testMethod void testCreateClinicalBidResourceRequestsOnLineItemGroupUpdate() {
        Line_Item_Group__c lineItemGroup = [SELECT id , Phase__c FROM Line_Item_Group__c limit 1];
        
        Test.startTest();
            lineItemGroup.Phase__c = 'Phase 1';
            update lineItemGroup;
        Test.stopTest();
        system.assert([Select id from pse__Resource_Request__c].size() > 0);
    }
}