public class CNT_CPQ_BoxIntegration {

    public String getBoxName(){

        String agreementBoxName = '';
        String currentRecordId  = ApexPages.CurrentPage().getParameters().get(CON_CPQ.ID);
        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_BID_NUMBER, CON_CPQ.AGREEMENT_OPPORTUNITY_ACCOUNT_NAME,
            CON_CPQ.AGREEMENT_NEW_BID_HISTORY_NUMBER, CON_CPQ.AGREEMENT_LEGACY_QUNTILES_OPPORTUNITY_NUMBER};        
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{currentRecordId}, fieldSet); 
        if(agreementList.size() > 0){
            agreementBoxName = agreementList[0].Apttus__Related_Opportunity__r.Account.Name + ' ' + agreementList[0].Apttus__Related_Opportunity__r.Legacy_Quintiles_Opportunity_Number__c + ' Bid ' + agreementList[0].Bid_Number__c + ': ' + agreementList[0].Bid_History_Number__c;
        }
        return agreementBoxName;
    }
}