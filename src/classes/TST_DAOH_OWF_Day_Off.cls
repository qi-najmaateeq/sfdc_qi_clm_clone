/**
* This test class is used to test all methods in DAOH_OWF_Day_Off Class.
* version : 1.0
*/
@isTest
private class TST_DAOH_OWF_Day_Off {
    /**
    * This method used to set up testdata
    */ 
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
    }
    
    /**
    * This test method used to test insertion of Assignment & Schedule through upsertRelatedAssignmentAndSchedule method
    */
    static testmethod void testUpsertRelatedAssignmentAndSchedule1() {
        Contact cont = [Select Id from Contact Limit 1];
        Days_Off__c daysOff = UTL_OWF_TestData.createDaysOff(cont.Id);
        Test.startTest();
        	insert daysOff;
        Test.stopTest();
        Integer expected = 1;
        Integer actual = [Select Id From pse__Assignment__c].size();
        system.assertEquals(expected, actual);
        actual = [Select Id From pse__Schedule__c].size();
        system.assertEquals(expected, actual);
    }
    
    /**
    * This test method used to test updation of Assignment & Schedule through upsertRelatedAssignmentAndSchedule method, 
    */
    static testmethod void testUpsertRelatedAssignmentAndSchedule2() {
        Contact cont = [Select Id from Contact Limit 1];
        Days_Off__c daysOff = UTL_OWF_TestData.createDaysOff(cont.Id);
        Test.startTest();
            insert daysOff;
            daysOff.First_Day_Off__c = System.today().addDays(1);
            update daysOff;
        Test.stopTest();
        Date expected = System.today().addDays(1);
        Date actual = [Select Id,pse__Start_Date__c From pse__Schedule__c limit 1].pse__Start_Date__c;
        system.assertEquals(expected, actual);
    }
    
    /**
    * This test method used to test deletion of Assignment & Schedule through upsertRelatedAssignmentAndSchedule method, 
    */
    static testmethod void testUpsertRelatedAssignmentAndSchedule3() {
        Contact cont = [Select Id from Contact Limit 1];
        Days_Off__c daysOff = UTL_OWF_TestData.createDaysOff(cont.Id);
        Test.startTest();
            insert daysOff;
            delete daysOff;
        Test.stopTest();
        Integer expected = 0;
        Integer actual = [Select Id From pse__Assignment__c].size();
        system.assertEquals(expected, actual);
        actual = [Select Id From pse__Schedule__c].size();
        system.assertEquals(expected, actual);
    }
}