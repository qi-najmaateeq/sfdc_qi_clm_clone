/*
 * Version       : 1.0
 * Description   : Test Class for CNT_CSM_CaseCategorization
 */
@isTest
private class TST_CNT_CSM_CaseCategorization {
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        Product2 product = UTL_TestData.createProduct();
        insert product;
        Account account = UTL_TestData.createAccount();
        insert account;
        Asset asset = new Asset(Name = 'TestAsset', AccountId = account.Id, Product2Id = product.id);
        insert asset;
        Contact contact = UTL_TestData.createContact(account.Id);
        contact.Contact_User_Type__c = 'HO User';
        insert contact;

        Id recordType =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
       
        CSM_QI_Case_Categorization__c categorization = new CSM_QI_Case_Categorization__c(Product__c = product.Id, SubType1__c='TestSubtype1', SubType2__c='TestSubtype2 1',SubType3__c='TestSubtype3 1',RecordTypeId__c=recordType);
        insert categorization;
        categorization = new CSM_QI_Case_Categorization__c(Product__c = product.Id, SubType1__c='TestSubtype1', SubType2__c='TestSubtype2 2',SubType3__c='TestSubtype3 2',RecordTypeId__c=recordType);
        insert categorization;
        
        categorization = new CSM_QI_Case_Categorization__c(Los__c ='Los' ,SubType1__c='TestSubtype1', SubType2__c='TestSubtype22',SubType3__c='TestSubtype33',RecordTypeId__c='0126A000000hC34QAE');
        insert categorization;
        
        Queue_User_Relationship__c queues=new Queue_User_Relationship__c();
        queues.Name ='Q1';
        queues.QueueName__c ='Q1';
        queues.Type__c ='Queue';
        queues.User__c = UserInfo.getUserId();
        insert queues;
        Queue_User_Relationship__c queueUser=new Queue_User_Relationship__c();
        queueUser.Name ='Q1';
        queueUser.QueueName__c ='Q1';
        queueUser.Type__c ='User';
        queueUser.User__c = UserInfo.getUserId();
        insert queueUser;
        Case c = New Case(Subject = 'TestCase',RecordTypeId=recordType, ContactId = contact.Id, AccountId = account.Id, Status = 'New', Priority = 'Medium', Origin = 'Email',CurrentQueue__c=queues.Id,InitialQueue__c = 'Q1');
        c.Case_Type__c = 'Problem';
        insert c;
        Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle', language = 'en_US',UrlName='TestUrlName');
        insert knowledge;
         knowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledge.Id];
        CaseArticle caseArticle = new CaseArticle(CaseId = c.Id, KnowledgeArticleId=knowledge.KnowledgeArticleId);
        insert caseArticle;
        CNT_CSM_CaseCategorization.getTimeSheetRecord(c.Id);
    }
    
     /**
     * This method used to get Case by recordId
     */    
    @IsTest
    static void testGetCaseRecord() {
        List<SObject> objects = new List<SObject>();  
        String objName ='Case';
        Case c = [SELECT id FROM Case WHERE Subject='TestCase'];
        String caseId = c.Id;
        Test.startTest();
        objects = CNT_CSM_CaseCategorization.getRecord(objName,caseId);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = objects.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get List<CaseArticle> by caseId
     */    
    @IsTest
    static void testGetCaseArticles() {
        List<CaseArticle> caseArticles = new List<CaseArticle>();  
        String objName ='Case';
        Case c = [SELECT id FROM Case WHERE Subject='TestCase'];
        String caseId = c.Id;
        Test.startTest();
        caseArticles = CNT_CSM_CaseCategorization.getCaseArticles(caseId);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = caseArticles.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get Knowledge by recordId
     */    
    @IsTest
    static void testGetKnowledgeRecord() {
        List<SObject> objects = new List<SObject>();  
        String objName ='Knowledge__kav';
        Knowledge__kav knowledge = [SELECT id FROM Knowledge__kav WHERE Title = 'TestTitle' and PublishStatus='Draft' and language='en_US'];
        Test.startTest();
        objects = CNT_CSM_CaseCategorization.getRecord(objName,knowledge.Id);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = objects.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get List<AggregateResult> of  Product__r.Name Categorization by given CaseId
     */    
    @IsTest
    static void testGetProductCategorizationForCase() {
        List<AggregateResult> products = new  List<AggregateResult>();
        Case c = [SELECT id FROM Case WHERE Subject='TestCase'];
        Test.startTest();
        products = CNT_CSM_CaseCategorization.getProductCategorizationForCase(c.Id);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = products.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get List<AggregateResult> of  Product__r.Name
     */    
    @IsTest
    static void testGetProductCategorization() {
        List<AggregateResult> products = new  List<AggregateResult>();
        Test.startTest();
        products = CNT_CSM_CaseCategorization.getProductCategorization();
        Test.stopTest();
        Integer expected = 2;
        Integer actual = products.size();
        System.assertEquals(expected, actual);
    }

    /**
     * This method used to get List<Product2> by given CaseId
     */    
    @IsTest
    static void testGetProductsCategorizationForData() {
        List<Product2> products = new  List<Product2>();
        Case c = [SELECT id FROM Case WHERE Subject='TestCase'];
        Test.startTest();
        products = CNT_CSM_CaseCategorization.getProductsCategorizationForData(c.Id);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = products.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get List<AggregateResult>
     */    
    @IsTest
    static void testGetQuery() {
        List<AggregateResult> cases = new  List<AggregateResult>();
        Test.startTest();
        cases = CNT_CSM_CaseCategorization.getCategorizationWithAggregate('select Subject from Case group by Subject');
        Test.stopTest();
    }
    
    /**
     * This method used to get cases
     */    
    @IsTest
    static void testGetQueryCases() {
        Test.startTest();
        Product2 p = [Select Id, Name from Product2 LIMIT 1];
        case cases = [select Id,AccountId,ContactId,Case_CategorizationId__c,ProductName__c,SubType1__c,SubType2__c,SubType3__c,Case_Type__c from Case LIMIT 1];
        Id recordType =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        CNT_CSM_CaseCategorization.updateObjectCategorization(cases, p.Name,'TestSubtype1','TestSubtype2 1','','', recordType);
        Test.stopTest();
    }
    
    /**
     * This method used to get cases
     */    
    @IsTest
    static void testGetQueryTechCases() {
        Test.startTest();
        Product2 p = [Select Id, Name from Product2 LIMIT 1];
        case cases = [select Id,AccountId,ContactId,Case_CategorizationId__c,ProductName__c,SubType1__c,SubType2__c,SubType3__c,Case_Type__c from Case LIMIT 1];
        Id recordType =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        CNT_CSM_CaseCategorization.updateObjectCategorization(cases, p.Name,'TestSubtype1','TestSubtype2 2','TestSubtype3 2','', recordType);
        Test.stopTest();
    }
    
    /**
     * This method used to get cases
     */    
    @IsTest
    static void testGetQueryCasesRD() {
        Test.startTest();
        Product2 p = [Select Id, Name from Product2 LIMIT 1];
        case cases = [select Id,AccountId,ContactId,Case_CategorizationId__c,ProductName__c,SubType1__c,SubType2__c,SubType3__c,Case_Type__c from Case LIMIT 1];
        Id recordType =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        CNT_CSM_CaseCategorization.updateObjectCategorization(cases, 'Los','TestSubtype1','TestSubtype22','TestSubtype33','Los', '0126A000000hC34QAE');
        Test.stopTest();
        
    }
    
    @IsTest
    static void testGetQueryKnowledge() {
        Test.startTest();
        Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle123', language = 'en_US',UrlName='TestUrlName123');
        insert knowledge;
        Product2 p = [Select Id, Name from Product2 LIMIT 1];
        Id recordType =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        CNT_CSM_CaseCategorization.updateObjectCategorization(knowledge, p.Name,'TestSubtype1','TestSubtype2 1','TestSubtype3 1','', '');
        Test.stopTest();
        
    }
    
    @IsTest
    static void testGetQueryControlField() {
        Test.startTest();
        CNT_CSM_CaseCategorization.getDependentOptionsImpl('Case' , 'Status' , 'SubStatus__c');
        Test.stopTest();
        
    }
}