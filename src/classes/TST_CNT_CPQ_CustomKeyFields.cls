@isTest
private class TST_CNT_CPQ_CustomKeyFields {

    static Group setGroupData(){
        Group testGroup = UTL_TestData.createGroup(CON_CPQ.CPQ_QC_REVIEWER, 'Regular');
        insert testGroup; 
        return testGroup;
    }

    static GroupMember setGroupMemberData(Id groupId){

        GroupMember testGroupMember = UTL_TestData.createGroupMember(groupId, UserInfo.getUserId());
        Insert testGroupMember; 
        return testGroupMember;
    }

    static Apttus__APTS_Agreement__c getAgreementData(Id OpportuntiyId, String recordTypeName){

        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_FINAL;
        testAgreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        testOpportunity.Opportunity_Type__c = COn_CPQ.OPPORTUNITY_RFP;
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }

    static user setUserData(Boolean isManagerRequire){

        String profileName = 'Sales User';
        User usr = UTL_TestData.createUser(profileName, 1)[0];
        if(isManagerRequire)
        usr.ManagerId = UserInfo.getUserId();
        insert usr;
        return usr;
    }

    @isTest
    static void testCheckIfCurrentUserInQCGroupShouldReturnTrue(){

        Group adminGroup = setGroupData();
        GroupMember adminGroupMember = setGroupMemberData(adminGroup.Id);

        Test.StartTest();
            Boolean isMember = CNT_CPQ_CustomKeyFields.checkIfCurrentUserInQCGroup();
        Test.StopTest();

        System.assertEquals(true, isMember, 'Should Return True');
    }

    @isTest
    static void testCheckIfCurrentUserIsLineManagerShouldReturnTrue(){
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.OwnerId = setUserData(true).Id;
        insert agreement;

        Test.StartTest();
            Boolean isMember = CNT_CPQ_CustomKeyFields.checkIfCurrentUserIsLineManager(agreement.Id);
        Test.StopTest();

        System.assertEquals(true, isMember, 'Should Return True');
    }

    @isTest
    static void testCheckIfCurrentUserIsLineManagerShouldReturnFalse(){
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.OwnerId = setUserData(false).Id;
        insert agreement;

        Test.StartTest();
            Boolean isMember = CNT_CPQ_CustomKeyFields.checkIfCurrentUserIsLineManager(agreement.Id);
        Test.StopTest();

        System.assertEquals(false, isMember, 'Should Return False');
    }

    @isTest
        static void testCheckIfCurrentUserInQCGroupShouldReturnFalse(){

        Group adminGroup = setGroupData();

        Test.StartTest();
            Boolean isMember = CNT_CPQ_CustomKeyFields.checkIfCurrentUserInQCGroup();
        Test.StopTest();

        System.assertEquals(false, isMember, 'Should Return False');
    }
}