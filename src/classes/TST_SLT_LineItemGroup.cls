@isTest
public class TST_SLT_LineItemGroup {
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Contact cnt = UTL_TestData.createContact(acc.Id);
        insert cnt;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        opp.Opportunity_Number__c = '124';
        insert opp;
        Line_Item_Group__c lineItemGroup = new Line_Item_Group__c();
        lineItemGroup.Opportunity__c = opp.Id;
        insert lineItemGroup;
    }
    @IsTest
    static void testSelectById() {
        Line_Item_Group__c lig = [SELECT id,Opportunity__c FROM Line_Item_Group__c limit 1];
        Set<String> oppFieldSet = new Set<String> {'Id'};
        Test.startTest();
        List<Line_Item_Group__c> ligList = new SLT_LineItemGroup().selectByOpportunityId(new Set<Id> { lig.Opportunity__c }, oppFieldSet);
        Map<Id, Line_Item_Group__c> ligMap = new SLT_LineItemGroup().selectById(new Set<Id> { lig.Opportunity__c }, oppFieldSet);
        new SLT_LineItemGroup(true,true);
        Test.stopTest();
    }
}