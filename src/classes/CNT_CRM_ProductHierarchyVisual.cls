/**
* Version : 1.0
* An apex controller that exposes the OpportunitySplit Component.
*/
public class CNT_CRM_ProductHierarchyVisual {
    
    /**
     * This method returns Product Records.
     * @params  String currentChain
     * @return List<Product2>
     */
    @AuraEnabled
    public static List<Product2> getProductDetails(String currentChain) {
        List<Product2> productList = new List<Product2>();
        try {
            Set<String> fieldSet = new Set<String>{'id','name','ProductCode','Hierarchy_Chain__c','Offering_Group__c','Territory__c','Offering_Segment__c','Offering_Type__c','COE_Name__c','Product_Group__c','Unit_Name__c'};
            String filterCondition = CON_CRM.INTERFACEDWITHMDM_TRUE + CON_CRM.AND_LOGIC;
            if(String.isBlank(currentChain)) {
                filterCondition += '(Hierarchy_Chain__c LIKE \'%\') AND (NOT Hierarchy_Chain__c LIKE \'%->%\') AND isActive = true';
                productList = SRV_CRM_Product.getProductsWithFilter(fieldSet, filterCondition);
            } else {
                filterCondition += '(Hierarchy_Chain__c LIKE \'' + currentChain + '%\') AND (NOT Hierarchy_Chain__c LIKE \'' + currentChain + '%->%\') AND isActive = true';
                productList = SRV_CRM_Product.getProductsWithFilter(fieldSet, filterCondition);
            }
        } catch(Exception ex) {
            throw new AuraHandledException(JSON.serialize(ex));
        }
        return productList;
    }
}