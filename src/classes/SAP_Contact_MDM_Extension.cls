public class SAP_Contact_MDM_Extension 
{
        public Address__c Address {get; private set;}
        public SAP_Contact__c SAP_Contact {get; set;}
        private ApexPages.StandardController StandardController;
        public Boolean RequestSubmitted {get; private set;}
        public Boolean ShowHeaderAndSidebar {get; private set;}
        public User CurrentUser {get;private set;}
        public boolean LocalLanguageRendered {get; private set;}
        
        public SAP_Contact_MDM_Extension(ApexPages.StandardController controller)
        {
                this.StandardController = controller;
                this.Address = [select Id,Name,SAP_Reference__c,International_Name__c,Country__c from Address__c where Id = :Apexpages.currentPage().getParameters().get('AddressId')];
                this.SAP_Contact = (SAP_Contact__c)StandardController.getRecord();
                this.RequestSubmitted = false;
                this.ShowHeaderAndSidebar = false;
                if (Apexpages.currentPage().getParameters().get('popup') == '0')
                {
                        this.ShowHeaderAndSidebar = true;
                }
                this.CurrentUser = [select Id,User_Country__c from User where Id = :userInfo.getUserId()];
                if (this.Address.Country__c == this.CurrentUser.User_Country__c && this.Address.International_Name__c != null)
                {
                    this.LocalLanguageRendered = true;
                }
                else
                {
                    this.LocalLanguageRendered = false;
                }
        }
        
        public List<SelectOption> getTitles()
        {
            List<SelectOption> Titles = new List<SelectOption>();
            Contact ProxyContact = new Contact();
            Schema.DescribeFieldResult fieldResult = Contact.Salutation.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            Titles.add(new SelectOption('None',''));
            for( Schema.PicklistEntry f : ple)
            {
                Titles.add(new SelectOption(f.getLabel(), f.getValue()));
            } 
            return Titles;      
    }
        
    public PageReference SendNewContactRequestNotificationEmail() 
    {
                try
                {
                        this.SAP_Contact.Address__c = this.Address.Id;
                        upsert this.SAP_Contact;
                        User Requestor = [select Id,Name,Email from User where Id = :UserInfo.getUserId()];
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                 
                        String[] toAddresses = new String[] {MDM_Defines.MdmApprovalEmailAddress};
                mail.setToAddresses(toAddresses);
                mail.setUseSignature(false);
                List<EmailTemplate> emailTemplateList = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(MDM_Defines.Email_Template_DevName_MDM_New_SAP_Contact_Request, new Set<String>{'Id'});
                mail.setTemplateId(emailTemplateList[0].Id);
                //mail.setTemplateId(MDM_Defines.SapContactRequestTemplateId);
                mail.setWhatId(this.SAP_Contact.Id);
                mail.setTargetObjectId(UserInfo.getUserId());
                        mail.setSaveAsActivity(false);
                if(!(!Mulesoft_Integration_Control__c.getInstance().Is_Mulesoft_User__c && Mulesoft_Integration_Control__c.getInstance().Ignore_Validation_Rules__c)){
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'New contact request has been sent to MDM Helpdesk. You will be notified once the contact has been created in SAP.');
            ApexPages.addMessage(myMsg);
            this.RequestSubmitted = true;
            delete this.SAP_Contact;
            return null;
        }
        catch(Exception e) 
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
                        System.debug(e.getMessage());
                        return null;
        }
    }
    
    public pageReference GoBack()
    {
                pageReference retUrl = new pageReference('/' + this.Address.Id);
                retUrl.setRedirect(true);
                return retUrl;
    }
}