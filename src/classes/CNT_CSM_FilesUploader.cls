/*
 * Version       : 1.0
 * Description   : Apex Controller for LXC_CSM_FilesUploader Lightning component 
 */ 
public class CNT_CSM_FilesUploader {
    /**
     * This method used to update visibility in ContentDocumentLink
     * @params String contentDocumentId
     * @params String visibility,
     */ 
    @AuraEnabled
    public static void updateContentDocumentLinkVisibility(String contentDocumentId, String linkedEntityId, String visibility){
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT Id FROM ContentDocumentLink where ContentDocumentId =:contentDocumentId and LinkedEntityId =:linkedEntityId];
        cdl[0].Visibility = visibility;
        update cdl[0];
    }
    
    /**
     * This method used to return a number of attachments by recordId
     * @params String recordId
     */ 
    @AuraEnabled
    public static double countAttachment(String recordId){
        double total =0;
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT Id FROM ContentDocumentLink where LinkedEntityId =:recordId];
        List<Attachment> al = new List<Attachment>();
        al= [SELECT Id FROM Attachment WHERE ParentId IN (
         SELECT Id FROM EmailMessage WHERE ParentId = :recordId)];
        total+=cdl.size() + al.size();
        return total;
    }
    
}