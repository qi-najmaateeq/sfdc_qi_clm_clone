@isTest
public class TST_BCH_CPQ_BatchToUnlockAgreementRecord {

    static Apttus__APTS_Agreement__c getAgreementData(DateTime budgetLockDate, String OpportuntiyId){

        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.Mark_as_Primary__c = true;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.XAE_Lock_Timestamp__c = budgetLockDate;
        testAgreement.Pricing_Tool_Locked__c = true;
        testAgreement.Budget_Checked_Out_By__c = UserInfo.getUserId();
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }
    
    static Apttus__APTS_Agreement__c getAgreement(Id agreementId){
        return [SELECT Notification_Sent_To_Budget_Owner__c, Pricing_Tool_Locked__c FROM 
                Apttus__APTS_Agreement__c WHERE Id =: agreementId];
    }

    @istest
    static void testBatchIsFirstNotificationSendShouldRetunTrue(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(system.now().addHours(-22), testOpportunity.Id);
        insert agreement;
        CPQ_Budget_Unlock_Setting__c unlockSetting = UTL_TestData.createBudgetUnlockSetting();
        insert unlockSetting;

        Test.startTest();
            database.executeBatch(new BCH_CPQ_BatchToUnlockAgreementRecord());
        Test.stopTest();

        Apttus__APTS_Agreement__c agreementRecord = getAgreement(agreement.Id);
        system.assertEquals(CON_CPQ.FIRST_SENT, agreementRecord.Notification_Sent_To_Budget_Owner__c, 'Should send first notification');
    }

    @istest
    static void testBatchIsSecondNotificationSendShouldRetunTrue(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(system.now().addHours(-22), testOpportunity.Id);
        agreement.Notification_Sent_To_Budget_Owner__c = CON_CPQ.FIRST_SENT;
        insert agreement;
        CPQ_Budget_Unlock_Setting__c unlockSetting = UTL_TestData.createBudgetUnlockSetting();
        insert unlockSetting;

        Test.startTest();
            database.executeBatch(new BCH_CPQ_BatchToUnlockAgreementRecord());
        Test.stopTest();

        Apttus__APTS_Agreement__c agreementRecord = getAgreement(agreement.Id);
        system.assertEquals(CON_CPQ.SECOND_SENT, agreementRecord.Notification_Sent_To_Budget_Owner__c, 'Should send second notification');
    }
    
    @istest
    static void testBatchIsThirdNotificationSendShouldRetunTrue(){
        
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(system.now().addHours(-22), testOpportunity.Id);
        agreement.Notification_Sent_To_Budget_Owner__c = CON_CPQ.SECOND_SENT;
        insert agreement;
        CPQ_Budget_Unlock_Setting__c unlockSetting = UTL_TestData.createBudgetUnlockSetting();
        insert unlockSetting;
        
        Test.startTest();
            database.executeBatch(new BCH_CPQ_BatchToUnlockAgreementRecord());
        Test.stopTest();
        
        Apttus__APTS_Agreement__c agreementRecord = getAgreement(agreement.Id);
        system.assertEquals(CON_CPQ.THIRD_SENT, agreementRecord.Notification_Sent_To_Budget_Owner__c, 'Should send last notification');
    }
    
    @istest
    static void testBatchToUnlockRBudgetSendShouldRetunFalse(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(system.now().addHours(-22), testOpportunity.Id);
        agreement.Notification_Sent_To_Budget_Owner__c = CON_CPQ.THIRD_SENT;
        insert agreement;
        CPQ_Budget_Unlock_Setting__c unlockSetting = UTL_TestData.createBudgetUnlockSetting();
        insert unlockSetting;

        Test.startTest();
            database.executeBatch(new BCH_CPQ_BatchToUnlockAgreementRecord());
        Test.stopTest();

        Apttus__APTS_Agreement__c agreementRecord = getAgreement(agreement.Id);
        system.assertEquals(false, agreementRecord.Pricing_Tool_Locked__c, 'Should unlock agreement');
    }
}