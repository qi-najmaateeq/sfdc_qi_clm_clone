@isTest
private class TST_SLT_Task {

    static Apttus__APTS_Agreement__c getAgreementData(Id OpportuntiyId, String recordTypeName){

        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = recordTypeId;
        testAgreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_FINAL;
        testAgreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        testOpportunity.Opportunity_Type__c = COn_CPQ.OPPORTUNITY_RFP;
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }
    
    static Task setTaskData(String whatId){

        Task taskObj = UTL_TestData.createTask('test', whatId, 'open','normal');
        taskObj.OwnerId = UserInfo.getUserId();
        taskObj.Status = 'Not Started';
        insert taskObj;
        return taskObj;
    }
    
    @isTest
	static void testSelectTaskById(){
        
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_FINAL;
        agreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
        insert agreement;
        Task taskObj = setTaskData(agreement.Id);

        Test.startTest();
        	List<Task> taskList = new SLT_Task().selectById(new Set<Id>{taskObj.Id});
        Test.stopTest();

        system.assertEquals(1, taskList.size(),'Should Return Task');
	}
	
    @isTest
	static void testSelectNonCompletedTaskByWhatIdAndOwnerId(){
        
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_FINAL;
        agreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
        agreement.OwnerId = UserInfo.getUserId();
        insert agreement;
        Task taskObj = setTaskData(agreement.Id);
		Set<String> taskFieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.STATUS};
            
        Test.startTest();
        	List<Task> taskList = new SLT_Task().selectNonCompletedTaskByWhatIdAndOwnerId(taskFieldSet, agreement.Id, UserInfo.getUserId());
        Test.stopTest();

        system.assertEquals(1, taskList.size(),'Should Return Task');
	}
}