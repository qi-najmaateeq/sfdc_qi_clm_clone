@isTest
private class TST_DAO_Agreement {

    private static Id createAccountAndOpty() {
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        return newOpportunity.Id;
    }

    @isTest static void testInsertAgreement() {

        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        Test.startTest();
            Apttus__APTS_Agreement__c newAgreement1 = UTL_TestData.createAgreement();
            newAgreement1.Mark_as_Primary__c = true;
            newAgreement1.Apttus__Related_Opportunity__c = newOpportunity.Id;
            newAgreement1.RecordTypeId = RecordTypeId;
            insert newAgreement1;

            Apttus__APTS_Agreement__c newAgreement2 = UTL_TestData.createAgreement();
            newAgreement2.Mark_as_Primary__c = true;
            newAgreement2.Apttus__Related_Opportunity__c = newOpportunity.Id;
            newAgreement2.RecordTypeId = RecordTypeId;
            newAgreement2.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
            insert newAgreement2;
        Test.stopTest();

        Apttus__APTS_Agreement__c updatedAgreement = [SELECT Id, Mark_as_Primary__c FROM Apttus__APTS_Agreement__c WHERE
                                                        Id=: newAgreement2.Id];
        System.assertEquals(false, updatedAgreement.Mark_as_Primary__c, 'Should make agreement non primary');
    }
	
    /**
    * This test method is used for updating Agreement record
    */
    @isTest static void testUpdateAgreement() {
    	Account newAccount = UTL_TestData.createAccount();
    	insert newAccount;
		
        Opportunity testOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert testOpportunity;
		
    	Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c(Apttus__Account__c = newAccount.Id);
        agmt.RecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Subscription_Products').getRecordTypeId();
        agmt.Apttus__Related_Opportunity__c = testOpportunity.Id;
    	insert agmt;
        Test.startTest();
            agmt.Apttus__Contract_Start_Date__c = Date.newInstance(2015,11,1);
            agmt.O_Term_Years__c = '2';
            update agmt;
        Test.stopTest();
        Apttus__APTS_Agreement__c updatedAgmt =
        	[SELECT Id, Apttus__Contract_End_Date__c, Apttus__Term_Months__c 
        	 FROM Apttus__APTS_Agreement__c 
        	 LIMIT 1];
        System.assertEquals(Date.newInstance(2017,10,31), updatedAgmt.Apttus__Contract_End_Date__c);
        System.assertEquals(24, updatedAgmt.Apttus__Term_Months__c);
    }

    @isTest static void testCloneDefaultsForFDTNInitialBid () {
        Id optyId = createAccountAndOpty();
        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('FDTN – Initial Bid').getRecordTypeId();

        Test.startTest();
            Apttus__APTS_Agreement__c newAgreement1 = UTL_TestData.createAgreement();
            newAgreement1.Mark_as_Primary__c = true;
            newAgreement1.Apttus__Related_Opportunity__c = optyId;
            newAgreement1.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
            newAgreement1.Select_Agreement_Type__c = 'FDTN - Initial Bid';
            // newAgreement1.RecordTypeId = RecordTypeId;
            insert newAgreement1;

        Test.stopTest();

        System.assertEquals(recordTypeId, 
                            [SELECT RecordTypeId FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement1.Id].RecordTypeId, 
                            'Record Types do not match.');

    }

    @isTest static void testCloneDefaultsForFDTNReBid () {
        Id optyId = createAccountAndOpty();
        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('FDTN – Rebid').getRecordTypeId();

        Test.startTest();
            Apttus__APTS_Agreement__c newAgreement1 = UTL_TestData.createAgreement();
            newAgreement1.Mark_as_Primary__c = true;
            newAgreement1.Apttus__Related_Opportunity__c = optyId;
            newAgreement1.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
            newAgreement1.Select_Agreement_Type__c = 'FDTN - Rebid';
            // newAgreement1.RecordTypeId = RecordTypeId;
            insert newAgreement1;

        Test.stopTest();

        System.assertEquals(recordTypeId, 
                            [SELECT RecordTypeId FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement1.Id].RecordTypeId, 
                            'Record Types do not match.');

    }

    @isTest static void testCloneDefaultsForFDTNContract () {
        Id optyId = createAccountAndOpty();
        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('FDTN – Contract').getRecordTypeId();

        Test.startTest();
            Apttus__APTS_Agreement__c newAgreement1 = UTL_TestData.createAgreement();
            newAgreement1.Mark_as_Primary__c = true;
            newAgreement1.Apttus__Related_Opportunity__c = optyId;
            newAgreement1.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
            newAgreement1.Select_Agreement_Type__c = 'FDTN - Contract';
            // newAgreement1.RecordTypeId = RecordTypeId;
            insert newAgreement1;

        Test.stopTest();

        System.assertEquals(recordTypeId, 
                            [SELECT RecordTypeId FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement1.Id].RecordTypeId, 
                            'Record Types do not match.');

    }

    @isTest static void testCloneDefaultsForFDTNCNF () {
        Id optyId = createAccountAndOpty();
        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('FDTN – CNF').getRecordTypeId();

        Test.startTest();
            Apttus__APTS_Agreement__c newAgreement1 = UTL_TestData.createAgreement();
            newAgreement1.Mark_as_Primary__c = true;
            newAgreement1.Apttus__Related_Opportunity__c = optyId;
            newAgreement1.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
            newAgreement1.Select_Agreement_Type__c = 'FDTN - CNF';
            // newAgreement1.RecordTypeId = RecordTypeId;
            insert newAgreement1;

        Test.stopTest();

        System.assertEquals(recordTypeId, 
                            [SELECT RecordTypeId FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement1.Id].RecordTypeId, 
                            'Record Types do not match.');

    }

    @isTest static void testCloneDefaultsForFDTNChangeOrder () {
        Id optyId = createAccountAndOpty();
        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('FDTN – Change Order').getRecordTypeId();

        Test.startTest();
            Apttus__APTS_Agreement__c newAgreement1 = UTL_TestData.createAgreement();
            newAgreement1.Mark_as_Primary__c = true;
            newAgreement1.Apttus__Related_Opportunity__c = optyId;
            newAgreement1.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
            newAgreement1.Select_Agreement_Type__c = 'FDTN - Change Order';
            // newAgreement1.RecordTypeId = RecordTypeId;
            insert newAgreement1;

        Test.stopTest();

        System.assertEquals(recordTypeId, 
                            [SELECT RecordTypeId FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement1.Id].RecordTypeId, 
                            'Record Types do not match.');

    }    
    
    @isTest
    static void testSetProjectManagerEmailShouldUpdateProjectManagerEmail(){
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER).getRecordTypeId();

        Test.startTest();
            insert newAgreement;
        Test.stopTest();

        Apttus__APTS_Agreement__c updatedAgreement = [SELECT Project_Manager_Email__c FROM Apttus__APTS_Agreement__c WHERE Id= :
                                                        newAgreement.Id];
        system.assertEquals('test.test@iqvia.com', updatedAgreement.Project_Manager_Email__c, 'Should update project manager email');
    }

    @isTest
    static void testSetEmailBodyAndSubjectShouldUpdatedEmailBody(){
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CHANGE_ORDER).getRecordTypeId();

        Test.startTest();
        insert newAgreement;
        Test.stopTest();

        Apttus__APTS_Agreement__c updatedAgreement = [SELECT Email_Body__c FROM Apttus__APTS_Agreement__c WHERE Id= :
                                                        newAgreement.Id];
        system.assertNotEquals(null, updatedAgreement.Email_Body__c, 'Should update email body');
    }    
}