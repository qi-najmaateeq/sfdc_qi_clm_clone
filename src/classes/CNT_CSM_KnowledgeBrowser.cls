/*
 * Version       : 1.0
 * Description   : Apex Controller for LXC_CSM_KnowledgeBrowser 
 */ 
public class CNT_CSM_KnowledgeBrowser {
    
    /*
    * Return a List of products Name with Community_Topics__c not null and and present in CaseCategorization Object 
    */
    @AuraEnabled
    public static List<AggregateResult> getProducts(){ 
        List<AggregateResult> products = new List<AggregateResult>();
        List<Product2> prds = new List<Product2>();
        prds = new SLT_Product2().getProductWithFilter(new Set<String>{'Name'},'Community_Topics__c != \'\' ');
        Set<String> prdNames = new Set<String>();
        for(Product2 prd: prds){
           prdNames.add(prd.Name);
        }
        products = new SLT_CaseCategorization().getProducts(prdNames);
        return products;
    }
    /*
    * Return List of Knowledge__kav by productName
    */
    @AuraEnabled
    public static List<Knowledge__kav> getArticlesByProductName(String productName){
        List<Knowledge__kav> articles =  new SLT_Knowledge().selectByProductName(productName);
        return articles;
    }
}