@isTest
private class TST_DAOH_Agreement {

    static Apttus__APTS_Agreement__c setAgreementData(Boolean markAsPrimary, Id OpportuntiyId){

        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = RecordTypeId;
        testAgreement.Mark_as_Primary__c = markAsPrimary;
        testAgreement.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
        insert testAgreement;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }
    
    static Document setDocumentData(){
        Document documentRecord = UTL_TestData.createDocument('QC Check List', 'QC_Check_List', 'application/pdf');
        documentRecord.IsPublic = true;
        documentRecord.FolderId = UserInfo.getUserId();
        insert documentRecord;
        return documentRecord;
    }

    @isTest
    static void testUpdateAgreementToMakeNonPrimary() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement1 = setAgreementData(true, testOpportunity.Id);
        Apttus__APTS_Agreement__c testAgreement2 = setAgreementData(true, testOpportunity.Id);
        Set<Id> opportunityIds = new Set<Id>{testOpportunity.Id};
        Set<Id> currentAgreementIds = new Set<Id>{testAgreement1.Id};

        Test.startTest();
        DAOH_Agreement.updateAgreementToMakeNonPrimary(opportunityIds, currentAgreementIds);
        Test.stopTest();

        Apttus__APTS_Agreement__c updatedAgreement = [SELECT Mark_as_Primary__c FROM Apttus__APTS_Agreement__c WHERE Id= :
                                                        testAgreement2.Id];
        system.assertEquals(false, updatedAgreement.Mark_as_Primary__c, 'Should make agreement non primary');
    }

    @isTest
    static void testUpdateEmailBodyAndSubjectShouldReturnEmailBody(){
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id);
        
        Test.startTest();
            DAOH_Agreement.updateEmailBodyAndSubject(new List<Apttus__APTS_Agreement__c>{testAgreement}, false);
        Test.stopTest();
        
        Apttus__APTS_Agreement__c updatedAgreement = [SELECT Email_Body__c FROM Apttus__APTS_Agreement__c WHERE Id= :
                                                        testAgreement.Id];
        system.assertNotEquals(null, updatedAgreement.Email_Body__c, 'Should return email body');
    }

    @isTest
    static void testOppNumber(){
        upsert new Mulesoft_Integration_Control__c(Ignore_Validation_Rules__c = true);
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;
        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;
        Id RecordTypeId = CON_CRM.AGREEMENT_RECORD_TYPE_EARLY_ENGAGEMENT_BID;
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        testAgreement.RecordTypeId = RecordTypeId;
        insert testAgreement;
    }   

    @isTest
    static void testSetAgreementStatusAndProcessStepForMVPForInitial(){
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId();
        newAgreement.Agreement_Status__c=CON_CPQ.DRAFT;
        newAgreement.User__c=userInfo.getUserId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
            newAgreement.Process_Step__c=CON_CPQ.QC_SELF_CHECK_DRAFT;
            update newAgreement;

            System.assertEquals(CON_CPQ.DRAFT,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is DRAFT.');

            newAgreement.Process_Step__c = CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL;
            update newAgreement;

            System.assertEquals(CON_CPQ.INTERNAL_REVIEW,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is Internal Review.');

            newAgreement.Process_Step__c = CON_CPQ.TSL_APPROVED;
            update newAgreement;

            System.assertEquals(CON_CPQ.PENDING_APPROVAL,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is TSL Approved.');

            newAgreement.Process_Step__c = CON_CPQ.BUDGET_APPROVED;
            update newAgreement;

            System.assertEquals(CON_CPQ.BUDGET_APPROVED,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is Budget Approved.');
        Test.stopTest();
    }
    

    @isTest
    static void testSetAgreementStatusAndProcessStepForMVPForRebid(){
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        newAgreement.Agreement_Status__c=CON_CPQ.DRAFT;
        newAgreement.User__c=userInfo.getUserId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
            newAgreement.Process_Step__c=CON_CPQ.RECEIVED_ATP_LOI;
            update newAgreement;

            System.assertEquals(CON_CPQ.STUDY_AWARDED,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is Study Awarded.');

            newAgreement.Process_Step__c = CON_CPQ.FINAL_PREPARATION;
            update newAgreement;

            System.assertEquals(CON_CPQ.FINAL_PREPARATION,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is Internal Review.');

        Test.stopTest();
    }

    @isTest
    static void testSetAgreementStatusAndProcessStepForMVPForContract(){
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CNF).getRecordTypeId();
        newAgreement.Agreement_Status__c=CON_CPQ.DRAFT;
        newAgreement.User__c=userInfo.getUserId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Process_Step__c = CON_CPQ.BID_GRID_MAINTENANCE_COMPLETION;
            update newAgreement;

            System.assertEquals(CON_CPQ.FINAL_PREPARATION,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is Final Preparation.');

            newAgreement.Process_Step__c = CON_CPQ.CNF_DELIVERED;
            update newAgreement;

            System.assertEquals('CNF Accepted',
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is CNF Accepted.');
        Test.stopTest();
    }

    @isTest
    static void testSetAgreementStatusAndProcessStepForMVPForCNF(){
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CONTRACT).getRecordTypeId();
        newAgreement.Agreement_Status__c=CON_CPQ.DRAFT;
        newAgreement.User__c=userInfo.getUserId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Agreement_Status__c = CON_CPQ.CUSTOMER_DELIVERABLE;
            newAgreement.Process_Step__c = CON_CPQ.BALLPARK_DELIVERED;
            update newAgreement;

            System.assertEquals(true,
                                [SELECT Pricing_Tool_Locked__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Pricing_Tool_Locked__c,
                                'Pricing Tool Selected should be true.');

            newAgreement.Agreement_Status__c = CON_CPQ.CUSTOMER_DELIVERABLE;
            newAgreement.Process_Step__c = CON_CPQ.PAYMENT_SCHEDULE_DELIVERED;
            update newAgreement;

            System.assertEquals(false,
                                [SELECT Pricing_Tool_Locked__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Pricing_Tool_Locked__c,
                                'Pricing Tool Selected should be false.');

            newAgreement.Process_Step__c = CON_CPQ.PAYMENT_SCHEDULE_DELIVERED;
            newAgreement.Agreement_Status__c = CON_CPQ.CUSTOMER_DELIVERABLE;
            update newAgreement;

            System.assertEquals(false,
                                [SELECT Pricing_Tool_Locked__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Pricing_Tool_Locked__c,
                                'Agreement Status is Draft.');
        Test.stopTest();
    }

    @isTest
    static void testSetAgreementStatusAndProcessStepForMVPForChangeOrder(){
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.Apttus__Company_Signed_Date__c = Date.Today();
        newAgreement.Apttus__Other_Party_Signed_Date__c = Date.Today()+1;
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CNF).getRecordTypeId();
        newAgreement.Agreement_Status__c=CON_CPQ.DRAFT;
        newAgreement.User__c=userInfo.getUserId();
        setDocumentData();                
        insert newAgreement;
        
        Attachment attach=UTL_TestData.createAttachment();   	
        attach.Name=CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_NOBI_APP_NAME;
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=newAgreement.id;
        attach.contentType = CON_CPQ.ATTACHMENT_CONTENT_TYPE;
        insert attach;

        Test.startTest();
            newAgreement.Process_Step__c = CON_CPQ.BUDGET_CHANGES_POST_PL_REVIEW;
            update newAgreement;

            System.assertEquals(CON_CPQ.Draft,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is Draft.');

            newAgreement.Agreement_Status__c = CON_CPQ.CNF_DELIVERED;
            newAgreement.Process_Step__c = CON_CPQ.CNF_DELIVERED;
            update newAgreement;

            System.assertEquals('CNF Accepted',
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is CNF Accepted.');

            newAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
            newAgreement.Process_Step__c = CON_CPQ.PENDING_PL_REVIEW;
            update newAgreement;

            System.assertEquals(CON_CPQ.PENDING_PL_REVIEW,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is Pending PL Review.');

            newAgreement.Agreement_Status__c = CON_CPQ.CNF_DELIVERED;
            newAgreement.Process_Step__c = CON_CPQ.CNF_DELIVERED;
            update newAgreement;

            System.assertEquals(CON_CPQ.NONE,
                                [SELECT Process_Step__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Process_Step__c,
                                'Agreement Status is Pending PL Review.');
        Test.stopTest();
    }

    @isTest
    static void testSetAgreementStatusAndProcessStepForMVPForInitialBid1(){
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.Apttus__Company_Signed_Date__c = Date.Today();
        newAgreement.Apttus__Other_Party_Signed_Date__c = Date.Today()+1;
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        newAgreement.Agreement_Status__c=CON_CPQ.DRAFT;
        newAgreement.User__c=userInfo.getUserId();
        insert newAgreement;
        
        Attachment attach=UTL_TestData.createAttachment();   	
        attach.Name=CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_NOBI_APP_NAME;
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=newAgreement.id;
        attach.contentType = CON_CPQ.ATTACHMENT_CONTENT_TYPE;
        insert attach;

        Test.startTest();
            newAgreement.Agreement_Status__c = CON_CPQ.PENDING_APPROVAL;
            newAgreement.Process_Step__c = CON_CPQ.CHALLENGE_CALL_COMPLETE;
            update newAgreement;

            System.assertEquals(CON_CPQ.INTERNAL_REVIEW,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                'Agreement Status is Internal Review.');

            newAgreement.Agreement_Status__c = CON_CPQ.PENDING_APPROVAL;
            newAgreement.Process_Step__c = CON_CPQ.TSL_REVIEW;
            update newAgreement;

            System.assertEquals(CON_CPQ.INTERNAL_REVIEW,
                                [SELECT Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Id =: newAgreement.Id].Agreement_Status__c,
                                    'Agreement Status is Internal Review.');
        Test.stopTest();
    }
    
    @isTest
    static void testCreateTaskOnAgreementShouldCreateTask(){
    
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert newOpportunity;

        String profileName = [SELECT Id, Name FROM Profile WHERE Name='Sales User' LIMIT 1].Name;
        User usr = UTL_TestData.createUser(profileName, 1)[0];
        usr.ManagerId = UserInfo.getUserId();
        insert usr;

        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Mark_as_Primary__c = true;
        testAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        testAgreement.RecordTypeId = RecordTypeId;
        testAgreement.Agreement_Status__c = CON_CPQ.DRAFT;
        testAgreement.Process_Step__c = CON_CPQ.QC_SELF_CHECK_DRAFT;
        testAgreement.User__c = userInfo.getUserId();
        testAgreement.OwnerId = usr.Id;
        insert testAgreement;

        Test.startTest();
            testAgreement.Process_Step__c = CON_CPQ.LINE_MANAGER_QC;
            update testAgreement;
        Test.stopTest();

        List<Task> taskList = [SELECT Id FROM Task WHERE WhatId =: testAgreement.Id];
        System.assertNotEquals(null, taskList.size(), 'Task has created');

    }
    
    @isTest
    private static void testCreateOrUpdateBudgetAgreementHistoryRecordForTSLApproved() {
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
        newAgreement.Process_Step__c = CON_CPQ.TSL_REVIEW;
        newAgreement.User__c = UserInfo.getUserId();
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Process_Step__c = CON_CPQ.TSL_APPROVED;
            newAgreement.Agreement_Status__c = CON_CPQ.PENDING_APPROVAL;
            update newAgreement;
        Test.stopTest();

        List<Budget_Agreement_History__c> budgetAgreementHistoryList = [SELECT Id FROM Budget_Agreement_History__c];

        System.assertEquals(budgetAgreementHistoryList.size()>0, true, 'Budget Agreement history is not null');
    }

    @isTest
    private static void testCreateOrUpdateBudgetAgreementHistoryRecordForChallengeCallComplete() {
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
        newAgreement.Process_Step__c = CON_CPQ.TSL_REVIEW;
        newAgreement.User__c = UserInfo.getUserId();
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Process_Step__c = CON_CPQ.CHALLENGE_CALL_COMPLETE;
            update newAgreement;
        Test.stopTest();

        List<Budget_Agreement_History__c> budgetAgreementHistoryList = [SELECT Id FROM Budget_Agreement_History__c];

        System.assertEquals(budgetAgreementHistoryList.size()>0, true, 'Budget Agreement history is not null');
    }

    @isTest
    private static void testCreateOrUpdateBudgetAgreementHistoryRecordForLineManagerQC() {
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.Agreement_Status__c = CON_CPQ.DRAFT;
        newAgreement.Process_Step__c = CON_CPQ.LINE_MANAGER_QC;
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
            newAgreement.Process_Step__c = CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL;
            update newAgreement;
        Test.stopTest();

        List<Budget_Agreement_History__c> budgetAgreementHistoryList = [SELECT Id FROM Budget_Agreement_History__c];

        System.assertEquals(budgetAgreementHistoryList.size()>0, true, 'Budget Agreement history is not null');
    }

    @isTest
    private static void testCreateOrUpdateBudgetAgreementHistoryRecordForBudgetApproved() {
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.Agreement_Status__c = CON_CPQ.PENDING_APPROVAL;
        newAgreement.Process_Step__c = CON_CPQ.APPROVAL_REVIEW;
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Agreement_Status__c = CON_CPQ.BUDGET_APPROVED;
            newAgreement.Process_Step__c = CON_CPQ.BUDGET_APPROVED;
            update newAgreement;
        Test.stopTest();

        List<Budget_Agreement_History__c> budgetAgreementHistoryList = [SELECT Id FROM Budget_Agreement_History__c];

        System.assertEquals(budgetAgreementHistoryList.size()>0, true, 'Budget Agreement history is not null');
    }

    @isTest
    private static void testCreateOrUpdateBudgetAgreementHistoryRecordForCustomerDelieverableSent() {
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.Agreement_Status__c = CON_CPQ.FINAL_PREPARATION;
        newAgreement.Process_Step__c = CON_CPQ.FINAL_QC;
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        insert newAgreement;

        Test.startTest();
            newAgreement.Agreement_Status__c = CON_CPQ.PROPOSAL_DELIVERY;
            newAgreement.Process_Step__c = CON_CPQ.CUSTOMER_DELIVERABLE_SENT;
            update newAgreement;
        Test.stopTest();

        List<Budget_Agreement_History__c> budgetAgreementHistoryList = [SELECT Id FROM Budget_Agreement_History__c];

        System.assertEquals(budgetAgreementHistoryList.size()>0, true, 'Budget Agreement history is not null');
    }

    @isTest
    private static void testCreateOrUpdateBudgetAgreementHistoryRecord() {
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';        
        newAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
        newAgreement.Process_Step__c = CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL;
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        insert newAgreement;

        Budget_Agreement_History__c budgetAgreementHistory = new Budget_Agreement_History__c(New_Process_Step__c=CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL);
        budgetAgreementHistory.Agreement__c = newAgreement.Id;
        insert budgetAgreementHistory;

        Test.startTest();
            newAgreement.Process_Step__c = CON_CPQ.LINE_MANAGER_QC;
            update newAgreement;
        Test.stopTest();

        List<Budget_Agreement_History__c> budgetAgreementHistoryList = [SELECT Id FROM Budget_Agreement_History__c];

        System.assertEquals(budgetAgreementHistoryList.size()>0, true, 'Budget Agreement history is not null');
    }

    @isTest
    private static void testCreateOrUpdateBudgetAgreementHistoryRecordForContractRecordType() {
        Account newAccount = UTL_TestData.createAccount();
        insert newAccount;

        Opportunity newOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        newOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert newOpportunity;

        Apttus__APTS_Agreement__c newAgreement = UTL_TestData.createAgreement();
        newAgreement.Mark_as_Primary__c = true;
        newAgreement.Apttus__Related_Opportunity__c = newOpportunity.Id;
        newAgreement.Project_Manager_Name__c = 'test test';
        newAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
        newAgreement.Process_Step__c = CON_CPQ.KEY_STAKEHOLDER_REVIEW;
        newAgreement.RecordTypeId = SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_CONTRACT).getRecordTypeId();
        insert newAgreement;

        Budget_Agreement_History__c budgetAgreementHistory = new Budget_Agreement_History__c(New_Process_Step__c=CON_CPQ.KEY_STAKEHOLDER_REVIEW);
        budgetAgreementHistory.Agreement__c = newAgreement.Id;
        insert budgetAgreementHistory;

        Test.startTest();
            newAgreement.Process_Step__c = CON_CPQ.PENDING_PL_REVIEW;
            update newAgreement;
        Test.stopTest();

        List<Budget_Agreement_History__c> budgetAgreementHistoryList = [SELECT Id FROM Budget_Agreement_History__c];

        System.assertEquals(budgetAgreementHistoryList.size()>0, true, 'Budget Agreement history is not null');
    }    
}