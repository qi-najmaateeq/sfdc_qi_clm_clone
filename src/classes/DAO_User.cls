public class DAO_User extends fflib_SObjectDomain {
    
    /**
    * Constructor of this class
    * @params sObjectList List<SObject>
    */
    public DAO_User(List<SObject> sObjectList) {
        super(sObjectList);
    }
    
    /**
    * Constructor Class for construct new Instance of This Class
    */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new DAO_User(sObjectList);
        }
    }
    
    /**
    * This method is used for before insert of the User trigger.
    * @return void
    */
    public override void onBeforeInsert() {
        if(!isUserSystemAdmin()){
            // Comment out below line as a hotfix, need to refactor again
            // blockRecordtoSave((List<User>)Records);
        }
        DAOH_User.setupFieldValues((List<User>)records, null);
        DAOH_User.updateLastNametoUnavailable((List<User>)records, null);
    }
    
    /**
    * This method is used for after insert of the User trigger.
    * @return void
    */
    public override void onAfterInsert() {
        DAOH_User.addPermissionSetAfterInsertCSM((List<User>)records);
        String jsonUserList = JSON.serialize((List<User>)records);
        DAOH_User.createOrUpdateContactForUsers(jsonUserList, null);
        DAOH_User.updateContactOnPortalUser((List<User>)Records);
    }
    
    /**
    * This method is used for before update of the User trigger.
    * @params  Map<Id, SObject> existingRecords
    * @return void
    */
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        if(!isUserSystemAdmin()){
            // Comment out below line as a hotfix, need to refactor again
            // blockRecordtoSave((List<User>)Records);
        }
        DAOH_User.setupFieldValues((List<User>)records, (Map<Id, User>)existingRecords);
        DAOH_User.updateLastNametoUnavailable((List<User>)records, (Map<Id, User>)existingRecords);
        
    }
    
    /**
    * This method is used for after update of the User trigger.
    * @params  Map<Id, SObject> existingRecords
    * @return void
    */
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        DAOH_User.PermissionSetAfterUpdateCSM((List<User>)records, (Map<Id,User>)existingRecords);
        if(!CON_CRM.preventContactUpdate) {
            String jsonUserList = JSON.serialize((List<User>)records);
            String jsonUserOldMap = JSON.serialize((Map<Id, User>)existingRecords);
            DAOH_User.createOrUpdateContactForUsers(jsonUserList, jsonUserOldMap);
        }
        DAOH_User.updateContactOnPortalUser((List<User>)Records);
    }
    
    private boolean isUserSystemAdmin(){
        string adminProfile='System Administrator';
        string serviceProfile='Service User';
        string salesProfile='Sales User';
        String pName = [Select Name from Profile where Id = :UserInfo.getProfileId()].Name;
        List<PermissionSetAssignment> permissionSetAssignments = [SELECT Id FROM PermissionSetAssignment WHERE PermissionSet.Label = 'Advanced Administrator' AND AssigneeId = :UserInfo.getUserId()];
        if(pName.equals(adminProfile) || (pName.equals(serviceProfile) && (permissionSetAssignments.size()>0)) || pName.equals(salesProfile)){
            return true;
        }else{
            return false;
        }    
    }
    
    //use to add custom Error on the Record
    
    private void blockRecordtoSave(List<User> records){
        for(User usr: records){
            usr.addError('You cannot Create/Update user record.');
        }
    }
}
