public with sharing class DAO_LineItemGroup extends fflib_SObjectDomain {

    /**
    * Constructor of this class
    * @params sObjectList List<Line_Item_Group__c> 
    */
    public DAO_LineItemGroup(List<Line_Item_Group__c> sObjectList) {
        super(sObjectList);
    }
    
    /**
    * Constructor Class for construct new Instance of This Class
    */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new DAO_LineItemGroup(sObjectList);
        }
    }    
    /**
     * This method is used for before insert of the Line_Item_Group__c trigger.
     * @return void
     */
    public override void onBeforeInsert() {
        DAOH_CRM_LineItemGroup.setLIGCurrency((List<Line_Item_Group__c>)Records);
    }
    /**
    * This method is used for After insert of the Line_Item_Group__c trigger.
    * @return void
    */
    public override void onAfterInsert() {
        setOppFields();
    } 
    
    /**
    * This method is used for before Update of the Line_Item_Group__c trigger.
    * @return void
    */
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        Map<Id, Line_Item_Group__c> oldMap = (Map<Id, Line_Item_Group__c>)existingRecords;
        List<Line_Item_Group__c> filteredLIG = new List<Line_Item_Group__c>();
        Set<Id> oppIdsSet = new Set<Id>();
        for(Line_Item_Group__c lig : (List<Line_Item_Group__c>) Trigger.new){   
            if(oldMap.get(lig.Id).RWE_Study__c != lig.RWE_Study__c ){
                filteredLIG.add(lig);
                oppIdsSet.add(lig.Opportunity__c);
            }
        }
        if(filteredLIG.size() > 0){
            List<RWEStudyProduct__c> rweStudyProducts = RWEStudyProduct__c.getall().values();
            List<String> rweStudyProductCodes = new List<String>();
            for(RWEStudyProduct__c  rweStudy : rweStudyProducts){
                rweStudyProductCodes.add(rweStudy.Product_Code__c);
            } 
            List<OpportunityLineItem> olisWithRWEProductList = new SLT_OpportunityLineItems().selectByOLIProductCode(oppIdsSet,rweStudyProductCodes,new Set<String>{'Id','ProductCode','OpportunityId'});
            Map<Id,List<OpportunityLineItem>> oppIdtoOLIMap = new Map<Id,List<OpportunityLineItem>>();
            for(OpportunityLineItem oli : olisWithRWEProductList){
                if(oppIdtoOLIMap.containsKey(oli.OpportunityId)){
                    oppIdtoOLIMap.get(oli.OpportunityId).add(oli);
                }else{
                    oppIdtoOLIMap.put(oli.OpportunityId,new List<OpportunityLineItem>{oli});
                }
            }
            for(Line_Item_Group__c lig : filteredLIG){   
                if(oppIdtoOLIMap.containsKey(lig.Opportunity__c) && oppIdtoOLIMap.get(lig.Opportunity__c).size() > 0){
                    lig.RWEManuallyEdited__c = true;
                }else{
                    lig.RWEManuallyEdited__c = false;
                    lig.RWE_Study__c = false;
                }
            }
        }        
    }  
    /**
    * This method is used for After update of the Line_Item_Group__c trigger.
    * @return void
    */
    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {
        //This is the section for OWF where all the methods that needs to be run in a normal sequence are included.
        if(!UTL_ExecutionControl.stopTriggerExecution_OWF && Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_RR_Trigger__c) {
           
                DAOH_OWF_LineItemGroup.createClinicalBidResourceRequestsOnLineItemGroupUpdate((List<Line_Item_Group__c>)Records, (Map<Id, Line_Item_Group__c>)existingRecords); 
            
        }
    }
    
    public void setOppFields(){
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Opportunity.SobjectType
            }
        );
        Set<Id> oppIdSet = new Set<Id>();
        for(Line_Item_Group__c lig : (List<Line_Item_Group__c>) Trigger.new){
            oppIdSet.add(lig.Opportunity__c);
        }
        Set<String> oppFieldSet = new Set<String>{'Id', 'Line_Item_Group__c'};
        Map<Id, Opportunity> oppIdToOppMap = new SLT_Opportunity().getOpportunityById(oppIdSet, oppFieldSet);
        List<Opportunity> oppToUpdateList = new List<Opportunity>();
        for(Line_Item_Group__c lig : (List<Line_Item_Group__c>) Trigger.new) {    
            if(oppIdToOppMap.containsKey(lig.Opportunity__c)){
                if(oppIdToOppMap.get(lig.Opportunity__c).Line_Item_Group__c == null){
                    oppIdToOppMap.get(lig.Opportunity__c).Line_Item_Group__c = lig.id;
                    oppToUpdateList.add(oppIdToOppMap.get(lig.Opportunity__c));
               }
            }
        }
        if(oppToUpdateList.size() > 0){
            UTL_ExecutionControl.stopTriggerExecution = true;
            uow.registerDirty(oppToUpdateList);
            uow.commitWork();
            UTL_ExecutionControl.stopTriggerExecution = false;
        }
    } 
}