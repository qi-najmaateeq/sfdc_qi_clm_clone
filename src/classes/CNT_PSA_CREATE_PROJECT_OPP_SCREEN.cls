/***********************************************
   Name      :     CNT_PSA_CREATE_PROJECT_OPP_SCREEN 
   Date      :     8-April-2019
   Usage     :     Validate Opportunity for create project before redirecting to LI org 
   Author    :     Anju Kumari
***********************************************/

public class CNT_PSA_CREATE_PROJECT_OPP_SCREEN {
    
    
    @AuraEnabled 
    public static List<String> validateOppForProjectCreation(Id opportunityId) {
        Boolean passedValidation = true;
        Id selectedComponentId;
        List<String> errorMessages = new List<String>();
        Opportunity currentOpportunity = new Opportunity();
        try {
            List<Opportunity> lstOpportunity = new List<Opportunity>([ SELECT LI_Opportunity_Id__c,Name,Opportunity_Number__c,StageName 
                                                                      FROM Opportunity 
                                                                      WHERE id =:opportunityId ]);
            currentOpportunity = lstOpportunity[0];
            System.debug('Current Opportunity '+currentOpportunity);
            System.debug('Current Opportunity '+currentOpportunity.StageName);
            // ensure oppty stage it at least stage 4
            if(currentOpportunity == null ||
               currentOpportunity.StageName == null ||
               currentOpportunity.StageName == '' ||
               currentOpportunity.StageName.substring(0,1) == '1' || 
               currentOpportunity.StageName.substring(0,1) == '2' || 
               currentOpportunity.StageName.substring(0,1) == '3' ||
               currentOpportunity.StageName.substring(0,1) == '4' ) {                   
                   System.debug('project_error_opp_stage_invalid'); 
                   passedValidation = false;
                   errorMessages.add('Opportunity stage is not valid for creating projects.');
               }  
            selectedComponentId = getComponentListData(opportunityId);
            // check for selected component
            if(selectedComponentId == null) {               
                System.debug('create_project_error_component_required');
                System.debug('Selected Opp Id ' +selectedComponentId);             
                passedValidation = false;
                errorMessages.add('Component is required to create projects.');
            } 
            
            if(currentOpportunity.LI_Opportunity_Id__c!=null && currentOpportunity.LI_Opportunity_Id__c!='' && passedValidation) {
                errorMessages.add(currentOpportunity.LI_Opportunity_Id__c);
                return errorMessages;
            }
            return errorMessages;
        } catch(Exception e) {
            return null;
        }
        
        //return passedValidation;
    }
    @AuraEnabled
    public static Id getComponentListData(Id opportunityId) {
        
        List <OpportunityLineItem> opportunityLineItemList = [SELECT o.Id,o.PricebookEntry.Product2.Name,o.PricebookEntry.Product2.Id,o.TotalPrice
                                                              FROM OpportunityLineItem o 
                                                              WHERE o.OpportunityId = :opportunityId AND o.PSA_Budget__c  = null 
                                                              //AND o.PricebookEntry.Product2.Business_Type__c != 'I&A'
                                                              AND o.PricebookEntry.Product2.Material_Type__c = 'ZREP'
                                                              ORDER BY o.PricebookEntry.Product2.Name ASC ];
        
        //if(opportunityLineItemList.size() > 0 && selectedComponentId == null)
        if(opportunityLineItemList.size() > 0){
            return opportunityLineItemList[0].Id;
            
        }
        return null;
    }
    
    @AuraEnabled
    public static Legacy_Org_Link__c getLegacyOrgLink(){
        return Legacy_Org_Link__c.getInstance();
    }
}