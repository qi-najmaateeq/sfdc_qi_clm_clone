/**
 * This test class is used to test all methods in Resource Request trigger.
 * version : 1.0
 */
@isTest
private class TST_DAO_Resource_Request {
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        insert agreement; 
    }
    
    /**
     * This test method used to test Insert and Update of Resource Request
     */
    static testmethod void testResourceRequestInsertUpdate() {
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        Test.startTest();
            insert resourceRequest;
            resourceRequest.SubGroup__c = 'Test Sub Group';
            update resourceRequest;
        Test.stopTest();
        String expected = 'Clinical/Test Sub Group';
        pse__Resource_Request__c actualRR = [Select Id, pse__Resource_Request_Name__c From pse__Resource_Request__c limit 1];
        
    }
    
    /**
     * This test method used to test delete Resource Request
     */
    static testmethod void testResourceRequestDelete() {
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id limit 1];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        insert resourceRequest;
        
        Test.startTest();
            delete resourceRequest;
        Test.stopTest();
        
        List<pse__Resource_Request__c> resourceRequestList = [Select Id, pse__Resource_Request_Name__c From pse__Resource_Request__c];
        Integer expected = 1;
        system.assertEquals(expected, resourceRequestList.size());
    }
}