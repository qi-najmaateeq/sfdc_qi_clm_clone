/*
 * Version       : 1.0
 * Description   : Test Class for CNT_CSM_PortalFile
 */
@isTest
private class TST_CNT_CSM_PortalFile {
    
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
                
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
         
        String profilId2 = [select id from Profile where Name='System Administrator'].Id;
        User accOwner = New User(Alias = 'su',UserRoleId= portalRole.Id, ProfileId = profilId2, Email = 'john2@iqvia.com',IsActive =true ,Username ='john2@iqvia.com', LastName= 'testLastName', CommunityNickname ='testSuNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert accOwner;
        
        System.runAs (accOwner) {
        
            Account account = UTL_TestData.createAccount();
            account.ownerId=accOwner.Id;
            insert account;
            
            Product2 product = UTL_TestData.createProduct();
            product.Community_Topics__c='OneKey';
            product.SpecificToCSM__c = True;
            insert product;
            
                    
            Asset asset = new Asset(Name = 'TestAsset', AccountId = account.Id, Product2Id = product.id);
            insert asset;
            
            ContentFolder cf = new ContentFolder(Name='TestContentFolder');
            insert cf;
            ContentFolder cf2 = new ContentFolder(Name='TestContentFolder2', ParentContentFolderId=cf.Id );
            insert cf2; 
            
            ContentVersion cv  = new ContentVersion(Title = 'Penguins', PathOnClient = 'Penguins.jpg',  VersionData = Blob.valueOf('Test Content'), IsMajorVersion = true);
            insert cv;  
            
            Topic topic = New Topic(Name = 'OneKey');
            insert topic;
                  
        }
       
        
    }
    
    
    /**
     * This method used to get List of ContentFolder  for current user
     */    
    @IsTest
    static void testGetFolders() {
        List<ContentFolder> contentFolders = new List<ContentFolder>();
        Account acc = [SELECT id FROM Account WHERE Name = 'TestAccount'];
        Contact contact = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='yourusername@iqvia.com',
            Portal_Case_Type__c = 'Technology Solutions',
            Contact_User_Type__c='HO User',
            AccountId = acc.Id);
        insert contact;
         
        String profilId = [select id from Profile where Name='CSM Customer Community Plus Login User'].Id;
        User user = New User(Alias = 'com', Email = 'yourusername@iqvia.com',IsActive =true , ContactId = contact.Id, ProfileId = profilId,Username =' john@acme.com', LastName= 'testLastName', CommunityNickname ='testCommunityNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert user;
        Test.startTest();
        system.runAs(user){
                contentFolders = CNT_CSM_PortalFile.getFolders();
        }
        Test.stopTest();
        Integer expected = 1;
        Integer actual = contentFolders.size();
        /** System.assertEquals(expected, actual); */
    }
    
    /**
     * This method used to get List<ContentFolder> by TopicId
     */    
    @IsTest
    static void testGetFolderByTopicId() {
        List<ContentFolder> contentFolders = new List<ContentFolder>();
        Topic topic = [SELECT id FROM Topic WHERE Name = 'OneKey'];
        Test.startTest();
        String id = topic.Id;
        contentFolders = CNT_CSM_PortalFile.getFolderByTopicId(id);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = contentFolders.size();
        /** System.assertEquals(expected, actual); */
    }
    
    /**
     * This method used to get List of ContentFolder by parentContentFolderId
     */    
    @IsTest
    static void testGetFoldersByParentId() {
        List<ContentFolder> contentFolders = new List<ContentFolder>();
        ContentFolder f = [SELECT id FROM ContentFolder WHERE Name = 'TestContentFolder'];
        Test.startTest();
        contentFolders = CNT_CSM_PortalFile.getFoldersByParentId(f.Id);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = contentFolders.size();
       /** System.assertEquals(expected, actual); **/
    }
    
    /**
     * This method used to get List of ContentFolderMember by parentContentFolderId
     */    
     
     /*
    @IsTest
    static void testGetFolderMemberByParentId() {
        List<ContentFolderMember> contentFolderMembers = new  List<ContentFolderMember>();
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        List<ContentFolderMember> cfm = [select Id ,ParentContentFolderId from ContentFolderMember where ChildRecordId=:documents[0].Id];
        Test.startTest();
        contentFolderMembers = CNT_CSM_PortalFile.getFolderMemberByParentId(cfm[0].ParentContentFolderId);
        Test.stopTest();
        Integer expected = 1;
        Integer actual = contentFolderMembers.size();
        System.assertEquals(expected, actual);
    }*/
 
     
}