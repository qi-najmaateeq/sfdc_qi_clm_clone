/**
 * This test class is used to test all methods in CNT_CRM_ChangeCurrency Controller.
 * version : 1.0
 */
@isTest
public class TST_DAOH_CRM_LineItemGroup {
	
    @testSetup
    static void dataSetup() {
        Mulesoft_Integration_Control__c mulesoft = UTL_TestData.createMulesoftIntegrationControl(UserInfo.getUserId());
        mulesoft.Is_Mulesoft_User__c = true;
        upsert mulesoft;
        List<User> userList = UTL_TestData.createUser('System Administrator', 1);
        userList[0].PIC_Eligible__c = true;
        insert userList;
        User_Permissions__c userPermission = new User_Permissions__c();
        userPermission.IES_User__c = true;
        userPermission.Name = 'Sales_with_Bid_History';
        insert userPermission;
        System.runAs(userList[0]) {
            PermissionSetAssignment psa = UTL_TestData.createPermissionSetAssignmentRecord(userPermission.Name, userList[0].Id);
            insert psa;
        }
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        opp.Line_of_Business__c = 'Lab';
        opp.Principle_in_Charge__c = userList[0].Id;
        insert opp;
        Product2 product = UTL_TestData.createProduct();
        product.Material_Type__c = 'ZQUI';
        product.Offering_Group_Code__c = 'GPRADS';
        insert product;
        PricebookEntry pbEntry = UTL_TestData.createPricebookEntry(product.Id);
        insert pbEntry;
        OpportunityLineItem oppLineItem = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
        insert oppLineItem;
        OpportunityLineItemSchedule olis =UTL_TestData.createOpportunityLineItemSchedule(oppLineItem.Id); 
        insert olis; 
    }
    
    static testmethod void testValidateLineItemGroup() {
        Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Name = 'TestOpportunity'];
        Line_Item_Group__c lig = [SELECT Id FROM Line_Item_Group__c LIMIT 1];
        User user = [SELECT Id FROM User WHERE LastName = 'lastName123' LIMIT 1];
        String expectedStage = CON_CRM.CLOSED_WON_STAGE;
        String expectedLineOfBusiness = 'Lab';
        System.runAs(user) {
            Test.startTest();
            Map<String, String> mapOfFieldAPIWithErrorMessage = new Map<String, String>();
            mapOfFieldAPIWithErrorMessage = DAOH_CRM_LineItemGroup.validateLineItemGroup(opp.id, expectedStage, lig.Id, expectedLineOfBusiness);
            Test.stopTest();    
        } 
    }
    
    static testMethod void testGetCurrencyConversionRate() {
        Test.startTest();
        Map<String,CurrencyType> currencyTypeMap = new Map<String,CurrencyType>(); 
        CurrencyType currencyType = [SELECT Id, ConversionRate, ISOcode FROM CurrencyType WHERE IsoCode = 'USD' LIMIT 1];
        currencyTypeMap.put(currencyType.IsoCode, currencyType);
        currencyType = [SELECT Id, ConversionRate, ISOcode FROM CurrencyType WHERE IsoCode = 'INR' LIMIT 1];
        currencyTypeMap.put(currencyType.IsoCode, currencyType);
        Double modifiedOlisPrice = DAOH_CRM_LineItemGroup.getCurrencyConversionRate(currencyTypeMap,'INR','USD', 2);
        Test.stopTest();
    }
}