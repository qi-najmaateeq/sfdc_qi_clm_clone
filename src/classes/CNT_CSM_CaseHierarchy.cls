public class CNT_CSM_CaseHierarchy {
@AuraEnabled
    public static List<HierarchyData> findHierarchyData(string recId){
        List<HierarchyData> returnValue = new List<HierarchyData>();
        List<String> currentParent      = new List<String>{};
        Integer level = 0;
        Boolean endOfStructure = false;
        string topMostparent = GetUltimateParentId(recId);
        currentParent.add(topMostparent);
        List<sObject> queryOutput = new List<sObject>();
        while ( !endOfStructure ){
            try{
                if(Limits.getLimitQueries()-Limits.getQueries()>0){
                    if( level == 0 ){
                        queryOutput = new SLT_Case().getCaseListHierarchy('Id', currentParent);
                    } 
                    else {
                        queryOutput = new SLT_Case().getCaseListHierarchy('ParentId', currentParent);
                    }
                }else{
                    endOfStructure = true;
                }
                
            }catch(exception ex){ 
                endOfStructure = true;
            }
            
            if( queryOutput.size() == 0 ){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                for ( Integer i = 0 ; i < queryOutput.size(); i++ ){
                    sobject sb= queryOutput[i];
                    currentParent.add(string.valueof(sb.get('id')) );
                    HierarchyData ss = new HierarchyData();
                    if(sb.get('Id') == recId || level == 0){
                        ss.expanded = true;
                    }else{
                        ss.expanded = false;
                    }
                    ss.rec = sb;
                    returnValue.add(ss);
                 }
            }
            level++;
        }
        return returnValue;
    }
    
    public static String GetUltimateParentId( string recId){
        Boolean top = false;
        while ( !top ) {
            Case record = new SLT_Case().getParentCaseHierarchy(recId);
            if(record !=null){
                if ( record.get('ParentId') != null ) {
                    recId = string.valueof(record.get('ParentId'));
                }else {
                    top = true;
                }    
            }
            else{
                top = true;
            }
        }
        return recId ;
    }

    public class HierarchyData{
        @AuraEnabled
        public sObject rec{get;set;}
        @AuraEnabled
        public boolean expanded{get;set;}
        public HierarchyData(){
            expanded = false;
        }
    }
}
