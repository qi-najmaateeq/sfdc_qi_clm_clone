public class SLT_Agreement extends fflib_SObjectSelector{

    /**
    * This method used to get field list of sobject
    */
    public List<Schema.SObjectField> getSObjectFieldList(){

        return new List<Schema.SObjectField> {
            Apttus__APTS_Agreement__c.Id,
            Apttus__APTS_Agreement__c.Select_Pricing_Tool__c,
            Apttus__APTS_Agreement__c.Mark_as_Primary__c,
            Apttus__APTS_Agreement__c.Apttus__Related_Opportunity__c,
            Apttus__APTS_Agreement__c.Pricing_tool_Locked__c,
            Apttus__APTS_Agreement__c.Budget_Checked_Out_By__c
        };
    }

    /**
     * Method to override OrderBy
     */
    public override String getOrderBy() {
        return CON_CRM.AGR_ORDERING_DESC;
    }

    /**
    * This method used to set up type of sobject
    * @return  Schema.SObjectType
    */
    public Schema.SObjectType getSObjectType(){

        return Apttus__APTS_Agreement__c.sObjectType;
    }

    /**
    * This method used to get Apttus__APTS_Agreement__c by Id
    * @return  List<Apttus__APTS_Agreement__c>
    */
    public List<Apttus__APTS_Agreement__c> selectById(Set<Id> idSet){

        return (List<Apttus__APTS_Agreement__c>) selectSObjectsById(idSet);
    }

    /*
    * This method is use to query primary agreement of related opportunity which exclude current agreement
    *
    */
    public List<Apttus__APTS_Agreement__c> getRelatedOpportunityPrimaryAgreement(Set<Id> opportunityIds, 
        Set<Id> currentAgreementIds, Set<String> fieldSet){

        return (List<Apttus__APTS_Agreement__c>) Database.query(
        newQueryFactory(true).selectFields(fieldSet).setCondition('Apttus__Related_Opportunity__c in: opportunityIds AND'
            + ' Id Not In: currentAgreementIds AND Mark_as_Primary__c = true').toSOQL());
    }
    
    /*
    * This method is use to query agreement on the basis of id to get the required details
    *
    */
    public Apttus__APTS_Agreement__c getAgreementDetails(Id agreementId, Set<String> fieldSet){
        return (Apttus__APTS_Agreement__c) Database.query(
            newQueryFactory(true).selectFields(fieldSet).setCondition('ID = :agreementId').toSOQL());
    }

    /*
    * This method is use to query agreement on the basis of id and record type
    *
    */
    public List<Apttus__APTS_Agreement__c> getAgreementDetailsByIdAndRecorType(Id agreementId, Set<Id> recordTypeIds,
        Set<String> fieldSet){
        
        return (List<Apttus__APTS_Agreement__c>) Database.query(
            newQueryFactory(true).selectFields(fieldSet).setCondition('Id =: agreementId AND RecordTypeId in:' +
                ' recordTypeIds').toSOQL());
    }
    
    /**
     * This method used to get Agreement by Id
     * @return  List<Apttus__APTS_Agreement__c>
     */
    public List<Apttus__APTS_Agreement__c> getAgreementFieldsById(Set<ID> idSet, Set<String> fieldSet) {
        return (List<Apttus__APTS_Agreement__c>) Database.query(
            									 newQueryFactory(true).
               									 selectFields(fieldSet).
               									 setCondition('Id in: idSet').
               									 toSOQL());
    }
    
    /*
    * This method is use to query agreement on the basis of id and record type
    *
    */
    public List<Apttus__APTS_Agreement__c> getAgreementDetailsByIdsAndRecorType(Set<Id> agreementIds, Set<Id> recordTypeIds,
                                                                                Set<String> fieldSet){

        return (List<Apttus__APTS_Agreement__c>) Database.query(
            newQueryFactory(true).selectFields(fieldSet).setCondition('Id =: agreementIds AND RecordTypeId in:' +
                                                                    ' recordTypeIds').toSOQL());
    }
    
    /**
     * This method used to get BudgetAgreementHistoryDetails by Agreement Id
     * @return  List<Apttus__APTS_Agreement__c>
     */
    public List<Apttus__APTS_Agreement__c> selectBudgetAgreementHistory(List<Apttus__APTS_Agreement__c> agreementList) {
        return Database.query('SELECT Id, (SELECT Id, New_Process_Step__c, Old_Process_Step__c, Process_Step_End_Date__c FROM Budget_Agreement_History__r)'+
            +'FROM Apttus__APTS_Agreement__c WHERE Id IN: agreementList');
    }
}