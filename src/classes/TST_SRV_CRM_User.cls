/*
 * Version       : 1.0
 * Description   : Test Class for SRV_CRM_User
 */
@isTest
private class TST_SRV_CRM_User {
    
    /**
     * test method to get user record details
     */      
    static testmethod void testGetUserDetail() {
        Test.startTest();
            List<User> userList = SRV_CRM_User.getUserDetail(new Set<Id> {UserInfo.getUserId()});
        Test.stopTest();
        Integer expected = 1;
        Integer actual = userList.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * test method to get field Details of Product Object with Exception
     */      
    static testmethod void testGetUserDetailException() {
        Test.startTest();
            try {
                List<User> userList = SRV_CRM_User.getUserDetail(null);
            } catch(Exception ex) {
                System.assertEquals('SRV_CRM_User.UserServiceException', ex.getTypeName());
            }    
        Test.stopTest();
    }
}