/*
 * Description : Service Class to Send Data to the JIRA REST and Get Data from the JIRA REST
 */
public class SRV_CSM_JiraCallouts {
    /* Call REST API with GET method
     * Returns HttpResponse: 
     * @url - String to url to request
     */
    public static HttpResponse makeGetCallout(String url) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', CON_JIRA.HTTP_REQ_HEADER_AUTHORIZATION);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        req.setEndpoint(url);
        req.setMethod('GET');
        HttpResponse res = h.send(req);
        return res;
    }
    /* Call REST API with POST method
     * Returns HttpResponse: 
     * @url - String to url to request
     * JSONString - Post request parameter
     */
    public static HttpResponse makePostCallout(String url, String JSONString) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', CON_JIRA.HTTP_REQ_HEADER_AUTHORIZATION);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody(JSONString);
        HttpResponse res = h.send(req);
        return res;
    }
    
    /* Call REST API with POST method
     * Returns HttpResponse: 
     * @url - String to url to request
     * JSONString - Post request parameter
     */
    public static HttpResponse makePostAttachmentCallout(String url,String fileName, Blob fileBody) {
        String boundary = '----------------------------741e90d31eff';
		String header = '--' + boundary + '\n' +
		    'Content-Disposition: form-data; name="file"; filename="' + fileName + '";\n' +
		    'Content-Type: application/octet-stream';
		
		String footer = '--' + boundary + '--';
		String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header + '\r\n\r\n'));
		while (headerEncoded.endsWith('=')) {
		    header += ' ';
		    headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
		}
		
		String bodyEncoded = EncodingUtil.base64Encode(fileBody);
		
		Blob bodyBlob = null;
		String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
		
		if (last4Bytes.endsWith('==')) {
		    last4Bytes = last4Bytes.substring(0, 2) + '0K';
		    bodyEncoded = bodyEncoded.substring(0, bodyEncoded.length() - 4) + last4Bytes;
		
		    String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
		    bodyBlob = EncodingUtil.base64Decode(headerEncoded + bodyEncoded + footerEncoded);
		} else if (last4Bytes.endsWith('=')) {
		    last4Bytes = last4Bytes.substring(0, 3) + 'N';
		    bodyEncoded = bodyEncoded.substring(0, bodyEncoded.length() - 4) + last4Bytes;
		    footer = '\n' + footer;
		    String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
		    bodyBlob = EncodingUtil.base64Decode(headerEncoded + bodyEncoded + footerEncoded);
		} else {
		    footer = '\r\n' + footer;
		    String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
		    bodyBlob = EncodingUtil.base64Decode(headerEncoded + bodyEncoded + footerEncoded);
		}
		
		HttpRequest req = new HttpRequest();
		req.setHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
		req.setHeader('Authorization', CON_JIRA.HTTP_REQ_HEADER_AUTHORIZATION);
		req.setHeader('X-Atlassian-Token', 'nocheck');
		req.setMethod('POST');
		req.setEndpoint(url);
		req.setBodyAsBlob(bodyBlob);
		req.setTimeout(120000);
 
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res;
    }
}