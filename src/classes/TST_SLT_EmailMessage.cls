@isTest
private class TST_SLT_EmailMessage {
    static testmethod void testPreventEmailMessageDel(){
        Account acct = new Account(
            Name = 'TestAcc',
            RDCategorization__c = 'Site');
        insert acct;
        
        Contact Con = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='briandent@trailhead.com',
            AccountId = acct.Id);
        insert Con;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=acct.Id,Type = 'TECHNO',
                                          BusinessHoursId = [select id from BusinessHours where Name = 'Default'].Id,
                                          StartDate=Date.valueof(System.now().addDays(-2)), 
                                          EndDate=Date.valueof(System.now().addYears(2)));
        insert ent;
        
        
        User u = [Select id from User where Id = :UserInfo.getUserId() and ProfileId = :UserInfo.getProfileId()];
        
        system.runAs(u) {
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            GroupMember grpUser = new GroupMember (
                UserOrGroupId = u.Id,
                GroupId = g1.Id);
            
            insert grpUser;
            
            Queue_User_Relationship__c qur = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                User__c = grpUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = grpUser.groupId);
            
            insert qur;
            Id RecordTypeIdCaseRD = Schema.SObjectType.case.getRecordTypeInfosByName().get('R&D - Activity Plan Case').getRecordTypeId();
            Case c1 = new Case(
                AccountId = acct.Id,
                ContactId = con.Id,
                Origin = 'Email',
                Status = 'Closed',
                InitialQueue__c = 'group name',
                CurrentQueue__c = qur.Id, 
                OwnerId = qur.Group_Id__c,
                EntitlementId = ent.Id,
                RecordTypeId = RecordTypeIdCaseRD
            );
            Test.startTest();
            insert c1;
            List<EmailMessage> newEmail = new List<EmailMessage>();
            newEmail.add(new EmailMessage(FromAddress = 'test@abc.org', Incoming = false, ToAddress= 'm.reddy@in.imshealth.com', Subject = 'Test email', TextBody = '23456 ', ParentId = c1.Id)); 
            insert newEmail;
            
            try{
                delete newEmail;
            }catch(DMLexception e){
                system.assert(e.getMessage().contains('Cannot delete Email with related to Case'),'Cannot delete Email with related to Case');                       
            }
            Test.stopTest();
        }
    }
}