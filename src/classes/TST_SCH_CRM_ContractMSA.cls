@isTest
public class TST_SCH_CRM_ContractMSA {
    static testMethod void testContractMSAReviewDateAlertBatch(){
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Stage_Gate_Status_Values__c statusvalue = new Stage_Gate_Status_Values__c();
        statusvalue.Name = 'AnalystPP';
        statusvalue.Status__c = 'Analyst Preparing Customer Budget Deliverable';
        upsert statusvalue;
        Contract cntrt = new Contract();
        cntrt.AccountId = acc.Id;
        cntrt.Status = 'Draft';
        cntrt.CurrencyIsoCode = 'INR';
        cntrt.Status = 'Analyst Preparing Customer Budget Deliverable';
        cntrt.StartDate = System.today();
        cntrt.ContractTerm = 12;
        cntrt.Parent_Contract_Number__c = 1234;
        cntrt.Ultimate_Parent_Contract_Number__c = 5678;
        insert cntrt;
        Test.startTest();
        BCH_CRM_ContractMSAReviewDateAlertBatch obj = new BCH_CRM_ContractMSAReviewDateAlertBatch();
        DataBase.executeBatch(obj); 
        Test.stopTest();
        System.assert(true);
    }
}