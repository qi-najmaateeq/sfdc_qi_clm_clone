@isTest
global class CSM_JiraCalloutsHttpResponseMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        if( req.getEndpoint().contains('/comment')){
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"comments":[{"id":"1", "body":"aBody","author":{"displayName":"author"},"updateAuthor":{"displayName":"updateAuthor"},"created":"2019-01-07T08:48:34.791+0100","updated":"2019-01-07T08:48:34.791+0100"}]}');
            res.setStatusCode(200);
        }else{
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"key":"TestJiraKey","fields":{"summary":"test","description":"test","issuetype":{"name":"IssuetypeName"},"status":{"name":"aStatus"},"priority":{"name":"aPriority"},"reporter":{"displayName":"reporter"},"assignee":{"displayName":"assignee"},"fixVersions":[],"customfield_16646":null,"customfield_14510":null,"customfield_14511":null}}');
            res.setStatusCode(200);
        }
        return res;
    }
}
