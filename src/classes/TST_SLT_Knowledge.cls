/*
 * Version       : 1.0
 * Description   : Test Class for SLT_Knowledge
 */
@isTest
public class TST_SLT_Knowledge {
    	/**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
    	Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle', language = 'en_US',UrlName='TestUrlName', IsVisibleInCsp= true);
        insert knowledge;
        Knowledge__kav k = [SELECT Id,KnowledgeArticleId FROM Knowledge__kav WHERE Id=:knowledge.Id];
        KbManagement.PublishingService.publishArticle(k.KnowledgeArticleId, true);
        Topic topic = New Topic(Name = 'TopicsName');
        insert topic;
        TopicAssignment ta = New TopicAssignment(EntityId=knowledge.Id,TopicId=topic.Id);
        insert ta;
    }
    
     /**
     * This method used to get List<Knowledge__kav> by ID
     */    
    @IsTest
    static void testSelectById() {
    	List<Knowledge__kav> knowledges = new  List<Knowledge__kav>();
    	Knowledge__kav k = [SELECT Id FROM Knowledge__kav WHERE Title = 'TestTitle' and PublishStatus='Online' and language='en_US'];	
        Test.startTest();
        knowledges = new SLT_Knowledge().selectById(new Set<Id> {k.Id});
        Test.stopTest();
        Integer expected = 1;
        Integer actual = knowledges.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get a List<Knowledge__kav> by topics
     */    
    @IsTest
    static void testSelectByTopicId() {
        List<Knowledge__kav> articles = new  List<Knowledge__kav>();
        List<Topic> topics =[SELECT Id FROM Topic WHERE Name='TopicsName'];
        Test.startTest();
        articles = new SLT_Knowledge().selectByTopicId(topics[0].Id);  
        Test.stopTest();
        Integer expected = 1;
        Integer actual = articles.size();
        System.assertEquals(expected, actual);
    }
}