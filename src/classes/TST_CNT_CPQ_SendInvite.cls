@isTest
public class TST_CNT_CPQ_SendInvite {
    
    @testSetup
    static void dataSetUp() {
        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        
        Opportunity testOpportunity= UTL_TestData.createOpportunity(testAccount.Id);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        testOpportunity.Legacy_Quintiles_Opportunity_Number__c='MVP123';
        insert testOpportunity;
        
        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = testOpportunity.Id;
        testAgreement.RecordTypeId = RecordTypeId;
        testAgreement.Agreement_Status__c = 'Draft';
        testAgreement.Process_Step__c = 'None';
        testAgreement.Select_Pricing_Tool__c = CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP;
        insert testAgreement;
    }
    
    private static Id getAgreementId() {
        Apttus__APTS_Agreement__c testAgreement = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
        return testAgreement.Id;
    } 
    
    @isTest
    static void testGetEmailTemplate() {
        Id agreementId = getAgreementId();
        
        Test.startTest();
            List<String> emailTemplates = CNT_CPQ_SendInvite.getEmailTemplate(agreementId);
        Test.stopTest();
        
        System.assertEquals(true, emailTemplates.size()>0, 'Should return emailTemplates');
    }
    
    @isTest
    static void testSendInviteEmail() {
        Id agreementId = getAgreementId();
        
         Test.startTest();
            CNT_CPQ_SendInvite.sendInviteEmail('test@test.com','test subject', 'test body', datetime.newInstance(2014, 9, 15, 12, 30, 0), datetime.newInstance(2014, 9, 15, 13, 30, 0));
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
    }
}