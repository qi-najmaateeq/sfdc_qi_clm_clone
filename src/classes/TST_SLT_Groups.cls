/*
* Version       : 1.0
* Description   : This test class is used for Select Groups
*/
@ISTest
private class TST_SLT_Groups {
    @IsTest
    private static void testGetGroupsWithGroupMembersByGroupName() {
        Group grp = new Group();
		grp.name = 'Test Group1';
		grp.Type = 'Regular'; 
		Insert grp; 
        GroupMember grpMem1 = new GroupMember();
        grpMem1.UserOrGroupId = UserInfo.getUserId();
        grpMem1.GroupId = grp.Id;
        Insert grpMem1;
        
        Test.startTest();
            List<Group> groups = new SLT_Groups().getGroupsWithGroupMembersByGroupName(new Set<String>{'Test Group1'});
        Test.stopTest();
        
        System.assertEquals(1, groups.size());
    }
}