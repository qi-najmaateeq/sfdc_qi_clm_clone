global with sharing class CPQ_PricingCallback implements Apttus_Config2.CustomClass.IPricingCallback3 {
/*
CHANGE HISTORY
===============================================================================
Created:        Sean McInturff
Date:           09/26/2018
Description:    PricingCallback class to supliment Apttus functionality in the
                cart.
Updated:        
===============================================================================
*/  

    private Apttus_Config2.CustomClass.PricingMode mode = null;

    Map<String, String> fieldLabelToFieldNameMap = new Map<String, String>();
    Apttus__APTS_Agreement__c agmt;

    // *** START ***
    global void start(Apttus_Config2.ProductConfiguration cart) {
        Id agmtId = cart.getConfigSO().Apttus_CMConfig__AgreementId__c;
        agmt = [Select Id, RecordType.Id, RecordType.DeveloperName, O_Total_Discount__c
                From Apttus__APTS_Agreement__c
                Where Id = :agmtId];
    }

    // *** SET MODE ***
    global void setMode(Apttus_Config2.CustomClass.PricingMode aMode) {
        /*
            === === === === === === === === === === === === === === === === ===
            From Apttus:
            ADJUSTMENT: The mode of the call back when adjustments are made to the cart and you click Reprice.
                        Mode is invoked for all items (including options) in the cart after applying adjustments to compute Net Price.
                        ** ADJUSTMENT mode does not have all option items in Large Cart mode and hence it is preferable not to use this mode.
            ROLLOUT:    The mode of the callback when Apttus_Config2.CustomClass.PricingMode.BASEPRICE == mode)).
                        Mode is invoked to apply the total or group level discounts at the line level.
            BASEPRICE:  The mode is base price when you click Go to Price.
                        Mode is used primarily to set quantity, term and so on in the line item beforePricing() method. 
                        Base price mode is called for each line number.
            === === === === === === === === === === === === === === === === ===
        */
        system.debug('***Inside set mode: '+aMode);
        this.mode = aMode;
    }


    // *** BEFORE PRICING ***
    global void beforePricing(Apttus_Config2.ProductConfiguration.LineItemColl itemColl) {
    /*
    === === === === === === === === === === === === === === === === ===
    From Apttus:
    Using this method, you can define the logic for net price calculation. 
    In beforePricing, no pricelist item is associated to the cart line items. 
    Price Matrix and Price Ruleset calculation occurs when the mode of the callback is Base Price Mode.
    Any modification to **custom fields** need to be done in the beforePricing method.
    === === === === === === === === === === === === === === === === ===
    */
        
        system.debug('***Inside before pricing: ');
        system.debug('***Mode ====================>: '+Mode);

        if (mode == Apttus_Config2.CustomClass.PricingMode.BasePrice)
        {
            if (agmt.RecordType.DeveloperName == 'Subscription_Products' &&
                agmt.O_Total_Discount__c > 0) {
                // Get line item Ids
                List<Id> lineItemIds = new List<Id>();
                for(Apttus_Config2.LineItem configLineItem :itemColl.getAllLineItems()) {
                    lineItemIds.add(configLineItem.getLineItemSO().Id);
                }
/*
                for(Apttus_Config2.LineItem configLineItem :itemColl.getAllLineItems()) {
                    Apttus_Config2__LineItem__c li = configLineItem.getLineItemSO();
                    if (li.Apttus_Config2__LineType__c == 'Product/Service' &&
                            li.Apttus_Config2__ChargeType__c == 'Subscription Fee') {
                        li.Apttus_Config2__AdjustmentType__c = '% Discount';
                        li.Apttus_Config2__AdjustmentAmount__c = agmt.O_Total_Discount__c;
                    }
                }
*/

                // Query existing % Discount adjustment line items for the line items in the cart
                List<Apttus_Config2__AdjustmentLineItem__c> existingAdjustments =
                    [Select Id, Apttus_Config2__LineItemId__c, Apttus_Config2__AdjustmentAmount__c
                     From Apttus_Config2__AdjustmentLineItem__c
                     Where Apttus_Config2__LineItemId__c in :lineItemIds
                     And Apttus_Config2__AdjustmentType__c = '% Discount'];
                // Create a Map of the existing adjustments by Line Item Id
                Map<Id,Apttus_Config2__AdjustmentLineItem__c> existingAdjustmentsMap =
                    new Map<Id,Apttus_Config2__AdjustmentLineItem__c>();
                for (Apttus_Config2__AdjustmentLineItem__c existingAdjustment: existingAdjustments) {
                    existingAdjustmentsMap.put(existingAdjustment.Apttus_Config2__LineItemId__c, existingAdjustment);
                }

                // Create Adjustment Line Items or update the % Discount for existing ones as needed
                List<Apttus_Config2__AdjustmentLineItem__c> adjustments = new List<Apttus_Config2__AdjustmentLineItem__c>();
                List<Apttus_Config2__LineItem__c> linesToUpdate = new List<Apttus_Config2__LineItem__c>();
                for(Apttus_Config2.LineItem configLineItem :itemColl.getAllLineItems()) {
                    Apttus_Config2__LineItem__c li = configLineItem.getLineItemSO();
                    if (li.Apttus_Config2__LineType__c == 'Product/Service' && 
                            li.Apttus_Config2__ChargeType__c == 'Subscription Fee') {
                        if(!existingAdjustmentsMap.containsKey(li.Id)) {
                            Apttus_Config2__AdjustmentLineItem__c adjustment = new Apttus_Config2__AdjustmentLineItem__c();
                            adjustment.Apttus_Config2__AdjustmentType__c = '% Discount';
                            adjustment.Apttus_Config2__AdjustmentAmount__c = agmt.O_Total_Discount__c;
                            adjustment.Apttus_Config2__LineItemId__c = li.Id;
                            adjustment.Apttus_Config2__LineNumber__c = 1;
                            li.Apttus_Config2__PricingStatus__c = 'Pending';
                            linesToUpdate.add(li);
                            adjustments.add(adjustment);
                        } else {
                            Apttus_Config2__AdjustmentLineItem__c adjustment = existingAdjustmentsMap.get(li.Id);
                            if (adjustment.Apttus_Config2__AdjustmentAmount__c != agmt.O_Total_Discount__c) {
                                adjustment.Apttus_Config2__AdjustmentAmount__c = agmt.O_Total_Discount__c;
                                li.Apttus_Config2__PricingStatus__c = 'Pending';
                                linesToUpdate.add(li);
                                adjustments.add(adjustment);
                            }
                        }
                    }
                }
                upsert adjustments;
                update linesToUpdate;

            }
        }
    }

    global void onPriceItemSet(Apttus_Config2__PriceListItem__c itemSO, Apttus_Config2.LineItem lineItemMO){
        System.debug('***Inside onPriceItemSet - Mode: ' + Mode);
    }

    global void beforePricingLineItem(Apttus_Config2.ProductConfiguration.LineItemColl itemColl, Apttus_Config2.LineItem lineItemMO) {
        System.debug('***Inside beforePricingLineItem ');
        System.debug('***Mode ====================>: '+Mode);
    }

    global void afterPricing(Apttus_Config2.ProductConfiguration.LineItemColl itemColl) {
    /*
    === === === === === === === === === === === === === === === === ===
    From Apttus:
    In this method, define the logic to be executed after Net Price calculation, 
    that is after the system has already calculated Net Price when this method is invoked.
    === === === === === === === === === === === === === === === === ===
    */
        system.debug('***Inside before pricing: ');
        system.debug('***Mode ====================>: '+Mode);
    }

    global void afterPricingLineItem(Apttus_Config2.ProductConfiguration.LineItemColl itemColl,Apttus_Config2.LineItem lineItemMO) {
        System.debug('***inside after pricing line item: ');
    }

    global void finish() {      
        System.debug('***inside finish');
    }
}