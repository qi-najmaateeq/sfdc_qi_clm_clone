/**
* This is Project(pse__Proj__c) trigger handler class.
* version : 1.0
*/
public class DAOH_OWF_Proj {
    
    /**
    * This method is used to create Resource Request (pse__Resource_Request__c) record on insert of a Bid type Project.
    * @params  newList List<pse__Proj__c>
    * @return  
    */
    public static void createRRBasedOnAgrAssociatedWithProj(List<pse__Proj__c> newList) {
        List<pse__Resource_Request__c> rrToBeInsertedList = new List<pse__Resource_Request__c>();
        rrToBeInsertedList = returnRRBasedOnAgrAssociatedWithProj(newList);
        if(rrToBeInsertedList.size() > 0) {
            insert rrToBeInsertedList;
        }
    }
    
    /**
    * This method is used to delete Resource Request record based on Project.
    * @params  newList List<pse__Proj__c>
    * @return  
    */
    public static void deleteResourceRequestBasedOnProject(List<pse__Proj__c> newList) {
        List<pse__Resource_Request__c> resourceRequesToBeDeletedList = new List<pse__Resource_Request__c>();
        resourceRequesToBeDeletedList = returnResourceRequestBasedOnProject(newList);
        if(resourceRequesToBeDeletedList.size() > 0) {
            QBL_OWF_DeletionLogic.QueueResourceRequestDeletion  queuableObject = new QBL_OWF_DeletionLogic.QueueResourceRequestDeletion (resourceRequesToBeDeletedList);
            System.enqueueJob(queuableObject);
        }
    }
    
    /**
    * This method is used to create Resource Request (pse__Resource_Request__c) record on insert of Project's Bid_Number.
    * @params  newList List<pse__Proj__c>
    * @return  
    */
    public static void createClinicalBidRRsBasedOnBidNoOfProjects(List<pse__Proj__c> newList) {
        List<pse__Resource_Request__c> rrToBeInsertedList = new List<pse__Resource_Request__c>();
        Set<Id> projectOppIdsSet = new Set<Id>();
        Map<Id, List<pse__Proj__c>> oppIdToReBidsProjectsMap = new Map<Id, List<pse__Proj__c>>();
        Map<Id, pse__Proj__c> initialAgrmtIdToProjectMap = new Map<Id, pse__Proj__c>();
        for(pse__Proj__c project : newList) {
            if(project.Agreement__c != NULL && project.pse__Opportunity__c != NULL && project.Bid_Number__c >= 1) {
                projectOppIdsSet.add(project.pse__Opportunity__c);
                //Prepar a map b/w OppId and list of projects
                if(project.Bid_Number__c > 1) {
                    if(!oppIdToReBidsProjectsMap.containsKey(project.pse__Opportunity__c)) {
                        oppIdToReBidsProjectsMap.put(project.pse__Opportunity__c, new List<pse__Proj__c>());
                    }
                    oppIdToReBidsProjectsMap.get(project.pse__Opportunity__c).add(project);
                }else {
                    initialAgrmtIdToProjectMap.put(project.Agreement__c, project);
                }
            }
        }
        
        if(projectOppIdsSet.size() > 0) {
            String agrCondition = 'Apttus__Related_Opportunity__c != NULL And Apttus__Related_Opportunity__c IN :sObjectIdSet ' +
                                    ' And RecordTypeId = \'' + CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID + '\'';
            
            List<pse__Resource_Request__c> reqRequestsInsertList = new List<pse__Resource_Request__c>();
            rrToBeInsertedList = UTL_OWF.processResRequestForInitialBids(projectOppIdsSet, agrCondition, initialAgrmtIdToProjectMap, oppIdToReBidsProjectsMap);
            if(rrToBeInsertedList.size() > 0) {
                insert rrToBeInsertedList;
            }    
        }
    }
        
    /**
    * This method is used to set Number of Requested Services based on selected Requested Services.
    * @params  newList List<pse__Proj__c>
    * @return  rrList List<pse__Resource_Request__c>
    */
    private static List<pse__Resource_Request__c> returnRRBasedOnAgrAssociatedWithProj(List<pse__Proj__c> newList) {
        Set<Id> agreementIdSet = new Set<Id>();
        Map<pse__Proj__c,Id> projectToAgreementMap = new Map<pse__Proj__c,Id>();
        Map<Id, Apttus__APTS_Agreement__c> agrIdToAgreementMap = new Map<Id, Apttus__APTS_Agreement__c>();
        Set<String> fieldSet = new Set<String>{'Id', 'Resources_Needed__c','RecordTypeId','Apttus__Account__c','Bid_Due_Date__c'};
        List<pse__Resource_Request__c> rrList = new List<pse__Resource_Request__c>();
        for(pse__Proj__c proj : newList) {
            if(proj.Agreement__c != null) {
                projectToAgreementMap.put(proj,proj.Agreement__c);
            }   
        }
        if(projectToAgreementMap.size() > 0) {
            agreementIdSet.addAll(projectToAgreementMap.values());       
            agrIdToAgreementMap = new SLT_APTS_Agreement(false,false).getAgreementsById(agreementIdSet, fieldSet);
            Map<String, OWF_Resources_Needed_to_SubGroup_Map__c> resourcesNeededToSubGrpMap = new Map<String,OWF_Resources_Needed_to_SubGroup_Map__c>();
            resourcesNeededToSubGrpMap = OWF_Resources_Needed_to_SubGroup_Map__c.getAll();
            if(agrIdToAgreementMap.size() > 0) {
                for(pse__Proj__c proj : projectToAgreementMap.keySet()) {
                    if(agrIdToAgreementMap.containsKey(proj.Agreement__c) && 
                       CON_OWF.RESOURCES_NEEEDED_FIELD_BASED_RR_AGR_RECORDTYPE_SET.contains(
                           agrIdToAgreementMap.get(proj.Agreement__c).RecordTypeId) &&
                       agrIdToAgreementMap.get(proj.Agreement__c).Resources_Needed__c != '' &&
                       agrIdToAgreementMap.get(proj.Agreement__c).Resources_Needed__c != null
                    ) {
                        List<String> resourcesNeededList = new List<String>();
                        resourcesNeededList = agrIdToAgreementMap.get(proj.Agreement__c).Resources_Needed__c.split(';');
                        for(String resourceNeeded : resourcesNeededList) {
                            
                            pse__Resource_Request__c newRR = new pse__Resource_Request__c(
                                                         recordTypeId = CON_OWF.OWF_RR_RECORD_TYPE_ID,
                                                         currencyIsoCode = agrIdToAgreementMap.get(proj.Agreement__c).currencyIsoCode,
                                                         pse__Project__c = proj.id,
                                                         pse__Opportunity__c = proj.pse__Opportunity__c,
                                                         pse__Group__c = proj.pse__Group__c,
                                                         Account__c = agrIdToAgreementMap.get(proj.Agreement__c).Apttus__Account__c,
                                                         Agreement__c = proj.Agreement__c,
                                                         SubGroup__c = resourcesNeededToSubGrpMap.containsKey(resourceNeeded)?
                                                                        resourcesNeededToSubGrpMap.get(resourceNeeded).Sub_Group__c:'',
                                                         pse__Start_Date__c = Date.Today(),
                                                         pse__End_Date__c = agrIdToAgreementMap.get(proj.Agreement__c).Bid_Due_Date__c,
                                                         pse__SOW_Hours__c = 0.01
                                                         );    
                            rrList.add(newRR);   
                        }                        
                    }
                    else if(agrIdToAgreementMap.containsKey(proj.Agreement__c) && 
                               agrIdToAgreementMap.get(proj.Agreement__c).RecordTypeId ==
                                CON_OWF.OWF_RFI_AGREEMENT_RECORD_TYPE_ID 
                           ) {
                                  pse__Resource_Request__c newRR = new pse__Resource_Request__c(
                                                         recordTypeId = CON_OWF.OWF_RR_RECORD_TYPE_ID,
                                                         currencyIsoCode = agrIdToAgreementMap.get(proj.Agreement__c).currencyIsoCode,
                                                         pse__Project__c = proj.id,
                                                         pse__Opportunity__c = proj.pse__Opportunity__c,
                                                         pse__Group__c = proj.pse__Group__c,
                                                         Account__c = agrIdToAgreementMap.get(proj.Agreement__c).Apttus__Account__c,
                                                         Bid_Category__c = CON_OWF.RFI_RR,
                                                         Agreement__c = proj.Agreement__c,
                                                         SubGroup__c = CON_OWF.RFI_SUBGROUP,
                                                         pse__Start_Date__c = Date.Today(),
                                                         pse__End_Date__c = agrIdToAgreementMap.get(proj.Agreement__c).Bid_Due_Date__c,
                                                         pse__SOW_Hours__c = 0.01
                                                         );    
                            rrList.add(newRR);     
                           }
                }
            }
        }  
        return rrList;
    }
    
    private static List<pse__Resource_Request__c> returnResourceRequestBasedOnProject(List<pse__Proj__c> newList) {
        List<pse__Resource_Request__c> resourceRequestToBeReturnedList = new List<pse__Resource_Request__c>();
         Set<String> resourceRequestFieldSet = new Set<String>{'Id'};
        Map<Id, pse__Proj__c> projectIdtoProjectMap= new Map<Id, pse__Proj__c>();
        for(pse__Proj__c project : newList)
        {
             projectIdtoProjectMap.put(project.ID, project );
        }
        
        Map<Id, pse__Resource_Request__c> rrIdToResourceRequestMap = new SLT_Resource_Request(false,false).getResourceRequestByProjectID(projectIdtoProjectMap.keySet(),resourceRequestFieldSet );
        resourceRequestToBeReturnedList  = rrIdToResourceRequestMap .values();
        return resourceRequestToBeReturnedList;
    } 
    
    public static void checkExistingProjectForAgreement(List<pse__Proj__c> newList){
        system.debug('newList-->' + newList);
        Set<Id> agreementIdSet = new Set<Id>();
        for(pse__Proj__c project : newList){
            if(project.recordTypeId == CON_OWF.OWF_BID_PROJECT_RECORD_TYPE_ID && project.Agreement__c != null){
                agreementIdSet.add(project.Agreement__c);
            } 
        }
        if(agreementIdSet.size() > 0){
            List<pse__Proj__c> projectList = [Select id, Agreement__c from pse__Proj__c where Agreement__c IN: agreementIdSet];
            Map<Id,pse__Proj__c> agreementIdToProjectMap = new Map<Id,pse__Proj__c>();
            if(projectList.size() > 0){
                for(pse__Proj__c project : projectList){
                    agreementIdToProjectMap.put(project.Agreement__c,project);
                }
                for(pse__Proj__c project : newList){
                    if(agreementIdToProjectMap.containsKey(project.Agreement__c)){
                        project.addError('Project for this Agreement already Exists with record Id: ' + agreementIdToProjectMap.get(project.Agreement__c).Id);
                    }
                }
            }
        }
    }
}
