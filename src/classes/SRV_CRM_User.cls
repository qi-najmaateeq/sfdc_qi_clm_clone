/*
 * Version       : 1.0
 * Description   : Service Class for User Selector class
 */
global class SRV_CRM_User {
    
    /**
     * service method to get user records
     * @params  Set<Id> userIdsSet
     * @return  List<User>
     */
    public static List<User> getUserDetail(Set<Id> userIds) {
        List<User> userList = new List<User>();
        try {
            userList = new SLT_User().selectById(userIds);
        } catch(Exception ex) {
            throw new UserServiceException(new List<String>{ex.getMessage()});
        }   
        return userList;
    }
    
    // Exception Class for Product Service
    public Class UserServiceException extends Exception {
        
        List<String> errorList;
        
        /**
         * constructor
         * @params  List<String> errorList
         */ 
        public UserServiceException(List<String> errorList) {
            this.errorList = errorList;
        }
    }
}