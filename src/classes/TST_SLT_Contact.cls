/**
* This test class is used to test all methods in opportunity trigger helper.
* version : 1.0
*/
@isTest
private class TST_SLT_Contact {
    
    /**
    * This method is used to setup data for all methods.
    */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        cont.email = 'test@test.com';
        insert cont;
    }
    
    /**
    * This test method used to get Contact by Id 
    */ 
    static testMethod void testSelectByContactId() {
        Contact contact = [SELECT id from Contact][0];
        Map<Id, Contact> idToContactMap = new SLT_Contact().selectByContactId(new Set<Id>{contact.Id}, new Set<String>{'Id'});
    }  
    
    static testMethod void testSelectByContactIdList() {
        Contact contact = [SELECT id from Contact][0];
        List<Contact> contactList = new SLT_Contact().selectByContactIdList(new Set<Id>{contact.Id}, new Set<String>{'Id'});
    }
    
    static testMethod void testSelectByEmail() {
        Contact contact = new SLT_Contact(false, false).selectByEmail('test@test.com');
    }
    
    static testMethod void testSelectByMobile() {
        Map<Id, Contact> idToContactMap = new SLT_Contact().selectByMobile('123456789');
    }
    
    static testMethod void testSelectBySalesforceUserIdList() {
        Contact contact = [SELECT id from Contact][0];
        List<Contact> contactList = new SLT_Contact().selectBySalesforceUserIdList(new Set<Id>{contact.Id}, new Set<String>{'Id'});
    }
    
    static testMethod void testSelectByEmailIdList() {
        List<Contact> contactList = new SLT_Contact().selectByEmailIdList(new Set<String> {'test@test.com'});
    }
    
    static testMethod void testSelectByPseSalesforceUserIdList() {
        Contact contact = [SELECT id from Contact][0];
        List<Contact> contactList = new SLT_Contact().selectByPseSalesforceUserIdList(new Set<Id>{contact.Id}, new Set<String>{'Id'});
    }
    
    static testMethod void testSelectContactByLiContactId() {
        Contact contact = [SELECT id from Contact][0];
        List<Contact> contactList = new SLT_Contact().selectContactByLiContactId(new Set<Id>{contact.Id}, new Set<String>{'Id'});
    }
    
    static testMethod void testGetContactByUserEmployeeNumber() {
        List<Contact> contactList = new SLT_Contact().getContactByUserEmployeeNumber('123456789');
    }
    
    static testMethod void testSelectByContactCondition() {
        Contact contact = [SELECT id from Contact][0];
        Map<Id, Contact> idToContactMap = new SLT_Contact().selectByContactCondition(new Set<Id>{contact.Id},' pse__Is_Resource__c = true', new Set<String>{'Id'});
    }

}