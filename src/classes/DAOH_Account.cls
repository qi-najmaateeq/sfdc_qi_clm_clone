public class DAOH_Account {
    /**
    * This method is used for create a new Data Audit Trail when the new case record is created.
    * @params  newList List<Account>
    * @return  void
    */
    public static void saveAuditLogAfterInsertAccount(List<Account> newList){
        CSM_QI_Data_Audit_Trail__c auditTrail = null;
        Map<ID,Schema.RecordTypeInfo> rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
        List<CSM_QI_Data_Audit_Trail__c> auditTrailList = new List<CSM_QI_Data_Audit_Trail__c>();
        if(newList!=null && newList.size()>0){
            for(Account acc : newList) {
                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_CREATED,Name = CON_CSM.S_ACCOUNT,Account__c = acc.Id);
                auditTrailList.add(auditTrail);
            }  
        } 
        
        
        try {
            if(auditTrailList!=null && auditTrailList.size()>0){
                insert auditTrailList;
            }
        } catch (DmlException e) {
            System.debug('Failed due to : '+e);
        }
        
    }
    /**
    * This method is used for create a new Data Audit Trail when the case record fields are updated.
    * @params  newList List<Account>,oldMap Map<Id, Account> 
    * @return  void
    */
    public static void saveAuditLogAfterUpdateAccountFields(List<Account> newList, Map<Id, Account> oldMap,List<FieldDefinition> fields) {
        CSM_QI_Data_Audit_Trail__c auditTrail=null;
        List<CSM_QI_Data_Audit_Trail__c> auditTrailList = new List<CSM_QI_Data_Audit_Trail__c>();
        Map<ID,Schema.RecordTypeInfo> rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
        EXT_CSM_CaseRelatedToObject accRelatedTo=null;
        List<EXT_CSM_CaseRelatedToObject> accRelatedToList=new List<EXT_CSM_CaseRelatedToObject>(); 
        if(fields!=null && fields.size()>0){
            for(Account acc : newList) {
                if(fields != null && fields.size() > 0 ) 
                {
                    for( FieldDefinition fd : fields){
                        
                        if(String.isBlank(fd.ExtraTypeInfo) && ((fd.DataType.contains(CON_CSM.S_TEXT) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_PICKLIST) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_NUMBER) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_DOUBLE)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_URL) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_PHONE) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_EMAIL) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_CHECKBOX) && fd.ValueTypeId.equalsIgnoreCase(CON_CSM.S_BOOLEAN)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_FORMULA) && fd.ValueTypeId.equalsIgnoreCase(CON_CSM.S_TEXT)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_DATE) && fd.ValueTypeId.equalsIgnoreCase(CON_CSM.S_DATETIME)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_DATE) && fd.ValueTypeId.equalsIgnoreCase(CON_CSM.S_DATE)))){
                            if(acc.get(fd.QualifiedApiName) == null && oldMap.get(acc.Id).get(fd.QualifiedApiName) != null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_DELETED,Name = fd.MasterLabel,Old_Value__c = String.valueOf(oldMap.get(acc.Id).get(fd.QualifiedApiName)),New_Value__c = String.valueOf(acc.get(fd.QualifiedApiName)),Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.get(fd.QualifiedApiName) != null && oldMap.get(acc.Id).get(fd.QualifiedApiName)==null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c=CON_CSM.S_ADDED,Name = fd.MasterLabel,Old_Value__c = String.valueOf(oldMap.get(acc.Id).get(fd.QualifiedApiName)),New_Value__c = String.valueOf(acc.get(fd.QualifiedApiName)),Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.get(fd.QualifiedApiName)!=null && !acc.get(fd.QualifiedApiName).equals(oldMap.get(acc.Id).get(fd.QualifiedApiName))){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c=CON_CSM.S_CHANGED,Name = fd.MasterLabel,Old_Value__c = String.valueOf(oldMap.get(acc.Id).get(fd.QualifiedApiName)),New_Value__c = String.valueOf(acc.get(fd.QualifiedApiName)),Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }
                        }else if(String.isBlank(fd.ExtraTypeInfo) && (fd.DataType.contains(CON_CSM.S_LOOKUP) || fd.DataType.contains(CON_CSM.S_HIERARCHY)) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_ID)){
                            if(acc.get(fd.QualifiedApiName) == null && oldMap.get(acc.Id).get(fd.QualifiedApiName) != null){
                                accRelatedTo = new EXT_CSM_CaseRelatedToObject(CON_CSM.S_DELETED,fd.RelationshipName,CON_CSM.S_ACCOUNT,fd.MasterLabel,(Id)oldMap.get(acc.Id).get(fd.QualifiedApiName),null,acc.Id);
                                accRelatedToList.add(accRelatedTo);
                            }else if(acc.get(fd.QualifiedApiName) != null && oldMap.get(acc.Id).get(fd.QualifiedApiName) == null){
                                accRelatedTo=new EXT_CSM_CaseRelatedToObject(CON_CSM.S_ADDED,fd.RelationshipName,CON_CSM.S_ACCOUNT,fd.MasterLabel,null,(Id)acc.get(fd.QualifiedApiName),acc.Id);
                                accRelatedToList.add(accRelatedTo);    
                            }else if(acc.get(fd.QualifiedApiName) != null && oldMap.get(acc.Id).get(fd.QualifiedApiName) != null && !acc.get(fd.QualifiedApiName).equals(oldMap.get(acc.Id).get(fd.QualifiedApiName))){
                                accRelatedTo = new EXT_CSM_CaseRelatedToObject(CON_CSM.S_CHANGED,fd.RelationshipName,CON_CSM.S_ACCOUNT,fd.MasterLabel,(Id)oldMap.get(acc.Id).get(fd.QualifiedApiName),(Id)acc.get(fd.QualifiedApiName),acc.Id);
                                accRelatedToList.add(accRelatedTo);
                            }
                        }else if(String.isNotBlank(fd.ExtraTypeInfo) && fd.DataType.contains(CON_CSM.S_TEXT) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)){
                            if(acc.get(fd.QualifiedApiName) != null && !acc.get(fd.QualifiedApiName).equals(oldMap.get(acc.Id).get(fd.QualifiedApiName))){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_EDITED,Name = fd.MasterLabel,Old_Text_Value__c = String.valueOf(oldMap.get(acc.Id).get(fd.QualifiedApiName)),Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }
                        }else if(String.isBlank(fd.ExtraTypeInfo) && fd.QualifiedApiName.equals(CON_CSM.S_BILLINGADDRESS) && fd.DataType.contains(CON_CSM.S_ADDRESS)){
                            
                            if(acc.BillingStreet==null && oldMap.get(acc.Id).BillingStreet!=null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_EDITED,Name = CON_CSM.S_A_BSTREET,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingStreet!=null && oldMap.get(acc.Id).BillingStreet==null){
                                auditTrail=new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_EDITED,Name = CON_CSM.S_A_BSTREET,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingStreet!=null && oldMap.get(acc.Id).BillingStreet!=null && !acc.BillingStreet.equalsIgnoreCase(oldMap.get(acc.Id).BillingStreet)){
                                auditTrail=new CSM_QI_Data_Audit_Trail__c(Action__c=CON_CSM.S_EDITED,Name=CON_CSM.S_A_BSTREET,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);    
                            }
                            if(acc.BillingCity == null && oldMap.get(acc.Id).BillingCity != null){
                                auditTrail=new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_DELETED,Name = CON_CSM.S_A_BCITY,Old_Value__c = oldMap.get(acc.Id).BillingCity,New_Value__c = acc.BillingCity,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingCity!=null && oldMap.get(acc.Id).BillingCity==null){
                                auditTrail=new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_ADDED,Name = CON_CSM.S_A_BCITY,Old_Value__c = oldMap.get(acc.Id).BillingCity,New_Value__c = acc.BillingCity,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingCity != null && oldMap.get(acc.Id).BillingCity != null && !acc.BillingCity.equalsIgnoreCase(oldMap.get(acc.Id).BillingCity)){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_CHANGED,Name = CON_CSM.S_A_BCITY,Old_Value__c = oldMap.get(acc.Id).BillingCity,New_Value__c = acc.BillingCity,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }
                            
                            if(acc.BillingState == null && oldMap.get(acc.Id).BillingState != null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_DELETED,Name = CON_CSM.S_A_BSTATE,Old_Value__c = oldMap.get(acc.Id).BillingState,New_Value__c = acc.BillingState,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingState != null && oldMap.get(acc.Id).BillingState == null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_ADDED,Name = CON_CSM.S_A_BSTATE,Old_Value__c = oldMap.get(acc.Id).BillingState,New_Value__c = acc.BillingState,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingState != null && oldMap.get(acc.Id).BillingState != null && !acc.BillingState.equalsIgnoreCase(oldMap.get(acc.Id).BillingState)){
                                auditTrail=new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_CHANGED,Name = CON_CSM.S_A_BSTATE,Old_Value__c = oldMap.get(acc.Id).BillingState,New_Value__c = acc.BillingState,Account__c = acc.Id);
                                auditTrailList.add(auditTrail); 
                            }
                            if(acc.BillingPostalCode == null && oldMap.get(acc.Id).BillingPostalCode != null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_DELETED,Name = CON_CSM.S_A_BPOSTALCODE,Old_Value__c = oldMap.get(acc.Id).BillingPostalCode,New_Value__c = acc.BillingPostalCode,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingPostalCode != null && oldMap.get(acc.Id).BillingPostalCode == null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_ADDED,Name = CON_CSM.S_A_BPOSTALCODE,Old_Value__c = oldMap.get(acc.Id).BillingPostalCode,New_Value__c = acc.BillingPostalCode,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingPostalCode != null && oldMap.get(acc.Id).BillingPostalCode != null && !acc.BillingPostalCode.equalsIgnoreCase(oldMap.get(acc.Id).BillingPostalCode)){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_CHANGED,Name = CON_CSM.S_A_BPOSTALCODE,Old_Value__c = oldMap.get(acc.Id).BillingPostalCode,New_Value__c = acc.BillingPostalCode,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }
                            if(acc.BillingCountry == null && oldMap.get(acc.Id).BillingCountry != null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_DELETED,Name = CON_CSM.S_A_BCOUNTRY,Old_Value__c = oldMap.get(acc.Id).BillingCountry,New_Value__c = acc.BillingCountry,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingCountry != null && oldMap.get(acc.Id).BillingCountry == null){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c=CON_CSM.S_ADDED,Name=CON_CSM.S_A_BCOUNTRY,Old_Value__c = oldMap.get(acc.Id).BillingCountry,New_Value__c = acc.BillingCountry,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }else if(acc.BillingCountry != null && oldMap.get(acc.Id).BillingCountry != null && !acc.BillingCountry.equalsIgnoreCase(oldMap.get(acc.Id).BillingCountry)){
                                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_CHANGED,Name = CON_CSM.S_A_BCOUNTRY,Old_Value__c = oldMap.get(acc.Id).BillingCountry,New_Value__c = acc.BillingCountry,Account__c = acc.Id);
                                auditTrailList.add(auditTrail);
                            }
                        }
                    }
                }
            }
            
            if(accRelatedToList!=null && accRelatedToList.size()>0)
            {
                Set<Id> pAccSet = new Set<Id>(),usrSet = new Set<Id>();
                
                for(EXT_CSM_CaseRelatedToObject obj:accRelatedToList){
                    if(CON_CSM.S_ACCOUNT.equals(obj.objRelName)){
                        pAccSet.add(obj.oldId);
                        pAccSet.add(obj.newId);
                    }else if(CON_CSM.S_USER.equals(obj.objRelName)){
                        usrSet.add(obj.oldId);
                        usrSet.add(obj.newId);
                    }
                }
                Set<String> fieldSet = new Set<String> {CON_CSM.s_id, CON_CSM.s_name};
                Map<Id, Account> parntaccountMap =null;
                Map<Id, User> usrMap=null;
                if(pAccSet.size() > 0) parntaccountMap = new SLT_Account().selectByAccountId(pAccSet, fieldSet);
                if(usrSet.size() > 0) usrMap = new SLT_User().selectByUserId(usrSet, fieldSet);
                for(EXT_CSM_CaseRelatedToObject obj : accRelatedToList){
                    if(parntaccountMap != null && parntaccountMap.size() > 0 && CON_CSM.S_ACCOUNT.equals(obj.objRelName)){
                        auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = obj.action,Name = obj.fieldName,Account__c = obj.objectId,Old_Value__c = parntaccountMap.containsKey(obj.oldId) ? parntaccountMap.get(obj.oldId).Name : '',New_Value__c = parntaccountMap.containsKey(obj.newId) ? parntaccountMap.get(obj.newId).Name : '');
                        auditTrailList.add(auditTrail);
                    }else if(usrMap != null && usrMap.size()>0 && CON_CSM.S_USER.equals(obj.objRelName)){
                        auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = obj.action,Name = obj.fieldName,Account__c = obj.objectId,Old_Value__c = usrMap.containsKey(obj.oldId) ? usrMap.get(obj.oldId).Name : '',New_Value__c = usrMap.containsKey(obj.newId) ? usrMap.get(obj.newId).Name : ''); 
                        auditTrailList.add(auditTrail);
                    } 
                    
                }
            }
            
            
            try {
                if(auditTrailList != null && auditTrailList.size()>0){
                    insert auditTrailList;     
                }
                
            } catch (DmlException e) {
                System.debug('Failed due to : '+e);
            }
        }
    }
    
    /**
     * This method is used to prevent deleteion of accounts by Non-Admin users
     * @params  List<Account> deleteList
     * @return  void
     */
    public static void preventDeletionOfAccounts(List<Account> deleteList) {
        Set<Id> idSet = new Set<Id>{UserInfo.getUserId()};
        Set<String> fieldSet = new Set<String>{'Profile.Name'};
        Map<Id, User> idUserMap = new SLT_User().selectByUserId(idSet, fieldSet);
        String profileName = idUserMap.get(UserInfo.getUserId()).Profile.Name;
        for (Account acc : deleteList) {
            if(acc.RecordTypeId == CON_CRM.GLOBAL_CUSTOMER_ACCOUNT_RECORD_TYPE_ID && !profileName.containsIgnoreCase(CON_CRM.SYSTEM_ADMIN_PROFILE)) {
                acc.addError(SYSTEM.LABEL.CRM_CL0020_GLOBAL_CUSTOMER_ACCOUNT_DELETION_ERROR);
            }
            if(acc.RecordTypeId == CON_CRM.MDM_VALIDATED_ACCOUNT_RECORD_TYPE_ID && !profileName.containsIgnoreCase(CON_CRM.SYSTEM_ADMIN_PROFILE)) {
                acc.addError(SYSTEM.LABEL.CRM_CL0021_MDM_VALIDATED_DELETION_ERROR);
            }
        }
    }
    
    /**
     * This method is used to change aaccount feilds.
     * @params  newList List<Account>
     * @params  oldMap Map<Id, Account>
     * @return  void
     */
    public static void setAccountFields(List<Account> newList, Map<Id, Account> oldMap) {
        Map<String,IQVIA_Account_Country_Region_Mapping__mdt> accountCountryRegionMap = new Map<String,IQVIA_Account_Country_Region_Mapping__mdt>();
        accountCountryRegionMap = new SLT_IQVIAAccountCountryRegionMapping().getaccountCountryToRegionMetadataMap();
        for (Account acc : newList) {  
            if (Trigger.isInsert || (Trigger.isUpdate && oldMap.get(acc.id).AccountCountry__c != acc.AccountCountry__c)) {
                if(!String.isBlank(acc.AccountCountry__c) && accountCountryRegionMap.containsKey(acc.AccountCountry__c)) {
                    acc.Region__c = accountCountryRegionMap.get(acc.AccountCountry__c).Region__c;
                }
                else if(String.isBlank(acc.AccountCountry__c) && !String.isBlank(acc.Region__c)) {
                    acc.Region__c = null;
                }   
            }
        }
    }
    
    /**
     * This method is used to merge duplicate accounts
     * @params  newMap Map<Id, Account>
     * @params  oldMap Map<Id, Account>
     * @return  void
     */
    public static void mergeAccounts(Map<Id, Account> newMap, Map<Id, Account> oldMap) {
        //Set<Id> sfdcGoldenRecordIdSet = new Set<Id>();
        Map<Id, List<Account>> sfdcGoldenIdToDupeAccountListMap = new Map<Id, List<Account>>();
        Set<String> excludeFieldSet = new Set<String>{'LQ_Account_Id__c', 'LI_Account_Id__c', 'MDM_SFDC_Golden_Record_Id__c', 'Legacy_Id__c'};
        for (Account dupeAccount : newMap.values()) {
            if(dupeAccount.MDM_Validation_Status__c != oldMap.get(dupeAccount.Id).MDM_Validation_Status__c && dupeAccount.MDM_Validation_Status__c == 'Rejected' && !String.isBlank(dupeAccount.MDM_SFDC_Golden_Record_Id__c) && dupeAccount.RecordTypeId != CON_CRM.GLOBAL_ACCOUNT_RECORD_TYPE_ID) {
                //Check that SurvivorAccount has valid LI_account_id__C and Valid LQ_Account_id__C
                // Check SurvivorAccount is not Global Customer
                //  Check that MDM_SFDC_Golden_Record_Id__c is a valid account id
                //sfdcGoldenRecordIdSet.add(dupeAccount.MDM_SFDC_Golden_Record_Id__c);
                if(sfdcGoldenIdToDupeAccountListMap.containsKey(dupeAccount.MDM_SFDC_Golden_Record_Id__c)) {
                    sfdcGoldenIdToDupeAccountListMap.get(dupeAccount.MDM_SFDC_Golden_Record_Id__c).add(dupeAccount);
                }
                else {
                    sfdcGoldenIdToDupeAccountListMap.put(dupeAccount.MDM_SFDC_Golden_Record_Id__c, new List<Account>{dupeAccount});
                }
                
            }
        }
        if(sfdcGoldenIdToDupeAccountListMap.size() > 0) {
            Map <String, Schema.SObjectField> fieldMap = Schema.SObjectType.Account.fields.getMap();
            Set<String> accountFieldSet = new Set<String>();
            for(Schema.SObjectField sfield : fieldMap.values()) {
                if(sfield.getDescribe().isUpdateable()) {
                    accountFieldSet.add(sfield.getDescribe().getName());
                }
            }
            System.debug('accountFieldSet---' + accountFieldSet);
            String whereCondition = 'LI_Account_Id__c != \'\'' +  ' and LQ_Account_id__c != \'\'' + ' and Recordtype.name != \'' + CON_CRM.GLOBAL_ACCOUNT_RECORD_TYPE_ID + '\' and Id IN  ( ';
            for(Id sfdcId : sfdcGoldenIdToDupeAccountListMap.keySet()) {
                whereCondition +=  '\'' + sfdcId + '\' , ';
            }
            whereCondition = whereCondition.removeEnd(', ');
            whereCondition += ' )';
            System.debug('whereCondition  ' + whereCondition);
            Map<Id, Account> accountMap = new SLT_Account().getAccounts(accountFieldSet, whereCondition);
            List<Account> survivorAccountList = accountMap.values();
            List<Account> dupeAccountList = new List<Account>();
            for(List<Account> dupeAccounts : sfdcGoldenIdToDupeAccountListMap.values()) {
                dupeAccountList.addAll(dupeAccounts);
            }
            List<AccountContactRelation> accContactRelList = new SLT_AccountContactRelation().getAccountContactRelations(dupeAccountList, survivorAccountList);
            if(accContactRelList.size() > 0) {
                delete accContactRelList;
            }
            if(survivorAccountList.size() > 0) {
                //  Check all fields of dupe account.  If dupe account has a value in the field and survivor has null value, update survivor to keep value of the dupe.
                //  Do not do this for LI_Account_Id__c and LQ_Account_Id__c fields as it will cause issues for mulesoft
                
                Map<Id, Set<Id>> surviAcTodupeAcccMap = new Map<Id, Set<Id>>();
                for(Account survivorAcc : survivorAccountList) {
                    for(String fieldName : accountFieldSet) {
                        if(!excludeFieldSet.contains(fieldName) && survivorAcc.get(fieldName) == NULL && sfdcGoldenIdToDupeAccountListMap.containsKey(survivorAcc.Id)) {
                            List<Account> dupeAccounts = sfdcGoldenIdToDupeAccountListMap.get(survivorAcc.Id);
                            for(Account dupeAccount : dupeAccounts) {
                                if(dupeAccount.get(fieldName) != NULL) {
                                    survivorAcc.put(fieldName, dupeAccount.get(fieldName));
                                    break;
                                }
                            }
                        } 
                    }
                    List<Account> dupeAccounts  = sfdcGoldenIdToDupeAccountListMap.get(survivorAcc.Id);
                    Set<Id> dupeAccSet = new Set<Id>();
                    for(Account dupeAcc : dupeAccounts) {
                        dupeAccSet.add(dupeAcc.Id);
                    }
                    surviAcTodupeAcccMap.put(survivorAcc.Id, dupeAccSet);
                }
                update survivorAccountList;
                if(surviAcTodupeAcccMap.size() > 0) {
                    mergeAccountsFuture(JSON.serialize(surviAcTodupeAcccMap));
                }
            }
        }
    }
    
    /**
     * This method is used to merge duplicate accounts
     * @params  newMap Map<Id, Account>
     * @params  oldMap Map<Id, Account>
     * @return  void
     */
    @future
    public static void mergeAccountsFuture(String surviAcTodupeAcccMapJson) {
        Map<Id, Set<Id>> surviAcTodupeAcccMap = (Map<Id, Set<Id>>)JSON.deserialize(surviAcTodupeAcccMapJson, Map<Id, Set<Id>>.Class);
        Set<Id> allAccountSet = new Set<Id>();
        allAccountSet.addAll(surviAcTodupeAcccMap.keySet());
        for(Id surId : surviAcTodupeAcccMap.keySet()) {
            allAccountSet.addAll(surviAcTodupeAcccMap.get(surId));
        }
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id from Account where Id IN :allAccountSet]);
        List<Merge_Queue__c> mergeQueueList = new List<Merge_Queue__c>();
        for(Id servivorAccountId : surviAcTodupeAcccMap.keySet()) {
            List<Account> dupeAccounts = new List<Account>();
            for(Id dupeId : surviAcTodupeAcccMap.get(servivorAccountId)) {
                Account dupeAccount = accountMap.get(dupeId);
                try {
                    merge accountMap.get(servivorAccountId) dupeAccount;
                } catch(Exception ex) {
                    Merge_Queue__c mq = new Merge_Queue__c();
                    mq.SobjectType__c = 'Account';
                    mq.Merge_Status__c = 'Failed';
                    mq.Merge_Error_Message__c = ex.getMessage();
                    mq.MasterRecordId__c = servivorAccountId;
                    mq.Dupe_Id_1__c = dupeAccount.Id;
                    mergeQueueList.add(mq);
                }
            }
        }
        if(mergeQueueList.size() > 0) {
            insert mergeQueueList;
        }
    }	
}
