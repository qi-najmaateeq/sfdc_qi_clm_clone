/**
* This is Attachment DAO helper class.
* version:1.0
*/
public class DAOH_Attachment {

    private static CDA_Request__c cdaRequest;
    public static Boolean isNegotiator {
        get {
            return (SRV_CDA_CDARequest.getUserPermission() == UTL_CDAUtility.NEGOTIATOR || SRV_CDA_CDARequest.getUserPermission() == UTL_CDAUtility.SYS_ADMIN);
        }
        set;
    }

    public static Boolean isEndStatus {
        get {
            if(cdaRequest != null) {
                return (cdaRequest.Status__c == UTL_CDAUtility.STATUS_CONTRACTEXECUTED || cdaRequest.Status__c == UTL_CDAUtility.STATUS_CANCELEDBYREQUESTOR || cdaRequest.Status__c == UTL_CDAUtility.STATUS_DISCLOSUREPERIODENDED);    //Updated by Vikram Singh under Item #254
            }
            return false;
        }
        set;
    }

    /**
    * This method is used to handle on before insert changes for attachment object.
    * @param newList List<Attachment>
    * @return  void
    */
    public static void handleOnBeforeInsertAttachment(List<Attachment> newList) {
        Set<Id> parentIdSet = new Set<Id>();
        //Map<Id, dsfs__DocuSign_Status__c> dsStausMapWithId;
        for(Attachment attach: newList) {
            parentIdSet.add(attach.parentId);
        }

        if(!UTL_CDAUtility.isSkipCDATriggers) {
            /*if(parentIdSet != null && parentIdSet.size() > 0) {
                //dsStausMapWithId = new Map<Id, dsfs__DocuSign_Status__c>([SELECT id, CDA_Request__c, CDA_Request__r.CDA_Id__c, CDA_Request__r.id, CDA_Request__r.CDA_Type__c, CDA_Request__r.Recipient_Account_Name_Formula__c, CDA_Request__r.CDA_Format__c, dsfs__DocuSign_Envelope_ID__c FROM dsfs__DocuSign_Status__c WHERE Id In: parentIdSet and CDA_Request__c != null ORDER BY createddate desc]);
                String query = 'SELECT id, CDA_Request__c, CDA_Request__r.CDA_Id__c, CDA_Request__r.id, CDA_Request__r.CDA_Type__c, CDA_Request__r.Recipient_Account_Name_Formula__c, CDA_Request__r.CDA_Format__c, dsfs__DocuSign_Envelope_ID__c FROM dsfs__DocuSign_Status__c WHERE Id In: parentIdSet and CDA_Request__c != null ORDER BY createddate desc';
                dsStausMapWithId = new Map<Id, dsfs__DocuSign_Status__c>((List<dsfs__DocuSign_Status__c>)Database.query(query));
            }
            if(dsStausMapWithId != null && dsStausMapWithId.size() > 0) {
                for(Attachment attach: newList) {
                    if(dsStausMapWithId.containsKey(attach.ParentId)) {
                        attach.Name = UTL_CDAUtility.getDocumentName(dsStausMapWithId.get(attach.ParentId).CDA_Request__r, UTL_CDAUtility.COMPLETED_KEYWORD);
                        SRV_CDA_DocusignStatus.splitSignedDocument(dsStausMapWithId.get(attach.ParentId).CDA_Request__r.id, dsStausMapWithId.get(attach.ParentId).dsfs__DocuSign_Envelope_ID__c);
                        attach.ParentId = dsStausMapWithId.get(attach.ParentId).CDA_Request__c;
                    }
                }
            }*/
            TGRH_CDAAttachment.handleOnBeforeInsertAttachment(newList, parentIdSet);
        }
    }

    /**
    * This method is used to handle on before delete changes for attachment object.
    * @param existingRecords Map<Id, Attachment>
    * @return  void
    */
    public static void handleOnBeforeDeleteAttachment(List<Attachment> attachmentList) {
        Set<Id> parentIdSet = new Set<Id>();
        List<CDA_Request__c> cdaRequestList;
        for (Attachment attach: attachmentList) {
            parentIdSet.add(attach.parentId);
        }
        if(parentIdSet != null && parentIdSet.size() > 0 && !UTL_CDAUtility.isSkipCDATriggers) {
            /*cdaRequestList = [SELECT id, Name, Status__c, Owner.Id, Owner.Name FROM CDA_Request__c WHERE Id IN :parentIdSet];
            if(cdaRequestList != null && cdaRequestList.size() > 0) {

                for(Attachment attach: attachmentList) {
                    for(CDA_Request__c cdaReq : cdaRequestList) {
                        cdaRequest = cdaReq;
                        if(attach.ParentId == cdaReq.Id && (isEndStatus  || (!isNegotiator && attach.OwnerId != UserInfo.getUserId()))) {
                            attach.ParentId.addError('You are not authorized to delete this attachment.');
                        }
                    }
                }
            }*/
            TGRH_CDAAttachment.handleOnBeforeDeleteAttachment(attachmentList, parentIdSet);
        }
    }

    /**
    * This method is used to handle on after update changes for attachment object.
    * @param newList List<Attachment>
    * @return  void
    */
    public static void handleOnAfterUpdateAttachment(List<Attachment> newList) {
        Set<Id> parentIdSet = new Set<Id>();
        for (Attachment attach: newList) {
            if(attach.parentId.getSObjectType().getDescribe().getName() == 'CDA_Request__c') {
                parentIdSet.add(attach.parentId);
            }
        }
        system.debug('####inside DAOH_Attachment.AfterUpdate attach.parentId: '+parentIdSet);
        if(parentIdSet != null && parentIdSet.size() > 0 && !UTL_CDAUtility.isSkipCDATriggers) {
            /*String fieldStr = UTL_CDAUtility.getObjectFieldStr('CDA_Request__c');
            List<CDA_Request__c> cdaRequestList = Database.query('SELECT ' + fieldStr + ' FROM CDA_Request__c WHERE id in: parentIdSet and Status__c = \'' + UTL_CDAUtility.STATUS_SUBMITTEDFORPROCESSING + '\'');
            if(cdaRequestList != null && cdaRequestList.size() > 0) {
                for (Attachment attach: newList) {
                    system.debug('####inside DAOH_Attachment.AfterUpdate attach: '+attach);
                    system.debug('####inside DAOH_Attachment.AfterUpdate CDARequest: '+cdaRequestList[0]);
                    if(attach.Name.contains(UTL_CDAUtility.GENERATED_DOCUMENT_KEYWORD) && attach.Name.contains(cdaRequestList[0].Name) && cdaRequestList[0].Status__c == UTL_CDAUtility.STATUS_SUBMITTEDFORPROCESSING) {
                        system.debug('####inside DAOH_Attachment.AfterUpdate Calling Docusign');
                        //Once CDA generated let's submit the request for further processing
                        QUE_CDA_AsyncSendCDADoc asyncSendDocQue = new QUE_CDA_AsyncSendCDADoc();
                        asyncSendDocQue.cdaRequest = cdaRequestList[0];
                        System.enqueueJob(asyncSendDocQue);
                    }
                }
            }*/
            TGRH_CDAAttachment.handleOnAfterUpdateAttachment(newList, parentIdSet);
        }
    }

    public static void updateContractOnAttachment(List<Attachment> attachmentList){
        Set<Id> docusignStatusId = new Set<Id>();
        for(Attachment att : attachmentList){
            //Check if added attachment is related to Account or not
            if(att.ParentId.getSobjectType() == dsfs__DocuSign_Status__c.SobjectType){
                docusignStatusId.add(att.ParentId);
            }
        }
        
        List<Contract> contractList = getContractList(docusignStatusId);
            //[select id, Date_executed_signed_contract_received__c, SOW_status__c, Agency_Program__c, Agency_Program__r.Name, ContractNumber, DocusignStatusId__c from Contract 
                                       //where DocusignStatusId__c in : docusignStatusId LIMIT 1];
        
        List<Contract> contractListToUpdate = new List<Contract>();
        
        //To update the attachment Name and Contract Status                                                     
        if(contractList !=null && contractList.size()>0){
            for(Attachment att : attachmentList){
                att.ParentId = contractList[0].Id;
                att.Name = contractList[0].Agency_Program__r.Name +'_'+ contractList[0].ContractNumber +'.pdf';
                contractList[0].SOW_status__c = 'Signed';
                contractList[0].Date_executed_signed_contract_received__c = date.today();
                contractListToUpdate.add(contractList[0]);
            }
        }
        
        if(contractListToUpdate.size() > 0){
            //Database.update(contractListToUpdate,true);
            updateContractList(contractListToUpdate);
        }
    }
    
    @testVisible private static List<Contract> testContractList;
    @testVisible private static Boolean useMock=False;
    @testVisible private static List<Contract> getContractList(Set<Id> docusignStatusId) {
        List<Contract> listOfContract; 
        if(useMock) {
      listOfContract = testContractList;            
        } else {
           listOfContract = [select id, Date_executed_signed_contract_received__c, SOW_status__c, Agency_Program__c, Agency_Program__r.Name, ContractNumber, DocusignStatusId__c from Contract 
                                       where DocusignStatusId__c in : docusignStatusId LIMIT 1];
        }
        return listOfContract;
    }
    
    @testVisible private static void updateContractList(List<Contract> contractListToUpdate) {
        if(!useMock) {
            Database.update(contractListToUpdate,true);  
        }
    }
} 
