public class CNT_CPQ_ApprovalReplyEmailService implements Messaging.InboundEmailHandler{
        
	String emailSubject;
	String emailHtmlBody;
	List<String> mailbody;
	String agreementPrefix;
	String fromAddress;
	String fromName;
    List<Apttus__APTS_Agreement__c> agreement= new List<Apttus__APTS_Agreement__c>();

    public Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){

        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        emailSubject = email.subject;
        emailHtmlBody = email.htmlBody;
        fromAddress = email.fromAddress; 
        fromName = email.fromName;     
        agreementPrefix = Apttus__APTS_Agreement__c.sobjecttype.getDescribe().getKeyPrefix();
        if(emailHtmlBody.contains(agreementPrefix)){   
            Id agreementId = emailHtmlBody.substring(emailHtmlBody.indexOf(agreementPrefix), emailHtmlBody.indexOf(agreementPrefix) + 15);
            if(agreementId != null){        
                Set<Id> recordTypeIdsFoundation = CPQ_Utility.getFoundationRecordTypes();

                if(emailHtmlBody.contains(CON_CPQ.REMOVE_THIS_LINE) ){
                    emailHtmlBody = emailHtmlBody.remove(emailHtmlBody.substring(emailHtmlBody.indexOf(CON_CPQ.REMOVE_THIS_LINE), emailHtmlBody.length()-1));
                }
                if(emailHtmlBody.contains('gmail_quote') ){
                    emailHtmlBody = emailHtmlBody.remove(emailHtmlBody.substring(emailHtmlBody.indexOf('gmail_quote'), emailHtmlBody.length()-1));
                }
                if(emailHtmlBody.contains('<b>From:</b>') ){
                    emailHtmlBody = emailHtmlBody.remove(emailHtmlBody.substring(emailHtmlBody.indexOf('<b>From:</b>'), emailHtmlBody.length()-1));
                }
                Set<Id> recordTypeIds= new Set<Id>(recordTypeIdsFoundation);
                Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_PROJECT_MANAGER_COMMENTS, CON_CPQ.AGREEMENT_AGREEMENT_STATUS, CON_CPQ.AGREEMENT_PROCESS_STEP, 
                    CON_CPQ.RECORDTYPEID, CON_CPQ.Owner, CON_CPQ.AGREEMENT_APPROVED_REJECT_BY_USER_NAME, CON_CPQ.AGREEMENT_APPROVED_REJECT_BY_USER_EMAIL};
                agreement = new SLT_Agreement().getAgreementDetailsByIdAndRecorType(agreementId,recordTypeIds, fieldSet);
                if(agreement.size() > 0){              
                    updateAgreementStatus(agreementId, agreement[0].Agreement_Status__c,agreement[0].Process_Step__c,
                        recordTypeIdsFoundation, agreement[0].RecordTypeId, agreement[0].OwnerId);
                }
            }
        }                                                                                      
        result.success = true;
        return result;                 
    }
    
    private void sendmailtoContractAnalyst(Id agreementId, Id OwnerId){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> toAddressList = new List<String>{OwnerId};
        mail = CPQ_Utility.getSingleMessage(CON_CPQ.CPQ_Email_Template_For_Contract_Analyst_PL_Approval_Received, agreementId, UserInfo.getUserId(),toAddressList);
        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
    }
	
    private void updateAgreementStatus(Id agreementId, String currentAgreementStatus, String currentProcessStep,
        Set<Id> recordTypeIdsFoundation, Id recordTypeId, Id OwnerId){
        if(agreement.size() > 0){          
            agreement[0].Apttus__Company_Signed_Title__c = fromName;
            agreement[0].Data_Asset_Physical_Location__c = fromAddress;
            if(emailHtmlBody.containsIgnoreCase(CON_CPQ.APPROVE) || emailHtmlBody.containsIgnoreCase(CON_CPQ.APPROVED) 
                || emailHtmlBody.containsIgnoreCase(CON_CPQ.YES)){
                agreement[0].Project_Manager_Comments__c = emailHtmlBody;

                if(currentAgreementStatus ==  CON_CPQ.PENDING_APPROVAL && currentProcessStep == CON_CPQ.APPROVAL_REVIEW &&
                    recordTypeIdsFoundation.contains(recordTypeId)){
                    agreement[0].Agreement_Status__c = CON_CPQ.BUDGET_APPROVED;
                    agreement[0].Process_Step__c = CON_CPQ.BUDGET_APPROVED;
                    Update agreement;
                }
                else if(currentAgreementStatus == CON_CPQ.PENDING_PL_REVIEW && currentProcessStep == CON_CPQ.PL_REVIEW &&
                    recordTypeIdsFoundation.contains(recordTypeId)){
                    agreement[0].Agreement_Status__c = CON_CPQ.PL_BUDGET_APPROVED;
                    agreement[0].Process_Step__c = CON_CPQ.PL_APPROVAL_RECEIVED;                            
                    Update agreement;
                    sendmailtoContractAnalyst(agreementId, OwnerId);                    		
                }                
            }
            else if(emailHtmlBody.containsIgnoreCase(CON_CPQ.REJECT) || emailHtmlBody.containsIgnoreCase(CON_CPQ.REJECTED) 
                || emailHtmlBody.containsIgnoreCase(CON_CPQ.NO)){
                agreement[0].Project_Manager_Comments__c = emailHtmlBody;
                if(currentAgreementStatus == CON_CPQ.PENDING_PL_REVIEW && currentProcessStep == CON_CPQ.PL_REVIEW &&
                    recordTypeIdsFoundation.contains(recordTypeId)){
                    agreement[0].Agreement_Status__c = CON_CPQ.DRAFT;
                    agreement[0].Process_Step__c = CON_CPQ.NONE;
                }
                Update agreement;
            }
        }
    }
}