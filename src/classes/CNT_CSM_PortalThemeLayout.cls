/*
 * Version       : 1.0
 * Description   : Apex Controller for PortalThemeLayout component.
 */
public class CNT_CSM_PortalThemeLayout { 
    @AuraEnabled
    public static List<Contact> getUserContact(){ 
        List<User> users = new List<User>();
        users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        List<Contact> contacts=new List<Contact>();
        contacts = new SLT_Contact().selectByContactIdList(new Set<Id> {users[0].ContactId}, new Set<String>{'Portal_Case_Type__c','AccountId','Contact_User_Type__c'});
        return contacts;
    }
    
    @AuraEnabled
    public static List<Major_Incident__c> getMajorIncidents(String accountId){ 
        List<Major_Incident__c> incidents = new List<Major_Incident__c>();
        Set<String> statusSet = new Set<String>();
        statusSet.add(CON_CSM.S_IN_PROGRESS);
        statusSet.add(CON_CSM.S_RESOLVED);
        incidents =[select Id, Major_Incident_Description__c, Major_Incident_Subject__c , Major_Incident_Customer_Communication__c, Status__c from Major_Incident__c where id in (select Major_Incident__c  from MI_AccountRelationship__c  where Accounts_Impacted__c =:accountId ) and  Status__c IN :statusSet];
        return incidents;
    }
    
    @AuraEnabled
    public static boolean checkTPAPermissionSetsAssigned(List<String> pmSetTPA){ 
        String userId = userInfo.getUserId();
        String profId = userInfo.getProfileId();
        boolean ret = false;
        List<Profile> proList = Database.query('select Id,Name from Profile where Id= :profId and Name in :pmSetTPA Limit 1');
        if(proList != null && proList.size() > 0 ){
            List<PermissionSetAssignment> pmSetList = Database.query('select PermissionSet.Name from PermissionSetAssignment where Assignee.Id = :userId and PermissionSet.Name =\'CSM_CSH_Community_Access\'');
            if(pmSetList != null && !pmSetList.isEmpty()){
                ret = true;   
            }else{
                ret = false;   
            }
        }
        return ret;
    }
}