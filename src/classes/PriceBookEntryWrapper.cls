public class PriceBookEntryWrapper {
    
    @AuraEnabled
    public Product2 productRecord;
    @AuraEnabled
    public PriceBookEntry pbeRecord;
    
    /**
     * constructor
     * @params  Product2 productRecord
     * @params  PriceBookEntry pbeRecord
     */
    public PriceBookEntryWrapper(Product2 productRecord, PriceBookEntry pbeRecord) {
        this.productRecord = productRecord;
        this.pbeRecord = pbeRecord;
    }
    
    /**
     * constructor
     * @params  Product2 productRecord
     */
    public PriceBookEntryWrapper(Product2 productRecord) {
        this.productRecord = productRecord;
        this.pbeRecord = new PriceBookEntry();
    }
}