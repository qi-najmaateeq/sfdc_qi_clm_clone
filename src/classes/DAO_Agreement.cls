/**
* This is Agreement Domain class.
* version : 1.0
*/
public class DAO_Agreement extends fflib_SObjectDomain{

    public static final String BID_GRID_MAPPING = 'Bid Grid Mapping';
    public static final String BUDGET_ACCEPTED = 'Budget Accepted';
    public static final String BUDGET_APPROVED = 'Budget Approved';
    public static final String BUDGET_DELIVERED = 'Budget Delivered';
    public static final String BUDGET_IN_REVIEW = 'Budget in Review';
    public static final String CNF_ACCEPTED = 'CNF Accepted';
    public static final String CNF_DELIVERED = 'CNF Delivered';
    public static final String CNF_EXECUTED = 'CNF Executed';
    public static final String CNF_IN_REVIEW = 'CNF in Review';
    public static final String CONTRACT_ACCEPTED = 'Contract Accepted';
    public static final String CONTRACT_DELIVERED = 'Contract Delivered';
    public static final String CONTRACT_EXECUTED = 'Contract Executed';
    public static final String CONTRACT_IN_REVIEW = 'Contract in Review';
    public static final String CUSTOMER_DELIVERABLE = 'Customer Deliverable';
    public static final String DRAFT = 'Draft';

    public static final String INTERNAL_REVIEW = 'Internal Review';
    public static final String FINAL_PREPARATION = 'Final Preparation';
    public static final String FDTN_CNF = 'FDTN_CNF';
    public static final String FDTN_CONTRACT = 'FDTN_Contract';
    public static final String FINAL_QC = 'Final QC';
    public static final String KEY_STAKEHOLDER_REVIEW = 'Key Stakeholder Review';
    public static final String LINE_MANAGER_QC = 'Line Manager QC';
    public static final String LINE_MANAGER_QC_CNF ='Line Manager QC - CNF';
    public static final String MAP_CALL = 'Map Call';
    public static final String PL_REVIEW = 'PL Review';
    public static final String PENDING_APPROVAL = 'Pending Approval';
    public static final String PENDING_PL_REVIEW = 'Pending PL Review';
    public static final String PROPOSAL_DELIVERED = 'Proposal Delivered';
    public static final String PROPOSAL_TO_CONTRACT = 'Proposal to Contract';
    public static final String QIP_SUBMISSION = 'QIP Submission';
    public static final String STRATEGY_CALL = 'Strategy Call';
    public static final String STUDY_AWARDED = 'Study Awarded';
    public static final String TSL_REVIEW = 'TSL Review';
	
    public static final String BID_GRID_COMPLETION = 'Bid Grid (Maintenance) Completion';
    public static final String QC_SELF_CHECK_FINAL = 'QC Self Check Final';
    public static final String CUSTOMER_DELIVERABLE_SENT = 'Customer Deliverable Sent';
    public static final String RECEIVED_ATP_LOI = 'Received ATP/LOI';
    public static final String PROPOSAL_DELIVERY = 'Proposal Delivery';
    public static final String PL_APPROVAL_RECEIVED = 'PL Approval Received';
    public static final String PL_BUDGET_APPROVED = 'PL Budget Approved';
    public static final String BUDGET_CONTRACT_DELIVERED = 'Budget & Contract Delivered';
    public static final String BALLPARK_DELIVERED = 'Ballpark Delivered';
    public static final String PAYMENT_SCHEDULE_DELIVERED = 'Payment Schedule Delivered';
    public static final String BUDGET_CONTRACT_ACCEPTED = 'Budget & Contract Accepted';
    public static final String COL_DELIVERED = 'COL Delivered';
    public static final String CNF_COL_ACCEPTED = 'CNF/COL Accepted';
    public static final String CNF_COL_REJECTED = 'CNF/COL Rejected';
    public static final String QC_SELF_CHECK_DRAFT = 'QC Self Check Draft';
    public static final String PROPOSAL_TO_CONTRACT_HANDOVER_CALL = 'Proposal to Contract Handover Call';
    public static final String KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL = 'Key Stakeholder Review and Challenge Call';
    public static final String TSL_APPROVED = 'TSL Approved';
    public static final String BUDGET_CHANGES_POST_PL_REVIEW = 'Budget Changes Post PL Review';


    /**
    * Constructor of this class
    * @params sObjectList List<SObject>
    */
    public DAO_Agreement(List<Apttus__APTS_Agreement__c> sObjectList) {
        super(sObjectList);
    }

    /**
    * Constructor Class for construct new Instance of This Class
    */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<Apttus__APTS_Agreement__c> sObjectList) {
            return new DAO_Agreement(sObjectList);
        }
    }

    /**
    * Unit of Work instance for CRUD operation
    */
    public static fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
        new Schema.SObjectType[] {
            Outbound_Message_Deletion_queue__c.SobjectType
        }
    );

    /**
    * This method is used for before insert of the Agreement trigger.
    * @return void
    */
    public override void onBeforeInsert() {
        //updting Bid_Number__c
        DAOH_Agreement.bidHistoryNumberIncrement((List<Apttus__APTS_Agreement__c>)Records,null);
        setProjectManagerEmail((List<Apttus__APTS_Agreement__c>)Records, null);
    }

    public override void onApplyDefaults() {
        setDefaultsForFoundationReleaseClone((List<Apttus__APTS_Agreement__c>) Records);
    }


    /**
    * This method is used for before update of the Agreement trigger.
    * @params  existingRecords Map<Id,SObject>
    * @return  void
    */
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        //updting Bid_Number__c
        DAOH_Agreement.bidHistoryNumberIncrement((List<Apttus__APTS_Agreement__c>)Records,(Map<Id, Apttus__APTS_Agreement__c>) existingRecords);
                   
        // Set End Date and Term Months for OCE/LTC MVP
        setEndDateAndTermMonths(new Map<Id, Apttus__APTS_Agreement__c>((List<Apttus__APTS_Agreement__c>)Records),
                                (Map<Id,Apttus__APTS_Agreement__c>)existingRecords);
        
        //Set Agreement Status Progression
        DAOH_Agreement.setAgreementStatusAndProcessStepForMVP((List<Apttus__APTS_Agreement__c>)Records,
           (Map<Id, Apttus__APTS_Agreement__c>) existingRecords);
        setProjectManagerEmail((List<Apttus__APTS_Agreement__c>)Records, (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
        setEmailFieldForInitialBid((List<Apttus__APTS_Agreement__c>)Records);
    }

    /**
    * This method is used for after insert of the Agreement trigger.
    * @return void
    */
    public override void onAfterInsert() {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Opportunity.SobjectType
            }

        );

        // SDM - 10/22/2018
        setAgreementExistsOnRelatedOpty(new Map<Id, Apttus__APTS_Agreement__c>((List<Apttus__APTS_Agreement__c>)Records), uow);
        uow.commitWork();

        makeOtherAgreementNonPrimary((List<Apttus__APTS_Agreement__c>)Records, null);
        setEmailBodyAndSubject((List<Apttus__APTS_Agreement__c>)Records, null);
    }


    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {

        makeOtherAgreementNonPrimary((List<Apttus__APTS_Agreement__c>)Records,
            (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
        setEmailBodyAndSubject((List<Apttus__APTS_Agreement__c>)Records,
            (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
        DAOH_Agreement.createOrUpdateBudgetAgreementHistoryRecord((List<Apttus__APTS_Agreement__c>)Records,
            (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
        DAOH_Agreement.createTaskOnAgreement((List<Apttus__APTS_Agreement__c>)Records,
           (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
        DAOH_Agreement.fireAprrovalRequestWhenAgreementStatusChanges((List<Apttus__APTS_Agreement__c>)Records,
           (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
    }

    public static void makeOtherAgreementNonPrimary(List<Apttus__APTS_Agreement__c> newAgreementList,
        Map<Id, Apttus__APTS_Agreement__c> oldRecordsMap){

        Set<Id> opportunityIds = new Set<Id>();
        Set<Id> currentAgreementIds = new Set<Id>();
        Set<Id> recordTypeIds = CPQ_Utility.getFoundationRecordTypes();
        for(Apttus__APTS_Agreement__c agreement : newAgreementList){

            if(agreement.Apttus__Related_Opportunity__c != null &&  (Trigger.isInsert || (Trigger.isUpdate &&
            agreement.Mark_as_Primary__c != oldRecordsMap.get(agreement.Id).Mark_as_Primary__c )) &&
            agreement.Mark_as_Primary__c && recordTypeIds.contains(agreement.RecordTypeId)){

                opportunityIds.add(agreement.Apttus__Related_Opportunity__c);
                currentAgreementIds.add(agreement.Id);
            }
        }
        if(opportunityIds.size() > 0 && currentAgreementIds.size() > 0){
            DAOH_Agreement.updateAgreementToMakeNonPrimary(opportunityIds, currentAgreementIds);
        }
    }

    /**
    Set the "Agreement for this Opportunity Exists" flag on the related Opportunity of an Agreement, if the 
    Agreement was NOT created from a Clone.
    @param newMap   Map of Agreement Id to Agreement
    @param uow      A instance of fflib_SObjectUnitOfWork.
    @return none
    */
    private static void setAgreementExistsOnRelatedOpty(Map<Id, Apttus__APTS_Agreement__c> newMap, fflib_SObjectUnitOfWork uow) {
	
        Set<Id> recordTypeIds = CPQ_Utility.getFoundationRecordTypes();

        for (Apttus__APTS_Agreement__c agmt : newMap.values()) {

            if (!agmt.Apttus__Workflow_Trigger_Created_From_Clone__c && agmt.Apttus__Related_Opportunity__c != null &&       	recordTypeIds.contains(agmt.RecordTypeId)) {
                Opportunity opty = new Opportunity(
                    Id = agmt.Apttus__Related_Opportunity__c, Agreement_for_This_Opportunity_Exists__c = True
                );
                uow.registerDirty(opty);
            }
        }
    }

    /**
    Set the default values for a handful of fields on the Agreement record.
    @param agreements   List of Agreement records to set default values on 
    @return none
    */
    private void setDefaultsForFoundationReleaseClone(List<Apttus__APTS_Agreement__c> agreements) {
        Set<Id> opportunityIdSet = new Set<Id>();
        Set<String> optyFieldSet = new Set<String> {'Id', 'Legacy_Quintiles_Opportunity_Number__c'};
        Set<Id> parentAgmtIdSet = new Set<Id>();
        Map<Id, Opportunity> optyIdToOptyMap;
        Map<String, RecordType> devNametoRecordTypeMap = new Map<String, RecordType>();
        Map<Id, Apttus__APTS_Agreement__c> parentAgmtIdToAgmtMap = new Map<Id, Apttus__APTS_Agreement__c>();

        // Get record type records
        for (RecordType rt : [select DeveloperName, Id from RecordType where DeveloperName like 'FDTN%']) {
            devNametoRecordTypeMap.put(rt.DeveloperName, rt);
        }

        // Collect the opty and agreement ids for all the agreements
        for (Apttus__APTS_Agreement__c agmt : agreements) {
            opportunityIdSet.add(agmt.Apttus__Related_Opportunity__c);
            parentAgmtIdSet.add(agmt.Agreement_Cloned_From__c);
        }

        optyIdToOptyMap = new SLT_Opportunity().getOpportunityById(opportunityIdSet, optyFieldSet);

        Set<String> agmtFields = new Set<String> {'Id', 'Contract_Number__c'};
        for (Apttus__APTS_Agreement__c agmt : new SLT_Agreement().getAgreementFieldsById(parentAgmtIdSet, agmtFields)) {
            parentAgmtIdToAgmtMap.put(agmt.Id, agmt);
        }
            
            
        for (Apttus__APTS_Agreement__c agmt : agreements) {
            opportunityIdSet.add(agmt.Apttus__Related_Opportunity__c);

            // If this is a clone, set default fields.  Record Type and Name are the only fields that change, 
            // based on Agreement Type.
            if (agmt.Apttus__Workflow_Trigger_Created_From_Clone__c) {

                switch on agmt.Select_Agreement_Type__c {
                    when 'FDTN - Initial Bid' {
                        agmt.RecordTypeId = devNametoRecordTypeMap.get('FDTN_Initial_Bid').Id;
                        agmt.Name = optyIdToOptyMap.get(agmt.Apttus__Related_Opportunity__c).Legacy_Quintiles_Opportunity_Number__c 
                        + '_Budget_Agreement_FDTN_InitialBid_' + Datetime.now().format('dd-MMM-yyy');
                    }
                    when 'FDTN - Rebid' {
                        agmt.RecordTypeId = devNametoRecordTypeMap.get('FDTN_Rebid').Id;
                        agmt.Name = optyIdToOptyMap.get(agmt.Apttus__Related_Opportunity__c).Legacy_Quintiles_Opportunity_Number__c 
                        + '_Budget_Agreement_FDTN_Rebid_' + Datetime.now().format('dd-MMM-yyy');
                    }
                    when 'FDTN - Contract' {
                        agmt.RecordTypeId = devNametoRecordTypeMap.get('FDTN_Contract').Id;
                        agmt.Name = optyIdToOptyMap.get(agmt.Apttus__Related_Opportunity__c).Legacy_Quintiles_Opportunity_Number__c 
                        + '_Budget_Agreement_FDTN_Contract_' + Datetime.now().format('dd-MMM-yyy');
                    }
                    when 'FDTN - CNF' {
                        agmt.RecordTypeId = devNametoRecordTypeMap.get('FDTN_CNF').Id;
                        agmt.Name = optyIdToOptyMap.get(agmt.Apttus__Related_Opportunity__c).Legacy_Quintiles_Opportunity_Number__c 
                        + '_Budget_Agreement_FDTN_CNF_' + Datetime.now().format('dd-MMM-yyy');
                    }
                    when 'FDTN - Change Order' {
                        agmt.RecordTypeId = devNametoRecordTypeMap.get('FDTN_Change_Order').Id;
                        agmt.Name = optyIdToOptyMap.get(agmt.Apttus__Related_Opportunity__c).Legacy_Quintiles_Opportunity_Number__c 
                        + '_Budget_Agreement_FDTN_ChangeOrder_' + Datetime.now().format('dd-MMM-yyy');
                    }
                    when else {
                        System.debug('When Else');
                    }
                }

                agmt.Is_Cloned__c = TRUE;
                agmt.Agreement_Status__c = DRAFT;
                agmt.Process_Step__c = CON_CPQ.NONE;
                agmt.RFP_Scenario__c = null;                
                agmt.Scenario_Description__c = '';
                agmt.Scenario_Number__c = null;
                agmt.Select_Agreement_Type__c = null;
                agmt.Mark_as_Primary__c = FALSE;        // Added - SDM 12/7/2018 - LC-1592
                agmt.Project_Manager_Comments__c = null;
                agmt.Pricing_Tool_Locked__c = FALSE;
                agmt.XAE_Lock_Timestamp__c = null;
                agmt.Budget_Checked_Out_By__c = null;
                agmt.Apttus__Company_Signed_Title__c = null;
                agmt.Customer_Opportunity_Background__c = null;
                agmt.Data_Asset_Physical_Location__c = null;
                agmt.Agreement_Purpose_List__c = null;
                agmt.Project_Manager_Comments__c = null;
                if (parentAgmtIdToAgmtMap.size() > 0){
                    agmt.Contract_Number__c = parentAgmtIdToAgmtMap.get(agmt.Agreement_Cloned_From__c).Contract_Number__c;
                }
                System.debug('Name: ' + agmt.Name);
                System.debug(Datetime.now().format('dd-MMM-yyy'));
            }
        }
    }

            
    /**
        For Subscription Agreements, set the End Date and Term Months based on the Start Date and Term Length.
        This is for OCE/LTC MVP
        @param newMap   Map of Agreement Id to Agreement
        @param uow      A instance of fflib_SObjectUnitOfWork.
        @return none.
    */
    public static void setEndDateAndTermMonths(Map<Id, Apttus__APTS_Agreement__c> newMap, 
                                               Map<Id, Apttus__APTS_Agreement__c> oldMap) {
        List<Apttus__APTS_Agreement__c> subscriptions =
            [Select Id
             From Apttus__APTS_Agreement__c
             Where Id in :newMap.keySet()
             And RecordType.DeveloperName = 'Subscription_Products'];
        for (Apttus__APTS_Agreement__c agmt: subscriptions) {
            Apttus__APTS_Agreement__c newAgmt = newMap.get(agmt.Id);
            Apttus__APTS_Agreement__c oldAgmt = oldMap.get(agmt.Id);
            if (newAgmt.Apttus__Contract_Start_Date__c != oldAgmt.Apttus__Contract_Start_Date__c ||
                    newAgmt.O_Term_Years__c != oldAgmt.O_Term_Years__c) {
                newAgmt.Apttus__Contract_End_Date__c = newAgmt.Apttus__Contract_Start_Date__c.addYears(Integer.valueOf(newAgmt.O_Term_Years__c)).addDays(-1);
                newAgmt.Apttus__Term_Months__c = Integer.valueOf(newAgmt.O_Term_Years__c) * 12;
            }
        }
    } 

    private void setProjectManagerEmail(List<Apttus__APTS_Agreement__c> newAgreementList,
        Map<Id, Apttus__APTS_Agreement__c> oldRecordsMap){
        Set<Id> recordTypeIds = CPQ_Utility.getContractRecordTypes();
        for(Apttus__APTS_Agreement__c agreement : newAgreementList){

            if((Trigger.isInsert || (Trigger.isUpdate &&
                agreement.Project_Manager_Name__c != oldRecordsMap.get(agreement.Id).Project_Manager_Name__c)) && 
                recordTypeIds.contains(agreement.RecordTypeId)){
                if(agreement.Project_Manager_Name__c != null){
                    agreement.Project_Manager_Email__c = '';
                    List<String> projectManagerName = agreement.Project_Manager_Name__c.split(' ');
                    if(projectManagerName.size() > 0){
                        agreement.Project_Manager_Email__c = projectManagerName[0].toLowerCase();
                    }
                    if(projectManagerName.size() > 1){
                        agreement.Project_Manager_Email__c +='.' +projectManagerName[1].toLowerCase();
                    }    
                    if(!String.isBlank(agreement.Project_Manager_Email__c))
                        agreement.Project_Manager_Email__c  += '@' + CON_CPQ.IQVIA_DOMAIN;
                }else
                    agreement.Project_Manager_Email__c = '';
            }
        } 
    }

    private void setEmailBodyAndSubject(List<Apttus__APTS_Agreement__c> newAgreementList,
        Map<Id, Apttus__APTS_Agreement__c> oldRecordsMap){

        List<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();
        Set<Id> recordTypeIds = CPQ_Utility.getContractRecordTypes();
        for(Apttus__APTS_Agreement__c agreement : newAgreementList){

            if(recordTypeIds.contains(agreement.RecordTypeId) && (Trigger.isInsert || (Trigger.isUpdate && 
                agreement.Name != oldRecordsMap.get(agreement.Id).Name))){
                agreementList.add(agreement);
            }
        }
        if(agreementList.size() > 0)
            DAOH_Agreement.updateEmailBodyAndSubject(agreementList, Trigger.isUpdate);

    }

    private void setEmailFieldForInitialBid(List<Apttus__APTS_Agreement__c> agreementList)
    {
        Set<Id> recordTypeIds = CPQ_Utility.getInitialBidRecordTypes();
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();

        if(cpqSetting != null && cpqSetting.Bid_Grid_Mapping_Email__c != null){

            for(Apttus__APTS_Agreement__c agreement : agreementList){
                if(agreement != null
                   && recordTypeIds.contains(agreement.RecordTypeId)){

                    agreement.Requester_email__c = cpqSetting.Bid_Grid_Mapping_Email__c;
                }
            }
        }
    }
}
