/**
 * This class contains unit tests for validating the behavior of MIBNF Triggers on OpportunityLineItem Update

 */
@isTest
private class MI_BNF_OppLineItemUpdateTrigger_Tests {

    public static MIBNF2__c MIBNF;
	public static MIBNF_Component__c MIBNF_Comp;
	public static MI_BNF_LineItem__c MIBNFLineItem;
	public static Opportunity opp;
	public static OpportunityLineItem[] opplineItemToCreate;
	


	static testMethod void testOpportunityLineItemUpdate(){
		
	    //*********************************setup MIBNF and MIBNF Component**********************************************************/
	    //**************************************************************************************************************************/
			SetupMIBNF();
			SetupMIBNF_Comp(MIBNF);
			
			MI_BNF_LineItem__c[] MIBNFLineItemToCreate = new MI_BNF_LineItem__c[]{};
			for(Integer x=0; x<200;x++)
			{
			    MI_BNF_LineItem__c lineItem = new MI_BNF_LineItem__c();
			    lineItem.MIBNF_Component__c = MIBNF_Comp.id;
			    lineItem.Opportunity_Line_Itemid__c=opplineItemToCreate[x].Id;
	        	lineItem.Total_Price__c=opplineItemToCreate[x].TotalPrice;
			    MIBNFLineItemToCreate.add(lineItem);
			}
			
			insert MIBNFLineItemToCreate;
		//****************************************************************************************************************************/
		
		
		//Update data causing an opportunitylineitem trigger to fire. 
		//****************************************************************************************************************************/
			OpportunityLineItem[] opplineItem = new OpportunityLineItem[]{};
			for(OpportunityLineItem oliItem : [select id from OpportunityLineItem where OpportunityId=:opp.id])
			{
				OpportunityLineItem OLI = new OpportunityLineItem(id=oliItem.Id);
			    OLI.TotalPrice=7000;
			    opplineItem.add(OLI);
			}
			update opplineItem;
		//****************************************************************************************************************************/
	}	
	
	static testMethod void testOpportunityLineItemDelete(){
		
	    //*********************************setup MIBNF and MIBNF Component**********************************************************/
	    //**************************************************************************************************************************/
			SetupMIBNF();
			SetupMIBNF_Comp(MIBNF);
			
			MI_BNF_LineItem__c[] MIBNFLineItemToCreate = new MI_BNF_LineItem__c[]{};
			for(Integer x=0; x<200;x++)
			{
			    MI_BNF_LineItem__c lineItem = new MI_BNF_LineItem__c();
			    lineItem.MIBNF_Component__c = MIBNF_Comp.id;
			    lineItem.Opportunity_Line_Itemid__c=opplineItemToCreate[x].Id;
	        	lineItem.Total_Price__c=opplineItemToCreate[x].TotalPrice;
			    MIBNFLineItemToCreate.add(lineItem);
			}
			
			insert MIBNFLineItemToCreate;
		//****************************************************************************************************************************/
		
		
		//Update data causing an opportunitylineitem trigger to fire. 
		//****************************************************************************************************************************/
			OpportunityLineItem[] opplineItem = new OpportunityLineItem[]{};
			for(OpportunityLineItem oliItem : [select id from OpportunityLineItem where OpportunityId=:opp.id])
			{
				OpportunityLineItem OLI = new OpportunityLineItem(id=oliItem.Id);
			    opplineItem.add(OLI);
			}
			delete opplineItem;
			
			List<OpportunityLineItem> oliItem =[select id from OpportunityLineItem where OpportunityId=:opp.id];
			system.assertEquals(oliItem.size(),0);
		//****************************************************************************************************************************/
	}	
	
	 //Setup MIBNF
    static void SetupMIBNF()
    {
        Current_Release_Version__c crv = new Current_Release_Version__c();
        crv.Current_Release__c = '3000.01';
        upsert crv;
        BNF_Settings__c bs = new BNF_Settings__c();
        bs.BNF_Release__c = '2019.01';
        upsert bs;
        
    	 // Create Opportunity
        ID RevenueAnalystID;
        for(Revenue_Analyst__c RA: [select id from Revenue_Analyst__c limit 1])
        {
        	RevenueAnalystID=RA.id;
        }
        
        opp =new Opportunity(Name='Quick opp',StageName='1. Identifying Opportunity',Amount=2000,CloseDate=System.today(),
        Contract_Term__c='Single-Period',Contract_Type__c='Individual');
		opp.LeadSource = 'Account Planning';
		opp.Budget_Available__c = 'Yes';
		//opp.Unique_Business_Value__c = 'Unknown';
		//opp.Compelling_Event__c = 'No';
		insert opp;
        
        Product2 p2 = new product2(name='Test Product1',IsActive = true,Business_Type__c = 'I&A');
		insert p2;
        PricebookEntry pbe = new PricebookEntry();
                pbe.UseStandardPrice = false;
                pbe.Pricebook2Id=Test.getStandardPricebookId();
                pbe.Product2Id=p2.id;
                pbe.IsActive=true;
                pbe.UnitPrice=100.0;
                pbe.CurrencyIsoCode = 'USD';
        insert pbe;
        
		AddLineItems(opp);
		
		MIBNF=new MIBNF2__c();
		MIBNF.Client__c=opp.AccountId;
		MIBNF.Opportunity__c=opp.Id;
		MIBNF.Sales_Org_Code__c='CH08';
		MIBNF.Billing_Currency__c='USD';
		MIBNF.IMS_Sales_Org__c='Acceletra';
		MIBNF.Fair_Value_Type__c='Stand Alone';
		MIBNF.Invoice_Default_Day__c='15';
		MIBNF.Contract_Start_Date__c=system.today();
		MIBNF.Contract_End_Date__c=system.today();
		MIBNF.Contract_Type__c='Individual';
		MIBNF.Contract_Term__c='Single-Period';
		MIBNF.Payment_Terms__c='0000-Default Payment Terms of Customer Master Data';
		MIBNF.Revenue_Analyst__c=RevenueAnalystID;
		
		insert MIBNF;
    }
    
    //setting up component for MIBNF
    static void SetupMIBNF_Comp(MIBNF2__c RefMIBNF)
    {
    	MIBNF_Comp=new MIBNF_Component__c();
    	MIBNF_Comp.MIBNF__c=RefMIBNF.Id;
    	MIBNF_Comp.BNF_Status__c='New';
    	MIBNF_Comp.Is_this_a_retainer_downpayment__c='No';
    	MIBNF_Comp.Print_Shop__c='No';
    	
    	insert MIBNF_Comp;
    }
    
    // AddLineItem to Opportunity
    static void AddLineItems(Opportunity O)
    {
        //  Add a line item to the opportunity
        PricebookEntry PE1 = [select Id, CurrencyIsoCode from PricebookEntry where CurrencyIsoCode = 'USD' and IsActive = true and Product2.IsActive = true and Product2.Business_Type__c = 'I&A' limit 1][0];
        opplineItemToCreate = new OpportunityLineItem[]{};
		
		for(Integer x=0; x<200;x++){
		    OpportunityLineItem OLI1 = new OpportunityLineItem();
		    OLI1.OpportunityId = O.Id;
		    OLI1.PricebookEntryId = PE1.Id;
        	OLI1.Quantity = 1.00;
        	OLI1.TotalPrice=8000;
            oli1.Sale_Type__c = 'New';
            oli1.Delivery_Country__c = 'USA';
            oli1.Revenue_Type__c = 'Ad Hoc';
		    opplineItemToCreate.add(OLI1);
		}
		Test.startTest();
		insert opplineItemToCreate;
		Test.stopTest();	
        
    }
}