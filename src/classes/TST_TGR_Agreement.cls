@isTest
public class TST_TGR_Agreement {

    static Apttus__APTS_Agreement__c setAgreementData(Boolean markAsPrimary, Id OpportuntiyId){

        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.Mark_as_Primary__c = markAsPrimary;
        testAgreement.recordTypeId = RecordTypeId;
        testAgreement.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
        insert testAgreement;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }

    @isTest
    public static void testMarkPrimaryAgreementNonPrimaryForRelatedOpportunity(){

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement1 = setAgreementData(true, testOpportunity.Id);
        Apttus__APTS_Agreement__c testAgreement2 = setAgreementData(true, testOpportunity.Id);

        Test.startTest();
            testAgreement2.Mark_as_Primary__c = true;
            Update testAgreement2;
            Apttus__APTS_Agreement__c updatedAgreement = [SELECT Mark_as_Primary__c FROM Apttus__APTS_Agreement__c
                                                            WHERE Id =: testAgreement1.Id];
        Test.stopTest();

        System.assertEquals(false, updatedAgreement.Mark_as_Primary__c, 'Should mark agreement non primary');

    }
}