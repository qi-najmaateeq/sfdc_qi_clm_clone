/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Assignment
 */
public class SLT_Assignment extends fflib_SObjectSelector {
    
    /**
     * constructor to initialize CRUD and FLS
     */
    public SLT_Assignment() {
        super(false, true, true);
    }
    
    /**
     * constructor to initialise CRUD and FLS with a parameter for FLS.
     */
    public SLT_Assignment(Boolean enforceFLS) {
        super(false, true, enforceFLS);
    }
    
    
    public SLT_Assignment(Boolean enforceFLS,Boolean enforceCRUD) {
        super(false, enforceFLS, enforceCRUD);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return pse__Assignment__c.sObjectType;
    }
    
    /**
     * This method used to get Resource requests with Assigments
     * @params  Set<ID> resourceRequestIdset
     * @params  Set<String> assignmentFieldSet
     * @return  Map<Id, pse__Assignment__c>
     */
    public Map<Id, pse__Assignment__c> selectAssignmentsByIdSet(Set<ID> sObjectIdset, String condition, Set<String> assignmentFieldSet) {
        fflib_QueryFactory assignmentQueryFactory = newQueryFactory(true);
        String queryString = assignmentQueryFactory.selectFields(assignmentFieldSet).setCondition(condition).toSOQL();
        return new Map<Id, pse__Assignment__c>((List<pse__Assignment__c>) Database.query(queryString));
    }
    
    /**
     * This method used to get Assignemnt of Resource Request
     * @params  Set<Id> resourceRequestIdset
     * @params  Set<String> assignmentFieldSet
     * @return  Map<Id, pse__Assignment__c>
     */
    public Map<Id, pse__Assignment__c> getAssignmentResourceRequest(Set<ID> resourceRequestIdset, Set<String> assignmentFieldSet) {
        fflib_QueryFactory assignmentQueryFactory = newQueryFactory(true);
        String queryString = assignmentQueryFactory.selectFields(assignmentFieldSet).setCondition('pse__Resource_Request__c in :resourceRequestIdset').toSOQL();
        return new Map<Id, pse__Assignment__c>((List<pse__Assignment__c>) Database.query(queryString));
    }
    
    /**
     * This method used to get Assignemnt of Resource
     * @params  Set<Id> sObjectIdset
     * @params  String assignmentCondition
     * @params  Set<String> assignmentFieldSet
     * @return  List<pse__Assignment__c>
     */
    public List<pse__Assignment__c> getAssignmentByResource(Set<Id> sObjectIdset, String assignmentCondition, Set<String> assignmentFieldSet) {
        fflib_QueryFactory assignmentQueryFactory = newQueryFactory(true);
        String queryString = assignmentQueryFactory.selectFields(assignmentFieldSet).setCondition(assignmentCondition).toSOQL();
        system.debug('Query String***** '+queryString);
        return ((List<pse__Assignment__c>) Database.query(queryString));
    }
    
    /**
     * This method used to get Assignemnt of Resource
     * @params  Set<Id> agrIdset
     * @params  String assignmentCondition
     * @params  Set<String> assignmentFieldSet
     * @return  List<pse__Assignment__c>
     */
    public List<pse__Assignment__c> getAssignmentByAgrAndCondition(Set<Id> agrIdset, String assignmentCondition, Set<String> assignmentFieldSet) {
        fflib_QueryFactory assignmentQueryFactory = newQueryFactory(true);
        String queryString = assignmentQueryFactory.selectFields(assignmentFieldSet).setCondition('Agreement__c in: agrIdset '+assignmentCondition).toSOQL();
        system.debug('Query String***** '+queryString);
        return ((List<pse__Assignment__c>) Database.query(queryString));
    }
}