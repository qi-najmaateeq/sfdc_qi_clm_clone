/**
 * This test class is used to test all methods in Resource Request trigger.
 * version : 1.0 
 */
@isTest(seeAllData=false)
private class TST_CNT_OWF_CreateAssignmentFromResReq {
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        cont.sub_group__c = 'TSL-Japan';
        cont.Available_for_triage_flag__c = true;
        insert cont;
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        agreement.Bid_Number__c = 1;
        agreement.Bid_Due_Date__c = System.today().AddDays(5);
        insert agreement; 
        pse__Proj__c bidProject = [Select id from pse__Proj__c where Agreement__c =: agreement.Id];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, opp.Id, bidProject.Id);
        resourceRequest.pse__Group__c = grp.Id;
        resourceRequest.subgroup__c = 'TSL-Japan';
        resourceRequest.Suggested_FTE__c = 11;
        insert resourceRequest;
    }
    
    /**
     * This test method used to test getSuggestedFTEFromResRequest method
     */
    static testmethod void testGetSuggestedFTEFromResRequest() {
        pse__Resource_Request__c resRequest = [SELECT Id, Name FROM pse__Resource_Request__c limit 1];
        
        Test.startTest();
            String suggestedFteValue = CNT_OWF_CreateAssignmentFromResReq.getSuggestedFTEFromResRequest(String.valueOf(resRequest.Id));

        Test.stopTest();
    }
    
    /**
     * This test method used to test createAssignment method
     */
    static testmethod void testCreateAssignment() {
        pse__Resource_Request__c resRequest = [SELECT Id, Name FROM pse__Resource_Request__c where subgroup__c = 'TSL-Japan'];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'];
        
        Test.startTest();
            String resultMessage = CNT_OWF_CreateAssignmentFromResReq.assignResourceAndCreateAssignment(resRequest.Id, cont.Id);
            system.assertEquals('success', resultMessage);
        Test.stopTest();
    }
    
    /**
     * This test method used to test createAssignment method
     */
    static testmethod void testCreateAssignmentWithError() {
        pse__Resource_Request__c resRequest = [SELECT Id, Name FROM pse__Resource_Request__c where subgroup__c = 'TSL-Japan'];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'];
        cont.sub_group__c = 'TSL';
        update cont;
        
        Test.startTest();
            String resultMessage = CNT_OWF_CreateAssignmentFromResReq.assignResourceAndCreateAssignment(resRequest.Id, cont.Id);
            system.assertNotEquals('success', resultMessage);
        Test.stopTest();
    }
    
    /**
     * This test method used to test createAssignment method
     */
    static testmethod void testHasAccess() {
              
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(new pse__Resource_Request__c());
            CNT_OWF_CreateAssignmentFromResReq cnt = new CNT_OWF_CreateAssignmentFromResReq(sc);
            cnt.hasAccess = true;
        Test.stopTest();
    }
}