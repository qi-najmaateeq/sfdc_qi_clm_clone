/*
 * @author		: Bryan Fry - Statera
 * Version      : 1.0
 * Description  : Controller for Calculate Escalation / Amortization Visualforce Page
 */
public with sharing class CNT_CPQ_CalculateEscalationAmortization {
	public CNT_CPQ_CalculateEscalationAmortization() {
	}

	public PageReference calculateEscalationAmortization() {
		Id agmtId = ApexPages.currentPage().getParameters().get('id');

		// Get Finalized Product Configuration and associated line items
		Apttus_Config2__ProductConfiguration__c pc = SRV_CPQ_AgreementAnnualCalculation.getFinalizedProductConfiguration(agmtId);
		if (pc != null && 
				pc.Apttus_Config2__LineItems__r != null && 
				pc.Apttus_CMConfig__AgreementId__r.O_Term_Years__c != null) {
			List<Apttus_Config2__LineItem__c> items = pc.Apttus_Config2__LineItems__r;

			// Find the term, total Subscription Fees, and total Implementation Fees from the data returned
			Decimal term = Decimal.valueOf(pc.Apttus_CMConfig__AgreementId__r.O_Term_Years__c);
			Decimal subscriptionTotal = SRV_CPQ_AgreementAnnualCalculation.getSubscriptionTotal(items);
			Decimal implementationTotal = SRV_CPQ_AgreementAnnualCalculation.getImplementationTotal(items);

			// Calculate annual escalation and amortization amounts for each year in the term
			List<Agreement_Annual_Calculation__c> calcs =
				SRV_CPQ_AgreementAnnualCalculation.calculateAnnualEscalationAndAmortization(agmtId, pc, term, subscriptionTotal, implementationTotal);

			// Total the escalation and amortization amounts across years in the term into agreement fields
			Apttus__APTS_Agreement__c agmt = 
				SRV_CPQ_AgreementAnnualCalculation.calculateAgreementTotals(agmtId, calcs, term, subscriptionTotal, implementationTotal);

			// Delete old calculation rows
			List<Agreement_Annual_Calculation__c> oldCalcs = SRV_CPQ_AgreementAnnualCalculation.getExistingAnnualCalculations(agmtId);
		    if (oldCalcs != null && !oldCalcs.isEmpty()) {
		        delete oldCalcs;
		    }

		    // Insert new calculation rows and update the agreement
			insert calcs;
			update agmt;
		}

		return new PageReference('/' + agmtId);
	}
}