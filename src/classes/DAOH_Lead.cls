/**
 * This is Lead trigger handler class.
 * version : 1.0
 */
 public class DAOH_Lead {
     
    /**
     * This method is used to set DispositionDate.
     * @params  newList List<Lead>
     * @params  oldMap Map<Id, Lead>
     * @return  void
     */
    public static void setDispositionDate(List<Lead> newList, Map<Id, Lead> oldMap) {
        for(Lead ld : newList){
            if(ld.Disposition__c != null && (Trigger.isInsert || oldMap.get(ld.id).Disposition__c != ld.Disposition__c)) {
                ld.Disposition_Date__c = Date.today();
            }
        }
    }
    
    /**
     * This method is used to set null values for various fields.
     * @params  newList List<Lead>
     * @return  void
     */
    public static void setFieldNullValues(List<Lead> newList) {
        Set<String> nurtureDetailFieldValuesSet = new Set<String> {
            CON_CRM.THREE_MONTHS,
            CON_CRM.SIX_MONTHS,
            CON_CRM.NINE_MONTHS,
            CON_CRM.TWELVE_MONTHS,
            CON_CRM.FIFTEEN_MONTHS,
            CON_CRM.EIGHTEEN_MONTHS
        };
        
        for(Lead ld : newList){
            if(ld.Disposition__c == CON_CRM.MARKETING_TO_NURTURE && nurtureDetailFieldValuesSet.contains(ld.Nurture_Detail__c)){
                ld.Disposition__c = null;
                ld.Disposition_Date__c = null;
                ld.Nurture_Area__c = null;
                ld.Nurture_Detail__c = null;
                ld.Nurture_Detail_Other__c = null;
            }
        }
    }
    
    /**
     * This method is used to set previous lead score when lead score is updated.
     * @params  newList List<Lead>
     * @params  oldMap Map<Id, Lead>
     * @return  void
     */
    public static void setPreviousLeadScore(List<Lead> newList, Map<Id, Lead> oldMap) {
        for(Lead ld : newList){
            if(ld.Lead_Score__c != oldMap.get(ld.id).Lead_Score__c) {
                ld.Previous_Lead_Score__c = oldMap.get(ld.id).Lead_Score__c;
            }
        }
    }

    /**
    * PEP-ACN Lead Registration logic
    * 
    * @Desc   To set the userId with role 'Alliance Manager' to field in lead record being created
              and setting lead owner to queue
    * @params List<Lead> newList
    * @return void
    */
    public static void setUserIdForAllianceManager(List<Lead> newList, Map<Id, Lead> oldMap) { 
        
        // using the list of lead being created: List<Lead> Records
        Map<Id,Id> mapAccTeam = new map<Id,Id>();
        Set<Id> accountId = new Set<Id>();
        Id prmLeadRecTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(CON_PEP.S_PEP_LED_RECORDTYPE).getRecordTypeId();
       
        // iterate for each lead record pull the accountid 
        for(Lead leadRecord : newList){
           system.debug('leadRecord:' + leadRecord);
            // Assert the lead record have a partner account id
            if(leadRecord.PartnerAccountId != null && leadRecord.recordTypeID == prmLeadRecTypeId){
                // To set the TECH Partner Account field since standard field is not accessible via formula, workflow criteria
                leadRecord.TECH_Partner_Account__c = leadRecord.PartnerAccountId;
                accountId.add(leadRecord.PartnerAccountId);
            }
            else if(leadRecord.TECH_Partner_Account__c != null && leadRecord.recordTypeID == prmLeadRecTypeId){
                accountId.add(leadRecord.TECH_Partner_Account__c);
            }
              
        }
        
        if(accountId.size() > 0){            
            // For each account id, fetch the userid with role 'Alliance Manager'
            for(AccountTeamMember member: new SLT_AccountTeamMember().selectByAccountId(accountId, CON_PEP.ALLIANCE_MANAGER)){
                mapAccTeam.put(member.AccountId,member.userId);
            }
            
            //iterate over the Records and set the field alliance manager with user Id
            for(Lead leadRecord : newList){
                if(mapAccTeam.containsKey(leadRecord.TECH_Partner_Account__c)){
                    // Get the user Id from the custom map which contains key for the partner account id
                    leadRecord.TECH_Alliance_Manager__c = mapAccTeam.get(leadRecord.TECH_Partner_Account__c);   
                }
            }
        }
        
       
    }//end method setUserIdForAllianceManager
     

    
    /**
    * PEP-ACN Lead Registration notification to partner manager
    * 
    * @Desc   create mail notification to partner manager upon lead creation 
    *         since a related partner manager cannot be retrieved via workflow/process builder
    * @params List<Lead> newList , Map<Id, Lead> oldMap
    * @return void
    */
    public static void sendNotifToManager(List<Lead> newList, Map<Id, Lead> oldMap){

        List<Lead> registeredLeadList = new  List<Lead>();
        List<Lead> convertedLeadList = new  List<Lead>();
        List<Lead> rejectedLeadList = new  List<Lead>();
        Map<Id,List<string>> pimUsrMap= new Map<Id,List<string>>();
        Set<Id> partnerAccSet = new Set<Id>();
        Set<String> emailFieldSet = new Set<String>{'Id', 'Name', 'DeveloperName', 'Subject', 'HtmlValue', 'Body'};
        Set<String> orgWideAddFieldSet = new Set<String> {'Id'};
        Id prmLeadRecTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(CON_PEP.S_PEP_LED_RECORDTYPE).getRecordTypeId();
                    
        for(Lead lead : newList){
            //send notif only if lead is created by partner user
            if(lead.recordTypeID == prmLeadRecTypeId && CON_PEP.S_P_PEP_COMMUNITY.equalsIgnoreCase(lead.Created_By_Profile__c)){
                if(lead.PartnerAccountId != null){
                    partnerAccSet.add(lead.PartnerAccountId);
                }

                if (oldMap == null && lead.Status == CON_PEP.S_PEP_LD_STATUS_REGISTERED){
                    registeredLeadList.add(lead); 
                }

                if(lead.TECH_Partner_Account__c != null && oldMap != null){
                     partnerAccSet.add(lead.TECH_Partner_Account__c);

                    if(!oldMap.get(lead.id).IsConverted && lead.IsConverted ){    
                      convertedLeadList.add(lead);      
                    }
                    else if (oldMap.get(lead.Id).Status != lead.Status
                        && lead.Status == CON_PEP.S_PEP_LD_STATUS_REJECTED){
                        rejectedLeadList.add(lead);
                    }
                }
            }
        }

        for(User manager : new SLT_User().selectManagerUserByAccountId(partnerAccSet)){
            //partner user cannot access user email field for other users, so retrieving contact email instead
            if(pimUsrMap.containsKey(manager.AccountId)){
                pimUsrMap.get(manager.AccountId).add(manager.Contact.Email);
            } else{
                pimUsrMap.put(manager.AccountId, new List<string> {manager.Contact.Email});
            }
        }
        
         //get org-wide email address
        List<OrgWideEmailAddress> senderEmail = new SLT_OrgWideEmailAddress().selectOrgWideEmailAddressByAdress(CON_PEP.S_PEP_ORG_WIDE_ADD_NO_REPLY, orgWideAddFieldSet);
        
        if(registeredLeadList.size() >0){
            List<EmailTemplate> templateList = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(CON_PEP.S_PEP_LD_DEV_NAME_CREATION, emailFieldSet);
            sendEmailsToPartnerManager(registeredLeadList, templateList[0].Id,senderEmail[0].Id,pimUsrMap);
        }
        
        if(convertedLeadList.size() >0){
            List<EmailTemplate> templateList = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(CON_PEP.S_PEP_LD_DEV_NAME_CONVERTED, emailFieldSet);
            sendEmailsToPartnerManager(convertedLeadList, templateList[0].Id,senderEmail[0].Id, pimUsrMap);
        }
        
        if(rejectedLeadList.size() >0){
            List<EmailTemplate> templateList = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(CON_PEP.S_PEP_LD_DEV_NAME_REJECTED, emailFieldSet);
            sendEmailsToPartnerManager(rejectedLeadList, templateList[0].Id,senderEmail[0].Id, pimUsrMap);
        }

    }//end method sendNotifToManager
     
     
    /**
    * PEP-ACN Lead Registration notification to partner manager
    * 
    * @Desc   send mail notification to partner manager upon lead creation
    * @params List<Lead> newList , Map<Id, Lead> oldMap
    * @return void
    */
     public static void sendEmailsToPartnerManager(List<Lead> aLeadList, Id aEmailTemplateId, 
                                                   Id aSenderEmailId, Map<Id,List<string>> aPimUsrMap){
         
         List<Messaging.SingleEmailMessage> sendMailNotif = new List<Messaging.SingleEmailMessage>();
         
         if(aLeadList.size()>0 && aPimUsrMap.size() > 0){             
             for (Lead updatedLead : aLeadList){
                /* Now create a new single email message object that will send out a single email to the addresses in the To, CC & BCC list. */
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setOrgWideEmailAddressId(aSenderEmailId);
                // Assign the addresses for the To and CC lists to the mail object.
                if(updatedLead.PartnerAccountId != null){
                    mail.setToAddresses(aPimUsrMap.get(updatedLead.PartnerAccountId));
                }
                else{
                    mail.setToAddresses(aPimUsrMap.get(updatedLead.TECH_Partner_Account__c));
                }
                 
                mail.setTargetObjectId(updatedLead.Id);
                //Specify Template used
                mail.setTemplateId(aEmailTemplateId);
                sendMailNotif.add(mail);
             
             }
             
              if(sendMailNotif.size() > 0) {
                  List<Messaging.sendEmailResult> sendEmailResults = Messaging.sendEmail(sendMailNotif);
                  
                  if(sendEmailResults.size() > 0){
                      
                      for(integer i; i< sendEmailResults.size(); i++){
                          if(!sendEmailResults[i].isSuccess()){
                              Messaging.SendEmailError[] errArr = sendEmailResults[i].getErrors();
                              aLeadList[i].addError(errArr[0].getTargetObjectId()+ 'Error sending mail: Status Code-'
                                                    + errArr[0].getStatusCode() + 'Error Message-'+ errArr[0].getMessage());
                          }

                      }

                  }
              }
          }
      }

    /**
    * PEP-ACN Lead Share logic
    * 
    * @Desc   define visibility and access rights of lead within partner account users
    * @params List<Lead> newList
    * @return void
    */
      public static void defineSharingRuleForPartner (List<Lead> newList , Map<Id, Lead> oldMap){

        UTL_PEP_LeadSharing.shareLeadWithPartner(newList , oldMap);

      }
 }