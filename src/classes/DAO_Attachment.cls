/**
 * This is Attachment DAO class.
 * version: 1.0
 */
public class DAO_Attachment extends fflib_SObjectDomain {


	/**
	 * Constructor of this class
	 * @params sObjectList List<Attachment>
	 */
	public DAO_Attachment(List<Attachment> sObjectList) {
		super(sObjectList);
	}

	/**
	 * Constructor Class to construct new Instance of This Class
	 */
	public class Constructor implements fflib_SObjectDomain.IConstructable {
		public fflib_SObjectDomain construct(List<SObject> sObjectList) {
			return new DAO_Attachment(sObjectList);
		}
	}

	/**
	 * This method is used for before insert of the Attachment trigger.
	 * @return void
	 */
	public override void onBeforeInsert() {
		//This is the section where all the methods that needs to be run at first are included.
		//This should be at the top since the trigger execution flags are reset in this method

		//This is the section where all the methods that needs to be run in a normal sequence are included.
		DAOH_Attachment.handleOnBeforeInsertAttachment((List<Attachment>)Records);

		DAOH_Attachment.updateContractOnAttachment((List<Attachment>)Records);
		//This is the section where all the methods that needs to be run at last are included.
		//This should be at the last since it will integrate the opp with the latest changes
	}
    
    /**
     * Override method Before Delete Call
     */
    public override void onBeforeDelete() {
        //This is the section where all the methods that needs to be run at first are included.
        
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        DAOH_Attachment.handleOnBeforeDeleteAttachment((List<Attachment>)Records);
        
        //This is the section where all the methods that needs to be run at last are included.
    } 
    
    /**
     * Override method After Update Call
     */
    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {
        //This is the section where all the methods that needs to be run at first are included.
        
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        //DAOH_Opportunity.createTeamMemberForPICUser(new Map<Id, Opportunity>((List<Opportunity>)Records), (Map<Id, Opportunity>)existingRecords);
        DAOH_Attachment.handleOnAfterUpdateAttachment((List<Attachment>)Records); 
        
        //This is the section where all the methods that needs to be run at last are included.
        //This should be at the last since it will set the sync fields with the latest changes    
    }
}