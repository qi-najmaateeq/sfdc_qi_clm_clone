/*
 * Version       : 1.0
 * Description   : Test Class for CNT_CSM_CSMNews
 */
@isTest
private class  TST_CNT_CSM_CSMNews {
    
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        CSM_QI_News__c csmnews = new CSM_QI_News__c(Name='testName', News__c='testNews', Mode__c='Published');
        insert csmnews;
    }
    
    /**
     * This method used to get all CSM_QI_News__c
     */    
    @IsTest
    static void testGetCSMNews() {
        List<CSM_QI_News__c> csmnews = new  List<CSM_QI_News__c>();
        Test.startTest();
        csmnews = CNT_CSM_CSMNews.getCSMNews();  
        Test.stopTest();
        Integer expected = 1;
        Integer actual = csmnews.size();
        System.assertEquals(expected, actual);
    }
}