/*
 * Version       : 1.0
 * Description   : Test Class for CNT_CSM_KnowledgeBrowser
 */
@isTest
public class TST_CNT_CSM_KnowledgeBrowser {
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        Product2 product = new Product2(Name = 'OneKey', ProductCode = '1234', isActive = true, CanUseRevenueSchedule = true, InterfacedWithMDM__c = true);
        product.Community_Topics__c='OneKey';
        product.SpecificToCSM__c = True;
        insert product;
        
        Id recordType =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
        CSM_QI_Case_Categorization__c categorization = new CSM_QI_Case_Categorization__c(Product__c = product.Id, SubType1__c='TestSubtype1', SubType2__c='TestSubtype2 1',RecordTypeId__c=recordType);
        insert categorization;
        categorization = new CSM_QI_Case_Categorization__c(Product__c = product.Id, SubType1__c='TestSubtype1', SubType2__c='TestSubtype2 2',RecordTypeId__c=recordType);
        insert categorization;
        
        Knowledge__kav knowledge = New Knowledge__kav(Title = 'TestTitle', language = 'en_US',UrlName='TestUrlName', IsVisibleInCsp= true, ProductName__c='OneKey');
        insert knowledge;
        Knowledge__kav k = [SELECT Id,KnowledgeArticleId FROM Knowledge__kav WHERE Id=:knowledge.Id];
        KbManagement.PublishingService.publishArticle(k.KnowledgeArticleId, true);
    }
    
    /**
     * This method used to get a List of Knowledge__kav by productName
     */    
    @IsTest
    static void testGetArticlesByProductName() {
        List<Knowledge__kav> articles = new  List<Knowledge__kav>();
        Test.startTest();
        articles = CNT_CSM_KnowledgeBrowser.getArticlesByProductName('OneKey');
        Test.stopTest();
        Integer expected = 1;
        Integer actual = articles.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get a List of products with Community_Topics__c not null and and present in CaseCategorization Object 
     */    
    @IsTest
    static void testGetProducts() {
        List<AggregateResult> products = new List<AggregateResult>();
        Test.startTest();
        products = CNT_CSM_KnowledgeBrowser.getProducts();
        Test.stopTest();
        Integer expected = 1;
        Integer actual = products.size();
        System.assertEquals(expected, actual);
    }
}