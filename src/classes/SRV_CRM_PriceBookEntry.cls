/*
 * Version       : 1.0
 * Description   : Service Class for PriceBookEntry
 */
global class SRV_CRM_PriceBookEntry {
    
    /**
     * service method to get filter pricebookentries
     * @params  Product2 fiterProductObj
     * @params  Set<String> pbEntryFieldSet
     * @params  List<String> fieldsAPIList
     * @params  Integer recordLimit
     * @return  OpportunityWrapper
     */
    public static List<PriceBookEntryWrapper> getFilteredPriceBookEntries(PriceBookEntryWrapper pbeWrapper, Set<String> pbeFieldSet, List<String> productFilterFieldList, List<String> pbeFilterFieldList, Integer recordLimit) {
        List<PriceBookEntryWrapper> pbEntryWrapperList = new List<PriceBookEntryWrapper>();
        List<PriceBookEntry> pbEntryList = null;
        try {
            if(pbeWrapper.productRecord.Territory__c != null) {
                Map<String, Territory_lookup__mdt> territoryLookupMetadataMap = new SLT_TerritoryLookup().getTerritoryLookup();
                if(territoryLookupMetadataMap.containsKey(pbeWrapper.productRecord.Territory__c)) {
                    String territoryRegion = territoryLookupMetadataMap.get(pbeWrapper.productRecord.Territory__c).AND_Territory_Region__c ;
                    String territories = '';
                    territories = CON_CRM.BACKSLASH + pbeWrapper.productRecord.Territory__c + CON_CRM.BACKSLASH + CON_CRM.COLON + CON_CRM.BACKSLASH + territoryRegion + CON_CRM.BACKSLASH + CON_CRM.COLON + CON_CRM.BACKSLASH + CON_CRM.GLOBAL_REGION + CON_CRM.BACKSLASH;
                    if(territories != null) {
                        pbeWrapper.productRecord.Territory__c = territories; 
                    }
                } else {
                    String territories = '';
                    territories = CON_CRM.BACKSLASH + pbeWrapper.productRecord.Territory__c + CON_CRM.BACKSLASH + CON_CRM.COLON + CON_CRM.BACKSLASH + CON_CRM.GLOBAL_REGION + CON_CRM.BACKSLASH;
                    if(territories != null) {
                        pbeWrapper.productRecord.Territory__c = territories; 
                    }
                }
            }
            pbEntryList = new SLT_PriceBookEntry().getPbEntriesByFilterParams(pbeWrapper, pbeFieldSet, productFilterFieldList, pbeFilterFieldList, recordLimit);
            for(PriceBookEntry pbEntry : pbEntryList) {
                pbEntryWrapperList.add(new PriceBookEntryWrapper(pbEntry.Product2, pbEntry));
            }
        } catch(Exception ex) {
            throw new PriceBookEntryServiceException(new List<String>{ex.getMessage()});
        }   
        return pbEntryWrapperList;
    }
    
    /**
     * service method to get favorite products
     * @params  Integer recordLimit
     * @return  OLIWrapper
     */
    public static List<PriceBookEntryWrapper> getUserFavoriteProducts(Id oppId) {
        List<PriceBookEntry> priceBookEntryList = null;
        List<PriceBookEntryWrapper> pbeWrapperList = new List<PriceBookEntryWrapper>();
        Set<Id> productIdSet = new Set<Id>();
        Map<Id, Opportunity> idToOpportunityMap = new Map<Id, Opportunity>();
        try {
            List<Favorite_Product__c> favoriteProductList = new SLT_FavoriteProduct().getUserFavoriteProducts();
            for(Favorite_Product__c fav : favoriteProductList) {
                productIdSet.add(fav.Product__c);
            }
            if(productIdSet.size() > 0) {
                Set<id> oppIdSet = new Set<Id>();
                oppIdSet.add(oppId);
                idToOpportunityMap = new SLT_Opportunity().getOpportunityById(oppIdSet, new Set<String>{'Id','CurrencyIsoCode'});
                set<String> fieldset = new Set<String>{'id','Product2.Name','Product2.Hierarchy_Level__c','Product2.Description','Product2.ProductCode', 'Product2.CanUseRevenueSchedule', 'Product2.Material_Type__c', 'Product2.Offering_Group_Code__c', 'Product2.Delivery_Type__c'};
                priceBookEntryList = new SLT_PriceBookEntry().getPbEntriesByProductIds(productIdSet, fieldset, idToOpportunityMap.get(oppId).CurrencyIsoCode);
                for(PriceBookEntry pbe : priceBookEntryList) {
                    pbeWrapperList.add(new PriceBookEntryWrapper(pbe.Product2, pbe));
                }
            }
        } catch(Exception ex) {
            String errMsg = ex.getMessage() + ' ' + ex.getStackTraceString();
            throw new PriceBookEntryServiceException(new List<String>{errMsg});
        }
        return pbeWrapperList;
    }
    
    // Exception Class for PriceBookEntry Service
    public Class PriceBookEntryServiceException extends Exception {
        
        List<String> errorList;
        
        /**
         * constructor
         * @params  List<String> errorList
         */ 
        public PriceBookEntryServiceException(List<String> errorList) {
            this.errorList = errorList;
        }
    }


}