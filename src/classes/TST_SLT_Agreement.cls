@isTest
private class TST_SLT_Agreement {

    static Apttus__APTS_Agreement__c setAgreementData(Boolean markAsPrimary, Id OpportuntiyId){

        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.Mark_as_Primary__c = markAsPrimary;
        testAgreement.RecordTypeId = recordTypeId;
        return testAgreement;
    }

    static Opportunity setOpportunityData(Id accountId){

        Opportunity testOpportunity= UTL_TestData.createOpportunity(accountId);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;
        return testOpportunity;
    }

    static Account setAccountData(){

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;
        return testAccount;
    }

    @isTest
    static void testSelectAgreementById() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id);
        insert testAgreement;
        Set<Id> idSet = new Set<Id>{testAgreement.Id};

        Test.startTest();
            List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().selectById(idSet);
        Test.stopTest();
        
        System.assertEquals(1, agreementList.size(), 'Should return one agreement');
    }

    @isTest
    static void testGetRelatedOpportunityPrimaryAgreement() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement1 = setAgreementData(true, testOpportunity.Id);
        insert testAgreement1;
        Apttus__APTS_Agreement__c testAgreement2 = setAgreementData(true, testOpportunity.Id);
        testAgreement2.Apttus__Workflow_Trigger_Created_From_Clone__c = true;
        insert testAgreement2;
        Set<Id> opportunityIds = new Set<Id>{testOpportunity.Id};
        Set<Id> currentAgreementIds = new Set<Id>{testAgreement2.Id};
        Set<String> fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.AGREEMENT_MARK_AS_PRIMARY};

        Test.startTest();
            List<Apttus__APTS_Agreement__c> agreementList = new
            SLT_Agreement().getRelatedOpportunityPrimaryAgreement(opportunityIds, currentAgreementIds, fieldSet);
        Test.stopTest();

        System.assertEquals(testAgreement1.Id, agreementList[0].Id, 'Should return second agreement');
    }

    @isTest
    static void testGetAgreementDetailsByIdAndPricingTool() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id);
		testAgreement.Select_Pricing_Tool__c = CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP;
        insert testAgreement;

        Set<String> fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL};

        Test.startTest();
            Apttus__APTS_Agreement__c agreement = new
            SLT_Agreement().getAgreementDetails(testAgreement.Id, fieldSet);
        Test.stopTest();

        System.assertEquals(CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_CPQ_QIP, agreement.Select_Pricing_Tool__c, 'Should Return QIP');
    }
    
    @isTest
    static void testgetAgreementDetailsByIdAndRecorType() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id);
        insert testAgreement;
        Set<String> fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.AGREEMENT_BId_HISTORY_NUMBER};
        Set<Id> recordtypeIds = new Set<Id>{
        SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId()};

        Test.startTest();
            List<Apttus__APTS_Agreement__c> agreementList = new
                SLT_Agreement().getAgreementDetailsByIdAndRecorType(testAgreement.Id, recordtypeIds, fieldSet);
        Test.stopTest();

        System.assertEquals(1, agreementList.size(), 'Should Return Agreement');
    }
    
    @isTest
    static void testgetAgreementDetailsByIdsAndRecorType() {

        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id);
        insert testAgreement;
        Set<String> fieldSet = new Set<String> {CON_CPQ.ID, CON_CPQ.AGREEMENT_BId_HISTORY_NUMBER};
        Set<Id> recordtypeIds = new Set<Id>{
        SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_REBID).getRecordTypeId()};

        Test.startTest();
            List<Apttus__APTS_Agreement__c> agreementList = new
                SLT_Agreement().getAgreementDetailsByIdsAndRecorType(new set<Id>{testAgreement.Id}, recordtypeIds, fieldSet);
        Test.stopTest();

        System.assertEquals(1, agreementList.size(), 'Should Return Agreement');
    }
    
    @isTest
    static void testSelectBudgetAgreementHistory(){
        Account testAccount = setAccountData();
        Opportunity testOpportunity = setOpportunityData(testAccount.Id);
        Apttus__APTS_Agreement__c testAgreement = setAgreementData(true, testOpportunity.Id);
        insert testAgreement;
        
        testAgreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
        testAgreement.Process_Step__c = CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL;
        update testAgreement;
		
        List<Apttus__APTS_Agreement__c> testagreementList =  new List<Apttus__APTS_Agreement__c> {testAgreement};
            
        Test.startTest();
        	List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().selectBudgetAgreementHistory(testagreementList);
        Test.stopTest();
        
        System.assertEquals(1, agreementList.size(), 'Should Return Agreement');
    }
}
