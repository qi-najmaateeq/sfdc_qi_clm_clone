@isTest
public class TST_UTL_OWF_TestData{
    @isTest
    public static void testMethod1(){
        UTL_OWF_TestData.createUser('System Administrator',1);
        UTL_OWF_TestData.createAccount();
        UTL_OWF_TestData.createContact(null);
        UTL_OWF_TestData.createOpportunity(null);
        UTL_OWF_TestData.createAgreement(null,null);
        UTL_OWF_TestData.createAgreementByRecordType(null,null,null);
        UTL_OWF_TestData.createGroup();
        UTL_OWF_TestData.createPermissionControl(null,null,null,null);
        UTL_OWF_TestData.createOWFConfig(null);
        UTL_OWF_TestData.createBidProject(null);
        UTL_OWF_TestData.createResourceRequest(null,null,null);
        UTL_OWF_TestData.createSchedule();
        UTL_OWF_TestData.createAssignment(null,null,null,null,null);
        UTL_OWF_TestData.setupOWFBatchConfig('test');
        UTL_OWF_TestData.setupOWFConfig(null);
        UTL_OWF_TestData.createSkills('Test','test');
        UTL_OWF_TestData.createResourceSkillRequest(null,null);
        UTL_OWF_TestData.createSkillCertificationRating(null,null);
        UTL_OWF_TestData.createIndication('Test','test');
        UTL_OWF_TestData.createLineItemGroup(null);
        UTL_OWF_TestData.createDaysOff(null);
    }
}