public class SLT_ContentDocument extends fflib_SObjectSelector{

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
            ContentDocument.Id,
            ContentDocument.CreatedById,
            ContentDocument.CreatedDate,
            ContentDocument.IsArchived,
            ContentDocument.ArchivedDate,
            ContentDocument.IsDeleted,
            ContentDocument.OwnerId,
            ContentDocument.Title,
            ContentDocument.PublishStatus,
            ContentDocument.Description,
            ContentDocument.FileExtension,
            ContentDocument.FileType,
            ContentDocument.ContentSize,
            ContentDocument.ContentModifiedDate,
            ContentDocument.LastViewedDate,
            ContentDocument.LatestPublishedVersionId
        };
    }
    
    public Schema.SObjectType getSObjectType() {
        return ContentDocument.sObjectType;
    }
    
    public List<ContentDocument> selectById(Set<ID> idSet) {
        return (List<ContentDocument>) selectSObjectsById(idSet);
    }
    
    public List<ContentDocumentLink> getDocumentsOnContentIdAndLinkedId(List<ID> caseIdList, List<ID>contentIdList) {
        return (List<ContentDocumentLink>)Database.query('SELECT Id, Visibility, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentIdList And LinkedEntityId IN :caseIdList');
    }
}
