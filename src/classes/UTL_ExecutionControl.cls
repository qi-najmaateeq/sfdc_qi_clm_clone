/**
 * This class used for control the execution of triggers
 * version : 1.0
 */
public class UTL_ExecutionControl {
    public static Boolean stopTriggerExecution = false;
    public static Boolean olisTriggerAlreadyRun = false;
    public static Boolean stopMilestoneTriggerExecution = false;
    //Flag for OWF module
    public static Boolean stopTriggerExecution_OWF = false;
}