global class AgreementLifecycle extends Apttus.AbstractAgreementLifecycleCallback {

    global override void afterClone(Apttus__APTS_Agreement__c originalSO, Apttus__APTS_Agreement__c cloneSO){
    
        System.debug('Parent: '+originalSO.Id);
        System.debug('Cloned: '+cloneSO.Id);
        cloneSO.Agreement_Cloned_From__c = originalSO.Id;
    }

    global override void afterComplete(Apttus__APTS_Agreement__c agreementSO, Apttus.CustomClass.ActionType actionType) {

        agreementSO = [SELECT Id, Agreement_Cloned_From__c, Agreement_Cloned_From__r.Apttus__FF_Agreement_Number__c 
                        FROM Apttus__APTS_Agreement__c WHERE Id = :agreementSO.Id LIMIT 1];

        System.debug('AgreementSO: '+agreementSO.Id);
        System.debug('Agreement Cloned From: '+agreementSO.Agreement_Cloned_From__c);
        System.debug('IS Clone Complete?: '+(actionType == Apttus.CustomClass.ActionType.ACTION_CLONE));

        if(actionType == Apttus.CustomClass.ActionType.ACTION_CLONE){

            List<Attachment> attachments = [SELECT Id, Name, Body, ContentType, IsPrivate, CreatedDate 
                                            FROM Attachment 
                                            WHERE ParentId = :agreementSO.Agreement_Cloned_From__c 
                                            ORDER BY CREATEDDATE DESC 
                                            LIMIT 1];
											
            if(attachments.size()>0) {
                copyAttachment(agreementSO, attachments);
            }
        }
    }

    private void copyAttachment(Apttus__APTS_Agreement__c agreementSO, List<Attachment> attachments) {
        Attachment newAttachment = attachments[0].clone();
        newAttachment.ParentId  = agreementSO.Id;
        newAttachment.Description = 'Clone recent file from agreement: '+agreementSO.Agreement_Cloned_From__r.Apttus__FF_Agreement_Number__c;
        try{  
            insert newAttachment;
        }catch(DMLException dmlExp){
            System.debug(System.LoggingLevel.ERROR, dmlExp.getMessage() + ' \n ' + dmlExp.getStackTraceString());
        }
    }	
}