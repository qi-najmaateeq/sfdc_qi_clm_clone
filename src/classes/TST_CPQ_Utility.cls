@isTest
private class TST_CPQ_Utility {
	
    @isTest
    static void testGetFoundationRecordTypesShouldReturnRecordType(){
        
        Test.startTest();
        	Set<Id> recordTypesId = CPQ_Utility.getFoundationRecordTypes();
        Test.stopTest();
        
        system.assertNotEquals(null, recordTypesId.size(), 'Should Return Foundation Record type');
    }
    
    @isTest
    static void testGetContractRecordTypesShouldReturnRecordType(){
        
        Test.startTest();
        	Set<Id> recordTypesId = CPQ_Utility.getContractRecordTypes();
        Test.stopTest();
        
        system.assertNotEquals(null, recordTypesId.size(), 'Should Return Contract Record type');
    }
    
    @isTest
    static void testGetInitialRecordTypesShouldReturnRecordType(){
        
        Test.startTest();
        	Set<Id> recordTypesId = CPQ_Utility.getInitialRecordTypes();
        Test.stopTest();
        
        system.assertNotEquals(null, recordTypesId.size(), 'Should Return Initial Record type');
    }
    
    @isTest
    static void testGetInitialBidRecordTypessShouldReturnRecordType(){
        
        Test.startTest();
        	Set<Id> recordTypesId = CPQ_Utility.getInitialBidRecordTypes();
        Test.stopTest();
        
        system.assertNotEquals(null, recordTypesId.size(), 'Should Return Initial Bid Record type');
    }
    
    @isTest
    static void testGetSingleMessage(){
        
        Account testAccount = new Account(Name = 'test');
        insert testAccount;
        
        Test.startTest();
        	Messaging.SingleEmailMessage Mail = CPQ_Utility.getSingleMessage(CON_CPQ.CPQ_TASK_COMPLETION_NOTIFICATION_TO_PD, testAccount.Id, UserInfo.getUserId(), new List<String>{'test@test.com'});
        	Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        
        System.assertNotEquals(null, Mail, 'Return Mail');
    }
    
}