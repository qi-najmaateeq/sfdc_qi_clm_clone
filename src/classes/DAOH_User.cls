public class DAOH_User {
    /**
     * This method is used for add KB Article Manager to the user when the Article Manager checkbox selected.
     * @params  newList List<Contact>,Map<Id,Contact> oldMap
     * @return  void
     */    
    public static void addPermissionSetAfterInsertCSM(List<User> newList){
        String[] sUsers = new List<String>();
        PermissionSet ps = null;
        for (User c : newList) {
            if(c.Article_Manager__c == true) {
                sUsers.add(c.Id);
            }
        }
        
        if(sUsers.isEmpty() == false){
            ps = Database.query('SELECT Id, Name FROM PermissionSet WHERE IsOwnedByProfile = False and Name=\'KB_Article_Manager\' Limit 1');
            if(sUsers.isEmpty() == false && ps != null){
                SRV_CSM_AssignPermissionSet.AssignPermissionSetToUsers(sUsers, ps.Id);
            }  
        }
        
    }
    /**
    * This method is used for add KB Article Manager to the user when the Article Manager checkbox selected.
    * @params  newList List<User>
    * @return  void
    */
    public static void PermissionSetAfterUpdateCSM(List<User> newList,Map<Id,User> oldMap){
        String[] sUsers = new List<String>();
        String[] sDeleteUsers = new List<String>();
        PermissionSetAssignment psa =null;
        List<PermissionSetAssignment> listPSAList = null;
        PermissionSet ps =null;
        for (User c : newList) {
            System.debug(' Check ::'+(c.Article_Manager__c != oldMap.get(c.Id).Article_Manager__c));
            if(c.Article_Manager__c != null && c.Article_Manager__c != oldMap.get(c.Id).Article_Manager__c){
                if(c.Article_Manager__c == true) {
                    sUsers.add(c.Id);
                }else if(c.Article_Manager__c == false) {
                    sDeleteUsers.add(c.Id);
                }    
            }
        }
        if(sUsers.isEmpty() == false || sDeleteUsers.isEmpty() == false){
            ps = Database.query('SELECT Id, Name FROM PermissionSet WHERE IsOwnedByProfile = False and Name=\'KB_Article_Manager\' Limit 1');
        }
        
        if(sUsers.isEmpty() == false  && ps!=null){
            SRV_CSM_AssignPermissionSet.AssignPermissionSetToUsers(sUsers, ps.Id);
        }
        if(sDeleteUsers.isEmpty() == false && ps!=null){
            SRV_CSM_AssignPermissionSet.DeletePermissionSetToUsers(sDeleteUsers, ps.Id); 
        }
    }

    public static void updateContactOnPortalUser(List<User> userList){
        Set<Id> userIdSet = new Set<Id>();
        for(User userRecord : userList) {
            userIdSet.add(userRecord.Id);
        }
        try{
            if(userIdSet.size()>0){
                updateContactOnUserId(userIdSet);
            }
        }
        catch(Exception e){
            e.getMessage();
        }
    }
    
    @Future
    private static void updateContactOnUserId(Set<Id> userIdSet){
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> profileIdSet = new Set<Id>();
        List<User> userList = [SELECT Id, ProfileId, isActive, ContactId, LastLoginDate FROM User WHERE Id IN: userIdSet];
        DateTime lastLogin ;
        lastLogin = userList[0].LastLoginDate;
        for(User userRecord : userList) {
            profileIdSet.add(userRecord.profileId);
            contactIdSet.add(userRecord.contactId);
        }
        
        Map<Id, Profile> profileMap = new Map<Id, Profile>([SELECT Id, Name FROM Profile WHERE Id IN :profileIdSet]);
        Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Id, Name, CSH_User__c FROM Contact WHERE Id IN :contactIdSet]);
        
	    if(contactMap != null){
        for(User usrObj : userList){
        if(contactMap != null && profileMap != null && contactMap.containsKey(usrObj.ContactId)){
            if(profileMap.get(usrObj.ProfileId).Name.equalsIgnoreCase(CON_CSM.S_P_CSM_COMMUNITY) || profileMap.get(usrObj.ProfileId).Name.equalsIgnoreCase(CON_CSM.S_P_CUSTOMER_COMMUNITY)){
                if(usrObj.IsActive){
                    contactMap.get(usrObj.ContactId).CSH_User__c = CON_CSM.S_YES;
                }
                else{
                    contactMap.get(usrObj.ContactId).CSH_User__c = CON_CSM.S_INACTIVE;
                }
            }
            else{
                contactMap.get(usrObj.ContactId).CSH_User__c = CON_CSM.S_NO;
            }
            contactMap.get(usrObj.ContactId).CSH_User_Last_connection_date__c = lastlogin ;
        }
        }
        update contactMap.values();
        }
    }

    public static void setupFieldValues(List<User> newList, Map<Id, User> oldMap){
        for (User u : newList){
            //  If active user is inserted or existing inactive user is updated and active is changed from false to true
            if (u.IsActive == true && (oldMap == null || oldMap.get(u.id).IsActive != u.IsActive)){
                u.Activation_Date__c = system.now();
            }
        }
    }
    
    /**
    * @author dirish.bhaugeerutty
    * This method is used by the batch BCH_CSM_BatchQueueUser to create or del queue records
    * @return void
    */
    public static void manageQueuesRelations(){

        Map<Id, Group> groupsById = new Map<Id, Group>([SELECT Id, Name,Email FROM Group WHERE type ='Queue' LIMIT 50000]);
        Map<Id,Queue_User_Relationship__c> existingQRGrpIds = new Map<Id,Queue_User_Relationship__c>();
        List<Queue_User_Relationship__c> newQueueRelations = new List<Queue_User_Relationship__c>();
        List<Queue_User_Relationship__c> updateQueueRelations = new List<Queue_User_Relationship__c>();

        //Look for group Ids which have been deleted
        List<Queue_User_Relationship__c> queueUserRelationsToDel = [SELECT Id, Name, TECH_UserGroupIdCombination__c, 
        Type__c, User__c, Group_Id__c, Queue_Email__c
        FROM Queue_User_Relationship__c 
        WHERE Group_Id__c NOT IN: groupsById.keySet() LIMIT 50000];

        //delete any record where Group_Id__c = deleted group Id
        if(queueUserRelationsToDel.size() > 0){
            try{
                Database.delete(queueUserRelationsToDel);
            }catch(Exception e){
                System.debug('An error occured deleting redundant queue user relations');
            }
        }        

        //Look for all Queue_User_Relationship__c of type = 'Queue'
        List<Queue_User_Relationship__c> queueUserRelations = [SELECT Id, Name, TECH_UserGroupIdCombination__c, 
        Type__c, User__c, Group_Id__c, Queue_Email__c 
        FROM Queue_User_Relationship__c 
        WHERE Type__c ='Queue' AND Group_Id__c != NULL LIMIT 50000];        

        //store all existing relationships Ids
        for(Queue_User_Relationship__c curRel: queueUserRelations){
            existingQRGrpIds.put(curRel.Group_Id__c,curRel);
        }

        Queue_User_Relationship__c newQueueRel;
        Queue_User_Relationship__c updateQueueRel;
        for(Id currId : groupsById.keySet()){
            
            //check if relation absent for current Id
            if(!existingQRGrpIds.keySet().contains(currId)){
                
                //create relation
                newQueueRel = new Queue_User_Relationship__c();
                newQueueRel.Name = groupsById.get(currId).Name;
                newQueueRel.Type__c = 'Queue';
                newQueueRel.Group_Id__c = currId;

                newQueueRelations.add(newQueueRel);
            }else if(existingQRGrpIds.keySet().contains(currId) && (!(existingQRGrpIds.get(currId).Name.equalsIgnoreCase(groupsById.get(currId).Name)) || (groupsById.get(currId).Email != null && !groupsById.get(currId).Email.equalsIgnoreCase(existingQRGrpIds.get(currId).Queue_Email__c)) ) ){
                updateQueueRel = existingQRGrpIds.get(currId);
                updateQueueRel.Name = groupsById.get(currId).Name;
                if((groupsById.get(currId).Email != null && !groupsById.get(currId).Email.equalsIgnoreCase(existingQRGrpIds.get(currId).Queue_Email__c))){
                    updateQueueRel.Queue_Email__c = groupsById.get(currId).Email;
                }else{
                    updateQueueRel.Queue_Email__c = null;
                }
                updateQueueRelations.add(updateQueueRel);
            }
        }

        if(newQueueRelations.size() > 0){
            //insert new queues to DB
            try{
                Database.insert(newQueueRelations);
            }catch(Exception e){
                System.debug('An error occured during insertion of new queue: '+e);
            }
        }
        
        if(updateQueueRelations.size() > 0){
            //Update Queue Name Change
            try{
                Database.update(updateQueueRelations);
            }catch(Exception e){
                System.debug('An error occured during updating the queue: '+e);
            }
        }
    }


    /**
    * @author dirish.bhaugeerutty
    * This method is used by the batch BCH_CSM_BatchQueueUser to create or del queue records
    * @return void
    */
    public static void manageUserQueuesRelations(List<Group> aGroups){
        
        List<Queue_User_Relationship__c> newQueueUserRelations = new List<Queue_User_Relationship__c>();
        List<Queue_User_Relationship__c> queueUserRelationsToDel = new List<Queue_User_Relationship__c>();
          List<Queue_User_Relationship__c> updateQueueUserRelations = new List<Queue_User_Relationship__c>();
        //customKey = userId + Group_Id__c
        Map<String, Queue_User_Relationship__c> queueUserRelationByCustomKey = new Map<String, Queue_User_Relationship__c>();
        Set<Id> existingUserIds = new Set<Id>();

        //look for current group members
        List<GroupMember> currentGroupMembers = [SELECT group.Id, group.Name, UserOrGroupId FROM GroupMember 
        WHERE group.Type ='Queue' 
        AND group.Id =: aGroups[0].Id LIMIT 50000];

        //Get all queue user relationship for current group
        List<Queue_User_Relationship__c> queueUserRelations = [SELECT Id, Name, TECH_UserGroupIdCombination__c, 
        Type__c, User__c, Group_Id__c 
        FROM Queue_User_Relationship__c 
        WHERE Group_Id__c =: aGroups[0].Id LIMIT 50000];


        Id currentGroupId;
        for(Queue_User_Relationship__c currQUserRel : queueUserRelations){

            if(currQUserRel.Type__c =='User'){
                queueUserRelationByCustomKey.put(currQUserRel.TECH_UserGroupIdCombination__c, currQUserRel);
            }else{
                currentGroupId = currQUserRel.Id;
            }
        }

        Queue_User_Relationship__c newUserRel;
        Queue_User_Relationship__c updateQueueRel;
        for(GroupMember currGrpMem : currentGroupMembers){
            
            String customKey =  String.valueOf(currGrpMem.UserOrGroupId) + String.valueOf(currGrpMem.group.Id);

            existingUserIds.add(currGrpMem.UserOrGroupId);

            //new members
            if(!queueUserRelationByCustomKey.containsKey(customKey)){

                newUserRel = new Queue_User_Relationship__c();

                newUserRel.Name = currGrpMem.group.Name;
                newUserRel.User__c = currGrpMem.UserOrGroupId;
                newUserRel.Type__c = 'User';
                newUserRel.Group_Id__c = currGrpMem.group.Id;
                newUserRel.TECH_QueueUser__c = currentGroupId;

                newQueueUserRelations.add(newUserRel);
            }else if(queueUserRelationByCustomKey.containsKey(customKey) && !(queueUserRelationByCustomKey.get(customKey).Name.equalsIgnoreCase(currGrpMem.group.Name)) ){
                //Updating Queue Name in Queue User Relation
                updateQueueRel = queueUserRelationByCustomKey.get(customKey);
                updateQueueRel.Name = currGrpMem.group.Name;
                updateQueueUserRelations.add(updateQueueRel);
            }
        }

        for(Queue_User_Relationship__c currRel : queueUserRelations){

            //departing members
            if(currRel.Type__c=='User' && !existingUserIds.contains(currRel.User__c)){
                queueUserRelationsToDel.add(currRel);
            }
        }  


        if(newQueueUserRelations.size() > 0){
            
            try{
                Database.insert(newQueueUserRelations);
            }catch(Exception e){
                System.debug('An error occured creating queue user relations '+e);
            }
        }
        if(updateQueueUserRelations.size() > 0){
            
            try{
                Database.update(updateQueueUserRelations);
            }catch(Exception e){
                System.debug('An error occured updating queue name '+e);
            }
        }
        if(queueUserRelationsToDel.size() > 0){
            try{
                Database.delete(queueUserRelationsToDel);
            }catch(Exception e){
                System.debug('An error occured deleting queue user relations '+e);
            }
        }
        
    }
    
    /**
    * This method is used to create/update contact when a user is updated
    * @params  newList List<User>,oldMap Map<Id, User> 
    * @return  void
    */
    @future
    public static void createOrUpdateContactForUsers(String jsonUserList, String jsonUserOldMap) {
        List<User> newList = (List<User>)JSON.deserialize(jsonUserList, List<User>.Class);
        Map<Id, User> oldMap;
        if(jsonUserOldMap != null) {
            oldMap = (Map<Id, User>)JSON.deserialize(jsonUserOldMap, Map<Id, User>.Class);
        }
        Set<Id> salesforceUserIdSet = new set<Id>();
        for(User usr : newList) {
            if(!usr.IsPortalEnabled) {
                salesforceUserIdSet.add(usr.id);
            }
        }
        Set<String> fieldSet = new Set<String>();
        fieldSet.add(CON_CRM.SALESFORCE_USER);
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(CON_CRM.USER_CONTACT_SYNC).getDescribe().fields.getMap();
        List<Schema.describefieldresult> listOfDescribeFields = new List<Schema.describefieldresult>();
        for(Schema.SObjectField sfield : fieldMap.Values()) {
            listOfDescribeFields.add(sfield.getDescribe());
        }
        SObject userContactSync = User_Contact_Sync__c.getInstance();
        for(Schema.describefieldresult dfield : listOfDescribeFields) {
            if(dfield.getName().contains(CON_CRM.UNDERSCORE_C)) {
                String synchField = String.valueOf(userContactSync.get(dfield.getName()));
                if(synchField != null) {
                    fieldSet.add(synchField.split(CON_CRM.COMMA)[1]);
                }
            }
        }
        
        Set<String> setOfUserLicense = new Set<String>{CON_CRM.SALESFORCE_LICENSE, CON_CRM.SALESFORCE_PLATFORM_LICENSE, CON_CRM.CHATTER_FREE_LICENSE};
        Set<String> userFieldSet = new Set<String>{'Profile.UserLicense.Name'};
        Map<Id, User> userIdToUserMap = new Map<Id, User>();
        if(salesforceUserIdSet.size() > 0) {
            userIdToUserMap = new SLT_User().selectByUserId(salesforceUserIdSet, userFieldSet);
        }
        
        List<Contact> existingContacts = new List<Contact>();
        Map<Id, Contact> userIdToContactMap = new Map<Id, Contact>();
        if(jsonUserOldMap != null) {
            existingContacts = new SLT_Contact().selectBySalesforceUserIdList(salesforceUserIdSet, fieldSet);
            for(Contact con : existingContacts) {
                userIdToContactMap.put(con.Salesforce_User__c, con);
            }
        }
        
        Boolean isContactCreated = false;
        SObject newContact;
        SObject clonedNewUser;
        SObject clonedOldUser;
        SObject updateContact;
        Boolean isContactUpdated;
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Contact.SobjectType
            }
        );
        Boolean toCommit = false;
        
        for(User usr : newList) {
            if(userIdToUserMap.containsKey(usr.id)) {
                String currUserLicense = userIdToUserMap.get(usr.id).Profile.UserLicense.Name;
                if(!setOfUserLicense.contains(currUserLicense)){
                    break;
                }
                if(!userIdToContactMap.containsKey(usr.Id)) {
                    newContact = new Contact();
                    clonedNewUser = usr.clone(true, true, false, false);
                    for(Schema.describefieldresult dfield : listOfDescribeFields) {
                        if(dfield.getName().contains(CON_CRM.UNDERSCORE_C)) {
                            String synchField = String.valueOf(userContactSync.get(dfield.getName()));
                            if(synchField != null && (synchField.split(CON_CRM.COMMA)[2].equalsIgnoreCase(CON_CRM.BOTH) || synchField.split(CON_CRM.COMMA)[2].equalsIgnoreCase(CON_CRM.USER_TO_CONTACT))) {
                                if(dfield.getName() == CON_CRM.RECORD_ACTIVE) {
                                    Boolean isActive = Boolean.valueOf(clonedNewUser.get(synchField.split(CON_CRM.COMMA)[0]));
                                    newContact.put(synchField.split(CON_CRM.COMMA)[1], !isActive);
                                } else {
                                    newContact.put(synchField.split(CON_CRM.COMMA)[1], clonedNewUser.get(synchField.split(CON_CRM.COMMA)[0]));
                                }
                                isContactCreated = true;
                            }
                        }
                    }
                    if(isContactCreated) {
                        newContact.put(CON_CRM.SALESFORCE_USER, usr.id);
                        newContact.put(CON_CRM.RECORD_TYPE_ID, CON_CRM.IQVIA_USER_CONTACT_RECORD_TYPE_ID);
                        newContact.put('AccountId', Org_Default_Setting__c.getInstance().Default_Account_Id__c);
                        uow.registerNew((Contact)newContact);
                        toCommit = true;
                    }
                }
                else {
                    isContactUpdated = false;
                    clonedNewUser = usr.clone(false, true, false, false);
                    clonedOldUser = oldMap.get(usr.id).clone(false, true, false, false);
                    updateContact = userIdToContactMap.get(usr.Id).clone(true, true, false, false);
                    for(Schema.SObjectField sfield : fieldMap.Values()) {
                        Schema.describefieldresult dfield = sfield.getDescribe();
                        if(dfield.getName().contains(CON_CRM.UNDERSCORE_C)) {
                            String synchField = String.valueOf(userContactSync.get(dfield.getName()));
                            if(synchField != null) {
                                if((synchField.split(CON_CRM.COMMA)[2].equalsIgnoreCase(CON_CRM.BOTH) || synchField.split(CON_CRM.COMMA)[2].equalsIgnoreCase(CON_CRM.USER_TO_CONTACT)) && clonedNewUser.get(synchField.split(CON_CRM.COMMA)[0]) != clonedOldUser.get(synchField.split(CON_CRM.COMMA)[0])) {
                                    if(dfield.getName() == CON_CRM.RECORD_ACTIVE) {
                                        Boolean isActive = Boolean.valueOf(clonedNewUser.get(synchField.split(CON_CRM.COMMA)[0]));
                                        updateContact.put(synchField.split(CON_CRM.COMMA)[1], !isActive );
                                    } else {
                                        updateContact.put(synchField.split(CON_CRM.COMMA)[1], clonedNewUser.get(synchField.split(CON_CRM.COMMA)[0]));
                                    }
                                    isContactUpdated = true;
                                }
                            }
                        }
                    }
                    if(isContactUpdated) {
                        uow.registerDirty((Contact)updateContact);
                        toCommit = true;
                    }
                }
            }
        }
        
        if(toCommit){
            Database.DMLOptions dmlOptions = new Database.DMLOptions();
            dmlOptions.DuplicateRuleHeader.AllowSave = true;
            uow.commitWork(dmlOptions);
        }
    }
    
    public static void updateLastNametoUnavailable(List<User> newList,Map<Id, User> oldMap){
        for (User c : newList) {
            if(Trigger.isInsert || (Trigger.IsUpdate && oldMap != null && oldMap.get(c.Id) != null && c.Unavailable__c != oldMap.get(c.Id).Unavailable__c )){
                if(!c.Unavailable__c) {
                    c.LastName = c.LastName.remove(' (Unavailable)');
                }else if(c.Unavailable__c){
                    c.LastName += ' (Unavailable)';
                }    
            }
        }
    }
}
