public class CNT_CSM_Timer {
    
    @AuraEnabled
    public static TimeSheet__c getTimeSheet(String caseId){
        List<TimeSheet__c> sheet = new List<TimeSheet__c>();
        String userId = UserInfo.getUserId();
        sheet = Database.query('Select Id, Name, Timeinhours__c,StartTime__c,Status__c,Case__c, Case__r.CaseNumber From TimeSheet__c Where Case__c =:caseId and LastModifiedById =:userId and Status__c = \'start\' LIMIT 1');
        if(!sheet.isEmpty()){
            return sheet[0];    
        }else{
            return null;    
        }
        
    }
    
    @AuraEnabled
    public static TimeSheet__c insertTime(String caseId){
        String userId = UserInfo.getUserId();
        List<TimeSheet__c> sheetList = new List<TimeSheet__c>();
        if(caseId != null){
            sheetList = Database.query('Select Id, Name, Timeinhours__c,StartTime__c,Status__c,Case__c, Case__r.CaseNumber From TimeSheet__c Where Case__c =:caseId and LastModifiedById =:userId and Status__c = \'start\' LIMIT 1');    
        }
        if(sheetList.isEmpty()){
            TimeSheet__c sheet = new TimeSheet__c(StartTime__c = System.now(), Case__c = caseId, Status__c = 'start');
            insert sheet;
            sheetList = Database.query('Select Id, Name, Timeinhours__c,StartTime__c,Status__c,Case__c, Case__r.CaseNumber From TimeSheet__c Where Case__c =:caseId and LastModifiedById =:userId and Status__c = \'start\' LIMIT 1');    
        }
            
        return sheetList[0];
    }
    
    @AuraEnabled
    public static TimeSheet__c updateStopTime(String caseId,String Id){
        String userId = UserInfo.getUserId();
        List<TimeSheet__c> sheet = new List<TimeSheet__c>();
        sheet = Database.query('Select Id, Name, Timeinhours__c,StartTime__c,Status__c,Case__c, Case__r.CaseNumber From TimeSheet__c Where Id = :Id and Case__c =:caseId and LastModifiedById =:userId LIMIT 1');
        if(!sheet.isEmpty()){
          
            DateTime start = sheet[0].StartTime__c;
            DateTime now = System.now();
            Long milliseconds = now.getTime() - start.getTime();
            
            Long minutes = milliseconds / (60*1000);
            Long hours = milliseconds / (60*60*1000);
            //Long days = hours / 24;
            Double min = Double.valueof(Math.mod(minutes, 60))/60;
            sheet[0].Timeinhours__c = hours + min;
            sheet[0].Status__c = 'stop';
            update sheet[0];
        }
        return sheet[0];
    }
    
    
    @AuraEnabled
    public static void saveTimeSheet(String caseId,Double timeInHours, String comment){
        TimeSheet__c sheet = new TimeSheet__c(StartTime__c = System.now(), Case__c = caseId, Timeinhours__c = timeInHours, Comment__c = comment, Status__c = 'stop');
        insert sheet;
        
    }

}