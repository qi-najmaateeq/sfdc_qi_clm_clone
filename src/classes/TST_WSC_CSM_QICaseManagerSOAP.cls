@isTest
private class TST_WSC_CSM_QICaseManagerSOAP {
    
    testmethod static void WSC_CSM_QICaseManagerSOAPCaseManager() {
        Product2 product = new Product2(Name = 'AggregateSpend360', ProductCode = 'AggregateSpend360', isActive = true);
        insert product;
        Account account = new Account(
            Name = 'TestAcc',
            Accountcountry__c='IN',
            RDCategorization__c = 'Site');
        insert account;
        
        Contact Con = new Contact( 
            AccountId = account.Id,
            Firstname='Mallikarjua', 
            Lastname='Reddy', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='malli@trailhead.com');
        insert Con;
		Queue_User_Relationship__c queues=new Queue_User_Relationship__c();
        queues.Name ='Q1';
        queues.QueueName__c ='Q1';
        queues.Type__c ='Queue';
        queues.User__c = UserInfo.getUserId();
        insert queues;
        Queue_User_Relationship__c queueUser=new Queue_User_Relationship__c();
        queueUser.Name ='Q1';
        queueUser.QueueName__c ='Q1';
        queueUser.Type__c ='User';
        queueUser.User__c = UserInfo.getUserId();
        insert queueUser;
        Asset asset = new Asset(Name = 'AggregateSpend360', AccountId = account.Id, Product2Id = product.id);
        insert asset;
        CSM_QI_Case_Categorization__c categorization = new CSM_QI_Case_Categorization__c(Product__c = product.Id, SubType1__c='Incident-Modules', SubType2__c='Missing Data');
        insert categorization;
        String caseNumber = WSC_CSM_QICaseManagerSOAP.createCase(Con.Email, 'Subject Test WS from Test Case', 'description test ws WS from Test Case', 'AggregateSpend360','Incident-Modules','Missing Data', 'High','Q1','Internal');
        WSC_CSM_QICaseManagerSOAP.CaseInfo caseInfo= WSC_CSM_QICaseManagerSOAP.getCaseById(caseNumber);
        WSC_CSM_QICaseManagerSOAP.CaseInfo caseUpdate= WSC_CSM_QICaseManagerSOAP.closeCase(caseNumber, 'Test Resolution');
       
    }
}