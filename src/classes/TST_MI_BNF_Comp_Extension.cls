@isTest
private class TST_MI_BNF_Comp_Extension {

    @testSetup static void setupTestData(){
        Current_Release_Version__c crv = new Current_Release_Version__c();
        crv.Current_Release__c = '3000.01';
        upsert crv;
        
        Global_Variables.PCFlag = false;
        Global_Variables.RunQuickOppLimitTrigger = false;
        Global_Variables.RenewalCloneFlag = TRUE;
        Global_Variables.syncAgreementToOppty=false;
        Global_Variables.PCTrigger_Hault_Execution=false;
        
        Account TestAccount = BNF_Test_Data.createAccount();
        List<Address__c> TestAddress_Array = BNF_Test_Data.createAddress_Array();
        List<SAP_Contact__c> TestSapContact_Array = BNF_Test_Data.createSapContact_Array();
        Opportunity opp = BNF_Test_Data.createOpp();
        BNF_Settings__c bnfsetting = BNF_Test_Data.createBNFSetting();
        List<User_Locale__c> User_LocaleSetting = BNF_Test_Data.create_User_LocaleSetting();
        List<OpportunityLineItem> OLI_Array = BNF_Test_Data.createOppLineItem();
        User u = BNF_Test_Data.createUser();
        Revenue_Analyst__c TestLocalRA = BNF_Test_Data.createRA();
        BNF2__c TestBNF = BNF_Test_Data.createBNF();
        MIBNF2__c TestMIBNF = BNF_Test_Data.createMIBNF();
        MIBNF_Component__c TestMIBNF_Comp = BNF_Test_Data.createMIBNF_Comp();
        MI_BNF_LineItem__c TestMI_BNFLineItem = BNF_Test_Data.createMI_BNF_LineItem();
        List<Billing_Schedule__c> billingSchedule = BNF_Test_Data.createBillingSchedule();
        List<Billing_Schedule_Item__c> billingScheduleItem = BNF_Test_Data.createBillingScheduleItem();
    }
    
    // Test Method for DeleteProduct
    static testMethod void DeleteProductTest() {
        MIBNF_Component__c TestMIBNF_Comp = [Select id,name,MIBNF__c, Addendum__c,Is_this_a_retainer_downpayment__c, Print_Shop__c,BNF_Status__c, Bill_To__c,X2nd_Copy__c,Carbon_Copy__c,Ship_To__c,Cover_Sheet__c from MIBNF_Component__c][0];
        MIBNF2__c TestMIBNF = [Select id,name,Client__c,Opportunity__c,Sales_Org_Code__c,Billing_Currency__c,IMS_Sales_Org__c,Fair_Value_Type__c,Invoice_Default_Day__c,Contract_Start_Date__c,Contract_End_Date__c,Contract_Type__c,Contract_Term__c,Payment_Terms__c,Revenue_Analyst__c from MIBNF2__c][0];
        List<OpportunityLineItem> oliList = [select id, OpportunityId, Opportunity.StageName, Opportunity.currencyIsoCode, Opportunity.Contract_Start_Date__c, Opportunity.Contract_End_Date__c, Project_Start_Date__c,Project_End_Date__c,
        							TotalPrice,PricebookEntry.Product2.Name, PricebookEntry.Product2.material_type__c,PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Item_Category_Group__c, Product_Start_Date__c, Product_End_Date__c,Revised_Price__c,Revised_Revenue_Schedule__c from OpportunityLineItem];
        
       Test.startTest();
       
       MI_BNF_Comp_Extension controller = new MI_BNF_Comp_Extension();
       controller.setMIBNF_CompId(TestMIBNF_Comp.id);
       controller.getMIBNF_CompId();
       
       controller.setSelectedInvoice(oliList[0].Id);
       controller.getSelectedInvoice();
       controller.MIBNF_Status='New';
        
       controller.deleteproduct();
       List<MI_BNF_LineItem__c> mibnflineitemList = [Select id, MIBNF_Component__c,Opportunity_Line_Itemid__c,Total_Price__c from  MI_BNF_LineItem__c];
       controller.MIBNFLineItemList = mibnflineitemList;
       controller.OLIItemList = oliList;
       controller.loadData();
        
     	Test.stopTest();
    }
    
     // Test Method for Disable delete button from layout
     static testMethod void DeleteProductDisableTest() {
             
       MIBNF_Component__c TestMIBNF_Comp = [Select id,name,MIBNF__c, Addendum__c,Is_this_a_retainer_downpayment__c, Print_Shop__c,BNF_Status__c, Bill_To__c,X2nd_Copy__c,Carbon_Copy__c,Ship_To__c,Cover_Sheet__c from MIBNF_Component__c][0];
        MIBNF2__c TestMIBNF = [Select id,name,Client__c,Opportunity__c,Sales_Org_Code__c,Billing_Currency__c,IMS_Sales_Org__c,Fair_Value_Type__c,Invoice_Default_Day__c,Contract_Start_Date__c,Contract_End_Date__c,Contract_Type__c,Contract_Term__c,Payment_Terms__c,Revenue_Analyst__c from MIBNF2__c][0];
        List<OpportunityLineItem> oliList = [select id, OpportunityId, Opportunity.StageName, Opportunity.currencyIsoCode, Opportunity.Contract_Start_Date__c, Opportunity.Contract_End_Date__c, Project_Start_Date__c,Project_End_Date__c,
        							TotalPrice,PricebookEntry.Product2.Name, PricebookEntry.Product2.material_type__c,PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Item_Category_Group__c, Product_Start_Date__c, Product_End_Date__c,Revised_Price__c,Revised_Revenue_Schedule__c from OpportunityLineItem];
        
       Test.startTest();
         
       MI_BNF_Comp_Extension controller = new MI_BNF_Comp_Extension();
       controller.setMIBNF_CompId(TestMIBNF_Comp.id);
       controller.getMIBNF_CompId();
       
       controller.setSelectedInvoice(oliList[0].Id);
       controller.getSelectedInvoice();
       controller.MIBNF_Status='Submitted';
       controller.deleteproduct();
       controller.loadData();
         
  	   Test.stopTest();
    }
    
    // Test Method for DeleteProduct
    static testMethod void t1() {
             
       MIBNF_Component__c TestMIBNF_Comp = [Select id,name,MIBNF__c, Addendum__c,Is_this_a_retainer_downpayment__c, Print_Shop__c,BNF_Status__c, Bill_To__c,X2nd_Copy__c,Carbon_Copy__c,Ship_To__c,Cover_Sheet__c from MIBNF_Component__c][0];
        MIBNF2__c TestMIBNF = [Select id,name,Client__c,Opportunity__c,Sales_Org_Code__c,Billing_Currency__c,IMS_Sales_Org__c,Fair_Value_Type__c,Invoice_Default_Day__c,Contract_Start_Date__c,Contract_End_Date__c,Contract_Type__c,Contract_Term__c,Payment_Terms__c,Revenue_Analyst__c from MIBNF2__c][0];
        List<OpportunityLineItem> oliList = [select id, OpportunityId, Opportunity.StageName, Opportunity.currencyIsoCode, Opportunity.Contract_Start_Date__c, Opportunity.Contract_End_Date__c, Project_Start_Date__c,Project_End_Date__c,
        							TotalPrice,PricebookEntry.Product2.Name, PricebookEntry.Product2.material_type__c,PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Item_Category_Group__c, Product_Start_Date__c, Product_End_Date__c,Revised_Price__c,Revised_Revenue_Schedule__c from OpportunityLineItem];
        
       Test.startTest();
    
       PageReference pageRef = ApexPages.currentPage();
       Test.setCurrentPageReference(pageRef);
       ApexPages.standardController sc = new ApexPages.standardController(TestMIBNF_Comp);
       
       MI_BNF_Comp_Extension controller = new MI_BNF_Comp_Extension(sc);
       controller.loadData();
        
       Test.stopTest();  
    }
    
    // Test Method for DeleteProduct
    static testMethod void isRevisedCheck() {
             
       MIBNF_Component__c TestMIBNF_Comp = [Select id,name,MIBNF__c, Addendum__c,Is_this_a_retainer_downpayment__c, Print_Shop__c,BNF_Status__c, Bill_To__c,X2nd_Copy__c,Carbon_Copy__c,Ship_To__c,Cover_Sheet__c from MIBNF_Component__c][0];
        MIBNF2__c TestMIBNF = [Select id,name,Client__c,Opportunity__c,Sales_Org_Code__c,Billing_Currency__c,IMS_Sales_Org__c,Fair_Value_Type__c,Invoice_Default_Day__c,Contract_Start_Date__c,Contract_End_Date__c,Contract_Type__c,Contract_Term__c,Payment_Terms__c,Revenue_Analyst__c from MIBNF2__c][0];
        List<OpportunityLineItem> oliList = [select id, OpportunityId, Opportunity.StageName, Opportunity.currencyIsoCode, Opportunity.Contract_Start_Date__c, Opportunity.Contract_End_Date__c, Project_Start_Date__c,Project_End_Date__c,
        							TotalPrice,PricebookEntry.Product2.Name, PricebookEntry.Product2.material_type__c,PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Item_Category_Group__c, Product_Start_Date__c, Product_End_Date__c,Revised_Price__c,Revised_Revenue_Schedule__c from OpportunityLineItem];
    
        Test.startTest();
       PageReference pageRef = ApexPages.currentPage();
       Test.setCurrentPageReference(pageRef);
       ApexPages.standardController sc = new ApexPages.standardController(TestMIBNF_Comp);
       
       MI_BNF_Comp_Extension controller = new MI_BNF_Comp_Extension(sc);
       controller.loadData();
       Test.stopTest();  
    }
}