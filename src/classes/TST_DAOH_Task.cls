/**
* This test class is used to test all methods in Task trigger.
* version : 1.0
*/
@isTest
private class TST_DAOH_Task {
    
    /**
    * This method used to set up testdata
    */ 
    @testSetup
    static void dataSetup() {
        Account acc = UTL_TestData.createAccount();
        acc.Website = 'www.test.com';
        insert acc;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        insert opp;
        
        /*CSM Start*/
        Account accCSM = TST_CSM_Util.createAccount();
        accCSM.Website = 'www.testabc.com';  
        insert accCSM;
        Contact conCSM = TST_CSM_Util.createContact(acc.Id,'CaseTestContact');
        insert conCSM;
        /*CSM END*/
    }
    
    /*Set Agreement FOR CPQ*/
    static Apttus__APTS_Agreement__c getAgreementData(Id OpportuntiyId, String recordTypeName){

        Id recordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = OpportuntiyId;
        testAgreement.RecordTypeId = recordTypeId;
        return testAgreement;
    }

    static List<Opportunity> getOpportunityData(){
        return [SELECT Id FROM Opportunity];
    }

    /*Set Task For CPQ*/
    static Task setTaskData(String whatId){

        Task taskObj = UTL_TestData.createTask('test', whatId, 'open','normal');
        taskObj.OwnerId = UserInfo.getUserId();
        insert taskObj;
        return taskObj;
    }

    /*get agreement for CPQ*/
    static Apttus__APTS_Agreement__c getAgreementData(Id agreementId){
        return [SELECT Id, Agreement_Status__c, Process_Step__c, Data_Access_End_Date__c FROM Apttus__APTS_Agreement__c WHERE Id =: agreementId];
    }
    
    /** 
    * This test method used for insert task record for CSM Data Audit Trail Test
    */ 
    static testMethod void testAuditLogTask() {
        CNT_CSM_FieldHistoryTracking.saveFields('Task','AccountId,CurrencyIsoCode,IsArchived,OwnerId,CallDurationInSeconds,CallObject,CallDisposition,CallType,Description,IsRecurrence,ActivityDate,Email,IsHighPriority,WhoId,Phone,Priority,RecurrenceActivityId,RecurrenceDayOfMonth,RecurrenceDayOfWeekMask,RecurrenceEndDateOnly,RecurrenceInstance,RecurrenceInterval,RecurrenceMonthOfYear,RecurrenceStartDateOnly,RecurrenceTimeZoneSidKey,RecurrenceType,WhatId,WhatCount,WhoCount,ReminderDateTime,IsReminderSet,RecurrenceRegeneratedType,Status,TaskSubtype,Type');
         Account acct = new Account(
            Name = 'TestAcc',
            RDCategorization__c = 'Site',Website = 'www.test.com');
        insert acct;
        
        Contact Con = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='briandent@trailhead.com',
            AccountId = acct.Id);
        insert Con;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=acct.Id,Type = 'TECHNO',
                                          BusinessHoursId = [select id from BusinessHours where Name = 'Default'].Id,
                                          StartDate=Date.valueof(System.now().addDays(-2)), 
                                          EndDate=Date.valueof(System.now().addYears(2)));
        insert ent;
        
        
        User u = [Select id from User where Id = :UserInfo.getUserId() and ProfileId = :UserInfo.getProfileId()];
        
        system.runAs(u) {
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            GroupMember grpUser = new GroupMember (
                UserOrGroupId = u.Id,
                GroupId = g1.Id);
            
            insert grpUser;
            
            Queue_User_Relationship__c qur = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                Type__c = 'Queue',
                Group_Id__c = grpUser.groupId);
            insert qur;
            
            Queue_User_Relationship__c qurUser = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                User__c = grpUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = grpUser.groupId);
            
            insert qurUser;
            
            Id RecordTypeIdCase = Schema.SObjectType.case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
            
            Case c = new Case(
                AccountId = acct.Id,
                ContactId = con.Id,
                Origin = 'Email',
                Status = 'New',
                InitialQueue__c = 'group name',
                OwnerId = g1.Id,
                EntitlementId = ent.Id,
                RecordTypeId = RecordTypeIdCase
            );
            insert c;
            Task tsk = new Task();
            tsk.WhatId = c.Id;
            tsk.Subject = 'test';
            test.startTest();
            insert tsk;
            tsk.Description ='Test Case for Task'; 
            tsk.Status = 'Open';
            tsk.Priority ='High'; 
            tsk.WhoId = con.Id;
            tsk.WhatId = acct.Id;  
            update tsk;  
            test.stopTest();
        }
    }
    
    static testMethod void testupdateStatusforLogACallonCase() {
         Account acct = new Account(
            Name = 'TestAcc',
            RDCategorization__c = 'Site',Website = 'www.test.com');
        insert acct;
        
        Contact Con = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='briandent@trailhead.com',
            AccountId = acct.Id);
        insert Con;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=acct.Id,Type = 'TECHNO',
                                          BusinessHoursId = [select id from BusinessHours where Name = 'Default'].Id,
                                          StartDate=Date.valueof(System.now().addDays(-2)), 
                                          EndDate=Date.valueof(System.now().addYears(2)));
        insert ent;
        
        
        User u = [Select id from User where Id = :UserInfo.getUserId() and ProfileId = :UserInfo.getProfileId()];
        
        system.runAs(u) {
            Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;
            
            GroupMember grpUser = new GroupMember (
                UserOrGroupId = u.Id,
                GroupId = g1.Id);
            
            insert grpUser;
            
            Queue_User_Relationship__c qur = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                Type__c = 'Queue',
                Group_Id__c = grpUser.groupId);
            insert qur;
            
            Queue_User_Relationship__c qurUser = new Queue_User_Relationship__c(
                Name = grpUser.group.Name,
                User__c = grpUser.UserOrGroupId,
                Type__c = 'User',
                Group_Id__c = grpUser.groupId);
            
            insert qurUser;
            
            Id RecordTypeIdCase = Schema.SObjectType.case.getRecordTypeInfosByName().get('TechnologyCase').getRecordTypeId();
            
            Case c = new Case(
                AccountId = acct.Id,
                ContactId = con.Id,
                Origin = 'Email',
                Status = 'New',
                InitialQueue__c = 'group name',
                OwnerId = g1.Id,
                EntitlementId = ent.Id,
                RecordTypeId = RecordTypeIdCase
            );
            insert c;
            Task tsk = new Task();
            tsk.WhatId = c.Id;
            tsk.Subject = 'test';
            tsk.WhoId = Con.Id;
            test.startTest();
            insert tsk;  
            test.stopTest();
        }
            
    }
        
    static testMethod void testMQLTask() {
        Account acct = new Account(
            Name = 'TestAcc',
            RDCategorization__c = 'Site',Website = 'www.test.com');
        insert acct;
        Lead lead = new Lead(LastName = 'test', Company = 'test', Status = 'Not Started');
        insert lead;
        Contact Con = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='briandent@trailhead.com',
            AccountId = acct.Id);
            insert Con;
            Id RecordTypeIdTask = Schema.SObjectType.Task.getRecordTypeInfosByName().get('MQL Task').getRecordTypeId();
            Task tsk = new Task();
            tsk.Subject = 'test';
            tsk.WhoId = Con.Id;
            tsk.RecordTypeId = RecordTypeIdTask;
            test.startTest();
            insert tsk;  
            tsk.WhoId = lead.Id;
            update tsk;
            test.stopTest();          
    }
    

    @isTest
    static void testUpdateAgreementStatusAndProcessStepShouldUpdateProcessStepANDSendMail(){

        Opportunity testOpportunity = getOpportunityData()[0];
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        update testOpportunity;
        Apttus__APTS_Agreement__c agreement = getAgreementData(testOpportunity.Id, CON_CPQ.AGREEMENT_FDTN_INITIAL_BID);
        agreement.Process_Step__c = CON_CPQ.LINE_MANAGER_QC;
        agreement.Agreement_Status__c = CON_CPQ.DRAFT;
        agreement.OwnerId = UserInfo.getUserId();
        insert agreement;
        Task taskObj = setTaskData(agreement.Id);

        Test.startTest();
            taskObj.status = 'Completed';
            update taskObj;
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        Apttus__APTS_Agreement__c updatedAgreement = getAgreementData(agreement.Id);
        system.assertEquals(CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL, updatedAgreement.Process_Step__c, 'Should Update Process Step');
        system.assertEquals(1, invocations, 'Should send mail');
    }
}