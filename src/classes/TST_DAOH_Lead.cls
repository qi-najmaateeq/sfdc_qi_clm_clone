/**
 * This test class is used to test all methods in opportunity trigger.
 * version : 1.0
 */
@isTest
private class TST_DAOH_Lead {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Lead testLead1 = UTL_TestData.createLead();
        testLead1.Disposition__c = 'Test Disposition';
        insert testLead1;
        
        Lead testLead2 = UTL_TestData.createLead();
        testLead2.Disposition__c = 'Marketing to Nurture';
        testLead2.Nurture_Detail__c = '3 months';
        insert testLead2;
    }
    
    /**
     * This test method used for insert lead records
     */ 
    static testMethod void testLeadInsert() {   
        List<Lead> leads = [SELECT Disposition__c, Disposition_Date__c, Nurture_Area__c, Nurture_Detail__c, Nurture_Detail_Other__c FROM Lead];
        
        for(Lead ld : leads) {
            if(ld.Disposition__c == 'Test Disposition') {
                System.assertEquals(Date.today(), ld.Disposition_Date__c);
            } else {
                System.assertEquals(null, ld.Disposition__c);
                System.assertEquals(null, ld.Disposition_Date__c);
                System.assertEquals(null, ld.Nurture_Area__c);
                System.assertEquals(null, ld.Nurture_Detail__c);
                System.assertEquals(null, ld.Nurture_Detail_Other__c);
            }
        }
    }
    
    /**
     * This test method used for update lead records
     */ 
    static testMethod void testLeadUpdate() {   
        List<Lead> leads = [SELECT Disposition__c, Disposition_Date__c, Nurture_Area__c, Nurture_Detail__c, Nurture_Detail_Other__c FROM Lead];
        
        Test.startTest();
            for(Lead ld : leads) {
                if(ld.Disposition__c == 'Test Disposition') {
                    ld.Disposition__c = 'Test Update';
                }
            }
            update leads;
        Test.stopTest();
        
        for(Lead ld : leads) {
            if(ld.Disposition__c == 'Test Update') {
                System.assertEquals(Date.today(), ld.Disposition_Date__c);
            } else {
                System.assertEquals(null, ld.Disposition__c);
                System.assertEquals(null, ld.Disposition_Date__c);
                System.assertEquals(null, ld.Nurture_Area__c);
                System.assertEquals(null, ld.Nurture_Detail__c);
                System.assertEquals(null, ld.Nurture_Detail_Other__c);
            }
        }
    }
}