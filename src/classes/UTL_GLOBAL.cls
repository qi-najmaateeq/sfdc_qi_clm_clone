/*
 * Version       : 1.0
 * Description   : Apex Utility class for IQVIA. 
 */
public class UTL_GLOBAL {
    
    private static Double currentReleaseVersion;
  
    /**
     * This function will return current release version.
     */
    public static Double getCurrentReleaseVersion(){
        if(currentReleaseVersion == null) {
            if(Test.isRunningTest()){
                currentReleaseVersion = 3000.12; 
            }else{
                Current_Release_Version__c releaseVersionSetting = Current_Release_Version__c.getInstance();
                if(releaseVersionSetting.Current_Release__c != null){
                    currentReleaseVersion = Double.valueOf(releaseVersionSetting.Current_Release__c);
                }
            }
        }
        return currentReleaseVersion; 
    }
}