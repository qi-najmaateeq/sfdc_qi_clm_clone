@isTest
public class TST_CPQ_SubmitForApproval {
    
    @isTest
	static void testSendEmailToApproveUser() {

        Account testAccount = UTL_TestData.createAccount();
        insert testAccount;

        Opportunity testOpportunity= UTL_TestData.createOpportunity(testAccount.Id);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        insert testOpportunity;

        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId();
        Apttus__APTS_Agreement__c agreement = UTL_TestData.createAgreement();
        agreement.RecordTypeId = RecordTypeId;
        agreement.Apttus__Related_Opportunity__c = testOpportunity.Id;
        agreement.Project_Manager_Name__c = 'test test';
        agreement.Email_subject__c = 'test'; 
        agreement.Email_Body__c = 'test';
        insert agreement;        

        agreement.Project_Manager_Email__c = 'test.test@gmail.com';
        update agreement;

        Attachment attach=UTL_TestData.createAttachment();   	
        attach.Name=CON_CPQ.AGREEMENT_SELECT_PRICING_TOOL_QIP;
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=agreement.id;
        attach.contentType = CON_CPQ.ATTACHMENT_CONTENT_TYPE;
        insert attach;

        CPQ_Settings__c cpqSetting = UTL_TestData.createCPQSettings();
        cpqSetting.Approval_Email_Service__c = 'test@test.com';
        insert cpqSetting;

        Test.startTest();        
            CNT_CPQ_SubmitForApproval.sendEmailToApproveUser(agreement.id);
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertEquals(1, invocations, 'An email has not been sent');
        
    }
} 