@isTest
public class TST_SLT_Bid_History_External {

    @isTest
    static void testGetBidHistoryUsingBidHistoryNummber() {

        Test.startTest();
            List<Bid_History__x> bidHistoryList = new
                SLT_Bid_History_External().getBidHistoryUsingBidHistoryNumber('', new Set<String>());
        Test.stopTest();

        system.assertEquals(0, bidHistoryList.size(), 'Should return null');
	}

    @isTest
    static void testSelectBidHistoryById() {

        Test.startTest();
            List<Bid_History__x> bidHistoryList = new SLT_Bid_History_External().selectById(new Set<Id>());
        Test.stopTest();

        system.assertEquals(0, bidHistoryList.size(), 'Should return null');
	}
}