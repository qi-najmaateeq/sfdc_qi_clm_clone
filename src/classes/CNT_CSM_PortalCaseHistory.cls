/*
 * Version       : 1.0
 * Description   : Apex Controller for PortalCaseHistory component.
 */
 
public with sharing class CNT_CSM_PortalCaseHistory {
    public class CSM_CaseHistory {
        @AuraEnabled
        public String id {get;set;}
        @AuraEnabled
        public String objName {get;set;}
        @AuraEnabled
        public String value {get;set;}
        @AuraEnabled
        public DateTime lastModifiedDate {get;set;}
        @AuraEnabled
        public String createdByName {get;set;}
        @AuraEnabled
        public String fileExtension {get;set;}
    }
    
    @AuraEnabled
    public static List<CSM_CaseHistory> getCSM_CaseHistory(String recordId ){
        CSM_CaseHistory csm_ch;
        List<CSM_CaseHistory> csm_chl = new List<CSM_CaseHistory>();
        
        
        List<CaseComment> ccl = getCaseComments(recordId);
        for(Integer i=0; i< ccl.size();i++){
            csm_ch =new CSM_CaseHistory();
            csm_ch.id = ccl[i].Id;
            csm_ch.objName = 'CaseComment';
            csm_ch.value = ccl[i].CommentBody;
            csm_ch.lastModifiedDate = ccl[i].LastModifiedDate;
            csm_ch.createdByName = ccl[i].CreatedBy.Name;
            csm_chl.add(csm_ch);
        }
        
        List<ContentDocumentLink> cdl = getAttachments(recordId);
        for(Integer i=0; i< cdl.size();i++){
            csm_ch =new CSM_CaseHistory();
            csm_ch.id = cdl[i].ContentDocumentId;
            csm_ch.objName = 'ContentDocumentLink';
            csm_ch.value = cdl[i].ContentDocument.Title+'.'+cdl[i].ContentDocument.FileExtension;
            csm_ch.lastModifiedDate = cdl[i].ContentDocument.LastModifiedDate;
            csm_ch.createdByName = cdl[i].ContentDocument.CreatedBy.Name;
            csm_chl.add(csm_ch);
        }
        
        List<CaseHistory> chl = getCaseHistory(recordId);
        for(Integer i=0; i< chl.size();i++){
            csm_ch =new CSM_CaseHistory();
            csm_ch.id = chl[i].Id;
            csm_ch.objName = 'CaseHistory';
            csm_ch.value = chl[i].Field + ' is updated  from ' + chl[i].OldValue + ' to ' + chl[i].NewValue;
            csm_ch.lastModifiedDate = chl[i].CreatedDate;
            csm_ch.createdByName = chl[i].CreatedBy.Name;
            csm_chl.add(csm_ch);
        }
        
        List<EmailMessage> eml = getEmailMessages(recordId);
        for(Integer i=0; i< eml.size();i++){
            csm_ch =new CSM_CaseHistory();
            csm_ch.id = eml[i].Id;
            csm_ch.objName = 'EmailMessage';
            String htmlBody = eml[i].HtmlBody;
            if (htmlBody.indexOf('--------------- Original Message ---------------') > -1) {
                htmlBody = eml[i].HtmlBody.split('--------------- Original Message ---------------')[0];
                if (htmlBody.indexOf('From:') > -1) {
                    htmlBody = htmlBody.split('From:')[0];
                }
            } else if(htmlBody.indexOf('From:') > -1){
                htmlBody = eml[i].HtmlBody.split('From:')[0];
            }
            if (htmlBody.lastIndexOf('@mailinator.com') > -1) {
                htmlBody = htmlBody.split('@mailinator.com')[0];
                htmlBody = htmlBody.substring(0, htmlBody.lastIndexOf('<p')-2); 
            } else if (htmlBody.lastIndexOf('replytocsm@iqvia.com') > -1) {
                htmlBody = htmlBody.split('replytocsm@iqvia.com')[0];
                htmlBody = htmlBody.substring(0, htmlBody.lastIndexOf('<p')-2);
            } else if (htmlBody.lastIndexOf('email2casecsm@iqvia.com') > -1) {
                htmlBody = htmlBody.split('email2casecsm@iqvia.com')[0];
                htmlBody = htmlBody.substring(0, htmlBody.lastIndexOf('<p')-2);
            }
            csm_ch.value = '<b>Subject:</b> ' + eml[i].Subject + '</a><br>'+'<b>From:</b> '+ eml[i].FromAddress + '<br>' +'<b>To:</b> '+ eml[i].ToAddress + '<br>' + htmlBody +'<br><a href="/support/s/detail/'+eml[i].id+'">' + eml[i].Subject + '</a>';
            csm_ch.lastModifiedDate = eml[i].LastModifiedDate;
            csm_ch.createdByName = eml[i].CreatedBy.Name;
            csm_chl.add(csm_ch);
        }
        return csm_chl;
    }
    
    /**
     * This method used to return List<CaseComment> for parentId
     * @params  String parentId
     * @return  List<CaseComment>
     */   
    @AuraEnabled
    public static List<CaseComment> getCaseComments(String recordId){
        List<CaseComment> caseComments = new List<CaseComment>();
        caseComments  = new SLT_CaseComment().selectByParentId(new Set<ID> {recordId});
        return CaseComments;
    }
    
    @AuraEnabled
    public static List<ContentDocumentLink> getAttachments(String recordId ){
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT ContentDocumentId,ContentDocument.Title, ContentDocument.LastModifiedDate, ContentDocument.CreatedBy.Id, ContentDocument.CreatedBy.Name, ContentDocument.FileExtension, ContentDocument.ContentSize, LinkedEntityId, Visibility , LinkedEntity.Name , LinkedEntity.Type FROM ContentDocumentLink where LinkedEntityId =:recordId];
        return cdl;
    }
    
    @AuraEnabled
    public static List<CaseHistory> getCaseHistory(String recordId ){
        List<CaseHistory> ch = new List<CaseHistory>();
        ch = [select Id, IsDeleted, CaseId, CreatedById,CreatedBy.Name, CreatedDate, Field, OldValue, NewValue from CaseHistory where Field in('Status') and CaseId =:recordId];
        return ch;
    }

    @AuraEnabled
    public static List<EmailMessage> getEmailMessages (String recordId ){
        List<EmailMessage> em = new List<EmailMessage>();
        em = [select Id, ParentId, Subject, HtmlBody, LastModifiedDate, CreatedBy.Name, FromName, FromAddress, ToAddress  from EmailMessage where ParentId =:recordId];
        return em;
    }
    
    /**
     * This method used to insert a CaseComment
     * @params  CaseComment caseComment
     */   
    @AuraEnabled
    public static void insertCaseComment(CaseComment caseComment){
        insert caseComment;
    }
    
}