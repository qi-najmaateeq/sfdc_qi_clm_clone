public class CNT_CPQ_CustomPathPopup {
    @AuraEnabled
    public static void updateAgreementPrcoessStep(Id agreementId, String processStep, String agreementStatus){

        Apttus__APTS_Agreement__c agreementRecord = new Apttus__APTS_Agreement__c(Id = agreementId);
        agreementRecord.Process_Step__c = processStep;

        if(!String.isBlank(agreementStatus)) {
            agreementRecord.Agreement_Status__c = agreementStatus;
        }
        update agreementRecord;
    }

    @AuraEnabled
    public static void updateAgreementStrategyDate(Id agreementId, String userInputDate){

        Apttus__APTS_Agreement__c agreementRecord = new Apttus__APTS_Agreement__c(Id = agreementId);
        agreementRecord.Data_Access_End_Date__c = Date.valueOf(userInputDate);
        update agreementRecord;
    }

    @AuraEnabled
    public static Boolean sendMail(Id recordId) {
        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_PROJECT_MANAGER_EMAIL};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{recordId},
        fieldSet);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<Document> documentList = new SLT_Document().selectUserByName(CON_CPQ.QC_CHECK_LIST);
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType(documentList[0].contentType);
        attach.setFileName(documentList[0].developerName+'.'+documentList[0].type);
        attach.setInline(false);
        attach.Body = documentList[0].Body;

        List<String> mailAddresses = agreementList[0].Project_Manager_Email__c.split(',');
        mail= CPQ_Utility.getSingleMessage(CON_CPQ.SEND_EMAIL_FOR_REQUEST_TO_KEYSTAKEHOLDER, recordId, UserInfo.getUserId(),mailAddresses);
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        if(cpqSetting != null && cpqSetting.Approval_Email_Service__c != null) {
            mail.setReplyTo(cpqSetting.Approval_Email_Service__c);
        } 
        String mbody= mail.getHtmlBody();
        mbody+= '<div class="agreementId" style="display:none">'+recordId+'</div>';
        mail.setHtmlBody(mbody);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
        Messaging.SendEmailResult[] results = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
        if (results[0].success) {
            return true;
        }
        return false;
    }

    @AuraEnabled
    public static void sendFinalQCMail(Id agreementId){
        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.OWNER};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId},
                                                                                                    fieldSet);        
        if(agreementList.size() > 0 && agreementList[0].OwnerId != null){

            CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
            Set<String> userFieldSet = new Set<String>{CON_CPQ.REGION} ;
            Map<Id, User> userMap = new SLT_User().selectByUserId(new Set<Id>{agreementList[0].OwnerId}, userFieldSet);
            String userRegion = '';
            if( !String.isBlank(userMap.get(agreementList[0].OwnerId).Region__c)){
                userRegion = userMap.get(agreementList[0].OwnerId).Region__c;
            }
            if(!String.isBlank(userRegion)){                
                Messaging.SingleEmailMessage mailToFinalQc = new Messaging.SingleEmailMessage();
                List<String> emailAddressList = new List<String>();
                if(userRegion == CON_CPQ.REGION_EMEA ){
                    emailAddressList.add(cpqSetting.Region_EMEA_Email__c);
                }else if(userRegion == CON_CPQ.REGION_AMR){
                    emailAddressList.add(cpqSetting.Region_AMR_Email__c);
                }else if(userRegion == CON_CPQ.REGION_ASIA_PASIFIC){
                    emailAddressList.add(cpqSetting.Region_Asia_Pacific_Email__c);
                }
                if(emailAddressList.size() > 0){
                    mailToFinalQc= CPQ_Utility.getSingleMessage(CON_CPQ.CPQ_SEND_MAIL_TO_FINAL_QC, agreementId, UserInfo.getUserId(),emailAddressList);
                    Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mailToFinalQc});
                }
            }
        }
    }
}