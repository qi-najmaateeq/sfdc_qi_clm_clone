/*
 * Version       : 1.0
 * Description   : Apex Controller for LXC_PEP_ContractList
 */
public with sharing class CNT_PEP_ContractList {
	@AuraEnabled
    public static List<Contract> getContract(){
        User contactUser = [SELECT Contact.AccountId from user where contactid !=null AND Id = :UserInfo.getUserId()];
        List<Contract> lstContract = new List<Contract>();
        
        if(contactUser!= null){
        	lstContract= [SELECT Id,ContractNumber,Name,SOW_status__c,Status,Date_executed_signed_contract_received__c 
                        FROM Contract
                        where AccountId =:contactUser.Contact.AccountId
                        ORDER BY Date_executed_signed_contract_received__c DESC];
        }       	
        return lstContract;
    }
}