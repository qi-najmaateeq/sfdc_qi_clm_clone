/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for OpportunityLineItems
 */
public class SLT_OpportunityLineItems extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_OpportunityLineItems() {
        super(false, true, false);
    }
    
    /**
     * constructor to initialise CRUD and FLS with a parameter for FLS.
     */
    public SLT_OpportunityLineItems(Boolean enforceFLS) {
        super(false, true, enforceFLS);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return OpportunityLineItem.sObjectType;
    }
    
    /**
     * This method used to get OpportunityLineItems by OpportunityId
     * @return  Map<Id, OpportunityLineItem>
     */
    public Map<Id, OpportunityLineItem> selectByOpportunityId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, OpportunityLineItem>((List<OpportunityLineItem>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('OpportunityId in :idSet').toSOQL()));
    }
    
    /**
     * This method used to get OpportunityLineItems by oliId
     * @return  List<OpportunityLineItem>
     */
    public List<OpportunityLineItem> selectByOLIIds(Set<ID> oliIdSet, Set<String> fieldSet) {
        return (List<OpportunityLineItem>) Database.query(newQueryFactory(false).selectFields(fieldSet).setCondition('Id in :oliIdSet').toSOQL());
    }
    
    /**
     * This method used to get OpportunityLineItems by OpportunityId
     * @return  Map<Id, OpportunityLineItem>
     */
    public Map<Id, OpportunityLineItem> getOliWithSchedules(Set<ID> idSet, Set<String> oliFieldSet, Set<String> schFieldSet) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        fflib_QueryFactory.SortOrder fieldSortOrder = fflib_QueryFactory.SortOrder.ASCENDING;
        new SLT_OpportunityLineItemSchedule().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITYLINEITEMSCHEDULE, true).selectFields(schFieldSet);
        String queryString = opportunitiesQueryFactory.selectFields(oliFieldSet).setCondition('Id in :idSet').toSOQL();
        return new Map<Id, OpportunityLineItem>((List<OpportunityLineItem>) Database.query(queryString));
    }
    
    /**
     * This method used to get OpportunityLineItems by Opportunity Id
     * @return  Map<Id, OpportunityLineItem>
     */
    public Map<Id, OpportunityLineItem> getOlisWithSchedules(Set<ID> idSet, Set<String> oliFieldSet, Set<String> schFieldSet) {
        fflib_QueryFactory opportunitiesQueryFactory = newQueryFactory(true);
        fflib_QueryFactory.SortOrder fieldSortOrder = fflib_QueryFactory.SortOrder.ASCENDING;
        new SLT_OpportunityLineItemSchedule().addQueryFactorySubselect(opportunitiesQueryFactory, CON_CRM.OPPORTUNITYLINEITEMSCHEDULE, true).selectFields(schFieldSet);
        String queryString = opportunitiesQueryFactory.selectFields(oliFieldSet).setCondition('OpportunityId in :idSet').toSOQL();
        return new Map<Id, OpportunityLineItem>((List<OpportunityLineItem>) Database.query(queryString));
    }
    
    /**
     * This method used to get OpportunityLineItems by LI OLI Id
     * @return  Map<Id, OpportunityLineItem>
     */
    public Map<Id, OpportunityLineItem> selectByLIOLIId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, OpportunityLineItem>((List<OpportunityLineItem>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('LI_OpportunityLineItem_Id__c in :idSet').toSOQL()));
    }
	
    /**
     * This method used to get OpportunityLineItems by Opportunity Id
     * @params Set<Id> oppIdset
     * @return List<OpportunityLineItem> 
     */
    public List<OpportunityLineItem> selectOLIByOpportunityId(Set<ID> oppIdSet) {
        return [SELECT OpportunityId, PricebookEntry.Product2.Global_Product_Code__c 
                FROM OpportunityLineItem 
                WHERE OpportunityId IN : oppIdSet
               ];
    }
    public List<OpportunityLineItem> selectByOpportunityIdProductCode(Set<ID> idSet,Set<String> productCodeSet, Set<Id> oliIds,Set<String> fieldSet) {
        return (List<OpportunityLineItem>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('OpportunityId in :idSet AND ProductCode IN :productCodeSet AND Id NOT IN :oliIds').toSOQL());
    }
    /**
     * This method is used to get OpportunityLineItems by Opportunity Id AND Product Code
     * @params Set<ID> oppIdSet
     * @params Set<String> fieldSet
     * @params Set<String> productCodeSet
     * @returns List<OpportunityLineItem> 
     */ 
    public List<OpportunityLineItem> getOLIByOppIdAndProductCode(Set<Id> oppIdSet, Set<String> fieldSet, Set<String> productCodeSet) {
        return (List<OpportunityLineItem>) Database.query(newQueryFactory(false).selectFields(fieldSet).setCondition('OpportunityId IN :oppIdSet AND ProductCode IN :productCodeSet').toSOQL());
    }
    public List<OpportunityLineItem> selectByOLIProductCode(Set<ID> oppIdSet,List<String> productCodeSet,Set<String> fieldSet) {
        return (List<OpportunityLineItem>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('OpportunityId in :oppIdSet AND ProductCode IN :productCodeSet').toSOQL());
    }
}
