/**
 * This is Line Item Group trigger Helper class.
 * version : 1.0
 */
public class DAOH_CRM_LineItemGroup {

    /**
     * This method is used to validate LineItem Groups.
     * @params  Id oppId
     * @params  String oldStage
     * @params  String newStage
     * @params  Id lineItemGroupId
     * @return  void
     */
     public static Map<String, String> validateLineItemGroup(Id oppId, String expectedStage, Id lineItemGroupId, String expectedLineOfBuisness) {
         Map<String, String> ligFieldToErrorMsgMap = new Map<String, String>();
         Mulesoft_Integration_Control__c mulesoft = Mulesoft_Integration_Control__c.getInstance();
         User_Permissions__c userPermission = User_Permissions__c.getInstance();
         List<PermissionSetAssignment> lstcurrentUserPerSet = [SELECT Id, PermissionSet.Name, AssigneeId FROM PermissionSetAssignment
                                                                WHERE AssigneeId = :Userinfo.getUserId() AND  Permissionset.Name = 'Sales_with_Bid_History'];
         Boolean hasSalesBidHistoryAccess = false;
         if(lstcurrentUserPerSet.size() > 0) {
             hasSalesBidHistoryAccess = true;
         }
         Set<String> oppAllStageSet = new Set<String>{CON_CRM.IDENTIFYING_OPP_STAGE, CON_CRM.QUALIFYING_OPP_STAGE, CON_CRM.DEVELOPING_PROP_STAGE,
         CON_CRM.DELIVERING_PROP_STAGE, CON_CRM.FINALIZING_DEAL_STAGE, CON_CRM.RECEIVED_ATP_STAGE, CON_CRM.CLOSED_WON_STAGE};
         Set<String> oppFromSecondStageSet = oppAllStageSet.clone();
         oppFromSecondStageSet.remove(CON_CRM.IDENTIFYING_OPP_STAGE);
         Set<String> oppFromThirdStageSet = oppFromSecondStageSet.clone();
         oppFromThirdStageSet.remove(CON_CRM.QUALIFYING_OPP_STAGE);
         Set<String> oppFromFourthStageSet = oppFromThirdStageSet.clone();
         oppFromFourthStageSet.remove(CON_CRM.DEVELOPING_PROP_STAGE);
         Set<String> oppFromFifthStageSet = oppFromFourthStageSet.clone();
         oppFromFifthStageSet.remove(CON_CRM.DELIVERING_PROP_STAGE);
         Set<String> oppFromSixthStageSet = oppFromFifthStageSet.clone();
         oppFromSixthStageSet.remove(CON_CRM.FINALIZING_DEAL_STAGE);
         Set<String> lineOfBusinessValueSet = new Set<String> {'Core Clinical', 'Data Management', 'Biostatistical/Medical Writing', 
                                              'Lifecycle Safety','Early Clinical Development'} ;
         Set<String> lineOfBusinessValueSet2 = new Set<String> {'Core Clinical', 'Data Management', 'Biostatistical/Medical Writing', 
                                              'Lifecycle Safety','Early Clinical Development', 'Lab', 'Novella'} ;
         Set<String> lineOfBusinessValueSet1 = new Set<String> {'Core Clinical', 'Lab', 'ECG/Cardiac Safety', 'Novella', 'Outcome', 'Data Management',
                                              'Lifecycle Safety', 'Early Clinical Development', 'Biostatistical/Medical Writing', 'IT Services'} ;
         Set<String> lineOfBusinessValueSet3 = lineOfBusinessValueSet2.clone();
         lineOfBusinessValueSet3.add('Biosimilars');
         lineOfBusinessValueSet3.add('GFR Clinical');
         lineOfBusinessValueSet3.add('Outcome');
         String whereCondition = '';
         if(lineItemGroupId != null) {
             whereCondition = 'Line_Item_Group__c = \'' +   lineItemGroupId + '\'';
         } else {
             whereCondition = 'Id = \'' +   oppId + '\'';
         }
         System.debug(whereCondition);
         String selectString = 'SELECT Id, Name, StageName, RD_Product_Count__c, Amount, CurrencyIsoCode, Line_Item_Group__c, Line_of_Business__c, Line_Item_Group__r.Business_Area__c, Line_Item_Group__r.Is_this_a_renewal__c, ' + 
             'Line_Item_Group__r.FSP_Duration__c, Line_Item_Group__r.FSP_Function__c, InterventionType__c, Line_Item_Group__r.Drug_Product_Name__c, Line_Item_Group__r.FSP__c, ' + 
             'Line_Item_Group__r.Anticipated_RFP_Date__c, Line_Item_Group__r.Financial_Check__c, Line_Item_Group__r.Global_project_unit__c, Line_Item_Group__r.RFP_Received_Date__c, ' + 
             'Line_Item_Group__r.Verbatim_Indication_Term__c, Line_Item_Group__r.Potential_Services__c, Line_Item_Group__r.Protocol_Number__c, Line_Item_Group__r.Q2_Budget_Tool__c, ' +
             'Line_Item_Group__r.Proposal_Due_Date__c, Line_Item_Group__r.Expected_Project_Start_Date__c, Line_Item_Group__r.Expected_Project_End_Date__c, Line_Item_Group__r.Proposal_Sent_Date__c, ' + 
             'Line_Item_Group__r.Will_Contract_have_a_fixed_price__c, Line_Item_Group__r.Does_study_include_LNS__c, Line_Item_Group__r.GPM_to_be_located_in_specific_region__c, Line_Item_Group__r.Test_Type__c, ' +
             'Line_Item_Group__r.Q2_CTMS_Picklist__c, Line_Item_Group__r.Extension_Continuation_Part_of_Progam__c, Line_Item_Group__r.Proposal_Lead__c, Line_Item_Group__r.Q2_Contract_Currency__c, '+
             'Line_Item_Group__r.Population_Age_Group__c, Line_Item_Group__r.FPI_Date__c, Line_Item_Group__r.Executive_Sponsor__c, Line_Item_Group__r.Total_Project_Awarded_Price__c FROM Opportunity WHERE ';
         System.debug(selectString);
         Opportunity opp = Database.query(selectString +  whereCondition + ' LIMIT 1');
         System.debug('expectedStage ' + expectedStage);
         System.debug('opp.RD_Product_Count__c '+ opp.RD_Product_Count__c);
         if((!mulesoft.Ignore_Validation_Rules__c) && oppAllStageSet.contains(expectedStage) && (Opp.RD_Product_Count__c > 0 || userPermission.IES_User__c)) {
             if(((hasSalesBidHistoryAccess && opp.Line_Item_Group__c != null) || opp.RD_Product_Count__c > 0) && opp.Line_Item_Group__r.Business_Area__c == null && expectedLineOfBuisness == 'Novella') {
                 ligFieldToErrorMsgMap.put('Business_Area__c', 'Business Area is required.');
             } 
             if(((hasSalesBidHistoryAccess && opp.Line_Item_Group__c != null) || opp.RD_Product_Count__c > 0) && opp.Line_Item_Group__r.Executive_Sponsor__c == null && lineOfBusinessValueSet1.contains(expectedLineOfBuisness)) {
                 Map<Id, CurrencyType> idTocurrencyTypeMap = new SLT_CurrencyType().selectAllCurrencyType();
                 Map<String, CurrencyType> isoCodeTocurrencyTypeMap = new Map<String, CurrencyType>();
                 for(Id currencyTypeId : idTocurrencyTypeMap.keySet()) {
                     isoCodeTocurrencyTypeMap.put(idTocurrencyTypeMap.get(currencyTypeId).isoCode, idTocurrencyTypeMap.get(currencyTypeId));
                 }
                 Double oppAmountUSD = (Double)opp.Amount;
                 if(opp.CurrencyIsoCode != CON_CRM.USD) {
                 	oppAmountUSD = getCurrencyConversionRate(isoCodeTocurrencyTypeMap, CON_CRM.USD, opp.CurrencyIsoCode, (Double)opp.Amount);
                 }
                 if(oppAmountUSD > 5000000){
                     ligFieldToErrorMsgMap.put('Executive_Sponsor__c', 'Executive Sponsor is required.');
                 }
             }
         } 
         /*if((!mulesoft.Ignore_Validation_Rules__c) && (Opp.RD_Product_Count__c > 0 || userPermission.IES_User__c)) {
             if(((hasSalesBidHistoryAccess && opp.Line_Item_Group__c != null) || opp.RD_Product_Count__c > 0) && String.isBlank(opp.Line_Item_Group__r.Protocol_Number__c) && expectedLineOfBuisness == 'Lab') {
                 ligFieldToErrorMsgMap.put('Protocol_Number__c', 'Protocol Number is required.');
             } 
         }*/
         if((!mulesoft.Ignore_Validation_Rules__c) && oppFromSecondStageSet.contains(expectedStage) && (Opp.RD_Product_Count__c > 0 || userPermission.IES_User__c)) {
             if(((hasSalesBidHistoryAccess && opp.Line_Item_Group__c != null) || opp.RD_Product_Count__c > 0)) {
                 if(lineOfBusinessValueSet.contains(expectedLineOfBuisness)) {
                     if(opp.Line_Item_Group__r.Is_this_a_renewal__c == null && opp.Line_Item_Group__r.FSP__c == 'Yes') {
                         ligFieldToErrorMsgMap.put('Is_this_a_renewal__c', 'Is this a renewal is required.');
                     }
                     /*if(opp.Line_Item_Group__r.FSP_Duration__c == null && opp.Line_Item_Group__r.FSP__c == 'Yes') {
                         ligFieldToErrorMsgMap.put('FSP_Duration__c', 'FSP Duration is required.');
                     }
                     if(opp.Line_Item_Group__r.FSP_Function__c == null && opp.Line_Item_Group__r.FSP__c == 'Yes') {
                         ligFieldToErrorMsgMap.put('FSP_Function__c', 'FSP Function is required.');
                     }*/
                     if(opp.Line_Item_Group__r.FSP__c == null) {
                         ligFieldToErrorMsgMap.put('FSP__c', 'FSP is required.');
                     }
                 }
                 if(lineOfBusinessValueSet2.contains(expectedLineOfBuisness)) {
                     /*if(opp.Line_Item_Group__r.Intervention_Type__c == null) {
                         ligFieldToErrorMsgMap.put('Intervention_Type__c', 'Intervention Type is required');
                     }*/
                     if(String.isBlank(opp.Line_Item_Group__r.Drug_Product_Name__c)) {
                         ligFieldToErrorMsgMap.put('Drug_Product_Name__c', 'Drug Product Name is required');
                     }
                 }
                 if(opp.Line_Item_Group__r.Anticipated_RFP_Date__c == null && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('Anticipated_RFP_Date__c', 'Anticipate RFP Date is required.');
                 }
                 
             } 
             
         } 
         if((!mulesoft.Ignore_Validation_Rules__c) && oppFromThirdStageSet.contains(expectedStage) && (Opp.RD_Product_Count__c > 0 || userPermission.IES_User__c)) {
             if(((hasSalesBidHistoryAccess && opp.Line_Item_Group__c != null) || opp.RD_Product_Count__c > 0)) {
                 if( lineOfBusinessValueSet.contains(expectedLineOfBuisness)) {
                     if(opp.Line_Item_Group__r.Global_project_unit__c  == null) {
                         ligFieldToErrorMsgMap.put('Global_project_unit__c', 'Global Project Unit is required.');
                     }
                     if(String.isBlank(opp.Line_Item_Group__r.Verbatim_Indication_Term__c)) {
                         ligFieldToErrorMsgMap.put('Verbatim_Indication_Term__c', 'Verbatim Indication Term is required.');
                     }
                     if(opp.Line_Item_Group__r.Potential_Services__c  == null) {
                         ligFieldToErrorMsgMap.put('Potential_Services__c', 'Potential Services is required.');
                     }
                 }
                 if( lineOfBusinessValueSet2.contains(expectedLineOfBuisness)) {
                     if(opp.Line_Item_Group__r.RFP_Received_Date__c  == null) {
                         ligFieldToErrorMsgMap.put('RFP_Received_Date__c', 'RFP Received Date is required.');
                     }
                 }
                 if( lineOfBusinessValueSet.contains(expectedLineOfBuisness) || expectedLineOfBuisness == 'Lab') {
                     if(opp.Line_Item_Group__r.Financial_Check__c  == null) {
                         ligFieldToErrorMsgMap.put('Financial_Check__c', 'Financial Check is required.');
                     }
                 }
                 if(String.isBlank(opp.Line_Item_Group__r.Verbatim_Indication_Term__c)  && expectedLineOfBuisness == 'Outcome') {
                     ligFieldToErrorMsgMap.put('Verbatim_Indication_Term__c', 'Verbatim Indication Term is required.');
                 }
                 if(String.isBlank(opp.Line_Item_Group__r.Protocol_Number__c) && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('Protocol_Number__c', 'Protocol Number is required.');
                 }
                 if(opp.Line_Item_Group__r.Q2_Budget_Tool__c == null && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('Q2_Budget_Tool__c', 'Q2 Budget Tool is required.');
                 }
                 if(opp.Line_Item_Group__r.Population_Age_Group__c == null) {
                     ligFieldToErrorMsgMap.put('Population_Age_Group__c', 'Population Age Group is required for all stages past 2. Qualifying Opportunity. Please review and capture the Population Age Group.');
                 }
                 if(opp.Line_Item_Group__r.FPI_Date__c == null) {
                     ligFieldToErrorMsgMap.put('FPI_Date__c', '	FPI Date is required for all stages past 2. Qualifying Opportunity. Please review and capture the FPI Date.');
                 }
                 
             }
             
         }
         if((!mulesoft.Ignore_Validation_Rules__c) && oppFromFourthStageSet.contains(expectedStage) && (opp.RD_Product_Count__c > 0 || userPermission.IES_User__c)) {
             if(((hasSalesBidHistoryAccess && opp.Line_Item_Group__c != null) || opp.RD_Product_Count__c > 0)) {
                 if(lineOfBusinessValueSet2.contains(expectedLineOfBuisness)) {
                     if(opp.Line_Item_Group__r.Proposal_Due_Date__c  == null) {
                         ligFieldToErrorMsgMap.put('Proposal_Due_Date__c', 'Proposal Due Date is required.');
                     }
                     if(opp.Line_Item_Group__r.Expected_Project_Start_Date__c  == null) {
                         ligFieldToErrorMsgMap.put('Expected_Project_Start_Date__c', 'Expected Project Start Date is required.');
                     }
                     if(opp.Line_Item_Group__r.Expected_Project_End_Date__c  == null) {
                         ligFieldToErrorMsgMap.put('Expected_Project_End_Date__c', 'Expected Project End Date is required.');
                     }
                 }
                 if(opp.Line_Item_Group__r.Will_Contract_have_a_fixed_price__c == null && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('Will_Contract_have_a_fixed_price__c', 'Will Contract have a fixed price? is required.');
                 }
                 if(opp.Line_Item_Group__r.Does_study_include_LNS__c  == null && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('Does_study_include_LNS__c', 'Does Study include LNS is required.');
                 }
                 if(opp.Line_Item_Group__r.GPM_to_be_located_in_specific_region__c  == null && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('GPM_to_be_located_in_specific_region__c', 'GPM to be located in specific region? is required.');
                 }
                 if(opp.Line_Item_Group__r.Test_Type__c == null && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('Test_Type__c', 'Test Type is required.');
                 }
                 if(opp.Line_Item_Group__r.Q2_CTMS_Picklist__c == null && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('Q2_CTMS_Picklist__c', 'Q2 CTMS is required.');
                 }
                 if(opp.Line_Item_Group__r.Extension_Continuation_Part_of_Progam__c == null && expectedLineOfBuisness == 'Lab') {
                     ligFieldToErrorMsgMap.put('Extension_Continuation_Part_of_Progam__c', 'Extension Continuation Part of Program is required.');
                 }
             }
         }
         if((!mulesoft.Ignore_Validation_Rules__c) && opp.Line_Item_Group__c != null && lineOfBusinessValueSet3.contains(expectedLineOfBuisness) && (!userPermission.IES_User__c)) {
             if((((opp.StageName == CON_CRM.DELIVERING_PROP_STAGE || opp.StageName == CON_CRM.FINALIZING_DEAL_STAGE || opp.StageName == CON_CRM.RECEIVED_ATP_STAGE) && expectedStage == CON_CRM.CLOSED_LOST_STAGE) 
                || oppFromFourthStageSet.contains(expectedStage)) && opp.Line_Item_Group__r.Proposal_Sent_Date__c  == null) {
                 ligFieldToErrorMsgMap.put('Proposal_Sent_Date__c', 'Proposal Sent Date must be populated at Stage 4, Stage 5, Stage 6, Stage 7a, and Stage 7b. Please review and revise.');
             }
         }
         if((!mulesoft.Ignore_Validation_Rules__c) && oppFromFifthStageSet.contains(expectedStage) && (Opp.RD_Product_Count__c > 0 || userPermission.IES_User__c)) {
             if(((hasSalesBidHistoryAccess && opp.Line_Item_Group__c != null) || opp.RD_Product_Count__c > 0) && String.isBlank(opp.Line_Item_Group__r.Proposal_Lead__c) && expectedLineOfBuisness == 'Novella') {
                 ligFieldToErrorMsgMap.put('Proposal_Lead__c', 'Proposal Lead is required.');
             } 
             
         }
         if((!mulesoft.Ignore_Validation_Rules__c) && oppFromSixthStageSet.contains(expectedStage) && (Opp.RD_Product_Count__c > 0 || userPermission.IES_User__c)) {
             if(((hasSalesBidHistoryAccess && opp.Line_Item_Group__c != null) || opp.RD_Product_Count__c > 0) && opp.Line_Item_Group__r.Q2_Contract_Currency__c == null && expectedLineOfBuisness == 'Lab') {
                 ligFieldToErrorMsgMap.put('Q2_Contract_Currency__c', 'Q2 Contract Currency is required.');
             } 
             
         }
         return ligFieldToErrorMsgMap;
     }
    
    /**
     * This method is used to get currency of new type by exchange rates.
     * @params  Map<String,CurrencyType> currencyTypeMap
     * @params  String requestedCurrency
     * @params  String currentCurrency
     * @params  Currency olisPrice
     * @retrun  Currency 
     */   
    public static Double getCurrencyConversionRate(Map<String,CurrencyType> currencyTypeMap, String requestedCurrency, String currentCurrency, Double olisPrice) {
        Double modifiedOlisPrice = (currencyTypeMap.get(requestedCurrency).ConversionRate/currencyTypeMap.get(currentCurrency).ConversionRate) * olisPrice;
        return modifiedOlisPrice;
    }
    /**
     * This method is used to set currency.
     * @params  newList List<Line_Item_Group__c>
     * @return  void
     */
    public static void setLIGCurrency(List<Line_Item_Group__c> newList) {
        Set<Id> oppIdsSet = new Set<Id>();
        for(Line_Item_Group__c currentLIG : newList){
            if(currentLIG.Opportunity__c != null){
                oppIdsSet.add(currentLIG.Opportunity__c);
            }
        }
        Map<Id,Opportunity> IdToOppMap = new SLT_Opportunity().getOpportunityById(oppIdsSet, new Set<String>{'Id','CurrencyIsoCode'});
        for(Line_Item_Group__c currentLIG : newList){
            if(IdToOppMap.containsKey(currentLIG.Opportunity__c)){
                currentLIG.CurrencyIsoCode = IdToOppMap.get(currentLIG.Opportunity__c).CurrencyIsoCode;
            }
        }
    }
}