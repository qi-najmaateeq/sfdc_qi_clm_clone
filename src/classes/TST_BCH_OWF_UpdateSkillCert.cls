/**
* This test class is used to test all methods in BCH_OWF_UpdateSkillCert Class.
* version : 1.0
*/
@isTest
private class TST_BCH_OWF_UpdateSkillCert {
   
    @testSetup
    static void dataSetup() {
        //TriggerHandler.bypass('TGR_SkillCertificationRating');
        
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        cont.FirstName = UserInfo.getFirstName();
        cont.LastName = UserInfo.getLastName();
        insert cont;
        pse__Skill__c skill = UTL_OWF_TestData.createSkills('Test Skill',CON_OWF.SKILL_TYPE_INDICATION);
        insert skill;
        
        pse__Skill__c skillInstance = UTL_OWF_TestData.createSkills('Test Skill New',CON_OWF.SKILL_TYPE_INDICATION);
        insert skillInstance;
        
        pse__Skill_Certification_Rating__c skillCertificationRating1 = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, cont.Id);
        insert skillCertificationRating1;
        
        //pse__Skill_Certification_Rating__c skillCertificationRating2 = UTL_OWF_TestData.createSkillCertificationRating(skill.Id, cont.Id);
        //insert skillCertificationRating2;
        
        pse__Skill_Certification_Rating__c skillCertificationRating3 = UTL_OWF_TestData.createSkillCertificationRating(skillInstance.Id, cont.Id);
        insert skillCertificationRating3;
    }
    
     /**
     * This test method used for checkIfRatingAlreadyExists method
     */
    static testmethod void testBatchExecuteMethod() {
        Test.startTest();
            BCH_OWF_UpdateSkillCert batch = new BCH_OWF_UpdateSkillCert();
            database.executeBatch(batch, 10);    
        Test.stopTest();
    }
    
     
    

}