/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for OpportunityLineItems
 */
public class SLT_PriceBookEntry extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_PriceBookEntry() {
        super(false, true, true);
    }

    /**
     * Method to override OrderBy
     */
    public override String getOrderBy() {
        return 'Product2.Name';
    }    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return PriceBookEntry.sObjectType;
    }
    
    /**
     * selector method used to get PriceBookEntries By filter params
     * @param   Product2 filterProductObj
     * @param   Set<String> pbEntryFieldSet
     * @param   List<String> filterFieldsApiList
     * @param   Integer recordLimit
     * @return  List<PriceBookEntry>
     */
    public List<PriceBookEntry> getPbEntriesByFilterParams(PriceBookEntryWrapper pbeWrapper, Set<String> pbeFieldSet, List<String> productFilterFieldList, List<String> pbeFilterFieldList, Integer recordLimit) {
        String condition = CON_CRM.SINGLE_SPACE;
        Product2 productRecord = pbeWrapper.productRecord;
        PriceBookEntry pbeRecord = pbeWrapper.pbeRecord;
        for(String filterField : productFilterFieldList) {
            if(filterField == CON_CRM.NAME_FIELD || filterField == CON_CRM.UNIT_NAME_FIELD) {
                condition += CON_CRM.SINGLE_SPACE + CON_CRM.PRODUCT_OBJECT + CON_CRM.DOT_LOGIC + filterField + CON_CRM.LIKE_LOGIC + CON_CRM.BACKSLASH + CON_CRM.PERCENT_LOGIC + productRecord.get(filterField) + CON_CRM.PERCENT_LOGIC + CON_CRM.BACKSLASH + CON_CRM.AND_LOGIC;
            } else if(filterField == CON_CRM.OPPORTUNITY_TERRITORY_FIELD_API) {
               List<String> territories = ((String)productRecord.get(filterField)).split(CON_CRM.COLON);
               condition += CON_CRM.SINGLE_SPACE + CON_CRM.PRODUCT_OBJECT + CON_CRM.DOT_LOGIC + filterField + CON_CRM.IN_LOGIC + territories + CON_CRM.AND_LOGIC;
            } else {
                condition += CON_CRM.SINGLE_SPACE + CON_CRM.PRODUCT_OBJECT + CON_CRM.DOT_LOGIC + filterField + CON_CRM.EQUAL_LOGIC + CON_CRM.BACKSLASH + productRecord.get(filterField) + CON_CRM.BACKSLASH + CON_CRM.AND_LOGIC;
            }
        }
        for(String filterField : pbeFilterFieldList) {
            condition += CON_CRM.SINGLE_SPACE + filterField + CON_CRM.EQUAL_LOGIC + CON_CRM.BACKSLASH + pbeRecord.get(filterField) + CON_CRM.BACKSLASH + CON_CRM.AND_LOGIC;
        }
        String defaultCondition =  CON_CRM.PRODUCT_OBJECT + CON_CRM.DOT_LOGIC + CON_CRM.ACTIVE_TRUE + CON_CRM.AND_LOGIC + CON_CRM.PRODUCT_OBJECT + CON_CRM.DOT_LOGIC + CON_CRM.INTERFACEDWITHMDM_TRUE;
        if(productFilterFieldList.size() + pbeFilterFieldList.size() > 0) {
            condition = condition.removeEnd(CON_CRM.AND_LOGIC);
            condition = CON_CRM.ROUND_BRACKET_OPEN + condition + CON_CRM.ROUND_BRACKET_CLOSE + CON_CRM.AND_LOGIC;
        }
        condition += defaultCondition;
        string queryString = newQueryFactory(false).selectFields(pbeFieldSet).setCondition(condition).setLimit(recordLimit).toSOQL();
        return (List<PriceBookEntry>) Database.query(queryString);
    }
    
    /**
     * selector method used to get PriceBookEntries By product id. 
     * @param   Set<Id> productIdSet
     * @param   Set<String> pbeFieldSet
     * @return  List<PriceBookEntry>
     */
    public List<PriceBookEntry> getPbEntriesByProductIds(Set<Id> productIdSet, Set<String> pbeFieldSet, String currencyIsoCode) {
        return ((List<PriceBookEntry>) Database.query(newQueryFactory(true).selectFields(pbeFieldSet).setCondition('Product2.id in :productIdSet AND CurrencyIsoCode = :currencyIsoCode').toSOQL()));
    }
    
    /**
     * selector method used to get PriceBookEntries By product id. 
     * @param   Set<Id> pbeIdSet
     * @return  Map<Id, PriceBookEntry>
     */
    public Map<Id, PriceBookEntry> getMapOfPbEntriesByProductIdSet(Set<Id> pbeIdSet) {
        return new Map<Id, PriceBookEntry>([SELECT Id, Product2.Offering_Type__c, Product2.Offering_Segment__c FROM PriceBookEntry WHERE Id In: pbeIdSet]);
    }
}