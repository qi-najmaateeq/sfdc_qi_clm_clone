/**
 * This test class is used to test all methods in CNT_CRM_ChangeCurrency Controller.
 * version : 1.0
 */
@isTest
public class TST_SRV_CRM_LineItemGroup {
	/**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Mulesoft_Integration_Control__c mulesoft = UTL_TestData.createMulesoftIntegrationControl(UserInfo.getUserId());
        mulesoft.Is_Mulesoft_User__c = true;
        upsert mulesoft;
        List<User> userList = UTL_TestData.createUser('System Administrator', 1);
        userList[0].PIC_Eligible__c = true;
        insert userList;
        Account acc = UTL_TestData.createAccount();
        insert acc;
        Opportunity opp = UTL_TestData.createOpportunity(acc.Id);
        opp.Line_of_Business__c = 'Lab';
        opp.Principle_in_Charge__c = userList[0].Id;
        insert opp;
        Product2 product = UTL_TestData.createProduct();
        product.Material_Type__c = 'ZQUI';
        product.Offering_Group_Code__c = 'GPRADS';
        insert product;
        PricebookEntry pbEntry = UTL_TestData.createPricebookEntry(product.Id);
        insert pbEntry;
        OpportunityLineItem oppLineItem = UTL_TestData.createOpportunityLineItem(opp.Id, pbEntry.Id);
        insert oppLineItem;
        OpportunityLineItemSchedule olis =UTL_TestData.createOpportunityLineItemSchedule(oppLineItem.Id); 
        insert olis; 
    }
    
    /**
     * test method for getListOfRequiredFields.
     */
    static testmethod void testGetListOfRequiredFields() {
        Opportunity opp = [SELECT Id, Name FROM Opportunity limit 1];
        Set<String> ligFieldSet = new Set<String>{'Id'};
        Set<Id> oppIdSet = new Set<Id>{opp.Id};
        String expectedStage = CON_CRM.CLOSED_WON_STAGE;
        String expectedLineOfBusiness = 'Lab';
        Test.startTest(); 
        SRV_CRM_LineItemGroup.getListOfRequiredFields(opp.id, expectedStage, expectedLineOfBusiness);
        //SRV_CRM_LineItemGroup.getListOfRequiredFields(null, expectedStage, expectedLineOfBusiness);
        SRV_CRM_LineItemGroup.getLIGRecordDetail(oppIdSet, ligFieldSet);
        //SRV_CRM_LineItemGroup.getLIGRecordDetail(null, ligFieldSet);
        Test.stopTest();
    }
    static testmethod void testGetListOfRequiredFields2() {
        Opportunity opp = [SELECT Id, Name FROM Opportunity limit 1];
        Set<String> ligFieldSet = new Set<String>{'Id'};
        Set<Id> oppIdSet = new Set<Id>{opp.Id};
        String expectedStage = CON_CRM.CLOSED_WON_STAGE;
        String expectedLineOfBusiness = 'Lab';
        Test.startTest(); 
        try{
            SRV_CRM_LineItemGroup.getListOfRequiredFields(null, expectedStage, expectedLineOfBusiness);
            SRV_CRM_LineItemGroup.getLIGRecordDetail(null, null);     
        }catch(Exception e){
            
        }
        Test.stopTest();
    }
}