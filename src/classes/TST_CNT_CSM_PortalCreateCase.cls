/*
 * Version       : 1.0
 * Description   : Test Class for CNT_CSM_PortalCreateCase
 */
@isTest
private class  TST_CNT_CSM_PortalCreateCase {
    
    @testSetup
    static void dataSetup() {
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
         
        String profilId2 = [select id from Profile where Name='System Administrator'].Id;
        User accOwner = New User(Alias = 'su',UserRoleId= portalRole.Id, ProfileId = profilId2, Email = 'john2@iqvia.com',IsActive =true ,Username ='john2@iqvia.com', LastName= 'testLastName', CommunityNickname ='testSuNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert accOwner;
        
        System.runAs (accOwner) {
        
            Account account = UTL_TestData.createAccount();
            account.ownerId=accOwner.Id;
            insert account;
            
            Product2 product = UTL_TestData.createProduct();
            product.Community_Topics__c='OneKey';
            product.SpecificToCSM__c = True;
            insert product;
            
                    
            Asset asset = new Asset(Name = 'TestAsset', AccountId = account.Id, Product2Id = product.id);
            insert asset;
           
            
            CSM_QI_Case_Categorization__c categorization = new CSM_QI_Case_Categorization__c(Product__c = product.Id, SubType1__c='Please Specify', SubType2__c='Please Specify',CSHSubType__c='Please Specify',CSH_Visible__c = true);
            insert categorization;
        }
        
    }
    
    /**
     * This method used to get c by recordId
     */    
    @IsTest
    static void testGetCategorization() {
        Test.startTest();
        List<CSM_QI_Case_Categorization__c> categorizations = CNT_CSM_PortalCreateCase.getCategorization('TestProduct','Please Specify');
        Test.stopTest();
        Integer expected = 1;
        Integer actual = categorizations.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get assete by productId
     */    
    @IsTest
    static void testGetAssetByProductId() {
        List<Asset> assets = new List<Asset>();
        Product2 p = [SELECT id FROM Product2 WHERE Name = 'TestProduct'];
        Account acc = [SELECT id FROM Account WHERE Name = 'TestAccount'];
        Contact contact = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='john@acme.com',
            Portal_Case_Type__c = 'Technology Solutions',
            Contact_User_Type__c='HO User',
            AccountId = acc.Id);
        insert contact;
         
        String profilId = [select id from Profile where Name='CSM Customer Community Plus Login User'].Id;
        User user = New User(Alias = 'com', Email = 'john@acme.com',IsActive =true , ContactId = contact.Id, ProfileId = profilId,Username =' john@acme.com', LastName= 'testLastName', CommunityNickname ='testCommunityNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert user;
            
        Test.startTest();
        system.runAs(user){
            assets = CNT_CSM_PortalCreateCase.getAssetByProductId(p.Id,acc.Id);
            CNT_CSM_PortalCreateCase.getUrgencyList();
            CNT_CSM_PortalCreateCase.getImpactList();
        }
        Test.stopTest();
        Integer expected = 1;
        Integer actual = assets.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get a List<AggregateResult> of categorization
     */    
    @IsTest
    static void testGetSubtypeCategorization() {
        Test.startTest();
        List<AggregateResult> categorizations = CNT_CSM_PortalCreateCase.getSubtypeCategorization('TestProduct','');
        Test.stopTest();
        Integer expected = 1;
        Integer actual = categorizations.size();
        System.assertEquals(expected, actual);
    }

   
    @IsTest
    static void testGetUserAccount() {
        Account acc = [SELECT id FROM Account WHERE Name = 'TestAccount'];
        Contact contact = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='john@acme.com',
            Portal_Case_Type__c = 'Technology Solutions',
            Contact_User_Type__c='HO User',
            AccountId = acc.Id);
        insert contact;
         
        String profilId = [select id from Profile where Name='CSM Customer Community Plus Login User'].Id;
        User user = New User(Alias = 'com', Email = 'john@acme.com',IsActive =true , ContactId = contact.Id, ProfileId = profilId,Username =' john@acme.com', LastName= 'testLastName', CommunityNickname ='testCommunityNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert user;
            
        Test.startTest();
        List<Account> a = new List<Account>();
        system.runAs(user){
            a = CNT_CSM_PortalCreateCase.getUserAccount();
        }
        Test.stopTest();
        Integer expected = 1;
        Integer actual = a.size();
        System.assertEquals(expected, actual);
    }
    

    @IsTest
    static void testGetUserContact() {
        Account acc = [SELECT id FROM Account WHERE Name = 'TestAccount'];
        Contact contact = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='john@acme.com',
            Portal_Case_Type__c = 'Technology Solutions',
            Contact_User_Type__c='HO User',
            AccountId = acc.Id);
        insert contact;
         
        String profilId = [select id from Profile where Name='CSM Customer Community Plus Login User'].Id;
        User user = New User(Alias = 'com', Email = 'john@acme.com',IsActive =true , ContactId = contact.Id, ProfileId = profilId,Username =' john@acme.com', LastName= 'testLastName', CommunityNickname ='testCommunityNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert user;
            
        Test.startTest();
        List<Contact> c = new List<Contact>();
        system.runAs(user){
            c = CNT_CSM_PortalCreateCase.getUserContact();
        }
        Test.stopTest();
        Integer expected = 1;
        Integer actual = c.size();
        System.assertEquals(expected, actual);
    }
    
    
    @IsTest
    static void testGetUserAssetsForDATA2() {
        List<Asset> assets = new List<Asset>();
        Account acc = [SELECT id FROM Account WHERE Name = 'TestAccount'];
        Contact contact = new Contact( 
            Firstname='Brian', 
            Lastname='Dent', 
            Phone='(619)852-4569', 
            Department='Mission Control', 
            Title='Mission Specialist - Neptune', 
            Email='john@acme.com',
            Portal_Case_Type__c = 'Technology Solutions',
            Contact_User_Type__c='HO User',
            AccountId = acc.Id);
        insert contact;
         
        String profilId = [select id from Profile where Name='CSM Customer Community Plus Login User'].Id;
        User user = New User(Alias = 'com', Email = 'john@acme.com',IsActive =true , ContactId = contact.Id, ProfileId = profilId,Username =' john@acme.com', LastName= 'testLastName', CommunityNickname ='testCommunityNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert user;
            
        Test.startTest();
        system.runAs(user){
            assets = CNT_CSM_PortalCreateCase.getUserAssetsForDATA2(new List<String>{'TestAsset'});
        }
        Test.stopTest();
        Integer expected = 1;
        Integer actual = assets.size();
        System.assertEquals(expected, actual);
    }
    
    /**
     * This method used to get a List<AggregateResult> of categorization
     */    
    @IsTest
    static void testGetCategorizationWithAggregate() {
        Test.startTest();
        List<AggregateResult> categorizations = CNT_CSM_PortalCreateCase.getCategorizationWithAggregate('select Subtype1__c from CSM_QI_Case_Categorization__c group by Subtype1__c');
        Test.stopTest();
        Integer expected = 1;
        Integer actual = categorizations.size();
        System.assertEquals(expected, actual);
    }
    
    @IsTest
    static void testGetpriorityvalue() {
        Test.startTest();
        List<string> prios = CNT_CSM_PortalCreateCase.getpriorityvalue();
        Test.stopTest();
        System.assert(prios.size()>0);
    }
    
    @IsTest
    static void testGetUrgencyList() {
        Test.startTest();
        List<string> urg = CNT_CSM_PortalCreateCase.getUrgencyList();
        Test.stopTest();
        System.assert(urg.size()>0);
    }

    @IsTest
    static void testGetImpactList() {
        Test.startTest();
        List<string> imp = CNT_CSM_PortalCreateCase.getImpactList();
        Test.stopTest();
        System.assert(imp.size()>0);
    }
    
    @IsTest
    static void testGetProductCategorizationForNewCase() {
        Account acc = [SELECT id FROM Account WHERE Name = 'TestAccount'];
        List<EXT_CSM_CheckboxDetails> keyValues = new  List<EXT_CSM_CheckboxDetails>();
        Test.startTest();
        keyValues = CNT_CSM_PortalCreateCase.getProductCategorizationForNewCase(acc.Id, '');
        Test.stopTest();
        System.assert(keyValues.size()>0);
    }   
}
