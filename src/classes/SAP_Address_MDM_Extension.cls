public with sharing class SAP_Address_MDM_Extension {

    public Address__c Address {get; set;}
    public SAP_Contact__c SAP_Contact {get; set;}
    
    public List<SAP_Contact__c> SAPContactList{get;set;}
    public Account Account {get; private set;}
    private BNF2__c BNF;
    private MIBNF_Component__c MIBNF_Comp;  // MIBNF : Added By Himanshu Parashar :: 23 nov 2011
    public Boolean RequestSubmitted {get; private set;}
    public Boolean ShowHeader {get; private set;}
    public Boolean ShowSidebar {get; private set;}
    public String Debug {get; private set;}
    public Boolean RequestSent{get;private set;}
    public User U {get; private set;}
    public Boolean LocalLanguageRendered {get;private set;}



    private Set<String> VAT_Mandatory_Countries = new Set<String>{'Austria','Belgium','Bulgaria','Cyprus','Czech Republic',
                'Denmark','Estonia','Finland','France','Germany','Greece','Hungary','Ireland',
                'Italy','Latvia','Lithuania','Luxembourg','Malta','Netherlands','Poland',
                'Portugal','Romania','Slovakia','Slovenia','Spain','Sweden','UK','Korea','Turkey'};          
                
    private Set<String> Addressline1_Mandatory_Countries = new Set<String>{'Turkey'};
    private Set<String> Tax_Number_1_Mandatory_Countries = new Set<String>{'Turkey'};
    
    public SAP_Address_MDM_Extension()
    {
        SAPContactList=new List<SAP_Contact__c>();
        User U=new User();
        this.RequestSubmitted = false;
        this.ShowHeader = false;
        this.ShowSidebar = false;
        this.RequestSent=false;
        this.Debug = 'YYY';
        this.U = [select Id, Sales_Org__c, Sales_Org_Code__c,User_Country__c from User where Id = :userInfo.getUserId()];
        if (Apexpages.currentPage().getParameters().get('popup') == '0')
        {
            this.ShowHeader = true;
            this.ShowSidebar = true;
        }
        if (Apexpages.currentPage().getParameters().get('ApprovalRequest') != null)
        {
            this.ShowHeader = true;
            this.ShowSidebar = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'There are currently no addresses available for this account.  Please use the form below to submit a request for the creation of a new address in SAP.');
            ApexPages.addMessage(myMsg);
        }
        if ((Id)(Apexpages.currentPage().getParameters().get('Id')) == null)
        {
            this.Account = [select Id,Name,Region_Territory__c from Account where Id = :Apexpages.currentPage().getParameters().get('AccountId')];
            if(Apexpages.currentPage().getParameters().get('compId')!=null)
                this.MIBNF_Comp = [select Id, MIBNF__r.IMS_Sales_Org__c, MIBNF__r.Sales_Org_Code__c, MIBNF__r.Revenue_Analyst__r.User__r.Email,MIBNF__r.Revenue_Analyst__r.User__r.Id from MIBNF_Component__c where Id = :Apexpages.currentPage().getParameters().get('compId')];
            
            if (Apexpages.currentPage().getParameters().get('BnfAccountId') != null)
               this.BNF = [select Id, IMS_Sales_Org__c, Sales_Org_Code__c, Revenue_Analyst__r.User__r.Email,Revenue_Analyst__r.User__r.Id from BNF2__c where Id = :Apexpages.currentPage().getParameters().get('BnfId')];
        }
        else
        {
            this.Account = [select Id,Name from Account where Id = :this.Address.Account__c];
        } 
    
         Address = new Address__c();
         this.Address.Country__c = this.Account.Region_Territory__c;
         if(Apexpages.currentPage().getParameters().get('compId')!=null)
             this.Address.Sales_Org__c = this.MIBNF_Comp.MIBNF__r.IMS_Sales_Org__c + ' ['+ this.MIBNF_Comp.MIBNF__r.Sales_Org_Code__c +']';
         if (Apexpages.currentPage().getParameters().get('BnfAccountId') != null)
             this.Address.Sales_Org__c = this.BNF.IMS_Sales_Org__c + ' ['+ this.BNF.Sales_Org_Code__c +']';
        else
            this.Address.Sales_Org__c = U.Sales_Org__c + ' ['+ U.Sales_Org_Code__c +']';
    }
    
    
      public SAP_Contact__c getSAP_Contact()
      {
            if(SAP_Contact == null)
            {
                 SAP_Contact = new SAP_Contact__c();
                 SAP_Contact.Address__c=this.Address.id;
            }
            
            return SAP_Contact;
       }
    
    //Called from Cancel Action   
    public pageReference ReturnToBnf()
    {
        pageReference retUrl;
        if (Apexpages.currentPage().getParameters().get('BnfAccountId') != null)
        {
            retUrl = new pageReference('/' + this.BNF.Id);
        }
        else if (Apexpages.currentPage().getParameters().get('retURL') != null)
        {
            retUrl = new pageReference('/' + Apexpages.currentPage().getParameters().get('retURL').replace('/',''));
        }
        else
        {
            retUrl = new pageReference('/' + this.Account.Id);
        }
        retUrl.setRedirect(true);
        return retUrl;
    }
    
    public List<SelectOption> getTitles()
    {
        List<SelectOption> Titles = new List<SelectOption>();
        Contact ProxyContact = new Contact();
        Schema.DescribeFieldResult fieldResult = Contact.Salutation.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Titles.add(new SelectOption('None',''));
        for( Schema.PicklistEntry f : ple)
        {
                Titles.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
        return Titles;      
    }
    
    //Send mail and 
    public PageReference SendNewRequestNotificationEmail() 
    {
            // If No Contact added already
            if(SAP_Contact.Last_Name__c==null && SAPContactList.size()==0)
            {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Last Name Required.');
                 ApexPages.addMessage(myMsg);
                 return null;
            }
            else
            {
                // Send Mail Request when user finish sending request
                this.RequestSent=true;
                if(SAP_Contact.Last_Name__c!=null)
                    AddSAPContact();
                User Requestor = [select Id,Name,Email from User where Id = :UserInfo.getUserId()];
                //*****************************************************************************************
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();            
                String[] toAddresses = new String[] {MDM_Defines.MdmApprovalEmailAddress};           
                mail.setToAddresses(toAddresses);
                List<EmailTemplate> emailTemplateList = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(MDM_Defines.Email_Template_DevName_MDM_New_Address_Request_Wizard, new Set<String>{'Id'});
                mail.setTemplateId(emailTemplateList[0].Id);
                mail.setWhatId(this.Address.Id);
                mail.setTargetObjectId(Requestor.Id);
                mail.setSaveAsActivity(false);
                if(!(!Mulesoft_Integration_Control__c.getInstance().Is_Mulesoft_User__c && Mulesoft_Integration_Control__c.getInstance().Ignore_Validation_Rules__c)){
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
                }
                //*****************************************************************************************
                // Delete Temp Data after sending Mail
                delete this.SAPContactList;
                delete this.Address;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'New contact request has been sent to MDM Helpdesk. You will be notified once the contact has been created in SAP.');
                ApexPages.addMessage(myMsg);
                this.RequestSubmitted = true; 
                
            }
        return null;
        
        
    }
    
    public PageReference AddSAPContact()
    {
        // Insert SAP Contact Details
        if(SAP_Contact.Last_Name__c==null)
        {
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Last Name Required.');
             ApexPages.addMessage(myMsg);
             return null;
        }
        try
        {
            //insert SAP Contact;
            // Skip account null for Temp insertion of Address
            // Date : 13 March 2012
            Global_Variables.RunAddressRequestTrigger=false;
            SAP_Contact.Marked_For_Deletion__c=true;
            
            insert SAP_Contact;
            if(!this.RequestSent)
            {   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'New Contact request has been added. You can add more contact or you can click finish to submit your request to MDM Help desk.');
                ApexPages.addMessage(myMsg);
            }
            this.RequestSubmitted = true;
            SAPContactList.add(SAP_Contact);
            SAP_Contact=null;
            getSAP_Contact(); 
            return null;  
        }
        catch(Exception e) 
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
            System.debug(e.getMessage());
            return null;
        }
    
        
    }
    
    // Save Address Details
    public PageReference SaveAddress()
    {
        if (this.Address.Name.length() > 40)
        {
            this.Address.Name.addError('Name must be less than 40 characters');
            return null;
        }
        if (VAT_Mandatory_Countries.contains(this.Address.Country__c) && 
            (this.Address.VAT_Registration_Number__c == null || this.Address.VAT_Registration_Number__c.length() == 0))
        {
            this.Address.VAT_Registration_Number__c.addError('VAT Registration Number is mandatory for addresses in ' + this.Address.Country__c);
            return null;
        }
        
        if (Addressline1_Mandatory_Countries.contains(this.Address.Country__c) && 
            (this.Address.Address_Line_1__c == null || this.Address.Address_Line_1__c.length() == 0))
        {
            this.Address.Address_Line_1__c.addError('Address Line 1 is mandatory for addresses in ' + this.Address.Country__c);
            return null;
        }
        
        if (Tax_Number_1_Mandatory_Countries.contains(this.Address.Country__c) && 
            (this.Address.Tax_Number_1__c == null || this.Address.Tax_Number_1__c.length() == 0))
        {
            this.Address.Tax_Number_1__c.addError('Tax Number 1 is mandatory for addresses in ' + this.Address.Country__c);
            return null;
        }
        
        
        
        try
        {
            // Skip account null for Temp insertion of Address
            // Date : 13 March 2012
            Global_Variables.RunAddressRequestTrigger=false;
            this.Address.Account__c = this.Account.Id;
            this.Address.Marked_For_Deletion__c=true;
            
            
            upsert this.Address;
            getSAP_Contact();
            RequestSubmitted=true;
            return null;
            
        }
        catch(Exception e) 
        {
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
           ApexPages.addMessage(myMsg);
           System.debug(e.getMessage());
           return null;
        }
            
    }
    
    public PageReference SendNewAddressRequestNotificationEmail() 
    {
        SaveAddress();
        if(ApexPages.getMessages().size()==0)
        {
            this.RequestSent=true;
            User Requestor = [select Id,Name,Email from User where Id = :UserInfo.getUserId()];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {MDM_Defines.MdmApprovalEmailAddress};
            mail.setToAddresses(toAddresses);
            mail.setUseSignature(false);
            List<EmailTemplate> emailTemplateList = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(MDM_Defines.Email_Template_DevName_MDM_New_Address_Request, new Set<String>{'Id'});
            mail.setTemplateId(emailTemplateList[0].Id);
            //mail.setTemplateId(MDM_Defines.AddressRequestTemplateId);
            mail.setWhatId(this.Address.Id);
            mail.setTargetObjectId(Requestor.Id);
            mail.setSaveAsActivity(false);
            if(!(!Mulesoft_Integration_Control__c.getInstance().Is_Mulesoft_User__c && Mulesoft_Integration_Control__c.getInstance().Ignore_Validation_Rules__c)){
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'New address request has been sent to MDM Helpdesk. You will be notified once the address has been created in SAP.');
            ApexPages.addMessage(myMsg);
            this.RequestSubmitted = true;
            try
            {
                delete this.Address;
            }
            catch(Exception e)
            {
                // return null;   
                ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(myMsg2);
                System.debug(e.getMessage());
                return null;
            }
        }
        
        
        return null;
    }
     
    public void DoNothing()
    {
        this.Debug += 'XXXXX';
    }   
   
}