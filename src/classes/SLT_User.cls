/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for User
 */
public class SLT_User extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_User() {
        super(false, false, false);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            User.Id,
            User.Name,
            User.accountId,
            User.contactId,
            User.FirstName,
            User.LastName,
            User.User_Country__c,
            User.Email,
            User.ProfileId,
            User.UserRoleId,
            User.IsActive,
            User.Username,
            User.Alias
        };
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return User.sObjectType;
    }
    
    /**
     * This method used to get User by Id
     * @return  List<User>
     */
    public List<User> selectById(Set<ID> idSet) {
        return (List<User>) selectSObjectsById(idSet);
    }
   /**
     * This method used to get User by Id
     * @return  Map<Id, User>
     */
    public Map<Id, User> selectByUserId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, User>((List<User>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    /**
     * This method used to get User by Id
     * @return  Map<Id, User>
     */
    public List<User> selectByContactId(Set<ID> idSet, Set<String> fieldSet) {
        return (List<User>)Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('ContactId != null and ContactId in :idSet').toSOQL());
    }
	
    public User selectUserByUserId(Set<ID> idSet) {
        return Database.query('SELECT LastLoginDate,contactId,LastLoginValue__c FROM User WHERE Id IN :idSet limit 1');
    }
    public List<User> selectUserByName(String name) {
        return Database.query('SELECT Id,GBO_CRM_Compliance_Category__c from User WHERE Name LIKE \'' + name + '\' AND IsActive = true limit 1');
    }
    public List<User> selectActiveUserByUserId(Set<ID> idSet, Set<String> fieldSet) {
        return (List<User>)Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id IN :idSet AND  IsActive = true ').toSOQL());
    }

    public List<User> selectUserByAccountId (Set<ID> accountIdSet){
        return Database.query('SELECT Id, AccountId, userRole.PortalRole from User WHERE AccountId IN :accountIdSet AND IsActive = true');
    }
    
    public List<User> selectManagerUserByAccountId (Set<ID> accountIdSet){
        return Database.query('SELECT Id, AccountId, Email, userRole.PortalRole, ContactId, Contact.Email from User WHERE AccountId IN :accountIdSet AND userRole.PortalRole =\'Manager\' AND IsActive = true');
    }
    
    public List<User> selectByManagerId(Set<ID> idSet, Set<String> fieldSet) {
        return (List<User>)Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('ManagerId != null and Id in :idSet').toSOQL());
    }
    
    public Map<Id,User> selectMapOfActiveUserByUserId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, User>((List<User>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id IN :idSet AND  IsActive = true ').toSOQL()));
    }
	
    public List<User> selectContactDetailByUser(Set<ID> idSet, Set<String> fieldSet){
        return (List<User>)Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id IN :idSet AND  IsActive = true ').toSOQL());
    }
} 

