@isTest
public class TST_SLT_OrgWideEmailAddress {

    @isTest
    static void testSelectOrgWideEmailAddressById() {

        OrgWideEmailAddress emailAdress = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        Set<Id> idSet = new Set<Id>{emailAdress.Id};
        
        Test.startTest();
            List<OrgWideEmailAddress> OrgWideEmailAddressList = new SLT_OrgWideEmailAddress().selectById(idSet);
        Test.stopTest();

        system.assertEquals(1, OrgWideEmailAddressList.size(), 'Should return one OrgWideEmailAddress');
    }

    @isTest
    static void testSelectOrgWideEmailAddressByEmailAdress() {

        Test.startTest();
            List<OrgWideEmailAddress> OrgWideEmailAddressList = new SLT_OrgWideEmailAddress().selectOrgWideEmailAddressByAdress(
                                                                    CON_CPQ.CPQ_Foundation_Tool_Address, new Set<String>{CON_CPQ.Id});
        Test.stopTest();

        system.assertEquals(1, OrgWideEmailAddressList.size(), 'Should return one OrgWideEmailAddress');
    }
}