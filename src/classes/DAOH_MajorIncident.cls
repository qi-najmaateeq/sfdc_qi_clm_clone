public class DAOH_MajorIncident {

    public static void validateInternalEmailAddresses(List<Major_Incident__c> majorIncidentList) {
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern patt = null;
        Matcher match = null;
        List<Messaging.SingleEmailMessage> ccSendEmailList = new List<Messaging.SingleEmailMessage>();            
        for(Major_Incident__c majorIncidentRecord : majorIncidentList){
            if(majorIncidentRecord.Internal_Email_Addresses__c != null){
                for(String mail : majorIncidentRecord.Internal_Email_Addresses__c.split(';')){
                    patt = Pattern.compile(emailRegex);
                    match = patt.matcher(mail.trim());
                    if(!match.matches()){
                        majorIncidentRecord.addError('Invalid Email Address Format In Internal Email Addresses');
                    }
                }
            }
        }
    }

    public static void restrictNewMajorIncident(List<Major_Incident__c> majorIncidentList) {
        Set<String> fieldSet = new Set<String>();
        fieldSet.add(CON_CSM.S_IN_PROGRESS);
        fieldSet.add(CON_CSM.S_RESOLVED);
        Map<Id, Major_Incident__c> ongoingRecordMap =new SLT_MajorIncident().selectOnGoingMajorIncident(fieldSet);
        if(ongoingRecordMap != null && ongoingRecordMap.size()>0){
            for(Major_Incident__c majorIncidentRecord : majorIncidentList){
                if(!ongoingRecordMap.containsKey(majorIncidentRecord.Id)){
                    if(majorIncidentRecord.Status__c.equalsIgnoreCase(CON_CSM.S_IN_PROGRESS) || majorIncidentRecord.Status__c.equalsIgnoreCase(CON_CSM.S_RESOLVED)){
                        majorIncidentRecord.addError('New Major Incident can\'t be progressed while other is ongoing');
                    }
                }
            }
        }
    }
}