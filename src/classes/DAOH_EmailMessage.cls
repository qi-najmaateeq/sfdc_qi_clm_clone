public class DAOH_EmailMessage {
    
    public static void updateStatusforEmailMessageonCase(List<EmailMessage> newList){
        Map<Id,EmailMessage> caseIds = new Map<Id,EmailMessage>();
        List<Id> technoCaseId = new List<Id>();
        boolean InitialResonseProdOpps = false;
        List<Id> technoCaseIdProd = new List<Id>();
        List<Id> RDCaseId = new List<Id>();
        //boolean isIncoming = false;
        List<Case> updateCase = new List<Case>();
        List<Case> caseList = null;
        boolean checkUpdate = false;
        Id userId = UserInfo.getUserId();
        List<Activity__c> activityList = new List<Activity__c>();
        Activity__c act = null;
        String body; 
        Map<ID,Schema.RecordTypeInfo> rt_Map =null;
        Set<ID> myIds = new Set<ID>();
        for(EmailMessage c : newList) {
            //isIncoming = c.Incoming;
            body = c.TextBody;
            if(c.ParentId != null){
                caseIds.put(c.ParentId,c); 
                myIds.add(c.ParentId);
            }
        }
        if(caseIds!=null && !caseIds.isEmpty()){
            caseList = new SLT_Case().selectById(caseIds.keySet()); 
            rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
            for(Case cs : caseList){
                checkUpdate = false;
                if(CON_CSM.S_DATACASE.equalsIgnoreCase(cs.RecordTypeName__c) && caseIds.get(cs.Id).Incoming && cs.CaseOriginatorEmail__c == null && cs.CaseOriginatorName__c == null && CON_CSM.S_NEW.equalsIgnoreCase(cs.Status) ){
                    cs.CaseOriginatorEmail__c =  caseIds.get(cs.Id).ToAddress;
                    cs.CaseOriginatorName__c =  cs.InitialQueue__c;
                    checkUpdate = true;
                }
                if(!caseIds.get(cs.Id).Incoming && rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_TECHNOLOGY_R_T) && (CON_CSM.S_EMAIL.equalsIgnoreCase(cs.Origin) || CON_CSM.S_CUSTOMER_PORTAL.equalsIgnoreCase(cs.Origin)) && string.valueOf(cs.OwnerId).startsWith(CON_CSM.S_QUEUE_ID) && CON_CSM.S_NEW.equalsIgnoreCase(cs.Status)){
                    cs.Status = CON_CSM.S_IN_PROGRESS;
                    checkUpdate = true;
                }
                if(caseIds.get(cs.Id).Incoming && CON_CSM.S_TECHNOLOGY_R_T == cs.RecordTypeName__c && CON_CSM.S_CLOSED != cs.Status && caseIds.get(cs.Id).Subject != null && caseIds.get(cs.Id).Subject.contains(cs.Case_Thread_ID__c)){
                    cs.Status = CON_CSM.S_IN_PROGRESS;
                    cs.SubStatus__c = CON_CSM.S_RESPONSE_RECEIVED;
                    checkUpdate = true;
                }
                if(caseIds.get(cs.Id).Incoming && !(CON_CSM.S_NEW.equalsIgnoreCase(cs.Status) || CON_CSM.S_CLOSED.equalsIgnoreCase(cs.Status) || (CON_CSM.S_WAITING_FOR.equalsIgnoreCase(cs.Status) && (rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_R_D_ASSISTANCE_R_T) || rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_R_D_ACTIVITY_R_T))))){
                    cs.Status = CON_CSM.S_IN_PROGRESS;
                    checkUpdate = true;
                }
                if(caseIds.get(cs.Id).Incoming && (rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_R_D_ASSISTANCE_R_T) || rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_R_D_ACTIVITY_R_T)) && !CON_CSM.S_NEW.equalsIgnoreCase(cs.Status)){
                    cs.SubStatus__c = CON_CSM.S_RESPONSE_RECEIVED;
                    checkUpdate = true;
                }
                if(!caseIds.get(cs.Id).Incoming && rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_R_D_ACTIVITY_R_T) && cs.template__c == CON_CSM.S_R_D_DATABASE_LOCK){
                    string Subject= caseIds.get(cs.Id).subject;
                     if ((cs.SlaExitDate == null) && Subject.indexOf(CON_CSM.S_R_D_DATABASE_LOCK)!= -1){
                          RDCaseId.add(cs.id);
                    }      
                }
                
                if(!caseIds.get(cs.Id).Incoming && rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_TECHNOLOGY_R_T) && (CON_CSM.S_EMAIL.equalsIgnoreCase(cs.Origin) || CON_CSM.S_CUSTOMER_PORTAL.equalsIgnoreCase(cs.Origin))){
                    DateTime completionDate = System.now();
                    if (((cs.SlaStartDate <= completionDate) && (cs.SlaExitDate == null))){
                        if(cs.TaskMilestone__c != null && !cs.TaskMilestone__c.contains(CON_CSM.S_FIRST_RESPONSE_T)){
                            cs.TaskMilestone__c += ';' + CON_CSM.S_FIRST_RESPONSE_T;
                            checkUpdate = true;
                            technoCaseId.add(cs.Id);
                        }else if(cs.TaskMilestone__c == null){
                            cs.TaskMilestone__c = CON_CSM.S_FIRST_RESPONSE_T; 
                            checkUpdate = true;
                            technoCaseId.add(cs.Id);
                        } 
                    }
                }else if(!caseIds.get(cs.Id).Incoming && rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_TECHNOLOGY_R_T)){
                    technoCaseIdProd.add(cs.Id);
                    InitialResonseProdOpps = true;
                    checkUpdate = true;
                }
                
                if((rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_R_D_ASSISTANCE_R_T) || rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_R_D_ACTIVITY_R_T))){
                    act = new Activity__c();
                    act.Case__c = cs.Id;
                    act.LOS__c = cs.LOS__c;
                    if(cs.ContactId != null)act.Contact__c = cs.ContactId;
                    if(cs.AccountId != null) act.Site__c = cs.AccountId; 
                    if(cs.Study__c != null) act.Study__c = cs.Study__c;
                    if(cs.RandD_Location__c != null) act.RandD_Location__c = cs.RandD_Location__c;
                    act.Type__c = caseIds.get(cs.Id).Incoming ? CON_CSM.S_EMAILINBOUND: CON_CSM.S_EMAILOUTBOUND;
                    act.Description__c = body;
                    act.StartDatetime__c = System.now();
                    act.ActivityOwner__c = UserInfo.getUserId();
                    act.RecordTypeId = CON_CSM.S_ACTIVITY_RT;
                    if(act.Type__c == CON_CSM.S_EMAILINBOUND || act.Type__c == CON_CSM.S_EMAILOUTBOUND){
                        EmailMessage emailRecord = caseIds.get(cs.Id);
                        act.From_Address__c = emailRecord.FromAddress;
                        act.To_Address__c = emailRecord.ToAddress;
                        if(act.Type__c == CON_CSM.S_EMAILINBOUND && cs.Status == CON_CSM.S_CASE_CLOSED){
                            act.Email_Categorization__c = CON_CSM.S_EC_NON_ACTIONABLE;
                        }
                    }
                    activityList.add(act);
                }
                
                if((rt_Map.get(cs.recordTypeID).getName().contains(CON_CSM.S_R_D_ASSISTANCE_R_T) || rt_Map.get(cs.recordTypeID).getName().contains(CON_CSM.S_R_D_ACTIVITY_R_T)) && caseIds.get(cs.Id).Incoming){
                    cs.Last_Incoming_Email_Date__c = System.now();
                    checkUpdate = true;
                }
                
                if(checkUpdate){
                    updateCase.add(cs);
                }
                
            }
            
            if(!updateCase.isEmpty()){
                try{
                    update updateCase; 
                }catch (DmlException e) {
                    System.debug('Failed due to : '+e);
                }
                
            }
            if(!technoCaseId.isEmpty()){
                DateTime completionDate = System.now();
                DAOH_Case.completeMilestone(technoCaseId, new List<String>{CON_CSM.S_FIRST_RESPONSE_T,CON_CSM.S_PRODOPS_INITIAL_RESPONSE}, completionDate);
            }else if(!technoCaseIdProd.isEmpty() && InitialResonseProdOpps){
                DateTime completionDate = System.now();
                DAOH_Case.completeMilestone(technoCaseIdProd, new List<String>{CON_CSM.S_PRODOPS_INITIAL_RESPONSE}, completionDate);
            }

           if(!RDCaseId.isEmpty()){
              DateTime completionDate = System.now();
              DAOH_Case.completeMilestone(RDCaseId, CON_CSM.S_R_D_DATABASE_LOCK , completionDate);
              
            }
            if(!activityList.isEmpty()){
                SRV_CSM_AssignPermissionSet.createActivityFromEmailMessage(JSON.serialize(activityList));   
            }
            
        }
    }
    
    public static void createCaseCollabration(List<EmailMessage> newList){
        Set<Id> caseIds = new Set<Id>();
        for(EmailMessage c : newList) {
            if(c.ParentId != null){
                caseIds.add(c.ParentId);   
            }
        }
        
        if(!caseIds.isEmpty()){
            List<Case> caseList = new SLT_Case().selectByEmailMessage(caseIds); 
            
            // storing cc and bcc email address list
            List<String> emailAddress = new List<String>();
            if(caseList.get(0).EmailMessages.size() == 1) 
            {
                Case_Collabrator__c caseCollabrator = new Case_Collabrator__c();
                caseCollabrator.Case__c = caseList.get(0).Id;
                /** caseCollabrator.Address1__c  = newList[0].FromAddress; */
                caseCollabrator.Case_Description__c = caseList.get(0).Description;
                caseCollabrator.Case_CreatedDate__c = caseList.get(0).CreatedDate;
                for(EmailMessage em : newList){
                   /* if(!String.isBlank(em.ToAddress))
                        emailAddress.addAll((em.ToAddress.toLowerCase()).Split(';'));*/
                    if(!String.isBlank(em.ccAddress))
                        emailAddress.addAll((em.ccAddress.toLowerCase()).Split(';'));
                    if(!String.isBlank(em.BccAddress))    
                        emailAddress.addAll((em.BccAddress.toLowerCase()).split(';'));
                }    
                
                /** List<Queue_User_Relationship__c> queueList = new SLT_QueueUserRelationshipC().selectByUserGroupQueueIdSet(new Set<Id>{caseList.get(0).OwnerId}, new Set<Id>{caseList.get(0).OwnerId},new Set<String>{'Id','User_Email__c'});
                    Set<String> emailToRemoveSet = new Set<String>();
                    for(Queue_User_Relationship__c qu : queueList){
                        if(qu.User_Email__c != null) emailToRemoveSet.add(qu.User_Email__c);
                    }*/
                
                /** for(Integer index = (emailAddress.size()-1); index >= 0 ;index--) {               
                        if(emailToRemoveSet.contains(emailAddress.get(index).trim())) {
                            emailAddress.remove(index);
                        }
                    } */
                
                SObjectType objToken = Schema.getGlobalDescribe().get('Case_Collabrator__c');
                DescribeSObjectResult objDef = objToken.getDescribe();
                Map<String, SObjectField> fields = objDef.fields.getMap(); 
                
                Set<String> fieldSet = fields.keySet();
                integer i=0;
                for(String s:fieldSet)
                {
                    SObjectField fieldToken = fields.get(s);
                    DescribeFieldResult selectedField = fieldToken.getDescribe();
                    /** if(selectedField.getName().indexOf('Address')!= -1 && selectedField.getName().indexOf('Address1') == -1 && emailAddress.size() > i){ */
                    if(selectedField.getName().indexOf('Address')!= -1 && emailAddress.size() > i){
                        caseCollabrator.put(s, emailAddress.get(i));
                        i++;
                    }
                    
                }
                if(!emailAddress.isEmpty() && emailAddress.size() > 0 && caseCollabrator.Address1__c != null){
                    insert caseCollabrator;    
                }
                
                
            }
        }
    }
    
    public static void afterupdatetoCreateActivityforRD(List<EmailMessage> newList,Map<Id, EmailMessage> oldMap){
        Map<Id,EmailMessage> caseIds = new Map<Id,EmailMessage>();
        List<Case> caseList = null;
        List<Activity__c> act = null;
        String body;
        Map<ID,Schema.RecordTypeInfo> rt_Map = null;
        for(EmailMessage c : newList) {
            body = c.TextBody;
            if(c.ParentId != null && !c.Incoming){
                caseIds.put(c.ParentId,c);   
            }
        }
        if(caseIds!=null && !caseIds.isEmpty()){
            caseList = new SLT_Case().selectById(caseIds.keySet()); 
            rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
            act = new SLT_ActivityC().selectByActivityCaseId(caseIds.keySet());
            for(Case cs : caseList){
                if(act!= null && !act.isEmpty() && (rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_R_D_ASSISTANCE_R_T) || rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_R_D_ACTIVITY_R_T))){
                    act[0].Description__c = body;
                    break;
                }
            }
            
            if(act != null && !act.isEmpty()){
                update act;
            }
        }
    }
    
    public static void cloneCaseStatusIsClosedEmailMessageonCase(List<EmailMessage> newList){
        Map<Id,EmailMessage> caseIds = new Map<Id,EmailMessage>();
        List<Case> cloneCaseList = new List<Case>();
        Case cloneCase = null;
        Map<Id,Case> caseMap = null;
        Set<String> caseField = null;
        String initialQueue = null;
        Map<ID,Schema.RecordTypeInfo> rt_Map =null;
        List<EmailMessage> cloneEmail = new List<EmailMessage>();
        EmailMessage cEM = null;
        Set<Id> emIdSet = new Set<Id>();
        List<String> fieldsList = new List<String>();
        for(EmailMessage c : newList) {
            if(c.ParentId != null && c.Incoming){
                caseIds.put(c.ParentId,c);   
            }
        }
        if(caseIds!=null && !caseIds.isEmpty()){
            caseField = new Set<String>();
            /*List<FieldPermissions> fieldList = Database.query('SELECT Id, Field, SObjectType, PermissionsRead, PermissionsEdit FROM FieldPermissions WHERE parentid in (select Id from permissionset  where  (PermissionSet.Profile.Name = \'Service User\' or Name=\'R_D_Case_Record_Type_Study_Activity_Objects_Access\')) and SObjectType=\'Case\' and PermissionsEdit =true');
            for(FieldPermissions fp : fieldList){
                caseField.add(fp.Field.substring(5,fp.Field.length())); 
            }*/
            String caseFieldString = 'AFU_TECH__c, Contact, Description, Origin, Parent, Priority, Subject, SuppliedName, Type, CSAT_Responded__c, CSAT_Sent__c, CaseOriginatorEmail__c, CaseOriginatorName__c, CaseTaskAction__c, First_Escalation_Time__c, Milestone_Violation__c, TaskMilestone__c, Techno_Impact__c, Urgency__c, '+
                'AFU_Next_Date__c, AFU_TimeStamp__c, Automated_FU_Email__c, CSM_QI_Data_Originator__c, Count_of_AFU__c, G_Inquiry_Investigation__c, Jira_Issue__c, Update_Survey_Sent_Date__c, Web2Case_Asset__c, Web2Case_CurrentQueue__c, Owner__c, ProductName__c, ServiceNow_Last_Updated_Date__c, Service_Now_Incident_Number__c, Service_Now_Type__c, SoftwareUpdatePatchHotFixNumber__c, StudyProtocolName__c, SubType1__c, SubType2__c, Version__c, '+
                'ActualElapsedTimeInDays__c, ActualElapsedTimeInHrs__c, ActualElapsedTimeInMins__c, CMDB__c, IfyeshowmanylookupsrequiredCorrec__c, RequireAction__c, ResolutionsharedwithCustomer__c, SubType3__c, SupportTier__c, Target_Date__c, AssignCaseToCurrentUser__c, AutoClosed__c, ComplexityLevel__c, ConsumerHealthData__c, Customer_Requested_Date__c, Describetheissuedifferenceindetail__c, IssuedDifferencepreresearchconducted__c, ResolutionCode__c, ServiceNow_Group__c, Workaround__c, '+
                'Sponsor__c, SubcaseClasification__c, SupplierCustom__c, SupplierName__c, Supplier__c, SwitchAddGracePeriod__c, Tag__c, Timeline__c, TypeCustom__c, UserTrainedDate__c, NumberOfNotifications__c, OnBehalfOf__c, PlanEndDate__c, PlanStartDate__c, Plan_Name__c, PlannedFixDate__c, ProductPackSize__c, Release__c, SendResolutionEmail__c, Site__c, '+
                'CreatedByGroup__c, CreatedDate__c, CustomerRequiredDate__c, CustomerUrgency__c, ForeignCallerId__c, Impact__c, ItemNumber__c, JIRANotes__c, NextNotificationDate__c, NovartisCaseType__c, BusinessHours__c, BusinessUntilDueDate__c, COREClassification__c, CORESubClassification__c, CaseSource__c, ChildCaseDescription__c, CloseCode__c, CloseNotes__c, ConfigurationItem__c, CoreTicket__c, '+
                'ACN__c, Activity__c, AdditionalThirdPartyCase2__c, Approach__c, SlaExitDate, SlaStartDate, Source, StopStartDate, Subject, SuppliedEmail, BusinessHours, Contact, Description, Entitlement, IsEscalated, IsStopped, Origin, Parent, Priority, RandD_Location__c, AdditionalThirdPartyCase__c, BusinessHours__c, CaseSource__c, CaseSubType1__c, '+
                'AccessionNumber__c, Case_CategorizationId__c, CurrentQueue1__c, CurrentQueue__c, IncompleteAMF__c, ProductName__c, Site_Related_to_the_Study__c, SubType1__c, SubType2__c, SubType3__c, ReOpenDate__c, ReOpened__c, ReOpener__c, Resolution__c, SendAutomaticAcknowledgmentEmail__c, SendResolutionEmail__c, Study__c, SubStatus__c, Template__c, ThirdPartyCase__c, '+
                'CaseSubType2__c, CaseSubType3__c, ClosedBy__c, Current_Queue__c, FollowUpDateLevel__c, LIMSLevel__c, LOS__c, LastModifiedDate__c, NoContactKnown__c, OnBehalfOf__c';
            fieldsList = caseFieldString.split(',');
            for(String field : fieldsList){
                caseField.add(field.trim());
            }
            caseMap = new SLT_Case().getCaseById(caseIds.keySet(),caseField); 
            rt_Map = Case.sObjectType.getDescribe().getRecordTypeInfosById();
            for(Case cs : caseMap.values()){
                if((rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_R_D_ASSISTANCE_R_T) || rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_R_D_ACTIVITY_R_T)) && CON_CSM.S_CLOSED.equalsIgnoreCase(cs.Status))
                {
                    cloneCase = cs.clone();
                    cloneCase.CreateChildCase__c = true;
                    initialQueue = cs.InitialQueue__c;
                    cloneCase.ParentId = cs.ParentId != null ? cs.ParentId : cs.Id;
                    cloneCase.Status = CON_CSM.S_NEW;
                    cloneCase.SubStatus__c = '';
                    cloneCase.ClosedDate = null;
                    cloneCase.IsStopped = false;
                    cloneCase.StoppedTimeInDays__c = null;
                    cloneCase.StoppedTimeInHrs__c = null;
                    cloneCase.StoppedTimeInHrs__c = null;
                    cloneCaseList.add(cloneCase);
                    if(caseIds.containsKey(cs.Id)){
                        cloneCase.Owner__c = caseIds.get(cs.Id).Id;
                    }
                }else if(cs.Owner__c != null && cs.Owner__c.startsWith('02s') && (rt_map.get(cs.recordTypeID).getName().contains(CON_CSM.S_R_D_ASSISTANCE_R_T) || rt_map.get(cs.recordTypeID).getName().containsIgnoreCase(CON_CSM.S_R_D_ACTIVITY_R_T))){
                    emIdSet.add(cs.Owner__c);
                }
            }
            if(initialQueue != null){
                List<Queue_User_Relationship__c> initial = new SLT_QueueUserRelationshipC().selectByQueueName(initialQueue,new Set<String>{'Id','Name','User__c','Group_Id__c'},null);    
                for(Case cas : cloneCaseList){
                    if(initial != null && !initial.isEmpty()){
                        cas.OwnerId = initial[0].Group_Id__c;
                        cas.CurrentQueue__c = initial[0].Id;
                    }
                }
            }
            if(!cloneCaseList.isEmpty()){
                insert cloneCaseList;
            }
            if(!emIdSet.isEmpty()){
                List<Attachment> closeAtt = new List<Attachment>();
                List<Attachment> attachList = Database.query('select Id, IsDeleted, ParentId, Name, IsPrivate, ContentType, BodyLength, Body, OwnerId, Description From Attachment where ParentId in :emIdSet');
                Attachment att = null;
                for(Attachment a : attachList){
                    att = a.clone(); 
                    att.ParentId = newList[0].Id;
                    closeAtt.add(att);
                }
                
                if(!closeAtt.isEmpty()){
                    insert closeAtt;
                }
            }
            
            
        }
    }
    
    public static void MailSenderWhenEmailReceivedOnTechClosedCase(List < EmailMessage > newList) {
        Map < Id, EmailMessage > caseIds = new Map < Id, EmailMessage > ();
        List < Case > caseList = null;
        for (EmailMessage c: newList) {
            if (c.ParentId != null && c.Incoming) {
                caseIds.put(c.ParentId, c);
            }
        }
        List < EmailTemplate > emailTemp = [Select id, Body from EmailTemplate where DeveloperName = :CON_CSM.S_TECH_CLOSE_EMAIL_TEMPLATE ];
        List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > ();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        if (caseIds != null && !caseIds.isEmpty()) {
            caseList = new SLT_Case().selectById(caseIds.keySet());
            Set<Id> contactIds = new Set<Id>();
            for(Case CaseRecord : caseList){
                if(caseRecord.ContactId != null){
                    contactIds.add(caseRecord.contactId);
                }
            }
            List<Contact> contactList = new SLT_Contact().getContactEmails(contactIds);
            Map<Id, String> contactIdEmailMap = new Map<Id, String>();
            for(Contact contactRecord : contactList){
                contactIdEmailMap.put(contactRecord.Id, contactRecord.Email);
            }
            List<String> toAddresses = null;
            for (Case cs: caseList) {
                if (CON_CSM.S_TECHNOLOGY_R_T.equalsIgnoreCase(cs.RecordTypeName__c) && caseIds.get(cs.Id).Incoming && CON_CSM.S_CLOSED.equalsIgnoreCase(cs.Status)) {
                    
                    if (emailTemp.size() > 0) {
                        mail = Messaging.renderStoredEmailTemplate(emailTemp[0].id, cs.ContactId, cs.Id);
                    }
                    else
                    {
                        mail.plaintextbody='This is to inform you that case is already closed and email sent on closed case is not taken into consideration. ';
                        mail.subject=cs.Subject +' -'+ cs.CaseNumber ; 
                    }
                    String senderId = '';
                    if(DAOH_Case.getCSMSettingData().get(CON_CSM.S_CUSTOMER_PORTAL)!=null)
                        senderId= DAOH_Case.getCSMSettingData().get(CON_CSM.S_CUSTOMER_PORTAL).Component_Id__c;                         
                    
                    if(senderId != null && senderId.length() > 0) {
                        mail.setOrgWideEmailAddressId(senderId);
                    }
                    if(contactIdEmailMap.get(cs.ContactId) == null){
                        toAddresses = new List<String>();
                        toAddresses.add(caseIds.get(cs.Id).FromAddress);
                        mail.setToAddresses(toAddresses);
                        mail.setTreatTargetObjectAsRecipient(false);
                    }
                    mail.setTargetObjectId(cs.ContactId);
                    mail.setSaveAsActivity(false);
                    mail.setWhatId(cs.Id);
                    mails.add(mail);
                    if(mails.Size() > 0){
                        try{
                            Messaging.sendEmail(mails);
                        }
                        catch(Exception ex){}
                    } 
                }
            }
        }
    }
}
