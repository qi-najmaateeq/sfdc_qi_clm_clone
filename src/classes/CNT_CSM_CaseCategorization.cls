/*
 * Version       : 1.0
 * Description   : Apex Controller for CNT_LXC_Categorization Lightning component 
 */ 
public class CNT_CSM_CaseCategorization {
	
	/**
     * This method used to return List<SObject> by given Id
     * @params  String recordId
     * @params  String objName
     * @return  List<SObject>
     */
    @AuraEnabled
    public static List<SObject> getRecord(String objName, String recordId){
        List<SObject> objects = new List<SObject>();  
        try {
        	if(objName == 'case') objects = new SLT_Case().selectById(new Set<Id> { recordId });
        	else if(objName == 'Knowledge__kav' )
            objects = new SLT_Knowledge().selectById(new Set<Id> { recordId });
            
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
         	 throw new AuraHandledException(JSON.serialize(ex));
        }  
        return objects;
    }
    
   	/**
     * This method used to return List<CaseArticle> by given caseId
     * @params  String caseId
     * @return  List<CaseArticle>
     */
    @AuraEnabled
    public static List<CaseArticle> getCaseArticles(String caseId){
        List<CaseArticle> CaseArticles = new List<CaseArticle>();  
        try {
        	caseArticles = new SLT_CaseArticle().selectByCaseId(new Set<Id> {caseId});
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
         	 throw new AuraHandledException(JSON.serialize(ex));
        }  
        return caseArticles;
    }
    
	/**
     * This method used to return List<AggregateResult> of  Product__r.Name Categorization by given CaseId
     * @params  String recordId
     * @return  List<AggregateResult>
     */   
    @AuraEnabled
    public static List<AggregateResult> getProductCategorizationForCase(String recordId){
    	List<Case> caseList = new List<Case>();
    	List<Product2> products=  new List<Product2>();
        List<AggregateResult> productList = new  List<AggregateResult>();
        try {
            caseList = new SLT_Case().selectById(new Set<Id> { recordId });
            if(caseList != null && !caseList.isEmpty() && caseList[0].AccountId != null ){
                Set<String> fieldSet = new Set<String>();
                fieldSet.add('Name');
                fieldSet.add('Id');
                fieldSet.add('Community_Topics__c');
                String filterCondition ='';
                filterCondition = 'Id in(SELECT Product2Id FROM asset WHERE AccountId=\''+ caseList[0].AccountId +'\' AND Status !=\'Obsolete\')';
                //products=  new SLT_Product2().selectByAccountId(new Set<Id> { caseList[0].AccountId });
                products=  new SLT_Product2().getProductWithFilter(fieldSet,filterCondition);
                Set<String> c = new Set<String>();
                for (Integer i=0; i<products.size();i++)
                    c.add(products[i].Name);
                productList = new SLT_CaseCategorization().getProducts(c,caseList[0].RecordTypeId);
            }
        	return productList;
        } catch(Exception ex) {
        	ApexPages.addMessages(ex);
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }

    /**
     * This method used to return List<Product2> by given caseId
     * @params  String caseId
     * @return  List<Product2>
     */   
    @AuraEnabled
    public static List<Product2> getProductsCategorizationForData(String recordId){
    	List<Case> caseList = new List<Case>();
    	List<Product2> productList=  new List<Product2>();
        try {
            caseList = new SLT_Case().selectById(new Set<Id> { recordId });
            if(caseList != null && !caseList.isEmpty() && caseList[0].AccountId != null ){
                productList =  new SLT_Product2().selectByAccountId(new Set<Id> { caseList[0].AccountId });
            }
        	return productList;
        } catch(Exception ex) {
        	ApexPages.addMessages(ex);
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
    
    /**
     * This method used to return List<AggregateResult> of  Product__r.Name Categorization
     * @params  String recordId
     * @return  List<AggregateResult>
     */ 
    @AuraEnabled
    public static List<AggregateResult> getProductCategorization(){
    	List<AggregateResult> productList = new  List<AggregateResult>();
    	try {
    		productList = new SLT_CaseCategorization().getProducts(null);
        	return productList;
        } catch(Exception ex) {
        	ApexPages.addMessages(ex);
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
    
    /**
     * This method used to return List<AggregateResult> of  SubType1__c Categorization by given productName
     * @params  String productName
     * @return  List<AggregateResult>
     */ 
    @AuraEnabled
    public static List<AggregateResult> getCategorizationWithAggregate(String q){
        try {
           return new SLT_CaseCategorization().selectWithAggregate(q);
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
    
    /**
     * This method used to update categorization of Object Case or Knowledge__kav by given objectToUpdate, productName, subtype1,subtype2
     * @params SObject objectToUpdate
     * @paramsString productName
     * @params String subtype1,
     * @params String subtype2
     * @params String subtype3
     * @params String template
     * @params recordTypeId
     */ 
    @AuraEnabled
    public static void updateObjectCategorization(SObject objectToUpdate, String controllingField,String subtype1,String subtype2,String subtype3,String template, String recordTypeId){   
        List<CSM_QI_Case_Categorization__c> categorizations= new List<CSM_QI_Case_Categorization__c>();
        String msg = '';
        if (objectToUpdate.getSObjectType() == Case.sObjectType) {
            Case caseToUpdate = new Case();
            caseToUpdate = (Case)objectToUpdate;
            if (recordTypeId =='0126A000000hC35QAE'){
                //----------------------------    Updated the query for S-26 (US-80) where we need to update case type on case record based on case categorization details.
                //                    categorizations = new SLT_CaseCategorization().getCaseCategorizationByFilter('Product__r.Name=\''+controllingField+'\' and SubType1__c=\''+subtype1+'\' and SubType2__c=\''+subtype2+'\' and SubType3__c in (\''+subtype3+'\','+'\'--none--\')');
                categorizations = new SLT_CaseCategorization().getCaseCategorizationDetails(controllingField, subtype1, subtype2, subtype3, recordTypeId);
                List<Asset> assets=new List<Asset>();
                List<Account> accounts=new List<Account>();
                List<Contact> contacts=new List<Contact>();    
                if(!categorizations.isEmpty()){
                    assets = new SLT_Asset().selectByAccountIdAndProductId(new Set<Id> {caseToUpdate.AccountId},new Set<Id>{categorizations[0].Product__c});
                    contacts = new SLT_Contact().selectByContactIdList(new Set<Id> {caseToUpdate.ContactId}, new Set<String>{'Id','Contact_User_Type__c'});
                    if(assets.size() > 0)caseToUpdate.AssetId=assets[0].Id;
                    caseToUpdate.Case_CategorizationId__c = categorizations[0].Id;
                    caseToUpdate.ProductName__c = categorizations[0].ProductName__c;
                    caseToUpdate.SubType1__c = categorizations[0].SubType1__c;
                    caseToUpdate.SubType2__c = categorizations[0].SubType2__c;
                    caseToUpdate.SubType3__c = categorizations[0].SubType3__c;
                    if(caseToUpdate.Case_Type__c != 'Problem' && categorizations[0].caseType__c != null && categorizations[0].caseType__c != ''){
                        caseToUpdate.Case_Type__c = categorizations[0].caseType__c;
                    }
                    
                    if(!contacts.isEmpty() && 'HO User'.equalsIgnoreCase(contacts[0].Contact_User_Type__c)){
                        accounts = new SLT_Account().selectById(new Set<Id> {caseToUpdate.AccountId});
                        if(!accounts.isEmpty() && accounts[0].CSH_SubType__c && categorizations[0] != null && categorizations[0].CSH_Visible__c){
                            caseToUpdate.CSHSubType__c = categorizations[0].CSHSubType__c;
                        }else{
                            List<CSM_QI_Case_Categorization__c> cateList = new SLT_CaseCategorization().getCaseCategorizationByFilter('Product__r.Name=\''+controllingField+'\' and SubType1__c=\''+subtype1+'\' and SubType2__c=\'Please Specify\'');
                            if(cateList != null && !cateList.isEmpty()) caseToUpdate.CSHSubType__c = cateList[0].CSHSubType__c;
                        }
                    }
                }
            }
            else if ((recordTypeId =='0126A000000hC34QAE')||(recordTypeId =='0126A000000hC32QAE')){
                categorizations = new SLT_CaseCategorization().getCaseCategorizationByFilter('LOS__c=\''+controllingField+'\' and SubType1__c=\''+subtype1+'\' and SubType2__c=\''+subtype2+'\' and SubType3__c=\''+subtype3+'\'');
                if(!categorizations.isEmpty()){
                    caseToUpdate.Case_CategorizationId__c = categorizations[0].Id;
                    caseToUpdate.LOS__c = categorizations[0].LOS__c;
                    caseToUpdate.SubType1__c = categorizations[0].SubType1__c;
                    caseToUpdate.SubType2__c = categorizations[0].SubType2__c;
                    caseToUpdate.SubType3__c = categorizations[0].SubType3__c;
                }
                if (recordTypeId =='0126A000000hC32QAE') caseToUpdate.Template__c = template;
            }
            else if (recordTypeId == '0126A000000hC33QAE'){
                List<Asset> assets=new List<Asset>();
                assets = [select Id from Asset where Name =: controllingField and AccountId =: caseToUpdate.AccountId];
                if(assets.size() > 0){
                    caseToUpdate.AssetId = assets[0].Id; 
                }else{
                    caseToUpdate.AssetId = null; 
                } 
                caseToUpdate.ProductName__c = controllingField;
                caseToUpdate.SubType1__c = subtype1;
                caseToUpdate.SubType2__c = subtype2;
                caseToUpdate.SubType3__c = subtype3;
            }
            if(!categorizations.isEmpty() || recordTypeId == '0126A000000hC33QAE'){
                try{
                    update caseToUpdate;    
                }catch(DmlException e){
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                        //Get Validation Rule & Trigger Error Messages
                        msg =+ e.getDmlMessage(i) +  '\n' ;
                    }
                    throw new AuraHandledException(msg);
                }catch(Exception e){
                    //throw all other exception message
                    throw new AuraHandledException(e.getMessage());
                }
                
            }
            
    		
		}else if (objectToUpdate.getSObjectType() == Knowledge__kav.sObjectType) {
		    categorizations = new SLT_CaseCategorization().getCaseCategorizationByFilter('Product__r.Name=\''+controllingField+'\' and SubType1__c=\''+subtype1+'\' and SubType2__c=\''+subtype2+'\' and SubType3__c=\''+subtype3+'\'');
			Knowledge__kav knowledgeToUpdate = new Knowledge__kav();
			knowledgeToUpdate = (Knowledge__kav)objectToUpdate;
    		knowledgeToUpdate.Case_CategorizationId__c = categorizations[0].Id;
    		knowledgeToUpdate.ProductName__c = categorizations[0].ProductName__c;
            knowledgeToUpdate.SubType1__c = categorizations[0].SubType1__c;
            knowledgeToUpdate.SubType2__c = categorizations[0].SubType2__c;
            knowledgeToUpdate.SubType3__c = categorizations[0].SubType3__c; 
    		update knowledgeToUpdate;
		}
    }
    
    	/**
     * This method used to update a Case
     * @params  String recordId
     * @params  String objName
     * @return  List<SObject>
     */
    @AuraEnabled
    public static void updateCase(Case c){
       update c;
    }
    
     /**
     * This method used to get dependencies between 2 pickList
     * @params  String objApiName
     * @params  String contrfieldApiName
     * @params  String depfieldApiName
     * @return  Map<String,List<String>>
     */    
	@AuraEnabled  
    public static Map<String,List<String>> getDependentOptionsImpl(string objApiName , string contrfieldApiName , string depfieldApiName){
        system.debug(objApiName + '##' + contrfieldApiName + '###' + depfieldApiName);
           
        String objectName = objApiName.toLowerCase();
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
         
        if (!Schema.getGlobalDescribe().containsKey(objectName)){
            return null;
         }
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType==null){
            return objResults;
        }
        UTL_Bitset bitSetObj = new UTL_Bitset();
      
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        //Check if picklist values exist
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            System.debug('FIELD NOT FOUND --.> ' + controllingField + ' OR ' + dependentField);
            return objResults;     
        }
        
        List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();
        objFieldMap = null;
        List<Integer> controllingIndexes = new List<Integer>();
        for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){            
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            String label = ctrlentry.getLabel();
            objResults.put(label,new List<String>());
            controllingIndexes.add(contrIndex);
        }
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
        List<UTL_PicklistEntryWrapper> objJsonEntries = new List<UTL_PicklistEntryWrapper>();
        for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){            
               Schema.PicklistEntry depentry = depEntries[dependentIndex];
               objEntries.add(depentry);
        } 
        objJsonEntries = (List<UTL_PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), List<UTL_PicklistEntryWrapper>.class);
        List<Integer> indexes;
        for (UTL_PicklistEntryWrapper objJson : objJsonEntries){
            if (objJson.validFor==null || objJson.validFor==''){
                continue;
            }
            indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
            for (Integer idx : indexes){                
                String contrLabel = contrEntries[idx].getLabel();
                objResults.get(contrLabel).add(objJson.label);
            }
        }
        objEntries = null;
        objJsonEntries = null;
        system.debug('objResults--->' + objResults);
        return objResults;
    }
    
    @AuraEnabled
    public static TimeSheet__c getTimeSheetRecord(String caseId){
        List<TimeSheet__c> sheet = new List<TimeSheet__c>();
        String userId = UserInfo.getUserId();
        sheet = Database.query('Select Id, Name, Timeinhours__c,StartTime__c,Status__c,Case__c From TimeSheet__c Where Case__c =:caseId and LastModifiedById =:userId and Status__c = \'start\' LIMIT 1');
        if(!sheet.isEmpty()){
            return sheet[0];    
        }else{
            return null;    
        }
        
    }
    
}