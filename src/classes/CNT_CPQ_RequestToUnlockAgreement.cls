public class CNT_CPQ_RequestToUnlockAgreement {
    
    @AuraEnabled 
    public static Boolean isNotifyCurrentXAEOwner(){

        Set<Id> adminUserIds = new Set<Id>();
        List<GroupMember> unlockingGroupMemebr = [SELECT UserOrGroupId FROM GroupMember WHERE Group.Name =: CON_CPQ.CPQ_ADMINS_FOR_BUDGET_UNLOCKING]; 
        if(unlockingGroupMemebr.size() > 0){  
            for (GroupMember gm : unlockingGroupMemebr) {
                adminUserIds.add(gm.UserOrGroupId);
            }
        }
        if(adminUserIds.contains(userInfo.getUserId()))
            return false;
        else
            return true;
    }
    
    @AuraEnabled 
    public static void notifyXAEOwner(Id userId, Id agreementId){
        
        List<String> adminUserEmailAddresses = new List<String>();
        List<OrgWideEmailAddress> orgWideEmailAddress = new SLT_OrgWideEmailAddress().selectOrgWideEmailAddressByAdress(
                                                            CON_CPQ.CPQ_Foundation_Tool_Address, new Set<String>{CON_CPQ.ADDRESS});
        if(orgWideEmailAddress.size() > 0)
            adminUserEmailAddresses.add(orgWideEmailAddress[0].Address);
        List<EmailTemplate> emailTemplate = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName('CPQ_Another_User_Request_To_Unlock_Budget', new Set<String>{CON_CPQ.Id});
        if(emailTemplate.size() > 0){
            Messaging.SingleEmailMessage mailTemp = Messaging.renderStoredEmailTemplate(emailTemplate[0].Id, userId, agreementId);
            Messaging.SingleEmailMessage mailToXAEOwner = new Messaging.SingleEmailMessage();
            mailToXAEOwner.setSubject(mailTemp.getSubject());
            mailToXAEOwner.setHTMLBody(mailTemp.getHtmlbody());
            mailToXAEOwner.setTargetObjectId(userId);
            mailToXAEOwner.setSaveAsActivity(false);
            mailToXAEOwner.setCcAddresses(adminUserEmailAddresses);
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mailToXAEOwner});
        }
    }
    
    @AuraEnabled 
    public static void unlockAgreement(Id userId, Id agreementId){
		
        Apttus__APTS_Agreement__c agreementRecord = new Apttus__APTS_Agreement__c(Id = agreementId);
        agreementRecord.Pricing_Tool_Locked__c = false;
        agreementRecord.Budget_Checked_Out_By__c = null;
        agreementRecord.XAE_Lock_Timestamp__c = null;
        UPDATE agreementRecord;
    }
}