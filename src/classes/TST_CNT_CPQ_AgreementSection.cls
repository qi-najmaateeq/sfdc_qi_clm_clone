@isTest
public class TST_CNT_CPQ_AgreementSection {

    @testSetup
    static void dataSetUp() {

        List<Account> accountList = new List<Account>();
        Account testAccount = UTL_TestData.createAccount();
        testAccount.MDM_Category__c = CON_CPQ.ACCOUNT_CATEGORY_GLOBAL_PHARMA;
        accountList.add(testAccount);

        Account testAccount1 = UTL_TestData.createAccount();
        testAccount1.MDM_Validation_Status__c = 'Rejected';
        testAccount1.ParentId = testAccount.Id;
        testAccount1.RecordTypeId = SObjectType.Account.getRecordTypeInfosByName().get(
            'Global Customer Account').getRecordTypeId();
        testAccount1.MDM_Category__c = CON_CPQ.ACCOUNT_CATEGORY_GLOBAL_PHARMA;
        accountList.add(testAccount1);

        insert accountList;

        Indication_List__c indicationList = new Indication_List__c(Therapy_Area__c = 'Oncology');
        insert indicationList;
        Line_Item_Group__c lineItemGroup = new Line_Item_Group__c(Indication__c = indicationList.Id);
        insert lineItemGroup;

        Opportunity testOpportunity= UTL_TestData.createOpportunity(testAccount.Id);
        testOpportunity.Line_of_Business__c = 'Biostatistical/Medical Writing';
        testOpportunity.Legacy_Quintiles_Opportunity_Number__c='MVP123';
        testOpportunity.Line_Item_Group__c = lineItemGroup.Id;
        insert testOpportunity;

        String profileName = [SELECT Id, Name FROM Profile WHERE Name='Sales User' LIMIT 1].Name;
        User usr = UTL_TestData.createUser(profileName, 1)[0];
        usr.Region__c = CON_CPQ.REGION_EMEA;
        insert usr;

        Id RecordTypeId =
            SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get(CON_CPQ.AGREEMENT_FDTN_INITIAL_BID).getRecordTypeId();
        Apttus__APTS_Agreement__c testAgreement = UTL_TestData.createAgreement();
        testAgreement.Apttus__Related_Opportunity__c = testOpportunity.Id;
        testAgreement.RecordTypeId = RecordTypeId;
        testAgreement.Opportunity_Type__c = 'RFP';
        testAgreement.OwnerId = usr.Id;
        testAgreement.User__c = usr.Id;
        testAgreement.Apttus__Account__c = testAccount1.Id;
        insert testAgreement;

        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = UTL_TestData.createAgreementSummaryGroup(testAgreement, 0);
        testAgreementSummaryGroup.Apttus_CMConfig__ItemSequence__c = 1;
        testAgreementSummaryGroup.Apttus_CMConfig__LineNumber__c = 1;
        testAgreementSummaryGroup.Apttus_CMConfig__ChargeType__c = 'Labor';
        insert testAgreementSummaryGroup;

        List<Challenge_Matrix__c> challengeMatrixList = new List<Challenge_Matrix__c>();
        Challenge_Matrix__c testMatrix = UTL_TestData.createChallengeMatrix('0 - 3M USD', 'RFP', 'Challenge', 'email');
        challengeMatrixList.add(testMatrix);
        Challenge_Matrix__c testMatrix1 = UTL_TestData.createChallengeMatrix('3 - 5M USD', 'RFP', 'Challenge', 'email');
        challengeMatrixList.add(testMatrix1);
        Challenge_Matrix__c testMatrix2 = UTL_TestData.createChallengeMatrix('5 - 10M USD', 'RFP', 'Challenge', 'email');
        challengeMatrixList.add(testMatrix2);
        Challenge_Matrix__c testMatrix3 = UTL_TestData.createChallengeMatrix('10 - 20M USD', 'RFP', 'Challenge', 'email');
        challengeMatrixList.add(testMatrix3);
        Challenge_Matrix__c testMatrix4 = UTL_TestData.createChallengeMatrix('20 - 25M USD', 'RFP', 'Challenge', 'email');
        challengeMatrixList.add(testMatrix4);
        Challenge_Matrix__c testMatrix5 = UTL_TestData.createChallengeMatrix('25 - 50M USD', 'RFP', 'Challenge', 'email');
        challengeMatrixList.add(testMatrix5);
        Challenge_Matrix__c testMatrix6 = UTL_TestData.createChallengeMatrix('50M + USD', 'RFP', 'Challenge', 'email');
        challengeMatrixList.add(testMatrix6);
        insert challengeMatrixList;


        //Approval_Matrix__c approvalMatrix = new Approval_Matrix__c(approverGroup, 'RFP',
          //  'Latin America', 'Oncology', 'Test User', '', '', '', '', 'Large');

    }

    private static Apttus__APTS_Agreement__c getAgreement() {
        Apttus__APTS_Agreement__c testAgreement = [SELECT Id, Name, Labor_Fees__c, Opportunity_Type__c, Indication__c, Apttus__Related_Opportunity__r.Therapy_Area__c ,
            Process_Step__c, Agreement_Status__c, OwnerId,
            User__r.Region__c, Apttus__Account__r.Category__c FROM Apttus__APTS_Agreement__c LIMIT 1];
        return testAgreement;
    }

    @isTest
    static void testGetAction() {
        Id agreementId = getAgreement().Id;

        Test.startTest();
          String action = CNT_CPQ_AgreementSection.getAction(agreementId);
        Test.stopTest();

        System.assertEquals('Email', action, 'should return email action');
    }

    @isTest
    static void testGetActionFor3To5LaborFee() {
        Id agreementId = getAgreement().Id;
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 4;
        update testAgreementSummaryGroup;

        Test.startTest();
          String action = CNT_CPQ_AgreementSection.getAction(agreementId);
        Test.stopTest();

        System.assertEquals('Email', action, 'should return email action');
    }

    @isTest
    static void testGetActionFor5To10LaborFee() {
        Id agreementId = getAgreement().Id;
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 6;
        update testAgreementSummaryGroup;

        Test.startTest();
          String action = CNT_CPQ_AgreementSection.getAction(agreementId);
        Test.stopTest();

        System.assertEquals('Email', action, 'should return email action');
    }

    @isTest
    static void testGetActionFor10To20LaborFee() {
        Id agreementId = getAgreement().Id;
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 12;
        update testAgreementSummaryGroup;

        Test.startTest();
          String action = CNT_CPQ_AgreementSection.getAction(agreementId);
        Test.stopTest();

        System.assertEquals('Email', action, 'should return email action');
    }

    @isTest
    static void testGetActionFor20To25LaborFee() {
        Id agreementId = getAgreement().Id;
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 24;
        update testAgreementSummaryGroup;

        Test.startTest();
          String action = CNT_CPQ_AgreementSection.getAction(agreementId);
        Test.stopTest();

        System.assertEquals('Email', action, 'should return email action');
    }

    @isTest
    static void testGetActionFor25To50LaborFee() {
        Id agreementId = getAgreement().Id;
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 27;
        update testAgreementSummaryGroup;

        Test.startTest();
          String action = CNT_CPQ_AgreementSection.getAction(agreementId);
        Test.stopTest();

        System.assertEquals('Email', action, 'should return email action');
    }

    @isTest
    static void testGetActionFor50AboveLaborFee() {
        Id agreementId = getAgreement().Id;
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 55;
        update testAgreementSummaryGroup;

        Test.startTest();
          String action = CNT_CPQ_AgreementSection.getAction(agreementId);
        Test.stopTest();

        System.assertEquals('Email', action, 'should return email action');
    }

    @isTest
    static void testGetUserRegion() {
      Apttus__APTS_Agreement__c agreement = getAgreement();

      Test.startTest();
          String region = CNT_CPQ_AgreementSection.getUserRegion(agreement);
      Test.stopTest();

      System.assertEquals(false, String.isblank(region), 'should return region');
    }

    @isTest
    static void testGetAgreement() {
      Apttus__APTS_Agreement__c agreement = getAgreement();

      Test.startTest();
          Apttus__APTS_Agreement__c testAgreement = CNT_CPQ_AgreementSection.getAgreement(agreement.Id);
      Test.stopTest();

      System.assertNotEquals(null, testAgreement, 'Should return agreement');
    }

    @isTest
    static void testGetApprovalMatrixListForTSSU() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_PL, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            '', 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<Approval_Matrix__c> approvalMatrixList = CNT_CPQ_AgreementSection.getApprovalMatrixList(agreement, approverGroup.Name);
        Test.stopTest();

        System.assertEquals(true, approvalMatrixList.size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORTSSU0To5LaboeFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_TSSU, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[0].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORTSSU5TO10LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 6;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_TSSU, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[0].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORTSSU10TO20LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 15;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_TSSU, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[0].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORTSSU20TO50LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 21;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_TSSU, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[0].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORTSSUMORTHAN50LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 60;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_TSSU, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[0].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORSALESANDACCOUNT0To5LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        System.debug('category value'+agreement.Apttus__Account__r.Category__c);
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_SALES_AND_ACCOUNT_MANAGEMENT, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[1].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORSALESANDACCOUNT5TO10LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 6;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_SALES_AND_ACCOUNT_MANAGEMENT, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[1].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORSALESANDACCOUNT10TO20LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 15;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_SALES_AND_ACCOUNT_MANAGEMENT, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[1].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORSALESANDACCOUNT20TO50LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 21;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_SALES_AND_ACCOUNT_MANAGEMENT, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[1].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORSALESANDACCOUNTMORTHAN50LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 60;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_SALES_AND_ACCOUNT_MANAGEMENT, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', CON_CPQ.APPROVER_SALES_LOCAL_PHARMA);
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[1].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORPL0To5LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        System.debug('category value'+agreement.Apttus__Account__r.Category__c);
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_PL, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', '');
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[2].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORPL5TO10LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 6;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_PL, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', '');
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[2].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORPL10TO20LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 15;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_PL, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', '');
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[2].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORPL20TO50LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 21;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_PL, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', '');
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[2].size()>0, 'should return approvalMatrixList');
    }

    @isTest
    static void testGetApproverDataFORPLMORTHAN50LaborFees() {

        Apttus__APTS_Agreement__c agreement = getAgreement();
        Apttus_CMConfig__AgreementSummaryGroup__c testAgreementSummaryGroup = [SELECT Id, Apttus_CMConfig__ExtendedPrice__c FROM Apttus_CMConfig__AgreementSummaryGroup__c LIMIT 1];
        testAgreementSummaryGroup.Apttus_CMConfig__ExtendedPrice__c = 60;
        update testAgreementSummaryGroup;
        Approver_Group__c approverGroup = UTL_TestData.createApproverGroup(CON_CPQ.APPROVER_GROUP_PL, 'general');
        insert approverGroup;
        Approval_Matrix__c approvalMatrix = UTL_TestData.createApprovalMatrix(approverGroup, agreement.Opportunity_Type__c,
            CON_CPQ.REGION_EMEA, 'Oncology', 'Test User', '', '', '', '', '');
        insert approvalMatrix;

        Test.startTest();
            List<List<String>> approverData = CNT_CPQ_AgreementSection.getApproverData(agreement.Id);
        Test.stopTest();

        System.assertEquals(true, approverData[2].size()>0, 'should return approvalMatrixList');
    }
}