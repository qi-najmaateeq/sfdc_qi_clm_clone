@isTest
public class TST_UTL_TestData{
    
    @isTest
    public static void testMethod1() {
        Opportunity testOpp = new Opportunity();
        testOpp.CloseDate = Date.newInstance(2019, 07, 13);
        testOpp.AccountId = new Account().Id;
        Test.startTest();
        UTL_TestData.createAccount();
        UTL_TestData.createContact(null);
        UTL_TestData.createContacts(null,null);
        UTL_TestData.createLead();
        UTL_TestData.createContractInOutLog(new Contract());
        UTL_TestData.createContract(testOpp, 'MSA_Consolidation');
        Test.stopTest();
    }
    
    @isTest
    public static void testMethod2() {
        Test.startTest();
        UTL_TestData.createOpportunity(null);
        UTL_TestData.createOpportunityContactRole(null,null);
        UTL_TestData.createProduct();
        UTL_TestData.createFavoriteProduct(new Product2());
        UTL_TestData.createPricebookEntry(null);
        UTL_TestData.createOpportunityLineItem(null,null);
        UTL_TestData.createBillingSchedule(null);
        Test.stopTest();
    }
    
    @isTest
    public static void testMethod3() {
        Test.startTest();
        UTL_TestData.createBillingSchedule(null);
        UTL_TestData.createDefaultProductSearch(null);
        UTL_TestData.createOpportunitySplit(null,null,null);
        UTL_TestData.createOpportunityTeamMember(null,null);
        UTL_TestData.createUser('system administrator',2);
        UTL_TestData.createOpportunityLineItemSchedule(null);
        UTL_TestData.createCompetitor();
        Test.stopTest();
    }
    
    @isTest
    public static void testMethod4() {
        Opportunity testOpp = new Opportunity();
        List<Address__c> testAddList = new List<Address__c>();
        for(Integer i = 0 ; i < 5;  i++){
            testAddList.add(new Address__c());
        }
        Test.startTest();
        UTL_TestData.createOpportunityLineItemSchedule(null);
        UTL_TestData.createMIBNF(new Opportunity(),new Revenue_Analyst__c());
        UTL_TestData.createMIBNF_Comp(new MIBNF2__c(),testAddList);
        UTL_TestData.createAddresses(new Account());
        UTL_TestData.createSAPContacts(testAddList);
        UTL_TestData.createRevenueAnalyst();
        UTL_TestData.createCPQSettings();
        UTL_TestData.createClientSatSurveyRecord('','','a138E000000N9dr',testOpp.Id);
        UTL_TestData.createDeclinedSurveyApproverGroup('','','','','',1.0);
        UTL_TestData.createCase();
        Test.stopTest();
    }
    
    @isTest
    public static void testMethod5() {
        Test.startTest();
        UTL_TestData.createMulesoftIntegrationControl(null);
        UTL_TestData.createLegacyOrgLink();
        UTL_TestData.createProxySCMAgreement(null);
        UTL_TestData.createProxyProject(null);
        UTL_TestData.createUserContactSync();
        UTL_TestData.createMergeQueue();
        UTL_TestData.createAgreement();
        UTL_TestData.createApplication();
        UTL_TestData.createBidHistoryExternal();
        UTL_TestData.createContractExternal();
        UTL_TestData.createBudgetUnlockSetting();
        UTL_TestData.createGroup('','');
        UTL_TestData.createGroupMember(null,null);
        UTL_TestData.createEmailTemplate('','','');
        UTL_TestData.createAttachment();
        Test.stopTest();
    }
}