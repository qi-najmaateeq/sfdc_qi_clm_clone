/*
 * Version       : 1.0
 * Description   : Test Class for CNT_CSM_PortalReports
 */
@isTest
public class TST_CNT_CSM_PortalReports {
    /**
     * This method used to set up testdata
     */ 
    @testSetup
    static void dataSetup() {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        String profilId2 = [select id from Profile where Name='System Administrator'].Id;
        User accOwner = New User(Alias = 'su',UserRoleId= portalRole.Id, ProfileId = profilId2, Email = 'john2@iqvia.com',IsActive =true ,Username ='john2@iqvia.com', LastName= 'testLastName', CommunityNickname ='testSuNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert accOwner;
        System.runAs (accOwner) {
            Account account = UTL_TestData.createAccount();
            account.ownerId=accOwner.Id;
            insert account;
                       
           
        }
    }
    
    /**
     * This method used to get List<Contact> for current user
     */    
    @IsTest
    static void testGetUserContact() {
        Account acc = [SELECT id FROM Account WHERE Name = 'TestAccount'];
        Contact contact = new Contact( 
        Firstname='Brian', 
        Lastname='Dent', 
        Phone='(619)852-4569', 
        Department='Mission Control', 
        Title='Mission Specialist - Neptune', 
        Email='john@acme.com',
        Portal_Case_Type__c = 'Technology Solutions',
        Contact_User_Type__c='HO User',
        AccountId = acc.Id);
        insert contact;
        String profilId = [select id from Profile where Name='CSM Customer Community Plus Login User'].Id;
        User user = New User(Alias = 'com', Email = 'john@acme.com',IsActive =true , ContactId = contact.Id, ProfileId = profilId,Username =' john@acme.com', LastName= 'testLastName', CommunityNickname ='testCommunityNickname', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
        insert user;
        Test.startTest();
        List<Contact> contacts = new List<Contact>();
        system.runAs(user){
            contacts =  CNT_CSM_PortalReports.getUserContact();
        }
        Test.stopTest();
        Integer expected = 1;
        Integer actual = contacts.size();
        System.assertEquals(expected, actual);
    }
}