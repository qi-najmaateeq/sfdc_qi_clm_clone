public without sharing class ApprovalItems {
    //public List<ProcessInstanceWorkitem> pendingItems {get; set;}
    public transient List<ItemWrapper> ItemWrapperList {get; set;}
    private static Map<String, Schema.SObjectType> gd;
    private transient Set<Id> memberOfQueue = new Set<Id>();
    //public Contact contactObj {get;set;}
    public transient pse__Proj__c project {get;set;}
    public boolean isShowHeader {get;set;}
    public boolean isShowsidebar {get;set;}
    // Dilip for Case -00280089 31-Jan-2017 start
    public string clickOnAllSelectItems{get;set;}
    public boolean checkboxAllValue {get;set;}
    public Integer itemPageSize {get;set;}
    public Integer itemTotalRecord {get;set;}
    public Integer itemTotalPages {get;set;}
    public Integer itemPageNumber{get;set;}
    public List<ItemWrapper> itemListOfToShow {get; set;}
    public Integer ItemPageSizeMultiplier{get;set;} 
    // Dilip for Case -00280089 31-Jan-2017 end
    // Added by Pramod
    private String bnfObjectPrefix {get; set;}
    private String mibnfObjectPrefix {get; set;}
    //Added on 20-04-2016
    public Map<Id,Boolean> approvalItems {get; set;}
    public List<Id> bnfList {get; set;}
    public List<Id> miBnfList {get; set;}
    
    public ApprovalItems(ApexPages.StandardController ex) { 
        //Dilip for ER-3150 17-Nov-2016 start 
        // Dilip for Case -00280089 03-Feb-2017 start
        /*set<Id> processTimeCardID = new set<ID>();
        set<Id> processLaborForecastID = new set<ID>();
        set<Id> processBudgetID = new set<ID>();
        set<Id> processPODID = new set<ID>();
        set<Id> processDaysOffID = new set<ID>();*/
        //Dilip for ER-3150 17-Nov-2016 end
        if(ApexPages.currentPage().getParameters().get('isShowHeader') == 'true' && ApexPages.currentPage().getParameters().get('isShowHeader') == 'true')
        {
            isShowHeader = true;
            isShowsidebar = true;        
        }
        else
        {
            isShowHeader = false;
            isShowsidebar = false;
        }
        //Dilip for ER-3150 17-Nov-2016 end

        //Dilip comments for ER-3150 end
        // Dilip for Case -00280089 01-Feb-2017 start
        setItemWrapperValues();
        itemPageNumber = 1;
        if(isShowsidebar)
            itemPageSize = 100;
        else
            itemPageSize = 50;    
        itemPageSizeMultiplier = 1; 
        itemTotalRecord = ItemWrapperList.size();
        itemTotalPages = itemTotalRecord / itemPageSize;
        if(Math.mod(itemTotalRecord,itemPageSize) != 0 ){
            itemTotalPages++;
        }
        reCalculateItemWrapperList();
       // Dilip for Case -00280089 01-Feb-2017 end
       
       //Added by Pramod
       this.bnfObjectPrefix  = BNF2__c.sObjectType.getDescribe().getKeyPrefix();
       this.mibnfObjectPrefix  = MIBNF_Component__c.sObjectType.getDescribe().getKeyPrefix();
       

    }
    // Dilip for Case -00280089 03-Feb-2017 start
    private void setItemWrapperValues()
    {
        set<Id> processTimeCardID = new set<ID>();
        set<Id> processLaborForecastID = new set<ID>();
        set<Id> processBudgetID = new set<ID>();
        set<Id> processPODID = new set<ID>();
        set<Id> processDaysOffID = new set<ID>();
        List<GroupMember> groupMembers = [Select Group.Id From GroupMember where UserOrGroupId = :UserInfo.getUserId() and Group.Type = 'Queue'];

       bnfList = new List<Id>();
         miBnfList = new List<Id>();
         approvalItems = new Map<Id,Boolean>();
        //list queue ids current user is a member of
        memberOfQueue = new Set<Id>();
        memberOfQueue.add(UserInfo.getUserId());
        if(groupMembers.size() > 0)
        {
            for(GroupMember g : groupMembers)
            {
                memberOfQueue.add(g.Group.Id);
            }
        }
        List<ProcessInstanceWorkitem> pendingItems = new List<ProcessInstanceWorkitem>();
        if(isShowsidebar){
            pendingItems = [SELECT ActorId, Actor.Name, Actor.Email, CreatedDate, CreatedBy.Name,CreatedById,
                           ProcessInstance.Status, ProcessInstance.TargetObjectId, 
                           ProcessInstance.TargetObject.Name,ProcessInstance.TargetObject.Type
                           FROM ProcessInstanceWorkitem
                           WHERE ActorId in :memberOfQueue AND 
                
                           ProcessInstance.Status = 'Pending' order by CreatedDate DESC];
            //                               ProcessInstance.TargetObjectId = 'a1VQ0000000D1hk' and
        }
        else
        {
             pendingItems = [SELECT ActorId, Actor.Name, Actor.Email, CreatedDate, CreatedBy.Name,CreatedById,
                            ProcessInstance.Status, ProcessInstance.TargetObjectId, 
                           ProcessInstance.TargetObject.Name,ProcessInstance.TargetObject.Type
                           FROM ProcessInstanceWorkitem
                           WHERE ActorId in :memberOfQueue AND 

                           ProcessInstance.Status = 'Pending' order by CreatedDate DESC limit 50];
//                               ProcessInstance.TargetObjectId = 'a1VQ0000000D1hk' and
        
        }
        gd = Schema.getGlobalDescribe();
        ItemWrapperList = new List<ItemWrapper> ();
        
        for(ProcessInstanceWorkitem wi : pendingItems) {
            ItemWrapper i = new ItemWrapper(wi);
            if(i.tName == 'Timecard')
                processTimeCardID.add(i.i.ProcessInstance.TargetObjectId);
            if(i.tName == 'Labor Forecast')
                processLaborForecastID.add(i.i.ProcessInstance.TargetObjectId);
            if(i.tName == 'Budget')
                processBudgetID.add(i.i.ProcessInstance.TargetObjectId);
            if(i.tName == 'Proof of Delivery')
                processPODID.add(i.i.ProcessInstance.TargetObjectId);
            if(i.tName == 'Days Off/Training')
                processDaysOffID.add(i.i.ProcessInstance.TargetObjectId);
            if(i.tName == 'BNF')   
              miBnfList.add(i.i.ProcessInstance.TargetObjectId);
            if(i.tName == 'Purchase/BNF')   
              bnfList.add(i.i.ProcessInstance.TargetObjectId);                                            
            ItemWrapperList.add(i);
        }
        List<BNF2__c> bnftmpList = [SELECT Id, Revenue_Analyst__r.Owner.type FROM BNF2__c Where Id In :bnfList];
        for(BNF2__c bnf : bnftmpList) {
               approvalItems.put(bnf.Id, bnf.Revenue_Analyst__r.Owner.type == 'Queue');
        }
                
        List<MIBNF_Component__c> mibnftmpList = [SELECT Id, Comp_Revenue_Analyst__r.Owner.type FROM MIBNF_Component__c Where Id In :miBnfList];
        for(MIBNF_Component__c mibnf : mibnftmpList) {
            if(mibnf.Comp_Revenue_Analyst__r.Owner.type == 'Queue') {
              approvalItems.put(mibnf.Id, true);
             } else {
               approvalItems.put(mibnf.Id, false);
             }
        }
        
        //Dilip comments for ER-3150 start
        //project = new pse__Proj__c();
        //contactObj = new contact();
        // Added by Dilip Natani 5-April 2017 ER-3579 add milestone name
        /*List<pse__Timecard_Header__c> lstTimecard = [SELECT Id,name,pse__Total_Hours__c,pse__Project__c,pse__Project__r.name,pse__Milestone__r.name from pse__Timecard_Header__c WHERE id in : processTimeCardID];
        for(ItemWrapper wi : ItemWrapperList) {
            if(wi.tName == 'Timecard')
            {
                for(pse__Timecard_Header__c timecard : lstTimecard)
                {
                    if(timecard.name == wi.i.ProcessInstance.TargetObject.Name)
                    {
                        wi.projectId = timecard.pse__Project__c;
                        wi.timeCardTotalHours = timecard.pse__Total_Hours__c;
                        wi.projectName = timecard.pse__Project__r.name;
                        // Added by Dilip Natani 5-April 2017 ER-3579
                        wi.milestoneName = timecard.pse__Milestone__r.name;                 
                    }                
                }            
            }
        }
        List<PsaLaborForecast__c> lstLaborForecast = [SELECT Id,name,Engagement__c,Engagement__r.name from PsaLaborForecast__c WHERE id in : processLaborForecastID];
        for(ItemWrapper wi : ItemWrapperList) {
            if(wi.tName == 'Labor Forecast')
            {
                for(PsaLaborForecast__c laborForecast : lstLaborForecast)
                {
                    if(laborForecast.name == wi.i.ProcessInstance.TargetObject.Name)
                    {
                        wi.projectId = laborForecast.Engagement__c;
                        wi.projectName = laborForecast.Engagement__r.name;                
                    }                
                }            
            }
        }
        List<pse__Budget__c> lstBudget = [SELECT Id,name,pse__Project__c,pse__Project__r.name from pse__Budget__c WHERE id in : processBudgetID];
        for(ItemWrapper wi : ItemWrapperList) {
            if(wi.tName == 'Budget')
            {
                for(pse__Budget__c budget : lstBudget)
                {
                    if(budget.name == wi.i.ProcessInstance.TargetObject.Name)
                    {
                        wi.projectId = budget.pse__Project__c;
                        wi.projectName = budget.pse__Project__r.name;                   
                    }                
                }            
            }
        }
        List<ProofOfDelivery__c> lstPOD = [SELECT Id,name,Project__c,Project__r.name from ProofOfDelivery__c WHERE id in : processPODID];
        for(ItemWrapper wi : ItemWrapperList) {
            if(wi.tName == 'Proof of Delivery')
            {
                for(ProofOfDelivery__c pod: lstPOD)
                {
                    if(pod.name == wi.i.ProcessInstance.TargetObject.Name)
                    {
                        wi.projectId = pod.Project__c;
                        wi.projectName = pod.Project__r.name;                   
                    }                
                }            
            }
        }
        List<DaysOff__c> lstDaysOff = [SELECT Id,name,Days_Off_Type__c,Days_Off_Type__r.name from DaysOff__c WHERE id in : processDaysOffID];
        for(ItemWrapper wi : ItemWrapperList) {
            if(wi.tName == 'Days Off/Training')
            {
                for(DaysOff__c daysOff : lstDaysOff)
                {
                    if(daysOff.name == wi.i.ProcessInstance.TargetObject.Name)
                    {
                        wi.projectId = daysOff.Days_Off_Type__c;
                        wi.projectName = daysOff.Days_Off_Type__r.name;                   
                    }                
                }            
            }
        } */   
    }
    // Dilip for Case -00280089 03-Feb-2017 end
    //Dilip comments for ER-3150 17-Nov-2016 start
    public PageReference ReassignPage(){
        boolean isIteamSelected = false;
        Boolean IsBNF = false;
        Boolean IsNotBNF = false;
        PageReference pageRef ;
        String targerObjectIds = '';
        //Added Issue-10525   30-03-2017 
        Integer numberOfSelectedItems = 0;
        //Ended Issue-10525   30-03-2017
        
        //Added on 19-04-2017 
        List<Id> idList = new List<Id>();
        //Ended Issue-10525   30-03-2017
        for(ItemWrapper wi : itemListOfToShow) {
            if(wi.isSelected)
            {
                isIteamSelected = true;            
                if(String.valueof(wi.i.ProcessInstance.TargetObjectId).startsWith(bnfObjectPrefix) 
                        || String.valueof(wi.i.ProcessInstance.TargetObjectId).startsWith(mibnfObjectPrefix)) {
                    targerObjectIds += String.valueOf(wi.i.ProcessInstance.TargetObjectId) + ','; 
                    IsBNF = true;
                    //Added Issue-10525  30-03-2017 
                    numberOfSelectedItems++;
                    //Ended  Issue-10525 30-03-2017 
                    
                    //Added on 19-04-2017 
                    iDList.add(String.valueOf(wi.i.ProcessInstance.TargetObjectId));
                } else {
                    IsNotBNF = true;
                }                
            }   
        }
        
        if(IsBNF && IsNotBNF) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'You can\'t select bnf with other ids in same list'));   
            return null;            
        } else if(IsBNF) {
        
            //Added by  Issue-10525  30-03-2017 
            if(numberOfSelectedItems > 5) {
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'You can\'t select more than 5 bnf or mibnf from the list'));
                return null;    
            }
            //Added on 19-04-2017             
            for(Id objId : idList) {
              if(approvalItems.containsKey(objId) && approvalItems.get(objId) == true) {
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'BNF’s assigned to SBS RA’s cannot be reassigned, they must be rejected and resubmitted'));
                  return null; 
              }
            //Ended on 19-04-2017 
              
            }
            //Ended Issue-10525  30-03-2017 

            // show bnf screen and update Issue-10525
            targerObjectIds = targerObjectIds.removeEnd(',');
            pageRef = new PageReference('/apex/BNF_Reassign');
            pageRef.getParameters().put('ids', targerObjectIds);
            pageRef.setRedirect(false);
            return pageRef;
        } else if(IsNotBNF) {
            // goto '/apex/ReassignApprovalItems'
            pageRef = new PageReference('/apex/ReassignApprovalItems');
            pageRef.setRedirect(false);
            return pageRef;
        }
        // Issue-10525
        /*if(isIteamSelected)
        { 
            PageReference pageRef = new PageReference('/apex/ReassignApprovalItems');
            pageRef.setRedirect(false);
            return pageRef;
        }*/
        if(!isIteamSelected)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select at least one row.')); 
            return null;
        } 
        return null; 
    }
    public PageReference ApproveRejectPage(){
        boolean isIteamSelected = false;
        for(ItemWrapper wi : itemListOfToShow) {
            if(wi.isSelected)
            {
                isIteamSelected = true;            
            }       
        }
        if(isIteamSelected)
        { 
            PageReference pageRef = new PageReference('/apex/ApproveRejectApprovalItems');
            pageRef.setRedirect(false);
            return pageRef;
        }
        else
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select at least one row.'));
            return null;
        }  
    }
    public PageReference Reassign1(){
        try
        { 
                List<ProcessInstanceWorkitem> workItemList = new List<ProcessInstanceWorkitem>();
                Set<id> setTargetObjectId = new Set<id>{};
                Approval.ProcessWorkitemRequest[] prWkItems = New Approval.ProcessWorkItemRequest[]{};
                for(ItemWrapper wi : itemListOfToShow) {
                    if(wi.isSelected)
                    {
                        workItemList.add(wi.i);          
                    }  
                }
                /*for(ProcessInstanceWorkitem workItemObj : workItemList)
                {
                    workItemObj.ActorId = contactObj.CES_Contact_Name__c;
                    //system.assert(false,workItemObj.id);
                }*/
            
                update workItemList;
           
            PageReference pageRef = new PageReference('/apex/ApprovalItems?isShowHeader=' + isShowHeader + '&isShowsidebar=' + isShowsidebar);
            pageRef.setRedirect(true);
            return pageRef;
        }
        catch(Exception ex)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getmessage()));        
        }  
        return null;  
    }
    public PageReference Approve1(){
        try { 
            Approval.ProcessWorkitemRequest[] prWkItems = New Approval.ProcessWorkItemRequest[]{};
            Set<id> setTargetObjectId = new Set<id>{};
            for(ItemWrapper wi : ItemListOfToShow) {
                if(wi.isSelected)
                {
                    setTargetObjectId.add(wi.i.ProcessInstance.TargetObjectId);          
                }       
            }
            //Approved the record
            ProcessInstance[] pi = [Select ID, Status, TargetObject.Name, 
                (SELECT Id, ActorId, ProcessInstanceId FROM Workitems),
                (SELECT Id, StepStatus, Comments FROM Steps) From ProcessInstance 
                Where TargetObjectID IN :setTargetObjectId AND Status = 'Pending'];           
            for(ProcessInstance instance : pi){  
                for(ProcessInstanceWorkItem workItem : instance.WorkItems){
                    Approval.ProcessWorkitemRequest prWkItem = new Approval.ProcessWorkitemRequest();
                    prWkItem.setWorkItemID(workItem.id);
                    //prWkItem.setComments(string.valueof(contactObj.CES_Signatory_Name__c));
                    prWkItem.setAction('Approve');
                    //prWkItem.setNextApproverIds(new Id[] {contactObj.CES_Contact_Name__c});
                    prWkItems.add(prWkItem);
                }
            }          
            
            if(!prWkItems.isEmpty()){
                Approval.ProcessResult[] appResult = Approval.process(prWkItems);
            }
            PageReference pageRef = new PageReference('/apex/ApprovalItems?isShowHeader=' + isShowHeader + '&isShowsidebar=' + isShowsidebar);
            pageRef.setRedirect(true);
            return pageRef;
        }
        catch(Exception ex)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,ex.getmessage()));        
        }
        return null; 
    }

    public PageReference Reject1(){
        try{
            Approval.ProcessWorkitemRequest[] prWkItems = New Approval.ProcessWorkItemRequest[]{};
            Set<id> setTargetObjectId = new Set<id>{};
            for(ItemWrapper wi : itemListOfToShow) {
                if(wi.isSelected)
                {
                    setTargetObjectId.add(wi.i.ProcessInstance.TargetObjectId);          
                }       
            }
            //Reject the record
            ProcessInstance[] pi = [Select ID, Status, TargetObject.Name, 
                (SELECT Id, ActorId, ProcessInstanceId FROM Workitems),
                (SELECT Id, StepStatus, Comments FROM Steps) From ProcessInstance 
                Where TargetObjectID IN :setTargetObjectId AND Status = 'Pending'];           
            for(ProcessInstance instance : pi){  
                for(ProcessInstanceWorkItem workItem : instance.WorkItems){
                    Approval.ProcessWorkitemRequest prWkItem = new Approval.ProcessWorkitemRequest();
                    prWkItem.setWorkItemID(workItem.id);
                    //prWkItem.setComments(string.valueof(contactObj.CES_Signatory_Name__c));
                    prWkItem.setAction('Reject');
                    //prWkItem.setNextApproverIds(new Id[] {contactObj.CES_Contact_Name__c});
                    prWkItems.add(prWkItem);
                }
            }          
            
            if(!prWkItems.isEmpty()){
                Approval.ProcessResult[] appResult = Approval.process(prWkItems);
            }
            PageReference pageRef = new PageReference('/apex/ApprovalItems?isShowHeader=' + isShowHeader + '&isShowsidebar=' + isShowsidebar);
            pageRef.setRedirect(true);
            return pageRef;
        }
        catch(Exception ex)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,ex.getmessage()));        
        }
        return null;     
    }         
     public PageReference Cancel1(){
        //contactObj = new contact();
        PageReference pageRef = new PageReference('/apex/ApprovalItems?isShowHeader=' + isShowHeader + '&isShowsidebar=' + isShowsidebar);
        pageRef.setRedirect(true);
        return pageRef; 
    }
    //Dilip comments for ER-3150 17-Nov-2016 end
    // Dilip for Case -00280089 31-Jan-2017 start
    public PageReference getSelected() {     
        for(ItemWrapper item :itemListOfToShow)
        {
            item.isSelected = checkboxAllValue;                   
        }       
        return null;
    }
     // method to reintialize the list which is shown on page
    private void reCalculateItemWrapperList(){
        itemListOfToShow = new List<ItemWrapper>();
        Integer iterator = (itemPageSize * (itemPageNumber - 1));
        while (iterator < itemPageSize * itemPageNumber){
            if(iterator < itemTotalRecord){
                ItemListOfToShow.add(ItemWrapperList.get(iterator));
                iterator++;
            }else{
                break;
            }
        }
    }
    
   //TimeCard Pagination 
    public Boolean ItemhasNext {  
        get {  
            if(itemPageNumber >= itemTotalPages || itemTotalPages == 0){
                return false;
            }
            return true;
        }  
        set;  
    }  
   
    //Boolean to check if there are more records before the present displaying records  
    public Boolean ItemhasPrevious {  
        get {  
            if(itemPageNumber <= 1 || itemTotalPages == 0){
                return false;
            }
            return true;
        }  
        set;  
    }  
   
    //Returns the previous page of records  
    public PageReference Itemprevious() {  
        itemPageNumber--;
        setItemWrapperValues();
        reCalculateItemWrapperList();
        return null;  
    }  
   
    //Returns the next page of records  
    public PageReference itemNext() {
        system.debug('Dilip ### start Itemnext');  
        itemPageNumber++;
        setItemWrapperValues();   
        reCalculateItemWrapperList();
        //Added by Priyamvada
        itemPageSizeMultiplier++;
        system.debug('Dilip ### end Itemnext');
        return null;
    }  
    
    //Returns the first page of records  
    public PageReference itemfirstPage() {  
        itemPageNumber = 1; 
        setItemWrapperValues(); 
        reCalculateItemWrapperList();
        return null;
    }  
    
    //Returns the last page of records  
    public PageReference itemlastPage() {  
        itemPageNumber = itemTotalPages;
        setItemWrapperValues();  
        reCalculateItemWrapperList();
        return null;  
    }    
     
   // Dilip for Case -00280089 31-Jan-2017 end
    public class ItemWrapper {
        public ProcessInstanceWorkitem i {get; set;}
        public boolean isSelected {get; set;}
        public String tName {get; private set;}
        //Dilip comments for ER-3150
        public decimal timeCardTotalHours {get; set;}
        public id projectId{get; set;}
        // Added by Dilip Natani ER- 3579 on 5-april-2017
        public string milestoneName{get;set;}
        public string projectName{get; set;}
        public ItemWrapper (ProcessInstanceWorkitem i1){
            i = i1;
            tName = gd.get(i.ProcessInstance.TargetObject.Type).getDescribe().getLabel();
        }
    }
    
    /*public static testmethod void testItemWrapper(){
        //create an approval request
        // Insert an account
        Account a = new Account(Name='Test',annualRevenue=100.0);
        insert a;    
        // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(a.id);
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        // Verify the result
        System.assert(result.isSuccess());
        
        ProcessInstanceWorkItem piw = [select id,ProcessInstance.TargetObject.Type from ProcessInstanceWorkItem
                                where processInstance.TargetObjectId =: a.id limit 1];
        
        ApexPages.standardController st = new ApexPages.StandardController(piw);
        ApprovalItems ai = new ApprovalItems (st);
        
        ItemWrapper itw = new ItemWrapper(piw);
        System.assert(itw.tname == 'Account');
        
    }*/
}