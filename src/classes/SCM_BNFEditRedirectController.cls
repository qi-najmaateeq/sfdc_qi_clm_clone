public class SCM_BNFEditRedirectController 
{
    public String newRedirectionFullURL{get; private set;}
    public String orgDomainURL{get;private set;}
    public SCM_BNFEditRedirectController()
    {
        onLoadRedirection();
    } 

    public void onLoadRedirection()
    {
        Map<string, string> urlParameterMap = new MAp<string, string>();
        orgDomainURL = URL.getOrgDomainUrl().toExternalForm();
        urlParameterMap  = ApexPages.currentPage().getParameters();
        String newRedirectionURL = ApexPages.currentPage().getParameters().get('newURL');
        Boolean needDecoding = true;
        if(newRedirectionURL.containsIgnoreCase('BNF_Sales_Org_Selector')) {
            needDecoding = false;
            orgDomainURL = '';
        }  
        string baseURL = '';
        Map<String, String> fieldApiToIdmap = UTL_Sobject.getCustomFieldByIds('BNF2');
        Map<Id, Opportunity> oppMapWithAgreements = new map<id, Opportunity>(); 
        string oppId = ((!string.isBlank(newRedirectionURL)&& newRedirectionURL.contains('=') && newRedirectionURL.split('=').size()>1)?  (newRedirectionURL.split('=')[1].contains('/')? newRedirectionURL.split('=')[1].remove('/') : newRedirectionURL.split('=')[1]) : '' ); 
        Contract_Management_Setting__c contractSetting = Contract_Management_Setting__c.getInstance();
        
        if(!string.isBlank(OppId) && ConstantClass.allowSalesOrgUpdateOnBNF_FromSCM && contractSetting != null && contractSetting.Allow_SalesOrg_Update_In_BNF_From_SCM__c)
        {
            oppMapWithAgreements = new map<id, Opportunity>([SELECT Id, (SELECT Id,IMS_Legal_Entity__c FROM Proxy_SCM_Agreements__r where Record_Type_Name__c = : contractSetting.SOW__c and is_Amendment_Record__c = false ) FROM Opportunity where id = : oppId]);
            if(oppMapWithAgreements.size() > 0 )
            {
                map<string, string> salesCodetoSalesOrgNameInBNFMap = ConstantClass.getSalesOrgNameinBNFSystem1('Purchase BNF');
                string IMSSalesOrgCode = '';
                string legalEntity = '';
                
                //Get legal entity of agreement based on opportunity
                legalEntity =  ConstantClass.checkAgreementLegalEntityForOpportunity(oppId, oppMapWithAgreements);             
                //get sales org code based on legal entity
                IMSSalesOrgCode = ConstantClass.getSalesOrgCodeforAgreementLegalEntity(legalEntity);
                
                //set Sales org name and code on BNF   
                if( salesCodetoSalesOrgNameInBNFMap != null && !string.isblank(IMSSalesOrgCode) && salesCodetoSalesOrgNameInBNFMap.containsKey(IMSSalesOrgCode))
                {
                    urlParameterMap.put(fieldApiToIdmap.get('IMS_Sales_Org__c'), salesCodetoSalesOrgNameInBNFMap.get(IMSSalesOrgCode));
                }
            }
        }
        
        if(!string.isBlank(newRedirectionURL))
        {
            newRedirectionFullURL  = newRedirectionURL;
            
            for(string urlParam : urlParameterMap.keySet())
            {
                if(urlParam != 'newURL' && urlParameterMap.get(urlParam) != '' && urlParameterMap.get(urlParam) != 'undefined')
                {
                    if(needDecoding && urlParam.contains('__c')) {
                        newRedirectionFullURL += '&' + fieldApiToIdmap.get(urlParam) + '=' + EncodingUtil.urlEncode(urlParameterMap.get(urlParam), 'UTF-8'); 
                    } else {
                    	newRedirectionFullURL += '&' + urlParam + '=' + EncodingUtil.urlEncode(urlParameterMap.get(urlParam), 'UTF-8'); 
                	}
                }
            }
        }
    }

}