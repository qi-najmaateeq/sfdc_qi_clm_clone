public class CNT_CSM_CallcenterInboundPopup {
    
    public String greetingText {get; set;}
    public String pv1 {get;set;}
    public String pv2 {get;set;}
    public String pv3 {get;set;}
    public String pv4 {get;set;}
    public String pv5 {get;set;}
    public String pv6 {get;set;}
    public String pv7 {get;set;}
    public String pv8 {get;set;}
    public String pv9 {get;set;}
    public String pv10 {get;set;}
    public String callerId {get;set;}
    public List<Contact> contacts {get; private set;}
    public List<Case> cases {get; private set;}
    public Boolean displayPopup {get;set;}
    public List<EXT_CSM_CheckboxDetails> recordTypes {get;set;}
    public String selectedRecord {get;set;}
    public List<InboundNumber__c> inboundList =null;
    public String RecordTypeName  {get;set;}
    
    
    public CNT_CSM_CallcenterInboundPopup() {
        
        callerId = ApexPages.currentPage().getParameters().get('ANI');
        pv1 = ApexPages.currentPage().getParameters().get('pv1'); // pq
        pv2 = ApexPages.currentPage().getParameters().get('pv2'); // vmbox 
        pv3 = ApexPages.currentPage().getParameters().get('pv3'); // project
        pv4 = ApexPages.currentPage().getParameters().get('pv4'); // country
        pv5 = ApexPages.currentPage().getParameters().get('pv5'); // language
        pv6 = ApexPages.currentPage().getParameters().get('pv6'); // queue
        pv7 = ApexPages.currentPage().getParameters().get('pv7'); // study
        pv8 = ApexPages.currentPage().getParameters().get('pv8'); // rona ****
        pv9 = ApexPages.currentPage().getParameters().get('pv9'); // los
        pv10 = ApexPages.currentPage().getParameters().get('pv10'); // last wave ****
        displayPopup = false;
        
        System.debug('callerId : '+callerId +'pq__c pv1 : '+pv1+' pv2 : '+pv2 +' project__c pv3 : '+pv3+' country__c pv4 : '+pv4+' language__c pv5 : '+ pv5+' queue__c pv6 : '+pv6 +' study__c pv7 : '+pv7+' los__c pv9 : '+pv9);
        inboundList = [select id, greeting_text__c,Study__c,LOS__C,Salesforce_LOS__c  from inboundnumber__c where project__c =:pv3 and country__c=:pv4 and language__c=:pv5 and queue__c=:pv6 and study__c=:pv7 and los__c=:pv9];
        Map<Id,Contact> mapContact=null;
        if(callerId != null){
            mapContact = new SLT_Contact().selectByMobile(callerId.replaceAll('\\D', ''));
        }
        if(mapContact != null && mapContact.isEmpty() == false){
            cases = new SLT_Case().selectByContactId(mapContact.keySet());  
            contacts = mapContact.values();
        }
        
        EXT_CSM_CheckboxDetails record=null;
        recordTypes = new List<EXT_CSM_CheckboxDetails>();
        for(RecordTypeInfo info: Case.SObjectType.getDescribe().getRecordTypeInfos()) {
            if(info.isAvailable()) {
                if(!('Master'.equalsIgnoreCase(info.getName()))){
                    record = new EXT_CSM_CheckboxDetails(String.valueOf(info.getRecordTypeId()),info.getName(),false);
                    recordTypes.add(record);  
                }
                System.debug('record value : '+record.value + ' record Label : '+record.Label + ' record Checked'+record.Checked);
            }
        }
        if(recordTypes.isEmpty() == false){
            recordTypes[0].checked = true;
            selectedRecord = recordTypes[0].value;
            RecordTypeName = recordTypes[0].Label;
        }
        if (inboundList != null && inboundList.size() > 0) {
            System.debug('found one match for inbound number');
            greetingText = inboundList[0].greeting_text__c;
        }else {
            inboundList = [select id, greeting_text__c,Study__c,LOS__c,Salesforce_LOS__c  from inboundnumber__c where pq__c='DEFAULT'];
            greetingText = inboundList[0].greeting_text__c;    
        }
        if(greetingText.contains('_______')){
            greetingText= greetingText.replaceAll('_______', '<b>'+UserInfo.getName()+'</b>');
        }
        
        
    }
    
    public void displayPopUpModel() {
        displayPopup = true;
    }
    public void closePopUpModel() {
        displayPopup = false;
    } 
    public PageReference createCase() {
        
        if(recordTypes != null && recordTypes.isEmpty() == false && recordTypes.size() == 1){
            selectedRecord =  recordTypes[0].value;
        }
        
        for(EXT_CSM_CheckboxDetails record : recordTypes){
            if(record.value.equalsIgnoreCase(selectedRecord)){
                RecordTypeName = record.Label;
            }else if(selectedRecord == null && record.checked == true){
                selectedRecord = record.value;
                RecordTypeName = record.Label;
            }   
        } 
        
        Case newCase = new Case();
        
        if(RecordTypeName != null && (CON_CSM.S_R_D_ASSISTANCE_R_T.equalsIgnoreCase(RecordTypeName) || CON_CSM.S_R_D_ACTIVITY_R_T.equalsIgnoreCase(RecordTypeName)))
        {
            
            List<Study__c> study = (inboundList.isEmpty() == false && inboundList[0].Study__c != null) ? new SLT_StudyC().selectAccountByStudyName(inboundList[0].Study__c,new Set<String>{'Id','Name'}) : null;
            if(study != null && study.isEmpty() == false){
              newCase.Study__c = study[0].Id;  
            }  
            if((inboundList != null && inboundList.isEmpty() == false && inboundList[0].LOS__c != null)){
                newCase.LOS__c = inboundList[0].Salesforce_LOS__c;
            }  
            
        }else if(RecordTypeName != null && (CON_CSM.S_DATA_CASE_R_T.equalsIgnoreCase(RecordTypeName) || CON_CSM.S_TECHNOLOGY_R_T.equalsIgnoreCase(RecordTypeName)) && contacts != null && contacts.size() == 1){
            newCase.AccountId = contacts[0].AccountId;
        }
        if(RecordTypeName != null && CON_CSM.S_TECHNOLOGY_R_T.equalsIgnoreCase(RecordTypeName)){
                newCase.PhoneVerification__c = true;
        }
        if(contacts != null && contacts.size() == 1){
            newCase.ContactId = contacts[0].Id;
        }
        newCase.RecordTypeId = selectedRecord;
        newCase.Origin = CON_CSM.S_PHONE;
        
        
        insert newCase;
        return new PageReference('/' + newCase.id);
    }
}