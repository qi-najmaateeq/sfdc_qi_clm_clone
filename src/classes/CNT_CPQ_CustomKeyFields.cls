public class CNT_CPQ_CustomKeyFields {

    @auraEnabled
    public static Boolean checkIfCurrentUserInQCGroup() {

        List<GroupMember> groupMemberList = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.Name =: CON_CPQ.CPQ_QC_REVIEWER AND UserOrGroupId =: UserInfo.getUserId()];
        if(groupMemberList.size() > 0){
            return true;
        }
        return false;
    }

    @auraEnabled
    public static Boolean checkIfCurrentUserIsLineManager(Id agreementId) {

        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.OWNER};
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementFieldsById(new Set<Id>{agreementId},
        fieldSet);
        if(agreementList.size() > 0 && agreementList[0].OwnerId != null){

            Set<String> UserFieldSet = new Set<String>{CON_CPQ.MANAGERID} ;
            Map<Id, User> userMap = new SLT_User().selectByUserId(new Set<Id>{agreementList[0].OwnerId}, UserFieldSet);
            if(!String.isBlank(userMap.get(agreementList[0].OwnerId).ManagerId) && userMap.get(agreementList[0].OwnerId).ManagerId == UserInfo.getUserId()){
                return true;
            }
        }
        return false;
    }
}