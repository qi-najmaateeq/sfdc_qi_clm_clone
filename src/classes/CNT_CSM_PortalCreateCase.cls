/*
 * Version       : 1.0
 * Description   : Apex Controller for LXC_CSM_PortalCreateCase Lightning component 
 */ 
public class  CNT_CSM_PortalCreateCase {
    /**
     * This method used to return List<AggregateResult> of  Product__r.Name Categorization fro a current user
     * @return  List<AggregateResult>
     */   
    @AuraEnabled
    public static List<EXT_CSM_CheckboxDetails> getProductCategorizationForNewCase(String accountId,String recordType){
        List<Asset> products =  new List<Asset>();
        List<AggregateResult> productList = new  List<AggregateResult>();
        List<EXT_CSM_CheckboxDetails> keyValues = new  List<EXT_CSM_CheckboxDetails>();
        EXT_CSM_CheckboxDetails obj=null;
        Map<String,String> c = new Map<String,String>();
        try {
            Set<String> fieldSet = new Set<String>();
            fieldSet.add('Product2.Name');
            fieldSet.add('Id');
            String filterCondition ='';
            filterCondition = ' AccountId=\''+ accountId +'\' AND Status !=\'Obsolete\' AND Visible_in_CSH__c = true';
            products = new SLT_Asset().getAssetsWithFilter(fieldSet,filterCondition);      
            //users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
            //products = new SLT_Asset().selectByAccountId(new Set<Id> {accountId});
            for (Asset ast : products){
                c.put(ast.Product2.Name,ast.Id);
            }
            productList = new SLT_CaseCategorization().getProducts(c.keySet(),recordType);
            for(AggregateResult result : productList){
                if(c.containsKey(String.valueOf(result.get('Name')))){
                   obj = new EXT_CSM_CheckboxDetails(c.get(String.valueOf(result.get('Name'))),String.valueOf(result.get('Name')),false);
                    keyValues.add(obj);
                } 
            }
            return keyValues;
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
    
    /**
     * This method used to return List<AggregateResult> of  SubType1__c Categorization by given productName
     * @params  String productName
     * @return  List<AggregateResult>
     */ 
    @AuraEnabled
    public static List<AggregateResult> getSubtypeCategorization(String productName, String origin){
        List<AggregateResult> subtypeList = new  List<AggregateResult>();
        try {
            // PEP-ACN added new param to distinguish PEP subtypes from CSH 
            if(origin == CON_PEP.S_PARTNER_PORTAL){
                subtypeList = new SLT_CaseCategorization().getPEPSubTypeByProductName(productName);
            }else{
                subtypeList = new SLT_CaseCategorization().getCSHSubTypeByProductName(productName);
            }
            return subtypeList;
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
        
    @AuraEnabled
    public static List<CSM_QI_Case_Categorization__c> getCategorization(String productName,String cshSubtype){   
        List<CSM_QI_Case_Categorization__c> categorizations= new List<CSM_QI_Case_Categorization__c>();
        if(cshSubtype != null && !'Please Specify'.equalsIgnoreCase(cshSubtype)){
            categorizations = new SLT_CaseCategorization().getCaseCategorizationByFilter('Product__r.Name=\''+productName+'\' and CSHSubType__c=\''+cshSubtype+'\'');    
        }else{
            categorizations = new SLT_CaseCategorization().getCaseCategorizationByFilter('Product__r.Name=\''+productName+'\' and SubType1__c=\'Please Specify\'');    
        }
        
        return categorizations;
    }
    
    @AuraEnabled
    public static List<Asset> getAssetByProductId(String productId,String accountId){ 
        List<Asset> assets=new List<Asset>();
        assets = new SLT_Asset().selectByAccountIdAndProductId(new Set<Id> {accountId},new Set<Id>{productId});    
        return assets;
    }
    
    @AuraEnabled
    public static List<Account> getUserAccount(){ 
        List<User> users = new List<User>();  
        users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        List<Account> accounts=new List<Account>();
        accounts = new SLT_Account().selectById(new Set<Id> {users[0].AccountId});    
        return accounts;
    }
    
    @AuraEnabled
    public static List<Contact> getUserContact(){ 
        List<User> users = new List<User>();
        users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        List<Contact> contacts=new List<Contact>();
        Set<string> contactDetailsSet= new Set<string>();
        contactDetailsSet.Add('Country__c');
        contactDetailsSet.Add('Portal_Case_Type__c');
        contactDetailsSet.Add('Contact_User_Type__c');
        if(users[0].ContactId!=null)
        {
            contacts = new SLT_Contact().selectByContactIdList(new Set<Id> {users[0].ContactId}, contactDetailsSet);
        }
        return contacts;
    }
    
    @AuraEnabled
    public static List<String> getPickListOptions(String fld){
        List<String> options =  new List<String>();
        Schema.DescribeFieldResult fieldResult = Schema.getGlobalDescribe().get('Case').getDescribe().fields.getMap().get(fld).getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)
        {
            options.add(f.getValue());
        }
        return options;
    } 
    
    @AuraEnabled
    public static List<Asset> getUserAssetsForDATA2(List<String> names){ 
        List<User> users = new List<User>();  
        users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        List<Asset> assets=new List<Asset>();
        String filter='Name in(';
        for( Integer i=0; i<names.size();i++){
           filter+='\''+names[i]+ '\'';
           if (i != names.size()-1)filter+=',';
        } 
        filter+=')';
        filter+=' AND AccountId = \''+users[0].AccountId + '\'';
        assets = new SLT_Asset().getAssetsWithFilter(new Set<String>{'Id','Name'}, filter);    
        return assets;
    }
    
	@AuraEnabled
    public static List<string> getpriorityvalue(){
        List<string> picList= getPickListOptions('Priority');
        return picList;
    }
    
    @AuraEnabled
    public static List<string> getUrgencyList(){
        List<string> picList= getPickListOptions('Urgency__c');
        return picList;
    }
    
    @AuraEnabled
    public static List<string> getImpactList(){
        List<string> picList= getPickListOptions('Techno_Impact__c');
        return picList;
    }
    
    @AuraEnabled
    public static List<AggregateResult> getCategorizationWithAggregate(String q){
        try {
           return new SLT_CaseCategorization().selectWithAggregate(q);
        } catch(Exception ex) {
            ApexPages.addMessages(ex);
            throw new AuraHandledException(JSON.serialize(ex));
        }
    }
}