/**
* This is Task Domain handler class.
* version : 1.0
*/
public class DAOH_Task {
    
    /**
    * This method is used for create a new Data Audit Trail when the new task record is created.
    * @params  newList List<Task>
    * @return  void
    */
    
    public static void saveAuditLogAfterInsertTask(List<Task> newList){
        CSM_QI_Data_Audit_Trail__c auditTrail = null;
        Map<ID,Schema.RecordTypeInfo> rt_Map  =  Task.sObjectType.getDescribe().getRecordTypeInfosById();
        List<CSM_QI_Data_Audit_Trail__c> auditTrailList = new List<CSM_QI_Data_Audit_Trail__c>();
        if(newList.size() > 0)
        {
            for(Task c : newList) {
                auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_CREATED,Name = CON_CSM.S_TASK,Task_Id__c  =  c.Id);
                auditTrailList.add(auditTrail);
            }
            
        }
        
        try {
            if(auditTrailList != null && auditTrailList.size() > 0){
                insert auditTrailList;  
            }
        } catch (DmlException e) {
            System.debug('Failed due to : '+e);
        }
        
    }
    /**
    * This method is used for create a new Data Audit Trail when the task record fields are updated.
    * @params  newList List<Task>,oldMap Map<Id, Task> 
    * @return  void
    */
    public static void saveAuditLogAfterUpdateTaskFields(List<Task> newList, Map<Id, Task> oldMap,List<FieldDefinition> fields) {
        
        if(fields != null && fields.size() > 0){
            
            CSM_QI_Data_Audit_Trail__c auditTrail = null;
            List<CSM_QI_Data_Audit_Trail__c> auditTrailList = new List<CSM_QI_Data_Audit_Trail__c>();
            Map<ID,Schema.RecordTypeInfo> rt_Map  =  Task.sObjectType.getDescribe().getRecordTypeInfosById();
            EXT_CSM_CaseRelatedToObject relatedTo = null;
            List<EXT_CSM_CaseRelatedToObject> relatedToList = new List<EXT_CSM_CaseRelatedToObject>(); 
            String objRelName = null;
            for(Task c : newList) {
                
                if(fields.size() > 0 ) 
                {
                    for( FieldDefinition fd: fields){
                        if(fd.QualifiedApiName.equals(CON_CSM.S_PHONE)) continue;
                        if(fd.QualifiedApiName.equals(CON_CSM.S_EMAIL)) continue;
                        
                        if(String.isBlank(fd.ExtraTypeInfo) && ((fd.DataType.contains(CON_CSM.S_TEXT) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_PICKLIST) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_NUMBER) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_DOUBLE)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_URL) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_PHONE) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_EMAIL) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_CHECKBOX) && fd.ValueTypeId.equalsIgnoreCase(CON_CSM.S_BOOLEAN)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_FORMULA) && fd.ValueTypeId.equalsIgnoreCase(CON_CSM.S_TEXT)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_DATE) && fd.ValueTypeId.equalsIgnoreCase(CON_CSM.S_DATETIME)) || (fd.DataType.startsWithIgnoreCase(CON_CSM.S_DATE) && fd.ValueTypeId.equalsIgnoreCase(CON_CSM.S_DATE)))){
                                if(c.get(fd.QualifiedApiName) == null && oldMap.get(c.Id).get(fd.QualifiedApiName) != null){
                                    auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_DELETED,Name = fd.MasterLabel,Old_Value__c = String.valueOf(oldMap.get(c.Id).get(fd.QualifiedApiName)),New_Value__c = String.valueOf(c.get(fd.QualifiedApiName)),Task_Id__c =  c.Id);
                                    auditTrailList.add(auditTrail);
                                }else if(c.get(fd.QualifiedApiName) != null && oldMap.get(c.Id).get(fd.QualifiedApiName) == null){
                                    auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_ADDED,Name = fd.MasterLabel,Old_Value__c = String.valueOf(oldMap.get(c.Id).get(fd.QualifiedApiName)),New_Value__c = String.valueOf(c.get(fd.QualifiedApiName)),Task_Id__c =  c.Id);
                                    auditTrailList.add(auditTrail);
                                }else if(c.get(fd.QualifiedApiName) != null && !c.get(fd.QualifiedApiName).equals(oldMap.get(c.Id).get(fd.QualifiedApiName))){
                                    auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_CHANGED,Name = fd.MasterLabel,Old_Value__c = String.valueOf(oldMap.get(c.Id).get(fd.QualifiedApiName)),New_Value__c = String.valueOf(c.get(fd.QualifiedApiName)),Task_Id__c =  c.Id);
                                    auditTrailList.add(auditTrail);
                                }
                            }else if(String.isBlank(fd.ExtraTypeInfo) && fd.DataType.contains(CON_CSM.S_LOOKUP) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_ID)){
                                if(c.WhatId != null && 'What'.equalsIgnoreCase(fd.RelationshipName)){
                                    objRelName =  String.valueOf(c.WhatId.getSObjectType()); 
                                }else if(c.WhoId != null && 'Who'.equalsIgnoreCase(fd.RelationshipName)){
                                    objRelName =  String.valueOf(c.WhoId.getSObjectType()); 
                                }else{
                                    objRelName =  fd.RelationshipName;   
                                }
                                if('Who'.equalsIgnoreCase(objRelName) || 'What'.equalsIgnoreCase(objRelName) ){
                                    continue; 
                                }
                                if(c.get(fd.QualifiedApiName) == null && oldMap.get(c.Id).get(fd.QualifiedApiName) != null){
                                    relatedTo = new EXT_CSM_CaseRelatedToObject(CON_CSM.S_DELETED,objRelName,CON_CSM.S_TASK,fd.MasterLabel,(Id)oldMap.get(c.Id).get(fd.QualifiedApiName),null,c.Id);
                                    relatedToList.add(relatedTo);
                                }else if(c.get(fd.QualifiedApiName) != null && oldMap.get(c.Id).get(fd.QualifiedApiName) == null){
                                    relatedTo = new EXT_CSM_CaseRelatedToObject(CON_CSM.S_ADDED,objRelName,CON_CSM.S_TASK,fd.MasterLabel,null,(Id)c.get(fd.QualifiedApiName),c.Id);
                                    relatedToList.add(relatedTo);    
                                }else if(c.get(fd.QualifiedApiName) != null && oldMap.get(c.Id).get(fd.QualifiedApiName) != null && !c.get(fd.QualifiedApiName).equals(oldMap.get(c.Id).get(fd.QualifiedApiName))){
                                    relatedTo = new EXT_CSM_CaseRelatedToObject(CON_CSM.S_CHANGED,objRelName,CON_CSM.S_TASK,fd.MasterLabel,(Id)oldMap.get(c.Id).get(fd.QualifiedApiName),(Id)c.get(fd.QualifiedApiName),c.Id);
                                    relatedToList.add(relatedTo);
                                }
                            }else if(String.isNotBlank(fd.ExtraTypeInfo) && fd.DataType.contains(CON_CSM.S_TEXT) && fd.ValueTypeId.startsWithIgnoreCase(CON_CSM.S_STRING)){
                                if(c.get(fd.QualifiedApiName) != null && !c.get(fd.QualifiedApiName).equals(oldMap.get(c.Id).get(fd.QualifiedApiName))){
                                    auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = CON_CSM.S_EDITED,Name = fd.MasterLabel,Old_Text_Value__c = String.valueOf(oldMap.get(c.Id).get(fd.QualifiedApiName)),Task_Id__c  =  c.Id);
                                    auditTrailList.add(auditTrail);
                                }
                            }
                    }
                }
            }
            
            if(relatedToList.size() > 0)
            {
                Set<Id> accSet = new Set<Id>(),assetSet = new Set<Id>(),csSet = new Set<Id>(),solSet = new Set<Id>(),conSet = new Set<Id>();
                
                for(EXT_CSM_CaseRelatedToObject obj : relatedToList){
                    if(CON_CSM.S_ACCOUNT.equals(obj.objRelName)){
                        accSet.add(obj.oldId);
                        accSet.add(obj.newId);
                    }else if(CON_CSM.S_ASSET.equals(obj.objRelName)){
                        assetSet.add(obj.oldId);
                        assetSet.add(obj.newId);
                    }else if(CON_CSM.S_CASE.equals(obj.objRelName)){
                        csSet.add(obj.oldId);
                        csSet.add(obj.newId);
                    }else if(CON_CSM.S_SOLUTION.equals(obj.objRelName)){
                        solSet.add(obj.oldId);
                        solSet.add(obj.newId);
                    }else if(CON_CSM.S_CONTACT.equals(obj.objRelName)){
                        conSet.add(obj.oldId);
                        conSet.add(obj.newId);
                    }
                    
                }
                Set<String> fieldSet  =  new Set<String> {CON_CSM.S_ID, CON_CSM.S_NAME};
                Map<Id, Account> accountMap  = null;
                Map<Id, Asset> assetMap  = null;
                Map<Id, Case> csMap = null;
                Map<Id, Solution> solMap = null;
                Map<Id, Contact> conMap = null;
                if(accSet.size() > 0) accountMap  =  new SLT_Account().selectByAccountId(accSet, fieldSet);
                if(assetSet.size() > 0) assetMap  =  new SLT_Asset().selectByAssetId(assetSet, fieldSet);
                if(csSet.size() > 0)  csMap  =  new SLT_Case().selectByCaseId(csSet, new Set<String>{CON_CSM.S_ID,CON_CSM.S_CASENUM});
                if(solSet.size() > 0)  solMap  =  new SLT_Solution().selectBySolutionId(solSet, fieldSet);
                if(conSet.size() > 0)  conMap  =  new SLT_Contact().selectByContactId(conSet, fieldSet);
                for(EXT_CSM_CaseRelatedToObject obj:relatedToList){
                    
                    if(accountMap != null && accountMap.size() > 0 && CON_CSM.S_ACCOUNT.equals(obj.objRelName)){
                        auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = obj.action,Name = obj.fieldName,Task_Id__c  =  obj.objectId,Old_Value__c = accountMap.containsKey(obj.oldId)?accountMap.get(obj.oldId).Name :'',New_Value__c = accountMap.containsKey(obj.newId)?accountMap.get(obj.newId).Name:'');
                        auditTrailList.add(auditTrail);
                    }else if(assetMap != null && assetMap.size() > 0 && CON_CSM.S_ASSET.equals(obj.objRelName)){
                        auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = obj.action,Name = obj.fieldName,Task_Id__c  =  obj.objectId,Old_Value__c = assetMap.containsKey(obj.oldId)?assetMap.get(obj.oldId).Name :'',New_Value__c = assetMap.containsKey(obj.newId)?assetMap.get(obj.newId).Name:'');
                        auditTrailList.add(auditTrail);
                    }else if(csMap != null && csMap.size() > 0 && CON_CSM.S_CASE.equals(obj.objRelName)){
                        auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = obj.action,Name = obj.fieldName,Task_Id__c  =  obj.objectId,Old_Value__c = csMap.containsKey(obj.oldId)?csMap.get(obj.oldId).CaseNumber :'',New_Value__c = csMap.containsKey(obj.newId)?csMap.get(obj.newId).CaseNumber:''); 
                        auditTrailList.add(auditTrail);
                    }else if(solMap != null && solMap.size() > 0 && CON_CSM.S_SOLUTION.equals(obj.objRelName)){
                        auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = obj.action,Name = obj.fieldName,Task_Id__c  =  obj.objectId,Old_Value__c = solMap.containsKey(obj.oldId)?solMap.get(obj.oldId).SolutionNumber :'',New_Value__c = solMap.containsKey(obj.newId)?solMap.get(obj.newId).SolutionNumber:''); 
                        auditTrailList.add(auditTrail);
                    }else if(conMap != null && conMap.size() > 0 && CON_CSM.S_CONTACT.equals(obj.objRelName)){
                        auditTrail = new CSM_QI_Data_Audit_Trail__c(Action__c = obj.action,Name = obj.fieldName,Task_Id__c  =  obj.objectId,Old_Value__c = conMap.containsKey(obj.oldId)?conMap.get(obj.oldId).Name :'',New_Value__c = conMap.containsKey(obj.newId)?conMap.get(obj.newId).Name:''); 
                        auditTrailList.add(auditTrail);
                    }
                    
                }
            }
            
            
            try {
                if(auditTrailList != null && auditTrailList.size() > 0){
                    insert auditTrailList;
                }
                
            } catch (DmlException e) {
                System.debug('Failed due to : '+e);
            }
        }
        
    }
    
    public static void updateStatusforLogACallonCase(List<Task> newList){
        Set<Id> caseIds = new Set<Id>();
        List<Case> updateCase = new List<Case>();
        Id userId = UserInfo.getUserId();
        List<Id> technoCaseId = new List<Id>();
        boolean InitialResonseProdOpps = false;
        List<Id> technoCaseIdProd = new List<Id>();
        List<Case> caseList = null;
        boolean isOutboundCall = false;
        boolean checkUpdate = false;
        boolean taskCheck = true;
        for(Task c : newList) {
            if(c.WhatId != null && CON_CSM.S_OUTBOUND_Call.equalsIgnoreCase(c.Type)  && 'Case'.equalsIgnoreCase(String.valueOf(c.WhatId.getSObjectType()))){
                caseIds.add(c.WhatId); 
                isOutboundCall = true;
            }else if(c.WhatId != null &&  'Case'.equalsIgnoreCase(String.valueOf(c.WhatId.getSObjectType()))){
                 if(CON_CSM.S_TASK_SUBJECT.equalsIgnoreCase(c.Subject)) { 
                     taskCheck = false;  
                 }
                caseIds.add(c.WhatId); 
            } 
        }
        
        
        if(!caseIds.isEmpty()){
            
            caseList = new SLT_Case().selectById(caseIds);
            for(Case cs : caseList){
                if(CON_CSM.S_TECHNOLOGY_R_T.equalsIgnoreCase(cs.RecordTypeName__c) && string.valueOf(cs.OwnerId).startsWith(CON_CSM.S_QUEUE_ID) && CON_CSM.S_NEW.equalsIgnoreCase(cs.Status)){
                    if(taskCheck) {
                        cs.Status = CON_CSM.S_IN_PROGRESS;
                    }
                    checkUpdate = true; 
                    
                }
                if(isOutboundCall && CON_CSM.S_TECHNOLOGY_R_T.equalsIgnoreCase(cs.RecordTypeName__c) && (CON_CSM.S_EMAIL.equalsIgnoreCase(cs.Origin) || CON_CSM.S_CUSTOMER_PORTAL.equalsIgnoreCase(cs.Origin))){
                    if(CON_CSM.S_NEW.equalsIgnoreCase(cs.Status)){
                        if(taskCheck) {
                            cs.Status = CON_CSM.S_IN_PROGRESS;
                        }
                        checkUpdate = true;    
                    }
                    
                    if (((cs.SlaStartDate <= System.now()) && (cs.SlaExitDate == null))){
                        if(cs.TaskMilestone__c != null && !cs.TaskMilestone__c.contains(CON_CSM.S_FIRST_RESPONSE_T)){
                            cs.TaskMilestone__c += ';' + CON_CSM.S_FIRST_RESPONSE_T;
                            technoCaseId.add(cs.Id);
                            checkUpdate = true;
                        }else if(cs.TaskMilestone__c == null){
                            cs.TaskMilestone__c = CON_CSM.S_FIRST_RESPONSE_T; 
                            technoCaseId.add(cs.Id);
                            checkUpdate = true;
                        }
                        
                    }
                }else if(isOutboundCall && CON_CSM.S_TECHNOLOGY_R_T.equalsIgnoreCase(cs.RecordTypeName__c)){
                    technoCaseIdProd.add(cs.Id);
                    InitialResonseProdOpps = true;
                    checkUpdate = true;
                }
                if(checkUpdate){
                    updateCase.add(cs);
                }
            }
            
            if(!updateCase.isEmpty()){
                try{
                    update updateCase;   
                }catch (DmlException e) {
                    System.debug('Failed due to : '+e);
                }
                
            }
            if(!technoCaseId.isEmpty()){
                DateTime completionDate = System.now();
                DAOH_Case.completeMilestone(technoCaseId, new List<String>{CON_CSM.S_FIRST_RESPONSE_T,CON_CSM.S_PRODOPS_INITIAL_RESPONSE}, completionDate);
            }else if(!technoCaseIdProd.isEmpty() && InitialResonseProdOpps){
                DateTime completionDate = System.now();
                DAOH_Case.completeMilestone(technoCaseIdProd, new List<String>{CON_CSM.S_FIRST_RESPONSE_T,CON_CSM.S_PRODOPS_INITIAL_RESPONSE}, completionDate);
            }
        }
        
    }
    
    /**
    * This method is used to set fields of task.
    * @params  newList List<Task>
    * @return  void
    */
    public static void setTaskFields(List<Task> newList, Map<Id, Task> oldMap) {
        Set<Id> userIdSet = new Set<Id>();
        Set<String> userFieldSet = new Set<String>{'Id','ManagerId','Manager.Email'};
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> leadIdSet = new Set<Id>();
        Map<Id, Contact> idToContactMap;
        Map<Id, Lead> idToLeadMap;
        Set<String> fieldSet = new Set<String>{'id','Phone','Email'};
        for(Task tsk : newList) {
            if((Trigger.isInsert || tsk.WhoId != oldMap.get(tsk.id).WhoId) && tsk.RecordTypeId == CON_CRM.MQL_TASK_RECORD_TYPE_ID) {
                String whoId = tsk.WhoId;
                if(!String.isBlank(WhoId) && WhoId.startsWith(CON_CRM.CONTACT_OBJECT_CODE)) {
                    contactIdSet.add(tsk.WhoId);
                }
                else if(!String.isBlank(WhoId) && whoId.startsWith(CON_CRM.LEAD_OBJECT_CODE)) {
                    leadIdSet.add(tsk.WhoId);
                }
            }
            if ( Trigger.isInsert && tsk.RecordTypeId == CON_CRM.MQL_TASK_RECORD_TYPE_ID ){
                userIdSet.add(tsk.OwnerId);
        }
        }
        Map<Id, User> idToUserMap = new SLT_User().selectByUserId(userIdSet, userFieldSet);
        if(contactIdSet != null && contactIdSet.size() > 0) {
            idToContactMap = new SLT_Contact().selectByContactId(contactIdSet, fieldSet);
        }
        if(leadIdSet != null && leadIdSet.size() > 0) {
            idToLeadMap = new SLT_Lead().selectByLeadId(leadIdSet, fieldSet);
        }
        for(Task tsk : newList) {
            if(idToContactMap != null && idToContactMap.size() > 0 && !String.isBlank(tsk.WhoId) && idToContactMap.containsKey(tsk.WhoId)) {
                tsk.Lead_Phone__c = idToContactMap.get(tsk.WhoId).Phone;
                tsk.Lead_Email__c = idToContactMap.get(tsk.WhoId).Email;
            }
            else if(idToLeadMap != null && idToLeadMap.size() > 0 && !String.isBlank(tsk.WhoId) && idToLeadMap.containsKey(tsk.WhoId)) {
                tsk.Lead_Email__c = idToLeadMap.get(tsk.WhoId).Email;
                tsk.Lead_Phone__c = idToLeadMap.get(tsk.WhoId).Phone;
            }
            if(idToUserMap.size() > 0 &&  idToUserMap.containsKey( tsk.OwnerId ) && idToUserMap.get(tsk.OwnerId).ManagerId != null && idToUserMap.get(tsk.OwnerId).Manager.Email != null){
                tsk.Email_Of_Manager__c = idToUserMap.get(tsk.OwnerId).Manager.Email;
            }
        }
    }
    
    /**
    * This method is used to change Lead Owner field .
    * @params  newList List<Task>
    * @params  oldMap Map<Id, Task>
    * @return  void
    */
    public static void changeLeadOwner(List<Task> newList, Map<Id, Task> oldMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Lead.SObjectType
            }
        );
        List<Lead> leadList = new List<Lead>();
        for(Task taskRecord : newList) {
            if(taskRecord.recordTypeId == CON_CRM.MQL_TASK_RECORD_TYPE_ID && taskRecord.WhoId != NULL && 'Lead'.equalsIgnoreCase(String.valueOf(taskRecord.WhoId.getSObjectType()))
               && taskRecord.OwnerId != oldMap.get(taskRecord.Id).OwnerId) {
                   leadList.add(new Lead(Id = taskRecord.WhoId, OwnerId = taskRecord.OwnerId));
               }
        }
        if(leadList.size() > 0) {
            uow.registerDirty(leadList);
            uow.commitWork();
        }
    }
    
    /**
    * This method is used to change Contact/Lead Disposition Details fields
    * @params  newList List<Task>
    * @params  oldMap Map<Id, Task>
    * @return  void
    */
    public static void changeDispositionDetailFields(List<Task> newList, Map<Id, Task> oldMap) {
        List<String> dispositionDetailFieldList = new List<String>{'Disposition__c', 'Not_Accept_Reason__c', 'Not_Accept_Reason_Detail__c', 
                                                                  'Nurture_Area__c', 'Nurture_Detail__c', 'Nurture_Detail_Other__c'};
        List<Sobject> leadOrContactList = new List<Sobject>();
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Lead.SObjectType,
                Contact.SObjectType
            }
        );
        Boolean toCommit = false;
        for(Task taskRecord : newList) {
            if(taskRecord.recordTypeId == CON_CRM.MQL_TASK_RECORD_TYPE_ID && taskRecord.WhoId != NULL) {
                if('Lead'.equalsIgnoreCase(String.valueOf(taskRecord.WhoId.getSObjectType())) || 'Contact'.equalsIgnoreCase(String.valueOf(taskRecord.WhoId.getSObjectType()))) {    
                    Sobject leadorContactRecord = taskRecord.WhoId.getSobjectType().newSobject(taskRecord.WhoId);
                    Boolean isChanged = false;
                    for(String field : dispositionDetailFieldList) {
                        if(taskRecord.get(field) != oldMap.get(taskRecord.Id).get(field)) {
                            leadorContactRecord.put(field, taskRecord.get(field));
                            isChanged = true;
                        }
                    }
                    if(isChanged) {
                        toCommit = true;
                        uow.registerDirty(leadorContactRecord);
                    }
                }
            }
        }
        if(toCommit) {
            uow.commitWork();
        }
    }
    
    public static void updateAgreementStatusAndProcessStep(List<Task> newList,Map<Id, Task> oldMap){
        Set<Id> agreementIds = new Set<Id>();

        for(Task taskObj : newList) {
            if(oldMap.get(taskObj.Id) != null
                && oldMap.get(taskObj.Id).Status != CON_CPQ.COMPLETED
                && taskObj.Status == CON_CPQ.COMPLETED
                && taskObj.WhatId != null
                && CON_CPQ.APTTUS_APTS_AGREEMENT.equalsIgnoreCase(String.valueOf(taskObj.WhatId.getSObjectType()))){
                agreementIds.add(taskObj.WhatId);
            }
        }

        Set<Id> recordTypeIds = CPQ_Utility.getInitialRecordTypes();

        Set<String> fieldSet = new Set<String>{CON_CPQ.Id, CON_CPQ.AGREEMENT_AGREEMENT_STATUS, CON_CPQ.AGREEMENT_PROCESS_STEP, CON_CPQ.RECORDTYPEID, CON_CPQ.OWNER};    	
        List<Apttus__APTS_Agreement__c> agreementList = new SLT_Agreement().getAgreementDetailsByIdsAndRecorType(agreementIds, recordTypeIds, fieldSet);

        List<Apttus__APTS_Agreement__c> updateAgreementList = new List<Apttus__APTS_Agreement__c>();
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<EmailTemplate> emailTemplate = new SLT_EmailTemplate().selectEmailDTemplateByDeveloperName(
            CON_CPQ.CPQ_TASK_COMPLETION_NOTIFICATION_TO_PD, new Set<String>{CON_CPQ.Id});
        if(agreementList.size() > 0){
            for(Apttus__APTS_Agreement__c agreement : agreementList){
                if(agreement.Agreement_Status__c == CON_CPQ.DRAFT
                    && agreement.Process_Step__c == CON_CPQ.LINE_MANAGER_QC
                    && emailTemplate.size() > 0 && agreement.OwnerId != null){

                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateID(emailTemplate[0].Id);
                    mail.setSaveAsActivity(false);
                    mail.setTargetObjectId(agreement.OwnerId);
                    mail.setWhatId(agreement.Id);
                    mailList.add(mail);

                    agreement.Process_Step__c = CON_CPQ.KEY_STAKEHOLDER_REVIEW_AND_CHALLENGE_CALL;
                    agreement.Agreement_Status__c = CON_CPQ.INTERNAL_REVIEW;
                    updateAgreementList.add(agreement);
                }
            }
        }

        if(updateAgreementList.size()>0){
            update updateAgreementList;
        }
        if(mailList.size() > 0)
            Messaging.sendEmail(mailList);
    }
}
