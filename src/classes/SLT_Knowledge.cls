/*
 * Version       : 1.0
 * Description   : This Apex class is selector layer for Knowledge__kav
 */
 
public class SLT_Knowledge extends fflib_SObjectSelector {
    /**
     * This method used to get field list of sobject
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
            Knowledge__kav.Id,
            Knowledge__kav.Case_CategorizationId__c,
            Knowledge__kav.ProductName__c,
            Knowledge__kav.SubType1__c,
            Knowledge__kav.SubType2__c,
            Knowledge__kav.SubType3__c,
            Knowledge__kav.PublishStatus
        };
    }
    
     /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Knowledge__kav.sObjectType;
    }
    
    /**
     * This method used to get Knowledge by Id
     * @return  List<Knowledge__kav>
     */
    public List<Knowledge__kav> selectById(Set<ID> idSet) {
        return (List<Knowledge__kav>) selectSObjectsById(idSet);
    }
    
    /**
     * This method used to get Knowledge by topicId where article is visible on CHS and Pulished;
     * @return  List<Knowledge__kav>
     */
    public List<Knowledge__kav> selectByTopicId(String topicId) {
        return (List<Knowledge__kav>) [select Id, KnowledgeArticleId, Topic_Chapter__c, PublishStatus, VersionNumber, IsLatestVersion, IsVisibleInApp, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInPrm, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, Language, Title, UrlName, CurrencyIsoCode, ArchivedDate, ArchivedById, Summary, ArticleNumber, FirstPublishedDate, LastPublishedDate, ArticleCreatedDate, SourceId, RecordTypeId, Answer__c, Content__c, L1Answer__c, L1Content__c, Question__c, ReviewDate__c, Test__c, Article_Link__c, SubType3__c, ProductName__c, SubType1__c, SubType2__c  from Knowledge__kav where id in (select EntityId from TopicAssignment where TopicId =: topicId and EntityType ='Knowledge') and PublishStatus ='Online' and IsVisibleInCsp = true  order By Topic_Chapter__c , LastPublishedDate desc ];
    }
    
    /**
     * This method used to get Knowledge by topicId;
     * @return  List<Knowledge__kav>
     */
    public List<Knowledge__kav> selectByTopicId2(String topicId) {
        return (List<Knowledge__kav>) [select Id, KnowledgeArticleId, Topic_Chapter__c, PublishStatus, VersionNumber, IsLatestVersion, IsVisibleInApp, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInPrm, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, Language, Title, UrlName, CurrencyIsoCode, ArchivedDate, ArchivedById, Summary, ArticleNumber, FirstPublishedDate, LastPublishedDate, ArticleCreatedDate, SourceId, RecordTypeId, Answer__c, Content__c, L1Answer__c, L1Content__c, Question__c, ReviewDate__c, Test__c, Article_Link__c, SubType3__c, ProductName__c, SubType1__c, SubType2__c  from Knowledge__kav where id in (select EntityId from TopicAssignment where TopicId =: topicId and EntityType ='Knowledge') and PublishStatus ='Online' order By Topic_Chapter__c , LastPublishedDate desc ];
    }
    
    /**
     * This method used to get Knowledge by productName;
     * @return  List<Knowledge__kav>
     */
    public List<Knowledge__kav> selectByProductName(String productName) {
        return (List<Knowledge__kav>) [select Id, KnowledgeArticleId, Topic_Chapter__c, PublishStatus, VersionNumber, IsLatestVersion, IsVisibleInApp, IsVisibleInPkb, IsVisibleInCsp, IsVisibleInPrm, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, Language, Title, UrlName, CurrencyIsoCode, ArchivedDate, ArchivedById, Summary, ArticleNumber, FirstPublishedDate, LastPublishedDate, ArticleCreatedDate, SourceId, RecordTypeId, Answer__c, Content__c, L1Answer__c, L1Content__c, Question__c, ReviewDate__c, Test__c, Article_Link__c, SubType3__c, ProductName__c, SubType1__c, SubType2__c  from Knowledge__kav where ProductName__c =: productName and PublishStatus ='Online' order By Topic_Chapter__c , LastPublishedDate desc ];
    }
    

}