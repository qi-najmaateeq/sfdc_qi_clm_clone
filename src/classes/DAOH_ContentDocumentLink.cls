public with sharing class DAOH_ContentDocumentLink {

    public static void defineSharingRuleCTRPRM(List<ContentDocumentLink> newList){
        Set<Id> parentIdSet = new Set<Id>();
        Set<String> fieldSet = new Set<String>{'Id','RecordTypeId'};
        Map<Id, Contract> contractMap;
        Id prmCtrRecTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByDeveloperName().get(CON_PEP.S_PEP_CTR_RECORDTYPE).getRecordTypeId();
        
        for(ContentDocumentLink cntDocLnk : newList){
            if(cntDocLnk.LinkedEntityId != null){
                parentIdSet.add(cntDocLnk.LinkedEntityId);
            }
        }
        
        if(parentIdSet.size() > 0){
            contractMap = new Map<Id, Contract>(new SLT_Contract().selectByContractIdList(parentIdSet,fieldSet));
        }
        
        if(contractMap !=null && contractMap.size() > 0){
            for(ContentDocumentLink cntDocLnk : newList){
                if(cntDocLnk.LinkedEntityId != null
                   && contractMap.get(cntDocLnk.LinkedEntityId) !=null 
                   && contractMap.get(cntDocLnk.LinkedEntityId).RecordTypeId == prmCtrRecTypeId){
                       cntDocLnk.Visibility = 'AllUsers';//sharing with all internal and external users
                       cntDocLnk.ShareType = 'I';//Inferred permission : based on related permission on contract
                   }
            }
        }
    }

    public static void sendEmailNotificationOnNewAttachment(List<ContentDocumentLink> documentList){
        if(documentList != null){
            Map<Id, Id> contentDocumentIdCaseMap = new Map<Id, Id>();
            List<ContentDocument> contentDocumentList = new List<ContentDocument>();
            Set<Id> caseIdSet = new Set<Id>();
            List<Case> caseList = new List<Case>();
            Map<Id, Case> caseIdRecordMap = new Map<Id, Case>();
            Map<Id, Case> documentIdCaseMap = new Map<Id, Case>();
            List<User> userList = new List<User>();
            List<Case> caseToUpdate = new List<Case>();
            
            for(ContentDocumentLink documentRecord : documentList){
                if(documentRecord.LinkedEntityId != null && documentRecord.ContentDocumentId != null){
                    String parentId = documentRecord.LinkedEntityId;
                    String parentIdString = String.valueOf(parentId).substring(0,3);
                    if(parentIdString.equalsIgnoreCase('500') && documentRecord.Visibility.EqualsIgnoreCase('AllUsers')){
                        contentDocumentIdCaseMap.put(documentRecord.ContentDocumentId, documentRecord.LinkedEntityId);
                        caseIdSet.add(documentRecord.LinkedEntityId);
                    }
                }
            }

            if((contentDocumentIdCaseMap != null && !contentDocumentIdCaseMap.isEmpty()) && (caseIdSet != null && !caseIdSet.isEmpty())){
                caseList = new SLT_Case().selectById(caseIdSet);
                for(Case caseRecord : caseList){
                    caseIdRecordMap.put(caseRecord.Id, caseRecord);
                }
                contentDocumentList = new SLT_ContentDocument().selectById(contentDocumentIdCaseMap.keySet());
                for(ContentDocument contentRecord : contentDocumentList){
                    if(contentRecord.CreatedById != null && contentDocument.Id != null){
                        documentIdCaseMap.put(contentRecord.CreatedById, caseIdRecordMap.get(contentDocumentIdCaseMap.get(contentRecord.Id)));
                    }
                }
                if(documentIdCaseMap != null && !documentIdCaseMap.isEmpty()){
                    userList = new SLT_User().selectById(documentIdCaseMap.keySet());
                    for(User userRecord : userList){
                        if(userRecord.ProfileId == CON_CSM.S_COMMUNITY_PLUS_USER_PROFILEID){
                            documentIdCaseMap.get(userRecord.Id).CSH_Attachment__c = TRUE;
                            documentIdCaseMap.get(userRecord.Id).New_Attachment__c = TRUE;
                            caseToUpdate.add(documentIdCaseMap.get(userRecord.Id));
                        }
                        else if(userRecord.ProfileId == CON_CSM.S_SERVICE_USER_PROFILEID){
                            documentIdCaseMap.get(userRecord.Id).CSH_Attachment__c = FALSE;
                            documentIdCaseMap.get(userRecord.Id).New_Attachment__c = TRUE;
                            caseToUpdate.add(documentIdCaseMap.get(userRecord.Id));
                        }
                    }
                }
            }
            
            if(caseToUpdate != null && !caseToUpdate.isEmpty()){
                update caseToUpdate;
            }
        }
    }
}
