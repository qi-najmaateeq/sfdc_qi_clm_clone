global class BCH_CSM_ChatterGroupInsert implements Database.Batchable<sObject> {
    global String query ;
    global string groupName = CON_CSM.S_ChatterGroup;
    global set<id> idSet = new set<id>();
    public  BCH_CSM_ChatterGroupInsert() {
        
        List<CollaborationGroupMember> collGroupMembersList =  [Select Member.id from CollaborationGroupMember where CollaborationGroup.Name =:groupName];
        for(CollaborationGroupMember groupMember :collGroupMembersList){
            idSet.add(groupMember.Member.id);
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
       set<string> permissionsetName = new set<string>();
       permissionsetName.add(CON_CSM.S_CSM_SmartSolve);
       permissionsetName.add(CON_CSM.S_TECHNO_Case_Record_Type_Contact_Fields_Access);
       permissionsetName.add(CON_CSM.S_DATA_Case_Record_Type_Non_US_Service_Prod_Fields_Access);
       permissionsetName.add(CON_CSM.S_R_D_Case_Record_Type_Study_Activity_Objects_Access);
       permissionsetName.add(CON_CSM.S_CSM_SmartSolve_Cloud);
       permissionsetName.add(CON_CSM.S_CSM_HCP_Onekey_Case_Record_Type_Fields_access);
      return Database.getQueryLocator(
       [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name in :permissionsetName and Assignee.IsActive =true and AssigneeId not in :idSet LIMIT :Test.isRunningTest()?1:50000000]
    );
    }
    
    global void execute(Database.BatchableContext BC, List<PermissionSetAssignment> permissionsetList) {
        if(Test.isRunningTest()) {
            groupName = 'Test Group1';
        }
        List<CollaborationGroupMember> collGroupMembers = new List<CollaborationGroupMember>();
        List<CollaborationGroup> qaGroup = [SELECT id,Owner.Name,OwnerId FROM CollaborationGroup WHERE Name=:groupName limit 1];
        for(PermissionSetAssignment pSetAssign : permissionsetList) {  
            if(qaGroup != null && qaGroup.size() > 0 ){
                CollaborationGroupMember member = new CollaborationGroupMember();
                member.MemberId = pSetAssign.AssigneeId ;
                member.CollaborationGroupId = qaGroup[0].Id ;
                collGroupMembers.add(member);
            }
            
        }
        try {
            insert collGroupMembers;
            
        } 
        catch(Exception e) {
            
        }
        
    }   
    
    global void finish(Database.BatchableContext BC) {
        
         if(!Test.isRunningTest()) {
              Database.executebatch(new BCH_CSM_ChatterGroupInsert(),1);
        }
    }
}