/**
* This test class is used to test all methods in Agreement Domain class.
* version : 1.0
*/
@isTest
private class TST_CNT_CPQ_CalculateEscalationAmort {    
    /**
    * This test method is used for updating Agreement record
    */
    @isTest static void testCalculateEscalationAmortization() {
    	Account newAccount = UTL_TestData.createAccount();
    	insert newAccount;
		
        Opportunity testOpportunity= UTL_TestData.createOpportunity(newAccount.Id);
        testOpportunity.Line_of_Business__c = CON_CPQ.BIOSTATISTICAL_MEDICALWRITING;
        insert testOpportunity;

    	Id rtId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get('Subscription_Products').getRecordTypeId();

    	Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c(Apttus__Account__c = newAccount.Id,
    																   O_Term_Years__c = '2',
    																   O_Ammortize_Y_N__c = 'Yes',
												                       O_Annual_Price_Escalation_Factor__c = 10.0,
												                       O_Money_Factor__c = 20.0,
												                       RecordTypeId = rtId,
                                                                       Apttus__Related_Opportunity__c = testOpportunity.Id);
    	insert agmt;

    	Apttus_Config2__ProductConfiguration__c pc = new Apttus_Config2__ProductConfiguration__c(
    		Apttus_CMConfig__AgreementId__c = agmt.Id, Apttus_Config2__Status__c = 'Finalized');
    	insert pc;

 		List<Product2> products = new List<Product2>();
 		products.add(new Product2(Name = 'Test Product 1', ProductCode = 'TstProd1'));
 		products.add(new Product2(Name = 'Test Product 2', ProductCode = 'TstProd2'));
 		insert products;

 		List<Apttus_Config2__LineItem__c> items = new List<Apttus_Config2__LineItem__c>();
 		items.add(new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = pc.Id, 
 												  Apttus_Config2__ProductId__c = products[0].Id,
 												  Apttus_Config2__ChargeType__c = 'Subscription Fee',
 												  Apttus_Config2__ExtendedPrice__c = 100.0,
 												  Apttus_Config2__LineNumber__c = 1,
 												  Apttus_Config2__ItemSequence__c = 1));
 		items.add(new Apttus_Config2__LineItem__c(Apttus_Config2__ConfigurationId__c = pc.Id, 
 												  Apttus_Config2__ProductId__c = products[1].Id,
 												  Apttus_Config2__ChargeType__c = 'Implementation Fee',
 												  Apttus_Config2__ExtendedPrice__c = 10.0,
 												  Apttus_Config2__LineNumber__c = 2,
 												  Apttus_Config2__ItemSequence__c = 1));
 		insert items;

    	ApexPages.currentPage().getParameters().put('id', agmt.Id);

        Test.startTest();
        	// Test calculations
        	CNT_CPQ_CalculateEscalationAmortization controller = new CNT_CPQ_CalculateEscalationAmortization();
        	controller.calculateEscalationAmortization();
        	List<Agreement_Annual_Calculation__c> aacList =
        		[Select Id From Agreement_Annual_Calculation__c Where Agreement__c = :agmt.Id];
        	System.assertEquals(2, aacList.size());
        	agmt = [Select Id, O_Adjusted_Total_Revenue__c, O_Adjusted_Base_Revenue__c,
        				   O_Adjusted_Total_Implementation__c, O_Adjusted_Base_Implementation__c,
        				   O_Amortization_Average__c, O_Escalation_Average__c
        			From Apttus__APTS_Agreement__c
        			Where Id = :agmt.Id];
        	System.assertEquals(105, agmt.O_Adjusted_Total_Revenue__c);
        	System.assertEquals(52.5, agmt.O_Adjusted_Base_Revenue__c);
        	System.assertEquals(5, agmt.O_Escalation_Average__c);
        	System.assertEquals(11, agmt.O_Adjusted_Total_Implementation__c);
        	System.assertEquals(5.5, agmt.O_Adjusted_Base_Implementation__c);
        	System.assertEquals(10, agmt.O_Amortization_Average__c);

        	// Test deleting calculation rows and creating new ones
        	agmt.O_Money_Factor__c = null;
        	update agmt;
        	controller.calculateEscalationAmortization();

        	aacList =
        		[Select Id From Agreement_Annual_Calculation__c Where Agreement__c = :agmt.Id];
        	System.assertEquals(2, aacList.size());
        	agmt = [Select Id, O_Adjusted_Total_Revenue__c, O_Adjusted_Base_Revenue__c,
        				   O_Adjusted_Total_Implementation__c, O_Adjusted_Base_Implementation__c,
        				   O_Amortization_Average__c, O_Escalation_Average__c
        			From Apttus__APTS_Agreement__c
        			Where Id = :agmt.Id];
        	System.assertEquals(105, agmt.O_Adjusted_Total_Revenue__c);
        	System.assertEquals(52.5, agmt.O_Adjusted_Base_Revenue__c);
        	System.assertEquals(5, agmt.O_Escalation_Average__c);
        	System.assertEquals(10, agmt.O_Adjusted_Total_Implementation__c);
        	System.assertEquals(5, agmt.O_Adjusted_Base_Implementation__c);
        	System.assertEquals(0, agmt.O_Amortization_Average__c);
        Test.stopTest();
    }
}