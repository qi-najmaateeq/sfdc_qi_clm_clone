/*
 * Version       : 1.0
 * Description   : Apex Controller for PortalFile component.
 */
public with sharing class CNT_CSM_PortalFile {
     /*
     * Return List of Account for current user
     */
    @AuraEnabled
    public static List<Account> getUserAccount(){
        List<User> users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        List<Account> accounts = new SLT_Account().selectById(new Set<Id> {users[0].AccountId});
        return accounts;        
    }
	
	 /*
     * Return List of ContentFolder for current user
     */
    @AuraEnabled
    public static List<ContentFolder> getFolders(){
    	List<User> users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
    	List<Product2> products = new SLT_Product2().selectByAccountId(new Set<Id> {users[0].AccountId});
    	Set<String> folderName = new Set<String>();
		for (Integer i=0; i<products.size();i++)
			folderName.add(products[i].Community_Topics__c);
    	return new SLT_ContentFolder().selectByName(folderName);
    }
    
     /*
     * Return List of ContentFolder by topic id
     * @id 
     */
    @AuraEnabled
    public static List<ContentFolder> getFolderByTopicId(String id){
    	List<Topic> topics = new SLT_Topic().selectById(new Set<id> {id});
    	return new SLT_ContentFolder().selectByName(new Set<String> {topics[0].Name});
    }
    
    /*
     * Return List of ContentFolder for current user
     */
    @AuraEnabled
    public static List<ContentFolder> getSharedFolderByUserParentAccount(){
        List<User> users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        List<Account> accounts = new SLT_Account().selectById(new Set<Id> {users[0].AccountId});
        String folderName;
        if (accounts[0].Shared_Folder_Type__c != 'Local Account')folderName=new SLT_Account().selectById(new Set<Id> {accounts[0].ParentId})[0].Name;
        else  folderName = accounts[0].Name;
        return new SLT_ContentFolder().selectByName(new Set<String> {folderName});
    }
    
    /*
     * Return List of ContentFolder by parentContentFolderId
     * @parentContentFolderId 
     */
    @AuraEnabled
    public static List<ContentFolder> getFoldersByParentId(String parentContentFolderId){
    	return new SLT_ContentFolder().selectByParentContentFolderId(new Set<id> {parentContentFolderId});
    }
    

    @AuraEnabled
    public static void updateContentFolderMember (String parentContentFolderId, String childRecordId){
       List<ContentFolderMember> cfm = new SLT_ContentFolderMember().selectByChildRecordId(new Set<id> {childRecordId});
       cfm[0].ParentContentFolderId = parentContentFolderId;
       update cfm;
    }
    
    /*
     * Return List of CSM_QI_Folder__c by contactId
     */
    @AuraEnabled
    public static List<CSM_QI_Folder__c> getMikadoFoldersByContact(){
        List<CSM_QI_Folder__c> mikadoFolders =  new List<CSM_QI_Folder__c> ();
        List<User> users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        mikadoFolders =  [select Id, Name  from CSM_QI_Folder__c  where id in( select CSM_Folder__c  from CSM_QI_Folder_Contact_Relationship__c where Contact__c =: users[0].ContactId)];
        return mikadoFolders;
    }
    
    
    @AuraEnabled
    public static List<ContentFolderMember> getMikadoContentFolderMember(){
        List<CSM_QI_Folder__c> mikadoFolders =  new List<CSM_QI_Folder__c> ();
        List<User> users = new SLT_User().selectById(new Set<Id> {userInfo.getUserId()});
        mikadoFolders =  [select Id, Name  from CSM_QI_Folder__c  where id in( select CSM_Folder__c  from CSM_QI_Folder_Contact_Relationship__c where Contact__c =: users[0].ContactId)];
        Set<String> mikadoFoldersIds = new Set<String>();
        for (Integer i=0; i<mikadoFolders.size();i++)
            mikadoFoldersIds.add(mikadoFolders[i].Id);
            
        List<CSM_QI_Folder_File_Relationship__c> ffrList = [ select Id, CSM_Folder__c, File_Id__c from CSM_QI_Folder_File_Relationship__c where CSM_Folder__c in :mikadoFoldersIds ];
        Set<String> fileIds = new Set<String>();
        for (Integer i=0; i<ffrList.size();i++)
            fileIds.add(ffrList[i].File_Id__c);
        return new SLT_ContentFolderMember().selectByChildRecordId(fileIds);
    }
    
    @AuraEnabled
    public static void insertNewFolder (String name,String  parentContentFolderId){
        ContentFolder cf = new ContentFolder(Name=name, ParentContentFolderId=parentContentFolderId);
        insert cf;
    }
    
     /*
     * Return List of ContentFolderMember by parentContentFolderId
     * @parentContentFolderId 
     */
    @AuraEnabled
    public static List<ContentFolderMember> getFolderMemberByParentId(String parentContentFolderId){
    	return new SLT_ContentFolderMember().selectByParentContentFolderId(new Set<id> {parentContentFolderId});
    }
    
    @AuraEnabled
    public static void deleteContentDocumentById(String contentDocumentId){
        ContentDocument cd = New ContentDocument (Id=contentDocumentId);
        delete cd;
    }
}