public class SLT_Contact  extends fflib_SObjectSelector {
    
    /**
     * constructor to initialise CRUD and FLS
     */
    public SLT_Contact() {
        super(false, true, false);
    }
    
    public SLT_Contact(Boolean enforceFLS,Boolean enforceCRUD) {
        super(false, enforceFLS, enforceCRUD);
    }
    
    /**
     * This method used to get field list of sobject
     * @return  List<Schema.SObjectField>
     */
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>();
    }
    
    /**
     * This method used to set up type of sobject
     * @return  Schema.SObjectType
     */
    public Schema.SObjectType getSObjectType() {
        return Contact.sObjectType;
    }
    
    /**
     * This method used to get Contact by Id
     * @return  Map<Id, Contact>
     */
    public Map<Id, Contact> selectByContactId(Set<ID> idSet, Set<String> fieldSet) {
        return new Map<Id, Contact>((List<Contact>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    public List<Contact> selectByContactIdList(Set<ID> idSet, Set<String> fieldSet) {
        return ((List<Contact>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet').toSOQL()));
    }
    
    public Contact selectByEmail(String email) {
        return Database.query('select Id,Email,AccountId from Contact Where Email = \''+ email +'\' LIMIT 1');
    }
    
    public Map<Id, Contact> selectByMobile(String phone) {
        return new Map<Id, Contact>((List<Contact>) Database.query('select Id,Salutation,Name,MailingCity,Phone,MobilePhone,Email,Title,AccountId,Account.Name,toLabel(Account.AccountCountry__c),PreferredLanguage__c from Contact Where (Phone like \'%'+ phone +'%\' or MobilePhone like \'%'+ phone +'%\' or AssistantPhone like \'%'+ phone +'%\' or Company_Local_Phone__c like \'%'+ phone +'%\' or HomePhone like \'%'+ phone +'%\' or OtherPhone like \'%'+ phone +'%\')'));
    }
    
    public List<Contact> selectBySalesforceUserIdList(Set<ID> idSet, Set<String> fieldSet) {
        return ((List<Contact>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Salesforce_User__c in :idSet').toSOQL()));
    }
    
    public Contact selectByEmailId(String email) {
        return Database.query('select Id,Email,AccountId from Contact Where Email =:email LIMIT 1');
    }
    
    public List<Contact> selectByEmailIdList(Set<String> emailIds) {
        return Database.query('select Id,Email,AccountId from Contact Where Email in :emailIds LIMIT 1');
    }
    
    public List<Contact> selectByPseSalesforceUserIdList(Set<ID> idSet, Set<String> fieldSet) {
        return ((List<Contact>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('pse__Salesforce_User__c in :idSet').toSOQL()));
    }

    public List<Contact> getContactEmails(Set<Id> contactIds){
        return Database.query('SELECT Id, Email, AccountId, Account.Name FROM Contact WHERE Id IN :contactIds');
    }
        
    public List<Contact> selectContactByLiContactId(Set<ID> idSet, Set<String> fieldSet) {
        return ((List<Contact>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('LI_Contact_Id__c in :idSet').toSOQL()));
    }
    
    public List<Contact> getContactByUserEmployeeNumber(String employeeNumber) {
        return [SELECT Id FROM Contact WHERE Salesforce_User__r.EmployeeNumber =: employeeNumber];
    }
    
    /**
     * This method used to get Contact by given condition
     * @return  Map<Id, Contact>
     */
    public Map<Id, Contact> selectByContactCondition(Set<ID> idSet, String contCondition, Set<String> fieldSet) {
        return new Map<Id, Contact>((List<Contact>) Database.query(newQueryFactory(true).selectFields(fieldSet).setCondition('Id in :idSet and '+contCondition).toSOQL()));
    }
}