/**
 * This is Agreement trigger handler class.
 * version : 1.0 
 */
public class DAO_APTS_Agreement extends fflib_SObjectDomain{
    
    /**
     * Constructor of this class
     * @params sObjectList List<Apttus__APTS_Agreement__c>
     */
    public DAO_APTS_Agreement(List<Apttus__APTS_Agreement__c> sObjectList) {
        super(sObjectList);
    }

    /**
     * Constructor Class for construct new Instance of This Class
     */
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new DAO_APTS_Agreement(sObjectList);
        }
    }
    
    /**
     * This method is used for before insert of the Agreement trigger.
     * @return void
     */
    public override void onBeforeInsert() {
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        if(Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_OWF_Agreement_Flow__c) {
            //FEATURE-2947
            //DAOH_OWF_APTS_Agreement.populateReBidFields((List<Apttus__APTS_Agreement__c>)records);
            //Added by sanjeev sharma 
            //DAOH_OWF_APTS_Agreement.checkAgreementRecordTypeMapping((List<Apttus__APTS_Agreement__c>)records);
            //added by sanjeev sharma 
            DAOH_OWF_APTS_Agreement.setBidNoBasedOnRecordType((List<Apttus__APTS_Agreement__c>)records);
            DAOH_OWF_APTS_Agreement.setNoOfRequestedServicesBasedOnRequestedServices((List<Apttus__APTS_Agreement__c>)Records, null);
            DAOH_OWF_APTS_Agreement.populateAgrNameWithOppName((List<Apttus__APTS_Agreement__c>)records);
            DAOH_OWF_APTS_Agreement.setMulesoftSyncfields((List<Apttus__APTS_Agreement__c>)records,null);
        }        
    }
    
    /**
     * This method is used for after insert of the Agreement trigger.
     * @return void
     */
    public override void onAfterInsert() {
        if(Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_Project_Trigger__c) {
            DAOH_OWF_APTS_Agreement.createProjectBasedOnAgreement((List<Apttus__APTS_Agreement__c>)records);
        }
    }
    
    /**
     * This method is used for before update of the Agreement trigger.
     * @params  existingRecords Map<Id,SObject>
     * @return  void
     */
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        
        if(Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_OWF_Agreement_Flow__c) {
            
            DAOH_OWF_APTS_Agreement.setNoOfRequestedServicesBasedOnRequestedServices((List<Apttus__APTS_Agreement__c>)Records, (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
            DAOH_OWF_APTS_Agreement.setMulesoftSyncfields((List<Apttus__APTS_Agreement__c>)records,(Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
            
        }
    }
    
    /**
     * This method is used for after update of the Agreement trigger.
     * @return void
     */
    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {
        //This is the section where all the methods that needs to be run in a normal sequence are included.
        if(Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_RR_Trigger__c) {
           
            DAOH_OWF_APTS_Agreement.createClinicalBidResRequestsOnAgreementUpdate((List<Apttus__APTS_Agreement__c>)Records, (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
            DAOH_OWF_APTS_Agreement.setEndDateAndStatusOnProjectResReqsAndAssignments((List<Apttus__APTS_Agreement__c>)Records, (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
            DAOH_OWF_APTS_Agreement.updateComplexityScoreTotalOnRR((List<Apttus__APTS_Agreement__c>)Records, (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
            DAOH_OWF_APTS_Agreement.updateDataBasedOnOasSetting((List<Apttus__APTS_Agreement__c>)Records, (Map<Id, Apttus__APTS_Agreement__c>)existingRecords);
            
        }
    }
    
    /**
     * This method is used for before Delete of the Agreement trigger.
     * @return void
     */
    public override void onBeforeDelete() {
        if(Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_OWF_Agreement_Flow__c) {
            
                DAOH_OWF_APTS_Agreement.deleteProjectBasedOnAgreement((List<Apttus__APTS_Agreement__c>)records);             
        }
    }
    public override void onAfterDelete() {
         DAOH_OWF_APTS_Agreement.createOutboundMessageDeletionQueue((List<Apttus__APTS_Agreement__c>)Records);
    }
}