/**
 * This test class is used to test all methods in Resource Request service class
 * version : 1.0
 */
@isTest
private class TST_SLT_Resource_Request {

    /**
     * This method is used to setup data for all methods.
     */
    @testSetup
    static void dataSetup() {
        Account acc = UTL_OWF_TestData.createAccount();
        insert acc;
        
        pse__Grp__c grp = UTL_OWF_TestData.createGroup();
        insert grp;
        
        pse__Permission_Control__c permissionControlGroup = UTL_OWF_TestData.createPermissionControl(null, null, grp, null);
        insert permissionControlGroup;
        
        Contact cont = UTL_OWF_TestData.createContact(acc.Id);
        cont.pse__Is_Resource__c = true;
        cont.pse__Is_Resource_Active__c = true;
        cont.pse__Group__c = grp.Id;
        insert cont;
        
        
        
        Opportunity opp = UTL_OWF_TestData.createOpportunity(acc.Id);
        insert opp;
        
        Apttus__APTS_Agreement__c agreement = UTL_OWF_TestData.createAgreementByRecordType(acc.Id, opp.Id, CON_OWF.OWF_CLINICAL_BID_AGREEMENT_RECORD_TYPE_ID);
        insert agreement;
    }
    
    /**
     * This test method used to cover basic methods
     */ 
    static testMethod void testServiceResourceRequest() {   
        List<Schema.SObjectField> resourceRequestList = new SLT_Resource_Request().getSObjectFieldList();
        Schema.SObjectType resourceRequest = new SLT_Resource_Request(true).getSObjectType();
    }    
    
    /**
     * This test method used to get Resource Request By Project ID
     */ 
    static testMethod void testGetResourceRequestByProjectID() {   
        pse__Proj__c project = [Select Id From pse__Proj__c limit 1];
        Set<ID> projectIdset = new Set<Id>{project.Id};
        Set<String> resourceRequestFieldSet = new Set<String>{'Id'};
            
        Test.startTest();
            Map<Id, pse__Resource_Request__c> rrIdToResourceRequestMap = new SLT_Resource_Request().getResourceRequestByProjectID(projectIdset, resourceRequestFieldSet);
        Test.stopTest();
    
        Integer expected = 1;
        System.assertEquals(expected, rrIdToResourceRequestMap.size());
    }
    
    /**
     * This test method used to test selectByIdWithResReqsAndRSRs method
     */ 
    static testMethod void testSelectByIdWithResReqsAndRSRs() {   
        pse__Proj__c project = [Select Id From pse__Proj__c limit 1];
        Set<ID> projectIdset = new Set<Id>{project.Id};
        Set<String> resourceRequestFieldSet = new Set<String>{'Id'};
        String resReqCondition = 'RecordType.DeveloperName = \'' + CON_OWF.OWF_RESOURCE_REQUEST_RECORD_TYPE_NAME + '\'';
        Set<String> resRequestFieldSet = new Set<String>{'Id', 'pse__Assignment__c', 'Therapy_Area__c', 'Indication__c'};
        Set<String> resSkillRequestFieldSet = new Set<String>{'Id', 'pse__Is_Primary__c', 'pse__Resource_Request__c', 'pse__Skill_Certification__c', 'pse__Skill_or_Certification__c', 'pse__Skill_Record_Type__c'};    
        String resSkillReqCondition = '(pse__Skill_Certification__r.pse__Type__c = \'' + CON_OWF.SKILL_TYPE_INDICATION 
                    + '\' OR pse__Skill_Certification__r.pse__Type__c = \''  + CON_OWF.SKILL_TYPE_THERAPY_AREA + '\')';
        Test.startTest();
            Map<Id, pse__Resource_Request__c> rrIdToRresourceRequestWithSkillMap = new SLT_Resource_Request(false).selectByIdWithResReqsAndRSRs(null, resReqCondition, resRequestFieldSet, resSkillRequestFieldSet, resSkillReqCondition);
        Test.stopTest();
    
        Integer expected = 1;
        System.assertEquals(expected, rrIdToRresourceRequestWithSkillMap.size());
    }
    
    /**
     * This test method used to test getResourceRequestByAgrID method
     */ 
    static testMethod void testGetResourceRequestByAgrID() {   
        Set<String> resourceRequestFieldSet = new Set<String>{'Id'};
        Apttus__APTS_Agreement__c agreement = [Select id from Apttus__APTS_Agreement__c limit 1];
        Set<Id> aggrementIdSet = new Set<Id>();
        aggrementIdSet.add(agreement.Id);
        Test.startTest();
            Map<Id, pse__Resource_Request__c> resourceRequestByAgrID = new SLT_Resource_Request(false).getResourceRequestByAgrID(aggrementIdSet,  resourceRequestFieldSet);
        Test.stopTest();
    
        Integer expected = 1;
        System.assertEquals(expected, resourceRequestByAgrID.size());
    }
    
    /**
     * This test method used to test selectResReqsById method
     */ 
    static testMethod void testSelectResReqsById() {   
        Set<String> resourceRequestFieldSet = new Set<String>{'Id'};
        Apttus__APTS_Agreement__c agreement = [SELECT Id, Name FROM Apttus__APTS_Agreement__c][0];
        Contact cont = [Select Id From Contact Where Name = 'TestContact'];
        Opportunity oppty = [SELECT Id FROM Opportunity WHERE name = 'TestOpportunity'];
        pse__Proj__c project = [Select Id From pse__Proj__c Where Agreement__c =: agreement.Id];
        pse__Grp__c grp1 = [Select Id From pse__Grp__c Where Name = 'TestGroup'];
        pse__Resource_Request__c resourceRequest = UTL_OWF_TestData.createResourceRequest(agreement.Id, oppty.Id, project.Id);
        resourceRequest.pse__Group__c = grp1.Id;
        Test.startTest();
            insert resourceRequest;
            Set<Id> rrIdSet = new Set<Id>();
            rrIdSet.add(resourceRequest.Id);
            Map<Id, pse__Resource_Request__c> resReqsById = new SLT_Resource_Request(false).selectResReqsById(rrIdSet,  resourceRequestFieldSet);
        Test.stopTest();
    
        Integer expected = 1;
        System.assertEquals(expected, resReqsById.size());
    }
}