public class SLT_Bid_History_External extends fflib_SObjectSelector{
    
    /**
    * This method used to get field list of sobject
    */
    public List<Schema.SObjectField> getSObjectFieldList(){
        return new List<Schema.SObjectField> {
            Bid_History__x.Id,
            Bid_History__x.Name__c,
            Bid_History__x.DisplayUrl,
            Bid_History__x.Bid_Number_c__c,
            Bid_History__x.QIP_Loaded_c__c
        };
    }

    /**
    * This method used to set up type of sobject
    * @return  Schema.SObjectType
    */
    public Schema.SObjectType getSObjectType(){
        return Bid_History__x.sObjectType;
    }

    /**
    * This method used to get Bid_History__x by Id
    * @return  List<Bid_History__x>
    */
    public List<Bid_History__x> selectById(Set<Id> idSet){
        return (List<Bid_History__x>) selectSObjectsById(idSet);
    }


    /*
    * This method is use to query primary agreement of related opportunity which exclude current agreement
    *
    */
    public List<Bid_History__x> getBidHistoryUsingBidHistoryNumber(String bidHistoryNumber, Set<String> fieldSet){

        return (List<Bid_History__x>) Database.query(
            newQueryFactory(true).selectFields(fieldSet).setCondition('Name__c =: bidHistoryNumber').toSOQL());
    }
}