<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>pse__SkillCertificationRatingHelp</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Records a resource&apos;s proficiency rating in a skill/certification.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Is_Duplicate__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Duplicate</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Legacy_ID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <externalId>true</externalId>
        <label>Legacy ID</label>
        <length>64</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>isPSEActiveResource__c</fullName>
        <description>To Check whether the resource is active or not</description>
        <externalId>false</externalId>
        <formula>IF( AND(pse__Resource__r.pse__Is_Resource__c,pse__Resource__r.pse__Is_Resource_Active__c),True, False)</formula>
        <inlineHelpText>To Check whether the resource is active or not</inlineHelpText>
        <label>isPSEActiveResource</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>pse__Date_Achieved__c</fullName>
        <deprecated>false</deprecated>
        <description>The date when the rating was achieved.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date when the rating was achieved.</inlineHelpText>
        <label>Date Achieved</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Evaluation_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Date on which the rating was evaluated.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date on which the rating was evaluated.</inlineHelpText>
        <label>Evaluation Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Expiration_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Date on which the skill or certification rating expires.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date on which the skill or certification rating expires.</inlineHelpText>
        <label>Expiration Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Last_Used_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>The date when the resource last used the skill.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date when the resource last used the skill.</inlineHelpText>
        <label>Last Used Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>pse__Notes__c</fullName>
        <deprecated>false</deprecated>
        <description>You can enter information about the skill or certification rating.</description>
        <externalId>false</externalId>
        <inlineHelpText>You can enter information about the skill or certification rating.</inlineHelpText>
        <label>Notes</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>pse__Numerical_Rating__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(
        		ISNUMBER(
        			LEFT(TEXT(pse__Rating__c),2)
        		),
				VALUE(LEFT(TEXT(pse__Rating__c),2)),
				VALUE(LEFT(TEXT(pse__Rating__c),1))
			)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Numerical Rating</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pse__Rating__c</fullName>
        <deprecated>false</deprecated>
        <description>The level of exposure or ability that the resource has for the skill or certification.</description>
        <externalId>false</externalId>
        <inlineHelpText>The level of exposure or ability that the resource has for the skill or certification.</inlineHelpText>
        <label>Rating</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>None</fullName>
                    <default>true</default>
                    <label>None</label>
                </value>
                <value>
                    <fullName>1 - Limited Exposure</fullName>
                    <default>false</default>
                    <label>1 - Limited Exposure</label>
                </value>
                <value>
                    <fullName>2 - Some Familiarity</fullName>
                    <default>false</default>
                    <label>2 - Some Familiarity</label>
                </value>
                <value>
                    <fullName>3 - Comfortable</fullName>
                    <default>false</default>
                    <label>3 - Comfortable</label>
                </value>
                <value>
                    <fullName>4 - Strong</fullName>
                    <default>false</default>
                    <label>4 - Strong</label>
                </value>
                <value>
                    <fullName>5 - Expert</fullName>
                    <default>false</default>
                    <label>5 - Expert</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pse__Resource__c</fullName>
        <deprecated>false</deprecated>
        <description>The resource to which the rating applies.</description>
        <externalId>false</externalId>
        <inlineHelpText>The resource to which the rating applies.</inlineHelpText>
        <label>Resource</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Skill and Certification Ratings</relationshipLabel>
        <relationshipName>Skill_Certification_Ratings</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>pse__Skill_Certification__c</fullName>
        <deprecated>false</deprecated>
        <description>The skill or certification to which the rating applies.</description>
        <externalId>false</externalId>
        <inlineHelpText>The skill or certification to which the rating applies.</inlineHelpText>
        <label>Skill / Certification</label>
        <referenceTo>pse__Skill__c</referenceTo>
        <relationshipLabel>Resource Ratings</relationshipLabel>
        <relationshipName>Resource_Ratings</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>pse__Years_Experience__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of years&apos; experience a resource has in a skill.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of years&apos; experience a resource has in a skill.</inlineHelpText>
        <label>Years of Experience</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Skill / Certification Rating</label>
    <nameField>
        <displayFormat>Rating{0000000}</displayFormat>
        <label>Rating Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Skill and Certification Ratings</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>SCR_VR01_OWF_PreventSCR_Create_Edit</fullName>
        <active>true</active>
        <description>OWF - MC- ESPSFDCQI-2272- Validates that User can create Skill Certification Ratings for its own Contact.</description>
        <errorConditionFormula>AND(
	$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c = FALSE,
	$Profile.Name &lt;&gt; &apos;System Administrator&apos;,
	AND( 
		NOT($Permission.Create_and_Edit_All_Skill_Certification_Ratings), 
		NOT($User.FirstName == pse__Resource__r.FirstName &amp;&amp; $User.LastName == pse__Resource__r.LastName) 
	)
)</errorConditionFormula>
        <errorMessage>You can only create Skill Certification Ratings for your own Contact. If you continue to have issues please
contact your Triage Manager for support.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>SCR_VR02_OWF_PreventSCR_Create</fullName>
        <active>true</active>
        <description>Rating field is required when trying to save a new skill/certification record</description>
        <errorConditionFormula>AND($Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c == FALSE,OR(ISPICKVAL(pse__Rating__c, &quot;None&quot;),ISPICKVAL(pse__Rating__c, &apos;&apos;))
,$Permission.OWF_Sales_User_with_PSA_Access = TRUE)</errorConditionFormula>
        <errorMessage>Please select a Rating.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>pse__Check_Resource</fullName>
        <active>true</active>
        <errorConditionFormula>AND(LEN(pse__Resource__c) &gt;  0,  NOT( pse__Resource__r.pse__Is_Resource__c))</errorConditionFormula>
        <errorDisplayField>pse__Resource__c</errorDisplayField>
        <errorMessage>Resource lookup must be set to an Contact with Is Resource = true</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
