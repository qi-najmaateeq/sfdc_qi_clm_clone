<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Global selling team info</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Assertiveness__c</fullName>
        <externalId>false</externalId>
        <label>Assertiveness</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>OrgChartPlus__ChartPosX__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>ChartPosX</label>
        <precision>16</precision>
        <required>false</required>
        <scale>8</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__ChartPosY__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>ChartPosY</label>
        <precision>16</precision>
        <required>false</required>
        <scale>8</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__Competitor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Competitor</label>
        <referenceTo>OrgChartPlus__SM_Competitor__c</referenceTo>
        <relationshipLabel>MSO OrgChart TeamMembers</relationshipLabel>
        <relationshipName>MSO_OrgChart_TeamMembers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OrgChartPlus__Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>MSO OrgChart TeamMembers</relationshipLabel>
        <relationshipName>MSO_OrgChart_TeamMembers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OrgChartPlus__DisplayOrder__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>DisplayOrder</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__MoleculePosX__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>MoleculePosX</label>
        <precision>16</precision>
        <required>false</required>
        <scale>8</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__MoleculePosY__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>MoleculePosY</label>
        <precision>16</precision>
        <required>false</required>
        <scale>8</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF ( ISNULL( OrgChartPlus__User__c )=false, OrgChartPlus__User__r.FirstName + &apos; &apos; + OrgChartPlus__User__r.LastName, 
IF ( ISNULL( OrgChartPlus__Contact__c )=false, OrgChartPlus__Contact__r.FirstName + &apos; &apos; + OrgChartPlus__Contact__r.LastName, 
IF ( ISNULL( OrgChartPlus__Competitor__c )=false, OrgChartPlus__Competitor__r.Name, 
IF ( ISNULL( OrgChartPlus__NonSFDCPerson__c )=false, OrgChartPlus__NonSFDCPerson__r.Name, 
null ))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__NonSFDCPerson__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Non-SFDC Person</label>
        <referenceTo>OrgChartPlus__SM_NonSFUser__c</referenceTo>
        <relationshipLabel>MSO OrgChart TeamMembers</relationshipLabel>
        <relationshipName>MSO_OrgChart_TeamMembers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OrgChartPlus__OrgChart__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>OrgChart</label>
        <referenceTo>OrgChartPlus__MSO_OrgChart2__c</referenceTo>
        <relationshipLabel>MSO OrgChart TeamMembers</relationshipLabel>
        <relationshipName>MSO_OrgChart_TeamMembers</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>OrgChartPlus__ShowFlags__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>ShowFlags</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>MSO_OrgChart_TeamMembers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>QuintilesCompetitor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>IQVIACompetitor</label>
        <referenceTo>Competitor__c</referenceTo>
        <relationshipLabel>MSO OrgChart TeamMembers</relationshipLabel>
        <relationshipName>MSO_OrgChart_TeamMembers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Responsiveness__c</fullName>
        <externalId>false</externalId>
        <label>Responsiveness</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <label>MSO OrgChart TeamMember</label>
    <nameField>
        <displayFormat>MOT-{00000000}</displayFormat>
        <label>Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>MSO OrgChart TeamMembers</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
