<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Global customer contact info</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Challenger_Profile_Score__c</fullName>
        <externalId>false</externalId>
        <formula>IF( ( ISPICKVAL(OrgChartPlus__Contact__r.Contact_Profile__c, &apos;Mobilizer&apos;)) ,3, 
IF( ( ISPICKVAL(OrgChartPlus__Contact__r.Contact_Profile__c, &apos;Talker&apos;)) ,0, 
1))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Challenger Profile Score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Challenger_Profile__c</fullName>
        <externalId>false</externalId>
        <label>Challenger Profile</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Influence_Score__c</fullName>
        <externalId>false</externalId>
        <formula>IF( OrgChartPlus__Influence__c =0,0, 
IF( OrgChartPlus__Influence__c =100,1, 
IF( OrgChartPlus__Influence__c =200,2, 
3)))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Influence Score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>MSO OrgChart Entity Common</relationshipLabel>
        <relationshipName>MSO_OrgChartEntityCommon</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OrgChartPlus__Influence__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Influence</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__MatrixText__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>MatrixText</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>OrgChartPlus__Opinion__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Opinion</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgChartPlus__Opportunity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>MSO OrgChart Entity Common</relationshipLabel>
        <relationshipName>MSO_OrgChart_EntityCommon</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Relationship_Score__c</fullName>
        <externalId>false</externalId>
        <formula>Influence_Score__c * ( Challenger_Profile_Score__c + View_of_Competitor_Score__c + View_of_Quintiles_Score__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Relationship Score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>View_of_Competitor_Score__c</fullName>
        <externalId>false</externalId>
        <formula>IF( View_of_Competitor__c =-300,-3, 
IF( View_of_Competitor__c =-200,-2, 
IF( View_of_Competitor__c =-100,-1, 
IF( View_of_Competitor__c =1,0, 
IF( View_of_Competitor__c =100,1, 
IF( View_of_Competitor__c =200,2, 
IF( View_of_Competitor__c =300,3, 
0)))))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>View of Competitor Score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>View_of_Competitor__c</fullName>
        <externalId>false</externalId>
        <label>View of Competitor</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>View_of_Quintiles_Score__c</fullName>
        <externalId>false</externalId>
        <formula>IF( View_of_Quintiles__c =-300,-3, 
IF( View_of_Quintiles__c =-200,-2, 
IF( View_of_Quintiles__c =-100,-1, 
IF( View_of_Quintiles__c =1,0, 
IF( View_of_Quintiles__c =100,1, 
IF( View_of_Quintiles__c =200,2, 
IF( View_of_Quintiles__c =300,3, 
0)))))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>View of IQVIA Score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>View_of_Quintiles__c</fullName>
        <externalId>false</externalId>
        <label>View of IQVIA</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>MSO OrgChart Entity Common</label>
    <nameField>
        <displayFormat>OEC-{00000000}</displayFormat>
        <label>Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>MSO OrgChart Entity Common</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
