<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>Lightning experience of MyCDA application</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>MyCDA</label>
    <navType>Standard</navType>
    <tabs>CDA_Request</tabs>
    <tabs>CDA_Account__c</tabs>
    <tabs>CDA_Negotiator_Support</tabs>
    <tabs>CDA_Lookup_Report</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>MyCDA_UtilityBar</utilityBar>
</CustomApplication>
