<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-Chatter</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-File</tabs>
    <tabs>Study__c</tabs>
    <tabs>Study_Site_Relationship__c</tabs>
    <tabs>Study_Contact_Relationship__c</tabs>
    <tabs>Activity__c</tabs>
    <tabs>ServiceNow_Group__c</tabs>
    <tabs>TimeSheet__c</tabs>
    <tabs>Queue_User_Relationship__c</tabs>
    <tabs>Production_Environment__c</tabs>
    <tabs>CaseHistory__c</tabs>
    <tabs>StudySiteContactRelationship__c</tabs>
    <tabs>Favorite__c</tabs>
    <tabs>Property__c</tabs>
</CustomApplication>
