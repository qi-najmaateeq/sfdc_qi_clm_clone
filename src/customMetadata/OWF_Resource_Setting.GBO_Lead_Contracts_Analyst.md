<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GBO-Lead Contracts Analyst</label>
    <protected>false</protected>
    <values>
        <field>Estimated_Fees__c</field>
        <value xsi:type="xsd:double">40.0</value>
    </values>
    <values>
        <field>Is_there_a_Client_Bid_Grid__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Number_of_Sites__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Potential_Regions__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Project_Ex_Number_of_Unique_Tables__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>RFP_Ranking__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Re_Bid_Complexity__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Requested_Services__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Staffing_Number_of_Unique_Tables__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Sub_Group__c</field>
        <value xsi:type="xsd:string">GBO-Lead Contracts Analyst</value>
    </values>
</CustomMetadata>
