<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CNF - GBO_Budget_Tool__c</label>
    <protected>false</protected>
    <values>
        <field>RecordTypeName__c</field>
        <value xsi:type="xsd:string">CNF - GBO</value>
    </values>
    <values>
        <field>SourceField__c</field>
        <value xsi:type="xsd:string">Budget_Tool__c</value>
    </values>
    <values>
        <field>TargetField__c</field>
        <value xsi:type="xsd:string">Budget_Tool__c</value>
    </values>
</CustomMetadata>
