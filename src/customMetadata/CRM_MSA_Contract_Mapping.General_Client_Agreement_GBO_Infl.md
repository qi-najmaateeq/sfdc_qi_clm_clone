<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>General Client Agreement - GBO_Infl</label>
    <protected>false</protected>
    <values>
        <field>RecordTypeName__c</field>
        <value xsi:type="xsd:string">General Client Agreement - GBO</value>
    </values>
    <values>
        <field>SourceField__c</field>
        <value xsi:type="xsd:string">Inflation_Index_used_if_not_IQVIA_Std__c</value>
    </values>
    <values>
        <field>TargetField__c</field>
        <value xsi:type="xsd:string">Inflation_Index_used_if_not_IQVIA_Std__c</value>
    </values>
</CustomMetadata>
