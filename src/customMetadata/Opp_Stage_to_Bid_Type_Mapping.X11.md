<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>11</label>
    <protected>false</protected>
    <values>
        <field>Agreement_Record_Type__c</field>
        <value xsi:type="xsd:string">Post Award</value>
    </values>
    <values>
        <field>Bid_Header_and_Desc__c</field>
        <value xsi:type="xsd:string">Post Award,Used at a closed stage to request resources to further develop or revisit the awarded work.  Can be used to request resource for Non-compete strategy, engage with the operations team to revisit a strategy and perform a paid feasibility.</value>
    </values>
    <values>
        <field>Opp_Stage__c</field>
        <value xsi:type="xsd:string">6</value>
    </values>
</CustomMetadata>
