<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>United States of America</label>
    <protected>false</protected>
    <values>
        <field>BD_Lead_Sub_Region__c</field>
        <value xsi:type="xsd:string">United States of America</value>
    </values>
    <values>
        <field>Potential_Region__c</field>
        <value xsi:type="xsd:string">USA/Canada</value>
    </values>
</CustomMetadata>
