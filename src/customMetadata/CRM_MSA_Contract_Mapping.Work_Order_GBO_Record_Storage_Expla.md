<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Work Order - GBO_Record_Storage_Expla</label>
    <protected>false</protected>
    <values>
        <field>RecordTypeName__c</field>
        <value xsi:type="xsd:string">Work Order - GBO</value>
    </values>
    <values>
        <field>SourceField__c</field>
        <value xsi:type="xsd:string">Record_Storage_Explanation__c</value>
    </values>
    <values>
        <field>TargetField__c</field>
        <value xsi:type="xsd:string">Record_Storage_Explanation__c</value>
    </values>
</CustomMetadata>
