<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Docusign Branding English</label>
    <protected>false</protected>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Docusign Branding</value>
    </values>
    <values>
        <field>Language__c</field>
        <value xsi:type="xsd:string">English</value>
    </values>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">c5993519-e4d2-4f25-9765-8a396f2cb219</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">Branding</value>
    </values>
</CustomMetadata>
