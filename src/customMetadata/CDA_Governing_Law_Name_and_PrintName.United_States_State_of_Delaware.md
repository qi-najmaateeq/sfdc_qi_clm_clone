<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>United States, State of Delaware</label>
    <protected>false</protected>
    <values>
        <field>Display_Text__c</field>
        <value xsi:type="xsd:string">the United States, State of Delaware</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">United States, State of Delaware</value>
    </values>
</CustomMetadata>
