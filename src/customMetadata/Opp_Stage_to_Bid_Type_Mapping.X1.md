<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>1</label>
    <protected>false</protected>
    <values>
        <field>Agreement_Record_Type__c</field>
        <value xsi:type="xsd:string">Early Engagement Bid</value>
    </values>
    <values>
        <field>Bid_Header_and_Desc__c</field>
        <value xsi:type="xsd:string">Early Engagement Bid,Used pre-RFP when sales require key therapeutic and/or functional experts support to influence a customer proactively or other pre-study initiation collaborative engagements.</value>
    </values>
    <values>
        <field>Opp_Stage__c</field>
        <value xsi:type="xsd:string">1</value>
    </values>
</CustomMetadata>
