<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Email Request Canceled English</label>
    <protected>false</protected>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Email Request Canceled</value>
    </values>
    <values>
        <field>Language__c</field>
        <value xsi:type="xsd:string">English</value>
    </values>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">00X4D000000HuTx</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">CDA Request Canceled</value>
    </values>
</CustomMetadata>
