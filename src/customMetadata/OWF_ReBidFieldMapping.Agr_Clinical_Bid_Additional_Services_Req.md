<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Agr_Clinical_Bid_Additional_Services_Req</label>
    <protected>false</protected>
    <values>
        <field>BidType__c</field>
        <value xsi:type="xsd:string">Rebid</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">Clinical_Bid</value>
    </values>
    <values>
        <field>SourceObject__c</field>
        <value xsi:type="xsd:string">Agreement</value>
    </values>
    <values>
        <field>SourceType__c</field>
        <value xsi:type="xsd:string">Field</value>
    </values>
    <values>
        <field>SourceValue__c</field>
        <value xsi:type="xsd:string">Additional_Services_Requested__c</value>
    </values>
    <values>
        <field>TargetField__c</field>
        <value xsi:type="xsd:string">Additional_Services_Requested__c</value>
    </values>
</CustomMetadata>
