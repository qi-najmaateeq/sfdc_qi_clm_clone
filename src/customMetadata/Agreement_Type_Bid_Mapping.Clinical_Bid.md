<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Clinical Bid</label>
    <protected>false</protected>
    <values>
        <field>Bid_No__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Is_Incremental__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
