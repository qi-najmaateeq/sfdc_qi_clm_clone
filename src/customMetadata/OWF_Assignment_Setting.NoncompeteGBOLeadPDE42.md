<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NoncompeteGBOLeadPDE42</label>
    <protected>false</protected>
    <values>
        <field>Agreement_Record_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Automation_backup_flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Automation_initial_bid_flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Automation_manual_triage_flag__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Complexity_Score__c</field>
        <value xsi:type="xsd:double">40.0</value>
    </values>
    <values>
        <field>Disable_availability_flag__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Field_End_Date__c</field>
        <value xsi:type="xsd:string">Bid_Due_Date</value>
    </values>
    <values>
        <field>Field_Start_Date__c</field>
        <value xsi:type="xsd:string">CreatedDate</value>
    </values>
    <values>
        <field>Ignore_FTE_Threshold__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Matching_Point_BD_Lead_Sub_Region__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Matching_Point_Customer__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Matching_Point_Grade_Level__c</field>
        <value xsi:type="xsd:double">20.0</value>
    </values>
    <values>
        <field>Matching_Point_Indication__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Matching_Point_Intervention_Type__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Matching_Point_Line_of_Business__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Matching_Point_Phase__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Matching_Point_Potential_Regions__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Matching_Point_Targeted_Countries__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Matching_Point_Therapeutic_Area__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Matching_Point_Threshold__c</field>
        <value xsi:type="xsd:double">50.0</value>
    </values>
    <values>
        <field>Matching_Point_Workload_Ranking__c</field>
        <value xsi:type="xsd:double">40.0</value>
    </values>
    <values>
        <field>RFP_Ranking__c</field>
        <value xsi:type="xsd:string">4</value>
    </values>
    <values>
        <field>Resource_Request_Type__c</field>
        <value xsi:type="xsd:string">Clinical</value>
    </values>
    <values>
        <field>Sub_Group__c</field>
        <value xsi:type="xsd:string">GBO-Lead PD</value>
    </values>
    <values>
        <field>Suggested_FTE__c</field>
        <value xsi:type="xsd:double">0.43</value>
    </values>
    <values>
        <field>Value_Adjusted_End_Days__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Value_Adjusted_Start_Days__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Workload_Ranking__c</field>
        <value xsi:type="xsd:string">E</value>
    </values>
</CustomMetadata>
