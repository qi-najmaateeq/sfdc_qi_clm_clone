<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Email Recipient ReviewRemind Auditor En</label>
    <protected>false</protected>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Email Recipient Review Reminder Auditor</value>
    </values>
    <values>
        <field>Language__c</field>
        <value xsi:type="xsd:string">English</value>
    </values>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">00X4D000000HuU4</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">CDA Recipient Review Reminder For Auditor/CEVA/Vendor</value>
    </values>
</CustomMetadata>
