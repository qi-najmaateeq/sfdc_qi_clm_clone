<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <label>Supplier</label>
    <protected>false</protected>
    <values>
        <field>AND_Territory_Region__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ISO_Code__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
