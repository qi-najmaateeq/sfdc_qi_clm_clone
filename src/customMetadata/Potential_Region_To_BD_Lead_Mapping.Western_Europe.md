<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Western Europe</label>
    <protected>false</protected>
    <values>
        <field>BD_Lead_Sub_Region__c</field>
        <value xsi:type="xsd:string">Western Europe</value>
    </values>
    <values>
        <field>Potential_Region__c</field>
        <value xsi:type="xsd:string">Europe/Middle East/Africa EMEA</value>
    </values>
</CustomMetadata>
