<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Usr_Non_RFP_Bid_Requester_Name__c</label>
    <protected>false</protected>
    <values>
        <field>BidType__c</field>
        <value xsi:type="xsd:string">None</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">Non_RFP_Bid</value>
    </values>
    <values>
        <field>SourceObject__c</field>
        <value xsi:type="xsd:string">User</value>
    </values>
    <values>
        <field>SourceType__c</field>
        <value xsi:type="xsd:string">Field</value>
    </values>
    <values>
        <field>SourceValue__c</field>
        <value xsi:type="xsd:string">Name</value>
    </values>
    <values>
        <field>TargetField__c</field>
        <value xsi:type="xsd:string">Requester_Name__c</value>
    </values>
</CustomMetadata>
