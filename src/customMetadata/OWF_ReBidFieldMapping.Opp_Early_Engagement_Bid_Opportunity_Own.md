<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Opp_Early_Engagement_Bid_Opportunity_Own</label>
    <protected>false</protected>
    <values>
        <field>BidType__c</field>
        <value xsi:type="xsd:string">None</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">Early_Engagement_Bid</value>
    </values>
    <values>
        <field>SourceObject__c</field>
        <value xsi:type="xsd:string">Opportunity</value>
    </values>
    <values>
        <field>SourceType__c</field>
        <value xsi:type="xsd:string">Field</value>
    </values>
    <values>
        <field>SourceValue__c</field>
        <value xsi:type="xsd:string">Owner.Name</value>
    </values>
    <values>
        <field>TargetField__c</field>
        <value xsi:type="xsd:string">Opportunity_Owner__c</value>
    </values>
</CustomMetadata>
