<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Chinese Translation</label>
    <protected>false</protected>
    <values>
        <field>Supporting_Language__c</field>
        <value xsi:type="xsd:string">Chinese</value>
    </values>
    <values>
        <field>localize_Field_on_Cross_Walk__c</field>
        <value xsi:type="xsd:string">Chinese_Translation__c</value>
    </values>
</CustomMetadata>
