<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>US Tech &amp; Apps</label>
    <protected>false</protected>
    <values>
        <field>AND_Territory_Region__c</field>
        <value xsi:type="xsd:string">US Tech &amp; Apps</value>
    </values>
    <values>
        <field>ISO_Code__c</field>
        <value xsi:type="xsd:string">5S</value>
    </values>
</CustomMetadata>
