<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>OLI_WR01_CRM_InsertUpdate_Outbound</fullName>
        <actions>
            <name>OLI_OB01_CRM_InsertUpdate</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>CRM-QI-ESPSFDCQI-497 Workflow to send outbound message when opportunity line item is inserted or updated.</description>
        <formula>$Setup.Mulesoft_Integration_Control__c.Suppress_Outbound_Messages__c == false  &amp;&amp; Hierarchy_Level__c == &apos;Material&apos; &amp;&amp; ( Opportunity.Mulesoft_Opportunity_Sync__r.LI_Opportunity_Id__c  != null || Opportunity.Mulesoft_Opportunity_Sync__r.LQ_Opportunity_Id__c != null) &amp;&amp;  Send_Mulesoft_Outbound_Msg__c == TRUE &amp;&amp;  Opportunity.IsCurrencyChanged__c == false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
