<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BNF_stuck_in_SAP_PENDING_status_Notification_fo_rmore_than_an_hour</fullName>
        <description>BNF stuck in SAP PENDING status Notification fo rmore than an hour</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/BNF_stuck_in_SAP_PENDING_status_Notification_fo_rmore_than_an_hour</template>
    </alerts>
    <alerts>
        <fullName>Notify_IF_BNF_Integration_status_changes_abnormally</fullName>
        <description>Notify IF BNF Integration status changes abnormally</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Abnormal_BNF_Stage_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>PurcahseBNFApprovalNotification</fullName>
        <description>Purcahse/BNF Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Current_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>First_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Submitter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/BNFApprovalNotification</template>
    </alerts>
    <alerts>
        <fullName>Purcahse_BNF_Submit_Notification_Japan</fullName>
        <description>Purcahse/BNF Submit Notification Japan</description>
        <protected>false</protected>
        <recipients>
            <recipient>JP01_Billing_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/BNFApprovalsNewBNFrecordsubmittednotification</template>
    </alerts>
    <alerts>
        <fullName>PurchaseBNFRejectionNotification</fullName>
        <description>Purchase/BNF Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Current_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>First_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Submitter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/BNF_Rejected_VF_Template</template>
    </alerts>
    <alerts>
        <fullName>Purchase_BNF_Rejection_Notification_SBS</fullName>
        <description>Purchase/BNF Rejection Notification From SBS</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>JP01_Billing_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/BNFRejectedBNFrecordrejected</template>
    </alerts>
    <alerts>
        <fullName>eBNFApprovalNotification</fullName>
        <description>eBNF Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Current_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>First_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Submitter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/BNFApprovalNotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>BNFStatusUpdateAccepted</fullName>
        <field>BNF_Status__c</field>
        <literalValue>Accepted</literalValue>
        <name>BNF Status Update: Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BNFStatusUpdateRejected</fullName>
        <field>BNF_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>BNF Status Update: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BNF_Status_Update_RA_Accepted</fullName>
        <field>BNF_Status__c</field>
        <literalValue>RA Accepted</literalValue>
        <name>BNF Status Update: RA Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BNF_Status_Update_RA_Rejected</fullName>
        <field>BNF_Status__c</field>
        <literalValue>RA Rejected</literalValue>
        <name>BNF Status Update: RA Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_First_RA_Id</fullName>
        <field>First_RA__c</field>
        <name>Clear First RA Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_rejection_reason</fullName>
        <field>Rejection_Reasons__c</field>
        <name>Clear rejection reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_First_RA_Email</fullName>
        <field>First_RA_Email__c</field>
        <formula>Revenue_Analyst__r.Revenue_Analyst_Email__c</formula>
        <name>Set First RA Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_submitter_email</fullName>
        <description>Set email of user who submitted BNF</description>
        <field>Submitter_Email__c</field>
        <formula>$User.Email</formula>
        <name>Set submitter email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStatus</fullName>
        <field>BNF_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BNF_Status_LO_Accepted</fullName>
        <field>BNF_Status__c</field>
        <literalValue>LO Accepted</literalValue>
        <name>Update BNF Status : LO Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BNF_Status_LO_Rejected</fullName>
        <field>BNF_Status__c</field>
        <literalValue>LO Rejected</literalValue>
        <name>Update BNF Status : LO Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pricing_Flag_TRUE</fullName>
        <field>Has_Pricing_Configuration__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity__r.CEUpdate Pricing Flag : T</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_New</fullName>
        <field>BNF_Status__c</field>
        <literalValue>New</literalValue>
        <name>Update Status : New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_SAP_Contract_Confirmed</fullName>
        <field>BNF_Status__c</field>
        <literalValue>SAP Contract Confirmed</literalValue>
        <name>Update Status : SAP Contract Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Submitted</fullName>
        <field>BNF_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status : Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitter_Email</fullName>
        <description>Store email address of user who submitted the BNF</description>
        <field>Submitter_Email__c</field>
        <formula>$User.Email</formula>
        <name>Update Submitter Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Abnormal BNF Stage Change Notification</fullName>
        <actions>
            <name>Notify_IF_BNF_Integration_status_changes_abnormally</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Abnormal BNF Stage Change Notification</description>
        <formula>AND(  AND( NOT(ISPICKVAL(PRIORVALUE( BNF_Status__c ),&apos;RA Accepted&apos;)), NOT(ISPICKVAL(PRIORVALUE( BNF_Status__c ),&apos;Accepted&apos;)) ) ,  ISPICKVAL( BNF_Status__c ,&apos;SAP Pending&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IBNF is stuck in SAP PENDING Status for more than an hour</fullName>
        <active>true</active>
        <criteriaItems>
            <field>BNF2__c.BNF_Status__c</field>
            <operation>equals</operation>
            <value>SAP Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BNF_stuck_in_SAP_PENDING_status_Notification_fo_rmore_than_an_hour</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>BNF2__c.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Pricing Flag %3A TRUE</fullName>
        <actions>
            <name>Update_Pricing_Flag_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Opportunity__r.CEQActiveRelease__c &gt; 0</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
