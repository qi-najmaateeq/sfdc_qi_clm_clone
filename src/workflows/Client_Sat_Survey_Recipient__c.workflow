<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Create_Outbound_Message_Survey_Rec</fullName>
        <apiVersion>44.0</apiVersion>
        <description>Send outbound message for each Survey Recipient to Qualtrics to send survey</description>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_5vtD0EJ83DaKldX&amp;u=UR_2bsaNc7DAqx1sRD&amp;t=OC_6g57JzRrg2kOxEZ&amp;b=imshealth</endpointUrl>
        <fields>Account_Region_Territory__c</fields>
        <fields>Client_Sat_Survey__c</fields>
        <fields>Contact__c</fields>
        <fields>CreatedById</fields>
        <fields>CreatedDate</fields>
        <fields>CurrencyIsoCode</fields>
        <fields>Email__c</fields>
        <fields>First_Name__c</fields>
        <fields>Id</fields>
        <fields>IsDeleted</fields>
        <fields>LastModifiedById</fields>
        <fields>LastModifiedDate</fields>
        <fields>LastReferencedDate</fields>
        <fields>LastViewedDate</fields>
        <fields>Last_Name__c</fields>
        <fields>Name</fields>
        <fields>Opportunity_Account_Name__c</fields>
        <fields>Opportunity_Account_Parent_Name__c</fields>
        <fields>Opportunity_Account_Parent_SAP_Ref__c</fields>
        <fields>Opportunity_Account_SAP_Ref__c</fields>
        <fields>Opportunity_Amount__c</fields>
        <fields>Opportunity_Name__c</fields>
        <fields>Opportunity_Number__c</fields>
        <fields>Opportunity_Primary_Project__c</fields>
        <fields>Opportunity_Product_Codes__c</fields>
        <fields>Opportunity_Type__c</fields>
        <fields>Phone__c</fields>
        <fields>Primary_Language__c</fields>
        <fields>Qualtrics_Survey_Language__c</fields>
        <fields>Send_Survey__c</fields>
        <fields>Survey_EM_Email__c</fields>
        <fields>Survey_EM_Employee_Number__c</fields>
        <fields>Survey_EM_Name__c</fields>
        <fields>Survey_PIC_Country__c</fields>
        <fields>Survey_PIC_Email__c</fields>
        <fields>Survey_PIC_Employee_Number__c</fields>
        <fields>Survey_PIC_Name__c</fields>
        <fields>Survey_Recipient_ID__c</fields>
        <fields>Survey_Send_Date__c</fields>
        <fields>Survey_Type__c</fields>
        <fields>SystemModstamp</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>qualtrics.integration@iqvia.com</integrationUser>
        <name>Create Outbound Message Survey Rec</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CSAT_Survey_Recipient_to_Qualtrics</fullName>
        <actions>
            <name>Create_Outbound_Message_Survey_Rec</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>Send an Outbound Message to Qualtrics when a Survey Recipient record is created</description>
        <formula>Contact__r.Survey_Opt_Out__c  &lt;&gt; True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
