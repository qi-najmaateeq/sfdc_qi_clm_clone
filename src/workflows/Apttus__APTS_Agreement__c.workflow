<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AGR_EA11_CRM_Novella_bid_history_alert</fullName>
        <ccEmails>request.proposal.global@quintiles.com</ccEmails>
        <description>CRM-MC-ESPSFDCQI-3284-Novella bid history alert</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_MC_Workflow_Templates/AGR_ET12_CRM_Bid_History_Clinical_Bid_alert_to_RFP_mailbox</template>
    </alerts>
	<alerts>
        <fullName>Bid_History_CSS_Bid_alert_to_RFP_mailbox</fullName>
        <ccEmails>request.proposal.global@quintiles.com</ccEmails>
        <description>Bid History - CSS Bid alert to RFP mailbox</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_MC_Workflow_Templates/AGR_ET20_CSS_Bid_alert_to_RFP_mailbox_VF</template>
    </alerts>
    <alerts>
        <fullName>CPQ_TSL_Review_rejects_PD_budget</fullName>
        <description>CPQ TSL Review rejects PD budget</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ/CPQ_TSL_Review_Reject_Budget</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_proposal_developer_in_case_of_TSL_approved_the_request</fullName>
        <description>Email Alert to proposal developer in case of TSL approved the request</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cpqadmin@iqvia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CPQ/CPQ_Approval_Approved_Notification_ToProposalDeveloper</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_proposal_developer_in_case_of_TSL_rejects_the_request</fullName>
        <description>Email Alert to proposal developer in case of TSL rejects the request</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>cpqadmin@iqvia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CPQ/CPQ_Approval_Rejection_Notification_ToProposalDeveloper</template>
    </alerts>
    <alerts>
        <fullName>Initial_Bid_Agreement_Creation_Alert</fullName>
        <description>Initial Bid Agreement Creation Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Requester_email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ/RDSUNIT_Initial_Creation_From_Clinical</template>
    </alerts>
    <rules>
        <fullName>AGR-WR01-CRM-Awarded email alert - Rob Von Alten</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284- email sent when the following additional services are selected and opportunity awarded - eDiary
o IVR (Cenduit) 
o Translation Services 
o Other Services</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), Opportunity_Stage__c == &apos;7a. Closed Won&apos;, 
OR(INCLUDES(Additional_Services_Requested__c, &quot;IVR (Cenduit)&quot;), INCLUDES(Additional_Services_Requested__c, &quot;eDiary&quot;),INCLUDES(Additional_Services_Requested__c, &quot;Translation Services&quot;),INCLUDES(Additional_Services_Requested__c, &quot;Other Services/Specific Vendor requested&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR02-CRM-Bid Auto Alert</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), AND( OR( ISPICKVAL(Apttus__Related_Opportunity__r.Line_of_Business__c, &apos;Novella&apos;), ISPICKVAL(Apttus__Related_Opportunity__r.Line_of_Business__c, &apos;Core Clinical&apos;), ISPICKVAL(Apttus__Related_Opportunity__r.Line_of_Business__c, &apos;Outcome&apos;)), OR(  ISPICKVAL(Bid_Type__c, &apos;Initial&apos;),  ISPICKVAL(Bid_Type__c, &apos;Re-bid&apos;)), OR( ISPICKVAL( RFP_Ranking__c , &apos;2&apos;), ISPICKVAL( RFP_Ranking__c , &apos;3&apos;), ISPICKVAL( RFP_Ranking__c , &apos;4&apos;), ISPICKVAL( RFP_Ranking__c , &apos;5&apos;)) ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR03-CRM-Bid Grid - Budget Analyst Assigned</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - email sent to the bid grid owner to notify them of the BA assigned and the grid due date</description>
        <formula>AND( (!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), Budget_Analyst_Assigned__c != null, OR(RecordType.Name == &apos;Preferred Provider&apos;, RecordType.Name == &apos;Prelim Agreement&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR04-CRM-Bid Grid - QC Budget Analyst Assigned</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - email sent to the budget analyst assigned to QC</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), Grid_QC_Assigned_to__c != null)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR05-CRM-Bid History - Exec Review</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 -email sent to strategic pricing when bid over $20</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), ISPICKVAL(Estimated_Fees__c,&apos;Greater than $20M&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR06-CRM-Bid History - Insufficient Information</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - email to Bid history owner/creator when more information required</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), NOT(ISBLANK(TEXT(Insufficient_Information__c))), RecordType.Name = &apos;Prelim Agreement&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR07-CRM-Bid History - Pricing considerations alert</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - sends email to pricing team when bid sent = yes</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), ISPICKVAL(Bid_Sent__c, &apos;Yes&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR08-CRM-Bid History Client Bid Grid Alert</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - Sends and email to the request for Bid Grids mailbox</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), ISPICKVAL(Client_Bid_Grid_Team__c, &apos;Request for Bid Grids&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR09-CRM-Bid History CSS - Insufficient Information</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - sends email to Bid history owner/creator when more information required</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), NOT(ISBLANK(TEXT(Insufficient_Information__c))), RecordType.Name = &apos;CSS Bid&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR11-CRM-EBP 2%2E0</fullName>
        <active>false</active>
        <description>CRM-MC-ESPSFDCQI-3284 - sends and email to Rob Murray, Elli Ganas and RFPIntake@novellaclinical.com and request for proposals</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), ISPICKVAL(IQVIA_biotech__c,&apos;Yes&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR12-CRM-Novella bid history alert</fullName>
        <actions>
            <name>AGR_EA11_CRM_Novella_bid_history_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - sends and email to Rob Murray, Elli Ganas and RFPIntake@novellaclinical.com</description>
        <formula>AND(  (!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), OR(Line_of_Business__c ==&apos;Core Clinical&apos;,              Line_of_Business__c ==&apos;GFR Clinical&apos;,              Line_of_Business__c ==&apos;Data Management&apos;,              Line_of_Business__c ==&apos;Biostatistical/Medical Writing&apos;,              Line_of_Business__c ==&apos;Lifecycle Safety&apos;,              Line_of_Business__c ==&apos;Early Clinical Development&apos;,              Line_of_Business__c ==&apos;Outcome&apos;, Line_of_Business__c ==&apos;Novella&apos;),    RecordType.DeveloperName == &apos;Clinical_Bid&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR13-CRM-QIP Loaded notification</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - Email sent to opp owner, creator, finance and bid owner when the QIP has been loaded</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), ISPICKVAL(QIP_Loaded__c,&apos;Yes&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR14-CRM-QIP Submission Alert</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - Alert sent each time a new bid is created using QIP or QIP plus Others</description>
        <formula>AND(!($Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), NOT(ISBLANK(Bid_Sent_Date__c)), NOT(ISBLANK(Budget_Tools__c)), ISPICKVAL(QIP_Loaded__c,&apos;No&apos;), NOT(ISBLANK(Link_to_Budget_Files__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR15-CRM-RFI Bid Owner assigned</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - sends email when RFI assigned</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), RecordType.Name == &apos;RFI Request&apos;,  Owner:User.Username != null)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR16-CRM-RFI Request Alert</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - email sent to RFI group</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), OR(RecordType.Name == &apos;RFI Request&apos;, RecordType.Name == &apos;RFI&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR17-CRM-RFI Request Completed</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - email sent to requester and creator when status is moved to sent</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), RecordType.Name == &apos;RFI Request&apos;, ISPICKVAL(Bid_Sent__c, &apos;Yes&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR18-CRM-SPN Strategy Lead assignment</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - sends email to SPN Strategy lead and bid owner</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c),(Not(isblank(SPN_Strategy_Lead__c))),not(isblank( SPN_Strategy_Lead_email__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR19-CRM-TSL assignment required</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-3284 - email sent to Helen Welbank and TSL email box</description>
        <formula>AND((!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c || $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c), Line_of_Business__c == &apos;Core Clinical&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AGR-WR20-Bid History - CSS Bid alert to RFP mailbox</fullName>
        <actions>
            <name>Bid_History_CSS_Bid_alert_to_RFP_mailbox</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CSS Bid</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Line_of_Business__c</field>
            <operation>equals</operation>
            <value>ECG/Cardiac Safety</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send email on Inital Bid Agreement Creation</fullName>
        <actions>
            <name>Initial_Bid_Agreement_Creation_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Is_there_a_Client_Bid_Grid__c, &apos;Yes&apos;)  &amp;&amp;  NOT( ISBLANK( Requester_email__c ) )   &amp;&amp;  PRIORVALUE(Record_Type__c) = &apos;Clinical Bid&apos;  &amp;&amp; Record_Type__c = &apos;FDTN – Initial Bid&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <fieldUpdates>
        <fullName>Agreement_status_field_update</fullName>
        <field>Agreement_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Agreement status field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Update_Process_Step_TSL_Reject</fullName>
        <field>Process_Step__c</field>
        <literalValue>Challenge Call Complete</literalValue>
        <name>CPQ Update Process Step TSL Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OCE_Approved</fullName>
        <field>Apttus_Approval__Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>OCE Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OCE_PendingApproval</fullName>
        <field>Apttus_Approval__Approval_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>OCE PendingApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>O_ApprovalReject</fullName>
        <field>Apttus_Approval__Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>O_ApprovalReject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>O_StatusFieldUpdate</fullName>
        <field>Apttus_Approval__Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>O_StatusFieldUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Process_Step_Change</fullName>
        <field>Process_Step__c</field>
        <literalValue>TSL Approved</literalValue>
        <name>Process Step Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>process_step_field_update</fullName>
        <field>Process_Step__c</field>
        <literalValue>TSL Approved</literalValue>
        <name>process step field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TSL_User_approved_request</fullName>
        <field>Agreement_Status__c</field>
        <literalValue>TSL Approved</literalValue>
        <name>TSL User approved request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
