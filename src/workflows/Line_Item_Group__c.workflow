<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>LIG_WR01_CRM_InsertUpdate_Outbound</fullName>
        <actions>
            <name>LIG_OB01_CRM_InsertUpdate</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>CRM-QI-ESPSFDCQI-1013 - Workflow to send outbound message when Line_Item_Group__c record is inserted or updated</description>
        <formula>$Setup.Mulesoft_Integration_Control__c.Suppress_Outbound_Messages__c == false &amp;&amp; Opportunity__r.LQ_Opportunity_Id__c != null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
