<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>OPP_WEA01_CRM_Bid_Defense_7_day_reminder</fullName>
        <description>CRM-MC-ESPSFDCQI-2874 - Bid Defense 7 day reminder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_MC_Workflow_Templates/OPP_ET03_CRM_Bid_defense_presentation_date_more_than_30_days</template>
    </alerts>
    <alerts>
        <fullName>OPP_WEA02_CRM_Bid_Defense_14_day_reminder</fullName>
        <description>CRM-MC-ESPSFDCQI-2874 - Bid Defense 14 day reminder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_MC_Workflow_Templates/OPP_ET04_CRM_Bid_defense_presentation_date_past_7_and_14_days</template>
    </alerts>
    <alerts>
        <fullName>OPP_WEA03_CRM_Bid_Defense_30_day_reminder</fullName>
        <description>CRM-MC-ESPSFDCQI-2874 - Bid Defense 30 day reminder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_MC_Workflow_Templates/OPP_ET03_CRM_Bid_defense_presentation_date_more_than_30_days</template>
    </alerts>
    <rules>
        <fullName>OPP_WR01_CRM_InsertUpdate_Outbound</fullName>
        <actions>
            <name>OPP_OB01_CRM_InsertUpdate</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>CRM-QI-ESPSFDCQI-314 Workflow to send outbound message when opportunity is inserted or updated.</description>
        <formula>$Setup.Mulesoft_Integration_Control__c.Suppress_Outbound_Messages__c == false  &amp;&amp; Send_Mulesoft_Outbound_Msg__c == true &amp;&amp;  IsCurrencyChanged__c  == false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP_WR02_CRM_Bid_Defense_Presentation_Reminders</fullName>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Presentation_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>CRM-QI-ESPSFDCQI-2874 -</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>OPP_WEA01_CRM_Bid_Defense_7_day_reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Presentation_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>OPP_WEA03_CRM_Bid_Defense_30_day_reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Presentation_Date__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>OPP_WEA02_CRM_Bid_Defense_14_day_reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Presentation_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OPP_WR03_CRM_R%26D _Opp_Loss_Approval</fullName>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Lost_Opportunity_Approval__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>5. Finalizing Deal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>6. Received ATP/LOI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RD_Product_Count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>CRM-QI-ESPSFDCQI-3210</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
