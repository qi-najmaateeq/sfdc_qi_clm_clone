<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Process_Approved_email_template</fullName>
        <description>Approval Process Approved email template</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>One_Key_Email_Template/CAS_OneKey_Approval_Response</template>
    </alerts>
    <alerts>
        <fullName>Approval_Process_Rejected_email_template</fullName>
        <description>Approval Process Rejected email template</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>One_Key_Email_Template/CAS_OneKey_Approval_Response</template>
    </alerts>
    <alerts>
        <fullName>CAS_CSM_Closed_Survey_Lupin</fullName>
        <description>CAS_CSM_Closed_Survey_Lupin</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CRM_MC_Workflow_Templates/CAS_ET_TECH_Case_Closed_Lupin</template>
    </alerts>
    <alerts>
        <fullName>CAS_EA24_SendEmailNotificationBefore48HoursForSLADeadline</fullName>
        <description>CAS_EA24_SendEmailNotificationBefore48HoursForSLADeadline</description>
        <protected>false</protected>
        <recipients>
            <field>CaseOriginatorEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>no.reply.support@iqvia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Data_English_Templates/CAS_ET_CSM_DATA_MILESTONE_SLA_DEADLINE</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA01_CSM_ACN_CaseEscalation</fullName>
        <description>CAS_WEA01_CSM_ACN_CaseEscalation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET01_CSM_CaseEscalationNotification</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA02_CSM_ACN_Standard_Case_Auto_Response_Email</fullName>
        <description>CAS_WEA02_CSM_ACN_Standard Case Auto-Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/CAS_ET02_CSM_Standard_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA03_CSM_ACN_Canada_Case_Auto_Response_Email</fullName>
        <description>CAS_WEA03_CSM_ACN_Canada Case Auto-Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET03_CSM_Canada_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA04_CSM_ACN_Spanish_Case_Auto_Response_Email</fullName>
        <description>CAS_WEA04_CSM_ACN_Spanish Case Auto-Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET04_CSM_Spanish_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA05_CSM_ACN_French_Case_Auto_Response_Email</fullName>
        <description>CAS_WEA05_CSM_ACN_ French Case Auto-Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET05_CSM_French_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA06_CSM_ACN_Canada_Case_Resolution_Email_Template</fullName>
        <description>CAS_WEA06_CSM_ACN_Canada Case Resolution Email Template</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET06_CSM_Canada_Case_Resolution_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA07_CSM_ACN_Spanish_Case_Resolution_Email_Template</fullName>
        <description>CAS_WEA07_CSM_ACN_Spanish Case Resolution Email Template</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET07_CSM_Spanish_Case_Resolution_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA08_CSM_ACN_Canada_Case_Resolution_Email_Template</fullName>
        <description>CAS_WEA08_CSM_ACN_French Case Resolution Email Template</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET08_CSM_French_Case_Resolution_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA09_CSM_ACN_R_D_waiting_feedback_from_customer</fullName>
        <description>CAS_WEA09_CSM_ACN_R&amp;D waiting feedback from customer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET09_CSM_R_Dwaitingfeedbackfromcustomer</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA10_CSM_ACN_CanadaResolutionEmailTemplate</fullName>
        <description>CAS_WEA10_CSM_ACN_SendCanadaResolutionEmailTemplate</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET06_CSM_Canada_Case_Resolution_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA10_CSM_ACN_CaseClosureEmailAlert</fullName>
        <description>CAS_WEA10_CSM_ACN_CanadaCaseClosureEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET10_CSM_CanadaCaseClosureEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA11_CSM_ACN_SpanishCaseClosureEmailAlert</fullName>
        <description>CAS_WEA11_CSM_ACN_SpanishCaseClosureEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET11_CSM_SpanishCaseClosureEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA12_CSM_ACN_FrenchCaseClosureEmailAlert</fullName>
        <description>CAS_WEA12_CSM_ACN_FrenchCaseClosureEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET12_CSM_FrenchCaseClosureEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA13_CSM_ACN_English_Case_Auto_Response_Email</fullName>
        <description>CAS_WEA13_CSM_ACN_ English Case Auto-Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET13_CSM_English_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA14_CSM_ACN_English_Case_Resolution_Email_Template</fullName>
        <description>CAS_WEA14_CSM_ACN_English Case Resolution Email Template</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET14_CSM_English_Case_Resolution_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA15_CSM_ACN_EnglishCaseClosureEmailAlert</fullName>
        <description>CAS_WEA15_CSM_ACN_EnglishCaseClosureEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET15_CSM_EnglishCaseClosureEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA16_CSM_ACN_StoppedSince</fullName>
        <ccEmails>karishmastar9@gmail.com</ccEmails>
        <description>CAS_WEA16_CSM_ACN_StoppedSince</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET16_CSM_StoppedSince</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA17_CSM_ACN_ResolutionTimeElapsed</fullName>
        <description>CAS_WEA17_CSM_ACN_ResolutionTimeElapsed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET17_CSM_ResolutionTimeElapsed</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA18_CSM_ACN_NewCase</fullName>
        <description>CAS_WEA18_CSM_ACN_NewCase</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CAS_ET02_CSM_Standard_Case_Auto_Response_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA20_CustomerReply</fullName>
        <description>CAS_WEA20_CustomerReply</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>email2casecsm@quintilesims.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CAS_ET18_CSM_CustomerReply</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA22_AddNewCommentContact</fullName>
        <description>CAS_WEA22_AddNewCommentContact</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>email2casecsm@quintilesims.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CAS_ET19_CSM_NewCaseComment</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA23_AddNewCommentOwner</fullName>
        <description>CAS_WEA23_AddNewCommentOwner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>email2casecsm@quintilesims.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CAS_ET19_CSM_NewCaseComment</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA_CSM_AssignedCaseNotification</fullName>
        <description>CAS_WEA_CSM_AssignedCaseNotification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>csm.notification@iqvia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CRM_MC_Workflow_Templates/CAS_ET_CSM_Techno_AssignNotification</template>    </alerts>
    <alerts>
        <fullName>CAS_WEA_CSM_DispatchCaseEscalation</fullName>
        <description>CAS_WEA_CSM_DispatchCaseEscalation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>csm.notification@iqvia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CRM_MC_Workflow_Templates/CAS_ET_CSM_Techno_EscalationNotification</template>
    </alerts>
    <alerts>
        <fullName>CAS_WEA19_RnD</fullName>
        <description>CAS_WEA19_RnD</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>email2casecsm@quintilesims.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CAS_ET02_CSM_Standard_Case_Auto_Response_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>CAS_WFU01_CSM_ACN_PrefillLOS</fullName>
        <description>CSM-ACN-S-0139 - Prefill LOS with value DATA</description>
        <field>LOS__c</field>
        <literalValue>DATA</literalValue>
        <name>CAS_WFU01_CSM_ACN_PrefillLOS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU01_CSM_AutoCheckFCR</fullName>
        <description>CSM-ACN-S-333 - Check FCR if closed date - Created Date &lt;=1</description>
        <field>FirstCallResolution__c</field>
        <literalValue>1</literalValue>
        <name>CAS_WFU01_CSM_AutoCheckFCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU01_CSM_CheckReopened</fullName>
        <description>CSM-ACN-S-0005 - update reopened checkbox when case status changes from closed to any other status</description>
        <field>ReOpened__c</field>
        <literalValue>1</literalValue>
        <name>CAS_WFU01_CSM_CheckReopened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU01_CSM_SetInProgress</fullName>
        <description>CSM-ACN-S-0007 - Update status to In progress</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>CAS_WFU01_CSM_SetInProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU01_CSM_UpdateStatus</fullName>
        <description>CSM-ACN-S-0007  - Update status to in progress and substatus to null if scheduled date is met</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>CAS_WFU01_CSM_UpdateStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU01_CSM_UpdateStatusInProgress</fullName>
        <description>CSM-ACN-S-0007 - Update Status to In Progress</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>CAS_WFU01_CSM_UpdateStatusInProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU01_CSM_UpdateStatusToInProgress</fullName>
        <description>CSM-ACN-S-0007 - Update status to &apos;In progress</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>CAS_WFU01_CSM_UpdateStatusToInProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU01_CSM_UpdateSubStatus</fullName>
        <description>CSM-ACN-S-0007 - Set SubStatus to blank</description>
        <field>SubStatus__c</field>
        <name>CAS_WFU01_CSM_UpdateSubStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU02_CSM_SetFieldBlank</fullName>
        <description>CSM-ACN-S-0007 - Set substatus to blank</description>
        <field>SubStatus__c</field>
        <name>CAS_WFU02_CSM_SetFieldBlank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU02_CSM_UpdateOpeningDate</fullName>
        <description>CSM-ACN-S0005 - Workflow action to update the Re-opened date of a case</description>
        <field>ReOpenDate__c</field>
        <formula>TODAY()</formula>
        <name>CAS_WFU02_CSM_UpdateOpeningDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU02_CSM_UpdateStatus</fullName>
        <description>CSM-ACN-S-0007 - Update status to &apos;In Progress</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>CAS WFU02 CSM UpdateStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU02_CSM_UpdateSubStatusToBlank</fullName>
        <description>CSM-ACN-S-0007 - Update substatus to blank</description>
        <field>SubStatus__c</field>
        <name>CAS_WFU02_CSM_UpdateSubStatusToBlank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU02_CSM_UpdateSubstatus</fullName>
        <description>CSM-ACN-S-0007 - Update substatus to NULL id scheduled date has been met</description>
        <field>SubStatus__c</field>
        <name>CAS WFU02 CSM UpdateSubstatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU03_CSM_UpdateCaseOpener</fullName>
        <description>CSM-ACN-S-0005 - Workflow action to update case re opener</description>
        <field>ReOpener__c</field>
        <formula>LastModifiedBy.FirstName  &amp; &apos;&apos; &amp;  LastModifiedBy.LastName</formula>
        <name>CAS_WFU03_CSM_UpdateCaseOpener</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU04_CSM_UpdateFirstCallResolution</fullName>
        <description>CSM-ACN-S0032 - Update First Call Resolution when case is created with status closed</description>
        <field>FirstCallResolution__c</field>
        <literalValue>1</literalValue>
        <name>CAS_WFU04_CSM_UpdateFirstCallResolution</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU07_CSM_CheckIsThisASpecialHan</fullName>
        <description>CSM - ACN - S0062</description>
        <field>IsThisASpecialHandlingClient__c</field>
        <literalValue>1</literalValue>
        <name>CAS_WFU07_CSM_CheckIsThisASpecialHan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU13</fullName>
        <description>CSM-ACN-S-0062 - Field update that will set the field &quot;Is this a Special Handling Client?&quot; in Case to TRUE.</description>
        <field>IsThisASpecialHandlingClient__c</field>
        <literalValue>1</literalValue>
        <name>CAS_WFU13_CSM_SetChkboxValTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU14_CSM_UpdateAutoCloosedToCheck</fullName>
        <description>CSM - ACN -S0127 - Update AutoClose To Checked</description>
        <field>AutoClosed__c</field>
        <literalValue>1</literalValue>
        <name>CAS_WFU14_CSM_UpdateAutoCloosedToCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU15_CSM_CaseFromResolvedToCloased</fullName>
        <description>CSM - ACN - S-0003 - Case closed five days after last update</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>CAS_WFU15_CSM_CaseFromResolvedToCloased</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU18_CSM_NewMilestoneStatustoSt</fullName>
        <description>CSM - ACN - S-0014 - New Milestone Status to Stopped</description>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Stopped</literalValue>
        <name>CAS_WFU18_CSM_NewMilestoneStopped</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU19_CSM_NewMilestoneOngoing</fullName>
        <description>CSM - ACN - S-0014 - Next Milestone Status Ongoing</description>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Ongoing</literalValue>
        <name>CAS_WFU19_CSM_NewMilestoneOngoing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU20_CSM_NewMilestone2Hours</fullName>
        <description>CSM - ACN - S-0014 -  Next Milestone status Remaining 2 hours</description>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Remaining 2 hours</literalValue>
        <name>CAS_WFU20_CSM_NewMilestone2Hours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU21_CSM_NewMilestone8Hours</fullName>
        <description>CSM - ACN - S-0014 - Next milestone status Remaining 8 hours</description>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Remaining 8 hours</literalValue>
        <name>CAS_WFU21_CSM_NewMilestone8Hours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU22_CSM_NewMilestone16Hours</fullName>
        <description>CSM - ACN - S-0014 - Next Milestone status 16 hours</description>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Remaining 16 hours</literalValue>
        <name>CAS_WFU22_CSM_NewMilestone16Hours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU23_CSM_NewMilestoneReached</fullName>
        <description>CSM - ACN- S-0014 - Next Milestone status Reached</description>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Reached</literalValue>
        <name>CAS_WFU23_CSM_NewMilestoneReached</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU24_CSM_NewMilestoneCompleted</fullName>
        <description>CSM - ACN - S-0014 - Next milestone status Completes</description>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Completed</literalValue>
        <name>CAS_WFU24_CSM_NewMilestoneCompleted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU25_CSM_CreateChildCase</fullName>
        <field>CreateChildCase__c</field>
        <literalValue>0</literalValue>
        <name>CAS_WFU25_CSM_CreateChildCase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU26_UpdateCaseModifiedDate</fullName>
        <field>CaseOwnerModifiedDate__c</field>
        <formula>NOW()</formula>
        <name>CAS_WFU26_UpdateCaseModifiedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU34_UncheckFCR</fullName>
        <description>CSM - ACN - Uncheck FCR</description>
        <field>FirstCallResolution__c</field>
        <literalValue>0</literalValue>
        <name>CAS_WFU34_UncheckFCR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU35_Update_Survey_Sent_Date</fullName>
        <description>CSM - Qualtric Integration</description>
        <field>Update_Survey_Sent_Date__c</field>
        <literalValue>1</literalValue>
        <name>CAS_WFU35_Update_Survey_Sent_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU40_CSM_NewMilestone2DaysRemaining</fullName>
        <field>NextMilestoneStatus__c</field>
        <literalValue>2 days remaining</literalValue>
        <name>CAS_WFU40_CSM_NewMilestone2DaysRemaining</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU41_CSM_NewMilestone1DayRemaining</fullName>
        <field>NextMilestoneStatus__c</field>
        <literalValue>1 day remaining</literalValue>
        <name>CAS_WFU41_CSM_NewMilestone1DayRemaining</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU42_CSM_NewMilestoneBreached</fullName>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Breached</literalValue>
        <name>CAS_WFU42_CSM_NewMilestoneBreached</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU43_CSM_UpdateCSATSentInCase</fullName>
        <field>CSAT_Sent__c</field>
        <formula>1</formula>
        <name>CAS_WFU43_CSM_UpdateCSATSentInCase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU_CSM_MilestoneViolationUpdate</fullName>
        <field>Milestone_Violation__c</field>
        <literalValue>1</literalValue>
        <name>CAS_WFU_CSM_MilestoneViolationUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAS_WFU_CSM_ResolutionPlanFailed</fullName>
        <field>CaseOriginatorName__c</field>
        <formula>&quot;Resolution Plan Failed&quot;</formula>
        <name>CAS_WFU_CSM_ResolutionPlanFailed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Status_Milestone_Update</fullName>
        <field>NextMilestoneStatus__c</field>
        <literalValue>Remaining 2 hours</literalValue>
        <name>Next Status Milestone Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Survey_Sent_Date</fullName>
        <field>Update_Survey_Sent_Date__c</field>
        <literalValue>1</literalValue>
        <name>Update Survey Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Rejected_for_on_key</fullName>
        <description>Update status to Rejected for on key</description>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update status to Rejected for on key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_approved_for_on_key</fullName>
        <description>Update status to approved for on key</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update status to approved for on key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WFU01_CSM_ACN_UpdateCaseStatusToClosed</fullName>
        <description>CSM-ACN-S-0136 - Field Update that will set status to closed if workflow criterias are met</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>WFU01_CSM_ACN_UpdateCaseStatusToClosed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>CAS_OB02_CSM_QualtricsEmailOutboud_RnD</fullName>
        <apiVersion>43.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_3QQZqAd1beEKH1H&amp;u=UR_4NkZvLn7k6XtM9f&amp;t=OC_a4OjHd9qxWZPWGl&amp;b=imshealth</endpointUrl>
        <fields>AccountId</fields>
        <fields>CaseNumber</fields>
        <fields>ContactId</fields>
        <fields>CreatedDate</fields>
        <fields>Description</fields>
        <fields>Id</fields>
        <fields>Resolution__c</fields>
        <fields>Subject</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>m.thomas@fr.imshealth.com.qi</integrationUser>
        <name>CAS_OB02_CSM_QualtricsEmailOutboud_RnD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>CAS_OB03_CSM_QualtricsEmailOutboud_TechO</fullName>
        <apiVersion>44.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_eYjgahRAdpHE0hn&amp;u=UR_4NkZvLn7k6XtM9f&amp;t=OC_2b1qsPMjqSvxXzT&amp;b=imshealth</endpointUrl>
        <fields>AccountId</fields>
        <fields>CaseNumber</fields>
        <fields>ContactId</fields>
        <fields>CreatedDate</fields>
        <fields>Description</fields>
        <fields>Id</fields>
        <fields>Resolution__c</fields>
        <fields>SLA_Policies__c</fields>
        <fields>Subject</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>ci_deployment@iqvia.com</integrationUser>
        <name>CAS_OB03_CSM_QualtricsEmailOutboud_TechO</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CAS_CSM_TECH_FromUserToUser</fullName>
        <actions>
            <name>CAS_WEA_CSM_AssignedCaseNotification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Tigger a work flow based on change of owner from Person to person</description>
        <formula>AND(
ISCHANGED(OwnerId),
LEFT(PRIORVALUE(OwnerId), 3) &lt;&gt; &apos;00G&apos;,
LEFT(OwnerId, 3) = &apos;005&apos;,
RecordTypeName__c =&apos;TechnologyCase&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR01_CSM_CheckReopened</fullName>
        <actions>
            <name>CAS_WFU01_CSM_CheckReopened</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_WFU02_CSM_UpdateOpeningDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_WFU03_CSM_UpdateCaseOpener</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CSM-ACN-S-0005 - when case status changes from closed to any other status this checkbox is checked</description>
        <formula>ISPICKVAL( PRIORVALUE(Status) , &apos;Closed&apos;) &amp;&amp; NOT(ISPICKVAL(Status , &apos;Closed&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR02_CSM_CaseOwnerNotification</fullName>
        <actions>
            <name>CAS_WEA01_CSM_ACN_CaseEscalation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CSM-ACN-S-0007 - Owner change notification</description>
        <formula>AND ( ISCHANGED(OwnerId),IsEscalated=true, RecordTypeName__c =&apos;DATACase&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR03_CSM_CheckedWhenCaseIsCreatedAsClosed</fullName>
        <actions>
            <name>CAS_WFU04_CSM_UpdateFirstCallResolution</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed,Resolved</value>
        </criteriaItems>
        <description>CSM-ACN-S-0017 - When case is created with status closed this checkbox is checked</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR11_CSM_UpdateResolvedByUserWhenCaseStatusIsResolved</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>CSM - ACN - S0023 - Update Resolved By User When Case Status Is Resolved</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR13_CSM_CheckSpecialHandlingClient</fullName>
        <actions>
            <name>CAS_WFU13</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.IsThisASpecialHandlingClient__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>CSM-ACN-S-0062 - Workflow that will set checkbox &quot;Is this a Special Handling Client?&quot; in Case object to TRUE if field &quot;Is this a Special Handling Client?&quot; in Account object is checked</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR15_CSM_RnD waiting feedback from customer</fullName>
        <active>false</active>
        <description>CSM_ACN_S0001_R&amp;D waiting feedback from customer</description>
        <formula>AND(ISPICKVAL( Status , &apos;Waiting for&apos;),     
ISPICKVAL( SubStatus__c , &apos;Customer&apos;),            

OR(RecordType.DeveloperName = &apos;ActivityPlan&apos;,     RecordType.DeveloperName = &apos;RandDCase&apos;, 

AND( RecordType.DeveloperName = &apos;DATACase&apos; , 

OR(  ISPICKVAL( SendAutomaticAcknowledgmentEmail__c , &apos;Yes&apos;) , ISPICKVAL(SendAutomaticAcknowledgmentEmail__c , &apos;Yes I overwrite Account choice&apos;) ) ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAS_WEA09_CSM_ACN_R_D_waiting_feedback_from_customer</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WR19_CSM_SetStatusToClosed</fullName>
        <active>false</active>
        <description>CSM-ACN-S-0136 - Workflow rule that will set Status to value &apos;Closed&apos; if criterias are met.</description>
        <formula>AND(      OR(        RecordType.DeveloperName=&apos;ActivityPlan&apos;,        RecordType.DeveloperName=&apos;RandDCase&apos;        ),  ISPICKVAL( Status , &quot;Resolved&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAS_WFU14_CSM_UpdateAutoCloosedToCheck</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>WFU01_CSM_ACN_UpdateCaseStatusToClosed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>    
    <rules>
        <fullName>CAS_WR23_CSM_AutoPopulateLOS</fullName>
        <actions>
            <name>CAS_WFU01_CSM_ACN_PrefillLOS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>DATACase</value>
        </criteriaItems>
        <description>CSM-ACN-S-0139 - Prefill LOS with value &quot;DATA&quot; If record type DATACase.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR27_CSM_StoppedSince</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>R&amp;D - Activity Plan Case</value>
        </criteriaItems>
        <description>CSM - ACN - S-0016 - Stopped Since (5 0r 10)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAS_WEA16_CSM_ACN_StoppedSince</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.StopStartDate</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WR28_CSM_StoppedSince</fullName>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>DATA Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>TechnologyCase</value>
        </criteriaItems>
        <description>CSM - ANC - S-0016 - Stopped Since</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAS_WEA16_CSM_ACN_StoppedSince</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.StopStartDate</offsetFromField>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WR29_CSM_CaseFromResolvedToClosed</fullName>
        <actions>
            <name>CAS_WFU15_CSM_CaseFromResolvedToCloased</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CSM- ACN - S-0003 - Case closed 5 days after last modified</description>
        <formula>AND( RecordType.DeveloperName  = &apos;TechnologyCase&apos;, ISPICKVAL( Status , &apos;Resolved&apos;),  NoofBusinessDaysSincemodified__c &gt; 5)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR31_CSM_SetStatusField</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.ScheduledDate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed,Resolved with Customer</value>
        </criteriaItems>
        <description>CSM-ACN-S-0007 - WR that sets Status to &apos;In progress&apos; &amp; substatus to blank if scheduled date is reached.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAS_WFU01_CSM_SetInProgress</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>CAS_WFU02_CSM_SetFieldBlank</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.ScheduledDate__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WR31_CSM_SetStatusFieldBasedOnScheduledDate</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.ScheduledDatetime__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed,Resolved with Customer</value>
        </criteriaItems>
        <description>CSM-ACN-S-0183 - WR that sets Status to &apos;In progress&apos; &amp; substatus to blank if scheduled date is reached.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAS_WFU01_CSM_SetInProgress</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>CAS_WFU02_CSM_SetFieldBlank</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.ScheduledDatetime__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WR33_CSM_NewMilestonStatustoStopped</fullName>
        <actions>
            <name>CAS_WFU18_CSM_NewMilestoneStatustoSt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsStopped</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>CSM - ACN - S-0014 - New Milestone Status to Stopped</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR35_CSM_CreateChildCase</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.CreateChildCase__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CAS_WFU25_CSM_CreateChildCase</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAS_WR36_CSM_CreateChildCase</fullName>
        <actions>
            <name>CAS_WFU26_UpdateCaseModifiedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR( AND( ISCHANGED( OwnerId ) ) ,

 AND( ISNEW() ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR39_CSM_RnD</fullName>
        <actions>
            <name>CAS_WEA19_RnD</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( OR( RecordType.DeveloperName  = &apos;ActivityPlan&apos;, RecordType.DeveloperName  = &apos;RandDCase&apos;) , Contact.EmailOptOut__c  = False, OR( AND( Account.SendAutomaticCaseAcknowledgmentEmail__c  = True, ISPICKVAL(SendAutomaticAcknowledgmentEmail__c , &apos;Yes&apos;)) , OR( ISPICKVAL(SendAutomaticAcknowledgmentEmail__c  , &apos;Yes I overwrite Account choice&apos;)) ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR40_CSM_CustomerReply</fullName>
        <actions>
            <name>CAS_WEA20_CustomerReply</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>If customer reply to mail after closure.</description>
        <formula>AND(Source:EmailMessage.Incoming = TRUE,  ISPICKVAL(Status, &quot;Closed&quot;), RecordType.DeveloperName = &apos;TechnologyCase&apos;, ((Now() - ClosedDate)*24*60*60) &gt; 1)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR42_CSM_AutoChecFCRChkbx</fullName>
        <actions>
            <name>CAS_WFU01_CSM_AutoCheckFCR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CSM-ACN-S-333 - Check chkbox First call resolution is difference between created date and closed date&lt;=1</description>
        <formula>AND ( 
VALUE(Text(( ClosedDate - CreatedDate) * 24)) &lt;= 1, 
ISBLANK(ReOpenDate__c), 
FirstCallResolution__c = false 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR50_UncheckFCR</fullName>
        <actions>
            <name>CAS_WFU34_UncheckFCR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CSM - ACN - Uncheck FCR if case status is &quot;In Progress&quot;</description>
        <formula>AND( 
FirstCallResolution__c = true, 
NOT(ISBLANK(ReOpenDate__c)) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR53_CSM_StatusFromNewToInProgress</fullName>
        <actions>
            <name>CAS_WFU01_CSM_SetInProgress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>TechnologyCase</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR58_CSM_CSATTechnoOptionB</fullName>
        <actions>
            <name>CAS_WFU35_Update_Survey_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_WFU43_CSM_UpdateCSATSentInCase</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_WOM01_CSM_QualtricsEmailOutboud</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>CSM S-0211 CSAT Step 2 - Qualtrics Interface_techno</description>
        <formula>AND( 				ISPICKVAL( Status , &apos;Closed&apos;),  				RecordType.DeveloperName = &apos;TechnologyCase&apos;,  				INCLUDES(Account.C_SAT_Survey_Option__c, &apos;TS Option B&apos;),  				ISBLANK(TEXT( SLA_Policies__c )), CONTAINS(InitialQueue__c, &quot;T1&quot;),  				NOT(ISBLANK(Contact.Email)),  				OR(TODAY() - IF(ISBLANK(Account.CSAT_Frequency_For_TechSol__c),$Setup.C_SAT_Default_Frequency__c.Default_Frequency_For_Techno__c,Account.CSAT_Frequency_For_TechSol__c) &gt;= Contact.C_SAT_Last_Submitted_Date__c,  							ISBLANK(Contact.C_SAT_Last_Submitted_Date__c) 						), 				Contact.Survey_Opt_Out__c = false )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR59_CSM_CSATRnDOptionB</fullName>
        <actions>
            <name>CAS_WFU35_Update_Survey_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_WFU43_CSM_UpdateCSATSentInCase</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_OB02_CSM_QualtricsEmailOutboud_RnD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>CSM S-0311 CSAT Step 2 - Qualtrics Interface_R&amp;D</description>
        <formula>OR( AND(  ISPICKVAL( Status , &apos;Closed&apos;) , RecordType.DeveloperName = &apos;RandDCase&apos;, INCLUDES(Account.C_SAT_Survey_Option__c, &apos;R&amp;D Option B&apos;), NOT(ISBLANK(Contact.Email)), NOT(ISPICKVAL(LOS__c,&quot;Registry Support&quot;)), NOT(ISPICKVAL(LOS__c,&quot;Emergency&quot;)), NOT(CONTAINS(SubType2__c,&quot;Administration&quot;)), NOT(CONTAINS(SubType2__c,&quot;Novella&quot;)), Contact.Email &lt;&gt; &quot;infosarioprod@service-now.com&quot;, NOT(ISPICKVAL(SubStatus__c ,&quot;Non-Actionable&quot;)), OR(TODAY() - IF(ISBLANK(Account.CSAT_Frequency_For_R_D__c),$Setup.C_SAT_Default_Frequency__c.Default_Frequency_For_R_D__c,Account.CSAT_Frequency_For_R_D__c) &gt;= Contact.C_SAT_Last_Submitted_Date__c, ISBLANK(Contact.C_SAT_Last_Submitted_Date__c) ), Contact.Survey_Opt_Out__c = false ), AND( ISPICKVAL( Status , &apos;Closed&apos;), RecordType.DeveloperName = &apos;RandDCase&apos;, INCLUDES(Account.C_SAT_Survey_Option__c, &apos;R&amp;D Option B&apos;), ISPICKVAL(LOS__c,&quot;Registry Support&quot;), SubType1__c =&quot;ACS_NSQIP_Bariatric&quot;, SubType2__c &lt;&gt;&quot;Administration&quot;, Contact.Email &lt;&gt; &quot;infosarioprod@service-now.com&quot;, NOT(ISPICKVAL(SubStatus__c ,&quot;Non-Actionable&quot;)), OR(TODAY() - IF(ISBLANK(Account.CSAT_Frequency_For_R_D__c),$Setup.C_SAT_Default_Frequency__c.Default_Frequency_For_R_D__c,Account.CSAT_Frequency_For_R_D__c) &gt;= Contact.C_SAT_Last_Submitted_Date__c, ISBLANK(Contact.C_SAT_Last_Submitted_Date__c) ), Contact.Survey_Opt_Out__c = false ), AND( ISPICKVAL( Status , &apos;Closed&apos;), RecordType.DeveloperName = &apos;RandDCase&apos;, INCLUDES(Account.C_SAT_Survey_Option__c, &apos;R&amp;D Option B&apos;), ISPICKVAL(LOS__c,&quot;Registry Support&quot;), NOT(CONTAINS(SubType1__c,&quot;ACS&quot;)), SubType2__c &lt;&gt;&quot;Administration&quot;, Contact.Email &lt;&gt; &quot;infosarioprod@service-now.com&quot;, NOT(ISPICKVAL(SubStatus__c ,&quot;Non-Actionable&quot;)), OR(TODAY() - IF(ISBLANK(Account.CSAT_Frequency_For_R_D__c),$Setup.C_SAT_Default_Frequency__c.Default_Frequency_For_R_D__c,Account.CSAT_Frequency_For_R_D__c) &gt;= Contact.C_SAT_Last_Submitted_Date__c, ISBLANK(Contact.C_SAT_Last_Submitted_Date__c) ), Contact.Survey_Opt_Out__c = false ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR60_CSM_CSATTechnoOptionD</fullName>
        <actions>
            <name>CAS_WFU35_Update_Survey_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_WFU43_CSM_UpdateCSATSentInCase</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_OB03_CSM_QualtricsEmailOutboud_TechO</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>AND( 				ISPICKVAL( Status , &apos;Closed&apos;),  				RecordType.DeveloperName = &apos;TechnologyCase&apos;,  				INCLUDES(Account.C_SAT_Survey_Option__c, &apos;TS Option D&apos;),  				ISBLANK(TEXT( SLA_Policies__c )), CONTAINS(InitialQueue__c, &quot;T1&quot;),  				NOT(ISBLANK(Contact.Email)),OR(TODAY() - IF(ISBLANK(Account.CSAT_Frequency_For_TechSol__c), $Setup.C_SAT_Default_Frequency__c.Default_Frequency_For_Techno__c,Account.CSAT_Frequency_For_TechSol__c) &gt;= Contact.C_SAT_Last_Submitted_Date__c,  							ISBLANK(Contact.C_SAT_Last_Submitted_Date__c) 						), 				Contact.Survey_Opt_Out__c = false )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR61_CSM_CSATTechnoCC_OptionD</fullName>
        <actions>
            <name>CAS_WFU35_Update_Survey_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_WFU43_CSM_UpdateCSATSentInCase</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAS_OB03_CSM_QualtricsEmailOutboud_TechO</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>CSAT Step 2 - Qualtrics Interface_techno</description>
        <formula>AND( 
ISPICKVAL( Status , &apos;Closed&apos;),
RecordType.DeveloperName =&apos;TechnologyCase&apos;,
NOT(ISBLANK(AccountId)),
NOT(ISBLANK(TEXT( SLA_Policies__c ))),
OR( INCLUDES(Account.C_SAT_Survey_Option__c, &apos;TS Option D&apos;)),
OR( CONTAINS( InitialQueue__c, &quot;T1&quot;), CONTAINS(InitialQueue__c, &quot;T2&quot;)),
NOT(ISBLANK(Contact.Email)),
Contact.Survey_Opt_Out__c = false )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR_CSM_CaseDispatchQueue</fullName>
        <actions>
            <name>CAS_WEA_CSM_DispatchCaseEscalation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Check when case is send into a queue and send email to member (Techno Only)</description>
        <formula>AND ( OR (ISCHANGED(OwnerId), 
        AND(ISNEW(),ISPICKVAL(Origin,&quot;Customer portal&quot;))), 
        LEFT(OwnerId, 3) &lt;&gt; &apos;005&apos;, 
        RecordTypeName__c =&apos;TechnologyCase&apos; 
        )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CAS_WR_CSM_TECH_FromUserToUser</fullName>
        <actions>
            <name>CAS_WEA_CSM_AssignedCaseNotification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Tigger a work flow based on change of owner from Person to person</description>
        <formula>AND( 
				ISCHANGED(OwnerId),
				/*LEFT(PRIORVALUE(OwnerId), 3) &lt;&gt; &apos;00G&apos;,*/
				LEFT(OwnerId, 3) = &apos;005&apos;,
				RecordTypeName__c =&apos;TechnologyCase&apos;,
			 $User.Id != OwnerId
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	    <rules>
        <fullName>CAS_WR_CSM_Created_From_Email2case_Queue</fullName>
        <actions>
            <name>CAS_WEA_CSM_DispatchCaseEscalation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send queue members notification when case get created from email2case (Techno)</description>
        <formula>and(
				(LastModifiedDate - CreatedDate)*86400 &lt; 2  ,
				LEFT(OwnerId,3) =&quot;00G&quot;,
				RecordTypeName__c =&apos;TechnologyCase&apos;
				)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
