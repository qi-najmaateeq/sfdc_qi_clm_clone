<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ASN_EA01_OWF_MC_Pending_Assignment_Notification</fullName>
        <description>ASN_EA01_OWF_MC_Pending_Assignment_Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>OWF/ASN_ET01_OWF_New_Assignment_Pending_Your_Review</template>
    </alerts>
</Workflow>
