<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BNF_stuck_in_SAP_PENDING_status_Notification</fullName>
        <description>BNF stuck in SAP PENDING status Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/BNF_stuck_in_SAP_PENDING_status_Notification</template>
    </alerts>
    <alerts>
        <fullName>MIBNF_Approval_Notification</fullName>
        <description>MIBNF Approval  Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Current_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>First_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Submitter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MIBNFApprovedBNFrecordrejected</template>
    </alerts>
    <alerts>
        <fullName>Send_notification_of_MIBNF_rejection</fullName>
        <description>Send notification of MIBNF rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>First_RA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Submitter_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MIBNF_Rejected_VF_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Rejection_Reason</fullName>
        <field>Rejection_Reasons__c</field>
        <name>Clear Rejection Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LO_Accepted</fullName>
        <field>BNF_Status__c</field>
        <literalValue>LO Accepted</literalValue>
        <name>LO Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LO_Rejected</fullName>
        <field>BNF_Status__c</field>
        <literalValue>LO Rejected</literalValue>
        <name>LO Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RA_Accepted</fullName>
        <field>BNF_Status__c</field>
        <literalValue>RA Accepted</literalValue>
        <name>RA Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RA_Rejected</fullName>
        <field>BNF_Status__c</field>
        <literalValue>RA Rejected</literalValue>
        <name>RA Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Accepted</fullName>
        <field>BNF_Status__c</field>
        <literalValue>Accepted</literalValue>
        <name>Update Status : Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_New</fullName>
        <field>BNF_Status__c</field>
        <literalValue>New</literalValue>
        <name>Update Status : New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Rejected</fullName>
        <field>BNF_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status : Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_SAP_Contract_Confirmed</fullName>
        <field>BNF_Status__c</field>
        <literalValue>SAP Contract Confirmed</literalValue>
        <name>Update Status : SAP Contract Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_SAP_Contract_Confirmedv1</fullName>
        <field>BNF_Status__c</field>
        <literalValue>SAP Contract Confirmed</literalValue>
        <name>Update Status : SAP Contract Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Submitted</fullName>
        <field>BNF_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status : Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitter_Email</fullName>
        <description>Store email address of user who submitted the BNF</description>
        <field>Submitter_Email__c</field>
        <formula>$User.Email</formula>
        <name>Update Submitter Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>IBNF is stuck in SAP PENDING Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>MIBNF_Component__c.BNF_Status__c</field>
            <operation>equals</operation>
            <value>SAP Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BNF_stuck_in_SAP_PENDING_status_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>MIBNF_Component__c.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
