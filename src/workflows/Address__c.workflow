<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Desc</fullName>
        <field>Description__c</field>
        <formula>&quot;Temp Value&quot;</formula>
        <name>Desc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Remove Temp Address</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Address__c.Marked_For_Deletion__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Remove Temp Address</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Desc</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Address__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
