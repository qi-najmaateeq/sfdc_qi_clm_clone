<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CTR_WFU01_CRM_Triage_Assignment_BD_Statu</fullName>
        <field>Status</field>
        <literalValue>Assigned - Not Started</literalValue>
        <name>CTR_WFU01_CRM_Triage Assignment BD Statu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTR_WFU01_PRM_RenewalDate</fullName>
        <description>PRM - ACN - Update renewal date on contract</description>
        <field>Renewal_date__c</field>
        <formula>IF(CONTAINS(Product__r.Name, &quot;OCE Sales&quot;), EndDate - 60 , EndDate - 30)</formula>
        <name>CTR_WFU01_PRM_RenewalDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTR_WFU2_CRM_Field_Update_Global_PU</fullName>
        <description>3233</description>
        <field>Date_Global_P_U_entered__c</field>
        <formula>TODAY()</formula>
        <name>CTR_WFU2_CRM_Field Update Global PU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CTR_WR01_CRM_Bid Grid - QC Budget Analyst Assigned</fullName>
        <active>true</active>
        <description>CRM- ESPSFDCQI-3233, Email sent to the budget analyst assigned to QC</description>
        <formula>AND(
  OR(
    $Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
    !$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c
  ),
  !(ISBLANK(Grid_QC_Assigned_to__c ))   
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR01_PRM_UpdateRenewalDate</fullName>
        <actions>
            <name>CTR_WFU01_PRM_RenewalDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>PRM - ACN - Workflow to update the renewal date as the formula field (renewal date) cannot be used in process builder</description>
        <formula>AND(RecordType.DeveloperName == &apos;PRM_Contract&apos;, NOT(ISNULL(Product__c)),NOT(ISNULL(EndDate)), 
OR(ISNEW(),ISCHANGED(Product__c) , ISCHANGED(EndDate)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR02_CRM_Cardiac safety contract stage change</fullName>
        <active>true</active>
        <description>CRM- ESPSFDCQI-3233,  sends email to CSS mailbox and contract analyst only if contract has been identified as a CSS contract and the analyst field has been populated.</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
!(ISBLANK( CSS_CD_Contract_Analyst__c )), 
ISPICKVAL( CSS_CD_Contract_Involvement__c , &apos;Yes&apos;), 
OR( 
ISPICKVAL( Status,&apos;Contract Executed&apos;), 
ISPICKVAL( Status,&apos;Negotiation Terminated&apos;), 
ISPICKVAL( Status,&apos;On Hold&apos;), 
ISPICKVAL( Status,&apos;Under Review - PM&apos;), 
ISPICKVAL( Status,&apos;Under Review - Client&apos;), 
ISPICKVAL( Status,&apos;Under Review - Legal&apos;), 
ISPICKVAL( Status,&apos;Under Review - Finance&apos;), 
ISPICKVAL( Status,&apos;Insufficient Information Received/Waiting ICOF from PM&apos;) 
) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR03_CRM_Cardiac Safety involvement alert</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, alert sent to CSS-Contract CRM notices mailbox when a contract has been identified as having Cardiac Safety involvement</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
ISPICKVAL( CSS_CD_Contract_Involvement__c , &apos;Yes&apos;), 
!(ISBLANK( CSS_CD_Contract_Analyst__c )), 
OR( 
ISPICKVAL( Status,&apos;Contract Executed&apos;), 
ISPICKVAL( Status,&apos;Negotiation Terminated&apos;), 
ISPICKVAL( Status,&apos;On Hold&apos;), 
ISPICKVAL( Status,&apos;Under Review - PM&apos;), 
ISPICKVAL( Status,&apos;Under Review - Client&apos;), 
ISPICKVAL( Status,&apos;Under Review - Legal&apos;), 
ISPICKVAL( Status,&apos;Under Review - Finance&apos;), 
ISPICKVAL( Status,&apos;Insufficient Information Received/Waiting ICOF from PM&apos;) 
) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR04_CRM_Contract QIP loaded notification</fullName>
        <active>true</active>
        <description>CRM- ESPSFDCQI-3233, email sent when QIP loaded field checked</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
 ISPICKVAL(QIP_Loaded__c , &apos;Yes&apos;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR05_CRM_ECD %28Ph 1%2FPK-PD%29 involvement alert</fullName>
        <active>true</active>
        <description>CRM- ESPSFDCQI-3233, alert sent to ECD_Ph1_PKPD_Global Contracts-Change_Orders mailbox when a contract has been identified as having ECD (Ph1/PK-PD)involvement</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
 ISPICKVAL(ECD_PK_PD_Stats_Contract_Involvement__c, &apos;Yes&apos;),
 !(ISBLANK( ECD_ph_1_PK_PD_Contract_Analyst__c)), 
OR(
  ISPICKVAL(Status, &apos;Contract Executed&apos;),
ISPICKVAL(Status, &apos;Negotiation Terminated&apos;),
ISPICKVAL(Status, &apos;On Hold&apos;),
ISPICKVAL(Status, &apos;In Development&apos;),
ISPICKVAL(Status, &apos;Under Review - PM&apos;),
ISPICKVAL(Status, &apos;Under Review - Client&apos;),
ISPICKVAL(Status, &apos;Under Review - Legal&apos;),
ISPICKVAL(Status, &apos;Under Review - Finance&apos;),
ISPICKVAL(Status, &apos;Insufficient Information Received/Waiting ICOF from PM&apos;)
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR06_CRM_Contract alert when Global P%2FU Field populated</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, email sent to the contract owner when the Global P/U field is populated by the PFM for the first time. Also populated the &apos;Date Global p/U entered&apos; field</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
!(ISBLANK( Total_Pick_up__c)) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR07_CRM_Contract signed alert</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233,email sent to Contract Owner, Q Project Manager - Contact and Q Project Finance Manager when the customer signed date has been entered</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
!(ISBLANK(CustomerSignedDate)), 
!ISPICKVAL( Division_Business_Unit__c,&apos;Lab&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR08_CRM_CRO Preliminary LOI%2FSUWO expiration alerts</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, sends email to Q contract manager 30 days, 15 days and 5 days prior to contract end date - only on CRM Preliminary contracts</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
if( RecordType.Name == &apos;Preliminary Agreement - GBO&apos;,true,false)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CTR_WR09_CRM_CRO%3A ATP expiry alert</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, email sent to PL, CA and PFM when expiry within 30 days</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
if( RecordType.Name == &apos;Preliminary Agreement - GBO&apos;,true,false) ,
OR(
ISPICKVAL( Original_signed_agreement_location__c,&apos;QAHM&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QBAN&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QBEJ&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QBKK&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QDLN&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QHAN&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QHCM&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QHKG&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QJKT&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QKUL&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QMEL&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QMNL&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QSEL&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QSHN&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QSNG&apos;), 
ISPICKVAL( Original_signed_agreement_location__c,&apos;QSYD&apos;),
ISPICKVAL( Original_signed_agreement_location__c,&apos;QTPE&apos;)
)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CTR_WR10_CRM_ECD contract stage change</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, sends email to ECD mailbox and contract analyst only if contract has been identified as a ECD contract and the analyst field has been populated</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
 !(ISBLANK( ECD_ph_1_PK_PD_Contract_Analyst__c )),
 ISPICKVAL(ECD_PK_PD_Stats_Contract_Involvement__c, &apos;Yes&apos;) 

)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR11_CRM_Email Alert on Auto Contract Creation</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, Email Alert on Auto Contract Creation</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
AND( ISNEW(), (RecordType.Name = &quot;Generic Contract&quot;), ISPICKVAL(Status, &quot;Pending Assignment&quot;) )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR12_CRM_Email Alert to Project Finance Manager</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, Alert when the Status is changed to = Budget at Customer for the first time with Confidence at High and QIP Loaded? = YES .</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
AND( ISCHANGED(Status), ISPICKVAL(Status, &apos;Budget at Customer for Review&apos;), ISPICKVAL(QIP_Loaded__c,&quot;Yes&quot;), ISPICKVAL(Confidence_in_Approval_of_Budget_Draft__c,&quot;High&quot;), Entered_Budget_at_Customer_Review__c = FALSE )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR13_CRM_PFM and PM alert when a contract unsigned status field is populated</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, email sent to the PFM and PM when the contract is either created or updated and the unsigned status is populated.</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
!ISPICKVAL(Unsigned_Status__c, &apos;&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR14_CRM_PFM and PM alert when contract status changes</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, email sent to the PFM and PM when the contract status is - In development, Under Review, ready to execute, contract executed,on hold, negotiation terminated</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
OR( 
ISPICKVAL( Status,&apos;Contract Executed&apos;), 
ISPICKVAL( Status,&apos;Negotiation Terminated&apos;), 
ISPICKVAL( Status,&apos;On Hold&apos;), 
ISPICKVAL( Status,&apos;Under Review - PM&apos;), 
ISPICKVAL( Status,&apos;Under Review - Client&apos;), 
ISPICKVAL( Status,&apos;Under Review - Legal&apos;), 
ISPICKVAL( Status,&apos;Under Review - Finance&apos;),
ISPICKVAL( Status,&apos;In Development&apos;), 
ISPICKVAL( Status,&apos;Ready to Execute&apos;), 
ISPICKVAL( Status,&apos;Insufficient Information Received/Waiting ICOF from PM&apos;) 
) ,
!ISPICKVAL( Unsigned_Status__c,&apos;&apos;),
!(ISBLANK( IQVIA_Project_Finance_Manager__c)) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR15_CRM_Contracts - PFM and PM alert</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, email sent to the PFM and PM when the contract status is - contract executed</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
ISPICKVAL( Status,&apos;Contract Executed&apos;), 
!ISPICKVAL( Unsigned_Status__c,&apos;&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR16_CRM_Third party vendor PM alert</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, sends an email to the PM when third party vendors involved</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
ISPICKVAL( Third_Party_Vendors_Involved__c,&apos;Yes&apos;),
if( Owner.Full_User_Name__c == &apos;Global Sales Operations&apos;,false,true) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR17_CRM_Third party vendor PM alert when PM named added</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, sends an email to the PM when third party vendors involved and the PM name is added restrospecitvely</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
ISPICKVAL( Third_Party_Vendors_Involved__c,&apos;Yes&apos;), 
!ISBLANK(IQVIA_Project_Finance_Manager__c)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR18_CRM_Time Based - Reminder Email to Upload QIP</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, Email to be sent to the Contract Analyst when a Contract enters the &quot;Budget at Customer Review&quot; stage for the first time, and when the QIP has not been loaded.</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
!Entered_Budget_at_Customer_Review__c,
!ISPICKVAL( QIP_Loaded__c,&apos;Yes&apos;), 
ISPICKVAL( Status,&apos;Budget at Customer for Review&apos;), 
if( RecordType.Name == &apos;Change Order&apos;,true,false),
ISPICKVAL( Budget_Tool__c,&apos;QIP&apos;), 
!ISPICKVAL( Is_this_Contract_a_Ballpark__c,&apos;Yes&apos;), 
ISPICKVAL( Confidence_in_Approval_of_Budget_Draft__c,&apos;High&apos;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CTR_WR19_CRM_Triage Assignment Complete</fullName>
        <actions>
            <name>CTR_WFU01_CRM_Triage_Assignment_BD_Statu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233,  Triage Assignment Complete</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
AND( ISCHANGED(OwnerId), Owner.Full_User_Name__c &lt;&gt; &quot;Global Sales Operations&quot;, ISPICKVAL(Status, &quot;Pending Assignment&quot;) )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR20_CRM_Bid Grid - Budget Analyst Assigned</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, Email Sent to the Contract Owner to Notify them of the BA assigned and the grid due date</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
!ISBLANK(Budget_Analyst_Assigned__c)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR21_CRM_PFM and PM alert when contract status changes to Contract Executed</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, email sent to the PFM and PM when the contract status is - contract executed</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
ISPICKVAL( Status,&apos;Contract Executed&apos;), 
!ISPICKVAL( Unsigned_Status__c,&apos;&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR22_CRM_MSA Consolidation - new MSA</fullName>
        <active>true</active>
        <description>CRM- ESPSFDCQI-3233, email sent to Paula Foster whenever a new MSA is created</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
 BEGINS (RecordType.Name, &apos;Master Service Agreement&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR23_CRM_MSA Moved to Executed</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233, email sent to Paula Foster for MSA consolidation</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
ISPICKVAL( Status,&apos;Contract Executed&apos;), 
(RecordType.Name == &apos;Master Service Agreement&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR24_CRM_Time based - Contract expiry date reminder</fullName>
        <active>true</active>
        <description>CRM-ESPSFDCQI-3233,sends reminder 1 month prior, 14 days prior and 14 days past contract end date - requested by Natasha Raina</description>
        <formula>AND(OR($Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, !$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c), OR(RecordType.DeveloperName == &apos;Change_Order&apos;, RecordType.DeveloperName == &apos;General_Client_Agreement_GBO&apos;,RecordType.DeveloperName == &apos;Master_Service_Agreement&apos;,RecordType.DeveloperName == &apos;Preliminary_Agreement_GBO&apos;,RecordType.DeveloperName == &apos;Work_Order_GBO&apos;), Business_Line_Grouping__c == &apos;Clinical&apos;, OR(ISPICKVAL(Status, &apos;Contract Executed&apos;),  ISPICKVAL(Status, &apos;Contract Executed - Ready to Lock&apos;), ISPICKVAL(Status, &apos;Contract Executed - Locked&apos;)), OR(Owner.Full_User_Name__c == &apos;Natasha Raina&apos;, Owner.Full_User_Name__c == &apos;April Fitzpatrick&apos;, Owner.Full_User_Name__c == &apos;Luba Lazarova&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CTR_WR25_CRM_Project finance alert when MSA marked as executed</fullName>
        <active>false</active>
        <description>CRM-ESPSFDCQI-3233,email sent to project finance global and Julie Meeder when a Master/PPA - MSA record is marked as executed</description>
        <formula>AND( 
OR( 
$Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, 
!$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c 
), 
ISPICKVAL( Status,&apos;Contract Executed&apos;), 
 (RecordType.Name == &apos;Master Service Agreement&apos;),
ISPICKVAL( Specific_Contract_Type__c,&apos;Master Services Agreement&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTR_WR26_CRM-Email Alert%3A Contract Triage Assignment</fullName>
        <active>true</active>
        <description>CRM-MC-ESPSFDCQI-2970</description>
        <formula>AND(OR($Setup.Mulesoft_Integration_Control__c.Is_Mulesoft_User__c, !$Setup.Mulesoft_Integration_Control__c.Ignore_Validation_Rules__c), ISCHANGED(IQVIA_Contract_Manager_Contact__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
