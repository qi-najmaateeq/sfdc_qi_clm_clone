/**
 * This trigger is used for UpdateLIG__e object.
 * version : 1.0
 */
trigger TGR_UpdateLIG on Update_LIG__e (after insert) {
    List<Opportunity_Stage__c> oppStageRecordsToUpsertList = new List<Opportunity_Stage__c>();
    Opportunity_Stage__c oppStage;
    for(Update_LIG__e lig : trigger.new) {
        oppStage = new Opportunity_Stage__c();
        oppStage.Name = lig.Opp_Id__c;
        oppStage.Current_Opp_Stage__c = lig.Current_Opp_Stage__c;
        oppStage.Expected_Opp_Stage__c = lig.Expected_Opp_Stage__c;
        oppStage.Current_Line_of_Buisness__c = lig.Current_Line_of_Buisness__c;
        oppStage.Expected_Line_of_Buisness__c = lig.Expected_Line_of_Buisness__c;
        oppStageRecordsToUpsertList.add(oppStage);
    }
    
    if(oppStageRecordsToUpsertList.size() > 0) {
        upsert oppStageRecordsToUpsertList Name;
    }
}