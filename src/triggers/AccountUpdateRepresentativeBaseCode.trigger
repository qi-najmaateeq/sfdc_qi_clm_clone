trigger AccountUpdateRepresentativeBaseCode on Address__c (after update, after insert, after delete) 
{
    Set <Id> Account_Id_Set = new Set <Id>();
    List <Address__c> Address_Array = new List <Address__c>();
    if (Trigger.isUpdate || Trigger.isInsert)
    {
        Address_Array = Trigger.New;
    }
    else
    {
        Address_Array = Trigger.Old;
    }
    for (Address__c A:Address_Array)
    {
        Account_Id_Set.add(A.Account__c);   
    }
    Account_MDM_Extension.UpdateRepresentativeBaseCode(Account_Id_Set);
}