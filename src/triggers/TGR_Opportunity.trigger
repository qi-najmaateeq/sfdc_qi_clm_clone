/**
 * This trigger is used for opportunity object.
 * version : 1.0
 */
trigger TGR_Opportunity on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(!UTL_ExecutionControl.stopTriggerExecution)
        fflib_SObjectDomain.triggerHandler(DAO_Opportunity.class);
}