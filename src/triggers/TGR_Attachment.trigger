/**
 * This trigger is used for Attachment Object.
 * version: 1.0
 */

trigger TGR_Attachment on Attachment (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    fflib_SObjectDomain.triggerHandler(DAO_Attachment.class);
    
}