/**
 * This trigger is used for ContentDocumentLinkobject.
 * version : 1.0
 */
trigger TGR_ContentDocumentLink on ContentDocumentLink (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    fflib_SObjectDomain.triggerHandler(DAO_ContentDocumentLink.class);

}