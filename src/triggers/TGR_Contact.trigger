/**
 * This trigger is used for Contact object.
 * version : 1.0
 */
trigger TGR_Contact on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    fflib_SObjectDomain.triggerHandler(DAO_Contact.class);
}