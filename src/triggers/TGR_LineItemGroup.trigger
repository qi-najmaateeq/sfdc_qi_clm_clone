trigger TGR_LineItemGroup on Line_Item_Group__c (before insert, after insert, after update, before update) {
    if(!UTL_ExecutionControl.stopTriggerExecution){
        fflib_SObjectDomain.triggerHandler(DAO_LineItemGroup.class);
    }
}