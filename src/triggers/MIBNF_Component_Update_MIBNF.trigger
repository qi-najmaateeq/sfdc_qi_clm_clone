trigger MIBNF_Component_Update_MIBNF on MIBNF_Component__c (after update)
{
    if(Trigger_Control_For_Migration__c.getInstance() != null && !Trigger_Control_For_Migration__c.getInstance().Disable_MIBNF_Trigger__c) {
        Set<Id> opportunityIdsSetForBNF = new Set<Id>();
        for(MIBNF_Component__c bnfRecord : Trigger.new){
            opportunityIdsSetForBNF.add(bnfRecord.Opportunity__c);
        }
        Map<Id,Opportunity> opportunityToUpdateMap = new Map<Id,Opportunity>();
        Set<String> bnfFieldsSet = new Set<String>{'Id','BNF_Status__c','Comp_Revenue_Analyst_user__c'};
        for(Integer i=2;i<=10;i++){
            bnfFieldsSet.add('Comp_Revenue_Analyst_user_'+i+'__c');
        }
        Map<Id, Opportunity> opportunityMap = new SLT_Opportunity().selectByIdWithMIBNF(opportunityIdsSetForBNF, new Set<String>{'Id'},bnfFieldsSet);
        Map<Id,Set<String>> oppIdToIdMap = new Map<Id,Set<String>>();
        Set<Id> revenueAnalaystUserIdsSet = new Set<Id>();
        for(Id mapId : opportunityMap.keySet()){
            Opportunity currentOpp = opportunityMap.get(mapId);
            List<Sobject> sobjList = currentOpp.getSObjects(CON_CRM.OPPORTUNITY_MIBNF_COMPONENT_RELATIONSHIP);
            List<MIBNF_Component__c> bnfRecordsList = new List<MIBNF_Component__c>();
            if(sobjList != null && sobjList.size() > 0){
                bnfRecordsList = (List<MIBNF_Component__c>)sobjList;
            }
            if(bnfRecordsList.size() > 0){
                Boolean isAnyBnfApproved = false;
                Boolean isAnyBnfSubmitted = false;
                Integer approvedBnfCount = 0;
                Set<String> bnfRecordsApprovalList = new Set<String>();
                for(MIBNF_Component__c bnfRecord : bnfRecordsList ){
                    if(bnfRecord.BNF_Status__c == MDM_Defines.BnfStatus_Map.get('ACCEPTED') || bnfRecord.BNF_Status__c == MDM_Defines.BnfStatus_Map.get('SAP_CONTRACT_CONFIRMED')){
                        isAnyBnfApproved = true;
                        approvedBnfCount++;
                    }else if(bnfRecord.BNF_Status__c == MDM_Defines.BnfStatus_Map.get('SUBMITTED') || bnfRecord.BNF_Status__c == MDM_Defines.BnfStatus_Map.get('LO_ACCEPTED') || bnfRecord.BNF_Status__c == MDM_Defines.BnfStatus_Map.get('RA_ACCEPTED') || bnfRecord.BNF_Status__c == MDM_Defines.BnfStatus_Map.get('SAP_CONTRACT_CREATED') || bnfRecord.BNF_Status__c == MDM_Defines.BnfStatus_Map.get('SAP_CONTRACT_PENDING')  || bnfRecord.BNF_Status__c == MDM_Defines.BnfStatus_Map.get('SAP_PENDING')){
                        isAnyBnfSubmitted = true;
                        if(Trigger.newMap.containsKey(bnfRecord.Id)){
                            if(bnfRecord.get('Comp_Revenue_Analyst_user__c') != null && bnfRecord.get('Comp_Revenue_Analyst_user__c') != ''){
                                bnfRecordsApprovalList.add(String.valueOf(bnfRecord.get('Comp_Revenue_Analyst_user__c')));
                                revenueAnalaystUserIdsSet.add(String.valueOf(bnfRecord.get('Comp_Revenue_Analyst_user__c')));
                            }
                            for(Integer i=2;i<=10;i++){
                                if(bnfRecord.get('Comp_Revenue_Analyst_user_'+i+'__c') != null && bnfRecord.get('Comp_Revenue_Analyst_user_'+i+'__c') != ''){
                                    bnfRecordsApprovalList.add(String.valueOf(bnfRecord.get('Comp_Revenue_Analyst_user_'+i+'__c')));
                                    revenueAnalaystUserIdsSet.add(String.valueOf(bnfRecord.get('Comp_Revenue_Analyst_user_'+i+'__c')));                            
                                }
                            }
                        }
                    }
                }
                currentOpp.Is_Any_BNF_Approved__c = isAnyBnfApproved;
                currentOpp.Is_Any_BNF_Submitted__c = isAnyBnfSubmitted;
                currentOpp.Approved_BNF_Count__c = approvedBnfCount;
                if(bnfRecordsApprovalList.size() > 0){
                    oppIdToIdMap.put(currentOpp.Id, bnfRecordsApprovalList);
                }
                opportunityToUpdateMap.put(currentOpp.Id,currentOpp);
            }
        }
        if(oppIdToIdMap.size() > 0){
            Map<Id, User> reveneuAnayalsytUsersMap = new SLT_User().selectByUserId(revenueAnalaystUserIdsSet,new Set<String>{'Id','LI_User_Id__c'});
            for(Id oppId : oppIdToIdMap.keySet()){
                Opportunity currentOpp = opportunityMap.get(oppId);
                Set<String> raUsersId = new Set<String>();
                for(String raId : oppIdToIdMap.get(oppId)){
                    if(reveneuAnayalsytUsersMap.containsKey(raId)){
                        raUsersId.add(reveneuAnayalsytUsersMap.get(raId).LI_User_Id__c);
                    }
                }
                currentOpp.Current_Approvers__c = JSON.serialize(raUsersId);  
                opportunityToUpdateMap.put(currentOpp.Id,currentOpp);
            }
        }
        if(opportunityToUpdateMap.size() > 0){
            UTL_ExecutionControl.stopTriggerExecution = true;
            update opportunityToUpdateMap.values(); 
            UTL_ExecutionControl.stopTriggerExecution = false;
        }
        if (!Global_Variables.MIBNF_Component_Update_In_Progress)
        {
            Global_Variables.MIBNF_Component_Update_In_Progress = true;
            Set<Id> MIBNF_Id_Set = new Set<Id>();
            for (MIBNF_Component__c MIBNF_Comp:trigger.New)
            {
                MIBNF_Id_Set.add(MIBNF_Comp.MIBNF__c);
            }
            Map<Id,MIBNF2__c> MIBNF_Map = new Map<Id,MIBNF2__c>([select Id,SAP_Master_Contract__c from MIBNF2__c where Id in :MIBNF_Id_Set]);
            for (MIBNF_Component__c MIBNF_Comp:trigger.New)
            {
                if (MIBNF_Map.get(MIBNF_Comp.MIBNF__c).SAP_Master_Contract__c == null && MIBNF_Comp.SAP_Master_Contract__c != null)
                {
                    MIBNF_Map.get(MIBNF_Comp.MIBNF__c).SAP_Master_Contract__c = MIBNF_Comp.SAP_Master_Contract__c;
                }
            }
            update MIBNF_Map.values();
            Global_Variables.MIBNF_Component_Update_In_Progress = false;
        }
    }
}