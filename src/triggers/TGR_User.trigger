/**
 * This trigger is used for User object.
 * version : 1.0
 */
trigger TGR_User on User (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if(!UTL_ExecutionControl.stopTriggerExecution)
        fflib_SObjectDomain.triggerHandler(DAO_User.class);
}