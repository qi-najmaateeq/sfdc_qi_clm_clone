//  Trigger to set the account reference to null when addresses are marked for deletion
//  This means that addresses marked for deletion are not available for selection
//  when choosing addresses for a BNF

trigger AddressUpdateDeletedAddresses on Address__c (before insert, before update) 
{
    /**************************************************************************
    Update by : Himanshu parashar
    Date : 3 Feb 2012
    Skip account null for sending temp insertion of Address and send Request mail
    ****************************************************************************/
    if(Global_Variables.RunAddressRequestTrigger)
    {
         for (Address__c A:Trigger.New)
        {
            //  Temporary fix to exclude dummy base customers created in SAP with codes above 700000
            if (A.SAP_Reference__c != null && A.SAP_Reference__c.isNumeric() && Integer.valueOf(A.SAP_Reference__c) >= 700000)
            {
                A.Marked_For_Deletion__c = true;
            }
            if (A.Marked_For_Deletion__c == true)
            {
                
                A.Account__c = null;
            }
        }
    }
    
    //Added By Rakesh : 20 Jan 2014 : Issue-3463
    //***** START ******
    if( Trigger.isUpdate && Trigger.isBefore )
    {
        String profileName=[Select Id,Name from Profile where Id = :userinfo.getProfileId()].Name;
        
        //Started by Naveena : Issue 6325 :2/12/2015 :start
        
        List<GroupMember>LisQueueMemeber = [SELECT Group.Name FROM GroupMember WHERE UserOrGroupId = :userInfo.getUserId()  AND Group.Type = 'Queue' AND Group.Name = 'MDM Approver Queue'];
        
        //ended by Naveena : Issue 6325 :2/12/2015
        
        BNF_Settings__c bnfSetting = BNF_Settings__c.getInstance();
        
        for (Address__c A:Trigger.New)
        {
            //Updated by Naveena : Issue 6325:2/12/2015 :added 'LisQueueMemeber' part in condition
            //updated by : Suman Sharma-- Date: 13 April, 2017 -- Issue-10718           
            if(!profileName.toLowerCase().contains('system administrator') && CON_CRM.updateAccountAddress == false && (LisQueueMemeber.size() == 0 || LisQueueMemeber.isEmpty()) && A.CreatedById == bnfSetting.Address_ETL_User_Id__c)
            {
                A.addError('Addresses cannot be edited in SFDC. Please request any changes through the MDM Global Helpdesk.');
            }
        }
    }
    //**** END : Issue-3463  *****
   
}